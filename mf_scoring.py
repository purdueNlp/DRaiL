import numpy as np
from scipy.spatial.distance import cosine
import os
import csv
import json

from drail.scoring import ScoringClass

class MoralFoundationSC(ScoringClass):

    def __init__(self, data_path):
        super(MoralFoundationSC, self).__init__()
        
        self.rule_scores = json.load(open(os.path.join(data_path, "local_shamik_scores.json")))
    
    def pretrained_score(self, features, fe_class):
        y_pred = []
        for i, sample in enumerate(features['input']):
            x = sample[0][0]
            y_pred.append(self.rule_scores[x])
        return np.array(y_pred)
        
    # Access to fe_class allows us to access the label embeddings for
    # scoring
    def cosine(self, features, fe_class):
        y_pred = [] # list will hold the scores for all samples
        for i, sample in enumerate(features['input']):
            x = sample[0][0]
            scores = [x, 1-x]
            #print(scores)
            y_pred.append(scores)

        return np.array(y_pred)

    def bias_neg(self, features, fe_class):
        y_pred = [] # list will hold the scores for all samples
        for i, sample in enumerate(features['input']):
            y_pred.append([1.0, 0.0])

        return np.array(y_pred)

    def bias_pos(self, features, fe_class):
        y_pred = [] # list will hold the scores for all samples
        for i, sample in enumerate(features['input']):
            y_pred.append([0.0, 1.0])

        return np.array(y_pred)
