## DRaiL

This repository contains the code for DRaiL, the neuro-symbolic declarative modeling framework introduced in our TACL 2021 paper: [**Modeling Content and Context with Deep Relational Learning**](https://aclanthology.org/2021.tacl-1.7/). If you build on these ideas or use any part of our code, please cite us!

```
@article{pacheco-goldwasser-2021-modeling,
    title = "Modeling Content and Context with Deep Relational Learning",
    author = "Pacheco, Maria Leonor  and
      Goldwasser, Dan",
    journal = "Transactions of the Association for Computational Linguistics",
    volume = "9",
    year = "2021",
    address = "Cambridge, MA",
    publisher = "MIT Press",
    url = "https://aclanthology.org/2021.tacl-1.7",
    doi = "10.1162/tacl_a_00357",
    pages = "100--119",
    abstract = "Building models for realistic natural language tasks requires dealing with long texts and accounting for complicated structural dependencies. Neural-symbolic representations have emerged as a way to combine the reasoning capabilities of symbolic methods, with the expressiveness of neural networks. However, most of the existing frameworks for combining neural and symbolic representations have been designed for classic relational learning tasks that work over a universe of symbolic entities and relations. In this paper, we present DRaiL, an open-source declarative framework for specifying deep relational models, designed to support a variety of NLP scenarios. Our framework supports easy integration with expressive language encoders, and provides an interface to study the interactions between representation, inference and learning.",
}
```

An earlier version of DRaiL was published in an EMNLP 2016 Workshop: [**Introducing DRAIL: a Step Towards Declarative Deep Relational Learning**](https://aclanthology.org/W16-5906/). Please, cite the TACL version. 

## Requirements
- Python 3
- gurobi optimizer (academic license can be requested)
- gurobipy (follow gurobi instructions, can be installed from package)
- Additional requirements in `requirements.txt`
- Instructions on how to install DRaiL from source, full documentation, and walkthrough examples can be found in the wiki below. 

## Documentation: [Wiki](https://gitlab.com/purdueNlp/DRaiL/-/wikis/home)


