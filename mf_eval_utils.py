from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import graphviz
import pickle
import os

class MFEvaluation(object):
    
    def __init__(self, predicted_heads, data_path):
        [tweet_id2tokens, entity_id2tokens, _, word_dict, _, _, word2idx] = \
            pickle.load(open(os.path.join(data_path, "drail_data.pickle"), "rb"), encoding="latin1")
        idx2word = {v: k for k, v in word2idx.items()}
        self.entity2name = {k: " ".join(idx2word[v] for v in values) for k, values in entity_id2tokens.items()}
        self.tweet2tokens = {k: " ".join(idx2word[v] for v in values) for k, values in tweet_id2tokens.items()}
        
        # data fed into DRaiL
        self.entity2label = {}
        self.entity2tweet = {}
        self.tweet2entities = {}
        self.tweet2label = {}
        self.tweet2ideo = {}
        self.tweet2topic = {}
        self.role2mf = {}; self.mf2roles = {}
        self.entity2group = {}
        self.concept2tweet = {}
        
        # for mapping group to names
        self.group2names = {}
        
        self.update_predictions(predicted_heads)
                
        with open(os.path.join(data_path, 'has_ideology.txt')) as fp:
            for line in fp:
                tweet, ideo = line.strip().split('\t')
                self.tweet2ideo[tweet] = ideo
        with open(os.path.join(data_path, 'has_topic.txt')) as fp:
            for line in fp:
                tweet, topic = line.strip().split('\t')
                self.tweet2topic[tweet] = topic
        with open(os.path.join(data_path, 'role_has_mf.txt')) as fp:
            for line in fp:
                role, mf = line.strip().split('\t')
                self.role2mf[role] = mf
                if mf not in self.mf2roles:
                    self.mf2roles[mf] = []
                self.mf2roles[mf].append(role)

        max_gr_index = 0
        with open(os.path.join(data_path, 'has_entity_group.txt')) as fp:
            for line in fp:
                ent, gr = line.strip().split('\t')
                _, gr_index = gr.split('-')
                max_gr_index = max(max_gr_index, int(gr_index))
                self.entity2group[ent] = gr
                if gr not in self.group2names:
                    self.group2names[gr] = []
                self.group2names[gr].append(self.entity2name[ent])
                
        for gr in self.group2names:
            self.group2names[gr] = Counter(self.group2names[gr]).most_common(1)[0][0]
        
        for ent in self.entity2label:
            if ent not in self.entity2group:
                max_gr_index += 1
                self.entity2group[ent] = "group-{}".format(max_gr_index)
                self.group2names["group-{}".format(max_gr_index)] = self.entity2name[ent]
                
                
        #print(self.group2names)
        
        #print(set(self.entity2label.values()))
        
        self.role2name =\
        {'fairness-q2': 'target-fair-cheat',
         'fairness-q3': 'ensure-fair',
         'fairness-q4': 'do-cheat',
         'authority-q2': 'just-auth',
         'authority-q3': 'just-auth-over',
         'authority-q4': 'fail-auth',
         'authority-q5': 'fail-auth-over',
         'sanctity-q1': 'target-purity-degrad',
         'sanctity-q2': 'preserve-purity',
         'sanctity-q3': 'cause-degradation',
         'care-q1': 'target-care-harm',
         'care-q2': 'cause-harm',
         'care-q3': 'provide-care',
         'loyalty-q3': 'target-loyal-betray',
         'loyalty-q4': 'be-loyal',
         'loyalty-q5': 'do-betray',
        }
        
    def update_predictions(self, predicted_heads):
        for h in predicted_heads:
            #print(h)
            pred_name, arguments = self._process_predicate(h)
            if pred_name == "HasRole":
                self.entity2label[arguments[1]] = arguments[-1]
                self.entity2tweet[arguments[1]] = arguments[0]
                if arguments[0] not in self.tweet2entities:
                    self.tweet2entities[arguments[0]] = set([])
                self.tweet2entities[arguments[0]].add(arguments[1])
            elif pred_name == "HasMf":
                self.tweet2label[arguments[0]] = arguments[-1]
            elif pred_name == "MentionsConcept" and arguments[-1] == "yes":
                if arguments[1] not in self.concept2tweet:
                    self.concept2tweet[arguments[1]] = set([])
                self.concept2tweet[arguments[1]].add(arguments[0])
        
    def get_roles(self):
        return list(self.role2name.values())
    
    def get_moral_foundations(self):
        return list(set(self.tweet2label.values()))
    
    def get_frequent_entities(self, topic='abort', k=10):
        relevant = [(self.group2names[self.entity2group[ent]])
                    for ent in self.entity2label
                    if self.tweet2topic[self.entity2tweet[ent]] == topic]
        return Counter(relevant).most_common(k)
        
    def _process_predicate(self, pred):
        pred_name, arguments = pred[:-1].split('(')
        arguments = arguments.split(',')
        return pred_name, arguments

    def top_k_entities(self, ideology, topic='abort', k=5):
        relevant = [(self.group2names[self.entity2group[ent]],
                     self.role2name[self.entity2label[ent]])
                    for ent in self.entity2label 
                    if self.tweet2ideo[self.entity2tweet[ent]] == ideology 
                    and self.tweet2topic[self.entity2tweet[ent]] == topic]
        return Counter(relevant).most_common(k)
    
    def top_k_entities_per_role(self, ideology, role, topic='abort', k=5):
        relevant = [self.group2names[self.entity2group[ent]]
                    for ent in self.entity2label 
                    if self.tweet2ideo[self.entity2tweet[ent]] == ideology 
                    and self.tweet2topic[self.entity2tweet[ent]] == topic
                    and self.role2name[self.entity2label[ent]] == role]
        return Counter(relevant).most_common(k)
    
    def _get_targets(self, ideology, topic, target_role):
        targets = [self.group2names[self.entity2group[ent]]
                    for ent in self.entity2label 
                    if self.tweet2ideo[self.entity2tweet[ent]] == ideology 
                    and self.tweet2topic[self.entity2tweet[ent]] == topic
                    and self.role2name[self.entity2label[ent]] == target_role]
        targets = [c[0] for c in Counter(targets).most_common(2)]
        return targets
        
    def _get_cause(self, ideology, topic, target, cause_role):
        cause = []
        entity_ids = list(self.entity2label.keys())
        
        for e in entity_ids:
            tw = self.entity2tweet[e]
            if self.group2names[self.entity2group[e]] != target \
                or  self.tweet2ideo[tw] != ideology \
                or self.tweet2topic[tw] != topic:
                continue
            
            for e2 in self.tweet2entities[tw]:
                if e != e2 \
                   and self.role2name[self.entity2label[e2]] == cause_role:
                    
                    cause.append(self.group2names[self.entity2group[e2]])

        #print(Counter(cause).most_common())
        cause  = [c[0] for c in Counter(cause).most_common(2)]
        
        return cause
    
    def _write_graph(self, dot, ideology, topic, mf, target_role, cause_pos, cause_neg):
        targets = self._get_targets(ideology, topic, target_role)
        #rint(targets, target_role, ideology, topic)
        for t in targets:
            dot.node(t, t, color='blue')
            if cause_neg:
                negative_cause = self._get_cause(ideology, topic, t, cause_neg)
                for n in negative_cause:
                    dot.node(n, n, color='red')
                    dot.edge(n, t, label=cause_neg)
            if cause_pos:
                positive_cause = self._get_cause(ideology, topic, t, cause_pos)
                for n in positive_cause:
                    if n in negative_cause:
                        dot.node(n, n, color='purple')
                    else:
                        dot.node(n, n, color='green')
                    dot.edge(n, t, label=cause_pos)
        return dot
        
    def entity_graph(self, ideology, mf, topic='abort'):
        dot = graphviz.Digraph(comment=mf)
        if mf == 'care':
            return self._write_graph(dot, ideology, topic, mf, 'target-care-harm', 'provide-care', 'cause-harm')
        elif mf == 'fairness':
            return self._write_graph(dot, ideology, topic, mf, 'target-fair-cheat', 'ensure-fair', 'do-cheat')
        elif mf == 'sanctity':
            return self._write_graph(dot, ideology, topic, mf, 'target-purity-degrad', 'preserve-purity', 'cause-degradation')
        elif mf == 'loyalty':
            return self._write_graph(dot, ideology, topic, mf, 'target-loyal-betray', 'be-loyal', 'do-betray')
        elif mf == 'authority':
            dot = self._write_graph(dot, ideology, topic, mf, 'fail-auth-over', None, 'fail-auth')
            return self._write_graph(dot, ideology, topic, mf, 'just-auth-over', 'just-auth', None)
        else:
            return None
        
    def _get_tweet_cause(self, tw, target, cause_role):
        cause = [];
        for e2 in self.tweet2entities[tw]:
            if e2 != target\
                and self.role2name[self.entity2label[e2]] == cause_role:
                    cause.append(self.group2names[self.entity2group[e2]])
        #cause = list(set(cause))
        #print(cause)
        return cause
        
    def _write_tweet_graph(self, dot, tw, target_role, cause_pos, cause_neg):
        targets = [e for e in self.tweet2entities[tw] if self.role2name[self.entity2label[e]] == target_role]
        targets = set(list(targets))
        for t in targets:
            dot.node(t, self.group2names[self.entity2group[t]], color='blue')
            if cause_neg:
                negative_cause = self._get_tweet_cause(tw, t, cause_neg)
                for n in negative_cause:
                    dot.node(n, n, color='red')
                    dot.edge(n, t, label=cause_neg)
            if cause_pos:
                positive_cause = self._get_tweet_cause(tw, t, cause_pos)
                for n in positive_cause:
                    if n in negative_cause:
                        dot.node(n, n, color='purple')
                    else:
                        dot.node(n, n, color='green')
                    dot.edge(n, t, label=cause_pos)
        return dot
       
           
    def tweet_graph(self, tw):
        dot = graphviz.Digraph(comment=tw)
        mf = self.tweet2label[tw]
        if mf == 'care':
            return self._write_tweet_graph(dot, tw, 'target-care-harm', 'provide-care', 'cause-harm')
        elif mf == 'fairness':
            return self._write_tweet_graph(dot, tw, 'target-fair-cheat', 'ensure-fair', 'do-cheat')
        elif mf == 'sanctity':
            return self._write_tweet_graph(dot, tw, 'target-purity-degrad', 'preserve-purity', 'cause-degradation')
        elif mf == 'loyalty':
            return self._write_tweet_graph(dot, tw, 'target-loyal-betray', 'be-loyal', 'do-betray')
        elif mf == 'authority':
            dot = self._write_tweet_graph(dot, tw, 'fail-auth-over', None, 'fail-auth')
            return self._write_tweet_graph(dot, tw, 'just-auth-over', 'just-auth', None)
        else:
            return None
        
    def _polarity_general(self, left, right, labels):
        labels = list((set(left) | set(right)) & set(labels))
        
        mc_left = [mf[0] for mf in Counter(left).most_common()]
        mc_right = [mf[0] for mf in Counter(right).most_common()]
        
        #print("left", mc_left, len(mc_left))
        #print("right", mc_right, len(mc_right))
        
        # For the ones that are missing
        for l in labels:
            if l not in mc_left:
                mc_left.append(l)
            if l not in mc_right:
                mc_right.append(l)
        
        #print("left", mc_left, len(mc_left))
        #print("right", mc_right, len(mc_right))
        #print(labels, len(labels))      
        
        left_rank=[]; right_rank=[]
        for mf in labels:
            right_rank.append((len(labels)-mc_right.index(mf))/float(len(labels)))
            left_rank.append((len(labels)-mc_left.index(mf))/float(len(labels)))
        
        x=np.array(left_rank)
        y=np.array(right_rank)
        z=np.array(labels)
        
        fig, ax = plt.subplots(figsize=(5, 5))
        ax.plot(x, y, 'o', color='black')
        ax.tick_params(labelsize=12)
        plt.rc('legend', fontsize=12)
        
        plt.rcParams['font.family'] = ['DejaVu Sans']
        ax.set_xlim([0, 1.1])
        ax.set_ylim([0, 1.1])
        
        for i, txt in enumerate(z):
            coordinates = (x[i], y[i] + 0.02)
            ax.plot(np.array([x[i]]), np.array([y[i]]), marker='^', markersize='10', color='black')
            ax.annotate(txt, coordinates, size=12)
            
        xb = np.arange(0, 1.2, 0.1)
        yb = np.arange(0, 1.2, 0.1)
        
        ax.plot(xb, yb, '--', color='black')
        
        ax.fill_between(xb, yb, color='#2a9df4')
        ax.fill_between(xb, yb, np.max(yb), color = '#ff8886')
        
        ax.set_xlabel("Rank Score in Democrats", fontsize=12)
        ax.set_ylabel("Rank Score in Republicans", fontsize=12)

    def mf_polarity_graph(self, topic='abort'):
        labels = list(set(self.tweet2label.values()))
        
        left = [self.tweet2label[tw] for tw in self.tweet2label 
                if self.tweet2ideo[tw] == 'left' 
                and self.tweet2topic[tw] == topic]
        right = [self.tweet2label[tw] for tw in self.tweet2label 
                 if self.tweet2ideo[tw] == 'right' 
                 and self.tweet2topic[tw] == topic]
        
        self._polarity_general(left, right, labels)
        
    def role_polarity_graph(self, entity, topic='abort'):
        labels = [self.role2name[r] for r in set(self.entity2label.values())]
        
        left = [self.role2name[self.entity2label[e]] for e in self.entity2label
                if self.group2names[self.entity2group[e]] == entity
                and self.tweet2ideo[self.entity2tweet[e]] == 'left' 
                and self.tweet2topic[self.entity2tweet[e]] == topic]
        right = [self.role2name[self.entity2label[e]] for e in self.entity2label
                if self.group2names[self.entity2group[e]] == entity
                and self.tweet2ideo[self.entity2tweet[e]] == 'right' 
                and self.tweet2topic[self.entity2tweet[e]] == topic]
        
        self._polarity_general(left, right, labels)

    def top_k_mfs(self, topic, ideology, k=5):
        relevant = [self.tweet2label[tw] for tw in self.tweet2label 
                    if self.tweet2ideo[tw] == ideology 
                    and self.tweet2topic[tw] == topic]

        return Counter(relevant).most_common(k)

    def _get_label_counts(self, relevant, labels):
        counter = Counter(relevant)
        return [counter[l] for l in labels]
    
    def mf_graph(self, topic='abort'):
        labels = list(set(self.tweet2label.values()))
        
        left = [self.tweet2label[tw] for tw in self.tweet2label 
                if self.tweet2ideo[tw] == 'left' 
                and self.tweet2topic[tw] == topic]
        right = [self.tweet2label[tw] for tw in self.tweet2label 
                 if self.tweet2ideo[tw] == 'right' 
                 and self.tweet2topic[tw] == topic]
    
        left_counts = self._get_label_counts(left, labels)
        right_counts = self._get_label_counts(right, labels)
        
        #print(left_counts, len(left_counts))
        #print(right_counts, len(right_counts))
        
        data = [left_counts, right_counts]
        X = np.arange(len(labels))

        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        plt.xticks(X, labels)
        ax.bar(X + 0.00, data[0], color = '#2a9df4', width = 0.25)
        ax.bar(X + 0.25, data[1], color = '#ff8886', width = 0.25)
        
        ax.legend(labels=['democrat', 'republican'])
        
    def show_examples_entity(self, ideology, entity, entity_role, topic='abort', k=1):
        cands = []
        for e in self.entity2label:
            
            tw = self.entity2tweet[e]
            
            if self.group2names[self.entity2group[e]] == entity\
                and self.role2name[self.entity2label[e]] == entity_role\
                and self.tweet2ideo[tw] == ideology\
                and self.tweet2topic[tw] == topic:
                
                cands.append([self.tweet2tokens[tw]])
        
        print("Total found: {}. Showing: {}".format(len(cands), min(len(cands), k)))
        for c in cands[:k]:
            print(c)
            print()
       
    def show_examples_entity_pair(self, ideology, entity_1, entity_role_1, entity_2, entity_role_2, topic='abort', k=1):
        cands = []
        
        for e in self.entity2label:
            
            tw = self.entity2tweet[e]
            
            if self.group2names[self.entity2group[e]] != entity_1 \
                or self.role2name[self.entity2label[e]] != entity_role_1 \
                or self.tweet2ideo[tw] != ideology \
                or self.tweet2topic[tw] != topic:
                continue
                
            #print("e", e, self.group2names[self.entity2group[e]])
            
            for e2 in self.tweet2entities[tw]:
                #print("\t", e2, self.group2names[self.entity2group[e2]])
                
                if (e != e2)\
                    and self.group2names[self.entity2group[e2]] == entity_2 \
                    and self.role2name[self.entity2label[e2]] == entity_role_2:
                    
                    cands.append((tw, self.tweet2tokens[tw]))
        
        #cands = list(cands)
        print("Total found: {}. Showing: {}".format(len(cands), min(len(cands), k)))
        for c in cands[:k]:
            print(c)
            print()
            
    def show_tweets_where_concept(self, concept, k=1):
        cands = []
        if concept in self.concept2tweet:
            for tw in self.concept2tweet[concept]:
                cands.append((tw, self.tweet2tokens[tw]))
        
        print("Total found: {}. Showing: {}".format(len(cands), min(len(cands), k)))
        for c in cands[:k]:
            print(c)
            print()
            