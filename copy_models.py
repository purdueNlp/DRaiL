import argparse
import scandir
import os

parser = argparse.ArgumentParser()
parser.add_argument("--source_dir", help="source directory", type=str, required=True)
parser.add_argument("--target_dir", help="target directory", type=str, required=True)
parser.add_argument("--source_model", help="source model", type=str, required=True)
parser.add_argument("--target_model", help="target model", type=str, required=True)
args = parser.parse_args()

source_subfolders = [f.name for f in os.scandir(args.source_dir) if f.is_dir()]

for name in source_subfolders:
    source_m = os.path.join(args.source_dir, name, args.source_model)
    target_dir = os.path.join(args.target_dir, name)
    if not os.path.exists(target_dir):
        print("mkdir {}".format(target_dir))
        os.mkdir(target_dir)

    target_m = os.path.join(args.target_dir, name, args.target_model)

    cmd = "cp {0} {1}".format(source_m, target_m)
    print(cmd)
    os.system(cmd)

