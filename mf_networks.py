import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel, BertModel

from drail.neuro.nn_model import NeuralNetworks

class BertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim

    def build_architecture(self, rule_template, fe, shared_params={}):
        # Get name of bert-model to use 
        self.bert_model_name = self.config['bert_model']
        # Load config file and pre-trained model
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)
        self.bert_model = AutoModel.from_pretrained(self.bert_model_name, add_pooling_layer=True)
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        hidden_dim=bert_config.hidden_size
        if "ideology" in self.config["features"]:
            self.ideology2hidden=torch.nn.Linear(self.config["ideology_input_dim"], self.config["ideology_hidden_dim"])
            if self.use_gpu:
                self.ideology2hidden = self.ideology2hidden.cuda()
                
            hidden_dim+=self.config["ideology_hidden_dim"]

        if "topic" in self.config["features"]:
            self.topic2hidden=torch.nn.Linear(self.config["topic_input_dim"], self.config["topic_hidden_dim"])
            if self.use_gpu:
                self.topic2hidden = self.topic2hidden.cuda()
            
            hidden_dim+=self.config["topic_hidden_dim"]
        
        self.hidden2label = torch.nn.Linear(hidden_dim, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        input_index = 0
        
        # Get bert-like input
        if "tweet" in self.config["features"] and "entity" in self.config["features"]:
            input_ids_1 = self._get_inputs(x, x_axis=input_index, y_axis=0)
            input_mask_1 = self._get_inputs(x, x_axis=input_index, y_axis=1)
            input_index += 1

            input_ids_2 = self._get_inputs(x, x_axis=input_index, y_axis=0)
            input_mask_2 = self._get_inputs(x, x_axis=input_index, y_axis=1)
            input_index += 1

            #print(input_ids_1.shape, input_mask_1.shape)
            #print(input_ids_2.shape, input_mask_2.shape)

            segment_ids_1 = np.zeros(input_ids_1.shape)
            segment_ids_2 = np.ones(input_ids_2.shape)

            input_ids = np.hstack([input_ids_1, input_ids_2])
            input_mask = np.hstack([input_mask_1, input_mask_2])
            segment_ids = np.hstack([segment_ids_1, segment_ids_2])

        elif "tweet" in self.config["features"]:
            input_ids = self._get_inputs(x, x_axis=input_index, y_axis=0)
            input_mask = self._get_inputs(x, x_axis=input_index, y_axis=1)
            input_index += 1
            segment_ids = np.zeros(input_ids.shape)

        else:
            print("Not supported")

        input_ids = self._get_long_tensor(input_ids)
        input_mask = self._get_long_tensor(input_mask)
        segment_ids = self._get_long_tensor(segment_ids)

        #print(input_ids.shape)
        #print(input_mask.shape)
        #print(segment_ids.shape)

        # Run bert
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None,
                                  head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)
        
        
        encoded_ideology=None
        if "ideology" in self.config["features"]:            
            ideology_feature = self._get_inputs(x, x_axis=input_index)
            input_index += 1
            
            ideology_feature = self._get_float_tensor(ideology_feature)
            #print (ideology_feature)
            encoded_ideology = self.ideology2hidden(ideology_feature)
            encoded_ideology = F.relu(encoded_ideology)
            pooled_output = torch.cat([pooled_output, encoded_ideology], dim=1)
        
        encoded_topic=None
        if "topic" in self.config["features"]:
            topic_feature = self._get_inputs(x, x_axis=input_index)
            input_index += 1
            
            topic_feature = self._get_float_tensor(topic_feature)
            #print(topic_feature.shape)
            encoded_topic = self.topic2hidden(topic_feature)
            encoded_topic = F.relu(encoded_topic)
            pooled_output = torch.cat([pooled_output, encoded_topic], dim=1)


        logits = self.hidden2label(pooled_output)
        probas = F.softmax(logits, dim=1)

        return logits, probas
