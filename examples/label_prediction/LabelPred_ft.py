from drail.features.feature_extractor import FeatureExtractor
import json
import numpy as np
from  scipy.spatial.distance import cosine

class LabelPred_ft(FeatureExtractor):

    def __init__(self, instance_grds, node_emd_filename=None, doc_filename=None, label_filename=None):
        super(LabelPred_ft, self).__init__(instance_grds)

        self.node_emd_filename = node_emd_filename
        self.doc_filename = doc_filename
        self.label_filename = label_filename

    def build(self):

        self.label_idx = {}
        if self.label_filename:
            with open(self.label_filename) as f:
                for i, lab in enumerate(f.readlines()):
                    self.label_idx[lab.strip()] = i

        if self.node_emd_filename:
            with open(self.node_emd_filename) as f:
                self.node_embedding = {}
                num_nodes, self.node_embedding_size = map(int, f.readline().strip().split())

                print num_nodes, self.node_embedding_size

                for line in f.readlines():
                    tokens = line.strip().split()
                    self.node_embedding[int(tokens[0])] = map(float, tokens[1:])

        if self.doc_filename:
            with open(self.doc_filename) as f:
                self.docs = json.load(f)

    def extract_multiclass_head(self, instance_grd):
        label = instance_grd.get_head_predicate()
        return self.label_idx[label['arguments'][1]]

    def bias(self, isntance_grd):
        return np.array([1], dtype='f');

    def extract_node_embedding(self, instance_grd):
        
        papers = instance_grd.get_body_predicates("InGraph")

        v = np.asarray([])

        for paper in papers:

            v = np.hstack([v, self.node_embedding.get(paper['arguments'][0], self.rng.uniform(-0.0025, 0.0025, self.node_embedding_size))])

        return v


    def extract_document(self, instance_grd):

        papers = instance_grd.get_body_predicates("InGraph")

        v = np.asarray([])

        for paper in papers:

            v = np.hstack([v, self.docs[str(paper['arguments'][0])]])

        return v

    def extract_label(self, instance_grd):

        labels = instance_grd.get_body_predicates("HasLabel")

        labels = [l['arguments'][1] for l in labels]
        label_idxs = [self.label_idx[l] for l in labels]
        vec = []
        for lbl_idx in label_idxs:
            vec += [(i == lbl_idx)*1 for i in range(len(self.label_idx))]
        return np.array(vec, dtype='f')

    def extract_doc_similarity(self, instance_grd):

        papers = instance_grd.get_body_predicates("Cites")[0]

        p1, p2 = papers['arguments'][0], papers['arguments'][1]

        return 1 - cosine(self.docs[str(p1)], self.docs[str(p2)])

    def extract_emd_similarity(self, instance_grd):

        papers = instance_grd.get_body_predicates("Cites")[0]

        p1, p2 = papers['arguments'][0], papers['arguments'][1]

        return 1 - cosine(self.node_embedding[p1], self.node_embedding[p2])

    def extract_first_embedding(self, instance_grd):

        citation = instance_grd.get_body_predicates("Cites")[0]

        return self.node_embedding.get(citation['arguments'][0],self.rng.uniform(-0.0025, 0.0025, self.node_embedding_size))

    def extract_second_embedding(self, instance_grd):

        citation = instance_grd.get_body_predicates("Cites")[0]

        return self.node_embedding.get(citation['arguments'][1],self.rng.uniform(-0.0025, 0.0025, self.node_embedding_size))

    def extract_first_doc(self, instance_grd):

        citation = instance_grd.get_body_predicates("Cites")[0]

        return self.docs[str(citation['arguments'][0])]

    def extract_second_doc(self, instance_grd):

        citation = instance_grd.get_body_predicates("Cites")[0]

        return self.docs[str(citation['arguments'][1])]
    
