3312
1656 802
1300 650
356 183
0
3264 128
Features being used
extract_document
SIZE OF VECTOR (3703,)
Paper(B) & Graph(G) & InGraph(B,G) => HasLabel(B,X^PaperLabel-Multiclass-6)
Building nnet for predicate HasLabel
{u'models': [{u'layers': [{u'ltype': u'Dense', u'n_hidden': 128, u'act_func': u'tanh'}, {u'ltype': u'Dense', u'act_func': u'softmax'}], u'weighted_examples': True, u'epsilon': 1e-06, u'learning_rate': 0.06, u'measurement': u'acc', u'batch_size': 100, u'patience': 15, u'rho': 1e-06, u'gradient_method': u'SGD', u'average': u'macro', u'momentum': 0.9}, {u'layers': [{u'ltype': u'Dense', u'n_hidden': 128, u'act_func': u'relu'}, {u'ltype': u'Dense', u'act_func': u'softmax'}], u'weighted_examples': True, u'epsilon': 1e-06, u'learning_rate': 0.06, u'measurement': u'acc', u'batch_size': 100, u'patience': 15, u'rho': 1e-06, u'gradient_method': u'SGD', u'average': u'macro', u'momentum': 0.9}, {u'layers': [{u'ltype': u'Dense', u'act_func': u'softmax'}], u'weighted_examples': True, u'epsilon': 1e-06, u'learning_rate': 0.06, u'measurement': u'acc', u'batch_size': 100, u'patience': 15, u'rho': 1e-06, u'gradient_method': u'SGD', u'average': u'macro', u'momentum': 0.9}, {u'layers': [{u'ltype': u'Dense', u'act_func': u'softmax'}], u'weighted_examples': True, u'epsilon': 1e-06, u'learning_rate': 0.06, u'measurement': u'acc', u'batch_size': 100, u'patience': 15, u'rho': 1e-06, u'gradient_method': u'SGD', u'average': u'macro', u'momentum': 0.9}]}
activation <function tanh at 0x7fca4a8762a8>
Extracting train rules...
Extracting test rules...
Extracting dev rules...
matches:  163 / 854
matches:  62 / 356
matches:  114 / 650
114 / 650
matches:  309 / 854
matches:  139 / 356
epoch 0 loss 50.634021759 train_acc 0.378461538462 test_acc 0.361826697892 dev_acc 0.390449438202
matches:  246 / 650
246 / 650
matches:  188 / 854
matches:  78 / 356
epoch 1 loss 160.65133667 train_acc 0.204615384615 test_acc 0.220140515222 dev_acc 0.219101123596
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 2 loss 447.143280029 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 3 loss 639.026733398 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 4 loss 638.55859375 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 5 loss 634.139587402 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 6 loss 633.768737793 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 7 loss 629.445007324 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 8 loss 629.167602539 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 9 loss 624.935424805 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 10 loss 624.747741699 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 11 loss 620.60357666 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 12 loss 620.50201416 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 13 loss 616.442382812 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 14 loss 616.42364502 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 15 loss 612.445129395 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 16 loss 612.505981445 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 17 loss 608.605407715 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 18 loss 608.742675781 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 19 loss 604.916992188 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 20 loss 605.127624512 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 21 loss 601.373901367 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 22 loss 601.655029297 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 23 loss 597.970397949 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 24 loss 598.319335938 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 25 loss 594.701049805 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 26 loss 595.114990234 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 27 loss 591.560546875 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 28 loss 592.036987305 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 29 loss 588.543762207 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 30 loss 589.080200195 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 31 loss 585.645812988 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 32 loss 586.239990234 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 33 loss 582.862121582 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 34 loss 583.511657715 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 35 loss 580.188049316 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 36 loss 580.890808105 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 37 loss 577.619384766 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 38 loss 578.373291016 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 39 loss 575.151916504 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 40 loss 575.95489502 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 41 loss 572.781738281 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 42 loss 573.631835938 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 43 loss 570.504882812 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 44 loss 571.40032959 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 45 loss 568.317749023 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 46 loss 569.256774902 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 47 loss 566.21685791 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 48 loss 567.197631836 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 49 loss 564.198669434 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 50 loss 565.219665527 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 51 loss 562.260070801 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 52 loss 563.319580078 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 53 loss 560.397827148 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 54 loss 561.494445801 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 55 loss 558.609008789 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 56 loss 559.741149902 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 57 loss 556.890625 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 58 loss 558.057006836 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 59 loss 555.239990234 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 60 loss 556.439208984 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 61 loss 553.65435791 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 62 loss 554.885131836 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 63 loss 552.131225586 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 64 loss 553.392333984 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 65 loss 550.66809082 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 66 loss 551.958312988 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 67 loss 549.262634277 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 68 loss 550.580810547 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 69 loss 547.912536621 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 70 loss 549.257568359 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 71 loss 546.615661621 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 72 loss 547.98651123 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 73 loss 545.369873047 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 74 loss 546.76550293 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 75 loss 544.173217773 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 76 loss 545.592651367 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 77 loss 543.023681641 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 78 loss 544.466003418 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 79 loss 541.919433594 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 80 loss 543.383728027 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 81 loss 540.858703613 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 82 loss 542.344116211 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 83 loss 539.83972168 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 84 loss 541.345458984 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 85 loss 538.860961914 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 86 loss 540.386108398 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 87 loss 537.920715332 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 88 loss 539.464599609 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 89 loss 537.017578125 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 90 loss 538.579406738 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 91 loss 536.150024414 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 92 loss 537.729125977 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 93 loss 535.316589355 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 94 loss 536.91229248 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 95 loss 534.516052246 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 96 loss 536.127685547 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 97 loss 533.747009277 train_acc 0.204615384615 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
matches:  179 / 854
matches:  74 / 356
epoch 98 loss 535.373962402 train_acc 0.207692307692 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  135 / 650
135 / 650
matches:  179 / 854
matches:  74 / 356
epoch 99 loss 533.008300781 train_acc 0.206153846154 test_acc 0.209601873536 dev_acc 0.207865168539
matches:  133 / 650
133 / 650
HasLabel Final accuracy: 0.210% 
HasLabel Final F1: 0.210% 
+------------------+----------------+----------------+----------------+----------------+----------------+----------------+---------+-------------+
| Class/Classifier |   Precision    |     Recall     |       F1       | NegPred Value  |    TN rate     |    Accuracy    | Support | Neg Support |
+------------------+----------------+----------------+----------------+----------------+----------------+----------------+---------+-------------+
|        AI        |       0        |      0.0       |       0        | 0.925058548009 |      1.0       | 0.925058548009 |   128   |     1580    |
|        ML        |       0        |      0.0       |       0        | 0.814988290398 |      1.0       | 0.814988290398 |   316   |     1392    |
|        IR        |       0        |      0.0       |       0        | 0.803278688525 |      1.0       | 0.803278688525 |   336   |     1372    |
|        DB        | 0.209601873536 |      1.0       | 0.346563407551 |       0        |      0.0       | 0.209601873536 |   358   |     1350    |
|      Agents      |       0        |      0.0       |       0        | 0.819672131148 |      1.0       | 0.819672131148 |   308   |     1400    |
|       HCI        |       0        |      0.0       |       0        | 0.846604215457 |      1.0       | 0.846604215457 |   262   |     1446    |
|     HasLabel     | 0.209601873536 | 0.209601873536 | 0.209601873536 | 0.841920374707 | 0.841920374707 | 0.209601873536 |   1708  |     8540    |
+------------------+----------------+----------------+----------------+----------------+----------------+----------------+---------+-------------+
