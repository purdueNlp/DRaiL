train_set = set([])
with open('train0.txt') as f:
    for line in f:
        train_set.add(line.strip())

test_set = set([])
with open('test0.txt') as f:
    for line in f:
        test_set.add(line.strip())

dev_set = set([])
with open('dev0.txt') as f:
    for line in f:
        dev_set.add(line.strip())

train_set = train_set - dev_set

print "train", len(train_set)
print "test", len(test_set)
print "dev", len(dev_set)
print len(set(train_set) & set(test_set))
print len(set(train_set) & set(dev_set))

graph = {}; graph_rev = {}
graph_test = {}; graph_dev = {}
with open('citeseer.links') as f:
    for line in f:
        (u, v) = line.strip().split()
        if u in train_set and v in train_set:
            if u not in graph:
                graph[u] = [v]
            else:
                graph[u].append(v)

            if v not in graph_rev:
                graph_rev[v] = [u]
            else:
                graph_rev[v].append(u)

        if u in test_set and v in test_set:
            if u not in graph_test:
                graph_test[u] = [v]
            else:
                graph_test[u].append(v)
        
        if u in dev_set and v in dev_set:
            if u not in graph_dev:
                graph_dev[u] = [v]
            else:
                graph_dev[u].append(v)

for v in train_set:
    if v not in graph:
        graph[v] = []

for v in test_set:
    if v not in graph_test:
        graph_test[v] = []

for v in dev_set:
    if v not in graph_dev:
        graph_dev[v] = []


def findPaths(G,u,n,excludeSet = None):
    if excludeSet == None:
        excludeSet = set([u])
    else:
        excludeSet.add(u)
    if n==0:
        return [[u]]
    if u in G:
        paths = [[u]+path for neighbor in G[u] if neighbor not in excludeSet
                for path in findPaths(G,neighbor,n-1,excludeSet)]
        excludeSet.remove(u)
    else:
        paths = []
    return paths

def findTrails(G,u,n):
    if n==0:
        return [[u]]
    if u in G:
        paths = [[u]+path for neighbor in G[u]
                for path in findPaths(G,neighbor,n-1)]
    else:
        paths = []
    return paths

# build all paths of size s
paths = {}; paths_rev = {}
'''
paths[0] = set(map(str, range(1, 3313)))
trails[0] = set(map(str, range(1, 3313)))
print "Finding paths of size", 0, len(paths[0])
print "Finding trails of size", 0, len(trails[0])
print
'''


s = 1

look_for_paths = True
look_for_paths_rev = True

while look_for_paths or look_for_paths_rev:
    paths[s] = {}; paths_rev[s] = {}
    paths_found = False; paths_rev_found = False

    # path of size s starting from v
    for u in graph:
        paths_ = set(map(str, findPaths(graph, u, s)))
        paths_rev_ = set(map(str, findPaths(graph_rev, u, s)))

        if s > 1:
            paths[s][u] = paths[s-1][u]
            paths_rev[s][u] = paths_rev[s-1][u]
        else:
            paths[s][u] = set([])
            paths_rev[s][u] = set([])

        if len(paths_) > 0:
            paths[s][u] |= paths_
            paths_found = True

        if len(paths_rev_) > 0:
            paths_rev[s][u] |= paths_rev_
            paths_rev_found = True


    if not paths_found:
        look_for_paths = False
    if not paths_rev_found:
        look_for_paths_rev = False

    print "subgraphs in s", s, ": ", len(paths[s])
    print "subgraphs in s", s, ": ", len(paths_rev[s])

    s += 1

for s in paths:
    if s == 0:
        continue

    in_instance_edge = set([]); in_instance_vertex = set([])
    instance_f = "instance_n{0}.txt".format(s)
    in_instance_edge_f = "edge_in_instance_n{0}.txt".format(s)
    in_instance_vertex_f = "vertex_in_instance_n{0}.txt".format(s)
    f1 = open(instance_f, 'w')
    f1.write("0\n")
    f1.write("1\n")

    for i, u in enumerate(paths[s]):
        f1.write(str(i + 2) + "\n")
        in_instance_vertex.add((u, i + 2))
        for path in paths[s][u]:
            path = eval(path)
            pairs = zip(path, path[1:])
            for (a, b) in pairs:
                in_instance_edge.add((a, b, i + 2))
                in_instance_vertex.add((a, i + 2))
                in_instance_vertex.add((b, i + 2))
        for rev_path in paths_rev[s][u]:
            rev_path = eval(rev_path)
            pairs = zip(rev_path, rev_path[1:])
            for (a, b) in pairs:
                in_instance_edge.add((b, a, i + 2))
                in_instance_vertex.add((a, i + 2))
                in_instance_vertex.add((b, i + 2))
    f1.close()

    f2 = open(in_instance_edge_f, 'w')
    f3 = open(in_instance_vertex_f, 'w')
    for (u, v, g) in in_instance_edge:
        f2.write(u + "\t" + v + "\t" + str(g) + "\n")
    for (u, g) in in_instance_vertex:
        f3.write(u + "\t" + str(g) + "\n")
    for u in graph_test:
        f3.write(u + "\t0\n")
        for v in graph_test[u]:
            f2.write(u + "\t" + v + "\t0\n")
    for u in graph_dev:
        f3.write(u + "\t1\n")
        for v in graph_dev[u]:
            f2.write(u + "\t" + v + "\t1\n")
    f2.close()
    f3.close()
