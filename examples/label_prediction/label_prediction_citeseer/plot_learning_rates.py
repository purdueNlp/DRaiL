import matplotlib.pyplot as plt
import numpy as np
import sys

def parse_doc(path):
    loss = []; train_acc = []; dev_acc = []
    local_train_acc = []; local_dev_acc = []
    with open(path) as f:
        for line in f:
            line = line.strip()
            if line.startswith('epoch'):
                data = line.split()
                loss.append(float(data[3]))
                train_acc.append(float(data[5]))
                #test_acc.append(float(data[7]))
                dev_acc.append(float(data[9]))
            elif line.startswith('local'):
                data = line.split()
                local_train_acc.append(float(data[2]))
                #local_test_acc.append(float(data[4]))
                local_dev_acc.append(float(data[6]))
    return loss, train_acc, dev_acc, \
           local_train_acc, local_dev_acc
'''
DOCS = ['001', '005', '01', '05', '1', '5', '1x']
LR = ['0.001', '0.005', '0.01', '0.05', '0.1', '0.5', '1']
DOC_PATH = 'experiments/output_doc_psl_global_{0}.txt'
COLORS = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

LOSS_ALL = []; TRAIN_ALL = []; DEV_ALL = []
LCL_TRAIN_ALL = []; LCL_DEV_ALL = []
#TEST_ALL = []; LCL_TEST_ALL = []
for doc in DOCS:
    #local_test_acc = []
    path = DOC_PATH.format(doc)
    loss, train_acc, dev_acc, local_train_acc, local_dev_acc = \
            parse_doc(path)

    LOSS_ALL.append(loss)
    TRAIN_ALL.append(train_acc)
    DEV_ALL.append(dev_acc)
    #TEST_ALL.append(test_acc)
    LCL_TRAIN_ALL.append(local_train_acc)
    LCL_DEV_ALL.append(local_dev_acc)
    #LCL_TEST_ALL.append(local_test_acc)
'''

'''
plt.figure()
plt.title("Curve for different learning rates")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.grid()


for i in range(7):
    plt.plot(range(0, 700), TRAIN_ALL[i], '-', color=COLORS[i],
             label="train_"+LR[i])
    plt.plot(range(0, 700), DEV_ALL[i], '--', color=COLORS[i],
             label="dev_"+LR[i])

plt.legend(loc="best")
plt.show()
'''


plt.figure()
plt.title("Training DOC network 'globally' LR=0.05")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.grid()

loss, train_acc, dev_acc, local_train_acc, local_dev_acc = \
        parse_doc(sys.argv[1])

plt.plot(range(0, len(loss)), train_acc, '-', color='r',
         label="train_0.05")
plt.plot(range(0, len(loss)), dev_acc, '--', color='r',
         label="dev_0.05")

plt.plot(range(0, len(loss)), local_train_acc, '-', color='g',
         label="doc_train_0.05")
plt.plot(range(0, len(loss)), local_dev_acc, '--', color='g',
         label="doc_dev_0.05")


plt.legend(loc="best")
plt.show()
