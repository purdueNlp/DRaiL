import sys
import os
import numpy as np
import cProfile as profile
from drail.model.argument import ArgumentType
from drail.learn.global_learner_v2 import GlobalLearner
from drail.learn.local_learner import LocalLearner

def get_train(test, data_size):

    train = []

    for i in range(1,data_size+1):
        if i not in test:
            train.append(i)

    return train

def getopts(argv):

    opts = {}
    while argv:
        if argv[0][0]=='-':
            opts[argv[0][1:]] = argv[1]
        argv = argv[1:]
    return opts

def load_ids(filename):
    ids = []
    with open(filename) as f:
        for l in f.readlines():
           id_ = l.strip().split(',')[0]
           ids.append(int(id_))

    return ids

def main():

    opts = getopts(sys.argv)

    path = sys.argv[1]
    rule_file = os.path.join(path, opts.get('rulefile','rule.dr'))
    arch_file = os.path.join(path, opts.get('conffile', 'config.json'))
    nemd_file = opts.get('emd', None)
    docs_file = os.path.join(path, opts.get('docfile','document.json'))

    papers_file = os.path.join(path, opts.get('papersfile', 'paper.txt'))
    labl_file = os.path.join(path, opts.get('labelfile', 'labels.txt'))
    hot_start = int(opts.get('hotstart', 0))
    dataset   = opts.get('dataset', 'citeseer')
    mode = opts.get('mode', 'global')
    lr = float(opts.get('lr', 0.01))

    if dataset == 'citeseer':
        data_size  = 3312
        train_size = 1300
        dev_size = 356
    elif dataset == 'cora':
        data_size = 2708
        train_size = 1100

    np.random.seed(123)

    if mode == "global":
        learner=GlobalLearner(learning_rate=lr)
    else:
        learner=LocalLearner()

    learner.compile_rules(rule_file)
    db=learner.create_dataset(path)

    for i in range(1):
        test_file = os.path.join(path,'test{0}.txt'.format(i))
        train_file = os.path.join(path, 'train{0}.txt'.format(i))
        knowns_file = os.path.join(path, 'seeds0.txt'.format(i))

        print data_size
        test = load_ids(test_file)
        train = load_ids(train_file)
        dev = np.random.choice(train, dev_size, replace=False)
        knowns = load_ids(knowns_file)
        train = list(set(train) - set(dev))

        print len(test), len(set(test) & set(knowns))
        print len(train), len(set(train) & set(knowns))
        print len(dev), len(set(dev) & set(knowns))
        print len(set(train) & set(test))

        db.add_filters(filters=[
            ("Paper", "isTrain", "paperId", train),
            ("Paper", "isDev", "paperId", dev),
            ("Paper", "isTest", "paperId", test),
            ("Paper", "isKnown", "paperId", knowns)
            ])

        learner.build_feature_extractors(db,
                                         filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                                         node_emd_filename=nemd_file,
                                         doc_filename=docs_file,
                                         label_filename=labl_file)

        # programatically define the neural nets
        '''
        configs = {'models':[]}
        for id, template in enumerate(learner.ruleset['rule']):
            configs['models'].append({"layers":[],
                                      "learning_rate": 0.05,
                                      "patience": 15,
                                      "gradient_method": "SGD",
                                      "batch_size": 650,
                                      "momentum": 0.9,
                                      "epsilon": 0.000001,
                                      "rho": 0.000001,
                                      "measurement": "acc",
                                      "average": "macro",
                                      "weighted_examples": False,
                                      "regularizer": "l2",
                                      "lambda_2": 0.1})

            # use no hidden layers for now, test using log reg
            configs['models'][id]['layers'].append(
                    {'ltype': 'Dense', 'act_func':'softmax'})
        learner.build_models(db, configs, isdic=True)
        '''

        learner.build_models(db, arch_file)

        '''
        profile.runctx("learner.train(db, train_ids, dev_ids, filter_pred, filter_var, n_epoch)",
                {}, {'learner': learner, 'db': db, 'train_ids': train[:train_size],
                            'dev_ids': train[train_size:], "filter_pred": "Paper",
                            'filter_var': "B", "n_epoch": 1})
        '''


        if mode == "global":
            # globally we always need inference data
            learner.extract_data(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "isTest", 1)])
            _ = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "isTest", 1)],
                    n_epoch=500, hot_start=hot_start)
        else:
            _ = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "isTest", 1)])
            # locally we only need inference data for testing
            learner.extract_data(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "isTest", 1)])

            res = learner.predict(
                    db,
                    filters=[("A", "isTest", 1), ("B", "isTest", 1)])
            '''
            for ruleidx in range(len(nnet_scores)):
                print "SCORES FOR RULE", ruleidx
                for key in nnet_scores[ruleidx]:
                    print "\tclass", key
                    for score in nnet_scores[ruleidx][key]:
                        print "\t",score
                        break
                    print
            '''
            print res.get_classifiers()

            for classifier in res.get_classifiers():
                print "{} Final accuracy: {:.3f}% ".format(classifier, res.classifier_accuracy(classifier, is_binary=False))
                print "{} Final F1: {:.3f}% ".format(classifier, res.bin_f1(classifier))
                print res.classification_report(classifier, is_binary=False)

if __name__ == "__main__":
    main()
