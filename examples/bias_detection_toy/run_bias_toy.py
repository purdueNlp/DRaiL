import numpy as np
import os
from sklearn.metrics import *
from drail.learn.local_learner import LocalLearner
from drail.learn.global_learner import GlobalLearner
import json

def main():
    path = 'examples/bias_detection_toy'
    rule_file = os.path.join(path, 'rule.dr')
    np.random.seed(123)

    learner = LocalLearner()
    learner.compile_rules(rule_file)
    db = learner.create_dataset(path)

    learner.build_feature_extractors(
        db, embeddings=os.path.join(path, 'toy_sim.json'),
        femodule_path=path)

    learner.extract_data(db)
    res,predicates = learner.predict(db, fold='test', scmodule_path=path, get_predicates=True)

    #print predicate
    doc2gold={}
    f=open("examples/bias_detection_toy/doc_has_label.txt","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.split(" ")
        doc=int(l[0])
        label=int(l[1])
        doc2gold[doc]=label
    f.close()

    print "Total Docs: %d"%len(doc2gold)

    doc2prediction={}
    for i in predicates:
        predicate=str(i)
        print predicate
        predicate=predicate.split("(")
        if predicate[0]!="DocHasLabel":
            continue

        predicate=predicate[1].split(")")
        predicate=predicate[0].split(",")
        doc=predicate[0]
        label=predicate[1]
        doc2prediction[int(doc)]=int(label)

    '''
    doc2label={}
    f=open("examples/bias_detection/cliques.txt","r")
    f1=open("examples/bias_detection/prediction.txt","w")
    with open("examples/bias_detection/embeddings.json") as f2:
        embeddings_dict = json.load(f2)
    while True:
        l=f.readline()
        if not l:
            break
        l = l.strip("\n")
        if l!="#":
            l=l.split(" ")
            doc=int(l[0])
            label=int(l[1])
            doc2label[doc]=label
            f1.write(str(doc)+" "+str(label)+" "+str(doc2prediction[doc])+"\n")
        else:
            f1.write("#\n")
    f.close()
    f1.close()
    '''
    '''
    for i in res.metrics:
	print i
	for j in res.metrics[i]:
		print j
    '''
    #print res.metrics

    print res.metrics


    y_gold = res.metrics['DocHasLabel']['gold_data']
    y_pred = res.metrics['DocHasLabel']['pred_data']

    correct_count=0
    total=0
    for doc in doc2gold:
        if doc2gold[doc]==doc2prediction[doc]:
            correct_count+=1
        total+=1
    print "total: %d\ncorrect: %d"%(total,correct_count)
    #print "accuracy: %lf"%accuracy_score(y_gold,y_pred)
    # any sklearn metrics can be used

if __name__ == "__main__":
    main()
