import json
import sys
import networkx as nx
from networkx.algorithms import approximation as apxa
import numpy as np
from numpy import array
from math import *
from scipy.spatial.distance import cosine
import math
from sklearn.metrics import f1_score

id2name={}
name2id={}
label_id2embd={}
doc_id2embd={}
user_id2embd={}
doc2label={}
user2label={}
followee2label={}
docs_written=[]
label_start, label_end=0,2
followee_start, followee_end =3,137
node_start, node_end =138,1741
doc_start, doc_end =1742,12126
node2adj={}
docs=set()
users=set()
labels=set()
followees=set()
user2sim_scores={}
left_followee=set()
right_followee=set()
center_followee=set()
left_users=set()
center_users=set()
right_users=set()

def LoadData():
    global id2name,name2id,label_id2embd,user_id2embd,doc_id2embd,doc2label,node2adj,docs,users,labels,followee2label,followees

    f=open("mygraph.mapping.supervised","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        id=int(l[0])
        name=str(l[1])
        id2name[id]=name
        name2id[name]=id
    f.close()

    f=open("doc.embeddings","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        doc_id=name2id[str(l[0])]
        embeddings=[float(l[i]) for i in range(1,len(l))]
        doc_id2embd[doc_id]=embeddings
    f.close()

    f = open("label.embeddings", "r")
    while True:
        l = f.readline()
        if not l:
            break
        l = l.strip("\n")
        l = l.strip(" ")
        l = l.split(" ")
        label_id = name2id[str(l[0])]
        embeddings = [float(l[i]) for i in range(1, len(l))]
        label_id2embd[label_id] = embeddings
    f.close()

    f = open("user.embeddings", "r")
    while True:
        l = f.readline()
        if not l:
            break
        l = l.strip("\n")
        l = l.strip(" ")
        l = l.split(" ")
        user_id = name2id[str(l[0])]
        embeddings = [float(l[i]) for i in range(1, len(l))]
        user_id2embd[user_id] = embeddings
    f.close()

    f=open("labels","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split("\t")
        doc=l[0]
        label=int(l[1])
        if label==-1:
            label1=2
        elif label==0:
            label1=1
        elif label==1:
            label1=0
        doc2label[doc]=label1

    docs = set([i for i in range(doc_start, doc_end + 1)])
    users = set([i for i in range(node_start, node_end + 1)])
    labels = set([i for i in range(label_start, label_end + 1)])
    followees = set([i for i in range(followee_start, followee_end + 1)])
    f = open("mygraph.pos.adjlist.supervised", "r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        node=int(l[0])
        adj=set([int(l[i]) for i in range(1,len(l))])
        if node>=doc_start and node<=doc_end:
            adj_users=adj & users
            for user in adj_users:
                user_adj=node2adj[user]
                user_adj=user_adj | set([node])
                node2adj[user]=user_adj
        if node>=followee_start and node<=followee_end:
            followee2label[node]=int(l[1])
        node2adj[node]=adj

    #print len(doc_id2embd),len(label_id2embd)

def WriteFiles(doc_count):
    docs=["3","4"]
    left_users=["5"]
    center_users=["6"]
    right_users=["7"]
    labels=["0","1","2"]

    f=open("labels.txt","w")
    for label_id in labels:
        f.write(str(label_id)+"\n")
    f.close()

    f = open("docs.txt", "w")
    for doc_id in docs:
        f.write(str(doc_id) + "\n")
    f.close()

    f=open("center_users.txt","w")
    for user in center_users:
        f.write(str(user)+"\n")
    f.close()

    f = open("left_users.txt", "w")
    for user in left_users:
        f.write(str(user) + "\n")
    f.close()

    f = open("right_users.txt", "w")
    for user in right_users:
        f.write(str(user) + "\n")
    f.close()

    f=open("instances.txt","w")
    f.write(str(0))
    f.close()


    '''
    f = open("doc_in_instance.txt", "w")
    for doc in docs_written:
        f.write(str(doc) + "   " + "0" + "\n")
    f.close()

    f = open("left_user_in_instance.txt", "w")
    for user in left_users:
        f.write(str(user) + "   " + "0" + "\n")
    f.close()

    f = open("center_user_in_instance.txt", "w")
    for user in center_users:
        f.write(str(user) + "   " + "0" + "\n")
    f.close()

    f = open("right_user_in_instance.txt", "w")
    for user in right_users:
        f.write(str(user) + "   " + "0" + "\n")
    f.close()
    '''

    f = open("doc_has_label.txt", "w")
    for doc in docs:
        f.write(str(doc) + " " + "1" + "\n")
    f.close()

    f=open("right_user_has_label.txt","w")
    for user in right_users:
        f.write(str(user)+" "+"0\n")
    f.close()
    f = open("center_user_has_label.txt", "w")
    for user in center_users:
        f.write(str(user)+" "+"1\n")
    f.close()
    f = open("left_user_has_label.txt", "w")
    for user in left_users:
        f.write(str(user)+" "+"2\n")
    f.close()

    f=open("luser_doc_has_same_label.txt","w")
    f.close()
    f = open("cuser_doc_has_same_label.txt", "w")
    f.close()
    f = open("ruser_doc_has_same_label.txt", "w")
    f.close()

    f=open("ruser_doc_pairs.txt","w")
    for user in right_users:
        f.write(str(user)+" "+str(docs[0])+"\n")
    f.close()

    f = open("cuser_doc_pairs.txt", "w")
    for user in center_users:
        f.write(str(user)+" "+str(docs[0])+"\n")
    f.close()

    f = open("luser_doc_pairs.txt", "w")
    for user in left_users:
        f.write(str(user)+" "+str(docs[0])+"\n")
    f.close()

    f=open("elements.txt","w")
    f1=open("in_instance.txt","w")
    for doc in docs:
        f.write(str(doc)+"\n")
        f1.write(str(doc)+"   0\n")
    for user in center_users:
        f.write(str(user)+"\n")
        f1.write(str(user) + "   0\n")
    for user in left_users:
        f.write(str(user)+"\n")
        f1.write(str(user) + "   0\n")
    for user in right_users:
        f.write(str(user)+"\n")
        f1.write(str(user) + "   0\n")
    f.close()
    f1.close()
    '''
    f = open("has_same_label.txt", "w")
    f.close()


    f = open("in_instance.txt", "w")
    for doc in docs_written:
        f.write(str(doc)+"   "+"0"+"\n")
    f.close()


    f=open("relevant_pairs.txt","w")
    docs_written=set(docs_written)
    for doc1 in docs_written:
        adj_all1=node2adj[doc1]
        adj_users1=adj_all1 & users
        adj_docs1=adj_all1 & docs_written
        adj_docs1=adj_docs1 & docs
        for doc2 in adj_docs1:
            adj_all2=node2adj[doc2]
            adj_users2=adj_all2 & users
            if len(adj_users1 & adj_users2)>=10:
                f.write(str(doc1)+" "+str(doc2)+"\n")
    f.close()
    '''

def WriteJson(doc_count):
    data={}
    data["3"]=[0.1,0.5,0.6]
    data["4"]=[0.5,0.9,0.6]
    data["5"]=[0.2,0.3,0.4]
    data["6"]=[0.2,0.6,0.4]
    data["7"]=[0.3,0.1,0.1]

    with open('toy_sim.json', 'w') as f:
        json.dump(data, f)
    return

    data={}
    for label_id in label_id2embd:
        data[label_id]=label_id2embd[label_id]
    #count=0
    for doc_id in doc_id2embd:
        data[doc_id]=doc_id2embd[doc_id]
        #count+=1
        #if count==doc_count:
        #    break
    for user_id in user_id2embd:
        data[user_id]=user_id2embd[user_id]

    with open('embeddings.json', 'w') as f:
        json.dump(data, f)

    '''
    How to read
    '''
    '''
    with open('embeddings.json', 'r') as f:
        data = json.load(f)

    print (data['0'])
    print (data['1'])
    print (data['2'])
    print (data['1742'])
    print (data['1743'])
    print (data['1744'])
    print (data['1745'])
    print (data['1746'])
    print (data['1747'])
    print (data['1748'])
    print (data['1749'])
    print (data['1750'])
    print (data['1751'])
    #print (data['1752'])
    '''

def Analysis():
    G1=nx.Graph()
    f=open("doc2doc.pos.adjlist.additional","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        n1=int(l[0])
        n2_set=[int(l[i]) for i in range(1,len(l))]
        for n2 in n2_set:
            G1.add_edge(n1,n2)
    f.close()
    print "Total nodes: %d" % len(G1.nodes())
    print "Total edges: %d" % len(G1.edges())
    print "No of connected Components: %d" % nx.number_connected_components(G1)

    G=nx.Graph()
    print G
    f=open("relevant_pairs.txt","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        n1=int(l[0])
        n2=int(l[1])
        G.add_edge(n1,n2)
    total_nodes=len(G.nodes())
    print "Total nodes: %d"%len(G.nodes())
    print "Total edges: %d"%len(G.edges())
    print "No of connected Components: %d"%nx.number_connected_components(G)
    '''
    graphs = list(nx.connected_component_subgraphs(G))
    print len(graphs)
    for g in graphs:
        print "Total nodes: %d" % len(g.nodes())
        print "Total edges: %d" % len(g.edges())
        print "No of connected Components: %d" % nx.number_connected_components(g)
    return
    '''
    '''
    cliques=[]
    while True:
        clique=apxa.max_clique(G)
        #print clique
        if len(clique)==0:
            break
        cliques.append(clique)
        G.remove_nodes_from(clique)
        print "Remaining nodes: %d" % len(G.nodes())
        print "Remaining edges: %d" % len(G.edges())

    '''

    cliques = []
    lst = list(nx.find_cliques(G1))
    while len(lst) > 0:
        lst.sort(key=len, reverse=True)
        #print lst
        cliques.append(lst[0])
        #print lst[0],len(lst[0])
        [G1.remove_node(nd) for nd in lst[0]]
        lst = list(nx.find_cliques(G1))

    '''
    k_components = apxa.k_components(G,0.95)
    for key in k_components:
        print key
        print k_components[key]
    '''
    '''
    all_nodes=[]
    for clique in cliques:
        all_nodes=all_nodes+clique
    print len(all_nodes)
    print len(set(all_nodes))
    '''
    #cliques=list(nx.enumerate_all_cliques(G))
    print "No of cliques: %d" % len(cliques)
    #print "max_clique: ",apxa.max_clique(G)
    #print "max_clique: ", apxa.clique_removal(G)
    #k_components=apxa.k_components(G,1)
    #print k_components
    #connected_components=sorted(nx.connected_components(G), key = len, reverse=True)
    #print "connected_components:\n"
    f=open("cliques.txt","w")
    for clique in cliques:
        #print clique
        for doc in clique:
            #print doc, doc2label[id2name[doc]]
            f.write(str(doc)+" "+str(doc2label[id2name[doc]])+"\n")
        #print "\n"
        f.write("#\n")
    f.close()

    f = open("relevant_pairs.txt", "w")
    singleton_cliques=0
    for clique in cliques:
        if len(clique)<2:
            singleton_cliques+=1
            continue
        for doc1 in clique:
            for doc2 in clique:
                if doc1!=doc2:
                    f.write(str(doc1)+" "+str(doc2)+"\n")
    f.close()
    print "No of singleton cliques: %d"%(singleton_cliques)
    '''
    f=open("cliques.txt","a+")
    f.write("No of total nodes: "+str(total_nodes)+"\n")
    f.write("No of cliques: "+str(len(cliques))+"\n")
    f.write("No of singleton cliques: "+str(singleton_cliques)+"\n")
    f.close()
    '''
def square_rooted(x):
    return round(sqrt(sum([a * a for a in x])), 3)

def cosine_similarity(x, y):
    numerator = sum(a * b for a, b in zip(x, y))
    denominator = square_rooted(x) * square_rooted(y)
    return round(numerator / float(denominator), 3)

def sigmoid(x):
    return 1/(1 + math.exp(-x))

def Analysis2():
    global user2label,user2sim_scores,left_followee,right_followee,center_followee

    '''
    Labbeling users based on their similarity scores with the labels.
    '''
    with open("embeddings.json") as f:
        embeddings_dict = json.load(f)
    all_labels=[]
    for user in users:
        label_sim=[]
        user_embd=embeddings_dict[str(user)]
        for label in labels:
            #print label
            label_embd=embeddings_dict[str(label)]
            sim=sigmoid(cosine_similarity(list(user_embd),list(label_embd)))
            label_sim.append(sim)
        user2sim_scores[user]=label_sim
        user_predicted_label=np.argmax(np.array(label_sim))
        user2label[user]=user_predicted_label
        all_labels.append(user_predicted_label)
        #print "user: %d label: %d"%(user,user_predicted_label)
    #print all_labels.count(0),all_labels.count(1),all_labels.count(2)

    '''
    Evaluation #1: based on average user similarity with labels
    '''
    predicted_labels_1={}
    correct=0
    total=0
    total_left = 0
    total_center = 0
    total_right = 0
    left_correct = 0
    right_correct = 0
    center_correct = 0
    y_true=[]
    y_pred=[]
    data={}
    for doc in docs:
        adj=set(node2adj[doc]) & users
        sharing_users_sim_sum=[0,0,0]
        for user in adj:
            sharing_users_sim=user2sim_scores[user]
            #print sharing_users_sim
            sharing_users_sim_sum=[sum(n) for n in zip(*[sharing_users_sim_sum,sharing_users_sim])]
        sharing_users_sim_sum=np.array(sharing_users_sim_sum)/float(len(adj))
        doc_predicted_label=np.argmax(sharing_users_sim_sum)
        data[doc]=sharing_users_sim_sum.tolist()
        #print sharing_users_sim_sum,doc_predicted_label,doc2label[id2name[doc]]
        predicted_labels_1[doc]=doc_predicted_label
        y_true.append(doc2label[id2name[doc]])
        y_pred.append(doc_predicted_label)
        if doc_predicted_label == doc2label[id2name[doc]]:
            if doc_predicted_label == 0:
                right_correct += 1
            elif doc_predicted_label == 1:
                center_correct += 1
            elif doc_predicted_label == 2:
                left_correct += 1
            correct += 1
        if doc2label[id2name[doc]] == 0:
            total_right += 1
        elif doc2label[id2name[doc]] == 1:
            total_center += 1
        elif doc2label[id2name[doc]] == 2:
            total_left += 1
        total += 1
    print "\nExperiment 1:"
    print correct, total, correct/float(total)
    print total_right, right_correct, right_correct/float(total_right)
    print total_center, center_correct, center_correct/float(total_center)
    print total_left, left_correct, left_correct/float(total_left)
    Print_F1_Score(y_true,y_pred)

    '''
    Evaluation #1.1: Based on similarity between documents and labels
    '''
    predicted_labels_2 = {}
    correct = 0
    total = 0
    total_left = 0
    total_center = 0
    total_right = 0
    left_correct = 0
    right_correct = 0
    center_correct = 0
    y_true = []
    y_pred = []
    data1={}
    for doc in docs:
        doc_embd=embeddings_dict[str(doc)]
        label_sim=[]
        for label in labels:
            #print label
            label_embd=embeddings_dict[str(label)]
            sim=sigmoid(cosine_similarity(list(doc_embd),list(label_embd)))
            label_sim.append(sim)
        data1[doc]=label_sim
        doc_predicted_label=np.argmax(np.array(label_sim))
        # print sharing_users_sim_sum,doc_predicted_label,doc2label[id2name[doc]]
        predicted_labels_2[doc] = doc_predicted_label
        y_true.append(doc2label[id2name[doc]])
        y_pred.append(doc_predicted_label)
        if doc_predicted_label == doc2label[id2name[doc]]:
            if doc_predicted_label == 0:
                right_correct += 1
            elif doc_predicted_label == 1:
                center_correct += 1
            elif doc_predicted_label == 2:
                left_correct += 1
            correct += 1
        if doc2label[id2name[doc]] == 0:
            total_right += 1
        elif doc2label[id2name[doc]] == 1:
            total_center += 1
        elif doc2label[id2name[doc]] == 2:
            total_left += 1
        total += 1
    print "\nExperiment 1.1:"
    print correct, total, correct / float(total)
    print total_right, right_correct, right_correct / float(total_right)
    print total_center, center_correct, center_correct / float(total_center)
    print total_left, left_correct, left_correct / float(total_left)
    Print_F1_Score(y_true, y_pred)


    '''
    Evaluation #2: based on % bias of followees. Followee bias will propagate to users. % of followees followed is bias score of an user.
    '''
    for followee in followees:
        if followee2label[followee]==0:
            right_followee.add(followee)
        elif followee2label[followee]==1:
            center_followee.add(followee)
        else:
            left_followee.add(followee)

    global right_users
    global center_users
    global left_users
    threshold1=0.7
    threshold2=0.4
    threshold3=0.02
    sub_users2score={}
    for user in users:
        adj_followees=node2adj[user] & followees
        right_followee_rate = len(adj_followees & right_followee)/float(len(right_followee))
        center_followee_rate = len(adj_followees & center_followee)/float(len(center_followee))
        left_followee_rate = len(adj_followees & left_followee)/float(len(left_followee))


        ''''
        if (right_followee_rate-center_followee_rate)>threshold2 and (right_followee_rate-left_followee_rate)>threshold2 and right_followee_rate>.9:
            right_users.add(user)


        if (center_followee_rate-right_followee_rate)>threshold2 and (center_followee_rate-left_followee_rate)>threshold3 and center_followee_rate>.9:
            center_users.add(user)


        if (left_followee_rate+center_followee_rate)/float(2)>right_followee_rate and left_followee_rate>center_followee_rate and (left_followee_rate-right_followee_rate)>.7:
            left_users.add(user)
        '''
        '''
        if center_followee_rate>.5 and left_followee_rate>.4 and center_followee_rate>left_followee_rate and right_followee_rate<.2:
            center_users.add(user)
        if right_followee_rate>.5 and left_followee_rate==0 and center_followee_rate==0:
            right_users.add(user)
        if left_followee_rate>.5 and center_followee_rate>.4 and left_followee_rate>center_followee_rate and right_followee_rate<.2:
            left_users.add(user)
        '''
        '''
        if center_followee_rate >= left_followee_rate and center_followee_rate>right_followee_rate:
            #print "old: ",center_followee_rate
            #center_followee_rate += .2
            if center_followee_rate>1:
                #print center_followee_rate
                center_followee_rate=1

            #print "1",center_followee_rate, sigmoid(center_followee_rate)
            center_users.add(user)
        if right_followee_rate>=.7 and left_followee_rate==0 and center_followee_rate==0:
            #right_followee_rate+=.2
            if right_followee_rate>1:
                right_followee_rate=1
            right_users.add(user)
        if left_followee_rate>=.4 and right_followee_rate==0 and center_followee_rate<0.4:
            #left_followee_rate+=.2
            if left_followee_rate>1:
                #print center_followee_rate
                left_followee_rate=1
            left_users.add(user)
        #elif center_followee_rate > right_followee_rate and (left_followee_rate-center_followee_rate)<=.3:
            #center_followee_rate+=.4
            #print "2", center_followee_rate, sigmoid(center_followee_rate)
            #center_users.add(user)
        '''

        if center_followee_rate>=0 and center_followee_rate>right_followee_rate and center_followee_rate>left_followee_rate:
            right_followee_rate=right_followee_rate/center_followee_rate
            left_followee_rate=left_followee_rate/center_followee_rate
            center_followee_rate = 1.0
            center_users.add(user)
        if left_followee_rate>=.4 and left_followee_rate>right_followee_rate and left_followee_rate>center_followee_rate:
            right_followee_rate=right_followee_rate/left_followee_rate
            center_followee_rate=center_followee_rate/left_followee_rate
            left_followee_rate = 1.0
            left_users.add(user)
        if right_followee_rate>=.7 and right_followee_rate>center_followee_rate and right_followee_rate>left_followee_rate:
            center_followee_rate=center_followee_rate/right_followee_rate
            left_followee_rate=left_followee_rate/right_followee_rate
            right_followee_rate = 1.0
            right_users.add(user)
        '''
        if left_followee_rate>.7:
            left_users.add(user)
        if right_followee_rate>.7:
            right_users.add(user)
        '''
        '''
        if left_followee_rate > right_followee_rate and left_followee_rate > center_followee_rate:
            left_users.add(user)
        '''
        '''
        if right_followee_rate > left_followee_rate and right_followee_rate > center_followee_rate:
            right_users.add(user)
        '''
        #elif center_followee_rate >right_followee_rate and center_followee_rate<=left_followee_rate:
            #temp=center_followee_rate
            #center_followee_rate=(left_followee_rate+center_followee_rate)/2
            #left_followee_rate=temp
            #center_users.add(user)
        '''
        if right_followee_rate > left_followee_rate and right_followee_rate>center_followee_rate and right_followee_rate>.95:
            right_users.add(user)
        '''
        '''
        if left_followee_rate > right_followee_rate and left_followee_rate>center_followee_rate and left_followee_rate>.9:
            left_users.add(user)
        '''
        sub_users2score[user] = [sigmoid(right_followee_rate), sigmoid(center_followee_rate),sigmoid(left_followee_rate)]
        #sub_users2score[user] = [right_followee_rate, center_followee_rate,left_followee_rate]
    print "center users: \n",center_users, "\ncount: %d"%(len(center_users))
    print "right users: \n",right_users, "\ncount: %d"%(len(right_users))
    print "left users: \n",left_users, "\ncount: %d\n\n"%(len(left_users))

    '''
    Create user2label adjacent list.
    '''
    '''
    Create positive and negative adj list
    '''
    user2label_pos_adj={}
    user2label_neg_adj={}
    for user in right_users:
        user2label_pos_adj[user]=set([str(0)])
        user2label_neg_adj[user]=set([str(1),str(2)])
    for user in center_users:
        user2label_pos_adj[user]=set([str(1)])
        user2label_neg_adj[user]=set([str(0),str(2)])
    for user in left_users:
        user2label_pos_adj[user]=set([str(2)])
        user2label_neg_adj[user]=set([str(0),str(1)])

    '''
    Write in file
    '''
    f1 = open("user2label.pos.adjlist.additional", "w")
    f2 = open("user2label.neg.adjlist.additional", "w")
    for user in user2label_pos_adj:
        f1.write(str(user))
        f2.write(str(user))
        pos_adj=user2label_pos_adj[user]
        neg_adj=user2label_neg_adj[user]
        for label in pos_adj:
            f1.write(" "+str(label))
        for label in neg_adj:
            f2.write(" "+str(label))
        f1.write("\n")
        f2.write("\n")
    f1.close()
    f2.close()
    '''
    Done
    '''

    right_documents=set()
    center_documents=set()
    left_documents=set()
    for user in right_users:
        #print "1"
        docs_shared=node2adj[user] & docs
        #print len(docs_shared)
        right_documents=right_documents | docs_shared
    for user in center_users:
        docs_shared=node2adj[user] & docs
        center_documents=center_documents | docs_shared
    for user in left_users:
        docs_shared=node2adj[user] & docs
        left_documents=left_documents | docs_shared
    print "# of right_doc: %d, center_doc: %d, left_doc: %d"%(len(right_documents),len(center_documents),len(left_documents))
    print "# of (right & center): %d, (center & left): %d, (left & right): %d, (right & center & left): %d\n\n"%(len(right_documents & center_documents),len(center_documents & left_documents),len(right_documents & left_documents), len(right_documents & left_documents & center_documents))

    center_document_labels = []
    left_document_labels = []
    right_document_labels = []

    for doc in center_documents:
        center_document_labels.append(doc2label[id2name[doc]])
    for doc in right_documents:
        right_document_labels.append(doc2label[id2name[doc]])
    for doc in left_documents:
        left_document_labels.append(doc2label[id2name[doc]])
    #print center_document_labels
    #print right_document_labels
    #print left_document_labels

    print "center: right: %d, center: %d, left: %d"%(center_document_labels.count(0),center_document_labels.count(1),center_document_labels.count(2))
    print "right: right: %d, center: %d, left: %d"%(right_document_labels.count(0), right_document_labels.count(1), right_document_labels.count(2))
    print "left: right: %d, center: %d, left: %d\n\n"%(left_document_labels.count(0), left_document_labels.count(1), left_document_labels.count(2))

    sub_documents_set=right_documents | center_documents | left_documents
    sub_users_set=right_users | center_users | left_users

    correct = 0
    total = 0
    total_left = 0
    total_center = 0
    total_right = 0
    left_correct = 0
    right_correct = 0
    center_correct = 0
    for doc in sub_documents_set:
        adj = set(node2adj[doc]) & sub_users_set
        #print "\n" + str(doc) + " " + str(doc2label[id2name[doc]]) + " " + str(len(adj))
        best_score = -10
        best_label = -1
        for user in adj:
            #label_scores = sub_users2score[user]
            label_scores=user2sim_scores[user]
            #sim_scores = data[doc]
            sim_scores=data1[doc]
            if user in center_users:
                #th1=.6
                th1=.55
                th2=1-th1
                th2=0.5
            elif user in right_users:
                #th1=.9
                th1=.5
                th2=1-th1
            elif user in left_users:
                #th1 = .8
                th1=.5
                th2 = 1 - th1
            label_scores=[n*th2 for n in label_scores]
            sim_scores=[n*th1 for n in sim_scores]
            combined_scores=[sum(n) for n in zip(*[label_scores,sim_scores])]
            for i in range(0, 3):
                if combined_scores[i] > best_score:
                    best_label = i
                    best_score = combined_scores[i]
                    #if doc2label[id2name[doc]] == 1:
                    #print best_label, best_score, user
        doc_predicted_label = best_label
        predicted_labels_2[doc]=doc_predicted_label
        # print label_counts, doc_predicted_label, doc2label[id2name[doc]]
        # print sharing_users_sim_sum,doc_predicted_label,doc2label[id2name[doc]]
        if doc_predicted_label == doc2label[id2name[doc]]:
            if doc_predicted_label == 0:
                right_correct += 1
            elif doc_predicted_label == 1:
                center_correct += 1
            elif doc_predicted_label == 2:
                left_correct += 1
            correct += 1
        if doc2label[id2name[doc]] == 0:
            total_right += 1
        elif doc2label[id2name[doc]] == 1:
            total_center += 1
        elif doc2label[id2name[doc]] == 2:
            total_left += 1
        total += 1

    base_correct_count=0
    for doc in sub_documents_set:
        if predicted_labels_2[doc]==doc2label[id2name[doc]]:
            base_correct_count+=1
    print "result: correct: %d, base_correct: %d, total: %d "%(correct,base_correct_count,total)
    print "gain: %d"%(correct-base_correct_count)
    print "right: correct: %d, total: %d "%(right_correct,total_right)
    print "center: correct: %d, total: %d "%(center_correct,total_center)
    print "left: correct: %d, total: %d \n\n"%(left_correct,total_left)

    correct = 0
    total = 0
    total_left = 0
    total_center = 0
    total_right = 0
    left_correct = 0
    right_correct = 0
    center_correct = 0
    y_true=[]
    y_pred=[]
    for doc in docs:
        doc_predicted_label=predicted_labels_2[doc]
        y_true.append(doc2label[id2name[doc]])
        y_pred.append(doc_predicted_label)
        if doc_predicted_label == doc2label[id2name[doc]]:
            if doc_predicted_label == 0:
                right_correct += 1
            elif doc_predicted_label == 1:
                center_correct += 1
            elif doc_predicted_label == 2:
                left_correct += 1
            correct += 1
        if doc2label[id2name[doc]] == 0:
            total_right += 1
        elif doc2label[id2name[doc]] == 1:
            total_center += 1
        elif doc2label[id2name[doc]] == 2:
            total_left += 1
        total += 1
    print correct, total, correct / float(total)
    print total_right, right_correct, right_correct / float(total_right)
    print total_center, center_correct, center_correct / float(total_center)
    print total_left, left_correct, left_correct / float(total_left)
    Print_F1_Score(y_true, y_pred)
    return
    '''
    Creating adjacency list of users for objective function:
    significant users with the same label, who have over a threshold number of share political users, should be similar.
    '''
    '''
    Create positive adj list
    '''

    left_users_group=set()
    right_users_group=set()
    center_users_group=set()
    user2user_adj_pos={}
    for user1 in center_users:
        adj_followees1 = node2adj[user1] & center_followee
        for user2 in center_users-set([user1]):
            adj_followees2 = node2adj[user2] & center_followee
            if len(adj_followees1 & adj_followees2)>20:
                center_users_group.add(user1)
                center_users_group.add(user2)
                if user1 in user2user_adj_pos:
                    adj=user2user_adj_pos[user1]
                    adj.add(user2)
                    user2user_adj_pos[user1]=adj
                else:
                    user2user_adj_pos[user1]=set([user2])
                if user2 in user2user_adj_pos:
                    adj=user2user_adj_pos[user2]
                    adj.add(user1)
                    user2user_adj_pos[user2]=adj
                else:
                    user2user_adj_pos[user2]=set([user1])
    for user1 in left_users:
        adj_followees1 = node2adj[user1] & left_followee
        for user2 in left_users-set([user1]):
            adj_followees2 = node2adj[user2] & left_followee
            if len(adj_followees1 & adj_followees2)>25:
                left_users_group.add(user1)
                left_users_group.add(user2)
                if user1 in user2user_adj_pos:
                    adj=user2user_adj_pos[user1]
                    adj.add(user2)
                    user2user_adj_pos[user1]=adj
                else:
                    user2user_adj_pos[user1]=set([user2])
                if user2 in user2user_adj_pos:
                    adj=user2user_adj_pos[user2]
                    adj.add(user1)
                    user2user_adj_pos[user2]=adj
                else:
                    user2user_adj_pos[user2]=set([user1])
    for user1 in right_users:
        adj_followees1 = node2adj[user1] & right_followee
        for user2 in right_users-set([user1]):
            adj_followees2 = node2adj[user2] & right_followee
            if len(adj_followees1 & adj_followees2)>25:
                right_users_group.add(user1)
                right_users_group.add(user2)
                if user1 in user2user_adj_pos:
                    adj=user2user_adj_pos[user1]
                    adj.add(user2)
                    user2user_adj_pos[user1]=adj
                else:
                    user2user_adj_pos[user1]=set([user2])
                if user2 in user2user_adj_pos:
                    adj=user2user_adj_pos[user2]
                    adj.add(user1)
                    user2user_adj_pos[user2]=adj
                else:
                    user2user_adj_pos[user2]=set([user1])
    print center_users_group,len(center_users_group)
    print left_users_group,len(left_users_group)
    print right_users_group,len(right_users_group)
    print len(left_followee), len(right_followee),len(center_followee)

    '''
    create negative adj list
    '''
    user2user_adj_neg={}
    for user in center_users_group:
        user2user_adj_neg[user]=right_users_group | left_users_group
    for user in left_users_group:
        user2user_adj_neg[user]=right_users_group | center_users_group
    for user in right_users_group:
        user2user_adj_neg[user]=center_users_group | left_users_group

    '''
    Write in files
    '''
    f=open("user2user.pos.adjlist.additional","w")
    for user in user2user_adj_pos:
        f.write(str(user))
        for adj_user in user2user_adj_pos[user]:
            f.write(" "+str(adj_user))
        f.write("\n")
    f.close()

    f = open("user2user.neg.adjlist.additional", "w")
    for user in user2user_adj_neg:
        f.write(str(user))
        for adj_user in user2user_adj_neg[user]:
            f.write(" " + str(adj_user))
        f.write("\n")
    f.close()

    '''
    Create adjacent list of documents for the objective:
    Documents shared by a threshold number of significant users of the same label should be similar.
    '''

    doc2doc_adj_pos={}
    doc2doc_adj_neg={}
    center_docs=set()
    left_docs=set()
    right_docs=set()
    count=0
    for doc1 in docs:
        adj_center_users1=node2adj[doc1] & center_users
        adj_left_users1=node2adj[doc1] & left_users
        adj_right_users1=node2adj[doc1] & right_users
        #print len(adj_center_users1),len(adj_left_users1),len(adj_right_users1)
        count+=1
        print count
        for doc2 in docs-set([doc1]):
            adj_center_users2 = node2adj[doc2] & center_users
            adj_left_users2 = node2adj[doc2] & left_users
            adj_right_users2 = node2adj[doc2] & right_users

            if len(adj_center_users1 & adj_center_users2)>10 or len(adj_left_users1 & adj_left_users2)>5 or len(adj_right_users1 & adj_right_users2)>5:

                if doc1 in doc2doc_adj_pos:
                    adj=doc2doc_adj_pos[doc1]
                    adj.add(doc2)
                    doc2doc_adj_pos[doc1]=adj
                else:
                    doc2doc_adj_pos[doc1]=set([doc2])
                if doc2 in doc2doc_adj_pos:
                    adj=doc2doc_adj_pos[doc2]
                    adj.add(doc1)
                    doc2doc_adj_pos[doc2]=adj
                else:
                    doc2doc_adj_pos[doc2]=set([doc1])

                if len(adj_center_users1 & adj_center_users2)>10:
                    center_docs.add(doc1)
                    center_docs.add(doc2)
                    print "center",len(adj_center_users1 & adj_center_users2)
                elif len(adj_left_users1 & adj_left_users2)>5:
                    left_docs.add(doc1)
                    left_docs.add(doc2)
                    print "left",len(adj_left_users1 & adj_left_users2)
                else:
                    right_docs.add(doc1)
                    right_docs.add(doc2)
                    print "right",len(adj_right_users1 & adj_right_users2)

    '''
    creating negative adj list
    '''
    for doc in center_docs:
        neg_adj=(left_docs | right_docs)-center_docs
        if doc in right_docs:
            neg_adj = neg_adj - right_docs
        if doc in left_docs:
            neg_adj = neg_adj - left_docs
        doc2doc_adj_neg[doc]=neg_adj
    for doc in left_docs:
        neg_adj = (center_docs | right_docs) - left_docs
        if doc in right_docs:
            neg_adj = neg_adj - right_docs
        if doc in center_docs:
            neg_adj = neg_adj - center_docs
        doc2doc_adj_neg[doc] = neg_adj
    for doc in right_docs:
        neg_adj = (center_docs | left_docs) - right_docs
        if doc in left_docs:
            neg_adj = neg_adj - left_docs
        if doc in center_docs:
            neg_adj = neg_adj - center_docs
        doc2doc_adj_neg[doc] = neg_adj

    print len(left_docs), len(center_docs), len(right_docs), len(center_docs & left_docs), len(left_docs & right_docs), len(right_docs & center_docs), len(right_docs & left_docs & center_docs)

    '''
    Writing in files
    '''
    f = open("doc2doc.pos.adjlist.additional", "w")
    for doc in doc2doc_adj_pos:
        f.write(str(doc))
        for adj_doc in doc2doc_adj_pos[doc]:
            f.write(" " + str(adj_doc))
        f.write("\n")
    f.close()

    f = open("doc2doc.neg.adjlist.additional", "w")
    for doc in doc2doc_adj_neg:
        f.write(str(doc))
        for adj_doc in doc2doc_adj_neg[doc]:
            f.write(" " + str(adj_doc))
        f.write("\n")
    f.close()


    '''
    Evaluation #3: Documents are labeled based on the number of bias of users. Suppose, A document is shared by 3 left, 2 center, 4 right biased users.\n
    Then the label of the document is right. Users are labeled based on their similarity with labels.

    '''
    '''
    correct = 0
    total = 0
    for doc in docs:
        adj = set(node2adj[doc]) & users
        sharing_users_label = []
        for user in adj:
            sharing_users_label.append(user2label[user])
        label_counts=[sharing_users_label.count(0),sharing_users_label.count(1),sharing_users_label.count(2)]
        doc_predicted_label = np.argmax(np.array(label_counts))
        #print label_counts,doc_predicted_label,doc2label[id2name[doc]]
        # print sharing_users_sim_sum,doc_predicted_label,doc2label[id2name[doc]]
        if doc_predicted_label == doc2label[id2name[doc]]:
            correct += 1
        total += 1
    print "result: ",correct, total

    correct = 0
    total = 0
    total_left=0
    total_center=0
    total_right=0
    left_correct=0
    right_correct=0
    center_correct=0
    for doc in docs:
        adj = set(node2adj[doc]) & users
        if doc2label[id2name[doc]]==1:
            print "\n" + str(doc) + " " + str(doc2label[id2name[doc]]) + " " + str(len(adj))
        best_score=-10
        best_label=-1
        for user in adj:
            sim_scores=user2sim_scores[user]
            for i in range(0,3):
                if sim_scores[i]>best_score:
                    best_label=i
                    best_score=sim_scores[i]
                    if doc2label[id2name[doc]] == 1:
                        print best_label,best_score, user
        doc_predicted_label = best_label
        #print label_counts, doc_predicted_label, doc2label[id2name[doc]]
        # print sharing_users_sim_sum,doc_predicted_label,doc2label[id2name[doc]]
        if doc_predicted_label == doc2label[id2name[doc]]:
            if doc_predicted_label==0:
                right_correct+=1
            elif doc_predicted_label==1:
                center_correct+=1
            elif doc_predicted_label==2:
                left_correct+=1
            correct += 1
        if doc2label[id2name[doc]]==0:
            total_right += 1
        elif doc2label[id2name[doc]]==1:
            total_center += 1
        elif doc2label[id2name[doc]]==2:
            total_left += 1
        total += 1
    print correct, total
    print total_right,right_correct
    print total_center,center_correct
    print total_left,left_correct
    '''

    with open('sim.json', 'w') as f:
        json.dump(data, f)

def Print_F1_Score(y_true,y_pred):
    print "Macro F1 score: %lf"%(f1_score(y_true, y_pred, average='macro'))
    print "Micro F1 score: %lf"%(f1_score(y_true, y_pred, average='micro'))

if __name__ == '__main__':
    num_of_docs=int(sys.argv[1])
    #LoadData()
    print "Data Loaded"
    WriteJson(num_of_docs) #same as above
    print "Json written"
    #Analysis()
    #Analysis2()
    print "Analysis done"
    WriteFiles(num_of_docs)  # set the passing parameter to the number of documents want to include in the dataset. Set infinity (a very large number) if want to include all.
    print "Files written"
    #print sigmoid(.9),sigmoid(.8),sigmoid(.7),sigmoid(.6),sigmoid(.5),sigmoid(.4),sigmoid(.3),sigmoid(.2),sigmoid(.1),sigmoid(0)