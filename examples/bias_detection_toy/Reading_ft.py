import json
import numpy as np
import re

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

class Reading_ft(FeatureExtractor):

    def __init__(self, embeddings):
        super(Reading_ft, self).__init__()
        # store the name of the embedding file in memory
        self.embeddings = embeddings
        np.random.seed(17477)

    def build(self):
        # actually load the dictionary in memory to be able to
        # access it during feature extraction

        with open(self.embeddings) as f:
            self.embeddings_dict = json.load(f)

        '''
        with open("examples/bias_detection/sim.json") as f:
            self.embeddings_dict=json.load(f)
        '''
        # we need to define the set of labels that we expect
        self.labels = map(str, range(0, 3))

    '''
    def get_doc1_embedding(self, rule_grd):
        documents = rule_grd.get_body_predicates("InInstance")
        d1 = documents[0]['arguments'][0]
        return self.embeddings_dict[str(d1)]
    '''

    def get_doc1_embedding(self, rule_grd):
        documents = rule_grd.get_body_predicates("InInstance")
        d1 = documents[0]['arguments'][0]
        #print d1
        return self.embeddings_dict[str(d1)]

    def get_doc2_embedding(self, rule_grd):
        documents = rule_grd.get_body_predicates("InInstance")
        d2 = documents[1]['arguments'][0]
        return self.embeddings_dict[str(d2)]

    def get_user_embedding(self, rule_grd):
        user=rule_grd.get_body_predicates("InInstance")
        u=user[0]['arguments'][0]
        return self.embeddings_dict[str(u)]

    def get_label_embeddings(self):
        label_emb = {}
        for label in self.labels:
            #print label
            label_emb[label] = self.embeddings_dict[label]
        return label_emb

    def extract_multiclass_head(self, rule_grd):
        ret = rule_grd.get_head_predicate()
        return ret['arguments'][1]
