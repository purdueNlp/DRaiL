import csv
from random import randrange
import numpy as np
import collections
import pickle
import sys
import re
import json
import string
import itertools
import pandas as pd
from scipy.spatial import distance
from sklearn.metrics import f1_score
import os

def similarity(vector_one, vector_two):
    return 1 - distance.cosine(vector_one, vector_two)

def load_glove(fpath):
    #print("Loading GloVe embeddings")
    words = {}
    with open(fpath, 'r') as fr:
        for line in fr:
            line = line.rstrip("\n")
            sp = line.split(" ")
            emb = [float(sp[i]) for i in range(1, len(sp))]
            assert len(emb) == 300
            words[sp[0]] = np.array(emb, dtype=np.float32)
    return words

def get_embedding(sentence, words):
    embeddings = []
    for w in sentence.split(" "):
        # emb = get_w(words, w)
        emb = words[w.lower()] if w.lower() in words else None
        if emb is not None:
            embeddings.append(emb)
    if len(embeddings) == 0:
        import pdb; pdb.set_trace()
        dim = words[list(words.keys())[0]].shape[0]
        sent_emb = np.random.uniform(low=-1.0/dim, high=1.0/dim, size=dim)
    else:
        word = np.sum(embeddings, axis=0)
        sent_emb = (word / len(embeddings))

    return sent_emb

def get_input_and_label(id_one, id_two, data):
    #import pdb; pdb.set_trace()
    sentence_one_id = int(id_one.split("_")[3][1:])
    sentence_two_id = int(id_two.split("_")[3][1:])

    post_one_id = int(id_one.split("_")[2][1:])
    post_two_id = int(id_two.split("_")[2][1:])

    dataset_one_id = int(id_one.split("_")[0][3:])
    dataset_two_id = int(id_two.split("_")[0][3:])

    discussion_one_id = int(id_one.split("_")[1][1:])
    discussion_two_id = int(id_two.split("_")[1][1:])


    row = data.loc[(data["sentenceId_1"]==sentence_one_id) & (data["sentenceId_2"]==sentence_two_id) & (data["postId_1"] == post_one_id) & (data["postId_2"]==post_two_id) & (data["datasetId_1"]==dataset_one_id) & (data["datasetId_2"]==dataset_two_id) & (data["discussionId_1"]==discussion_one_id) & (data["discussionId_2"]==discussion_two_id)]

    try:
        if row["regression_label"].item() >= 3:
            label = 1
        else:
            label = 0
    except:
        import pdb; pdb.set_trace() 

    sentence_one = row["sentence_1"].item()
    sentence_two = row["sentence_2"].item()

    return sentence_one, sentence_two, label

def train(issue, embedding_file, data_file):
    print("ISSUE: {}".format(issue))
    average_glove_score = 0
    #import pdb;pdb.set_trace()
    data = pd.read_csv(data_file, encoding="ISO-8859-1")
    with open("../../data/afs/folds/fold_pairs_{}.json".format(issue)) as f:
        folds = json.load(f)
    glove_embeddings = load_glove(embedding_file)

    #import pdb;pdb.set_trace()
    similarities = [0.75, 0.8, 0.85, 0.9, 0.95]
    # similarities = []

    #K-Fold Cross Validation

    for fold in folds:
        #import pdb; pdb.set_trace()
        train_folds = folds.copy()
        train_folds.pop(fold)

        #Iterate over train folds and find optimum threshold
        best_score = float('-inf')
        best_threshold = float('-inf')
        for threshold in similarities:
            macro_f1_scores = []
            for k in train_folds:
                curr_fold = train_folds[k]
                actual = []
                predicted = []
                for pair in curr_fold:
                    id_one = pair[0]
                    id_two = pair[1]

                    sentence_one, sentence_two, label = get_input_and_label(id_one, id_two, data)
                    actual.append(label)

                    embedding_one = get_embedding(sentence_one, glove_embeddings)
                    embedding_two = get_embedding(sentence_two, glove_embeddings)

                    sim_score = similarity(embedding_one, embedding_two)

                    if sim_score >= threshold:
                        predicted.append(1)
                    else:
                        predicted.append(0)

                macro_f1 = f1_score(y_true = actual, y_pred = predicted, average = 'macro')
                macro_f1_scores.append(macro_f1)

            avg_macro_f1 = sum(macro_f1_scores) / len(macro_f1_scores)
            if avg_macro_f1 > best_score:
                best_score = avg_macro_f1
                best_threshold = threshold

        test_threshold = best_threshold
        test_actual = []
        test_predicted = []

        for pair in folds[fold]:
            id_one = pair[0]
            id_two = pair[1]

            sentence_one, sentence_two, label = get_input_and_label(id_one, id_two, data)
            test_actual.append(label)

            embedding_one = get_embedding(sentence_one, glove_embeddings)
            embedding_two = get_embedding(sentence_two, glove_embeddings)

            sim_score = similarity(embedding_one, embedding_two)

            if sim_score >= test_threshold:
                test_predicted.append(1)
            else:
                test_predicted.append(0)

        macro_f1 = f1_score(y_true = test_actual, y_pred = test_predicted, average = 'macro')
        print("-------MACRO F1 SCORE FOR FOLD {}: {}---------".format(fold, macro_f1))
        average_glove_score += macro_f1

    average_glove_score /= 10

    print("--------AVERAGE MACRO F1 SCORE FOR GLOVE BASELINE: {}---------".format(average_glove_score))



if __name__ == "__main__":
    issue = sys.argv[1]
    embedding_file = sys.argv[2]
    data_directory = sys.argv[3]
    data_file = data_directory+"ArgPairs_{}.csv".format(issue)

    train(issue, embedding_file, data_file)