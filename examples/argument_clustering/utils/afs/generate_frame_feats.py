import json
from nltk.stem import LancasterStemmer
from nltk.tokenize import word_tokenize

def text2feats(stemmer, frame_lexicon, text):
    text = text.lower()
    tokens = word_tokenize(text)

    sentence_stems = set()
    for token in tokens:
        stem = stemmer.stem(token)
        sentence_stems.add(stem)

    sentence_feats = []
    for frame in frame_lexicon:
        for _token in frame_lexicon[frame]:
            if _token in sentence_stems:
                sentence_feats.append(1.0)
            else:
                sentence_feats.append(0.0)
    return sentence_feats

def extract_afs(stemmer, sentence_text, frame_lexicon, sentence2feat):
    topics = ["DP", "GC", "GM"]
    for topic in topics:
        filename = "data/afs/is_sentence_{}.txt".format(topic)
        with open(filename) as fp:
            for line in fp:
                text = sentence_text[line.strip()]
                sentence2feat[line.strip()] = text2feats(stemmer, frame_lexicon, text)

def extract_tw(stemmer, sentence_text, frame_lexicon, sentence2feat):
    with open("data/afs/is_sentence_tw.txt") as fp:
        for line in fp:
            text = sentence_text[line.strip()]
            sentence2feat[line.strip()] = text2feats(stemmer, frame_lexicon, text)

def main():
    frame_lexicon = json.load(open("data/frame_lexicon.json"))
    for frame in frame_lexicon:
        frame_lexicon[frame] = set(frame_lexicon[frame])

    sentence_text = json.load(open("data/afs/sentence_text.json"))
    tw_text = json.load(open("data/tw_text.json"))
    sentence2feat = {}
    stemmer = LancasterStemmer()

    extract_afs(stemmer, sentence_text, frame_lexicon, sentence2feat)
    extract_tw(stemmer, tw_text, frame_lexicon, sentence2feat)

    with open("data/afs/sentence_frame_inds.json", "w") as fp:
        json.dump(sentence2feat, fp)

if __name__ == "__main__":
    main()
