import os
import csv
from transformers import AutoTokenizer
import json

def text2bert(bert_tokenizer, bert_inputs, text_inputs, sentence_id, sentence, max_len):
    id_sequence = bert_tokenizer(sentence)['input_ids'][1:-1]
    max_len = max(max_len, len(id_sequence))
    bert_inputs[sentence_id] = id_sequence
    text_inputs[sentence_id] = sentence
    return max_len

def extract_afs(bert_tokenizer, bert_inputs, text_inputs, max_len):
    afs_pairs = ["GC", "DP", "GM"]
    for afs in afs_pairs:
        filename = os.path.join("data/afs/walker_16_data/ArgPairs_{}.csv".format(afs))
        with open(filename, encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile)
            for i, row in enumerate(spamreader):
                if i == 0:
                    continue
                else:
                    label, d_id_1, d_id_2, p_id_1, p_id_2, da_id_1, s_id_1, s_id_2, da_id_2, s1, s2 = row
                    sentence_id_1 = "dat{}_d{}_p{}_s{}".format(da_id_1, d_id_1, p_id_1, s_id_1)
                    sentence_id_2 = "dat{}_d{}_p{}_s{}".format(da_id_2, d_id_2, p_id_2, s_id_2)
                    if sentence_id_1 not in bert_inputs:
                        max_len = text2bert(bert_tokenizer, bert_inputs, text_inputs, sentence_id_1, s1, max_len)
                    if sentence_id_2 not in bert_inputs:
                        max_len = text2bert(bert_tokenizer, bert_inputs, text_inputs, sentence_id_2, s2, max_len)
    return max_len

def extract_tw(bert_tokenizer, bert_inputs, text_inputs, max_len):
    with open("data/afs/is_sentence_tw.txt") as fp:
        for line in fp:
            sentence_id = line.strip()
            sentence = text_inputs[sentence_id]
            max_len = text2bert(bert_tokenizer, bert_inputs, text_inputs, sentence_id, sentence, max_len)
    return max_len

def main():
    bert_inputs = {}; text_inputs_afs = {}
    text_inputs_tw = json.load(open("data/tw_text.json"))
    max_len = 0
    bert_tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    max_len = extract_afs(bert_tokenizer, bert_inputs, text_inputs_afs, max_len)
    max_len = extract_tw(bert_tokenizer, bert_inputs, text_inputs_tw, max_len)

    print("max_sent", max_len)
    with open("data/afs/sentence_bert.json", "w") as fp:
        json.dump(bert_inputs, fp)

    with open("data/afs/sentence_text.json", "w") as fp:
        json.dump(text_inputs_afs, fp)

if __name__ == "__main__":
    main()
