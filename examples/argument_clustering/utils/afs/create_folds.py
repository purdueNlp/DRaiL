import sys
import random
import os
import csv
import numpy as np
import json

def main():

    afs_pairs = ["GC", "DP", "GM"]
    for afs in afs_pairs:
        candidate_pairs = []
        filename = os.path.join("data/afs/walker_16_data/ArgPairs_{}.csv".format(afs))
        with open(filename, encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile)
            for i, row in enumerate(spamreader):
                if i == 0:
                    continue
                else:
                    label, d_id_1, d_id_2, p_id_1, p_id_2, da_id_1, s_id_1, s_id_2, da_id_2, s1, s2 = row
                    sentence_id_1 = "dat{}_d{}_p{}_s{}".format(da_id_1, d_id_1, p_id_1, s_id_1)
                    sentence_id_2 = "dat{}_d{}_p{}_s{}".format(da_id_2, d_id_2, p_id_2, s_id_2)

                    candidate_pairs.append((sentence_id_1, sentence_id_2))


        random.shuffle(candidate_pairs)
        candidate_pairs = np.array_split(candidate_pairs, 10)

        print(afs)
        folds = {}; fold_pairs = {}
        for i, lst in enumerate(candidate_pairs):
            print(len(lst))
            folds[i] = []; fold_pairs[i] = []
            for (a, b) in lst:
                folds[i].append(a)
                folds[i].append(b)
                fold_pairs[i].append((a,b))

        with open("data/afs/folds/fold_{}.json".format(afs), "w") as fp:
            json.dump(folds, fp)
        with open("data/afs/folds/fold_pairs_{}.json".format(afs), "w") as fp:
            json.dump(fold_pairs, fp)

if __name__ == "__main__":
    main()
