import csv
import os
import networkx as nx
import spacy
from collections import Counter
import re

def extract_entities(nlp, sentence):
    entities_rep = []
    sent_text = nlp(sentence)
    for ent in sent_text.ents:
        #print(ent.text, ent.label_)
        # Skip these ones
        if ent.label_ not in ['TIME', 'MONEY', 'CARDINAL', 'ORDINAL', 'DATE']:
            entities_rep.append((ent.text, ent.label_))
    return entities_rep

def preprocess(e):
    e = e.encode("ascii", "ignore").decode().lower()
    return e

def main():
    nlp = spacy.load("en_core_web_sm")
    same_cluster = {}; is_sentence = {}; has_entity = {}
    has_topic = {}; candidate_pairs = {}

    # instantiate accums
    same_cluster['all'] = set()
    is_sentence['all'] = set()
    has_entity['all'] = set()
    candidate_pairs['all'] = set()
    has_topic['all'] = set()

    afs_pairs = ["GC", "DP", "GM"]
    for afs in afs_pairs:
        # instantiate sets
        same_cluster[afs] = set()
        is_sentence[afs] = set()
        has_entity[afs] = set()
        candidate_pairs[afs] = set()

        G = nx.Graph()
        node2idx = {}
        filename = os.path.join("data/afs/walker_16_data/ArgPairs_{}.csv".format(afs))
        n_pairs = 0; node_idx = 0
        id2text = {}
        with open(filename, encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile)
            for i, row in enumerate(spamreader):
                if i == 0:
                    continue
                else:
                    label, d_id_1, d_id_2, p_id_1, p_id_2, da_id_1, s_id_1, s_id_2, da_id_2, s1, s2 = row
                    n_pairs += 1
                    sentence_id_1 = "dat{}_d{}_p{}_s{}".format(da_id_1, d_id_1, p_id_1, s_id_1)
                    sentence_id_2 = "dat{}_d{}_p{}_s{}".format(da_id_2, d_id_2, p_id_2, s_id_2)
                    candidate_pairs[afs].add((sentence_id_1, sentence_id_2))
                    candidate_pairs['all'].add((sentence_id_1, sentence_id_2))

                    if sentence_id_1 not in node2idx:
                        node2idx[sentence_id_1] = node_idx; node_idx += 1
                        id2text[node2idx[sentence_id_1]] = s1
                        G.add_node(node2idx[sentence_id_1])
                    if sentence_id_2 not in node2idx:
                        node2idx[sentence_id_2] = node_idx; node_idx += 1
                        id2text[node2idx[sentence_id_2]] = s2
                        G.add_node(node2idx[sentence_id_2])

                    label = float(label)

                    if sentence_id_1 not in is_sentence[afs]:
                        entities = extract_entities(nlp, s1)
                        for e in entities:
                            has_entity[afs].add((sentence_id_1, e[0].replace(' ', '_').encode("ascii", "ignore").decode(), e[1]))
                            has_entity['all'].add((sentence_id_1, e[0].replace(' ', '_').encode("ascii", "ignore").decode(), e[1]))

                    if sentence_id_2 not in is_sentence[afs]:
                        entities = extract_entities(nlp, s2)
                        for e in entities:
                            has_entity[afs].add((sentence_id_2, e[0].replace(' ', '_').encode("ascii", "ignore").decode(), e[1]))
                            has_entity['all'].add((sentence_id_2, e[0].replace(' ', '_').encode("ascii", "ignore").decode(), e[1]))

                    is_sentence[afs].add((sentence_id_1,))
                    is_sentence[afs].add((sentence_id_2,))
                    is_sentence['all'].add((sentence_id_1,))
                    is_sentence['all'].add((sentence_id_2,))
                    has_topic['all'].add((sentence_id_1, afs))
                    has_topic['all'].add((sentence_id_2, afs))

                    if label >= 3:
                        G.add_edge(node2idx[sentence_id_1], node2idx[sentence_id_2])
                        same_cluster[afs].add((sentence_id_1, sentence_id_2))
                        #same_cluster[afs].add((sentence_id_2, sentence_id_1))
                        same_cluster['all'].add((sentence_id_1, sentence_id_2))
                        #same_cluster['all'].add((sentence_id_2, sentence_id_1))

        print(afs)
        connected_comp = [len(c) for c in sorted(nx.connected_components(G), key=len, reverse=True)]
        print(len(connected_comp))
        print(connected_comp)

    # Write the files
    files = {
        'same_cluster': same_cluster,
        'is_sentence': is_sentence,
        'has_entity': has_entity,
        'candidate_pairs': candidate_pairs,
        'has_topic': has_topic
    }

    for dataframe in files:
        print(files[dataframe].keys())
        for afs in files[dataframe]:
            filename = "{}_{}.txt".format(dataframe, afs)
            fp = open("data/afs/{}".format(filename), "w")
            for tup in files[dataframe][afs]:
                fp.write("\t".join(tup))
                fp.write("\n")
            fp.close()

if __name__ == "__main__":
    main()
