import json

with open("../frames/data/frame_subframe_lexicon.json") as fp:
    lexicon = json.load(fp)

frame_lexicon = lexicon[0]

with open("data/frame_lexicon.json", "w") as fp:
    json.dump(frame_lexicon, fp)
