import json
import sys

locations, orgs, pers = [], [], []

with open('country.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            locations.append(' ' + line + ' ')

with open('loc_generalkey.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            locations.append(' ' + line + ' ')

with open('loc_key.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            locations.append(' ' + line + ' ')

with open('province.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            locations.append(' ' + line + ' ')

with open('region.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            locations.append(' ' + line + ' ')

with open('org_base.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            orgs.append(' ' + line + ' ')

with open('org_key.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            orgs.append(' ' + line + ' ')

with open('org_pre.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            orgs.append(' ' + line + ' ')

with open('organization_nouns.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            orgs.append(' ' + line + ' ')

with open('person_female_lower.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            pers.append(' ' + line + ' ')

with open('person_male_lower.lst') as f:
    for line in f.readlines():
        line=line.strip().lower()
        if line != "":
            pers.append(' ' + line + ' ')

with open('../phrase_text.json') as f:
    phrase_text = json.load(f)

gaz_feat = {}
num = len(phrase_text)
for i, pid in enumerate(phrase_text):
    text = phrase_text[pid][0]
    text = ' ' + ' '.join(text.split('/')).lower().encode('utf-8') + ' '

    v=[0, 0, 0] # loc, org, per

    for loc in locations:
        if text.find(loc) > -1:
            v[0] = 1
            break

    for org in orgs:
        if text.find(org) > -1:
            v[1] = 1
            break

    for per in pers:
        if text.find(per) > -1:
            v[2] = 1
            break

    gaz_feat[pid]=v
    sys.stdout.write("\r\t{0}%".format((i * 100) / num))
sys.stdout.write('\n')

with open('../gaz_features.json', 'w') as f:
    json.dump(gaz_feat, f)
