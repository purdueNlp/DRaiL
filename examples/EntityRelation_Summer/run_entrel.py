import sys
import json
import os

from drail.learn.local_learner import LocalLearner

def get_fold_ids(data_file_path, fold):
    filter_ids = []
    curr_path = os.path.join(data_file_path, 'fold{0}.txt'.format(fold))
    with open(curr_path) as f:
        for line in f:
            filter_ids.append(line.strip())
    return filter_ids

def get_splits(data_file_path, test_fold):
    dev_fold = (test_fold + 1) % 5

    filter_train = []
    for train_fold in range(5):
        if train_fold != test_fold and train_fold != dev_fold:
            filter_train += get_fold_ids(data_file_path, train_fold)

    filter_dev = get_fold_ids(data_file_path, dev_fold)
    filter_test = get_fold_ids(data_file_path, test_fold)
    return filter_train, filter_dev, filter_test

def getopts(argv):

    opts = {}
    while argv:
        if argv[0][0]=='-':
            opts[argv[0][1:]] = argv[1]
        argv = argv[1:]
    return opts

def main():

    opts = getopts(sys.argv[3:])

    rulefile = 'rule.dr'
    conffile = 'config.json'
    inference = 0

    if 'rulefile' in opts:
        rulefile = opts['rulefile']

    if 'conffile' in opts:
        conffile = opts['conffile']

    if 'inference' in opts:
        inference = int(opts['inference'])

    print rulefile, conffile

    data_file_path = sys.argv[1]
    folds = ['fold0.txt', 'fold1.txt', 'fold2.txt', 'fold3.txt', 'fold4.txt']
    rule_filename = os.path.join(data_file_path, rulefile)
    config_filename = os.path.join(data_file_path, conffile)
    phrase_file = os.path.join(data_file_path, 'phrase_text.json')
    text_info_file = os.path.join(data_file_path, 'info.json')
    all_pos_file = os.path.join(data_file_path, 'all_pos_tags.txt')
    gaz_file =  os.path.join(data_file_path, 'gaz_features.json')
    sent_file = os.path.join(data_file_path, 'sentence_text.json')
    ngram_file = os.path.join(data_file_path, 'ngrams.json')
    pid2tid_file = os.path.join(data_file_path, 'phrase_token_id.json')
    paths_file = os.path.join(data_file_path, 'path_text.json')
    pos_path_file = os.path.join(data_file_path, 'pos_paths.json')
    path_label_file = os.path.join(data_file_path, 'path_labels.json')
    jobtitles_file = os.path.join(data_file_path, 'gazetters/jobtitle.lst')

    with open(phrase_file) as f:
        phrase_text = json.load(f)

    with open(sent_file) as f:
        sent_text = json.load(f)

    learner=LocalLearner()

    print 'Compiling rules...'
    learner.compile_rules(rule_filename)

    print 'Creating database...'
    db=learner.create_dataset(data_file_path) 

    f1_score = {}

    n_iter=1
    for test_fold in range(n_iter):

        filter_train, filter_dev, filter_test = get_splits(data_file_path, test_fold)

        print 'Building feature extractors...'
        learner.build_feature_extractors(db, filter_train, filter_pred="Sentence",
                                         w2v_bin_filename=sys.argv[2],
                                         phrase_filename=phrase_file, all_pos_filename=all_pos_file,
                                         gaz_filename=gaz_file, sentence_filename=sent_file,
                                         paths_filename=paths_file,
                                         pos_path_filename=pos_path_file,
                                         path_labels_filename=path_label_file)
                                         


        print 'Building learning models...'
        learner.build_models(config_filename)

        print 'Training models...'

        # add weighted_examples to weight class imbalance during training
        errors, instance_map = learner.train(db, filter_train, filter_dev, filter_test,
                      filter_pred="Sentence", limit=120000)

        for id in errors:
            print 'Classification errors in rule', id
            try:
                for i, j in enumerate(errors[id]):
                    if j==0:
                        continue
                    instance_grd = instance_map[id][i]
                    inSentences = instance_grd.get_body_predicates("InSentence")
                    arg1 = inSentences[0]['arguments'][0]
                    arg2 = inSentences[1]['arguments'][0]
                    sentId = inSentences[0]['arguments'][1]
                    
                    if j==1:
                        print '**False negative**'
                    else:
                        print '**False positive**'
                    print phrase_text[str(arg1)][0], ' ' ,phrase_text[str(arg2)][0]
                    print sent_text[str(sentId)]
                    print
            except:
                pass

        if inference: 
            curr_score = learner.predict(db, filter_test, filter_pred="Sentence")

            for classifier in curr_score:
                if classifier in f1_score:
                    f1_score[classifier] += curr_score[classifier]
                else:
                    f1_score[classifier] = curr_score[classifier]
        
        learner.reset()
        # then pass filter_* accordingly to methods in learner

    print 'Average performance over %d folds:'%(n_iter)
    for classifier in f1_score:
        print classifier,' : ', f1_score[classifier]/n_iter

if __name__ == "__main__":
    main()



