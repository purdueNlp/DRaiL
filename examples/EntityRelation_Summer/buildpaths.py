import json

with open('sent_dependencies.json') as f:
    sen_dep = json.load(f)

with open('sent_plain.json') as f:
    sent_text = json.load(f)

paths = {}

for id in sen_dep:

    paths[id] = {}

    num_nodes = 0

    for dep in sen_dep[id]:
        num_nodes = max(num_nodes, max(dep['governor'], dep['dependent']))

    graph = [[] for i in range(num_nodes+1)]

    vertex_label = ['' for i in range(num_nodes+1)]

    for dep in sen_dep[id]:

        u, v = dep['governor'], dep['dependent']

        graph[u].append((v, dep['dep']))
        graph[v].append((u, dep['dep']))

        vertex_label[u], vertex_label[v] = dep['governorGloss'], dep['dependentGloss']

    for i in range(1,num_nodes+1):
        for j in range(i+1,num_nodes+1):

            if i==j:
                continue

            vis = [False for k in range(num_nodes+1)]
            par = [(-1, '') for k in range(num_nodes+1)]

            queue = [i]
            vis[i] = True
            
            reached = False

            while queue != []:

                x = queue.pop(0)

                for y, rel in graph[x]:

                    if vis[y]:
                        continue

                    par[y] = (x, rel)
                    vis[y] = True
                    queue.append(y)

                    if y==j:
                        reached = True
                    
                if reached:
                    break

            if not reached:
                continue

            # Trace the path
            cur, nxt = j, par[j][0]

            path_labels = [par[j][1]]
            path_nodes = []

            while nxt != i:
                path_nodes.append(nxt)
                path_labels.append(par[nxt][1])
                cur, nxt = nxt, par[nxt][0]

            path_labels = path_labels[::-1]
            path_nodes  = path_nodes[::-1]

            path_nodes = [vertex_label[x] for x in path_nodes]

            paths[id][str(i)+'-'+str(j)] = (path_nodes, path_labels)


    #print sent_text[id]
    #for path in paths[id]:
    #    print path, paths[id][path]
    #break


with open('path_info.json', 'w') as f:
    json.dump(paths, f)
