import numpy as np

with open('sentences.txt', 'r') as f:
    sentences = f.read().split('\n')

np.random.seed(123)
np.random.shuffle(sentences)

folds = {}
for i in range(5):
    folds[i]=[]

for i, sent in enumerate(sentences):
    folds[i%5].append(sent)

for f in range(5):
    with open('fold'+str(f)+'.txt', 'w') as fp:
        for sent in folds[f]:
            fp.write(sent+'\n')
