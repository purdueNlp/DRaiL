from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import numpy as np
import json
import re
import os
from operator import add
from collections import OrderedDict, Counter
from sklearn.feature_extraction import DictVectorizer
import time

class EntityRel_fe(FeatureExtractor):
    
    def __init__ (self, instance_grds, w2v_bin_filename=None, phrase_filename=None, all_pos_filename=None, gaz_filename=None, sentence_filename=None, ngrams_filename=None, pid2tid_filename = None, paths_filename = None, pos_path_filename=None, path_labels_filename=None, jobtitles=None):
        super(EntityRel_fe, self).__init__(instance_grds)
        self.w2v_bin_filename = w2v_bin_filename
        self.phrase_filename = phrase_filename
        self.all_pos = all_pos_filename
        self.gaz_filename = gaz_filename
        self.sentence_filename = sentence_filename
        self.ngrams_filename = ngrams_filename
        self.pid2tid_filename = pid2tid_filename
        self.paths_filename = paths_filename
        self.pos_path_filename = pos_path_filename
        self.path_labels_filename = path_labels_filename
        self.jobtitles = jobtitles
        np.random.seed(123)

    def build(self):
        if self.phrase_filename:
            self.phraseId_to_word = json.load(open(self.phrase_filename,'r'))
            suffix_count, prefix_count, word_count = {}, {}, {}

            for pid in self.phraseId_to_word:
                words = self.phraseId_to_word[pid][0].lower().replace('/',' ').split()
                for word in words:

                    if word not in word_count.keys():
                        word_count[word]=1
                    else:
                        word_count[word]+=1

                    if len(word) >= 3:
                        if word[-3:] not in suffix_count.keys():
                            suffix_count[word[-3:]]=1
                        else:
                            suffix_count[word[-3:]]+=1

                        if word[:3] not in prefix_count.keys():
                            prefix_count[word[:3]] = 1
                        else:
                            prefix_count[word[:3]]+=1

            words    = set(word_count.keys())
            for word, count in word_count.iteritems():
                if count < 5:
                    words.remove(word)

            suffixes = set(suffix_count.keys())
            for suffix, count in suffix_count.iteritems():
                if count < 10:
                    suffixes.remove(suffix)

            prefixes = set(prefix_count.keys())
            for prefix, count in prefix_count.iteritems():
                if count < 10:
                    prefixes.remove(prefix)

            _, self.word_vec_size,   self.bow_embeddings    = utils.onehot_dictionary(words)
            _, self.suffix_vec_size, self.suffix_embeddings = utils.onehot_dictionary(suffixes)
            _, self.prefix_vec_size, self.prefix_embeddings = utils.onehot_dictionary(prefixes)

        if self.w2v_bin_filename:
            self.num_word_vectors, self.word_embedding_size, self.word_embedding = utils.embeddings_dictionary(self.w2v_bin_filename)

        self.ent_type_idx = {'Peop':0, 'Loc':1, 'Org':2, 'Other':3, 'O':4}

        if self.all_pos:
            _, _, self.pos_vector = utils.onehot_dictionary(eval(open(self.all_pos,'r').read()))

        if self.gaz_filename:
            with open(self.gaz_filename, 'r') as f:
                self.gaz_feat = json.load(f)

        if self.sentence_filename:
            with open(self.sentence_filename, 'r') as f:
                self.sent_text = json.load(f)

        if self.ngrams_filename:
            with open(self.ngrams_filename, 'r') as f:
                self.ngrams = json.load(f)

        if self.pid2tid_filename :
            with open(self.pid2tid_filename, 'r') as f:
                self.pid2tid = json.load(f)

        if self.paths_filename :
            with open(self.paths_filename, 'r') as f:
                self.paths = json.load(f)

        if self.pos_path_filename:
            with open(self.pos_path_filename, 'r') as f:
                self.pos_path = json.load(f)
                freq_paths = [x[0] for x in Counter(self.pos_path.values()).most_common(2000)]
                _, self.path_vec_size, self.path_embedding = utils.onehot_dictionary(freq_paths)

        if self.path_labels_filename:
            with open(self.path_labels_filename, 'r') as f:
                self.path_label = json.load(f)
                freq_paths = [x[0] for x in Counter(self.path_label.values()).most_common(2000)]
                _, self.path_label_size, self.path_label_embedding = utils.onehot_dictionary(freq_paths)

        if self.jobtitles:
            self.profs = []
            with open(self.jobtitles) as f:
                for title in f.readlines():
                    title = title.strip().lower()
                    self.profs.append(title)
        else:
            self.profs = []
                    

    def extract_w2v(self, rule_grd):

        phrases = rule_grd.get_body_predicates("Phrase") 
        #sentences = rule_grd.get_body_predicates("Sentence")
        #inSentence = rule_grd.get_body_predicates("InSentence")

        rule_vec = []

        #print '\nVectorising Rule grounding: ', rule_grd.body

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                text = self.phraseId_to_word[str(arg)][0].lower()
                #print 'Vectorising %s'%(text)

                phrase = re.split(r'/|-', text)

                vectorised_phrase = []

                for  word in phrase:
                    if word in self.word_embedding:
                        #print 'Found in embeddings'
                        vectorised_phrase.append(self.word_embedding[word])
                    else:
                        #print 'Not found. Random vector assigned.'
                        vectorised_phrase.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                vector.append(np.mean(vectorised_phrase, axis=0))

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)

    def extract_pos(self, instance_grd):
        
        phrases = instance_grd.get_body_predicates("Phrase")
        
        rule_vec = []

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                pos_tag = self.phraseId_to_word[str(arg)][1]
                vector.append(self.pos_vector[pos_tag])

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)
    
    def extract_multiclass_head(self, instance_grd):
        ret = instance_grd.get_head_predicate()
        return self.ent_type_idx[ret['arguments'][1]]
        
    def extract_cap(self, instance_grd):
        
        phrases = instance_grd.get_body_predicates("Phrase")

        rule_vec = []

        for pred_grd in phrases:
            
            vector = []
            for arg in pred_grd['arguments']:
                words = self.phraseId_to_word[str(arg)][0].replace('/', ' ').split()

                if words==[]:
                    vector.append([0]*4)
                    continue

                phr_v = []
                for word in words:
                    v=[0, 0, 0, 0] # all caps, first cap, inside cap, all lower
                    if word.lower().upper()==word:
                        v[0]=1
                    elif word[0].isupper():
                        v[1]=1
                    elif word.lower() != word:
                        v[2]=1
                    else:
                        v[3]=1

                    phr_v.append(v)

                vector.append(np.amax(phr_v, axis=0))
                            
            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)
    
    def get_label(self, class_name):
        return self.ent_type_id[class_name]

    def get_class(self, label):
        for key in self.ent_type_id:
            if self.ent_type_id[key] == label:
                return key

    def extract_noun(self, instance_grd):

        phrases = instance_grd.get_body_predicates("Phrase")

        rule_vec = []

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                pos_tag = self.phraseId_to_word[str(arg)][1]
                if re.match(r'.*NN.*',pos_tag):
                    vector.append(1)
                else:
                    vector.append(0)

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)

    def extract_suffix(self, instance_grd):
        phrases = instance_grd.get_body_predicates("Phrase") 

        rule_vec = []

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                words = self.phraseId_to_word[str(arg)][0].lower().replace('/',' ').split()
                if words==[]:
                    vector.append([0]*self.suffix_vec_size)
                    continue
                v = []
                for word in words:
                    wv = self.suffix_embeddings.get(word[-3:],[0]*self.suffix_vec_size)
                    v.append(wv)

                vector.append(np.amax(v, axis=0))

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)

    def extract_prefix(self, instance_grd):
        phrases = instance_grd.get_body_predicates("Phrase") 

        rule_vec = []

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                words = self.phraseId_to_word[str(arg)][0].lower().replace('/',' ').split()
                #print self.phraseId_to_word[str(arg)][0].lower()
                if words==[]:
                    vector.append([0]*self.prefix_vec_size)
                    continue
               
                v = []
                for word in words:
                    wv = self.prefix_embeddings.get(word[:3],[0]*self.prefix_vec_size)
                    v.append(wv)

                vector.append(np.amax(v, axis=0))

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)

    def extract_gaz(self, instance_grd):
        phrases = instance_grd.get_body_predicates("Phrase")

        rule_vec = []

        for pred_grd in phrases:

            vector = []

            for arg in pred_grd['arguments']: 
                vector.append(self.gaz_feat[str(arg)])

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)
                    

    def extract_bow(self, instance_grd):
        phrases = instance_grd.get_body_predicates("Phrase") 

        rule_vec = []

        for pred_grd in phrases:

            vector = []
            for arg in pred_grd['arguments']:
                words = self.phraseId_to_word[str(arg)][0].lower().replace('/',' ').split()
                if words==[]:
                    vector.append([0]*self.word_vec_size)
                    continue

                v = []
                for word in words:
                    wv = self.bow_embeddings.get(word, [0]*self.word_vec_size)
                    v.append(wv)

                vector.append(np.amax(v, axis=0))

            rule_vec.append(np.hstack(vector))

        return np.hstack(rule_vec)

    def extract_basic_rule_ft(self, instance_grd):
        
        phrases = instance_grd.get_body_predicates("Phrase")
        sentence = instance_grd.get_body_predicates("Sentence")

        sent_id   = sentence[0]['arguments'][0]
        sent_text = self.sent_text[str(sent_id)].lower()

        p1, p2 = phrases

        pid1, pid2 = p1['arguments'][0], p2['arguments'][0]
        p1_text, p1_pos, p1_loc = self.phraseId_to_word[str(pid1)]
        p2_text, p2_pos, p2_loc = self.phraseId_to_word[str(pid2)]
        p1_text, p2_text = p1_text.lower(), p2_text.lower()

        p1_loc, p2_loc = map(int, [p1_loc, p2_loc])
        ################

        vector = []

        word_distance = abs(p1_loc-p2_loc)
        vector.append(word_distance)

        has_same_pos  = p1_loc==p2_loc and 1 or 0
        vector.append(has_same_pos)

        words1 = set(p1_text.replace('/',' ').lower().split())
        words2 = set(p2_text.replace('/',' ').lower().split())

        phrase_sim = len(words1 & words2)
        vector.append(phrase_sim)

        vector.append(p1_loc)
        vector.append(p2_loc)

        vector.append(len(p1_text))
        vector.append(len(p2_text))

        vector.append(len(sent_text))

        vector.append(p1_loc==0 and 1 or 0)
        vector.append(p2_loc==0 and 1 or 0)

        i1 = sent_text.find(p1_text.lower())

        if i1 > -1 and i1+len(p1_text)==len(sent_text):
            vector.append(1)
        else:
            vector.append(0)
        
        i2 = sent_text.find(p2_text.lower())

        if i2 > -1 and i2+len(p2_text)==len(sent_text):
            vector.append(1)
        else:
            vector.append(0)

        vector.append(p1_loc < p2_loc and 1 or 0)
        vector.append(p2_loc < p1_loc and 1 or 0)

        comma1 = sent_text.find(p1_text + " COMMA " + p2_text) > -1 or sent_text.find(p1_text + " , " + p2_text) > -1
        comma2 = sent_text.find(p2_text + " COMMA " + p1_text) > -1 or sent_text.find(p2_text + " , " + p1_text) > -1

        vector.append(comma1)
        vector.append(comma2)

        intermediate_nn_count = 0
        for i in range(min(pid1, pid2), max(pid1, pid2)+1):
            pos = self.phraseId_to_word[str(i)][1]
            if re.match(r'.*NN.*', pos):
                intermediate_nn_count+=1
        vector.append(intermediate_nn_count)

        intermediate_vb_count = 0
        for i in range(min(pid1, pid2), max(pid1, pid2)+1):
            pos = self.phraseId_to_word[str(i)][1]
            if re.match(r'.*VB.*', pos):
                intermediate_vb_count+=1
        vector.append(intermediate_vb_count)

        num_vb_pre_p1 = 0
        for i in range(0, p1_loc):
            #print 'pid1, i, p1_loc:', pid1, i, p1_loc
            pos = self.phraseId_to_word[str(pid1-i-1)][1]
            if re.match(r'.*VB.*', pos):
                num_vb_pre_p1+=1
        vector.append(num_vb_pre_p1)

        num_vb_pre_p2 = 0
        for i in range(0, p2_loc):
            pos = self.phraseId_to_word[str(pid2-i-1)][1]
            if re.match(r'.*VB.*', pos):
                num_vb_pre_p2+=1
        vector.append(num_vb_pre_p2)

        num_phrases = max(p1_loc, p2_loc)
        curr = max(pid1, pid2) + 1
        N = len(self.phraseId_to_word)
        while curr<N and int(self.phraseId_to_word[str(curr)][2]) > num_phrases:
            num_phrases+=1
            curr+=1

        num_phrases+=1

        num_vb_post_p1 = 0
        for i in range(p1_loc+1, num_phrases):
            pos = self.phraseId_to_word[str(pid1+i-p1_loc)][1]
            if re.match(r'.*VB.*', pos):
                num_vb_post_p1+=1
        vector.append(num_vb_post_p1)
            
        num_vb_post_p2 = 0
        for i in range(p2_loc+1, num_phrases):
            pos = self.phraseId_to_word[str(pid2+i-p2_loc)][1]
            if re.match(r'.*VB.*', pos):
                num_vb_post_p2+=1
        vector.append(num_vb_post_p2)

        #print vector

        return vector

    def extract_rel_indicators_li(self, rule_grd):
        inSentences = rule_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]
        sentId = inSentences[0]['arguments'][1]
        preSentence = self.sent_text[str(sentId)].lower().replace('/', ' ')

        phrase1 = ' '.join(self.phraseId_to_word[str(arg1)][0].split('/')).lower()
        phrase2 = ' '.join(self.phraseId_to_word[str(arg2)][0].split('/')).lower()

        vector = np.asarray([])
        indicators = ['live', 'native', 'in', 'at', 'home', 'of', 'president', 'comma']
        #indicators = ['in', 'at']

        p1, p2  = map(re.escape, [phrase1, phrase2])

        
        v1 = re.search(p1+r'\s+of\s+'+p2, preSentence) != None
        v2 = re.search(p1+r'.*\ native\ .*'+p2, preSentence) != None
        v6 = re.search(p1+'.*'+p2+'.*\ native', preSentence) != None
        v7 = re.search('native\ .*'+p2+'.*'+p1, preSentence) != None
        v3 = re.search(p1+'.*'+p2+'.*\ home', preSentence) != None
        v8 = re.search(p1+'.*home.*'+p2, preSentence) != None
        v4 = re.search('president.*'+p1+'.*'+p2, preSentence) != None
        v5 = re.search(p2+r'.*president\s*'+p1, preSentence) != None     
        v9 = re.search(p1+r'\s*\w*\s*'+p2, preSentence) != None
        v10 = re.search('home.*'+p1+'.*'+'p2', preSentence) != None
        v11 = re.search(p1+r'\s*\ comma\ \s*'+p2, preSentence) != None
        
        for indicator in indicators:
            ind = (preSentence.find(indicator) >= 0) * 1
            indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6 = 0, 0, 0, 0, 0, 0
            if (phrase1 != '?' and phrase2 != '?'):
                indOrder1 = (re.search('.*'.join([phrase1, phrase2, indicator]), preSentence) != None) * ind
                indOrder2 = (re.search('.*'.join([phrase2, phrase1, indicator]), preSentence) != None) * ind
                indOrder3 = (re.search('.*'.join([phrase1, indicator, phrase2]), preSentence) != None) * ind
                indOrder4 = (re.search('.*'.join([phrase2, indicator, phrase1]), preSentence) != None) * ind
                indOrder5 = (re.search('.*'.join([indicator, phrase1, phrase2]), preSentence) != None) * ind
                indOrder6 = (re.search('.*'.join([indicator, phrase2, phrase1]), preSentence) != None) * ind
            vector = np.hstack([vector, ind, indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6])
       
        
        #return vector
        return np.hstack([vector,[v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11]])
    
    def extract_rel_indicators_wf(self, rule_grd):
        inSentences = rule_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]
        sentId = inSentences[0]['arguments'][1]
        preSentence = self.sent_text[str(sentId)].lower().replace('/',' ')

        phrase1 = ' '.join(self.phraseId_to_word[str(arg1)][0].split('/')).lower()
        phrase2 = ' '.join(self.phraseId_to_word[str(arg2)][0].split('/')).lower()

        vector = np.asarray([])
        '''
        indicators = '('+'|'.join(map(re.escape, self.profs))+')'
        p1, p2 = map(re.escape, [phrase1, phrase2])
        ordL = ['.*'.join([p1, indicators, p2]), '.*'.join([p1, p2, indicators]), '.*'.join([indicators, p1, p2]),
                '.*'.join([indicators, p2, p1]), '.*'.join([p2, p1, indicators]), '.*'.join([p2, indicators, p1])]

        for pat in ordL:
            vector = np.hstack([vector, re.search(pat, preSentence) != None])
            print vector
        ''' 

        p1, p2 = map(re.escape, [phrase1, phrase2])

        v1 = re.search(p2+r'\s*\w+\s*'+p1, preSentence) != None
        v2 = re.search(p1+r'\s*((comma)?\s*(\w+\s*){0,2}\s*(of|on|for)|(of|on|for))\s*'+p2, preSentence) != None
        v3 = re.search(p1+r'\s*(comma)?\s*who\s*\w+\s*'+p2, preSentence) != None
        v4 = re.search(r'\w+\s*of\s*'+p2+r'\s*comma\s*'+p1, preSentence) != None

        
        indicators = ['works', 'employ', 'at', 'comma', 'stars', 'directs', 'chairman', 'covers']
        for indicator in indicators:
            ind = (preSentence.find(indicator) >= 0) * 1
            indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6 = 0, 0, 0, 0, 0, 0
            if (phrase1 != '?' and phrase2 != '?'):
                indOrder1 = (re.search('.*'.join([phrase1, phrase2, indicator]), preSentence) != None) * ind
                indOrder2 = (re.search('.*'.join([phrase2, phrase1, indicator]), preSentence) != None) * ind
                indOrder3 = (re.search('.*'.join([phrase1, indicator, phrase2]), preSentence) != None) * ind
                indOrder4 = (re.search('.*'.join([phrase2, indicator, phrase1]), preSentence) != None) * ind
                indOrder5 = (re.search('.*'.join([indicator, phrase1, phrase2]), preSentence) != None) * ind
                indOrder6 = (re.search('.*'.join([indicator, phrase2, phrase1]), preSentence) != None) * ind
            vector = np.hstack([vector, ind, indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6])
        
       
        return np.hstack([vector, [v1, v2, v3, v4]])
       
        #return [v1, v2, v3, v4]
        

    def extract_rel_fe(self, instance_grd):
        
        phrases = instance_grd.get_body_predicates("Phrase")
        sentence = instance_grd.get_body_predicates("Sentence")

        sent_id   = sentence[0]['arguments'][0]
        sent_text = self.sent_text[str(sent_id)].lower()

        p1, p2 = phrases

        pid1, pid2 = p1['arguments'][0], p2['arguments'][0]
        p1_text, p1_pos, p1_loc = self.phraseId_to_word[str(pid1)]
        p2_text, p2_pos, p2_loc = self.phraseId_to_word[str(pid2)]

        p1_loc, p2_loc = map(int, [p1_loc, p2_loc])
        ################

        vector = []

        start = time.time()

        #print pid1, pid2

        for i in range( min(pid1, pid2), max(pid1, pid2)+1  ):

            text, pos, _ = self.phraseId_to_word[str(i)]

            if pos.find('VB') > -1:
                words = text.lower().replace('/',' ').split()
                v = []
                for word in words:
                    if word in self.word_embedding:
                        v.append(self.word_embedding[word])
                    else:
                        v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                if v != []:
                    vector.append(np.mean(v, axis=0))


#        print instance_grd
        if vector == []:
            vector.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

        #print time.time()-start

        return np.mean(vector, axis=0)

    def extract_ngrams(self, instance_grd):

        sentence = instance_grd.get_body_predicates("Sentence")

        sent_id = sentence[0]['arguments'][0]
        
        return self.ngrams[str(sent_id)]

    def extract_context(self, instance_grd):

        phrases = instance_grd.get_body_predicates("Phrase")
        sentence = instance_grd.get_body_predicates("Sentence")

        sent_id   = sentence[0]['arguments'][0]
        sent_text = self.sent_text[str(sent_id)].lower()

        p1, p2 = phrases

        pid1, pid2 = p1['arguments'][0], p2['arguments'][0]
        p1_text, p1_pos, p1_loc = self.phraseId_to_word[str(pid1)]
        p2_text, p2_pos, p2_loc = self.phraseId_to_word[str(pid2)]

        p1_loc, p2_loc = map(int, [p1_loc, p2_loc])
        ################

        num_phrases = max(p1_loc, p2_loc)
        curr = max(pid1, pid2) + 1
        N = len(self.phraseId_to_word)
        while curr<N and int(self.phraseId_to_word[str(curr)][2]) > num_phrases:
            num_phrases+=1
            curr+=1

        v1 = [[0]*self.word_embedding_size for i in range(5)]

        for i in range(-2,3):
            if p1_loc+i>=0 and p1_loc+i<=num_phrases:

                text = self.phraseId_to_word[str(pid1+i)][0].lower().replace('/',' ').split()
                v = []
                for word in text:
                    if word in self.word_embedding:
                        v.append(self.word_embedding[word])
                    else:
                        v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                if v==[]:
                    v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                v1[i+2]=np.mean(v, axis=0)

        v2 = [[0]*self.word_embedding_size for i in range(5)]

        for i in range(-2,3):
            if p2_loc+i>=0 and p2_loc+i<=num_phrases:

                text = self.phraseId_to_word[str(pid2+i)][0].lower().replace('/',' ')
                v = []
                for word in text:
                    if word in self.word_embedding:
                        v.append(self.word_embedding[word])
                    else:
                        v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                if v==[]:
                    v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

                v2[i+2]=np.mean(v, axis=0)

        return np.hstack(v1+v2)

    def extract_dep_path(self, instance_grd):

#        print 'It works!'
        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]
        sentId = inSentences[0]['arguments'][1]

        key1, key2 = str(arg1)+'-'+str(arg2), str(arg2)+'-'+str(arg1)

        v = []
        words = []
        if key1 in self.paths:
            words = self.paths[key1]
        elif key2 in self.paths:
            words = self.paths[key2]

        if words != []:
            for word in words:
                word = word.lower()
                if word in self.word_embedding:
                    v.append(self.word_embedding[word])
                else:
                    v.append(np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

            return np.mean(v, axis=0)

        return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)


    def extract_path_pos(self, instance_grd):
        
        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]

        key1 = str(arg1)+'-'+str(arg2)
        key2 = str(arg2)+'-'+str(arg1)

        vector = [0] * (self.path_vec_size + 1)

        if key1 in self.pos_path and self.pos_path[key1] in self.path_embedding:
            vector[:-1] = self.path_embedding[self.pos_path[key1]]

        elif key2 in self.pos_path and self.pos_path[key2] in self.path_embedding:
            vector[:-1] = self.path_embedding[self.pos_path[key2]]

        else:
            vector[-1] = 1

        return vector

    def extract_path_labels(self, instance_grd):

        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]

        key1 = str(arg1)+'-'+str(arg2)
        key2 = str(arg2)+'-'+str(arg1)

        vector = [0] * (self.path_label_size + 1)

        if key1 in self.path_label and self.path_label[key1] in self.path_label_embedding:
            vector[:-1] = self.path_label_embedding[self.path_label[key1]]

        elif key2 in self.path_label and self.path_label[key2] in self.path_label_embedding:
            vector[:-1] = self.path_label_embedding[self.path_label[key2]]

        else:
            vector[-1] = 1

        return vector

    def extract_path_verbs(self, instance_grd):

        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]

        key1 = str(arg1)+'-'+str(arg2)
        key2 = str(arg2)+'-'+str(arg1)

        key = ''
        if key1 in self.paths:
            key=key1
        elif key2 in self.paths:
            key=key2
        else:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        pos_path = self.pos_path[key]
        path_txt = self.paths[key]

        pos_tags = pos_path.replace('<-', '->').split(' -> ')

        v = []

        for word, pos in zip(path_txt, pos_tags):
            word = word.lower()
            if pos.find('VB') > -1 and word in self.word_embedding:
                #print 'found verb', word
                v.append(self.word_embedding[word])

        if v==[]:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        return np.mean(v, axis=0)


    def extract_path_nouns(self, instance_grd):

        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]

        key1 = str(arg1)+'-'+str(arg2)
        key2 = str(arg2)+'-'+str(arg1)

        key = ''
        if key1 in self.paths:
            key=key1
        elif key2 in self.paths:
            key=key2
        else:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        pos_path = self.pos_path[key]
        path_txt = self.paths[key]

        pos_tags = pos_path.replace('<-', '->').split(' -> ')

        v = []

        for word, pos in zip(path_txt, pos_tags):
            word = word.lower()
            if pos.find('NN') > -1 and word in self.word_embedding:
                v.append(self.word_embedding[word])

        if v==[]:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        return np.mean(v, axis=0)

                    
    def extract_path_prepos(self, instance_grd):

        inSentences = instance_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0]['arguments'][0]
        arg2 = inSentences[1]['arguments'][0]

        key1 = str(arg1)+'-'+str(arg2)
        key2 = str(arg2)+'-'+str(arg1)

        key = ''
        if key1 in self.paths:
            key=key1
        elif key2 in self.paths:
            key=key2
        else:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        pos_path = self.pos_path[key]
        path_txt = self.paths[key]

        pos_tags = pos_path.replace('<-', '->').split(' -> ')

        v = []

        for word, pos in zip(path_txt, pos_tags):
            word = word.lower()
            if pos.find('IN') > -1 and word in self.word_embedding:
                v.append(self.word_embedding[word])

        if v==[]:
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        return np.mean(v, axis=0)

    def extract_conv_features(self, instance_grd):

        inSentences = instance_grd.get_body_predicates("InSentence")
        sentId = inSentences[0]['arguments'][1]
        
        words = self.sent_text[str(sentId)].lower().replace('/',' ').split()

        height = 173 # 173 is the length of largest sentence

        mat = np.zeros((height, self.word_embedding_size))

        n  = len(words)
        start = (height - n)//2

        for i in range(n):
            mat[start+i] = self.word_embedding.get(words[i], np.random.uniform(-0.0025, 0.0025, self.word_embedding_size))

        return mat

