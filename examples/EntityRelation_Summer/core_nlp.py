from pycorenlp import StanfordCoreNLP
import json

nlp = StanfordCoreNLP('http://localhost:9000')

obj = json.load(open('sentence_text.json'))

info = {}

for sent_id in obj:
#if True:
    try:
        t=list(obj[sent_id])

        for i,c in enumerate(t):
            if c=='/':
                t[i]=' '

        t=str(''.join(t))

        output = nlp.annotate(t, properties={'annotators':'tokenize,ssplit,pos,depparse,parse','outputFormat':'json'})

        info[int(sent_id)] = output['sentences']

    except Exception as e:
        print e, sent_id, obj[sent_id]

with open('info.json', 'w') as f:
    json.dump(info, f)
