import json
import re
'''
f1 = open('sentences.txt','w')
f2 = open('phrases.txt', 'w')
f3 = open('in_sentence.txt', 'w')
f4 = open('entity.txt')
'''

sentences = set()
sentence_text = {}
sent_plain = {}
phrase_text = {}
#phrase_global_to_local = {}
phrases = []
in_sentence = []
is_entity = []
nouns = []
chunked_sent = {}
relation_set = {}

non_nouns = {}
non_nouns['Peop'], non_nouns['Org'], non_nouns['Loc'], non_nouns['Other'], non_nouns['O'] = [], [], [], [], []

num = {}
for key in non_nouns:
    num[key]=0

with open('conll04.corp','r') as f:
    state=0
    wc = 0
    order={}
    curr_sentence = []
    curr_id = 0

    for line in f.readlines():
        
#        if wc==0:
#            print line, state

        if line == '\n':
            state=1-state
            if state==0:
                del curr_sentence[:]
            else:
                t = ''
                for i, phrase in enumerate(curr_sentence):
                    if t!='':
                        t+=' '
                    if phrase=='/':
                        t+='/'
                    else:
                        t+=' '.join(phrase.split('/'))

                t = t.replace(' COMMA ',' , ' )

                sent_plain[curr_id] = t

                sentence_text[curr_id] = ' '.join(curr_sentence)

                chunked_sent[curr_id] = curr_sentence[:]
            continue

        tokens=line.strip().split()

        if state==0:
            wc += 1
#            if wc==1:
#                print wc, tokens[5]
            sentences.add(int(tokens[0]))
            curr_id = int(tokens[0])
            phrases.append(wc)
            num[tokens[1]]+=1
            if re.match(r'.*NN.*', tokens[4]):
                nouns.append(wc)
            else:
                non_nouns[tokens[1]].append((tokens[5], tokens[4])) 
            phrase_text[wc] = (tokens[5], tokens[4], tokens[2])
            in_sentence.append((wc, int(tokens[0])))
            is_entity.append((wc, tokens[1]))
            order[int(tokens[2])]=wc
            #phrase_global_to_local[wc] = int(tokens[2])
            curr_sentence.append(tokens[5])

        else:
            p1, p2 = order[int(tokens[0])], order[int(tokens[1])]
            if tokens[2] in relation_set.keys():
                relation_set[tokens[2]].append((p1, p2))
            else:
                relation_set[tokens[2]]=[(p1,p2)]



with open('sentences.txt','w') as f:
    for Id in sentences:
        f.write(str(Id)+'\n')

with open('phrases.txt','w') as f:
    for Id in phrases:
        f.write(str(Id)+'\n')

with open('in_sentence.txt', 'w') as f:
    for pair in in_sentence:
        f.write(str(pair[0])+' '+str(pair[1])+'\n')

with open('is_entity.txt', 'w') as f:
    for pair in is_entity:
        f.write(str(pair[0])+' '+str(pair[1])+'\n')

for relation in relation_set.keys():
    with open(relation+'.txt', 'w') as f:
        for pair in relation_set[relation]:
            f.write(str(pair[0])+' '+str(pair[1])+'\n')

with open('sentence_text.json', 'w') as f:
    json.dump(sentence_text, f)

with open('phrase_text.json', 'w') as f:
    json.dump(phrase_text, f)

#with open('#phrase_global_to_local.json', 'w') as f:
#    json.dump(#phrase_global_to_local, f)

with open('nouns.txt', 'w') as f:
    for Id in nouns:
        f.write(str(Id)+'\n')

print 'Non noun stats:\n'
for key in non_nouns:
    print key, len(non_nouns[key]) * 100 / float(num[key])

with open('non_nouns.json', 'w') as f:
    json.dump(non_nouns, f)

with open('sent_plain.json', 'w') as f:
    json.dump(sent_plain, f)

with open('chunked_sent.json', 'w') as f:
    json.dump(chunked_sent, f)
