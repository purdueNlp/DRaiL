import logging

import torch
import numpy as np
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks

class NN_One_Hot_Entity_Group_Pos(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(NN_One_Hot_Entity_Group_Pos, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        
        if self.use_gpu:
            self.input2encoding = torch.nn.Linear(self.config["nn_input_dim"], self.config["nn_hidden_dim"]).cuda()
            self.encoding2label = torch.nn.Linear(self.config["nn_hidden_dim"], self.output_dim).cuda()
        else:
            self.input2encoding = torch.nn.Linear(self.config["nn_input_dim"], self.config["nn_hidden_dim"])
            self.encoding2label = torch.nn.Linear(self.config["nn_hidden_dim"], self.output_dim)

    def forward(self, x):
        input_index = 0
        mf_feature = [elem[input_index] for elem in x['input']]
        mf_feature = self._get_float_tensor(mf_feature)
        mf_feature = self._get_variable(mf_feature)
    
        #print (mf_feature)
        
        encoded_feature = self.input2encoding(mf_feature)
        encoded_feature = F.relu(encoded_feature)
        
        logits = self.encoding2label(encoded_feature)
    
        #print (logits)
        
        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        
        #print (probas)
        
        return logits, probas






