import logging

import torch
import numpy as np
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks

class Rule_One_Net(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(Rule_One_Net, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.minibatch_size = self.config['batch_size']

        self.embedding_layers, emb_dims = self._embedding_inputs(rule_template, fe)
        for key in self.embedding_layers:
            self.embedding_layers[key].weight.requires_grad = False

        # Get shared BLSTM for Tweets
        name = self.config["shared_lstm_tweet"]
        self.n_input_sequence_tweet = shared_params[name]["nin"]
        self.n_hidden_sequence_tweet = shared_params[name]["nout"]
        self.sequence_lstm_tweet = shared_params[name]["lstm"]

        # Get shared BLSTM for Entities
        name = self.config["shared_lstm_entity"]
        self.n_input_sequence_entity = shared_params[name]["nin"]
        self.n_hidden_sequence_entity = shared_params[name]["nout"]
        self.sequence_lstm_entity = shared_params[name]["lstm"]

        self.ideology2encoding = torch.nn.Linear(self.config["ideology_encoder_in"], self.config["ideology_encoder_out"])

        self.concat2hidden = torch.nn.Linear(shared_params[self.config["shared_lstm_tweet"]]["nout"]*2 +\
                                             shared_params[self.config["shared_lstm_entity"]]["nout"]*2 + \
                                             self.config["ideology_encoder_out"], self.config["n_hidden_concat"])


        self.dropout_layer = torch.nn.Dropout(p=self.config['dropout_rate'])
        self.hidden2label = torch.nn.Linear(self.config["n_hidden_concat"], self.output_dim)

        if self.use_gpu:
            self.sequence_lstm_tweet = self.sequence_lstm_tweet.cuda()
            self.sequence_lstm_entity = self.sequence_lstm_entity.cuda()
            self.ideology2encoding = self.ideology2encoding.cuda()

            self.concat2hidden = self.concat2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()


        self.hidden_bilstm = self.init_hidden_bilstm()

    def init_hidden_bilstm(self):
        var1 = torch.autograd.Variable(torch.zeros(2, self.minibatch_size,
                                                    self.n_hidden_sequence_tweet))
        var2 = torch.autograd.Variable(torch.zeros(2, self.minibatch_size,
                                                    self.n_hidden_sequence_entity))

        if self.use_gpu:
            var1 = var1.cuda()
            var2 = var2.cuda()

        return (var1, var2)


    def _run_sequence(self, seqs, sequence_lstm, key):
        self.minibatch_size = len(seqs)

        # get the length of each seq in your batch
        seq_lengths = self._get_long_tensor(list(map(len, seqs)))

        # dump padding everywhere, and place seqs on the left.
        # NOTE: you only need a tensor as big as your longest sequence
        max_seq_len = seq_lengths.max()

        # Sort according to lengths
        seq_len_sorted, sorted_idx = seq_lengths.sort(descending=True)

        tensor_seq = torch.zeros((len(seqs), max_seq_len)).long()
        if self.use_gpu:
            tensor_seq = tensor_seq.cuda()
        for idx, (seq, seqlen) in enumerate(zip(seqs, seq_lengths)):
            tensor_seq[idx, :seqlen] = self._get_long_tensor(seq)

        # sort inputs
        tensor_seq = tensor_seq[sorted_idx]

        var_seq = self._get_variable(tensor_seq)
        seq_lengths = self._get_variable(seq_lengths)

        var_seq = self.embedding_layers[key](var_seq)

        # pack padded sequences
        packed_input_seq = pack_padded_sequence(var_seq, list(seq_len_sorted.data), batch_first=True)
        # run lstm over sequence
        self.hidden_bilstm = self.init_hidden_bilstm()
        packed_output, self.hidden_bilstm = \
                sequence_lstm(packed_input_seq, self.hidden_bilstm)
        # unpack the output
        unpacked_output, _ = pad_packed_sequence(packed_output, batch_first=True)

        # Reverse sorting
        unpacked_output = torch.zeros_like(unpacked_output).scatter_(0, sorted_idx.unsqueeze(1).unsqueeze(1).expand(-1, unpacked_output.shape[1], unpacked_output.shape[2]), unpacked_output)

        # Do global avg pooling over timesteps
        lstm_output = torch.mean(unpacked_output, dim=1)
        return lstm_output

    def forward(self, x):
        input_index = 0

        key_tweet = list(x['embedding'].keys())[0]
        seqs_tweet = x['embedding'][key_tweet]

        key_entity = list(x['embedding'].keys())[1]
        seqs_entity = x['embedding'][key_entity]

        lstm_output_tweet = self._run_sequence(seqs_tweet, self.sequence_lstm_tweet, key_tweet)
        lstm_output_entity = self._run_sequence(seqs_entity, self.sequence_lstm_tweet, key_entity)


        # now add the extra features
        ideology_feature = [elem[input_index] for elem in x['input']]
        input_index += 1
        ideology_feature = self._get_float_tensor(ideology_feature)
        ideology_feature = self._get_variable(ideology_feature)
        ideology_feature = self.ideology2encoding(ideology_feature)
        ideology_feature = F.relu(ideology_feature)

        concatemated_feature = torch.cat([lstm_output_tweet, lstm_output_entity, ideology_feature], 1)

        hidden_feature = self.concat2hidden(concatemated_feature)
        hidden_feature = F.relu(hidden_feature)
        hidden_feature = self.dropout_layer(hidden_feature)

        logits = self.hidden2label(hidden_feature)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)

        return logits, probas






