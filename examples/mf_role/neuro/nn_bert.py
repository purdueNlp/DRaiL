import logging

import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel, BertModel

from drail.neuro.nn_model import NeuralNetworks


class BertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        #self.output_dim = 3
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = self.config['bert_model']
        if "bert_model_type" in self.config:
            self.bert_model_name = self.config["bert_model_type"]
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        if "shared_encoder" in self.config:
            name = self.config["shared_encoder"]
            self.bert_model = shared_params[name]["bert"]
        else:
            pretrained_bert_model = AutoModel.from_pretrained(self.bert_model_name, add_pooling_layer=True)
            self.bert_model = BertModel(bert_config)
            self.bert_model.load_state_dict(pretrained_bert_model.state_dict(), strict=False)
            del pretrained_bert_model


        if "bert_freeze" in self.config and self.config["bert_freeze"]:
            for name, W in self.bert_model.named_parameters():
                print("freezing", name)
                W.requires_grad = False
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)
        
        hidden_dim=bert_config.hidden_size
        if "ideology" in self.config["features"]:
            self.ideology2hidden=torch.nn.Linear(self.config["ideology_input_dim"], self.config["ideology_hidden_dim"])
            if self.use_gpu:
                self.ideology2hidden = self.ideology2hidden.cuda()
                
            hidden_dim+=self.config["ideology_hidden_dim"]
        
        if "topic" in self.config["features"]:
            self.topic2hidden=torch.nn.Linear(self.config["topic_input_dim"], self.config["topic_hidden_dim"])
            if self.use_gpu:
                self.topic2hidden = self.topic2hidden.cuda()
            
            hidden_dim+=self.config["topic_hidden_dim"]
        
        if "frame" in self.config["features"]:
            self.frame2hidden=torch.nn.Linear(self.config["frame_input_dim"], self.config["frame_hidden_dim"])
            if self.use_gpu:
                self.frame2hidden = self.frame2hidden.cuda()
            
            hidden_dim+=self.config["frame_hidden_dim"]
        
        #self.hidden_extra2hidden=torch.nn.Linear(hidden_dim, 100)

        if "shared_output" in self.config:
            name = self.config["shared_output"]
            self.hidden2label = shared_params[name]["layer"]
        else:
            #self.hidden2label = torch.nn.Linear(bert_config.hidden_size, self.output_dim)
            self.hidden2label = torch.nn.Linear(hidden_dim, self.output_dim)
            #self.hidden2label = torch.nn.Linear(100, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()
            #self.hidden_extra2hidden=self.hidden_extra2hidden.cuda()

    def forward(self, x):
        input_index_used = -1
        
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []
        #if len(x['input'][0]) == 2:
        if "tweet" in self.config["features"] and "entity" in self.config["features"]:
            for elem in x['input']:
                input_ids.append(elem[0][0] + elem[1][0])
                input_mask.append(elem[0][1] + elem[1][1])
                segment_ids.append([0] * len(elem[0][0]) + [1] * len(elem[1][0]))
            input_index_used+=2
        #elif len(x['input'][0]) == 1:
        elif "tweet" in self.config["features"]:
            for elem in x['input']:
                input_ids.append(elem[0][0])
                input_mask.append(elem[0][1])
                segment_ids.append([0] * len(elem[0][0]))
            input_index_used+=1
        else:
            #print("Current BERT doesn't support more than 2 sequences")
            #exit()
            print("No feature found to encode with BERT")
            exit()

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

        #print(input_ids.size(), input_mask.size(), segment_ids.size())

        # Run model
        #print(input_ids.size())
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)
        
        encoded_ideology=None
        if "ideology" in self.config["features"]:
            input_index_used+=1
            
            ideology_feature = [elem[input_index_used] for elem in x['input']]
            ideology_feature = self._get_float_tensor(ideology_feature)
            ideology_feature = self._get_variable(ideology_feature)
            #print (ideology_feature)
            encoded_ideology = self.ideology2hidden(ideology_feature)
            encoded_ideology = F.relu(encoded_ideology)
        
        encoded_topic=None
        if "topic" in self.config["features"]:
            input_index_used+=1
            
            topic_feature = [elem[input_index_used] for elem in x['input']]
            topic_feature = self._get_float_tensor(topic_feature)
            topic_feature = self._get_variable(topic_feature)
            #print (topic_feature)
            encoded_topic = self.topic2hidden(topic_feature)
            encoded_topic = F.relu(encoded_topic)
        
        encoded_frame=None
        if "frame" in self.config["features"]:
            input_index_used+=1
            
            frame_feature = [elem[input_index_used] for elem in x['input']]
            frame_feature = self._get_float_tensor(frame_feature)
            frame_feature = self._get_variable(frame_feature)
            #print (topic_feature)
            encoded_frame = self.frame2hidden(frame_feature)
            encoded_frame = F.relu(encoded_frame)
        
        if encoded_ideology!=None:
            pooled_output = torch.cat([pooled_output, encoded_ideology], 1)
        if encoded_topic!=None:
            pooled_output = torch.cat([pooled_output, encoded_topic], 1)
        if encoded_frame!=None:
            pooled_output = torch.cat([pooled_output, encoded_frame], 1)
        
        '''
        pooled_output=self.hidden_extra2hidden(pooled_output)
        pooled_output=F.relu(pooled_output)
        '''
        logits = self.hidden2label(pooled_output)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas


