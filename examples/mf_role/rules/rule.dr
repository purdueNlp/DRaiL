entity:    "Component",    arguments: ["componentId"::ArgumentType.UniqueString];
entity:    "Essay",        arguments: ["essayId"::ArgumentType.UniqueString];
entity:    "ArgType",      arguments: ["argType"::ArgumentType.UniqueString];
entity:    "StanceType",   arguments: ["argType"::ArgumentType.UniqueString];
entity:    "Par",          arguments: ["parId"::ArgumentType.UniqueID];


predicate: "HasComponent", arguments: [Essay, Component];
predicate: "IsArgType",    arguments: [Essay, Component, ArgType];
predicate: "HasStance",    arguments: [Essay, Component, Component, StanceType];
predicate: "Reln",         arguments: [Essay, Component, Component];
predicate: "Supports",     arguments: [Essay, Component, Component];
predicate: "IsFor",        arguments: [Essay, Component];
predicate: "InPar",        arguments: [Essay, Component, Par];

label: "ArgLabel", classes: 3, type: LabelType.Multiclass;
label: "StanceLabel", classes: 3, type: LabelType.Multiclass;

load: "HasComponent", file: "has_component.txt";
load: "IsArgType",    file: "is_argument_type.txt";
load: "HasStance",    file: "has_stance.txt";
load: "Reln",         file: "reln.txt";
load: "Supports",     file: "supports.txt";
load: "IsFor",        file: "is_for.txt";
load: "ArgType",      file: "argument_label.txt";
load: "ArgLabel",     file: "argument_label.txt";
load: "StanceType",   file: "stance_label.txt";
load: "StanceLabel",  file: "stance_label.txt";
load: "InPar",        file: "in_par.txt";

femodule: "Argument_ft";
feclass: "Argument_ft";

ruleset {

  rule: HasComponent(E,C) & InPar(E,C,P) => IsArgType(E,C,T^ArgLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [embedding("words_component_left", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
                vector("structural_feats"), vector("indicator_feats"), vector("context_feats")],
  target: C;

  rule: HasComponent(E,A) & HasComponent(E,B) & InPar(E,A,P) & InPar(E,B,P) => Reln(E,A,B)^?
  lambda: 1.0,
  network: "config.json",
  fefunctions: [embedding("words_component_left", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
                embedding("words_component_right", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
                input("component_feats_left"),
                input("component_feats_right"),
                vector("reln_feats")],
  target: C;

  //rule: HasComponent(E,A) & HasComponent(E,B) & InPar(E,A,P) & InPar(E,B,P)
  //      & IsArgType(E,A,T^ArgLabel?) & IsArgType(E,B,U^ArgLabel?) => Reln(E,A,B)^?,
  //lambda: 1.0,
  //network: "config.json",
  //fefunctions: [embedding("words_component_left", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
  //              embedding("words_component_right", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
  //              input("component_feats_left"),
  //              input("component_feats_right"),
  //              vector("reln_feats"), vector("reln_transitions")],
  //target: C;

  //rule: HasComponent(E,A) & HasComponent(E,B)
  //      & InPar(E,A,P) & InPar(E,B,Q) => HasStance(E,A,B,T^StanceLabel?),
  //lambda: 1.0,
  //network: "config.json",
  //fefunctions: [embedding("words_component_left", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
  //              embedding("words_component_right", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
  //              input("component_feats_left"),
  //              input("component_feats_right"),
  //              input("reln_feats")],
  //target: C;

  // source is always a premise
  hardconstr: HasComponent(E,A) & HasComponent(E,B)
              & InPar(E,A,P) & InPar(E,B,P) & Reln(E,A,B)^?
                => IsArgType(E,A,"Premise")^?;

  // target is either a premise or a claim
  hardconstr: HasComponent(E,A) & HasComponent(E,B)
              & InPar(E,A,P) & InPar(E,B,P) & Reln(E,A,B)^?
                => ~IsArgType(E,B,"MajorClaim")^?;

  // relations always have to be labeled
  //hardconstr: HasComponent(E,A) & HasComponent(E,B)
  //            & InPar(E,A,P) & InPar(E,B,P) & Reln(E,A,B)^?
  //              => ~HasStance(E,A,B,"None")^?;

  // non-relations can't have labels
  //hardconstr: HasComponent(E,A) & HasComponent(E,B)
  //            & InPar(E,A,P) & InPar(E,B,P) & ~Reln(E,A,B)^?
  //            & ~IsArgType(E,A,"Claim")^? & ~IsArgType(E,B,"MajorClaim")^?
  //              => HasStance(E,A,B,"None")^?;

  // there is always a labeled reln between claims and major claims
  //hardconstr: HasComponent(E,A) & HasComponent(E,B)
  //            & IsArgType(E,A,"Claim")^? & IsArgType(E,B,"MajorClaim")^?
  //              => ~HasStance(E,A,B,"None")^?;

  // define tree structure
  treeconstr: HasComponent(E,A) & HasComponent(E,B) & InPar(E,A,P) & InPar(E,B,P) => Reln(E,A,B)^?,
  root: ~IsArgType(E,A,"Premise"),
  notRoot: IsArgType(E,A,"Premise"),
  leaf: IsArgType(E,B,"MajorClaim");

} groupby: HasComponent.1;
