import numpy as np
import argparse
import os
import cProfile as profile
import random
import torch
import csv
import json
import logging.config
from pathlib import Path

from sklearn.metrics import *
from sklearn.model_selection import KFold

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

from drail.inference.randomized.purepython.infer_argmining import RInfArgMining # pure python
# from drail.inference.randomized.infer_argmining import RInfArgMining              # C++


def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('-m', help='mode: [global|local]', dest='mode', type=str, default='local')
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--inference-only', help='run inference only', dest='inference_only', default=False, action='store_true')
    parser.add_argument('--train-only', help='run training only', dest='train_only', default=False, action='store_true')
    parser.add_argument('--drop-scores', help='drop predicted scores', dest='drop_scores', default=False, action='store_true')
    parser.add_argument('--continue-from-checkpoint', help='continue from checkpoint in savedir', dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_argument('--lr', help='global learning rate', dest='global_lr', type=float)
    parser.add_argument('--delta', help='loss augmented inference', dest='delta', action='store_true', default=False)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--logging_config', help='logging configuration file', type=str, required=True)
    parser.add_argument('--folds', help='number of folds', dest='folds', type=int, default=5)

    opts = parser.parse_args()
    return opts

'''
def get_folds(n_folds):
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    data_file = realpath + "data/has_entity.txt"

    train_tweets=set()
    f=open(data_file, "r")
    while True:
        l=f.readline()
        if not l or l=='':
            break
        l=l.split('\t')
        train_tweets.add(int(l[0]))

    train_tweets=list(train_tweets)
    random.shuffle(train_tweets)

    tweets = train_tweets
    train_tweets, dev_tweets, test_tweets = [], [], []
    k_folds = KFold(n_splits=n_folds, shuffle=True)

    for train, test in k_folds.split(tweets):
        dev_split = int(len(test) * 0.8)
        
        
        train_tweets.append([tweets[i] for i in test[:dev_split]])
        dev_tweets.append([tweets[i] for i in test[dev_split:]])
        test_tweets.append([tweets[i] for i in train])
        
    return train_tweets, dev_tweets, test_tweets
'''

def get_folds(n_folds):
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    data_file = realpath + "data/has_entity.txt"

    train_tweets=set()
    f=open(data_file, "r")
    while True:
        l=f.readline()
        if not l or l=='':
            break
        l=l.split('\t')
        train_tweets.add(l[0])

    train_tweets=list(train_tweets)
    random.shuffle(train_tweets)

    tweets = train_tweets
    train_tweets, dev_tweets, test_tweets = [], [], []
    
    
    for i in range(n_folds):
        random.seed(100*i)
        train_size=int((len(tweets)*60)/100)
        train=random.sample(tweets, train_size)
        valid=random.sample([t for t in tweets if t not in train], int((len(tweets)*10)/100))
        test=[t for t in tweets if t not in train+valid]
        train_tweets.append(train)
        dev_tweets.append(valid)
        test_tweets.append(test)
    
    
    return train_tweets, dev_tweets, test_tweets



def main():
    opts = parse_options()
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    continue_from_checkpoint_ORIGINAL = opts.continue_from_checkpoint

    # External resources for features
    # These were generated running extract_features.py + txt file for glove embeddings
    DATA_F       = realpath + "data/drail_data.pickle"
    FE_PATH      = realpath + "feats"
    NE_PATH      = realpath + "neuro"

    # seed and cuda
    np.random.seed(1234)
    random.seed(1234)

    if opts.gpu_index:
        torch.cuda.set_device(opts.gpu_index)

    rule_file = os.path.join(opts.rules)
    conf_file = os.path.join(opts.config)

    # TO-DO: Generalize loading new inference classes and its parameters
    if opts.mode == "global":
        learner = GlobalLearner(learning_rate=opts.global_lr,
                                use_gpu=(opts.gpu_index is not None),
                                gpu_index=opts.gpu_index,
                                loss_fn=opts.lossfn)
    else:
        learner = LocalLearner()

    learner.compile_rules(rule_file)
    db = learner.create_dataset(opts.dir)

    train, dev, test = get_folds(opts.folds)
    avg_metrics = {}

    for i in range(opts.folds):
        curr_savedir = os.path.join(opts.savedir, "f{}".format(i))
        Path(curr_savedir).mkdir(parents=True, exist_ok=True)

        if opts.folds > 1:
            train_tweets, dev_tweets, test_tweets = train[i], dev[i], test[i]
        else:
            train_tweets, dev_tweets, test_tweets = train, dev, test

        # Debug
        if opts.debug:
            train_tweets = train_tweets[:10]
            dev_tweets = dev_tweets[:10]
            test_tweets = test_tweets[:10]

        logger.info("train {}, dev {}, test {}".format(len(train_tweets), len(dev_tweets), len(test_tweets)))

        db.add_filters(filters=[
            ("HasEntity", "isDummy", "tweetId_1", train_tweets[0:1]),
            ("HasEntity", "isTrain", "tweetId_1", train_tweets),
            ("HasEntity", "isDev", "tweetId_1", dev_tweets),
            ("HasEntity", "isTest", "tweetId_1", test_tweets),
            ("HasEntity", "allFolds", "tweetId_1", train_tweets + dev_tweets + test_tweets),
            ])

        learner.build_feature_extractors(db,
                                        data_f=DATA_F,
                                        debug=opts.debug,
                                        femodule_path=FE_PATH,
                                        filters=[("HasEntity", "isDummy", 1)])
        learner.set_savedir(curr_savedir)
        learner.build_models(db, conf_file, netmodules_path=NE_PATH)

        if opts.mode == "local":
            if opts.continue_from_checkpoint:
                learner.init_models()

            if not opts.inference_only:
                learner.train(db, train_filters=[("HasEntity", "isTrain", 1)], dev_filters=[("HasEntity", "isDev", 1)], test_filters=[("HasEntity", "isTest", 1)])

            if not opts.train_only:
                learner.extract_data(db, extract_test=True,
                                      test_filters=[("HasEntity", "isTest", 1)])
                del learner.fe
                res, heads = learner.predict(None, fold='test', get_predicates=True)
                if opts.drop_scores:
                    learner.drop_scores(None, fold='test', output=os.path.join(opts.savedir, 'test_scores_f{}.csv'.format(i)), heads=heads)
        else:
            learner.extract_data(
                    db,
                    train_filters=[("HasEntity", "isTrain", 1)],
                    dev_filters=[("HasEntity", "isDev", 1)],
                    test_filters=[("HasEntity", "isTest", 1)],
                    extract_train=not opts.inference_only,
                    extract_dev=not opts.inference_only,
                    extract_test=True)

            res, heads = learner.train(
                    db,
                    train_filters=[("HasEntity", "isTrain", 1)],
                    dev_filters=[("HasEntity", "isDev", 1)],
                    test_filters=[("HasEntity", "isTest", 1)],
                    opt_predicate='HasRole',
                    loss_augmented_inference=opts.delta,
                    continue_from_checkpoint=opts.continue_from_checkpoint,
                    inference_only=opts.inference_only,
                    weight_classes=True,
                    patience=10)
            opts.continue_from_checkpoint = continue_from_checkpoint_ORIGINAL

        if not opts.train_only:
            for pred in res.metrics:
                if pred in set(['HasRole']):
                    y_gold = res.metrics[pred]['gold_data']
                    y_pred = res.metrics[pred]['pred_data']

                    if pred not in avg_metrics:
                        avg_metrics[pred] = {}
                        avg_metrics[pred]['gold'] = y_gold
                        avg_metrics[pred]['pred'] = y_pred
                    else:
                        avg_metrics[pred]['gold'].extend(y_gold)
                        avg_metrics[pred]['pred'].extend(y_pred)

                    logger.info('\n'+ pred + ':\n' + classification_report(y_gold, y_pred, digits=4))
                    macro_f1 = f1_score(y_gold, y_pred, average='macro')
                    logger.info("TEST macro f1 {}".format(macro_f1))

            learner.reset_metrics()

    if opts.folds > 1:
        logger.info(40 * '=' + '\navg classif. report of {0} folds\n'.format(opts.folds) + 40 * '=')
        for pred in avg_metrics:
            logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))
            logger.info("TEST macro f1 {}".format(f1_score(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], average='macro')))

if __name__ == "__main__":
    opts = parse_options()
    if opts.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(opts.logging_config)))
    # Reproducibility
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)
    main()

