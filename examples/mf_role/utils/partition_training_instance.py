from collections import Counter

data = {}
topics = set([])
ideos = set([])
months = set([])
instance_indices = {}

with open("data/has_topic.txt") as fp:
    for line in fp:
        tweet, topic = line.strip().split()
        data[tweet] = {'topic': topic}
        topics.add(topic)

with open("data/has_ideology.txt") as fp:
    for line in fp:
        tweet, ideo = line.strip().split()
        data[tweet]['ideo'] = ideo
        ideos.add(ideo)

with open("data/has_dop.txt") as fp:
    for line in fp:
        tweet, dop = line.strip().split()
        year, month, day = dop.split('-')
        data[tweet]['month'] = int(month)
        months.add(int(month))

index = 0
for topic in topics:
    for ideo in ideos:
        if topic not in instance_indices:
            instance_indices[topic] = {}
        if ideo not in instance_indices[topic]:
            instance_indices[topic][ideo] = {}
        # month < 5
        instance_indices[topic][ideo]['lower'] = index
        index += 1
        # month >= 5 and < 9
        instance_indices[topic][ideo]['middle'] = index
        index += 1
        # month >= 9
        instance_indices[topic][ideo]['upper'] = index
        index += 1

indices = []; tweets = []
for tweet in data:
    topic = data[tweet]['topic']
    ideo = data[tweet]['ideo']
    if 'month' in data[tweet]:
        month = data[tweet]['month']
        if month < 5:
            indices.append(instance_indices[topic][ideo]['lower'])
        elif month < 9 and month >= 5:
            indices.append(instance_indices[topic][ideo]['middle'])
        else:
            indices.append(instance_indices[topic][ideo]['upper'])
    else:
        # this tweet has no DOP
        #print(tweet)
        indices.append(instance_indices[topic][ideo]['lower'])
    tweets.append(tweet)
print(Counter(indices))

with open("data/in_instance_topic_ideo_month.txt", "w") as fp:
    for tweet in data:
        topic = data[tweet]['topic']
        ideo = data[tweet]['ideo']
        if 'month' in data[tweet]:
            month = data[tweet]['month']
            if month < 5:
                fp.write("{}\t{}\n".format(tweet, instance_indices[topic][ideo]['lower']))
            elif month >= 5 and month < 9:
                fp.write("{}\t{}\n".format(tweet, instance_indices[topic][ideo]['middle']))
            else:
                fp.write("{}\t{}\n".format(tweet, instance_indices[topic][ideo]['upper']))
        else:
            fp.write("{}\t{}\n".format(tweet, instance_indices[topic][ideo]['upper']))

