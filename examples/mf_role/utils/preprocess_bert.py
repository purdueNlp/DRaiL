import pickle
from transformers import AutoTokenizer

def ids2text(ids, idx2word):
    ret = []
    for _id in ids:
        ret.append(idx2word[_id])
    return " ".join(ret)

def main():
    data_f = "data/drail_data.pickle"

    with open(data_f, 'rb') as fp:
        [tweet_id2tokens, entity_id2tokens,\
         ideology_id2one_hot,\
         word_dict, vocab_size, word_emb_size, word2idx]  = pickle.load(fp, encoding="latin1")

    idx2word = {v: k for k, v in word2idx.items()}
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    batch_tweet_ids = []; batch_tweet_strings = []
    for tweet in tweet_id2tokens:
        tweet_str = ids2text(tweet_id2tokens[tweet], idx2word)
        batch_tweet_ids.append(tweet)
        batch_tweet_strings.append(tweet_str)

    batch_entity_ids = []; batch_entity_strings = []
    for entity in entity_id2tokens:
        entity_str = ids2text(entity_id2tokens[entity], idx2word)
        batch_entity_ids.append(entity)
        batch_entity_strings.append(entity_str)

    encoded_input = tokenizer(batch_tweet_strings, padding=True)
    tweet_id2bert = {}; entity_id2bert = {}
    for i in range(0, len(batch_tweet_ids)):
        tweet_id2bert[batch_tweet_ids[i]] = {'input_ids': encoded_input['input_ids'][i], 'attention_mask': encoded_input['attention_mask'][i]}
    encoded_input = tokenizer(batch_entity_strings, padding=True)
    for i in range(0, len(batch_entity_ids)):
        entity_id2bert[batch_entity_ids[i]] = {'input_ids': encoded_input['input_ids'][i], 'attention_mask': encoded_input['attention_mask'][i]}


    output = [tweet_id2bert, entity_id2bert, ideology_id2one_hot]
    pickle.dump(output, open('data/drail_data_bert.pickle', 'wb'))

if __name__ == "__main__":
    main()
