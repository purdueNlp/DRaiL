import numpy as np
import argparse
import os
import cProfile as profile
import random
import torch
import csv
import json
import logging.config
from pathlib import Path

from sklearn.metrics import *
from sklearn.model_selection import KFold

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

from drail.inference.randomized.purepython.infer_argmining import RInfArgMining # pure python
# from drail.inference.randomized.infer_argmining import RInfArgMining              # C++


def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('-m', help='mode: [global|local]', dest='mode', type=str, default='local')
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--inference-only', help='run inference only', dest='inference_only', default=False, action='store_true')
    parser.add_argument('--train-only', help='run training only', dest='train_only', default=False, action='store_true')
    parser.add_argument('--drop-scores', help='drop predicted scores', dest='drop_scores', default=False, action='store_true')
    parser.add_argument('--output-probas', help='drop predicted scores in proba form', dest='output_probas', default=False, action='store_true')
    parser.add_argument('--continue-from-checkpoint', help='continue from checkpoint in savedir', dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_argument('--lr', help='global learning rate', dest='global_lr', type=float)
    parser.add_argument('--delta', help='loss augmented inference', dest='delta', action='store_true', default=False)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--logging_config', help='logging configuration file', type=str, required=True)
    parser.add_argument('--folds', help='number of folds', dest='folds', type=int, default=5)
    parser.add_argument('--use_bert', help='use bert', dest='use_bert', default=False, action='store_true')
    parser.add_argument('--start_fold', type=int, default=0)
    parser.add_argument('--end_fold', type=int, default=None)
    opts = parser.parse_args()
    return opts


def get_folds(n_folds):
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    data_file = realpath + "data/in_instance.txt"

    train_tweets=[]
    f=open(data_file, "r")
    while True:
        l=f.readline()
        if not l or l=='':
            break
        l=l.split('\t')
        if l[0] not in train_tweets:
            train_tweets.append(l[0])

    random.shuffle(train_tweets)

    tweets = train_tweets
    train_tweets, dev_tweets, test_tweets = [], [], []

    if n_folds > 1:
        k_folds = KFold(n_splits=n_folds, shuffle=True)

        for train, test in k_folds.split(tweets):
            dev_split = int(len(train) * 0.8)

            train_tweets.append([tweets[i] for i in train[:dev_split]])
            dev_tweets.append([tweets[i] for i in train[dev_split:]])
            test_tweets.append([tweets[i] for i in test])
    elif n_folds == 1:
        dev_split = int(len(tweets) * 0.8)
        train_tweets = tweets[:dev_split]
        dev_tweets = tweets[dev_split:]
    else:
        logger.error("nfolds must be >= 1")
        exit(-1)

    return train_tweets, dev_tweets, test_tweets

'''
def get_folds(n_folds):
    torch.manual_seed(1534)
    np.random.seed(1534)
    random.seed(1534)
    
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    data_file = realpath + "data/has_entity.txt"

    train_tweets=set()
    f=open(data_file, "r")
    while True:
        l=f.readline()
        if not l or l=='':
            break
        l=l.split('\t')
        train_tweets.add(l[0])

    train_tweets=list(train_tweets)
    random.shuffle(train_tweets)

    tweets = train_tweets
    train_tweets, dev_tweets, test_tweets = [], [], []
    
    
    for i in range(n_folds):
        #random.seed(100*i)
        train_size=int((len(tweets)*60)/100)
        train=random.sample(tweets, train_size)
        valid=random.sample([t for t in tweets if t not in train], int((len(tweets)*10)/100))
        test=[t for t in tweets if t not in train+valid]
        train_tweets.append(train)
        dev_tweets.append(valid)
        test_tweets.append(test)
    
    
    return train_tweets, dev_tweets, test_tweets
'''


def main():
    opts = parse_options()
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    continue_from_checkpoint_ORIGINAL = opts.continue_from_checkpoint

    # External resources for features
    # These were generated running extract_features.py + txt file for glove embeddings
    if not opts.use_bert:
        DATA_F       = realpath + "data/drail_data.pickle"
        optimizer = "SGD"
    else:
        DATA_F       = realpath + "data/drail_data_bert.pickle"
        optimizer = "AdamW"
    FE_PATH      = realpath + "feats"
    NE_PATH      = realpath + "neuro"

    # seed and cuda
    torch.manual_seed(1534)
    np.random.seed(1534)
    random.seed(1534)

    if opts.gpu_index:
        torch.cuda.set_device(opts.gpu_index)

    rule_file = os.path.join(opts.rules)
    conf_file = os.path.join(opts.config)

    # TO-DO: Generalize loading new inference classes and its parameters
    if opts.mode == "global":
        learner = GlobalLearner(learning_rate=opts.global_lr,
                                use_gpu=(opts.gpu_index is not None),
                                gpu_index=opts.gpu_index,
                                loss_fn=opts.lossfn)
    else:
        learner = LocalLearner()

    learner.compile_rules(rule_file)
    db = learner.create_dataset(opts.dir)

    train, dev, test = get_folds(opts.folds)
    test = dev
    avg_metrics = {}

    for i in range(opts.folds):
        print(i, opts.end_fold)
        if i < opts.start_fold:
            continue
        if opts.end_fold and i >= opts.end_fold:
            break

        torch.cuda.empty_cache()
        curr_savedir = os.path.join(opts.savedir, "f{}".format(i))
        Path(curr_savedir).mkdir(parents=True, exist_ok=True)

        if opts.folds > 1:
            train_tweets, dev_tweets, test_tweets = train[i], dev[i], test[i]
        else:
            train_tweets, dev_tweets, test_tweets = train, dev, test

        # Debug
        if opts.debug:
            train_tweets = train_tweets[:10]
            dev_tweets = dev_tweets[:10]
            test_tweets = test_tweets[:10]

        logger.info("train {}, dev {}, test {}".format(len(train_tweets), len(dev_tweets), len(test_tweets)))

        db.add_filters(filters=[
            ("InInstance", "isDummy", "tweetId_1", train_tweets[0:1]),
            ("InInstance", "isTrain", "tweetId_1", train_tweets),
            ("InInstance", "isDev", "tweetId_1", dev_tweets),
            ("InInstance", "isTest", "tweetId_1", test_tweets),
            ("InInstance", "allFolds", "tweetId_1", train_tweets + dev_tweets + test_tweets)
            ])

        learner.build_feature_extractors(db,
                                        data_f=DATA_F,
                                        use_bert=opts.use_bert,
                                        debug=opts.debug,
                                        femodule_path=FE_PATH,
                                        filters=[("InInstance", "isDummy", 1)])
        learner.set_savedir(curr_savedir)
        learner.build_models(db, conf_file, netmodules_path=NE_PATH)

        if opts.mode == "local":
            if opts.continue_from_checkpoint:
                learner.init_models()

            if not opts.inference_only:
                learner.train(db, train_filters=[("InInstance", "isTrain", 1)], dev_filters=[("InInstance", "isDev", 1)], test_filters=[("InInstance", "isTest", 1)],
                              optimizer=optimizer)

            if not opts.train_only:
                learner.extract_instances(db,
                                    extract_test=True, test_filters=[("InInstance", "isTest", 1)],
                                    extract_dev=False, dev_filters=[("InInstance", "isDev", 1)],
                                    extract_train=False, train_filters=[("InInstance", "isTrain", 1)])
                res, heads = learner.predict(db, fold_filters=[("InInstance", "isTest", 1)], fold='test', get_predicates=True)
                #_, heads_dev = learner.predict(db, fold_filters=[("InInstance", "isDev", 1)], fold='dev', get_predicates=True)
                #_, heads_train = learner.predict(db, fold_filters=[("InInstance", "isTrain", 1)], fold='train', get_predicates=True)
                if opts.drop_scores:
                    learner.drop_scores(db, fold_filters=[("InInstance", "isTrain", 1)], fold='train', output=os.path.join(opts.savedir, 'train_scores_f{}.csv'.format(i)), heads=heads_train, output_probas=opts.output_probas)
                    #learner.drop_scores(db, fold_filters=[("InInstance", "isDev", 1)], fold='dev', output=os.path.join(opts.savedir, 'dev_scores_f{}.csv'.format(i)), heads=heads_dev, output_probas=opts.output_probas)
                    #learner.drop_scores(db, fold_filters=[("InInstance", "isTest", 1)], fold='test', output=os.path.join(opts.savedir, 'test_scores_f{}.csv'.format(i)), heads=heads, output_probas=opts.output_probas)
        else:
            learner.extract_data(
                    db,
                    train_filters=[("InInstance", "isTrain", 1)],
                    dev_filters=[("InInstance", "isDev", 1)],
                    test_filters=[("InInstance", "isTest", 1)],
                    extract_train=not opts.inference_only,
                    extract_dev=not opts.inference_only,
                    extract_test=True)

            res, heads = learner.train(
                    db,
                    train_filters=[("InInstance", "isTrain", 1)],
                    dev_filters=[("InInstance", "isDev", 1)],
                    test_filters=[("InInstance", "isTest", 1)],
                    opt_predicates=set(['HasRole','HasMf']),
                    loss_augmented_inference=opts.delta,
                    continue_from_checkpoint=opts.continue_from_checkpoint,
                    inference_only=opts.inference_only,
                    weight_classes=True,
                    patience=10,
                    optimizer=optimizer)
            opts.continue_from_checkpoint = continue_from_checkpoint_ORIGINAL

        if not opts.train_only:
            for pred in res.metrics:
                if pred in set(['HasRole', 'HasMf']):
                    y_gold = res.metrics[pred]['gold_data']
                    y_pred = res.metrics[pred]['pred_data']

                    if pred not in avg_metrics:
                        avg_metrics[pred] = {}
                        avg_metrics[pred]['gold'] = y_gold
                        avg_metrics[pred]['pred'] = y_pred
                    else:
                        avg_metrics[pred]['gold'].extend(y_gold)
                        avg_metrics[pred]['pred'].extend(y_pred)

                    logger.info('\n'+ pred + ':\n' + classification_report(y_gold, y_pred, digits=4))
                    macro_f1 = f1_score(y_gold, y_pred, average='macro')
                    logger.info("TEST macro f1 {}".format(macro_f1))

            learner.reset_metrics()

    if opts.folds > 1:
        logger.info(40 * '=' + '\navg classif. report of {0} folds\n'.format(opts.folds) + 40 * '=')
        for pred in avg_metrics:
            logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))
            logger.info("TEST macro f1 {}".format(f1_score(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], average='macro')))

if __name__ == "__main__":
    opts = parse_options()
    if opts.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(opts.logging_config)))
    # Reproducibility
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)
    main()

