 # -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import json
import string
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
import random
from numpy import array
import re
import pickle
from pattern.text.en import singularize
from nltk.corpus import wordnet
import enchant
from nltk.stem import LancasterStemmer
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
from nltk.corpus import stopwords
from sklearn.metrics import f1_score
import operator
from collections import Counter
from nltk.corpus import wordnet as wn
import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm
from collections import defaultdict
import json
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk import RegexpParser
from nltk import Tree
import pandas as pd
import nltk
from difflib import SequenceMatcher
import wordninja
import spacy
from nltk.stem import LancasterStemmer

nlp = spacy.load('en')

ls = LancasterStemmer()

nltk.download('maxent_ne_chunker')
nltk.download('words')


ls = LancasterStemmer()
dictionary = enchant.Dict("en_US")
import json
import os
import re

glove={}

def is_ascii(s):
    return all(ord(c) < 128 for c in s)


care_dict=['safe', 'peace', 'compassion', 'empath', 'sympath', 'care', 'caring', 'protect', 'shield', 'shelter', 'amity', 'secur', 'benefit', 'defen',\
            'guard', 'preserve']
harm_dict=['harm', 'suffer', 'war', 'wars', 'warl', 'warring', 'violen', 'hurt', 'kill', 'kills', 'killer', 'killed', 'killing', 'endanger', 'cruel', 'brutal',\
            'abuse', 'damag', 'ruin', 'ravage', 'detriment', 'crush', 'attack', 'annihilate', 'destroy', 'stomp', 'abandon', 'spurn', 'impair', 'exploit',\
            'exploits', 'exploited', 'exploiting', 'wound']


stem2words=defaultdict(set)
word2stem=defaultdict(set)

def RefinedEntity(text):
    global word2stem, stem2words

    _5_grams = None
    _4_grams = None
    _3_grams = None
    _2_grams = None
    _1_grams = None
    refined_e = None


    text = " " + text
    text = re.sub(r'http\S+', '', text)
    text = text.replace("&amp;", "")
    text = text.replace('’', '\'')
    text = text.replace("“", "\"")
    text = text.replace("”", "\"")
    text = text.replace("—", "-")
    text = text.replace(' RT ', '')
    text = text.replace(' rt ', '')
    remove = string.punctuation
    remove = remove.replace("\'", "")
    pattern = r"[{}]".format(remove)
    text = re.sub(pattern, " ", text)
    text = text.strip()
    text = text.lower()
    text = text.split()

    _entity = ""

    for w in text:
        if (w in glove or '\'' in w):
            _entity += " " + w

        else:
            splitted_w = wordninja.split(w)

            for sw in splitted_w:
                _entity += " " + sw
    _entity = _entity.strip()

    doc = nlp(_entity)

    ne = ''
    stemmed_entity = ''
    for token in doc:
        if token.lemma_ in stopwords.words('english') or token.lemma_.isdigit() == True or '\'' in token.lemma_ or token.lemma_=='-PRON-':
            continue
        ne += " " + token.lemma_
        stemmed_lemma = ls.stem(token.lemma_)

        stemmed_entity+=" "+stemmed_lemma

        stem2words[stemmed_lemma].add(token.lemma_)
        word2stem[token.lemma_].add(stemmed_lemma)

    refined_e = ne.strip()
    stemmed_entity = stemmed_entity.strip()

    _1_grams = nltk.word_tokenize(stemmed_entity)
    _2_grams = ngrams(_1_grams, 2)
    _3_grams = ngrams(_1_grams, 3)
    _4_grams = ngrams(_1_grams, 4)
    _5_grams = ngrams(_1_grams, 5)

    _1_grams = set(_1_grams)
    _2_grams = set([tuple(sorted(g)) for g in _2_grams])
    _3_grams = set([tuple(sorted(g)) for g in _3_grams])
    _4_grams = set([tuple(sorted(g)) for g in _4_grams])
    _5_grams = set([tuple(sorted(g)) for g in _5_grams])



    return [refined_e, _1_grams, _2_grams, _3_grams, _4_grams, _5_grams]




def Break_Words():
    #ProcessGlove()

    with open("data.json", "r") as fi:
        all_data_points=json.load(fi)



    _1_gram2count = defaultdict(int)
    _2_gram2count = defaultdict(int)
    _3_gram2count = defaultdict(int)
    _4_gram2count = defaultdict(int)
    _5_gram2count = defaultdict(int)

    entity2_1_grams = defaultdict(set)
    entity2_2_grams = defaultdict(set)
    entity2_3_grams = defaultdict(set)
    entity2_4_grams = defaultdict(set)
    entity2_5_grams = defaultdict(set)

    entity2refined_e = {}

    entities=set()
    for d in all_data_points:
        entity=d[2]
        [refined_e, _1_grams, _2_grams, _3_grams, _4_grams, _5_grams]=RefinedEntity(entity)

        entity2refined_e[entity] = refined_e
        entity2_1_grams[entity] = _1_grams
        entity2_2_grams[entity] = _2_grams
        entity2_3_grams[entity] = _3_grams
        entity2_4_grams[entity] = _4_grams
        entity2_5_grams[entity] = _5_grams

        entities.add(refined_e)

        for g in _1_grams:
            _1_gram2count[g]+=1
        for g in _2_grams:
            _2_gram2count[g]+=1
        for g in _3_grams:
            _3_gram2count[g]+=1
        for g in _4_grams:
            _4_gram2count[g]+=1
        for g in _5_grams:
            _5_gram2count[g]+=1


    f=open("unigrams.txt", "w")
    for ug in _1_gram2count:
        if _1_gram2count[ug]>=5:
            f.write(ug+"\t"+str(stem2words[ug])+"\t"+str(_1_gram2count[ug])+"\n")
    f.close()


    permitted_1_grams = set()
    permitted_2_grams = set([g for g in _2_gram2count if _2_gram2count[g] > 1])
    permitted_3_grams = set([g for g in _3_gram2count if _3_gram2count[g] > 1])
    permitted_4_grams = set([g for g in _4_gram2count if _4_gram2count[g] > 1])
    permitted_5_grams = set([g for g in _5_gram2count if _5_gram2count[g] > 1])

    print permitted_2_grams
    print permitted_3_grams
    print permitted_4_grams
    print permitted_5_grams

    f = open("permitted_unigrams.txt", "r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.split("\t")
        permitted_1_grams.add(l[0])



    entity2adjacent_entities=defaultdict(set)
    #f1=open("splitted_words.txt", "w")
    for e1 in entity2refined_e:
        #f1.write(e1+"\t")
        entity2adjacent_entities[e1]=set()
        for e2 in entity2refined_e:
            if e1==e2:
                continue

            '''
            if len(entity2_5_grams[e1] & entity2_5_grams[e2]) > 0\
                    or len(entity2_4_grams[e1] & entity2_4_grams[e2]) > 0\
                    or len(entity2_3_grams[e1] & entity2_3_grams[e2]) > 0\
                    or len(entity2_2_grams[e1] & entity2_2_grams[e2]) > 0:

                entity2adjacent_entities[e1].add(e2)
            '''
            if len(entity2_5_grams[e1] & entity2_5_grams[e2]) > 0:
                entity2adjacent_entities[e1].add(e2)
            if len(entity2_4_grams[e1])==len(entity2_4_grams[e2]) and len(entity2_4_grams[e1])>0\
                    and len(entity2_4_grams[e1] & entity2_4_grams[e2])==len(entity2_4_grams[e2])\
                    and len(entity2_5_grams[e1])==0\
                    and len(entity2_5_grams[e2])==0:
                entity2adjacent_entities[e1].add(e2)
            elif len(entity2_3_grams[e1])==len(entity2_3_grams[e2]) and len(entity2_3_grams[e1])>0\
                    and len(entity2_3_grams[e1] & entity2_3_grams[e2])==len(entity2_3_grams[e2])\
                    and len(entity2_5_grams[e1])==0 and len(entity2_4_grams[e1])==0\
                    and len(entity2_5_grams[e2])==0 and len(entity2_4_grams[e2])==0:
                entity2adjacent_entities[e1].add(e2)
            elif len(entity2_2_grams[e1])==len(entity2_2_grams[e2]) and len(entity2_2_grams[e1])>0\
                    and len(entity2_2_grams[e1] & entity2_2_grams[e2])==len(entity2_2_grams[e2])\
                    and len(entity2_5_grams[e1])==0 and len(entity2_4_grams[e1])==0 and len(entity2_3_grams[e1])==0\
                    and len(entity2_5_grams[e2])==0 and len(entity2_4_grams[e2])==0 and len(entity2_3_grams[e2])==0:
                entity2adjacent_entities[e1].add(e2)



            elif len(entity2_1_grams[e1] & entity2_1_grams[e2] & permitted_1_grams) > 0:

                common_unigrams=list(entity2_1_grams[e1] & entity2_1_grams[e2] & permitted_1_grams)

                e1_ngrams=(entity2_5_grams[e1] & permitted_5_grams)\
                          | (entity2_4_grams[e1] & permitted_4_grams)\
                          | (entity2_3_grams[e1] & permitted_3_grams)\
                          | (entity2_2_grams[e1] & permitted_2_grams)

                e2_ngrams = (entity2_5_grams[e2] & permitted_5_grams) \
                            | (entity2_4_grams[e2] & permitted_4_grams) \
                            | (entity2_3_grams[e2] & permitted_3_grams) \
                            | (entity2_2_grams[e2] & permitted_2_grams)

                e1_ngrams_splitted=[]

                for ngram in e1_ngrams:
                    e1_ngrams_splitted+=list(ngram)

                e2_ngrams_splitted = []

                for ngram in e2_ngrams:
                    e2_ngrams_splitted += list(ngram)

                if len(common_unigrams)>0 and len(e1_ngrams)==0 and len(e2_ngrams)==0:
                    entity2adjacent_entities[e1].add(e2)
                    #print e1, e2

        #f1.write("\n")

    f=open("entity_groups.txt", "w")
    for entity in entity2adjacent_entities:
        adj_entities=entity2adjacent_entities[entity]

        for e in adj_entities:
            f.write(e+"\n")

        f.write("\n\n\n")
    f.close()


    with open("entity2adjacent_entities.pickle", "wb") as out_file:
        pickle.dump(entity2adjacent_entities, out_file)


    return



'''
def Clean_Text(text):
    print text
    text = " " + text
    text = re.sub(r'http\S+', '', text)
    text = text.replace("&amp;", "")
    text = text.replace('’', '\'')
    text = text.replace("“", "\"")
    text = text.replace("”", "\"")
    text = text.replace("—", "-")
    remove = string.punctuation
    pattern = r"[{}]".format(remove)
    text = re.sub(pattern, ' ', text)
    text = text.replace(' RT ', '')
    text = text.replace(' rt ', '')

    text=" ".join(text.split())

    text=text.strip()

    text = text.lower()
    text = text.split()

    #print text
    modified_text=''
    for i in range(len(text)):
        w=text[i]
        if w == "" or is_ascii(w) == False or w.isdigit() == True or w in stopwords.words('english'):
            continue
        if modified_text=='':
            modified_text+=w
        else:
            modified_text+=" "+w

    #print modified_text
    return modified_text
'''


def Clean_Text(text):
    text = " " + text
    text = re.sub(r'http\S+', '', text)
    text = text.replace("&amp;", "")
    text = text.replace('’', '\'')
    text = text.replace("“", "\"")
    text = text.replace("”", "\"")
    text = text.replace("—", "-")
    text = text.replace(' RT ', '')
    text = text.replace(' rt ', '')
    remove = string.punctuation
    pattern = r"[{}]".format(remove)
    text = re.sub(pattern, " ", text)
    text = text.strip()
    text = text.lower()
    text = text.split()

    _text = ""

    for w in text:
        if w in glove:
            _text += " " + w

        else:
            splitted_w = wordninja.split(w)

            for sw in splitted_w:
                _text += " " + sw

    _text = _text.strip()

    return _text







def Read_Data():
    #ProcessGlove()
    with open("data.json", "r") as fi:
        all_data_points=json.load(fi)

    with open("metadata.json", "r") as fi:
        [tweet_id2text, tweet_id2author_label, tweet_id2label, tweet_id2mf_label, tweet_id2multi_label, tweet_id2issue, tweet_id2dop]=json.load(fi)


    for tweet_id in tweet_id2dop:
        tweet_id2dop[tweet_id]=tweet_id2dop[tweet_id].split(" ")[0]


    with open("entity2adjacent_entities.pickle", "rb") as in_file:
        entity2adjacent_entities=pickle.load(in_file)

    revised_data_points=[]

    #[tw, tweet_id2text[tw], phrase, str(phrase2label[phrase]), str(phrase2prediction[phrase]), str(phrase2svo[phrase])]


    tweet_id2text={}
    verb_id2text={}
    entity_id2text={}
    encoded_entity2actual_entity={}
    actual_entity2encoded_entity=defaultdict(set)
    tweet_id2count={}
    entity2label = defaultdict(set)

    counter=0

    for d in all_data_points:


        if d[5]!="None":
            svo=d[5].split("_")[0]
            verb=d[5].split("_")[1]

            if verb not in care_dict+harm_dict:
                verb = "None"
            else:
                verb = "verb" + str(counter) + "\t" + verb
        else:
            svo = "None"
            verb = "None"

        if d[0] in tweet_id2count:
            tweet_id2count[d[0]]=tweet_id2count[d[0]]+1
        else:
            tweet_id2count[d[0]] = 1

        data=[d[0], d[1], "entity"+str(counter)+"\t"+d[2], d[3], d[4], svo, verb, str(d[6])]

        encoded_entity2actual_entity["entity"+str(counter)+"\t"+d[2]]=d[2]
        actual_entity2encoded_entity[d[2]].add("entity"+str(counter)+"\t"+d[2])
        entity2label[d[2]].add(d[3])

        tweet_id2text[data[0]] = data[1]

        revised_data_points.append(data)

        counter+=1


    all_tweet_ids=[d[0] for d in revised_data_points]
    all_phrases=[d[2] for d in revised_data_points]
    all_labels=[d[3] for d in revised_data_points]
    all_svos=[d[5] for d in revised_data_points]
    all_verbs=[d[6] for d in revised_data_points]
    all_political_labels = [str(d[7]) for d in revised_data_points]


    unique_tweet_ids=set(all_tweet_ids)
    unique_phrases=set(all_phrases)
    unique_labels=set(all_labels)
    unique_svos=set(all_svos)-set(['None'])
    unique_verbs=set(all_verbs)-set(['None'])
    unique_political_labels=set(all_political_labels)

    print unique_political_labels, unique_svos

    id2name={}
    name2id={}

    id=0
    political_label_start = id
    for label in unique_political_labels:
        id2name[id] = label
        name2id[label] = id
        id += 1
    political_label_end = id - 1

    id=10
    label_start=id
    for label in unique_labels:
        id2name[id]=label
        name2id[label]=id
        id+=1
    label_end=id-1

    id = 100
    svo_start = id
    for svo in unique_svos:
        id2name[id] = svo
        name2id[svo] = id
        id += 1
    svo_end = id - 1

    id=1000
    tweet_start=id
    for tw in unique_tweet_ids:
        id2name[id]=tw
        name2id[tw]=id
        id+=1
    tweet_end=id-1


    id = 1000000
    verb_start = id
    for vb in unique_verbs:
        if vb in name2id:
            print "verb: ", vb
        id2name[id] = vb
        name2id[vb] = id
        verb_id2text[id]=vb
        id += 1
    verb_end = id - 1

    id = 100000
    phrase_start = id
    for ph in unique_phrases:
        '''
        if ph in name2id:
            ph="entity "+ph
        '''
        id2name[id] = ph
        name2id[ph] = id
        entity_id2text[id] = ph

        id += 1
    phrase_end = id - 1

    f=open("all_entities.txt", "w")
    for entity in entity_id2text:
        f.write(entity_id2text[entity]+"\n")
    f.close()

    f=open("all_verbs.txt", "w")
    for verb in verb_id2text:
        f.write(verb_id2text[verb] + "\n")
    f.close()



    #"has_entity.txt"



    f1 = open("has_entity.txt", "w")
    f2 = open("has_role.txt", "w")
    f3 = open("is_seed.txt", "w")
    f4 = open("has_srl.txt", "w")
    f5 = open("has_ideology.txt", "w")
    f6 = open("in_instance.txt", "w")
    for d in revised_data_points:
        '''
        if name2id[d[2]] in range(verb_start, verb_end+1):
            d[2]="entity "+d[2]
        '''
        f1.write(str(name2id[d[0]])+"\t"+str(name2id[d[2]])+"\n")
        f2.write(str(name2id[d[0]])+"\t"+str(name2id[d[2]])+"\t"+str(name2id[d[3]])+"\n")
        if d[4]!="None":
            f3.write(str(name2id[d[0]]) + "\t" + str(name2id[d[2]]) + "\t" + str(name2id[d[4]]) + "\n")
        if d[6]!="None":
            '''
            if d[6] not in care_dict+harm_dict:
                continue
            '''
            f4.write(str(name2id[d[0]]) + "\t" + str(name2id[d[2]]) + "\t" + str(name2id[d[6]]) + "\t" + str(name2id[d[5]]) + "\n")

        f5.write(str(name2id[d[0]]) + "\t" + str(name2id[d[7]]) + "\n")

        f6.write(str(name2id[d[0]]) + "\t" + str(100000000) + "\n")

    f1.close()
    f2.close()
    f3.close()
    f4.close()
    f5.close()
    f6.close()


    #f7=open("is_same_entity.txt", "w")

    same_entities=[]
    for entity in actual_entity2encoded_entity:
        encoded_entities=actual_entity2encoded_entity[entity]

        if len(encoded_entities)==1:
            continue


        for e1 in encoded_entities:
            for e2 in encoded_entities:
                if e1==e2:
                    continue
                #f7.write(str(name2id[e1])+"\t"+str(name2id[e2])+"\n")
                #same_entities.add(sorted((name2id[e1], name2id[e2])))
                pair=sorted([name2id[e1], name2id[e2]])
                if pair not in same_entities:
                    same_entities.append(pair)

                #print encoded_entity2actual_entity[e1], entity2label[encoded_entity2actual_entity[e1]], "\n", encoded_entity2actual_entity[e2], entity2label[encoded_entity2actual_entity[e2]], "\n"

    mismatch_count=0
    for entity in entity2adjacent_entities:
        encoded_entities=actual_entity2encoded_entity[entity]
        adjacent_entities=entity2adjacent_entities[entity]

        adjacent_encoded_entities=set()

        for entity in adjacent_entities:
            adjacent_encoded_entities |= actual_entity2encoded_entity[entity]


        for e1 in encoded_entities:
            for e2 in adjacent_encoded_entities:
                if e1==e2:
                    continue

                #f7.write(str(name2id[e1])+"\t"+str(name2id[e2])+"\n")
                #same_entities.add(sorted((name2id[e1], name2id[e2])))
                pair = sorted([name2id[e1], name2id[e2]])
                if pair not in same_entities:
                    same_entities.append(pair)

                if len(entity2label[encoded_entity2actual_entity[e1]])==1 and len(entity2label[encoded_entity2actual_entity[e2]])==1 and len(entity2label[encoded_entity2actual_entity[e1]] & entity2label[encoded_entity2actual_entity[e2]])!=1:
                    mismatch_count+=1
                    print encoded_entity2actual_entity[e1], entity2label[encoded_entity2actual_entity[e1]], "\n", encoded_entity2actual_entity[e2], entity2label[encoded_entity2actual_entity[e2]], "\n"

    #f7.close()
    print "same entities: ", len(same_entities), mismatch_count

    #return

    f7 = open("is_same_entity.txt", "w")
    for p in same_entities:
        #f7.write(str(p[0])+" "+id2name[p[0]]+"\t"+str(p[1])+" "+id2name[p[1]]+"\n")
        f7.write(str(p[0]) + "\t" + str(p[1]) + "\n")
    f7.close()

    f8 = open("is_same_topic.txt", "w")
    for t1 in tweet_id2issue:
        for t2 in tweet_id2issue:
            if t1==t2 or t1 not in name2id or t2 not in name2id:
                continue

            if tweet_id2issue[t1]==tweet_id2issue[t2]:
                f8.write(str(name2id[t1])+"\t"+str(name2id[t2])+"\n")
    f8.close()

    f9 = open("is_same_event.txt", "w")
    for t1 in tweet_id2dop:
        for t2 in tweet_id2dop:
            if t1==t2 or t1 not in name2id or t2 not in name2id:
                continue

            if tweet_id2dop[t1] == tweet_id2dop[t2]:
                f9.write(str(name2id[t1]) + "\t" + str(name2id[t2]) + "\n")
    f9.close()

    f10 = open("is_feasible.txt", "w")
    for tweet_id in tweet_id2count:
        if tweet_id2count[tweet_id]<=3:
            f10.write(str(name2id[tweet_id])+"\t"+str(500000000)+"\n")
    f10.close()


    f11=open("is_care_verb.txt", "w")
    for v in range(verb_start, verb_end+1):
        if id2name[v].split("\t")[1] in care_dict:
            f11.write(str(v)+"\n")
    f11.close()

    f12 = open("is_harm_verb.txt", "w")
    for v in range(verb_start, verb_end + 1):
        if id2name[v].split("\t")[1] in harm_dict:
            f12.write(str(v) + "\n")
    f12.close()



    f6=open("role.txt", "w")
    #f7=open("has_role.txt", "w")
    for r in range(label_start, label_end+1):
        f6.write(str(r)+"\n")
        #f7.write(str(r)+"\n")
    f6.close()
    #f7.close()


    srl_id2one_hot={}
    for svo in unique_svos:
        onehot=[0 for i in range(len(unique_svos))]
        #print name2id[svo], svo_start
        onehot[name2id[svo]-svo_start]=1
        srl_id2one_hot[name2id[svo]]=onehot

    ideology_id2one_hot={}
    for ideology in all_political_labels:
        onehot = [0 for i in range(len(unique_political_labels))]
        onehot[name2id[ideology] - political_label_start] = 1
        ideology_id2one_hot[name2id[ideology]] = onehot

    print srl_id2one_hot
    print ideology_id2one_hot


    f=open("mapping.txt", "w")
    for id in id2name:
        f.write(str(id)+"\t"+id2name[id]+"\n")
    f.close()

    #return

    tweet_id2tokens, entity_id2tokens, verb_id2token,\
    word_dict, vocab_size, word_emb_size, word2idx = Tokenize_sentences(tweet_id2text, entity_id2text, verb_id2text, name2id, id2name)

    with open("drail_data.pickle", "wb") as out_file:
        pickle.dump([tweet_id2tokens, entity_id2tokens, verb_id2token,\
                   ideology_id2one_hot, srl_id2one_hot,\
                   word_dict, vocab_size, word_emb_size, word2idx], out_file)


    print len(entity_id2tokens), phrase_end, phrase_start

    '''
    for phrase in range(phrase_start, phrase_end+1):
        if phrase not in entity_id2tokens:
            print "phrase: ", phrase
    '''

    #print entity_id2tokens[str(1000131)], phrase_start, phrase_end

    '''
    for eid in entity_id2tokens:
        print eid
    '''

    print vocab_size, word_emb_size

    for tw in tweet_id2tokens:
        print tweet_id2tokens[tw]
        break

    for e in entity_id2tokens:
        print entity_id2tokens[e]
        break

    for v in verb_id2token:
        print verb_id2token[v]
        break

    for w in word2idx:
        print w, word2idx[w]
        break


def Tokenize_sentences(tweet_id2text, entity_id2text, verb_id2text, name2id, id2name):

    tweet_id2tokens={}
    entity_id2tokens={}
    verb_id2token={}

    word2idx={}
    idx2word={}

    word2idx['<pad>'] = len(word2idx)
    idx2word[len(idx2word)] = '<pad>'

    for tw in tweet_id2text:
        tokenized_text=[]
        text = Clean_Text(tweet_id2text[tw])
        print text
        text = text.split()
        for w in text:

            if w!="" and w not in word2idx and is_ascii(w):
                word2idx[w]=len(word2idx)
                idx2word[len(idx2word)]=w

            if w in word2idx:
                tokenized_text.append(word2idx[w])

        if len(tokenized_text)==0:
            print "Length Zero Text"
            #tokenized_text.append(word2idx['<pad>'])

        tweet_id2tokens[name2id[tw]]=tokenized_text

    for et in entity_id2text:
        tokenized_text = []
        text = Clean_Text(entity_id2text[et].split("\t")[1])
        print text
        text = text.split()
        for w in text:

            if w != "" and w not in word2idx and is_ascii(w):
                word2idx[w] = len(word2idx)
                idx2word[len(idx2word)] = w

            if w in word2idx:
                tokenized_text.append(word2idx[w])

        if len(tokenized_text) == 0:
            print "Length Zero Entity"
            #tokenized_text.append(word2idx['<pad>'])

        entity_id2tokens[et] = tokenized_text


    for vb in verb_id2text:
        tokenized_text = []
        print verb_id2text[vb]
        text = verb_id2text[vb].split("\t")[1]
        print text
        text = text.split()
        print text
        for w in text:

            if w != "" and w not in word2idx and is_ascii(w):
                word2idx[w] = len(word2idx)
                idx2word[len(idx2word)] = w

            if w in word2idx:
                tokenized_text.append(word2idx[w])

        if len(tokenized_text) == 0:
            print "Length Zero Verb"
            #tokenized_text.append(word2idx['<pad>'])

        verb_id2token[vb] = tokenized_text[0]


    print "Tokenization complete!"
    #ProcessGlove()

    word_dict={}
    words_found=0
    not_found=[]

    for word in word2idx:
        try:
            word_dict[word] = glove[word]
            words_found += 1
        except KeyError:
            not_found.append(word)
            if word2idx[word]==0:
                word_dict[word] = np.random.normal(scale=0, size=(300,))
            else:
                word_dict[word] = np.random.normal(scale=1/float(300), size=(300,))

    print "found: ", words_found, "not-found: ", len(not_found)
    print not_found

    return tweet_id2tokens, entity_id2tokens, verb_id2token, word_dict, len(word2idx), len(word_dict['<pad>']), word2idx






def ProcessGlove():
    global glove
    print "Loading Glove Model"
    f = open("../../Community_Detection/Resources/glove.6B/glove.6B.300d.txt", 'r')
    for line in f:
        splitLine = line.split()
        word = splitLine[0]
        embedding = np.array([float(val) for val in splitLine[1:]])
        glove[word] = embedding
    print "Done.", len(glove), " words loaded!"
















if __name__ == '__main__':
    ProcessGlove()
    Break_Words()
    #exit()
    Read_Data()




