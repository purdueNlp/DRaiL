# -*- coding: utf-8 -*-
import os
import argparse
import random
import json
import logging.config
from copy import deepcopy

import numpy as np
import torch
from sklearn.metrics import *
from tensorboardX import SummaryWriter

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner
from drail.learn.evaluator import EvaluatorBase


def parse_arguments():
    parser = argparse.ArgumentParser(description="EventCausalityPlus DRaiL")
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('-f', '--fedir', help='fe directory', dest='fedir', type=str, required=True)
    parser.add_argument('-n', '--netdir', help='net directory', dest='nedir', type=str, required=True)
    parser.add_argument('-m', help='mode: [global|local|joint]', dest='mode', type=str, default='local')
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--lrate", help="learning rate", type=float, default=2e-5)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true', default=False)
    parser.add_argument('-p', '--pkldir', help='pkl directory', dest='pkldir', type=str, default=None)
    parser.add_argument("--constraints", help="use constraints", default=False, action="store_true")
    parser.add_argument('--randomized', help='randomized inference on training', dest='rand_inf', default=False, action='store_true')
    parser.add_argument('--ad3', help='use ad3 solver rather than ilp', dest='ad3', default=False, action='store_true')
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--seed', help='random seed', type=int, default=1234)
    parser.add_argument('--k', help='k for k-folds', type=int, default=5)
    parser.add_argument('--patience', help='patience for global learner', type=int, default=3)
    parser.add_argument('--f1_avg', help='averaging type for F1Evaluator', type=str, default="micro", choices=['macro', 'micro'])
    # parser.add_argument('--dev_metric', help='metric for development: [f1|temporal]', default='f1', type=str, choices=['f1', 'temporal'])
    args = parser.parse_args()
    return args


class F1Evaluator(EvaluatorBase):
    def __init__(self, ignored_classes=[5, 6], average='micro'):
        # ignore 5 (none), 6 (vague)
        super(F1Evaluator, self).__init__()
        self.ignored_classes = ignored_classes
        self.average = average
        logger.info('using F1 Evaluator')

    def _measure(self, y_preds, y_golds, ids, verbose=False):
        if isinstance(y_preds, torch.Tensor) and y_preds.is_cuda:
            preds = y_preds.cpu()
        else:
            preds = y_preds
        valid_classes = sorted(list(set(y_golds) - set(self.ignored_classes)))
        prec, recall, f1, sup = precision_recall_fscore_support(y_golds, preds, labels=valid_classes, average=self.average)
        if verbose:
            logger.info('F1Evaluator: prec={}, recall={}, f1={}, sup={}'.format(prec, recall, f1, sup))
        return prec, recall, f1

    def measure(self, y_preds, y_golds, ids, verbose=False):
        prec, recall, f1 = self._measure(y_preds, y_golds, ids, verbose)
        return f1


class TemporalAwarenessEvaluator(EvaluatorBase):
    def __init__(self, gold_closure_f, gold_reduced_f):
        super(TemporalAwarenessEvaluator, self).__init__()
        self.gold_closure = self.load_graph(gold_closure_f)
        self.gold_reduced = self.load_graph(gold_reduced_f)
        self.n_gold_closure = self.count_relations(self.gold_closure)
        logger.info('using TemporalAwareness Evaluator')

    def inverse_rtype(self, rtype):
        inv_dict = {
                'before': 'after',
                'after': 'before',
                'includes': 'is_included',
                'is_included': 'includes',
                'simultaneous': 'simultaneous',
                'vague': 'vague',
                # 'causes': 'caused_by',
                # 'caused_by': 'causes'
                }
        return inv_dict[rtype]

    def add_symmetricity(self, g):
        new_g = deepcopy(g)
        for doc_id, doc in g.items():
            for (e1, e2), rtype in doc.items():
                if (e2, e1) in new_g[doc_id] and rtype != 'none' and new_g[doc_id][(e2, e1)] in ['none', 'vague']:
                    new_g[doc_id][(e2, e1)] = self.inverse_rtype(rtype)
        return new_g
    
    def add_transitivity(self, g):
        new_g = deepcopy(g)
        i_round = 0
        all_triplets = [(doc_id, (e1, e2)) for doc_id, doc in g.items() for (e1, e2), _ in doc.items()]

        while True:
            # logger.info('add_transitivity: round {}'.format(i_round))
            updated_ep3s = []
            for doc_id, ep1 in all_triplets:
                for doc_id2, ep2 in all_triplets:
                    if doc_id != doc_id2:
                        continue
                    if ep1 == ep2:
                        continue
                    if ep1[0] == ep2[1] and ep1[1] == ep2[0]:
                        continue
                    r1, r2 = new_g[doc_id][ep1], new_g[doc_id][ep2]
                    if r1 == 'none' or r2 == 'none':
                        continue

                    ep3 = (ep1[0], ep2[1])
                    if ep1[1] == ep2[0] and ep3 in new_g[doc_id]:
                        if new_g[doc_id][ep3] == 'none' or new_g[doc_id][ep3] == 'vague':
                            if r1 == r2 or r2 == 'simultaneous':
                                new_r3 = str(r1)
                            else:
                                new_r3 = 'vague'
                            if new_g[doc_id][ep3] != new_r3:
                                new_g[doc_id][ep3] = new_r3
                                updated_ep3s.append((doc_id, ep3))
                                # logger.debug("ep1={}, ep2={}, ep3={}, r1={}, r2={}, r3={}".format(ep1, ep2, ep3, r1, r2, new_g[doc_id][ep3]))
                        else: # we don't update relations that are trustworthy
                            # logger.warning("r3 not none or vague: r1={}, r2={}, r3={}".format(r1, r2, new_g[doc_id][ep3]))
                            pass
            if len(updated_ep3s) == 0:
                break
            # check symmetricity for ep3s
            for doc_id, ep3 in updated_ep3s:
                rep3 = (ep3[1], ep3[0])
                if rep3 not in new_g[doc_id]:
                    continue
                if new_g[doc_id][ep3] == 'vague' and new_g[doc_id][rep3] not in ['none', 'vague']:
                    # recover vague from symmetricity
                    if rep3 in new_g[doc_id]:
                        new_g[doc_id][ep3] = str(self.inverse_rtype(new_g[doc_id][rep3]))
                        # logger.debug('transitivity recover from symmetricity: ep3={}, rtype={}'.format(ep3, new_g[doc_id][ep3]))
                else: # not vague
                    # update the other pair
                    if new_g[doc_id][rep3] in ['none', 'vague']:
                        new_g[doc_id][rep3] = str(self.inverse_rtype(new_g[doc_id][ep3]))
                        # logger.debug('transitivity update by symmetricity: ep3={}, rtype={}'.format(rep3, new_g[doc_id][rep3]))
            i_round += 1
        # logger.info('add_transitivity: final round {}'.format(i_round))
        return new_g

    def _all_measure(self, g, with_symmetricity=False, with_transitivity=False, verbose=False):
        g_reduced = g
        if not with_symmetricity:
            g_reduced = self.add_symmetricity(g)

        g_closure = g
        if not with_transitivity:
            g_closure = self.add_transitivity(g)

        precision_matched = self.count_matches(g_reduced, self.gold_closure)
        recall_matched = self.count_matches(g_closure, self.gold_reduced)
        n_g_reduced = self.count_relations(g_reduced)

        global_n_g_reduced, global_n_gold_closure = 0, 0
        global_precision_matched, global_recall_matched = 0, 0
        precisions, recalls, f1s = [], [], []
        for doc_id, prec_m in precision_matched.items():
            prec = float(prec_m) / n_g_reduced[doc_id] if n_g_reduced[doc_id] > 0 else 0.0
            recall = float(recall_matched[doc_id]) / self.n_gold_closure[doc_id] if self.n_gold_closure[doc_id] > 0 else 0.0
            f1 = (2.0 * prec * recall) / (prec + recall) if prec+recall > 0 else 0.0
            if verbose:
                logger.info("{}: TemporalAwareness: prec={}, recall={}, f1={}".format(doc_id, prec, recall, f1))

            global_n_gold_closure += self.n_gold_closure[doc_id]
            global_n_g_reduced += n_g_reduced[doc_id]
            global_precision_matched += prec_m
            global_recall_matched += recall_matched[doc_id]
            precisions.append(prec)
            recalls.append(recall)
            f1s.append(f1)

        doc_avg_precision = sum(precisions) / len(precisions)
        doc_avg_recall = sum(recalls) / len(recalls)
        doc_avg_f1 = sum(f1s) / len(f1s)
        if verbose:
            logger.info("DOC_AVG: TemporalAwareness: prec={}, recall={}, f1={}".format(doc_avg_precision, doc_avg_recall, doc_avg_f1))

        global_precision = float(global_precision_matched) / global_n_g_reduced if global_n_g_reduced > 0 else 0.0
        global_recall = float(global_recall_matched) / global_n_gold_closure if global_n_gold_closure > 0 else 0.0
        global_f1 = (2.0 * global_precision * global_recall) / (global_precision + global_recall) if global_precision+global_recall > 0 else 0.0
        if verbose:
            logger.info("GLOBAL: TemporalAwareness: prec={}, recall={}, f1={}".format(global_precision, global_recall, global_f1))
        return doc_avg_precision, doc_avg_recall, doc_avg_f1
    
    def _measure(self, y_preds, y_golds, ids, with_symmetricity=False, with_transitivity=False, verbose=False):
        g = self.build_graph(y_preds, ids)
        return self._all_measure(g, with_symmetricity, with_transitivity, verbose)

    def measure(self, y_preds, y_golds, ids, with_symmetricity=False, with_transitivity=False, verbose=False):
        # we are not using y_golds, since we have a local copy
        g = self.build_graph(y_preds, ids)
        prec, recall, f1 = self._all_measure(g, with_symmetricity, with_transitivity, verbose)
        return f1

    def count_relations(self, g):
        cnts = {}
        for doc_id, doc in g.items():
            cnts[doc_id] = 0
            for (e1, e2), rtype in doc.items():
                if rtype in ['vague', 'none']:
                    continue
                cnts[doc_id] += 1
        return cnts

    def count_matches(self, g, gold):
        cnts = {}
        for doc_id, doc in g.items():
            cnts[doc_id] = 0
            for (e1, e2), rtype in doc.items():
                if rtype in ['vague', 'none']:
                    continue
                gold_rtype = gold[doc_id][(e1, e2)]
                if rtype == gold_rtype:
                    cnts[doc_id] += 1
        return cnts

    def load_graph(self, fpath):
        graph = {}
        with open(fpath, 'r') as fr:
            for line in fr:
                line = line.rstrip('\n')
                sp = line.split('\t')
                assert len(sp) == 4
                doc_id = int(sp[0])
                eid1 = int(sp[1])
                eid2 = int(sp[2])
                rtype = sp[3]
                if doc_id not in graph:
                    graph[doc_id] = {}
                graph[doc_id][(eid1, eid2)] = rtype
        return graph
    
    def build_graph(self, ys, ids):
        graph = {}
        for y, args in zip(ys, ids):
            sp = args.split(',')
            doc_id = int(sp[0])
            eid1 = int(sp[1])
            eid2 = int(sp[2])
            if doc_id not in graph:
                graph[doc_id] = {}
            graph[doc_id][(eid1, eid2)] = y
        return graph


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if args.gpu_index is not None:
        torch.cuda.manual_seed_all(seed)


def train_folds(folds, avoid):
    ret = []
    for j in range(0, len(folds)):
        if j not in avoid:
            ret += folds[j]
    return ret


def load_test_doc_ids(fpath):
    doc_ids = []
    with open(fpath, 'r') as fr:
        for line in fr:
            line = line.strip()
            doc_ids.append(int(line))
    return doc_ids


def show_fold_results(y_gold, y_pred, labels, predicate_prefix):
    logger.info('{}'.format(predicate_prefix))
    results = {}

    results['all_labels'] = {}
    results['all_labels']['acc'] = accuracy_score(y_gold, y_pred)
    results['all_labels']['macro-prec'] = precision_score(y_gold, y_pred, average='macro', labels=labels)
    results['all_labels']['macro-recall'] = recall_score(y_gold, y_pred, average='macro', labels=labels)
    results['all_labels']['macro-f1'] = f1_score(y_gold, y_pred, average='macro', labels=labels)
    results['all_labels']['micro-prec'] = precision_score(y_gold, y_pred, average='micro', labels=labels)
    results['all_labels']['micro-recall'] = recall_score(y_gold, y_pred, average='micro', labels=labels)
    results['all_labels']['micro-f1'] = f1_score(y_gold, y_pred, average='micro', labels=labels)
    logger.info("TEST (all labels, {} nodes)".format(len(y_gold)))
    # logger.info('\n'+classification_report(y_gold, y_pred, digits=4))

    # exclude none
    results['exclude_none'] = {}
    idxs = [i for i in range(len(y_gold)) if y_gold[i] != 'none']
    y_gold_no_n = [y_gold[i] for i in idxs]
    y_pred_no_n = [y_pred[i] for i in idxs]
    labels_no_n = deepcopy(labels)
    if 'none' in labels_no_n:
        labels_no_n.remove('none')
    results['exclude_none']['acc'] = accuracy_score(y_gold_no_n, y_pred_no_n)
    results['exclude_none']['macro-prec'] = precision_score(y_gold_no_n, y_pred_no_n, average='macro', labels=labels_no_n)
    results['exclude_none']['macro-recall'] = recall_score(y_gold_no_n, y_pred_no_n, average='macro', labels=labels_no_n)
    results['exclude_none']['macro-f1'] = f1_score(y_gold_no_n, y_pred_no_n, average='macro', labels=labels_no_n)
    results['exclude_none']['micro-prec'] = precision_score(y_gold_no_n, y_pred_no_n, average='micro', labels=labels_no_n)
    results['exclude_none']['micro-recall'] = recall_score(y_gold_no_n, y_pred_no_n, average='micro', labels=labels_no_n)
    results['exclude_none']['micro-f1'] = f1_score(y_gold_no_n, y_pred_no_n, average='micro', labels=labels_no_n)
    logger.info("TEST (exclude none): {} nodes".format(len(y_gold_no_n)))
    # logger.info('\n'+classification_report(y_gold_no_n, y_pred_no_n, digits=4))

    # exclude none and vague
    results['exclude_none_vague'] = {}
    idxs = [i for i in range(len(y_gold)) if y_gold[i] != 'none' and y_gold[i] != 'vague']
    y_gold_no_nv = [y_gold[i] for i in idxs]
    y_pred_no_nv = [y_pred[i] for i in idxs]
    labels_no_nv = deepcopy(labels)
    if 'none' in labels_no_nv:
        labels_no_nv.remove('none')
    if 'vague' in labels_no_nv:
        labels_no_nv.remove('vague')
    results['exclude_none_vague']['acc'] = accuracy_score(y_gold_no_nv, y_pred_no_nv)
    results['exclude_none_vague']['macro-prec'] = precision_score(y_gold_no_nv, y_pred_no_nv, average='macro', labels=labels_no_nv)
    results['exclude_none_vague']['macro-recall'] = recall_score(y_gold_no_nv, y_pred_no_nv, average='macro', labels=labels_no_nv)
    results['exclude_none_vague']['macro-f1'] = f1_score(y_gold_no_nv, y_pred_no_nv, average='macro', labels=labels_no_nv)
    results['exclude_none_vague']['micro-prec'] = precision_score(y_gold_no_nv, y_pred_no_nv, average='micro', labels=labels_no_nv)
    results['exclude_none_vague']['micro-recall'] = recall_score(y_gold_no_nv, y_pred_no_nv, average='micro', labels=labels_no_nv)
    results['exclude_none_vague']['micro-f1'] = f1_score(y_gold_no_nv, y_pred_no_nv, average='micro', labels=labels_no_nv)
    logger.info("TEST (exclude none and vague): {} nodes".format(len(y_gold_no_nv)))
    # logger.info('\n'+classification_report(y_gold_no_nv, y_pred_no_nv, digits=4))

    logger.info("{}".format(json.dumps(results, indent=2)))
    return results


def main():
    # Fixed resources
    TEMPORAL_GOLD_CLOSURE_F = os.path.join(args.dir, "has_temporal_relation_full.txt")
    TEMPORAL_GOLD_REDUCED_F = os.path.join(args.dir, "has_temporal_relation_symmetricity.txt")
    EVENTS_F = os.path.join(args.dir, "event_bert_inputs.json")
    PAIRWISE_F = os.path.join(args.dir, "event_pairwise_inputs.pkl")
    TEST_DOC_IDXS_F = os.path.join(args.dir, "test_doc_idxs.txt")
    fpath = os.path.join(args.dir, 'train_kfolds.json')
    FOLDS = json.load(open(fpath))
    optimizer = "AdamW"

    # seed
    set_seed(args.seed)

    temporal_evaluator = TemporalAwarenessEvaluator(TEMPORAL_GOLD_CLOSURE_F, TEMPORAL_GOLD_REDUCED_F)
    evaluator = F1Evaluator(average=args.f1_avg)
    # evaluator = F1Evaluator() if args.dev_metric=='f1' \
            # else temporal_evaluator

    inference_solver = "ILP" if not args.ad3 else "AD3"
    logger.info("inference solver: {}".format(inference_solver))
    logger.info("randomized: {}".format(args.rand_inf))

    # Select what gpu to use
    if args.gpu_index is not None:
        torch.cuda.set_device(args.gpu_index)

    if args.mode == "global":
        logger.info("learning rate={}, lossfn={}".format(args.lrate, args.lossfn))
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index, rand_inf=args.rand_inf, ad3=args.ad3, loss_fn=args.lossfn)
    else:
        learner=LocalLearner()

    learner.compile_rules(args.rules)
    db=learner.create_dataset(args.dir)

    test_docs = load_test_doc_ids(TEST_DOC_IDXS_F)
    if args.debug:
        test_docs = test_docs[:2]
    ta_precisions, ta_recalls, ta_f1s = [], [], []
    all_results, temporal_results, causal_results = [], [], []
    tb_log_dir = os.path.join(args.savedir, 'tensorboard_log')
    tb_writer = SummaryWriter(log_dir=tb_log_dir, comment='_{}'.format(args.savedir))
    for i_fold in range(args.k):
        logger.info("Fold {}".format(i_fold))
        dev_docs = FOLDS[i_fold]
        train_docs = train_folds(FOLDS, [i_fold])

        if args.debug:
            dev_docs = dev_docs[:2]
            train_docs = train_docs[:2]

        db.add_filters(filters=[
            ("HasEvent", "isTrain", "docId_1", train_docs),
            ("HasEvent", "isDev", "docId_1", dev_docs),
            ("HasEvent", "isTest", "docId_1", test_docs),
            ("HasEvent", "isDummy", "docId_1", train_docs[:1])
        ])

        logger.info("{} train docs, {} dev docs, {} test docs".format(len(train_docs), len(dev_docs), len(test_docs)))

        learner.build_feature_extractors(db,
                                         data_f=EVENTS_F,
                                         pairwise_f=PAIRWISE_F,
                                         debug=args.debug,
                                         femodule_path=args.fedir,
                                         filters=[("HasEvent", "isDummy", 1)])
        
        if not os.path.isdir(args.savedir):
            os.mkdir(args.savedir)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i_fold)))
        learner.build_models(db, args.config, netmodules_path=args.nedir)

        pklpath = None
        if args.pkldir is not None:
            pklpath = os.path.join(args.pkldir, "f{0}".format(i_fold))

        if args.mode == "global":
            learner.extract_data(
                    db,
                    train_filters=[("HasEvent", "isTrain", 1)],
                    dev_filters=[("HasEvent", "isDev", 1)],
                    test_filters=[("HasEvent", "isTest", 1)],
                    extract_train=not args.infer_only,
                    extract_dev=not args.infer_only,
                    extract_test=True,
                    pickledir=pklpath,
                    from_pickle=(args.pkldir is not None),
                    extract_constraints=args.constraints)

            # Passing accum_loss=True to accumulate gradients before backprop
            res, heads = learner.train(
                    db,
                    train_filters=[("HasEvent", "isTrain", 1)],
                    dev_filters=[("HasEvent", "isDev", 1)],
                    test_filters=[("HasEvent", "isTest", 1)],
                    opt_predicate=None,
                    loss_augmented_inference=args.delta,
                    continue_from_checkpoint=args.continue_from_checkpoint,
                    inference_only=args.infer_only,
                    scale_data=False,
                    weight_classes=True,
                    accum_loss=True,
                    patience=args.patience,
                    optimizer=optimizer,
                    evaluator=evaluator,
                    tb_writer=tb_writer,
                    tb_prefix='fold{}'.format(i_fold))
        else:
            if args.continue_from_checkpoint:
                learner.init_models(scale_data=False)

            if not args.infer_only:
                learner.train(db,
                            train_filters=[("HasEvent", "isTrain", 1)],
                            dev_filters=[("HasEvent", "isDev", 1)],
                            test_filters=[("HasEvent", "isTest", 1)],
                            scale_data=False,
                            optimizer=optimizer,
                            evaluator=evaluator,
                            tb_writer=tb_writer,
                            tb_prefix='fold{}'.format(i_fold))

            if not args.train_only:
                learner.extract_data(db, extract_test=True,
                            test_filters=[("HasEvent", "isTest", 1)],
                            pickledir=pklpath,
                            from_pickle=(args.pkldir is not None),
                            extract_constraints=args.constraints)
                res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
                #learner.drop_scores(db)
        if not args.train_only:
            all_y_gold, all_y_pred = [], []
            if 'HasTemporalRelation' in res.metrics:
                y_gold = res.metrics['HasTemporalRelation']['gold_data']
                y_pred = res.metrics['HasTemporalRelation']['pred_data']
                ids = res.metrics['HasTemporalRelation']['ids']
                all_y_gold += y_gold
                all_y_pred += y_pred

                logger.info("HasTemporalRelation")

                labels = list(set(y_gold))
                results = show_fold_results(y_gold, y_pred, labels, "HasTemporalRelation_inference_fold_{}".format(i_fold))
                temporal_results.append(results)

                ta_precision, ta_recall, ta_f1 = temporal_evaluator._measure(y_pred, y_gold, ids,
                        with_symmetricity=False, with_transitivity=False, verbose=True)
                # ta_precision, ta_recall, ta_f1 = temporal_evaluator._measure(y_pred, y_gold, ids,
                        # with_symmetricity=True, with_transitivity=True, verbose=True)

                ta_precisions.append(ta_precision)
                ta_recalls.append(ta_recall)
                ta_f1s.append(ta_f1)
                tb_writer.add_scalar('fold{}_test_precision_temporal_ilp'.format(i_fold), results['exclude_none_vague']['micro-prec'])
                tb_writer.add_scalar('fold{}_test_recall_temporal_ilp'.format(i_fold), results['exclude_none_vague']['micro-recall'])
                tb_writer.add_scalar('fold{}_test_f1_temporal_ilp'.format(i_fold), results['exclude_none_vague']['micro-f1'])

            if 'HasCausalRelation' in res.metrics:
                y_gold = res.metrics['HasCausalRelation']['gold_data']
                y_pred = res.metrics['HasCausalRelation']['pred_data']
                all_y_gold += y_gold
                all_y_pred += y_pred

                logger.info("HasCausalRelation")

                labels = list(set(y_gold))
                results = show_fold_results(y_gold, y_pred, labels, 'HasCausalRelation_inference_fold_{}'.format(i_fold))
                causal_results.append(results)

                tb_writer.add_scalar('fold{}_test_precision_causal_ilp'.format(i_fold), results['exclude_none_vague']['micro-prec'])
                tb_writer.add_scalar('fold{}_test_recall_causal_ilp'.format(i_fold), results['exclude_none_vague']['micro-recall'])
                tb_writer.add_scalar('fold{}_test_f1_causal_ilp'.format(i_fold), results['exclude_none_vague']['micro-f1'])

            if len(all_y_gold) > 0:
                logger.info("AllRelation")

                labels = list(set(all_y_gold))
                results = show_fold_results(all_y_gold, all_y_pred, labels, 'AllRelation_inference_fold_{}'.format(i_fold))
                all_results.append(results)

        if not args.infer_only and args.mode == 'global':
            # pure inference time during training
            logger.info('\ninference encoding time:\t{0}\n'.format(learner.train_metrics.metrics['encoding_time']) +\
                  'inference optimiz. time:\t{0}\n'.format(learner.train_metrics.metrics['solving_time']) +\
                  'total inference time:   \t{0}'.format(learner.train_metrics.total_time()))

            learner.reset_train_metrics()

        learner.reset_metrics()
        #exit()

        # TODO: remove
        if args.debug and i_fold >= 1:
            break

    def _show_avg_results(ress, rcat):
        logger.info("--------------------------------")
        logger.info('TEST AVG: {}'.format(rcat))

        label_cats = list(ress[0].keys())
        for lc in label_cats:
            logger.info('LABELS = {}'.format(lc))

            accs = [res[lc]['acc'] for res in ress]
            macro_precs = [res[lc]['macro-prec'] for res in ress]
            macro_recalls = [res[lc]['macro-recall'] for res in ress]
            macro_f1s = [res[lc]['macro-f1'] for res in ress]
            micro_precs = [res[lc]['micro-prec'] for res in ress]
            micro_recalls = [res[lc]['micro-recall'] for res in ress]
            micro_f1s = [res[lc]['micro-f1'] for res in ress]
            k = len(accs)
            avg_acc = sum(accs) / k
            avg_macro_prec, avg_macro_recall, avg_macro_f1 = sum(macro_precs)/k, sum(macro_recalls)/k,sum(macro_f1s)/k
            avg_micro_prec, avg_micro_recall, avg_micro_f1 = sum(micro_precs)/k, sum(micro_recalls)/k,sum(micro_f1s)/k
            logger.info("acc = {}".format(avg_acc))
            logger.info("MACRO: prec={}, recall={}, f1={}".format(avg_macro_prec, avg_macro_recall, avg_macro_f1))
            logger.info("MICRO: prec={}, recall={}, f1={}".format(avg_micro_prec, avg_micro_recall, avg_micro_f1))
            
            tb_writer.add_scalar('avg_test_precision_{}_ilp'.format(lc), avg_micro_prec)
            tb_writer.add_scalar('avg_test_recall_{}_ilp'.format(lc), avg_micro_recall)
            tb_writer.add_scalar('avg_test_f1_{}_ilp'.format(lc), avg_micro_f1)


    if len(temporal_results) > 0:
        _show_avg_results(temporal_results, 'TEMPORAL')
        logger.info('AVG TemporalAwareness: prec={}, recall={}, f1={}'.format(
            sum(ta_precisions) / len(ta_precisions),
            sum(ta_recalls) / len(ta_recalls),
            sum(ta_f1s) / len(ta_f1s)
            ))
    if len(causal_results) > 0:
        _show_avg_results(causal_results, 'CAUSAL')
    if len(all_results) > 0:
        _show_avg_results(all_results, 'ALL')
    tb_writer.close()


def get_root_logger(args, debug=True, log_fname=None):
    # set logger
    logger = logging.getLogger()
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(levelname)-6s[%(name)s][%(filename)s:%(lineno)d] %(message)s")
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)

    log_dir = './'
    if hasattr(args, 'savedir'):
        log_dir = args.savedir
        if not os.path.isdir(args.savedir):
            os.mkdir(args.savedir)

    fpath = os.path.join(log_dir, log_fname) if log_fname \
            else os.path.join(log_dir, 'log')

    fileHandler = logging.FileHandler(fpath)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)
    return logger


if __name__ == "__main__":
    args = parse_arguments() # put args here so it's global
    logger = get_root_logger(args)
    main()
