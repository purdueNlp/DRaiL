import re
import json
import random
import pickle
import logging

import numpy as np

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils


class EventCausalityPlus_ft(FeatureExtractor):
    def __init__(self, data_f, pairwise_f=None, debug=False):
        super(EventCausalityPlus_ft, self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.n_max_len = 128

        self.data_f = data_f
        self.pairwise_f = pairwise_f
        self.debug = debug

        # BERT
        self.CLS = 101
        self.SEP = 102

    def build(self):
        self.temporal_relation_idx = {
                'before': 0,
                'after': 1,
                'includes': 2,
                'is_included': 3,
                'simultaneous': 4,
                'vague': 5,
                'none': 5
                }
        self.causal_relation_idx = {
                'causes': 0,
                'caused_by': 1,
                'none': 2,
                }

        # Load posts
        with open(self.data_f) as fp:
            self.data = json.load(fp)
        if self.pairwise_f:
            self.pairwise_features = pickle.load(open(self.pairwise_f, 'rb'))
            k = next(iter(self.pairwise_features))
            self.pairwise_dim = len(self.pairwise_features[k])
        else:
            self.pairwise_features = None
            self.pairwise_dim = 0

    def get_pairwise_dim(self):
        return self.pairwise_dim

    def prepare_bert_inputs(self, event_id, pad_token_id=0):
        input_ids = self.data[str(event_id)]['input_ids'][:self.n_max_len-2]
        event_idxs = self.data[str(event_id)]['event_idxs']

        input_ids = [self.CLS] + input_ids + [self.SEP]
        input_mask = [1] * len(input_ids)
        if len(input_ids) < self.n_max_len:
            input_ids += [pad_token_id] * (self.n_max_len - len(input_ids))
            input_mask += [0] * (self.n_max_len - len(input_mask))

        token_type_ids = [0] * len(input_ids)

        # we have to make that sure the event_idxs won't exceed the max sequence length in preprocessing
        # we use 1 to indicate event location
        event_idxs = self.data[str(event_id)]['event_idxs']
        for idx in event_idxs:
            token_type_ids[idx] = 1

        return input_ids, input_mask, token_type_ids

    def bert_event_left(self, rule_grd):
        has_event = rule_grd.get_body_predicates("HasEvent")[0]
        doc_id, event_id, is_timex = has_event['arguments']
        input_ids, input_mask, token_type_ids = \
                self.prepare_bert_inputs(event_id, self.n_max_len)
        return (input_ids, input_mask, token_type_ids)

    def bert_event_right(self, rule_grd):
        has_event = rule_grd.get_body_predicates("HasEvent")[1]
        doc_id, event_id, is_timex = has_event['arguments']
        input_ids, input_mask, token_type_ids = \
                self.prepare_bert_inputs(event_id, self.n_max_len)
        return (input_ids, input_mask, token_type_ids)

    def prepare_pairwise_features(self, rule_grd):
        has_event = rule_grd.get_body_predicates("HasEvent")
        doc_id1, event_id1, is_timex1 = has_event[0]['arguments']
        doc_id2, event_id2, is_timex2 = has_event[1]['arguments']
        assert doc_id1 == doc_id2
        key = (str(doc_id1), event_id1, event_id2)
        return self.pairwise_features[key]

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasTemporalRelation':
            label = head['arguments'][3]
            return self.temporal_relation_idx[label]
        elif head['name'] == 'HasCausalRelation':
            label = head['arguments'][3]
            return self.causal_relation_idx[label]
        return None

    def label2index(self, head_name, label):
        if head_name == 'HasTemporalRelation':
            return self.temporal_relation_idx[label]
        elif head_name == 'HasCausalRelation':
            return self.causal_relation_idx[label]
        return None

    def is_label_valid(self, head_name, label):
        if head_name == 'HasTemporalRelation':
            return (label in self.temporal_relation_idx)
        elif head_name == 'HasCausalRelation':
            return (label in self.causal_relation_idx)
        return False
