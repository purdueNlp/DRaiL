# -*- coding: utf-8 -*-
import os
import argparse
import random
import json
import logging.config
from copy import deepcopy

import numpy as np
import torch
from sklearn.metrics import *

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner


def parse_arguments():
    parser = argparse.ArgumentParser(description="Pre-extract data for global learning")
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-f', '--fedir', help='fe directory', dest='fedir', type=str, required=True)
    parser.add_argument('-p', '--pkldir', help='pkl directory', dest='pkldir', type=str, required=True)
    parser.add_argument('--seed', help='random seed', type=int, default=1234)
    parser.add_argument('--k', help='k for k-folds', type=int, default=5)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    args = parser.parse_args()
    return args


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    # if args.gpu_index is not None:
        # torch.cuda.manual_seed_all(seed)


def train_folds(folds, avoid):
    ret = []
    for j in range(0, len(folds)):
        if j not in avoid:
            ret += folds[j]
    return ret


def load_test_doc_ids(fpath):
    doc_ids = []
    with open(fpath, 'r') as fr:
        for line in fr:
            line = line.strip()
            doc_ids.append(int(line))
    return doc_ids


def main():
    # Fixed resources
    EVENTS_F = os.path.join(args.dir, "event_bert_inputs.json")
    fpath = os.path.join(args.dir, 'train_kfolds.json')
    FOLDS = json.load(open(fpath))
    TEST_DOC_IDXS_F = os.path.join(args.dir, "test_doc_idxs.txt")
    PAIRWISE_F = os.path.join(args.dir, "event_pairwise_inputs.pkl")

    # seed
    set_seed(args.seed)

    # Select what gpu to use
    # learner=GlobalLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index, rand_inf=args.rand_inf, ad3=args.ad3, loss_fn=args.lossfn)
    learner=LocalLearner()

    learner.compile_rules(args.rules)
    db=learner.create_dataset(args.dir)

    test_docs = load_test_doc_ids(TEST_DOC_IDXS_F)
    for i_fold in range(args.k):
        logger.info("Fold {}".format(i_fold))

        dev_docs = FOLDS[i_fold]
        train_docs = train_folds(FOLDS, [i_fold])

        db.add_filters(filters=[
            ("HasEvent", "isTrain", "docId_1", train_docs),
            ("HasEvent", "isDev", "docId_1", dev_docs),
            ("HasEvent", "isTest", "docId_1", test_docs),
            ("HasEvent", "isDummy", "docId_1", train_docs[:1])
        ])

        logger.info("{} train docs, {} dev docs, {} test docs".format(len(train_docs), len(dev_docs), len(test_docs)))

        learner.build_feature_extractors(db,
                                         data_f=EVENTS_F,
                                         pairwise_f=PAIRWISE_F,
                                         femodule_path=args.fedir,
                                         filters=[("HasEvent", "isDummy", 1)])
        
        pkl_path = os.path.join(args.pkldir, 'f{}'.format(i_fold))
        if not os.path.isdir(pkl_path):
            os.makedirs(pkl_path)

        learner.extract_data(
                db,
                train_filters=[("HasEvent", "isTrain", 1)],
                dev_filters=[("HasEvent", "isDev", 1)],
                test_filters=[("HasEvent", "isTest", 1)],
                extract_train=True,
                extract_dev=True,
                extract_test=True,
                pickleit=True,
                pickledir=pkl_path)


def get_root_logger(args, debug=True, log_fname=None):
    # set logger
    logger = logging.getLogger()
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(levelname)-6s[%(name)s][%(filename)s:%(lineno)d] %(message)s")
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)

    # log_dir = './'
    # if hasattr(args, 'pkldir'):
        # log_dir = args.pkldir
        # if not os.path.isdir(args.pkldir):
            # os.mkdir(args.pkldir)

    # fpath = os.path.join(log_dir, log_fname) if log_fname \
            # else os.path.join(log_dir, 'log')

    # fileHandler = logging.FileHandler(fpath)
    # fileHandler.setFormatter(formatter)
    # logger.addHandler(fileHandler)
    return logger


if __name__ == "__main__":
    args = parse_arguments() # put args here so it's global
    logger = get_root_logger(args)
    main()
