import logging

import torch
import numpy as np
import torch.nn.functional as F
from transformers import BertModel, BertConfig

from drail.neuro.nn_model import NeuralNetworks


class BertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        bert_config = BertConfig.from_pretrained(self.config['bert_weight_name'])

        if "shared_encoder" in self.config:
            name = self.config["shared_encoder"]
            self.bert_model = shared_params[name]["bert"]
        else:
            self.bert_model = BertModel.from_pretrained(self.config['bert_weight_name'])
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        if "shared_output" in self.config:
            name = self.config["shared_output"]
            self.hidden2label = shared_params[name]["layer"]
        else:
            self.hidden2label = torch.nn.Linear(bert_config.hidden_size, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; token_type_ids = []

        import pdb; pdb.set_trace()
        assert len(x['input'][0]) == 1
        if len(x['input'][0]) == 1:
            for elem in x['input']:
                input_ids.append(elem[0][0])
                input_mask.append(elem[0][1])
                token_type_ids.append(elem[0][2])

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        token_type_ids = self._get_variable(self._get_long_tensor(token_type_ids))

        # forward
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=token_type_ids, position_ids=None, head_mask=None)
        # TODO: try pooled token embeddings
        pooled_output = outputs[1] # CLS embedding
        pooled_output = self.dropout(pooled_output)

        logits = self.hidden2label(pooled_output)

        # TODO: skip softmax for speed
        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
