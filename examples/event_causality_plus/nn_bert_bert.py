import logging

import torch
import numpy as np
import torch.nn.functional as F
from transformers import BertModel, BertConfig

from drail.neuro.nn_model import NeuralNetworks


class BertBertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertBertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim={}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        bert_config = BertConfig.from_pretrained("bert-base-uncased")

        name = self.config["shared_encoder"]
        if "shared_encoder" in self.config:
            name = self.config["shared_encoder"]
            self.bert_model = shared_params[name]["bert"]
        else:
            self.bert_model = BertModel.from_pretrained(self.config['bert_weight_name'])
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        dim = bert_config.hidden_size*2 + fe.get_pairwise_dim()
        self.logger.info('event_pair_dim = {}*2 + {} = {}'.format(bert_config.hidden_size, fe.get_pairwise_dim(), dim))
        self.hidden2hidden = torch.nn.Linear(dim, self.config["n_hidden"])

        self.logger.info('hidden = {}'.format(self.config['n_hidden']))
        self.hidden2label = torch.nn.Linear(self.config["n_hidden"], self.output_dim)

        self.logger.info('output_dim = {}'.format(self.output_dim))
        self.use_token_average_pooling = bool(self.config['use_token_average_pooling']) \
                if "use_token_average_pooling" in self.config else True
        self.logger.info("use_token_average_pooling={}".format(self.use_token_average_pooling))
        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2hidden = self.hidden2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def token_average_pooling(self, outputs, token_type_ids):
        # outputs[0]: tokens, (batch, sequence_len, 768)
        # outputs[1]: CLS, (batch_size, 768)
        pooled_outputs = None
        for toks, tyids  in zip(outputs[0], token_type_ids):
            trigger_idxs = torch.nonzero(tyids).view(-1)
            pooled = toks[trigger_idxs].mean(0).view(1, -1)
            if pooled_outputs is None:
                pooled_outputs = pooled
            else:
                pooled_outputs = torch.cat((pooled_outputs, pooled), 0)
        return pooled_outputs

    def forward(self, x):
        # Prepare input for bert
        input_ids_1 = []; input_mask_1 = []; token_type_ids_1 = []
        input_ids_2 = []; input_mask_2 = []; token_type_ids_2 = []
        pairwises = []
        for elem in x['input']:
            input_ids_1.append(elem[0][0])
            input_mask_1.append(elem[0][1])
            token_type_ids_1.append(elem[0][2])

            input_ids_2.append(elem[1][0])
            input_mask_2.append(elem[1][1])
            token_type_ids_2.append(elem[1][2])

            if len(elem) > 2:
                pairwises.append(elem[2])

        input_ids_1 = self._get_variable(self._get_long_tensor(input_ids_1))
        input_mask_1 = self._get_variable(self._get_long_tensor(input_mask_1))
        token_type_ids_1 = self._get_variable(self._get_long_tensor(token_type_ids_1))

        input_ids_2 = self._get_variable(self._get_long_tensor(input_ids_2))
        input_mask_2 = self._get_variable(self._get_long_tensor(input_mask_2))
        token_type_ids_2 = self._get_variable(self._get_long_tensor(token_type_ids_2))

        if len(pairwises) > 0:
            pairwises = self._get_variable(self._get_float_tensor(pairwises))

        # forward
        outputs_1 = self.bert_model(input_ids_1, attention_mask=input_mask_1,
                                    token_type_ids=token_type_ids_1, position_ids=None, head_mask=None)
        pooled_output_1 = self.token_average_pooling(outputs_1, token_type_ids_1) \
                if self.use_token_average_pooling else outputs_1[1]
        assert pooled_output_1.shape == outputs_1[1].shape, "{} != {}".format(pooled_output_1.shape, outputs_1[1].shape)
        pooled_output_1 = self.dropout(pooled_output_1)


        outputs_2 = self.bert_model(input_ids_2, attention_mask=input_mask_2,
                                    token_type_ids=token_type_ids_2, position_ids=None, head_mask=None)
        pooled_output_2 = self.token_average_pooling(outputs_2, token_type_ids_2) \
                if self.use_token_average_pooling else outputs_2[1]
        assert pooled_output_2.shape == outputs_2[1].shape, "{} != {}".format(pooled_output_2.shape, outputs_2[1].shape)
        pooled_output_2 = self.dropout(pooled_output_2)

        hidden = self.hidden2hidden(torch.cat([pooled_output_1, pooled_output_2, pairwises], 1)) if len(pairwises) > 0 \
                else self.hidden2hidden(torch.cat([pooled_output_1, pooled_output_2], 1))

        hidden = F.relu(hidden)
        logits = self.hidden2label(hidden)

        # TODO: skip softmax for speed
        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
