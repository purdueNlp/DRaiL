entity: "Doc",  arguments: ["docId"::ArgumentType.UniqueID];
entity: "Event", arguments: ["eventId"::ArgumentType.UniqueID];
entity: "IsTimex", arguments: ["eventId"::ArgumentType.UniqueID];
entity: "TemporalRelationType", arguments: ["temporalRelationType"::ArgumentType.String];
entity: "CausalRelationType", arguments: ["causalRelationType"::ArgumentType.String];

predicate: "HasEvent", arguments: [Doc, Event, IsTimex];
predicate: "IsNeighbor", arguments: [Doc, Event, Event];
predicate: "HasTemporalRelation", arguments: [Doc, Event, Event, TemporalRelationType];
predicate: "HasCausalRelation", arguments: [Doc, Event, Event, CausalRelationType];

// Temporal: before, after, includes, is_included, simultaneous, vague, None
// Causal: causes, caused_by, None
label: "TemporalLabel", classes: 7, type: LabelType.Multiclass;
label: "CausalLabel", classes: 3, type: LabelType.Multiclass;

load: "HasEvent", file: "has_event.txt";
load: "IsNeighbor", file: "is_neighbor.txt";
load: "HasTemporalRelation", file: "has_temporal_relation.txt";
load: "HasCausalRelation", file: "has_causal_relation.txt";
load: "TemporalLabel", file: "temporal_labels.txt";
load: "CausalLabel", file: "causal_labels.txt";

femodule: "event_causality_plus_ft";
feclass: "EventCausalityPlus_ft";

ruleset {

  rule: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) => HasTemporalRelation(D, E, F, X^TemporalLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [
    input("bert_event_left"),
    input("bert_event_right")
  ],
  target: E;

  rule: HasEvent(D, E, "0") & HasEvent(D, F, "0") & IsNeighbor(D, E, F) => HasCausalRelation(D, E, F, X^CausalLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [
    input("bert_event_left"),
    input("bert_event_right")
  ],
  target: E;

  // gold TT
  hardconstr: HasEvent(D, E, "1") & HasEvent(D, F, "1") & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, R) => HasTemporalRelation(D, E, F, R)^?;

  // temporal relation symmetric constrain
  hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, "before")^? => HasTemporalRelation(D, F, E, "after")^?;
  hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, "after")^? => HasTemporalRelation(D, F, E, "before")^?;
  hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, "includes")^? => HasTemporalRelation(D, F, E, "is_included")^?;
  hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, "is_included")^? => HasTemporalRelation(D, F, E, "includes")^?;
  hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & IsNeighbor(D, E, F) & HasTemporalRelation(D, E, F, "simultaneous")^? => HasTemporalRelation(D, F, E, "simultaneous")^?;

  // basic temporal relation transitivity constraint
  // Table 1 in https://www.aclweb.org/anthology/P18-1212/
  // code ref: https://github.com/rujunhan/EMNLP-2019
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "before")^? => HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "after")^? => HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "includes")^? => HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "is_included")^? => HasTemporalRelation(D, E, G, "is_included")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "simultaneous")^? & HasTemporalRelation(D, F, G, "simultaneous")^? => HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "simultaneous")^? => HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "simultaneous")^? => HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "simultaneous")^? => HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "simultaneous")^? => HasTemporalRelation(D, E, G, "is_included")^?;

  // advanced temporal relation transitivity constraint
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  //hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "before")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "after")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "after")^? => ~HasTemporalRelation(D, E, G, "before")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "includes")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "before")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "after")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "after")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "before")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "includes")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "is_included")^? => ~HasTemporalRelation(D, E, G, "includes")^?;


  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "is_included")^? & HasTemporalRelation(D, F, G, "vague")^? => ~HasTemporalRelation(D, E, G, "includes")^?;

  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "simultaneous")^?;
  // hardconstr: HasEvent(D, E, I) & HasEvent(D, F, J) & HasEvent(D, G, K) & IsNeighbor(D, E, F) & IsNeighbor(D, F, G) & IsNeighbor(D, E, G) & HasTemporalRelation(D, E, F, "vague")^? & HasTemporalRelation(D, F, G, "includes")^? => ~HasTemporalRelation(D, E, G, "is_included")^?;


  // TODO: temporal linguistic constrains

  // causal symmetric constrains
  hardconstr: HasEvent(D, E, "0") & HasEvent(D, F, "0") & IsNeighbor(D, E, F) & HasCausalRelation(D, E, F, "causes")^? => HasCausalRelation(D, F, E, "caused_by")^?;
  hardconstr: HasEvent(D, E, "0") & HasEvent(D, F, "0") & IsNeighbor(D, E, F) & HasCausalRelation(D, E, F, "caused_by")^? => HasCausalRelation(D, F, E, "causes")^?;

  // causal-temporal constrains
  // hardconstr: HasEvent(D, E, "0") & HasEvent(D, F, "0") & IsNeighbor(D, E, F) & HasCausalRelation(D, E, F, "causes")^? => HasTemporalRelation(D, E, F, "before")^?;
  // hardconstr: HasEvent(D, E, "0") & HasEvent(D, F, "0") & IsNeighbor(D, E, F) & HasCausalRelation(D, E, F, "caused_by")^? => HasTemporalRelation(D, E, F, "after")^?;

} groupby: HasEvent.1;
