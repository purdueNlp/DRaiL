import json
import sys

id2name={}
name2id={}
label_id2embd={}
doc_id2embd={}
doc2label={}
docs_written=[]

def LoadData():
    global id2name,name2id,label_id2embd,doc_id2embd,doc2label
    f=open("mygraph.mapping.supervised","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        id=int(l[0])
        name=str(l[1])
        id2name[id]=name
        name2id[name]=id
    f.close()

    f=open("doc.embeddings","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split(" ")
        doc_id=name2id[str(l[0])]
        embeddings=[float(l[i]) for i in range(1,len(l))]
        doc_id2embd[doc_id]=embeddings
    f.close()

    f = open("label.embeddings", "r")
    while True:
        l = f.readline()
        if not l:
            break
        l = l.strip("\n")
        l = l.strip(" ")
        l = l.split(" ")
        label_id = name2id[str(l[0])]
        embeddings = [float(l[i]) for i in range(1, len(l))]
        label_id2embd[label_id] = embeddings
    f.close()

    f=open("labels","r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n")
        l=l.strip(" ")
        l=l.split("\t")
        doc=l[0]
        label=int(l[1])
        if label==-1:
            label=2
        elif label==0:
            label=0
        elif label==1:
            label=1
        doc2label[doc]=label


    #print len(doc_id2embd),len(label_id2embd)

def WriteFiles(doc_count):
    global docs_written

    f=open("labels.txt","w")
    for label_id in label_id2embd:
        f.write(str(label_id)+"\n")
    f.close()

    f = open("docs.txt", "w")
    count=0
    for doc_id in doc_id2embd:
        f.write(str(doc_id) + "\n")
        count+=1
        docs_written.append(doc_id)
        if count==doc_count:
            break
    f.close()

    f = open("has_label.txt", "w")
    for doc in docs_written:
        f.write(str(doc) + " " + str(doc2label[id2name[doc]]) + "\n")
    f.close()
    f = open("has_same_label.txt", "w")
    f.close()

    f = open("in_instance.txt", "w")
    for doc in docs_written:
        f.write(str(doc)+"   "+"0"+"\n")
    f.close()

def WriteJson(doc_count):
    data={}
    for label_id in label_id2embd:
        data[label_id]=label_id2embd[label_id]
    count=0
    for doc_id in doc_id2embd:
        data[doc_id]=doc_id2embd[doc_id]
        count+=1
        if count==doc_count:
            break

    with open('embeddings.json', 'w') as f:
        json.dump(data, f)

    '''
    How to read
    '''
    '''
    with open('embeddings.json', 'r') as f:
        data = json.load(f)

    print (data['0'])
    print (data['1'])
    print (data['2'])
    print (data['1742'])
    print (data['1743'])
    print (data['1744'])
    print (data['1745'])
    print (data['1746'])
    print (data['1747'])
    print (data['1748'])
    print (data['1749'])
    print (data['1750'])
    print (data['1751'])
    #print (data['1752'])
    '''

if __name__ == '__main__':
    num_of_docs=int(sys.argv[1])
    LoadData()
    WriteFiles(num_of_docs) #set the passing parameter to the number of documents want to include in the dataset. Set infinity (a very large number) if want to include all.
    WriteJson(num_of_docs) #same as above
