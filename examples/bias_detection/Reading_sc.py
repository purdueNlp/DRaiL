from scipy.spatial.distance import cosine
import numpy as np
from numpy import array
from math import *


def square_rooted(x):
    return round(sqrt(sum([a * a for a in x])), 3)

def cosine_similarity(x, y):
    numerator = sum(a * b for a, b in zip(x, y))
    denominator = square_rooted(x) * square_rooted(y)
    return round(numerator / float(denominator), 3)

def dot_product(x, y):
    x=np.array(x)
    y=np.array(y)
    return np.dot(x,y)


def doc_doc_similarity_score(features, fe_class):
    '''
    codes to calculate similarity
    '''
    y_pred = []
    for i, sample in enumerate(features['input']):
        doc1_emb = sample[0]
        doc2_emb = sample[1]
        x = cosine_similarity(list(doc1_emb),list(doc2_emb))
        #x = dot_product(list(doc1_emb), list(doc2_emb))
        #print x
        scores = [0, x]
        y_pred.append(scores)
    return np.array(y_pred)

# Access to fe_class allows us to access the label embeddings for
# scoring
def doc_label_similarity_score(features, fe_class):
    '''
    codes to calculate similarity
    '''
    y_pred = [] # list will hold the scores for all samples
    label_embeddings = fe_class.get_label_embeddings()
    for i, sample in enumerate(features['vector']):
        doc_embd = sample
        scores = [] # list will hold the score for every label-doc
        for lbl in label_embeddings:
            label_embd = label_embeddings[lbl]
            score = cosine_similarity(list(doc_embd),list(label_embd))
            #score = dot_product(list(doc_embd), list(label_embd))
            scores.append(score)
        y_pred.append(scores)
    return array(y_pred)
