from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

import json
import numpy as np
import re

class TriDNR_fe(FeatureExtractor):

    def __init__(self, doc_filename,
                 label_filename, w2v_filename, doc2vec_filename,
                 deepWalk_filename, tridnr_filename,
                 jointNode_filename, jointText_filename,
                 text_ids_filename):
        super(TriDNR_fe, self).__init__()
        self.doc_filename = doc_filename
        self.label_filename = label_filename
        self.doc2vec_filename = doc2vec_filename
        self.deepWalk_filename = deepWalk_filename
        self.w2v_filename = w2v_filename
        self.tridnr_filename = tridnr_filename
        self.jointNode_filename = jointNode_filename
        self.jointText_filename = jointText_filename
        self.text_ids_filename = text_ids_filename
        np.random.seed(17477)

    @staticmethod
    def jsonKeys2str(x):
        if isinstance(x, dict):
            return {int(k):v for k,v in x.items()}
        return x

    def build(self):
        vocabulary = set([])

        self.label_idx = {}
        if self.label_filename:
            with open(self.label_filename) as f:
                for i, lab in enumerate(f.readlines()):
                    self.label_idx[lab.strip()] = i
        print self.label_idx

        if self.doc_filename:
            with open(self.doc_filename) as f:
                self.docs = json.load(f, object_hook=self.jsonKeys2str)

            for key in self.docs:
                for word in self.docs[key].split():
                    w = re.sub(r'[^a-zA-Z0-9_\-]+', '', word.lower())
                    vocabulary.add(w)
            self.vocab_size, self.word2idx = utils.idx_dictionary(vocabulary)

        if self.text_ids_filename:
            with open(self.text_ids_filename) as f:
                self.text_ids = json.load(f, object_hook=self.jsonKeys2str)
        if self.w2v_filename:
            self.num_word_vectors, self.word_embedding_size, self.word_embedding = \
                    utils.embeddings_dictionary(self.w2v_filename, vocabulary)
        if self.doc2vec_filename:
            self.num_doc_vectos, self.doc2vec_size, self.doc2vec_dict = \
                    utils.embeddings_dictionary(self.doc2vec_filename)
        if self.deepWalk_filename:
            self.num_deepWalk_vectors, self.deepWalk_size, self.deepWalk_dict = \
                    utils.embeddings_dictionary(self.deepWalk_filename)
        if self.tridnr_filename:
            self.num_tridnr_vectors, self.tridnr_size, self.tridnr_dict = \
                    utils.embeddings_dictionary(self.tridnr_filename)
        if self.jointNode_filename:
            self.num_jointNode_vectors, self.jointNode_size, self.jointNode_dict = \
                    utils.embeddings_dictionary(self.jointNode_filename)
        if self.jointText_filename:
            self.num_jointText_vectors, self.jointText_size, self.jointText_dict = \
                    utils.embeddings_dictionary(self.jointText_filename)
            #self.vocab_size = self.num_jointText_vectors


    def doc2vec(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.doc2vec_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[0]['arguments'][0]
        return self.doc2vec_dict[str(doc)]

    def w2v_avg(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.doc2vec_size)
            return map(float, list(rndm))

        doc = rule_grd.get_body_predicates("InGraph")[0]
        words = self.docs[doc['arguments'][0]].lower().split()
        words = [re.sub(r'\W+', '', word.lower()) for word in words]
        embeddings = [self.word_embedding[word] if word in self.word_embedding
                      else map(float, list(np.random.uniform(-0.0025, 0.0025,
                      self.word_embedding_size))) for word in words]
        return map(float, list(np.mean(embeddings, axis=0)))

    def w2v_sentence(self, rule_grd):
        doc = rule_grd.get_body_predicates("InGraph")[0]
        words = self.docs[doc['arguments'][0]].lower().split()
        words = [re.sub(r'[^a-zA-Z0-9_\-]+', '', word.lower()) for word in words]
        idx = [self.word2idx[word] for word in words]
        return idx

    def deepWalk(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[0]['arguments'][0]
        return self.deepWalk_dict[str(doc)]

    def tridnr(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.tridnr_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[0]['arguments'][0]
        return self.tridnr_dict[str(doc)]

    def jointNode(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointNode_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[0]['arguments'][0]
        return self.jointNode_dict[str(doc)]

    def jointText_avg(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointNode_size)
            return map(float, list(rndm))

        doc = rule_grd.get_body_predicates("InGraph")[0]['arguments'][0]
        words = self.text_ids[doc]
        embeddings = [self.jointText_dict[str(word)] for word in words]

        if len(embeddings) == 0:
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointText_size)
            return map(float, list(rndm))

        l =  list(np.mean(embeddings, axis=0))
        l = map(float, l)
        return l

    def jointText_sentence(self, rule_grd):
        doc = rule_grd.get_body_predicates("InGraph")[0]
        return self.text_ids[doc['arguments'][0]]

    def jointNode_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointNode_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[1]['arguments'][0]
        return self.jointNode_dict[str(doc)]

    def jointText_avg_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointNode_size)
            return map(float, list(rndm))

        doc = rule_grd.get_body_predicates("InGraph")[1]['arguments'][0]
        words = self.text_ids[doc]
        embeddings = [self.jointText_dict[str(word)] for word in words]

        if len(embeddings) == 0:
            rndm = np.random.uniform(-0.0025, 0.0025, self.jointText_size)
            return map(float, list(rndm))

        l =  list(np.mean(embeddings, axis=0))
        l = map(float, l)
        return l

    def doc2vec_neighbor(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.doc2vec_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[1]['arguments'][0]
        return self.doc2vec_dict[str(doc)]

    def deepWalk_neighbor(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_size)
            return map(float, list(random))
        doc = rule_grd.get_body_predicates("InGraph")[1]['arguments'][0]
        return self.deepWalk_dict[str(doc)]

    def tridnr_neighbor(self, rule_grd):
        if not rule_grd.has_body_predicate("InGraph"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.tridnr_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InGraph")[1]['arguments'][0]
        return self.tridnr_dict[str(doc)]

    def extract_label(self, instance_grd):
        labels = instance_grd.get_body_predicates("HasLabel")
        labels = [l['arguments'][1] for l in labels]
        label_idxs = [self.label_idx[str(l)] for l in labels]
        vec = []
        for lbl_idx in label_idxs:
            vec += [(i == lbl_idx)*1 for i in range(len(self.label_idx))]
        return vec

    def bias(self, isntance_grd):
        return [1]

    def extract_multiclass_head(self, instance_grd):
        label = instance_grd.get_head_predicate()['arguments'][1]
        return self.label_idx[str(label)]

