import os
from itertools import tee, izip
from drail.model.rule import RuleGrounding
from drail.model.label import LabelType
import random 

def findPaths(G,u,n,excludeSet = None):
    if excludeSet == None:
        excludeSet = set([u])
    else:
        excludeSet.add(u)
    if n==0:
        return [[u]]
    if u in G:
        paths = [[u]+path for neighbor in G[u] if neighbor not in excludeSet
                for path in findPaths(G,neighbor,n-1,excludeSet)]
        excludeSet.remove(u)
    else:
        paths = []
    return paths

class TriDNR_db():

    def __init__(self, neighborhood=None):
        self.cited_by = {};
        self.cites = {};
        self.has_label = {}
        if neighborhood is not None:
            self.neighborhood = neighborhood
        else:
            self.neighborhood = 2
        self.labels = set([])
        random.seed(2017)

    def load_data(self, dataset_path):
        self.dataset_path = dataset_path
        isolated_nodes = 0
        with open(os.path.join(dataset_path, "adjedges.txt")) as f:
            for line in f:
                nodes = line.strip().split()
            	for n in nodes[1:]:
                    if n not in self.cites:
                        self.cites[n] = [nodes[0]]
                    else:
                        self.cites[n].append(nodes[0])

                self.cited_by[nodes[0]] = nodes[1:]

        print "cites", len(self.cites)

        with open(os.path.join(dataset_path, "labels.txt")) as f:
            for line in f:
                node, label = line.strip().split()
                self.has_label[node] = label
                self.labels.add(label)


        # used to break down the problem in global learning
        self.instances = {}
        portion = self.has_label.keys()
        random.shuffle(portion)
        #for j, node in enumerate(portion[:10]):
        for j, node in enumerate(portion):
            self.instances[str(j+1)] = set([])
            if node not in self.cites:
                self.instances[str(j+1)].add(node)
            for i in range(1, self.neighborhood):
                paths = findPaths(self.cites, node, i)
                for p in paths:
                    for v in p:
                        if v in self.has_label:
                            self.instances[str(j+1)].add(v)
        self.instances["0"] = set(self.has_label.keys())

    def add_filters(self, filters=[]):
        self.filters = {}
        for fil in filters:
            self.filters[fil[1]] = fil[3]

    def base(self, istrain, isneg, filters, split_class, instance_id):
        if instance_id is None:
            instance_id = "0"
        filter_a = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])

        if len(filter_a) == 0:
            filter_a = self.has_label.keys()

        ret = []

        for node in set(filter_a) & self.instances[instance_id]:

            InGraph = {"name": "InGraph",
                       "arguments": [int(node), int(instance_id)],
                       "ttype": None,
                       "obs": True,
                       "isneg": False,
                       "target_pos": None
                       }

            if not istrain:
                for label in self.labels:
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node), label],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    body = [InGraph]
                    head = HasLabel_

                    if label != self.has_label[node]:
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                    else:
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
            elif istrain and not isneg:
                HasLabel = {"name": "HasLabel",
                           "arguments": [int(node), self.has_label[node]],
                           "ttype": LabelType.Multiclass,
                           "obs": False,
                           "isneg": False,
                           "target_pos": 1
                           }
                body = [InGraph]
                head = HasLabel
                rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                rg.build_predicate_index()
                rg.build_body_predicate_dic()
                ret.append(rg)
            elif istrain and isneg:
                for label in self.labels:
                    if label == self.has_label(node):
                        continue
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node), label],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    body = [InGraph]
                    head = HasLabel_
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

        return ret

    def cites_helper(self, istrain, isneg, filters, split_class, instance_id, mode):
        if instance_id is None:
            instance_id = "0"
        if mode == 'cites':
            graph = self.cites
        else:
            graph = self.cited_by


        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            if fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])

        if len(filter_a) == 0:
            filter_a = self.has_label.keys()
        if len(filter_b) == 0:
            filter_b = self.has_label.keys()


        #print "# of As", len(filter_a), "# of Bs", len(filter_b)
        ret = []

        isolated_nodes = 0

        for node in set(filter_a) & self.instances[instance_id]:

            if node in graph:
                relevant_neighbors = set(graph[node]) & set(filter_b)
            else:
                relevant_neighbors = set([])

            if len(relevant_neighbors & self.instances[instance_id]) == 0:
                isolated_nodes += 1


            for node_ in relevant_neighbors & self.instances[instance_id]:
                if node == node_:
                    continue
                InGraph1 = {"name": "InGraph",
                           "arguments": [int(node), int(instance_id)],
                           "ttype": None,
                           "obs": True,
                           "isneg": False,
                           "target_pos": None
                           }

                InGraph2 = {"name": "InGraph",
                           "arguments": [int(node_), int(instance_id)],
                           "ttype": None,
                           "obs": True,
                           "isneg": False,
                           "target_pos": None
                           }

                Cites = {"name": "Cites",
                         "arguments": [int(node), int(node_)],
                         "ttype": None,
                         "obs": True,
                         "isneg": False,
                         "target_pos": None
                         }

                if not istrain:
                    for label in self.labels:
                        HasLabel = {"name": "HasLabel",
                                   "arguments": [int(node), label],
                                   "ttype": LabelType.Multiclass,
                                   "obs": False,
                                   "isneg": False,
                                   "target_pos": 1
                                   }
                        for label_ in self.labels:

                            HasLabel_ = {"name": "HasLabel",
                                       "arguments": [int(node_), label_],
                                       "ttype": LabelType.Multiclass,
                                       "obs": False,
                                       "isneg": False,
                                       "target_pos": 1
                                       }


                            body = [InGraph1, InGraph2, Cites, HasLabel_]
                            head = HasLabel
                            if label == self.has_label[node] and label_ == self.has_label[node_]:
                                rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                                rg.build_predicate_index()
                                rg.build_body_predicate_dic()
                                ret.append(rg)
                            else:
                                rg = RuleGrounding(body, head, None, False, head['arguments'][0], False)
                                rg.build_predicate_index()
                                rg.build_body_predicate_dic()
                                ret.append(rg)
                elif istrain and not isneg:
                    HasLabel = {"name": "HasLabel",
                               "arguments": [int(node), self.has_label[node]],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node_), self.has_label[node_]],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    body = [InGraph1, InGraph2, Cites, HasLabel_]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
                elif istrain and isneg:
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node_), self.has_label[node_]],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    for label in self.labels:
                        if label == self.has_label[node]:
                            continue

                        HasLabel = {"name": "HasLabel",
                                   "arguments": [int(node), label],
                                   "ttype": LabelType.Multiclass,
                                   "obs": False,
                                   "isneg": False,
                                   "target_pos": 1}

                        body = [InGraph1, InGraph2, Cites, HasLabel_]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

        #print "isolated", isolated_nodes
        return ret

    def agreement_helper(self, istrain, isneg, filters, split_class, instance_id, mode):
        if instance_id is None:
            instance_id = "0"
        if mode == 'cites':
            graph = self.cites
        else:
            graph = self.cited_by

        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            if fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])

        if len(filter_a) == 0:
            filter_a = self.has_label.keys()
        if len(filter_b) == 0:
            filter_b = self.has_label.keys()

        ret = []
        for node in set(filter_a)  & self.instances[instance_id]:

            if node in graph:
                relevant_neighbors = set(graph[node]) & set(filter_b)
            else:
                relevant_neighbors = set([])

            for node_ in relevant_neighbors & self.instances[instance_id]:

                InGraph1 = {"name": "InGraph",
                           "arguments": [int(node), int(instance_id)],
                           "ttype": None,
                           "obs": True,
                           "isneg": False,
                           "target_pos": None
                           }

                InGraph2 = {"name": "InGraph",
                           "arguments": [int(node_), int(instance_id)],
                           "ttype": None,
                           "obs": True,
                           "isneg": False,
                           "target_pos": None
                           }

                Cites = {"name": "Cites",
                         "arguments": [int(node), int(node_)],
                         "ttype": None,
                         "obs": True,
                         "isneg": False,
                         "target_pos": None
                         }


                if split_class is not None:
                    '''
                    if self.has_label[node] == split_class:
                        print self.has_label[node], self.has_label[node_]
                    '''
                    HasLabel = {"name": "HasLabel",
                               "arguments": [int(node), split_class],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node_), split_class],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                else:
                    HasLabel = {"name": "HasLabel",
                               "arguments": [int(node), self.has_label[node]],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [int(node_), self.has_label[node_]],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }

                body = [InGraph1, InGraph2, Cites, HasLabel_]
                head = HasLabel

                if not istrain and split_class is not None:
                    if split_class == self.has_label[node] and split_class == self.has_label[node_]:
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                    else:
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                elif istrain and not isneg and split_class is not None:
                    if split_class == self.has_label[node] and split_class == self.has_label[node_]:
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                elif istrain and isneg and split_class is not None:
                    if split_class == self.has_label[node] and split_class != self.has_label[node_]:
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                elif istrain and not isneg and split_class is None:
                    rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

        return ret

    def cites_paper(self, istrain, isneg, filters, split_class, instance_id=None):
        return self.cites_helper(istrain, isneg, filters, split_class, instance_id, mode='cites')

    def cited_by_paper(self, istrain, isneg, filters, split_class, instance_id=None):
        return self.cites_helper(istrain, isneg, filters, split_class, instance_id, mode='cited_by')

    def cites_agreement(self, istrain, isneg, filters, split_class, instance_id=None):
        return self.agreement_helper(istrain, isneg, filters, split_class, instance_id, mode='cites')

    def cited_by_agreement(self, istrain, isneg, filters, split_class, instance_id=None):
        return self.agreement_helper(istrain, isneg, filters, split_class, instance_id, mode='cited_by')

    def cites_transitions(self, istrain, isneg, filters, split_class, instance_id=None):
        return self.transition_helper(istrain, isneg, filters, split_class, instance_id, mode='cites')

    def seed(self, filters, instance_id=None):
        if instance_id is None:
            instance_id = "0"
        filter_c = []
        for fil in filters:
            if fil[0] == 'C':
                filter_c += map(str, self.filters[fil[1]])
        ret = []
        for node in set(filter_c) & self.instances[instance_id]:
            InGraph = {"name": "InGraph",
                       "arguments": [int(node), 1],
                       "ttype": None,
                       "obs": True,
                       "isneg": False,
                       "target_pos": None
                       }
            HasLabel = {"name": "HasLabel",
                       "arguments": [int(node), self.has_label[node]],
                       "ttype": None,
                       "obs": True,
                       "isneg": False,
                       "target_pos": 1
                       }
            body = [InGraph, HasLabel]
            head = HasLabel
            rg = RuleGrounding(body, head, None, False, None, True)
            rg.build_predicate_index()
            rg.build_body_predicate_dic()
            ret.append(rg)
        return ret

    def get_gold_predicates(self, *args):
        instance_id = args[3]
        ret = []
        filters = args[1]; filter_a = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])

        for node in set(filter_a) & self.instances[instance_id]:
            HasLabel = {"name": "HasLabel",
                       "arguments": [int(node), self.has_label[node]],
                       "ttype": None,
                       "obs": False,
                       "isneg": False,
                       "target_pos": 1
                       }
            ret.append(HasLabel)
        return ret


    def get_ruleset_instances(self, *args):
        is_global_train = args[3]
        if not is_global_train:
            return ["0"]
        else:
            return set(self.instances.keys()) - set(["0"])


    def get_distinct_values(self, *args):
        return list(self.labels)

    def get_target_counts(self, *args):
        ret = {}
        filters = args[3]; filter_a = []
        rule_template = args[0]
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])

        for node in filter_a:
            if len(rule_template.body) > 1 and node in self.cites:
                ret[int(node)] = len(self.cites[node])
            else:
                ret[int(node)] = 1
        return ret
