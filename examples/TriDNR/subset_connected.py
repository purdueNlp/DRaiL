import sys
import os

input_dataset = sys.argv[1]
output_dataset = sys.argv[2]

relevant = set([])

out = open(os.path.join(output_dataset, "adjedges.txt"), "w")
with open(os.path.join(input_dataset, "adjedges.txt")) as f:
    for line in f:
        nodes = line.strip().split()
        if len(nodes) > 1:
            relevant.add(nodes[0])
            out.write(line)
out.close()

out = open(os.path.join(output_dataset, "docs.txt"), "w")
with open(os.path.join(input_dataset, "docs.txt")) as f:
    for line in f:
        fields = line.strip().split()
        if fields[0] in relevant:
            out.write(line)
out.close()

out = open(os.path.join(output_dataset, "labels.txt"), "w")
with open(os.path.join(input_dataset, "labels.txt")) as f:
    for line in f:
        fields = line.strip().split()
        if fields[0] in relevant:
            out.write(line)
out.close()
