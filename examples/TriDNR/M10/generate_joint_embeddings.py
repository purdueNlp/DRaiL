import json
import numpy as np

DATASET_DETAILS = "joint/nodes_infor_{0}.json"
NODE_EMBEDDINGS = "joint/node_emb_{0}.npy"
TEXT_EMBEDDINGS = "joint/text_emb_{0}.npy"

TEXT_INDEX_OUT = "text_index_{0}.json"
NODE_EMB_OUT = "node_emb_{0}.bin"
TEXT_EMB_OUT = "text_emb_{0}.bin"

with open("docs.json") as f:
    docs = json.load(f)

for fold in range(5):
    print "processing fold {0}...".format(fold)

    data_f = DATASET_DETAILS.format(fold)
    node_f = NODE_EMBEDDINGS.format(fold)
    text_f = TEXT_EMBEDDINGS.format(fold)

    with open(data_f) as f:
        data = json.load(f)

    node_embs = np.load(node_f)
    text_embs = np.load(text_f)

    node_ids = {}
    node_text = {}
    for i, node in enumerate(data):
        if node['node_name'] in docs:
            node_ids[node['node_name']] = i
            node_text[node['node_name']] = node['node_text']

    with open(NODE_EMB_OUT.format(fold), 'wb') as f:
        first = True
        for node in docs:

            if first:
                f.write(str(len(docs)))
                f.write(' ')
                f.write(str(node_embs[node_ids[node]].shape[0]))
                f.write('\n')
                first = False

            f.write(node)
            f.write(' ')
            f.write(node_embs[node_ids[node]].tobytes())
            f.write('\n')

    with open(TEXT_EMB_OUT.format(fold), 'wb') as f:
        first = True
        for i in range(text_embs.shape[0]):
            if first:
                f.write(str(text_embs.shape[0]))
                f.write(' ')
                f.write(str(text_embs[i].shape[0]))
                f.write('\n')
                first = False
            f.write(str(i))
            f.write(' ')
            f.write(text_embs[i].tobytes())
            f.write('\n')

    with open(TEXT_INDEX_OUT.format(fold), 'w') as f:
        json.dump(node_text, f)
