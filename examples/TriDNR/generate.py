import optparse
import os
import json
import random

parser = optparse.OptionParser()
parser.add_option('-d', '--dir', help='directory', dest='dir', type='string')

(opts, args) = parser.parse_args()

mandatories = ['dir']
for m in mandatories:
    if not opts.__dict__[m]:
        print "mandatory option is missing\n"
        parser.print_help()
        exit(-1)


INPUT_EDGES = os.path.join(opts.dir, "adjedges.txt")
INPUT_DOCS = os.path.join(opts.dir, "docs.txt")
INPUT_LABELS = os.path.join(opts.dir, "labels.txt")

OUTPUT_NODES = os.path.join(opts.dir, "nodes.txt")
OUTPUT_DOCS = os.path.join(opts.dir, "docs.json")
OUTPUT_EDGES = os.path.join(opts.dir, "edges.txt")
OUTPUT_CLASSES = os.path.join(opts.dir, "classes.txt")

OUTPUT_GRAPH_FULL = os.path.join(opts.dir, "graph.txt")
OUTPUT_INGRAPH_FULL = os.path.join(opts.dir, "in_graph.txt")

# parse docs
fn = open(OUTPUT_NODES, 'w')
titles = {}
with open(INPUT_DOCS) as f:
    for line in f:
        doc, words = line.strip().split(' ', 1)
        titles[int(doc)] = words
        fn.write(doc + "\n")
fn.close()

random.seed(4321)
for i in range(10):
    documents = titles.keys()
    random.shuffle(documents)
    train_data = documents[:len(documents)/2]
    test_data = documents[len(documents)/2:]

    with open(os.path.join(opts.dir, "folds", "train_{0}".format(i)), 'w') as f:
        for doc in train_data:
            f.write(str(doc) + "\n")

    with open(os.path.join(opts.dir, "folds", "test_{0}".format(i)), 'w') as f:
        for doc in test_data:
            f.write(str(doc) + "\n")

with open(OUTPUT_DOCS, 'w') as f:
    json.dump(titles, f)

# parse classes
classes = set([])
with open(INPUT_LABELS) as f:
    for line in f:
        node, label = line.strip().split()
        classes.add(label)

fc = open(OUTPUT_CLASSES, 'w')
for label in classes:
    fc.write(label + "\n")
fc.close()

# parse edges and join them in full graph
fe = open(OUTPUT_EDGES, 'w')
fg = open(OUTPUT_INGRAPH_FULL, 'w')

edges = set([])
with open(INPUT_EDGES) as f:
    for line in f:
        nodes = line.strip().split()
        fg.write(nodes[0] + "\t0\n")

        for n in nodes[1:]:
            fe.write(nodes[0] + "\t" + n +  "\n")
            edges.add((nodes[0], n))
    fe.close()
    fg.close()

print len(edges)

# a single full graph
fg = open(OUTPUT_GRAPH_FULL, 'w')
fg.write("0\n")
fg.close()
