import sys
import os
import numpy as np
import optparse
import cProfile as profile
import random
import torch
from sklearn.metrics import *

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner

def get_ids(papers_file):
    papers = []
    with open(papers_file) as f:
        for line in f:
            papers.append(int(line.strip()))
    random.shuffle(papers)
    return papers

def main():
    np.random.seed(1234)
    random.seed(1234)
    torch.cuda.set_device(1)

    parser = optparse.OptionParser()
    parser.add_option('--dd', help='data directory', dest='data_dir', type='string')
    parser.add_option('--dm', help='module directory', dest='mod_dir', type='string')
    parser.add_option('-r', '--rule', help='rule file', dest='rules', type='string',
                       default='rule.dr')
    parser.add_option('-c', '--config', help='config file', dest='config', type='string',
                       default="config.json")
    parser.add_option('--we', help='word embeddings', dest='w2v', type='string',
                      default=None)
    parser.add_option('-m', help='mode: [global|local]', dest='mode', type='string',
                      default='local')
    (opts, args) = parser.parse_args()

    mandatories = ['data_dir', 'mod_dir']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    rule_file = os.path.join(opts.data_dir, opts.rules)
    conf_file = os.path.join(opts.data_dir, opts.config)
    doc2vec_file = None
    deepwalk_file = None
    papers_file = os.path.join(opts.data_dir, "nodes.txt")
    doc_file = os.path.join(opts.data_dir, "docs.json")
    class_file = os.path.join(opts.data_dir, "classes.txt")

    macro_f1_scores = []

    if opts.mode == "global":
        learner=GlobalLearner(learning_rate=0.05)
        learner.compile_rules(rule_file)
        db=learner.create_dataset(opts.data_dir, dbmodule_path=opts.mod_dir)
        db.neighborhood = 2
    else:
        learner=LocalLearner()
        learner.compile_rules(rule_file)
        db=learner.create_dataset(opts.data_dir, dbmodule_path=opts.mod_dir)

    print db

    for i in range(0, 5):
        print "PROCESSING FOLD {0}...".format(i)

        train_papers = get_ids("{0}/folds/train_{1}".format(opts.data_dir, i))
        test_papers = get_ids("{0}/folds/test_{1}".format(opts.data_dir, i))
        dev_size = int(len(test_papers)*0.2)
        dev_papers = test_papers[:dev_size]
        test_papers = test_papers[dev_size:]


        print len(train_papers), len(dev_papers), len(test_papers)

        tridnr = "{0}/folds/tridnr_{1}.bin".format(opts.data_dir, i)
        '''
        joint_node = "{0}/folds/node_emb_{1}.bin".format(opts.data_dir, i)
        joint_text = "{0}/folds/text_emb_{1}.bin".format(opts.data_dir, i)
        text_index = "{0}/folds/text_index_{1}.json".format(opts.data_dir, i)
        '''
        joint_node = None
        joint_text = None
        text_index = None

        db.add_filters(filters=[
            ("Paper", "isTrain", "paperId", train_papers),
            ("Paper", "isDev", "paperId", dev_papers),
            ("Paper", "isTest", "paperId", test_papers),
            ("Paper", "isTrainDev", "paperId", train_papers + dev_papers)
            ])

        print "FEATURE EXTRACTOR"
        learner.build_feature_extractors(db,
                                         doc_filename=doc_file,
                                         label_filename=class_file,
                                         doc2vec_filename=doc2vec_file,
                                         deepWalk_filename=deepwalk_file,
                                         w2v_filename=opts.w2v,
                                         tridnr_filename=tridnr,
                                         jointNode_filename=joint_node,
                                         jointText_filename=joint_text,
                                         text_ids_filename=text_index,
                                         femodule_path=opts.mod_dir)
        if opts.mode == "global":
            # do we want to add a subset of the train for seeding in the global model?
            learner.build_models(db, conf_file)
            learner.extract_data(db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("C", "isTrain", 1)],
                    test_filters=[("A", "isTest", 1), ("C", "isTrainDev", 1)])
            res = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("C", "isTrain", 1)],
                    test_filters=[("A", "isTest", 1), ("C", "isTrainDev", 1)],
                    opt_predicate='HasLabel',
                    hot_start=False)

        else:
            learner.build_models(db, conf_file)
            _ = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "isTest", 1)])
            learner.extract_data(db, test_filters=[("A", "isTest", 1), ("B", "isTest", 1)])
            #("C", "isTrainDev", 1)])
            learner.drop_scores(db, fold='test', output="citation_scores.csv")
            res, _ = learner.predict(
                    db,
                    fold='test')

        y_gold = res.metrics['HasLabel']['gold_data']
        y_pred = res.metrics['HasLabel']['pred_data']
        print res.metrics['encoding_time']
        print res.metrics['solving_time']
        print classification_report(y_gold, y_pred)
        f1_test = f1_score(y_gold, y_pred, average='macro')
        print "TEST F1", f1_test
        acc_test = accuracy_score(y_gold, y_pred)
        print "TEST ACC", acc_test
        macro_f1_scores.append(f1_test)
        learner.reset_metrics()
        exit()
    print macro_f1_scores

if __name__ == "__main__":
    main()
