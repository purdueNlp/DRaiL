import re
import json
import random
import logging

import numpy as np

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils


class FourForums_ft(FeatureExtractor):

    def __init__(self, word_emb_f, data_f, word2idx_f,
                 use_wordembed=False, debug=False):
        super(FourForums_ft, self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.n_max_len = 100

        # Word Embeddings
        self.word_emb_f = word_emb_f
        self.data_f = data_f
        self.word2idx_f = word2idx_f
        self.debug = debug
        self.use_wordembed = use_wordembed

        # BERT
        self.CLS = 101
        self.SEP = 102

    def build(self):
        self.stance_idx = {'con-legal': 0,  # conservative
                           'pro-legal': 1,    # liberal
                           'pro-intelligent-design': 0,
                           'pro-evolution': 1,
                           'con-strict-control': 0,
                           'pro-strict-control': 1,
                           'pro-life': 0,
                           'pro-choice': 1,
                           'other': 2,
                           }

        # Load posts
        with open(self.data_f) as fp:
            self.data = json.load(fp)

        if self.use_wordembed:
            # Load word indices
            with open(self.word2idx_f) as fp:
                self.word2idx = json.load(fp)

            # Load word embeddings from txt file
            vocabulary = set(self.word2idx.keys())
            self.vocab_size = len(vocabulary)
            self.logger.info("vocab_size {}".format(self.vocab_size))
            if self.word_emb_f:
                _, self.word_emb_size, self.word_dict =\
                    utils.embeddings_dictionary_txt(self.word_emb_f, vocabulary=vocabulary, debug=self.debug)

    def pad_and_mask(self, vector, max_seq_len):
        pad_token = 0
        mask = [1] * len(vector)
        if len(vector) < max_seq_len:
            vector += [pad_token] * (max_seq_len - len(vector))
            mask += [0] * (max_seq_len - len(mask))
        return (vector, mask)

    def bert_first(self, rule_grd):
        in_thread = rule_grd.get_body_predicates("InThread")[0]
        thread_id, post_id = in_thread['arguments']
        vector = self.data[str(post_id)][:self.n_max_len]
        (vector, mask) = self.pad_and_mask([self.CLS] + vector + [self.SEP], self.n_max_len + 2)
        return (vector, mask)

    def bert_second(self, rule_grd):
        in_thread = rule_grd.get_body_predicates("InThread")[1]
        thread_id, post_id = in_thread['arguments']
        vector = self.data[str(post_id)][:self.n_max_len]
        (vector, mask) = self.pad_and_mask(vector + [self.SEP], self.n_max_len + 1)
        return (vector, mask)

    def post_first(self, rule_grd):
        in_thread = rule_grd.get_body_predicates("InThread")[0]
        thread_id, post_id = in_thread['arguments']
        ret = self.data[str(post_id)][:self.n_max_len]
        if len(ret) > 0:
            return ret
        else:
            return [0]

    def post_second(self, rule_grd):
        in_thread = rule_grd.get_body_predicates("InThread")[1]
        thread_id, post_id = in_thread['arguments']
        ret = self.data[str(post_id)][:self.n_max_len]
        if len(ret) > 0:
            return ret
        else:
            return [0]

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasStance':
            label = head['arguments'][2]
            return self.stance_idx[label]

