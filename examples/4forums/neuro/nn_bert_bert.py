import torch
import numpy as np
import torch.nn.functional as F
from transformers import BertModel, BertConfig

from drail.neuro.nn_model import NeuralNetworks

class BertBertClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertBertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)

    def build_architecture(self, rule_template, fe, shared_params={}):
        bert_config = BertConfig.from_pretrained("bert-base-uncased")

        name = self.config["shared_encoder"]
        self.bert_model = shared_params[name]["bert"]
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        self.hidden2hidden = torch.nn.Linear(bert_config.hidden_size*2, self.config["n_hidden"])
        self.hidden2label = torch.nn.Linear(self.config["n_hidden"], self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2hidden = self.hidden2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids_1 = []; input_mask_1 = []; segment_ids_1 = []
        input_ids_2 = []; input_mask_2 = []; segment_ids_2 = []

        for elem in x['input']:
            input_ids_1.append(elem[0][0])
            input_mask_1.append(elem[0][1])
            segment_ids_1.append([0] * len(elem[0][0]))

            input_ids_2.append(elem[1][0])
            input_mask_2.append(elem[1][1])
            segment_ids_2.append([0] * len(elem[1][0]))

        input_ids_1 = self._get_variable(self._get_long_tensor(input_ids_1))
        input_mask_1 = self._get_variable(self._get_long_tensor(input_mask_1))
        segment_ids_1 = self._get_variable(self._get_long_tensor(segment_ids_1))

        input_ids_2 = self._get_variable(self._get_long_tensor(input_ids_2))
        input_mask_2 = self._get_variable(self._get_long_tensor(input_mask_2))
        segment_ids_2 = self._get_variable(self._get_long_tensor(segment_ids_2))

        outputs_1 = self.bert_model(input_ids_1, attention_mask=input_mask_1,
                                    token_type_ids=segment_ids_1, position_ids=None, head_mask=None)
        pooled_output_1 = outputs_1[1]
        pooled_output_1 = self.dropout(pooled_output_1)

        outputs_2 = self.bert_model(input_ids_2, attention_mask=input_mask_2,
                                    token_type_ids=segment_ids_2, position_ids=None, head_mask=None)
        pooled_output_2 = outputs_2[1]
        pooled_output_2 = self.dropout(pooled_output_2)

        hidden = self.hidden2hidden(torch.cat([pooled_output_1, pooled_output_2], 1))
        hidden = F.relu(hidden)
        logits = self.hidden2label(hidden)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
