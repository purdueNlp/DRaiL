import torch
import numpy as np
import torch.nn.functional as F
from pytorch_transformers import BertModel, BertConfig

from drail.neuro.nn_model import NeuralNetworks

class BertClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)

    def build_architecture(self, rule_template, fe, shared_params={}):
        bert_config = BertConfig.from_pretrained("bert-base-uncased")

        if "shared_encoder" not in self.config:
            self.bert_model_edge = BertModel.from_pretrained("bert-base-uncased")

        name = self.config["resnet_encoder"]
        self.bert_model_node = shared_params[name]["bert"]

        name = self.config["resnet_output"]
        self.node_output = shared_params[name]["layer"]
        self.node_n_out = shared_params[name]["nout"]

        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        if "shared_encoder" in self.config:
            self.hidden2hidden = torch.nn.Linear(bert_config.hidden_size*2 + self.node_n_out*2, self.config["n_hidden"])
        else:
            self.hidden2hidden = torch.nn.Linear(bert_config.hidden_size + self.node_n_out*2, self.config["n_hidden"])
        self.hidden2label = torch.nn.Linear(self.config["n_hidden"], self.output_dim)

        if self.use_gpu:
            self.bert_model_node = self.bert_model_node.cuda()
            self.node_output = self.node_output.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2hidden = self.hidden2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

            if "shared_encoder" not in self.config:
                self.bert_model_edge = self.bert_model_edge.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []
        input_ids_1 = []; input_mask_1 = []; segment_ids_1 = []
        input_ids_2 = []; input_mask_2 = []; segment_ids_2 = []

        # We will always have two sequences
        for elem in x['input']:
            input_ids.append(elem[0][0] + elem[1][0])
            input_mask.append(elem[0][1] + elem[1][1])
            segment_ids.append([0] * len(elem[0][0]) + [1] * len(elem[1][0]))

            input_ids_1.append(elem[0][0])
            input_mask_1.append(elem[0][1])
            segment_ids_1.append([0] * len(elem[0][0]))

            input_ids_2.append(elem[1][0])
            input_mask_2.append(elem[1][1])
            segment_ids_2.append([0] * len(elem[1][0]))

        input_ids_1 = self._get_variable(self._get_long_tensor(input_ids_1))
        input_mask_1 = self._get_variable(self._get_long_tensor(input_mask_1))
        segment_ids_1 = self._get_variable(self._get_long_tensor(segment_ids_1))

        input_ids_2 = self._get_variable(self._get_long_tensor(input_ids_2))
        input_mask_2 = self._get_variable(self._get_long_tensor(input_mask_2))
        segment_ids_2 = self._get_variable(self._get_long_tensor(segment_ids_2))

        # Run model
        outputs_1 = self.bert_model_node(input_ids_1, attention_mask=input_mask_1,
                                         token_type_ids=segment_ids_1, position_ids=None, head_mask=None)
        outputs_2 = self.bert_model_node(input_ids_2, attention_mask=input_mask_2,
                                         token_type_ids=segment_ids_2, position_ids=None, head_mask=None)

        pooled_output_1 = outputs_1[1]
        pooled_output_2 = outputs_2[1]

        pooled_output_1 = self.dropout(pooled_output_1)
        pooled_output_2 = self.dropout(pooled_output_2)

        logits_1 = self.node_output(pooled_output_1)
        logits_2 = self.node_output(pooled_output_2)

        if "shared_encoder" not in self.config:
            input_ids = self._get_variable(self._get_long_tensor(input_ids))
            input_mask = self._get_variable(self._get_long_tensor(input_mask))
            segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

            outputs = self.bert_model_edge(input_ids, attention_mask=input_mask,
                                      token_type_ids=segment_ids, position_ids=None, head_mask=None)
            pooled_output = outputs[1]
            pooled_output = self.dropout(pooled_output)

            hidden = self.hidden2hidden(torch.cat([pooled_output, logits_1, logits_2], 1))
            logits = self.hidden2label(hidden)
        else:
            hidden = self.hidden2hidden(torch.cat([pooled_output_1, pooled_output_2, logits_1, logits_2], 1))
            logits = self.hidden2label(hidden)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

