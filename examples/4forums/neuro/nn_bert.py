import logging

import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel

from drail.neuro.nn_model import NeuralNetworks


class BertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        #self.output_dim = 3
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = "bert-base-uncased"
        if "bert_model_type" in self.config:
            self.bert_model_name = self.config["bert_model_type"]
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        if "shared_encoder" in self.config:
            name = self.config["shared_encoder"]
            self.bert_model = shared_params[name]["bert"]
        else:
            self.bert_model = AutoModel.from_pretrained(self.bert_model_name)

        if "bert_freeze" in self.config and self.config["bert_freeze"]:
            for name, W in self.bert_model.named_parameters():
                print("freezing", name)
                W.requires_grad = False
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        if "shared_output" in self.config:
            name = self.config["shared_output"]
            self.hidden2label = shared_params[name]["layer"]
        else:
            self.hidden2label = torch.nn.Linear(bert_config.hidden_size, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []
        if len(x['input'][0]) == 2:
            for elem in x['input']:
                input_ids.append(elem[0][0] + elem[1][0])
                input_mask.append(elem[0][1] + elem[1][1])
                segment_ids.append([0] * len(elem[0][0]) + [1] * len(elem[1][0]))
        elif len(x['input'][0]) == 1:
            for elem in x['input']:
                input_ids.append(elem[0][0])
                input_mask.append(elem[0][1])
                segment_ids.append([0] * len(elem[0][0]))
        else:
            print("Current BERT doesn't support more than 2 sequences")
            exit()

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

        #print(input_ids.size(), input_mask.size(), segment_ids.size())

        # Run model
        #print(input_ids.size())
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)

        logits = self.hidden2label(pooled_output)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

