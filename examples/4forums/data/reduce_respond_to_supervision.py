import sys
import json
import random

random.seed(1234)

issue = sys.argv[1]
fold = sys.argv[2]

with open("folds.json") as fp:
    allfolds = json.load(fp)

folds = allfolds[issue]
prop = 0.1
print issue, fold

dev_fold = (int(fold) + 1) % 5
test_fold = int(fold)

train_folds = [i for i in range(0,5) if i not in set([dev_fold, test_fold])]
threads_train = []
for i in train_folds:
    threads_train += folds[i]
threads_train = map(str, threads_train)
threads_train = set(threads_train)

with open("{0}/respond_to.txt".format(issue)) as fp:
    new_respond_to = set([])
    for line in fp:
        (thread, p1, p2) = line.strip().split()
        if thread in threads_train:
            r = random.uniform(0, 1)
            if r <= prop:
                new_respond_to.add((thread, p1, p2))
        else:
            new_respond_to.add((thread, p1, p2))

with open("{0}/respond_to_0.1_fold0.txt".format(issue), "w") as fp:
    for (thread, p1, p2) in new_respond_to:
        fp.write("{0}\t{1}\t{2}\n".format(thread, p1, p2))
