# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
from sklearn.metrics import *

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-f', '--fedir', help='fe directory', dest='fedir', type=str, required=True)
    parser.add_argument("--bert", action="store_true", default=False)
    parser.add_argument("--bert_tiny", action="store_true", default=False)
    parser.add_argument("--wordembed", action="store_true", default=False)
    parser.add_argument('--issue', type=str, required=True)
    parser.add_argument('-p', '--pkldir', help='pkl directory', dest='pkldir', type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument('--folds', help='folds to extract', type=str, required=True)
    parser.add_argument('--fold_type', help='[thread|post]', type=str, required=True)
    args = parser.parse_args()
    return args

def train(folds, avoid):
    ret = []
    for j in range(0, len(folds)):
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    # Fixed resources
    args = parse_arguments()

    if args.wordembed and not args.bert:
        POSTS_F = "examples/4forums/data/posts_preprocessed.json"
        WORD2IDX_F = "examples/4forums/data/word2idx.json"
        # TO-DO: Receive this as input
        WORD_EMB_F = "/scratch/pachecog/GloVe/glove.840B.300d.txt"
    elif args.bert and not args.wordembed:
        POSTS_F = "examples/4forums/data/posts_bert.json"
        WORD2IDX_F = None; WORD_EMB_F = None
    elif args.bert_tiny and not args.wordembed:
        POSTS_F = "examples/4forums/data/posts_bert_tiny.json"
        WORD2IDX_F = None; WORD_EMB_F = None
    else:
        print("Choose only one: --wordembed or (--bert | --bert_tiny)")
        exit(-1)

    FOLDS = json.load(open(args.folds))

    # seed
    np.random.seed(1234)
    random.seed(1234)

    learner=LocalLearner()
    learner.compile_rules(args.rules)
    db=learner.create_dataset(os.path.join(args.dir, args.issue))

    for i in range(0, 5):
        print("Fold", i)
        dev_fold = (i + 1) % 5

        test_threads = FOLDS[args.issue][i]
        dev_threads = FOLDS[args.issue][dev_fold]
        train_threads = train(FOLDS[args.issue], [i, dev_fold])

        if args.debug:
            train_threads = train_threads[:10]
            dev_threads = dev_threads[:10]
            test_threads = test_threads[:10]
        print("{0} folds:".format(args.fold_type), len(train_threads), len(dev_threads), len(test_threads))
        print("{0} folds:".format(args.fold_type), len(train_threads), len(dev_threads), len(test_threads))

        if args.fold_type == "post":
            db.add_filters(filters=[
                ("InThread", "isTrain", "postId_2", train_threads),
                ("InThread", "isDev", "postId_2", dev_threads),
                ("InThread", "isTest", "postId_2", test_threads),
                ("InThread", "isDummy", "postId_2", train_threads[:10])
            ])
            print(train_threads[:1])
        elif args.fold_type == "thread":
            db.add_filters(filters=[
                ("InThread", "isTrain", "threadId_1", train_threads),
                ("InThread", "isDev", "threadId_1", dev_threads),
                ("InThread", "isTest", "threadId_1", test_threads),
                ("InThread", "isDummy", "threadId_1", train_threads[:10])
            ])
        else:
            print("Supported fold types are thread and post")
            exit()

        learner.build_feature_extractors(db,
                                         word_emb_f=WORD_EMB_F,
                                         data_f=POSTS_F,
                                         word2idx_f=WORD2IDX_F,
                                         use_wordembed=args.wordembed,
                                         debug=args.debug,
                                         femodule_path=args.fedir,
                                         filters=[("InThread", "isDummy", 1)])

        picklepath = os.path.join(args.pkldir, "f{0}".format(i))
        if not os.path.exists(picklepath):
            os.makedirs(picklepath)

        learner.extract_data(
                db,
                train_filters=[("InThread", "isTrain", 1)],
                dev_filters=[("InThread", "isDev", 1)],
                test_filters=[("InThread", "isTest", 1)],
                extract_train=True,
                extract_dev=True,
                extract_test=True,
                pickleit=True,
                pickledir=picklepath)

if __name__ == "__main__":
    main()
