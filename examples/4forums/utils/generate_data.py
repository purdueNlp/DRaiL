import os
import sys
import json
import random
import numpy as np
import re

def preprocess_name(name):
    if len(name.split()) == 1:
        return name
    if name == "evolution involves more than purely natural mechanisms (intelligent design)":
        return "pro-intelligent-design"
    elif name == "evolution occurs via purely natural mechanisms":
        return "pro-evolution"
    elif name == "gay marriage should be illegal":
        return "con-legal"
    elif name == "gay marriage should be legal":
        return "pro-legal"
    elif name == "opposes strict gun control":
        return "con-strict-control"
    elif name == "prefers strict gun control":
        return "pro-strict-control"
    return None

random.seed(1234)

# argument one is the path to the corpus fourforums_processed
assert len(sys.argv) > 1, 'usage: {} <path_to_4forum_corpus>'.format(sys.argv[0])
DIR = sys.argv[1]
ISSUES = ["abortion", "evolution", "gay_marriage", "gun_control"]
OUTPUT_FOLDER = 'data'


# create folders when needed
if not os.path.isdir(OUTPUT_FOLDER):
    os.mkdir(OUTPUT_FOLDER)


for iss in ISSUES:
    dir_path = os.path.join(OUTPUT_FOLDER, iss)
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)


sets = {
    "has_stance": set([]),
    "respond_to": set([]),
    "is_author": set([]),
    "in_thread": set([]),
    "disagree": set([]),
    "agree": set([]),
    "stance_label": set([])
}

posts = {}; responses = {}; stances = {}
has_stance = {}; folds = {}; thread_id = {}
thread_idx = 0

for issue in ISSUES:
    posts[issue] = {}; responses[issue] = {}
    has_stance[issue] = {}
    directory = os.path.join(DIR, issue)
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        text = ""
        with open(filepath) as fp:
            for line in fp:
                if line.startswith("ID:"):
                    _, post_id = line.strip().split(':')
                elif line.startswith("Stance:"):
                    _, stance = line.strip().split(':')
                    stance_ = preprocess_name(stance)
                    if not stance_:
                        print(stance)
                        exit()
                    stance = stance_
                    sets["stance_label"].add((issue, stance))
                    has_stance[issue][post_id] = stance
                elif line.startswith("PID:"):
                    _, r_post_id = line.strip().split(":")
                    responses[issue][post_id] = r_post_id
                elif line.startswith("Author:"):
                    _, author = line.strip().split(":")
                    author = re.sub(r"\s+", '_', author)
                    sets["is_author"].add((issue, post_id, author))
                else:
                    text += line.strip() + "[NL]"
            posts[issue][post_id] = text

    threads = {};
    allposts = set(has_stance[issue].keys())

    while len(allposts) > 0:
        #print(len(allposts))
        for key in responses[issue]:
            if key not in allposts:
                continue
            value = responses[issue][key]
            if value == "None":
                threads[thread_idx] = set([key])
                allposts.remove(key)
                sets["in_thread"].add((issue, str(thread_idx), key))
                thread_id[key] = str(thread_idx)
                thread_idx += 1
            elif len(threads) > 0:
                for idx in threads:
                    t = threads[idx]
                    if value in t:
                        threads[idx].add(key)
                        allposts.remove(key)
                        sets["in_thread"].add((issue, str(idx), key))
                        thread_id[key] = str(idx)
                        sets["respond_to"].add((issue, str(idx), key, value))
        if issue == "abortion" and len(allposts) == 1:
            pidx = list(allposts)[0]
            if pidx == "318700381":
                sets["in_thread"].add((issue, thread_id["318700379"], pidx))
                thread_id[pidx] = thread_id["318700379"]
                allposts.remove(pidx)
    #print(threads)

    thread_ids = list(threads.keys())
    random.shuffle(thread_ids)
    folds[issue] = np.array_split(thread_ids, 5)
    folds[issue] = [x.tolist() for x in folds[issue]]

for (i, t, p1, p2) in sets["respond_to"]:
    if p1 == "318700380" or p2 == "318700380":
        continue
    if has_stance[i][p1] != has_stance[i][p2]:
        sets["disagree"].add((i, t, p1, p2))
    else:
        sets["agree"].add((i, t, p1, p2))

#print(len(thread_id), len(has_stance['abortion']) + len(has_stance['evolution']) + len(has_stance['gun_control'])  + len(has_stance['gay_marriage']))
#exit()

for i in has_stance:
    for p1 in has_stance[i]:
        sets["has_stance"].add((i, thread_id[p1], p1, has_stance[i][p1]))

for filename in sets:
    filepaths = {}
    for i in ISSUES:
        filepaths[i] = open("data/{0}/{1}.txt".format(i, filename), "w")
    for xtuple in sets[filename]:
        fp = filepaths[xtuple[0]]
        fp.write("\t".join(xtuple[1:]))
        fp.write("\n")

    for i in ISSUES:
        filepaths[i].close()

with open("data/posts.json", "w") as fp:
    json.dump(posts, fp)

