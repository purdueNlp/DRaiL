import os
import sys
import json
import nltk
from transformers import AutoTokenizer, AutoModel


assert len(sys.argv) > 1, 'usage: {} <path_to_generated_data>'.format(sys.argv[0])
DIR = sys.argv[1]
dir_path = os.path.join(DIR, 'posts.json')
posts = json.load(open(dir_path))
posts_preprocessed = {}; posts_bert = {}; posts_bert_tiny = {}
word_indices = {}

bert_tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)
bert_tiny_tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-2_H-128_A-2", do_lower_case=True)

word_idx = 0
for issue in posts:
    for post_id in posts[issue]:
        text = posts[issue][post_id]
        paragraphs = text.split('[NL]')
        paragraphs_preprocessed = []
        paragraphs_bert = []; paragraphs_bert_tiny = []
        for par in paragraphs:
            bert_encoding = bert_tokenizer.encode(par)
            paragraphs_bert += bert_encoding

            bert_encoding_tiny = bert_tiny_tokenizer.encode(par)
            paragraphs_bert_tiny += bert_encoding_tiny

            tokens = nltk.word_tokenize(par)

            for tok in tokens:
                if tok not in word_indices:
                    word_indices[tok] = word_idx
                    word_idx += 1
                paragraphs_preprocessed.append(word_indices[tok])
        posts_preprocessed[post_id] = paragraphs_preprocessed
        posts_bert[post_id] = paragraphs_bert
        posts_bert_tiny[post_id] = paragraphs_bert_tiny

dir_path = os.path.join(DIR, 'posts_preprocessed.json')
with open(dir_path, "w") as fp:
    json.dump(posts_preprocessed, fp)

dir_path = os.path.join(DIR, 'posts_bert.json')
with open(dir_path, "w") as fp:
    json.dump(posts_bert, fp)

dir_path = os.path.join(DIR, 'posts_bert_tiny.json')
with open(dir_path, "w") as fp:
    json.dump(posts_bert_tiny, fp)

dir_path = os.path.join(DIR, 'word2idx.json')
with open(dir_path, "w") as fp:
    json.dump(word_indices, fp)
