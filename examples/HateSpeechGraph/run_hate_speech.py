import sys
import numpy as np
import os

from drail.learn.local_learner import LocalLearner

def getopts(argv):

    opts = {}
    while argv:
        if argv[0][0]=='-':
            opts[argv[0][1:]] = argv[1]
        argv = argv[1:]
    return opts

def get_fold_ids(data_file_path, fold):
    filter_ids = []
    curr_path = os.path.join(data_file_path, 'fold{0}.txt'.format(fold))
    with open(curr_path) as f:
        for line in f:
            filter_ids.append(line.strip())
    return filter_ids

def get_splits(data_file_path, test_fold):
    dev_fold = (test_fold + 1) % 5

    filter_train = []
    for train_fold in range(5):
        if train_fold != test_fold and train_fold != dev_fold:
            filter_train += get_fold_ids(data_file_path, train_fold)

    filter_dev = get_fold_ids(data_file_path, dev_fold)
    filter_test = get_fold_ids(data_file_path, test_fold)
    return filter_train, filter_dev, filter_test

def main():

    opts = getopts(sys.argv)

    rule_file = os.path.join(sys.argv[1], opts.get('rulefile', 'rule.dr'))
    arch_file = os.path.join(sys.argv[1], opts.get('conffile', 'config.json'))

    learner = LocalLearner()

    learner.compile_rules(rule_file)

    for i in range(5):

        train, dev, test = get_splits(sys.argv[1], i)

        db=leaner.create_dataset(sys.argv[1])

        learner.build_feature_extractors(db, train, filter_pred="Tweet")

        learner.build_models(arch_file)

        learner.train(db, train, dev, test, filter_pred="Tweet")

        if inference:
            learner.predict(db, test, filter_pred="Tweet")

        break

if __name__=='__main__':
    main()
