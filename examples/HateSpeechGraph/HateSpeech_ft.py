from drail.features.feature_extractor import FeatureExtractor
import json
import numpy as np

class HateSpeech_ft(FeatureExtractor):

    def __init__(self, instance_grds):
        super(HateSpeech_ft, self).__init__(instance_grds)

    def build(self):
        
        self.label_idx = {'racism': 0, 'sexism': 1, 'none': 2}

    def extract_multiclass_head(self, instance_grd):
        label = instance_grd.get_head_predicate()
        return self.label_idx[label['arguments'][1]]

    def extract_label(self, instance_grd):

        hateSpeech = instance_grd.get_body_predicates("HateSpeech")[0]

        return [i==self.label_idx[hateSpeech['arguments'][1]] for i in range(3)]
