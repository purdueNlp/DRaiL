import json
import optparse
from tweepy import *
import time

parser = optparse.OptionParser()
parser.add_option('--follow', help='boolean option', dest='extract_follow', \
    default=False, action='store_true')
parser.add_option('--rt', help='boolean option', dest='extract_rts', \
    default=False, action='store_true')
parser.add_option('--fav', help='boolean option', dest='extract_fav', \
    default=False, action='store_true')
(opts, args) = parser.parse_args()

with open("users.json") as f:
    users = json.load(f)

with open("tweets.json") as f:
    tweets = json.load(f)

with open("credentials.json") as f:
    credentials = json.load(f)

auth = OAuthHandler(credentials['consumer_key'], credentials['consumer_secret'])
auth.set_access_token(credentials["access_token"], credentials["access_token_secret"])
api = API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

written_follow_edges = set([])
with open("follows.txt", 'r') as f:
    for line in f:
        x = line.strip().split('\t')
        if len(x) == 2:
            (u1, u2) = x
            written_follow_edges.add((int(u1), int(u2)))

print "Initially we have", len(written_follow_edges)

if opts.extract_follow:
    print "FOLLOWERS"
    user_ids = set(map(int, users.keys()))
    problematic_id = 2541902209; found = False

    print "NUMBER OF USERS", len(user_ids)
    for i, user_id in enumerate(user_ids):
        
        if user_id == problematic_id:
            found = True
            print "Processed so far", (1.0 * (i+1)) / len(user_ids)

        if not found:
            continue
        

        #print "extracting followers for", user_id
        followers = []; friends = []
        cursor = Cursor(api.followers_ids, user_id=user_id).pages()
        while True:
            try:
                page = cursor.next()
                followers.extend(page)
            except error.TweepError as e:
                if e.reason == "Not authorized.":
                    print "Failed to retrieve followers for", user_id
                    break
                elif e.reason == "[{u'message': u'Sorry, that page does not exist.', u'code': 34}]":
                    print "Failed to retrieve followers for", user_id
                    break
                else:
                    print e.reason
                    time.sleep(180)
                    continue
            except StopIteration:
                break

        #print "extracting friends for", user_id
        cursor = Cursor(api.friends_ids, user_id=user_id).pages()
        while True:
            try:
                page = cursor.next()
                friends.extend(page)
            except error.TweepError as e:
                if e.reason == "Not authorized.":
                    print "Failed to retrieve friends for", user_id
                    break
                elif e.reason == "[{u'message': u'Sorry, that page does not exist.', u'code': 34}]":
                    print "Failed to retrieve friends for", user_id
                    break
                else:
                    print e.reason
                    time.sleep(180)
                    continue
            except StopIteration:
                break

        local_followers = set(followers) & user_ids
        local_friends = set(friends) & user_ids
        #print "\tlocal followers", len(local_followers)
        #print "\tlocal friends", len(local_friends)

        follow_edges = set([])
        for follower in local_followers:
            follow_edges.add((follower, user_id))
        for friend in local_friends:
            follow_edges.add((user_id, friend))

        if len(follow_edges) > 0:
            with open("follows.txt", 'a') as f:
                for (u1, u2) in follow_edges:
                    if (u1, u2) not in written_follow_edges:
                        f.write(str(u1) + "\t" + str(u2) + "\n")
                        written_follow_edges.add((u1, u2))
        print "Written", len(written_follow_edges), "edges"
if opts.extract_rts:
    tweet_ids = set(tweets.keys())
    user_ids = set(users.keys())

    rt_edges = set([])

    for tw_id in tweet_ids:
        print tw_id
        user = str(tweets[tw_id]['user'])

        try:
            retweets = api.retweets(tw_id)
        except tweepy.error.TweepError as e:
            retweets = []

        retweeters = [tw._json['user']['id_str'] for tw in retweets]
        local_retweeters = set(retweeters) & user_ids

        for retweeter in local_retweeters:
            rt_edges.add((retweeter, user))
        
        if len(rt_edges) > 0:
            print "added", len(rt_edges), "retweet edges"
            print rt_edges 
    with open("retweets.txt", 'w') as f:
        for (u1, u2) in rt_edges:
            f.write(u1 + "\t" + u2 + "\n")


