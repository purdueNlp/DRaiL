import tweepy
import json
import sys
import os

credentials_file = sys.argv[1]

tweet_files = []
for i in range(2, len(sys.argv)):
    tweet_files.append(sys.argv[i])

with open(credentials_file) as f:
    credentials = json.load(f)

tweet_ids = []; labels = []
for tf in tweet_files:
    print tf
    with open(tf) as f:
       for line in f:
            tweet_id, label = line.strip().split(',', 1)
            label = label.split()
            tweet_ids.append(tweet_id)
            labels.append(label)
print len(tweet_ids), len(set(tweet_ids))
print "processing", len(tweet_ids), "tweet IDs"

auth = tweepy.OAuthHandler(credentials['consumer_key'], credentials['consumer_secret'])
auth.set_access_token(credentials["access_token"], credentials["access_token_secret"])

api = tweepy.API(auth)

# get tweets 100 by 100
tweets_dict = {}
users_dict = {}
prev = 0;
i = min(len(tweet_ids), 100)

while prev < len(tweet_ids):
    tw_ids = tweet_ids[prev:i]
    lbls = labels[prev:i]
    tweets = api.statuses_lookup(id_=tw_ids, include_entities=True)
    tweets = dict((tw.id_str, tw._json) for tw in tweets)
    
    for twid, lbl in zip(tw_ids, lbls):
        
        if twid not in tweets:
            continue

        tw = tweets[twid]
        user = tw['user']
        tw['user'] = user['id']
        
        # store tweets
        if tw['id'] in tweets_dict:
            tweets_dict[tw['id']]['labels'].append(lbl)
        else:
            tweets_dict[tw['id']] = tw
            tweets_dict[tw['id']]['labels'] = [lbl]

        # store users
        if user['id'] not in users_dict:
            users_dict[user['id']] = user
    
    prev = i
    i += 100

print len(tweets_dict.keys())
print len(users_dict.keys())

with open("tweets.json", 'w') as f:
    json.dump(tweets_dict, f)

with open("users.json", 'w') as f:
    json.dump(users_dict, f)
