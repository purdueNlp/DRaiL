import skipthoughts
import json
import re

sentences = json.load(open("debate_sentences.json"))
debates_all = json.load(open("debates.json"))
sentence_id = {}
sentences_to_extract = []

idx = 0
for dname in sentences:
    # Add all sentences in debate
    for side in ['pro', 'con']:
        if side not in sentences[dname]:
            continue
        for par in sentences[dname][side]:
            for sent in par:
                if sent not in sentence_id:
                    sentence_id[sent] = idx
                    idx += 1
                    sentences_to_extract.append(sent)

    # Add title
    sent = debates_all[dname]['title']
    if sent not in sentence_id:
        sentence_id[sent] = idx
        idx += 1
        sentences_to_extract.append(sent)

# Extract skipthoughts

# remove empty sentences
sentences = [sent for sent in sentences_to_extract if re.match(r'^\s*$', sent) is None]

print "Number of sentences:", len(sentences), len(sentence_id)
model = skipthoughts.load_model()
encoder = skipthoughts.Encoder(model)
vectors = encoder.encode(sentences)

f_skipthoughts = open('skipthoughts/skipthoughts_sentences.bin', 'wb')

f_skipthoughts.write(str(len(sentences)))
f_skipthoughts.write(' 4800\n')

for (sent, vect) in zip(sentences, vectors):
    f_skipthoughts.write(str(sentence_id[sent]))
    f_skipthoughts.write(' ')
    f_skipthoughts.write(vect.tobytes())
    f_skipthoughts.write('\n')
f_skipthoughts.close()

with open('skipthoughts/skipthoughts_sentid.json', 'w') as f:
    print "Sentences in files", len(sentence_id)
    json.dump(sentence_id, f)
