import json
import os

# load relevant debates
fname = "data/debate_posts_noquotes.json"
debates = json.load(open(fname))

fname = "data/debates.json"
debates_all = json.load(open(fname))

# create a dictionary that maps username to ids
username_id = {}
with open('../../../../debate-data/usernames.txt') as f:
    for i, line in enumerate(f):
        username_id[line.strip()] = str(i).zfill(8)

votes = {}; vote_edges = 0

for dname in debates:
    contender = debates_all[dname]['contender']
    instigator = debates_all[dname]['instigator']

    contender = debates_all[dname][contender]
    instigator = debates_all[dname][instigator]

    votes[dname] = {contender: [], instigator: []}

    filename = os.path.join("../../../../debate-data/votes", "{0}.txt".format(dname))
    if os.path.isfile(filename):
        with open(filename) as f:
            instigator_pts = None; contender_pts = None
            for line in f:
                fields = line.strip().split()
                if len(fields) != 9:
                    break
                user_id = username_id[fields[0]]
                if int(fields[1]) == 0:
                    agreewith = instigator
                elif int(fields[1]) == 1:
                    agreewith = contender
                else:
                    continue
                votes[dname][agreewith].append(user_id)


with open("data/votes.json", "w") as fp:
    json.dump(votes, fp)
