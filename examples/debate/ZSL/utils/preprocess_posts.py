import json
import difflib
import re

def common_subsequences(a, b, threshold):
    # tokenize two string (keep order)
    tokens_a = a.split()
    tokens_b = b.split()
    # store all common subsequences
    common = set()
    # with each token in a
    i = 0
    while i < len(tokens_a):
        token = tokens_a[i]
        #print i, token
        # if it also appears in b
        # then this should be a starting point for a common subsequence
        token = token.strip('"')
        if token in tokens_b:
            # get the first occurence of token in b
            # and start from there
            j = tokens_b.index(token)
            k = i
            temp = token
            # since we need all subsequences, we get length-1 subsequences too
            #common.add(temp)
            # while still have token in common
            while j < len(tokens_b) and k < len(tokens_a):
                if j + 1 < len(tokens_b) and k + 1 < len(tokens_a):
                    #print tokens_b[j+1], tokens_a[k+1]
                    if tokens_b[j+1].strip('"') == tokens_a[k+1].strip('"'):
                        temp += " " + tokens_b[j+1]
                        j += 1
                        k += 1
                    # adding (new) common subsequences
                    #common.add(temp)
                # or else we break
                    else:
                        break
                else:
                    break
            #print "temp", temp
            common.add(temp)
            i += len(temp.split()) - 1
        i += 1
    # we only get the ones having length >= threshold
    return [s for s in common if len(s.split()) >= threshold]

with open("data/debates.json") as f:
    debates = json.load(f)

debates_new = dict(debates)

for dname in debates:

    instigator = debates[dname]['instigator']
    contender = debates[dname]['contender']
    instigator_paragraphs = "{0}_paragraphs".format(instigator)
    contender_paragraphs = "{0}_paragraphs".format(contender)

    curr_instigator_par = 0
    for i, par in enumerate(debates[dname][contender_paragraphs]):
        #print "Paragraph", i
        if curr_instigator_par >= len(debates[dname][instigator_paragraphs]):
            break
        '''
        print "====="
        print par
        print "..."
        print debates[dname][instigator_paragraphs][curr_instigator_par]
        print "====="
        '''
        matches = common_subsequences(
                par,
                debates[dname][instigator_paragraphs][curr_instigator_par],
                5)

        # remove text from contender that was said by instigator on post before
        for match in matches:
            match = re.escape(match)
            par = re.sub(match, '', par)

        debates[dname][contender_paragraphs][i] = par
        curr_instigator_par += 1

    curr_contender_par = 0
    for i, par in enumerate(debates[dname][instigator_paragraphs][1:]):
        if curr_contender_par >= len(debates[dname][contender_paragraphs]):
            break

        matches = common_subsequences(
                par,
                debates[dname][contender_paragraphs][curr_contender_par],
                5)

        # remove text from instigator that was said by contender before
        for match in matches:
            match = re.escape(match)
            par = re.sub(match, '', par)

        debates[dname][instigator_paragraphs][i+1] = par
        curr_contender_par += 1

with open("data/debates_noquotes.json", 'w') as f:
    json.dump(debates, f)
