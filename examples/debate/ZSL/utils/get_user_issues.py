import json
import os
from collections import Counter

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)

debates_all = json.load(open("data/debates.json"))
print len(debates_all)
exit()

USERS = set([])
for dname in debates_all:
    USERS.add(debates_all[dname]['pro'])
    USERS.add(debates_all[dname]['con'])

issues_pro = {}; issues_con = {}
all_issues = []
missing_users = 0
for user in USERS:
    filename = os.path.join("/scratch/pachecog/debate-data/issues", "{0}.txt".format(user))

    if os.path.isfile(filename):
        with open(filename) as f:
            for line in f:
                pr = line.split('Comments')
                issue, stance = pr[0].split(':')
                issue = issue.strip()
                stance = stance.strip()


                '''if issue not in relevant:
                    continue
                '''

                if stance == "Pro":
                    if issue not in issues_pro:
                        issues_pro[issue] = set([user])
                    else:
                        issues_pro[issue].add(user)
                    all_issues.append(issue)
                elif stance == "Con":
                    if issue not in issues_con:
                        issues_con[issue] = set([user])
                    else:
                        issues_con[issue].add(user)
                    all_issues.append(issue)
                else:
                    #print stance
                    pass

print Counter(all_issues)

with open("data/issues_pro.json", 'w') as f:
    json.dump(issues_pro, f, cls=SetEncoder)

with open("data/issues_con.json", 'w') as f:
    json.dump(issues_con, f, cls=SetEncoder)

