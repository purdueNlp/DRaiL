import json
import skipthoughts
import re
import spacy
import en_core_web_sm

sentences = []
sentences_id = {}

current_id = 0
nlp = en_core_web_sm.load()

with open("../ProIssue/docs.txt") as f:
    for line in f:
        user, summary = line.split(' ', 1)
        doc = nlp(unicode(summary, 'utf8'))
        curr_sentences = [sent.string.strip() for sent in doc.sents
                          if re.search('\w', sent.string.strip()) is not None]

        for sent in curr_sentences:
            if sent not in sentences_id:
                sentences_id[sent] = current_id
                current_id += 1
                sentences.append(sent)


model = skipthoughts.load_model()
encoder = skipthoughts.Encoder(model)
vectors = encoder.encode(sentences)


f_skipthoughts = open('skipthoughts_sentences_summaries.bin', 'wb')

f_skipthoughts.write(str(len(sentences)))
f_skipthoughts.write(' 4800\n')

for (sent, vect) in zip(sentences, vectors):
    f_skipthoughts.write(str(sentences_id[sent]))
    f_skipthoughts.write(' ')
    f_skipthoughts.write(vect.tobytes())
    f_skipthoughts.write('\n')
f_skipthoughts.close()

with open('skipthoughts_sentid_summaries.json', 'w') as f:
    print "Sentences in files", len(sentences_id)
    json.dump(sentences_id, f)
