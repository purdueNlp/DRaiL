import glob
import gensim
import json
import sys
from nltk.tokenize import TweetTokenizer

debates = glob.glob("/scratch/pachecog/debate-data/debates/*_info.txt")
num = len(debates)
print "All debates scrapped", num

tknzr = TweetTokenizer()


vocabulary = {}
print "Preprocessing..."
training = []; index = 0
for i, debate in enumerate(debates):
    with open(debate) as fp:
        fp.readline()
        title = fp.readline().split(':', 1)[1].strip()
        if len(title) < 1:
            continue
        words = tknzr.tokenize(title.lower())
        for w in words:
            if w not in vocabulary:
                vocabulary[w] = index
                index += 1

print "Dumping vocab..."
with open("../data/title_vocabulary.json", "w") as fp:
    json.dump(vocabulary, fp)
