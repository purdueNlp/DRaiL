import json
import os

# load relevant debates
fname = "data/debate_sentences_noquotes.json"
debates = json.load(open(fname))

# load all debates (to access title and authors)
fname = "data/debates.json"
debates_all = json.load(open(fname))

# load profile information
fname = "data/user_info.json"
user_info = json.load(open(fname))

fname = "data/issues_pro.json"
issues_pro = json.load(open(fname))
fname = "data/issues_con.json"
issues_con = json.load(open(fname))


def political_leaning(user, issues_pro, issues_con):
    liberal_stances = 0; conservative_stances = 0

    for issue in ["Abortion", "Gay Marriage", "Drug Legalization", "Environmental Protection", "Global Warming Exists",
                  "Medical Marijuana", "National Health Care"]:
        if user in issues_pro[issue]:
            liberal_stances += 1
        if user in issues_con[issue]:
            conservative_stances += 1
    for issue in ["Death Penalty", "Gun Rights", "War on Terror", "War in Afghanistan", "Border Fence"]:
        if user in issues_pro[issue]:
            conservative_stances += 1
        if user in issues_con[issue]:
            liberal_stances += 1

    return (liberal_stances, conservative_stances)

for dname in debates:
    contender = debates_all[dname]['contender']
    instigator = debates_all[dname]['instigator']

    contender = debates_all[dname][contender]
    instigator = debates_all[dname][instigator]

    for user in [contender, instigator]:

        lib, con = political_leaning(user, issues_pro, issues_con)

        # User leans liberal
        if (user in user_info) and ("Party" in user_info[user] or "Ideology" in user_info[user]) and\
           (user_info[user]["Party"] == "Democratic Party" or user_info[user]["Ideology"] == "Liberal"):
            user_info[user]["Leaning"] = "Liberal"

        # User leans conservative
        if (user in user_info) and ("Party" in user_info[user] or "Ideology" in user_info[user]) and\
           (user_info[user]["Party"] == "Republican Party" or user_info[user]["Ideology"] == "Conservative"):
            user_info[user]["Leaning"] = "Conservative"


        if user not in user_info or "Leaning" not in user_info[user]:
            if user not in user_info:
                user_info[user] = {}

            if lib > con:
                user_info[user]["Leaning"] = "Liberal"
            elif con > lib:
                user_info[user]["Leaning"] = "Conservative"
            else:
                user_info[user]["Leaning"] = None

    '''
    print "--- contender"
    print user_info[contender]
    print "--- instigator"
    print user_info[instigator]
    print "============="
    '''

with open("data/user_info_updated.json", 'wb') as f:
    json.dump(user_info, f)

