import os
from glob import glob
import re
import json
import random

random.seed(1234)
username_id = {}
with open('/scratch/pachecog/debate-data/usernames.txt') as f:
    for i, line in enumerate(f):
        username_id[line.strip()] = str(i).zfill(8)

with open("/scratch/pachecog/DRaiL/examples/debate/ZSL/data/debates.json") as fp:
    debates = json.load(fp)
print len(debates)

# Political Issues
abort = '.*(abort|pregnan|pro-choice|prochoice|pro-life|prolife|birth control).*'; n_abort = 0
lgbt = '.*(lgbt|gay|homosexual|transgender|lesbian|queer|same sex|same-sex|sexual orientation|sexuality).*'; n_lgbt = 0
guns = '.*(gun|riffle|firearm|weapon|second amendment|semi automatic|semi-automatic|semiautomatic|2nd amendment|open carry).*'; n_guns = 0
isis = '.*((^|\W)isis(\W|$)|terrorism|terrorist|war on terror|al qaeda|alqaeda|al-qaeda|osama|islamic state).*'; n_isis = 0
immi = '.*(immigra|border|migration|undocumented|(^|\W)fence(\W|$)|deport).*'; n_immi = 0
care = '.*(obamacare|healthcare|(^|\W)aca($|\W)|affordable care act|health care|obama care|medicare|medicaid).*'; n_care = 0
dpen = '.*(death penal|capital punishment|death sentence).*'; n_dpen = 0
race = '.*((\W|^)race(\W|$)|racism|racist|white|black|supremac|african american|african-american|kkk|racial|people of color|hispanic|asian|affirmative action|minorit|ethnic).*'; n_race = 0
femn = '.*(feminis|women|gender|female|(\W|^)male(\W|$)|sexual assault|rape|woman|sexism|sexist|domestic violence|rapist|consent|masculinity).*'; n_femn = 0
weed = '.*(marijuana|weed|cannabis|legalize pot).*'; n_weed = 0
drugs = '.*drug.*'; n_drugs = 0
tax = '.*tax.*'; n_tax = 0
climate = '.*(climate change|pollution|recycl|sustainab|environment|clean energy|renewable|co2|global warming|endangered species).*'; n_climate = 0
speech = '.*(speech|first amendment|1st amendment|freedom of expression|censorship).*'; n_speech = 0
wage = '.*(wage).*'; n_wage = 0
welfare = '.*(welfare|social service|social program).*'; n_welfare = 0
elect = '.*(electoral college|popular vote|direct vote).*'; n_elect = 0
polsci = '.*(socialis|capitalis|fascis|communis|democra|anarach|free market|free-market|marx|statism).*'; n_polsci = 0
# Politicians
obama = '.*obama.*'; n_obama = 0
bush = '.*bush.*'; n_bush = 0
clinton = '.*(clinton|hillary).*'; n_clinton = 0
trump = '.*trump.*'; n_trump = 0

n_debates = 0; n_new = 0
charged_debates = []

for fname in glob("/scratch/pachecog/debate-data/debates/*_info.txt"):
    n_debates += 1
    counted = False
    with open(fname) as f:
        dname = fname[-15:-9]
        pro_username = None; con_username = None; dname = None

        for i, line in enumerate(f):
            fields = line.strip().split(':')

            if i == 2 and fields[0] == "Pro Username":
                instigator = "pro"; contender = "con"
            elif i == 2 and fields[0] == "Con Username":
                instigator = "con"; contender = "pro"

            if fields[0] == "Pro Username":
                pro_username = fields[1]
            if fields[0] == "Con Username":
                con_username = fields[1]
            if fields[0]  == "Debate No":
                dname = fields[1].zfill(6)

            if fields[0] == "Category":
                dcategory = fields[1]

            if fields[0] == "Title":
                dtitle = fields[1]

                if re.match(abort, dtitle.lower()):
                    n_abort += 1
                    counted = True
                elif re.match(lgbt, dtitle.lower()):
                    n_lgbt += 1
                    counted = True
                elif re.match(guns, dtitle.lower()):
                    n_guns += 1
                    counted = True
                elif re.match(isis, dtitle.lower()):
                    n_isis += 1
                    counted = True
                elif re.match(immi, dtitle.lower()):
                    print dtitle
                    n_immi += 1
                    counted = True
                elif re.match(care, dtitle.lower()):
                    n_care += 1
                    counted = True
                elif re.match(dpen, dtitle.lower()):
                    n_dpen += 1
                    counted = True
                elif re.match(race, dtitle.lower()):
                    n_race += 1
                    counted = True
                elif re.match(femn, dtitle.lower()):
                    n_femn += 1
                    counted = True
                elif re.match(weed, dtitle.lower()):
                    n_weed += 1
                    counted = True
                elif re.match(obama, dtitle.lower()):
                    n_obama += 1
                    counted = True
                elif re.match(clinton, dtitle.lower()):
                    n_clinton += 1
                    counted = True
                elif re.match(bush, dtitle.lower()):
                    n_bush += 1
                    counted = True
                elif re.match(trump, dtitle.lower()):
                    n_trump += 1
                    counted = True
                elif re.match(drugs, dtitle.lower()):
                    n_drugs += 1
                    counted = True
                elif re.match(tax, dtitle.lower()):
                    n_tax += 1
                    counted = True
                elif re.match(climate, dtitle.lower()):
                    n_climate += 1
                    counted = True
                elif re.match(speech, dtitle.lower()):
                    n_speech += 1
                    counted = True
                elif re.match(wage, dtitle.lower()):
                    n_wage += 1
                    counted = True
                elif re.match(welfare, dtitle.lower()):
                    n_welfare += 1
                    counted = True
                elif re.match(elect, dtitle.lower()):
                    n_elect += 1
                    counted = True
                elif re.match(polsci, dtitle.lower()):
                    n_polsci += 1
                    counted = True
                    #print dtitle
            if not counted and fields[0] == 'Category' and fields[1] == 'Politics':
                #print dtitle
                pass
        if counted:
            charged_debates.append(dname)
        if counted and dname not in debates:
            n_new += 1
            fnew = open("/scratch/pachecog/debate-data/debates/{0}_pro.txt".format(dname))
            pro_paragraphs = fnew.readlines()
            fnew.close()
            debates[dname] = {'pro': username_id[pro_username],
                              'contender': contender,
                              'instigator': instigator,
                              'title': dtitle,
                              'category': dcategory,
                              'pro_paragraphs': pro_paragraphs}
            fnew = open("/scratch/pachecog/debate-data/debates/{0}_con.txt".format(dname))
            con_paragraphs = fnew.readlines()
            fnew.close()
            if dname not in debates:
                debates[dname] = {'con': username_id[con_username],
                                  'contender': contender,
                                  'instigator': instigator,
                                  'title': dtitle,
                                  'category': dcategory,
                                  'con_paragraphs': con_paragraphs}
            else:
                debates[dname]['con'] = username_id[con_username]
                debates[dname]['con_paragraphs'] = con_paragraphs

exit()
print n_debates, n_new
print n_abort, n_lgbt, n_guns, n_isis, n_immi, n_care, n_dpen, n_race, n_femn, n_weed, n_obama, n_clinton, n_bush, n_trump, n_drugs, n_tax, n_climate, n_speech, n_wage, n_welfare, n_elect, n_polsci
print sum([n_abort, n_lgbt, n_guns, n_isis, n_immi, n_care, n_dpen, n_race, n_femn, n_weed, n_obama, n_clinton, n_bush, n_trump, n_drugs, n_tax, n_climate, n_speech, n_wage, n_welfare, n_elect, n_polsci])
print len(debates)

with open("/scratch/pachecog/DRaiL/examples/debate/ZSL/data/debates.json", "wb") as fp:
    json.dump(debates, fp)


random.shuffle(charged_debates)

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

charged_folds = chunkIt(charged_debates, 5)

charged_folds_dict = {}
for i, elem in enumerate(charged_folds):
    charged_folds_dict[i] = elem

print map(len, charged_folds)

with open("/scratch/pachecog/DRaiL/examples/debate/ZSL/data/charged_folds.json", "wb") as fp:
    json.dump(charged_folds_dict, fp)

