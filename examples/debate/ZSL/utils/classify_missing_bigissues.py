import json
import os
from sklearn import linear_model, metrics
import pandas as pd
import optparse

issues_pro = json.load(open("data/issues_pro.json"))
issues_con = json.load(open("data/issues_con.json"))
user_info = json.load(open("data/user_info.json"))
debates_all = json.load(open("data/debates.json"))
num_issues = len(issues_pro)
issues = issues_pro.keys()
issues_pred = {'metrics': {}, 'users': {}}

USERS = set([])
debate_count = 0
for dname in debates_all:
    USERS.add(debates_all[dname]['pro'])
    USERS.add(debates_all[dname]['con'])
    debate_count += 1

'''
number_issues = []
for user in USERS:
    n = 0
    for issue in issues:
        if user in issues_pro[issue] or user in issues_con[issue]:
            n += 1
    number_issues.append(n)

print "Issues per user"
s = pd.Series(number_issues)
print s.describe()
print "Users with 0 count", number_issues.count(0)
'''

for i, issue in enumerate(issues):
    print("Building classifier for issue", issue)

    # Known data
    X = []; y = []
    # Unknown data
    X_new = []; users_new = []

    for user in set(user_info.keys()) | USERS:
        if user == 'features':
            continue

        if user not in issues_pred['users']:
            issues_pred['users'][user] = {}

        user_issues_pro = [0] * num_issues
        user_issues_con = [0] * num_issues

        for j, other_issue in enumerate(issues):
            if i == j:
                continue

            if user in issues_pro[other_issue]:
                user_issues_pro[j] = 1
            if user in issues_con[other_issue]:
                user_issues_con[j] = 1

        user_features = user_issues_pro + user_issues_con
        for feat in user_info['features']:
            temp = [0] * len(user_info['features'][feat])
            if user in user_info and feat in user_info[user]:
                index = user_info['features'][feat].index(user_info[user][feat])
                temp[index] = 1
            user_features += temp

        if user not in issues_pro[issue] and user not in issues_con[issue]:
            X_new.append(user_features)
            users_new.append(user)
        else:
            X.append(user_features)
            y.append(int(user in issues_pro[issue]))
            issues_pred['users'][user][issue] = {'pred': int(user in issues_pro[issue]), 'proba': int(user in issues_pro[issue])}

    print len(X), len(y)
    print len(X_new)
    print "Training and testing..."
    test_index = int(0.8 * len(X))

    logreg = linear_model.LogisticRegression()
    logreg.fit(X[:test_index], y[:test_index])
    y_pred = logreg.predict(X[test_index:])

    print metrics.classification_report(y[test_index:], y_pred)
    precision_pro = metrics.precision_score(y[test_index:], y_pred)
    precision_con = metrics.precision_score(y[test_index:], y_pred, pos_label=0)

    issues_pred['metrics'][issue] = {'pro': precision_pro, 'con': precision_con}
    print "Predicting new..."

    y_pred_new = logreg.predict(X_new)
    y_prob_new = logreg.predict_proba(X_new)

    for user, y, y_prob in zip(users_new, y_pred_new, y_prob_new):
        issues_pred['users'][user][issue] = {'pred': y, 'proba': y_prob[0]}


with open("data/issues_pred.json", "w") as f:
    json.dump(issues_pred, f)
