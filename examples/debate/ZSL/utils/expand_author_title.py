import json

debates_all = json.load(open("../data/debates.json"))
sentences = json.load(open("../data/debate_sentences_noquotes.json"))
sentences_final = {}


for i, debate in enumerate(sentences):
    sentences_final[debate] = {'pro': {}, 'con': {}}

    sentences_final[debate]['title'] = debates_all[debate]['title']
    sentences_final[debate]['pro']['author'] = debates_all[debate]['pro']
    sentences_final[debate]['con']['author'] = debates_all[debate]['con']
    sentences_final[debate]['pro']['posts'] = sentences[debate]['pro']
    sentences_final[debate]['con']['posts'] = sentences[debate]['con']
    sentences_final[debate]['category'] = debates_all[debate]['category']

print i
with open("../data/debate_posts_noquotes.json", "w") as f:
    json.dump(sentences_final, f)
