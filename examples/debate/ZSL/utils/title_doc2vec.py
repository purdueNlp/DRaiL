import glob
import gensim
import json
import sys

debates = glob.glob("/scratch/pachecog/debate-data/debates/*_info.txt")
num = len(debates)
print "All debates scrapped", num

print "Preprocessing..."
training = []; index = 0
for i, debate in enumerate(debates):
    with open(debate) as fp:
        fp.readline()
        title = fp.readline().split(':', 1)[1].strip()
        if len(title) < 1:
            continue
        training.append(gensim.models.doc2vec.TaggedDocument(gensim.utils.simple_preprocess(title), [index]))
        index += 1
    sys.stdout.write("\r\t{0}%".format((i * 100) / num))
sys.stdout.write('\n')

for tr in training:
    if tr is None:
        print tr

print "Training..."
model = gensim.models.doc2vec.Doc2Vec(vector_size=300, min_count=2, epochs=40)
model.build_vocab(training)
model.train(training, total_examples=model.corpus_count, epochs=model.epochs)

debates = json.load(open("../data/debate_posts_noquotes.json"))
num = len(debates)
print "Debates on file (debates.json)", num

print "Predicting..."
output = {}
for i, debate in enumerate(debates):
    title = debates[debate]['title'].strip()
    output[debate] = map(float, list(model.infer_vector(gensim.utils.simple_preprocess(title))))
    sys.stdout.write("\r\t{0}%".format((i * 100) / num))
sys.stdout.write('\n')

print "Dumping vectors..."
with open("../data/title_doc2vec.json", "w") as fp:
    json.dump(output, fp)
