import json
from drail.features.utils import embeddings_dictionary
import numpy as np

TOPICS = ["Politics", "Society", "Philosophy", "Religion",
          "Economics", "Education", "Health",
          "People", "News", "Science", "summaries"]

sentence_id = {}
skipthoughts = {}
posts = {}
current_id = 1
# for batching
sentence_id['<pad>'] = 0

for batch in TOPICS:
    print "Processing batch", batch
    f_skipthoughts_batch = "skipthoughts/skipthoughts_sentences_{0}.bin".format(batch)
    f_sentenceid_batch = "skipthoughts/skipthoughts_sentid_{0}.json".format(batch)

    with open(f_sentenceid_batch) as f:
        batch_sentenceid = json.load(f)
        batch_sentenceid_rev = {v: k for k, v in batch_sentenceid.iteritems()}

    _, _, batch_skipthoughts = embeddings_dictionary(f_skipthoughts_batch)


    for key in batch_sentenceid:
        if key not in sentence_id:
            sentence_id[key] = current_id
            skipthoughts[current_id] = batch_skipthoughts[str(batch_sentenceid[key])]
            current_id += 1

    if batch == "summaries":
        continue

    f_posts_batch = "posts/posts_{0}.json".format(batch)

    with open(f_posts_batch) as f:
        batch_posts = json.load(f)

    for debate in batch_posts:
        posts[debate] = {}
        pars = []
        for par in batch_posts[debate]['con_paragraphs']:
            curr_par = []
            for sent_id in par:
                curr_par.append(sentence_id[batch_sentenceid_rev[sent_id]])
            pars.append(curr_par)
        posts[debate]['con_paragraphs'] = pars

        pars = []
        for par in batch_posts[debate]['pro_paragraphs']:
            curr_par = []
            for sent_id in par:
                curr_par.append(sentence_id[batch_sentenceid_rev[sent_id]])
            pars.append(curr_par)
        posts[debate]['pro_paragraphs'] = pars
        posts[debate]['title'] = batch_posts[debate]['title']

    print len(posts), "debates..."
    print len(sentence_id), "sentences..."

with open("skipthoughts/skipthoughts_sentid.json", 'w') as f:
    json.dump(sentence_id, f)

with open("posts/posts.json", 'w') as f:
    json.dump(sentence_id, f)

f_skipthoughts = open('skipthoughts/skipthoughts_sentences.bin', 'wb')
f_skipthoughts.write(str(len(skipthoughts)))
f_skipthoughts.write(' 4800\n')
for id_ in skipthoughts:
    f_skipthoughts.write(str(id_))
    f_skipthoughts.write(' ')
    f_skipthoughts.write(np.array(skipthoughts[id_], dtype='float32').tobytes())
    f_skipthoughts.write('\n')
f_skipthoughts.close()
