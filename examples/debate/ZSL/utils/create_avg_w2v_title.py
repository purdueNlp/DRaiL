import json
import sys
from nltk.tokenize import TweetTokenizer
import numpy as np

def embeddings_dictionary(embedding_file, vocabulary=None, id2int=False, debug=False):
    '''
    Helper function to build an embeddings dictionary
    from a bin file and a set of vocabulary of words

    Returns the number of embeddings extracted
    the size of the embeddings
    and the key-value mapping from word to vector
    '''
    print "loading embeddings", embedding_file, "..."
    dictionary = {}
    with open(embedding_file, 'r') as f:
        header = f.readline()
        #print header
        num, size = map(int, header.split())
        #print num, size, vocabulary
        bin_len = np.dtype('float32').itemsize * size
        i = 0
        for line in range(num):
            char = f.read(1); word = ""
            while char != ' ':
                word += char
                char = f.read(1)

            vector = np.fromstring(f.read(bin_len), dtype='float32')
            vector = map(float, list(vector))
            #x = f.read(1)
            if vocabulary is None:
                if not id2int:
                    dictionary[word.lower().strip()] = vector
                else:
                    dictionary[int(word.lower())] = vector
            elif word.lower() in vocabulary:
                if not id2int:
                    dictionary[word.lower().strip()] = vector
                else:
                    dictionary[int(word.lower())] = vector

            i += 1
            #this is here for stopping early when debugging
            if i == 100 and debug:
                break

            sys.stdout.write("\r\t{0}%".format((i * 100) / num))
    sys.stdout.write('\n')
    return (len(dictionary), size, dictionary)

print "python create_avg_w2v_vectors.py [w2v file]"

sentence_file = "data/debate_posts_noquotes.json"

_, _, word2vec = embeddings_dictionary(sys.argv[1])

with open(sentence_file) as fp:
    sentences = json.load(fp)

output = {}
tknzr = TweetTokenizer()

for debate in sentences:
    words = tknzr.tokenize(sentences[debate]['title'].lower())
    ret = []
    for word in words:
        if word in word2vec:
            ret.append(word2vec[word])
    if len(ret) == 0:
        rndm = np.random.uniform(-0.0025, 0.0025, 300)
        output[debate] = map(float, list(rndm))
    else:
        ret = np.mean(ret, axis=0)
        output[debate] = map(float, list(ret))

with open("data/word2vec_title.json", "w") as fp:
    json.dump(output, fp)
