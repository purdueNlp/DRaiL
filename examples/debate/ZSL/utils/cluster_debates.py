import json
import re
import sys
import optparse
import random
import numpy as np

parser = optparse.OptionParser()
parser.add_option('--include-off-topic', default=False, action='store_true', dest='include_offtopic')
(opts, args) = parser.parse_args()

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)

if opts.include_offtopic:
    debate_clusters = "debate_clusters_offtopic.json"
    debate_folds = "debate_folds_offtopic.json"
    debate_easy_folds = "debate_easy_folds_offtopic.json"
else:
    debate_clusters = "debate_clusters.json"
    debate_folds = "debate_folds.json"
    debate_easy_folds = "debate_easy_folds.json"
    debate_veasy_folds = "debate_very_easy_folds.json"

veasy_titles = set(['abortion', 'gun control', 'gay marriage', 'death penalty', 'gun rights', 'marriage equality',
                    'global warming', 'marijuana', 'environmental protection', 'drug legalization',
                    'capital punishment'])

keywords = {
        0: ['abortion', 'prochoice', 'pro-choice', 'pregnancies', 'pregnancy', 'abort', 'birth control', 'pro choice'],
        1: ['gay', 'homosexual', 'marriage', 'sexuality', 'lesbian', 'marriage', 'same sex',
            'sexual orientation', 'queer', 'asexual', 'same-sex', 'polygamy', 'lgbt', 'civil unions', 'transphobia', 'homophobia'],
        2: ['marijuana', 'weed', 'legalize pot', 'cannabis'],
        3: ['welfare', 'wage', 'stamps', 'inequality', 'poor', 'disability', 'affirmative action',
            'minorities', 'minority', 'charity', 'charities', 'nonprofit', 'volunteer', 'middle class', 'financial aid', 'discrimination', 
            'poverty'],
        4: ['capitalism', 'socialism', 'anarchy', 'socalist', 'communism', 'communist', 'anarchism', 'anarchist',
            'democracy', 'authoritarianism', 'ideologies', 'marxist', 'monarch', 'dictator'],
        5: ['christianity', 'bible', 'biblical', 'christian', 'jesus', 'church', 'catholic', 'catholism', 'satan',
            'creationism', 'evolution', 'salvation', 'trinity', 'heaven', 'scripture', 'hell', 'creation', 'sunday school', 'abstinence',
            'new testament', 'old testament', 'pope', 'purgatory'],
        6: ['war', 'ww2', 'terrorism', 'iraq', 'bomb', 'armed forces', 'military',
            'foreign policy', 'troops', 'patriot', 'veteran', 'vietnam', 'terrorist', 'korea', 'iran', 'torture',
            'china', 'russia', 'india', 'putin', 'UN', 'cuba', 'world trade center', 'european union', 'world peace', 'ukraine',
            'margaret thatcher', 'scotland', '9/11', 'japan', 'siria', 'assad', 'germany', 'foreign aid',
            'international community', 'paris accord', 'child labor', 'peace', 'first world', 'third world', 'european aid', 'greece', 'isis',
            'spy', 'spies', 'twin tower', 'cia', 'osama', 'middle east', 'attack', 'pakistan', 'palestin'],
        7: ['medicare', 'healthcare', 'health', 'euthanasia', 'aids', 'cancer', 'organ donors', 'transplant',
            'assisted suicide', 'vaccination', 'vaccine', 'autism', 'obese', 'chemotherapy', 'terminally ill', 'fda', 'pharma',
            'immunize', 'immunization', 'vaccin', 'disease', 'hiv', 'depression', 'anxiety', 'injur'],
        8: ['drug', 'drugs', 'alcohol', 'alcoholism', 'addiction', 'prohibition', 'cocaine', 'cigarettes', 'smoking', 'drinking',
            'cigarrete', 'drink', 'wine', 'lsd', 'shrooms', 'heroin', 'coca plant', 'dui', 'tobacco'],
        9: ['gun', 'guns', 'riffles', 'riffle', 'weapon', 'shooting', 'arms', 'semi-automatic', 'second amendment',
            'firearm', 'violence', '2nd amendment'],
        10: ['rape', 'sexual assault', 'pornography', 'porn', 'rapist', 'rapists', 'consent', 'prostitution', 'sex', 'sex-ed',
            'nudity'],
        11: ['race', 'racism', 'white', 'supremasist', 'black', 'kkk', 'nazi', 'african americans', 'confederacy', 'racial', 'confederate', 'slave',
             'racist', 'hitler'],
        12: ['immigrant', 'immigration', 'border', 'refugee', 'visa', 'mexico'],
        13: ['god', 'religion', 'moral', 'morality', 'evil', 'pray', 'agnostic', 'atheist', 'atheism', 'soul', 'afterlife',
             'omnipotence', 'religious'],
        14: ['government', 'administration', 'tax', 'constitution', 'congress', 'economy', 'federal reserve', 'economic', 'globalization'],
        15: ['capital punishment', 'death penalty', 'death sentence', 'death penalties', 'corporal punishment', 'convicts', 
             'prison', 'jail', 'juvenile', 'criminal justice', 'imprisonment', 'law enforcement', 'police', 'confinement'],
        16: ['environment', 'climate change', 'energy', 'renewable', 'sustainable', 'sustainability',
            'endangered species', 'animal', 'extinct', 'hunting', 'fishing', 'hunt', 'dog fighting', 'co2', 'fur trade', 'dog',
            'polution'],
        17: ['women', 'feminist', 'feminism', 'woman', 'gender', 'sexism', 'domestic violence', 'chivalry', 'breastfeeding', 'female', 
            'patriarchy', 'patriarchal'],
        18: ['crops', 'genetically modified', 'gmo', 'meat', 'vegetarian', 'vegan', 'genetic', 'organic', 'junk food', 'soda', 'obesity',
            'fast food', 'food'],
        19: ['conservative', 'liberal', 'democrat', 'libertarian', 'republican', 'political', 'politics',
             'party', 'obama', 'clinton', 'hillary', 'donald', 'trump', 'bernie', 'reagan', 'bush',
             'electoral college', 'election', 'vote', 'voting', 'sanders', 'ted cruz', 'mccain', 'lincoln',
             'pledge of allegiance', 'usa', 'united states', 'u.s', 'citizen', 'president', 'presidential', 'electoral'],
        20: ['jew', 'judaism', 'sabbath', 'israel', 'torah', 'semitic', 'holocaust'],
        21: ['islam', 'muslim', 'allah', 'burqa'],
        22: ['freedom of speech', 'speech', 'first amendment', '1st amendment', 'freedom of the press'],
        23: ['school', 'tuition', 'college', 'education', 'bully', 'student', 'teacher'],
        24: ['transportation', 'amtrak'],
        25: ['nasa', 'space', 'alien', 'mars', 'extraterrestial', 'ufo'],
        26: ['child abuse', 'pedophile']}

for key in keywords:
    keywords[key] = set(keywords[key])

with open('debates.json') as f:
    debates = json.load(f)

with open('debate_sentences.json') as f:
    relevant_debates = json.load(f)

print "Number of debates", len(relevant_debates)

K = max(keywords.keys()) + 1
grouped = {K: set([])}
titles = []; vectors = []; dnames = []

tokens = set([])

veasy_debates = []
for dname in relevant_debates:
    titl = debates[dname]['title']
    words = titl.lower()
    for kw in veasy_titles:
        #if kw == words:
        if re.search(kw, words):
            veasy_debates.append(dname)
            print words
random.shuffle(veasy_debates)
exit()

for dname in relevant_debates:
    titl = debates[dname]['title']
    found = False
    if re.search('\w+', titl.strip()) is not None:
        words = titl.lower()
        tokens |= set(words.split())
        for kset in keywords:
            if found:
                break
            for kw in keywords[kset]:
                if re.search(kw, words) is not None:
                    if kset not in grouped:
                        grouped[kset] = set([])
                    grouped[kset].add(dname)
                    found = True
                    break
    if not found:
        grouped[K].add(dname)


print "Group Stats"
for group_id in grouped:
    print group_id, ":", len(grouped[group_id])

with open("title_vocabulary.txt", "w") as f:
    for token in tokens:
        f.write(token.encode('utf-8'))
        f.write('\n')

folds = {0: [], 1: [], 2: [], 3:[], 4:[]}

#for dname in grouped[27]:
#    print debates[dname]['title'].encode('utf-8')

# First sort groups in oder of length, biggest to smallest
group_lengths = [(len(grouped[gr_id]), gr_id) for gr_id in grouped]
group_lengths.sort(reverse=True)

for (_, group_id) in group_lengths:
    if group_id == 27:
        continue
    lengths = [len(folds[i]) for i in range(0, 5)]
    min_fold = lengths.index(min(lengths))
    print min_fold, group_id
    folds[min_fold] += list(grouped[group_id])

# Scatter off topic debates around
if opts.include_offtopic:
    for debate in grouped[27]:
        lengths = [len(folds[i]) for i in range(0, 5)]
        min_fold = lengths.index(min(lengths))
        folds[min_fold].append(debate)

lengths = [len(folds[i]) for i in range(0, 5)]
print lengths


with open(debate_clusters, "w") as f:
    json.dump(grouped, f, cls=SetEncoder)

with open(debate_folds, "w") as f:
    json.dump(folds, f)

## Now create an easy version of folds
easy_folds = {}
index = 0
for group_id in grouped:
    if group_id == 27 and not opts.include_offtopic:
        continue
    for debate in list(grouped[group_id]):
        if index not in easy_folds:
            easy_folds[index] = []
        easy_folds[index].append(debate)
        index = (index + 1) % 5

lengths = [len(easy_folds[i]) for i in range(0, 5)]
print lengths

with open(debate_easy_folds, "w") as f:
    json.dump(easy_folds, f)

# create the easiest version of folds
veasy_folds = {}
temp = np.array_split(veasy_debates, 5)

for i, elem in enumerate(temp):
    veasy_folds[i] = list(elem)

lengths = [len(veasy_folds[i]) for i in range(0, 5)]
print lengths

with open(debate_veasy_folds, "w") as f:
    json.dump(veasy_folds, f)
