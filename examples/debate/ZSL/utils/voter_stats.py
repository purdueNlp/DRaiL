import sys
import json

class SetDecoder(json.JSONDecoder):
    def default(self, obj):
        if isinstance(obj, list):
            return set(obj)
        return json.JSONDecoder.default(self, obj)

print sys.argv[0]
folds = json.load(open(sys.argv[1]))
votes = json.load(open("data/votes.json"))
user_info = json.load(open("data/user_info_updated.json"))
debates = json.load(open("data/debate_posts_noquotes.json"))

def load_issues(filenamecon, filenamepro):
    with open(filenamepro) as f:
        pro = json.load(f, cls=SetDecoder)
    with open(filenamecon) as f:
        con = json.load(f, cls=SetDecoder)
    stances = {}
    issues = set([])
    for issue in pro:
        issues.add(issue)
        for user in pro[issue]:
            if user not in stances:
                stances[user] = {}
            stances[user][issue] = 'pro'

        for user in con[issue]:
            if user not in stances:
                stances[user] = {}
            stances[user][issue] = 'con'
    return stances, issues

bigissues, issues = load_issues(
        'data/issues_con.json',
        'data/issues_pro.json')


for f in folds:
    voters = 0; no_info_voters = 0
    users = 0; no_posts = 0
    
    for i, dname in enumerate(folds[f]):

        for label in ['pro', 'con']:
            if len(debates[dname][label]['posts']) == 0:
                continue

            author = debates[dname][label]['author']
            title = debates[dname]['title']
            #print title, author 
            users += 1        

            for voter in votes[dname][author]:
                voters += 1
                if voter not in user_info and voter not in bigissues:
                    no_info_voters += 1
    print "instances", i
    print "fold", f, "-- voters:", voters-no_info_voters, "no-info:", no_info_voters, "users:", users


