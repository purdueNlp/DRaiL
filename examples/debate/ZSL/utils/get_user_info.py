import os
from collections import Counter
import json

features = {}; users = {}

'''
RELIGIONS = set(['Christian',
                 'Not Saying',
                 'Atheist',
                 'Agnostic',
                 'Secular',
                 'Other',
                 'Muslim',
                 'Jewish',
                 'Hindu',
                 'Buddhist'])
'''

def normalize_religion(religion):
    #if religion in RELIGIONS:
    #    return religion
    if religion.startswith('Christian'):
        return 'Christian'
    elif religion.startswith('Islam'):
        return 'Muslim'
    elif religion.startswith('Muslim'):
        return 'Muslim'
    elif religion.startswith('Jewish'):
        return 'Jewish'
    elif religion.startswith('Buddhist'):
        return 'Buddhist'
    elif religion.startswith('Hindu'):
        return 'Hindu'
    #else:
    #    return 'Other'
    else:
        return religion

debates_all = json.load(open("data/debates.json"))

USERS = set([])
for dname in debates_all:
    USERS.add(debates_all[dname]['pro'])
    USERS.add(debates_all[dname]['con'])

missing_users = 0
for user in USERS:
    user_info_path = '/scratch/pachecog/debate-data/info/{0}.txt'.format(user)
    if not os.path.isfile(user_info_path):
        missing_users += 1
        continue
    users[user] = {}
    with open(user_info_path) as f2:
        for line2 in f2:
            feat, value = line2.strip().split(':')
            if feat not in features:
                features[feat] = []
            if feat == 'Religion':
                users[user][feat] = normalize_religion(value)
                features[feat].append(normalize_religion(value))
            elif feat in ['Ideology', 'Party', 'President', 'Interested', 'Gender', 'Relationship', 'Education', 'Ethnicity', 'Occupation', 'Looking']:
                users[user][feat] = value
                features[feat].append(value)
            '''
            else:
                print feat, value
                features[feat].append(value)
    exit()
    '''

'''
for feat in features:
    print Counter(features[feat])
exit()
'''


users['features'] = {}
for feat in features:
    users['features'][feat] = list(set(features[feat]))

'''
with open('data/user_info.json', 'w') as f:
    json.dump(users, f)
'''
print "Missing profiles", missing_users / (1.0 * len(USERS))
looking_na = 0; relation_na = 0; gender_na = 0;
ideology_na = 0; religion_na = 0; interested_na = 0;
party_na = 0; president_na = 0; education_na = 0;
ethnicity_na = 0; occupation_na = 0;

for user in users:
    if 'Looking' not in users[user] or users[user]['Looking'] == 'No Answer':
        looking_na += 1
    if 'Relationship' not in users[user] or users[user]['Relationship'] == 'Not Saying':
        relation_na += 1
    if 'Gender' not in users[user] or users[user]['Gender'] == 'Prefer not to say':
        gender_na += 1
    if 'Ideology' not in users[user] or users[user]['Ideology'] == 'Not Saying':
        ideology_na += 1
    if 'Religion' not in users[user] or users[user]['Religion'] == 'Not Saying':
        religion_na += 1
    if 'Interested' not in users[user] or users[user]['Interested'] == 'No Answer':
        interested_na += 1
    if 'Party' not in users[user] or users[user]['Party'] == 'Not Saying':
        party_na += 1
    if 'President' not in users[user] or users[user]['President'] == 'Not Saying':
        president_na += 1
    if 'Education' not in users[user] or users[user]['Education'] == 'Not Saying':
        education_na += 1
    if 'Ethnicity' not in users[user] or users[user]['Ethnicity'] == 'Not Saying':
        ethnicity_na += 1
    if 'Occupation' not in users[user] or users[user]['Occupation'] == 'Not Saying':
        occupation_na += 1

print "Looking", looking_na / (1.0 * len(USERS))
print "Relationship", religion_na / (1.0 * len(USERS))
print "Gender", gender_na / (1.0 * len(USERS))
print "Ideology", ideology_na / (1.0 * len(USERS))
print "Religion", religion_na / (1.0 * len(USERS))
print "Interested", religion_na / (1.0 * len(USERS))
print "Party", party_na / (1.0 * len(USERS))
print "President", president_na / (1.0 * len(USERS))
print "Education", education_na / (1.0 * len(USERS))
print "Ethnicity", ethnicity_na / (1.0 * len(USERS))
print "Occupation", occupation_na / (1.0 * len(USERS))

print(len(USERS))
