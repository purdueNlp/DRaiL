import numpy as np
import json
import re
import spacy
from collections import Counter
import en_core_web_sm
import pandas as pd

#CATEGORIES = ['Politics', 'Society', 'Philosophy', 'Religion', 'Economics',
#              'Education', 'Health', 'People', 'Science', 'News']
nlp = en_core_web_sm.load()

all_cats = []

#sentences = json.load(open("debate_sentences.json"))
sentences = {}

'''
for CAT in CATEGORIES:
    print "Processing", CAT, "..."
'''

num_paragraphs = []
len_sentences = []
with open("debates_noquotes.json") as f:
    debates = json.load(f)

    for i, dname in enumerate(debates.keys()):
        category = debates[dname]['category']
        all_cats.append(category)
        '''
        if category not in CATEGORIES:
            if dname in sentences:
                del sentences[dname]
            continue
        '''
        #if category != CAT:
        #    continue

        if dname not in sentences:
            sentences[dname] = {'pro': [], 'con': []}

        if 'pro_paragraphs' in debates[dname] and len(sentences[dname]['pro']) == 0:
            print i, dname, 'pro'
            for par in debates[dname]['pro_paragraphs']:
                doc = nlp(par)
                curr_sentences_pro = [sent.string.strip() for sent in doc.sents
                                  if re.search('\w', sent.string.strip()) is not None]
                sentences[dname]['pro'].append(curr_sentences_pro)
                len_sentences.append(len(curr_sentences_pro))

        if 'con_paragraphs' in debates[dname] and len(sentences[dname]['con']) == 0:
            print i, dname, 'con'
            for par in debates[dname]['con_paragraphs']:
                doc = nlp(par)
                curr_sentences_con = [sent.string.strip() for sent in doc.sents
                                  if re.search('\w', sent.string.strip()) is not None]
                sentences[dname]['con'].append(curr_sentences_con)
                len_sentences.append(len(curr_sentences_con))


print "Paragraphs per debate:"
s = pd.Series(num_paragraphs)
print s.describe()
print
print "Sentences per paragraph:"
s = pd.Series(len_sentences)
print s.describe()

print Counter(all_cats)
with open('debate_sentences_noquotes.json', 'w') as f:
    json.dump(sentences, f)
