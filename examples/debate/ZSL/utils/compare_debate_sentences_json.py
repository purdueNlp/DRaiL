import json

with open("data/debate_sentences.json") as f:
    debates = json.load(f)

with open("data/debate_sentences_noquotes.json") as f:
    debates_noquotes = json.load(f)

with open("data/debates.json") as f:
    debates_all = json.load(f)

print debates_all['018913']['title']
print len(debates['018913']['pro']), len(debates['018913']['con'])
print
print len(debates_noquotes['018913']['pro']), len(debates_noquotes['018913']['con'])
