import json
import os
import sys

def load_issues(filenamecon, filenamepro):
    with open(filenamepro) as f:
        pro = json.load(f)
    with open(filenamecon) as f:
        con = json.load(f)
    stances = {}
    issues = set([])
    for issue in pro:
        issues.add(issue)
        for user in pro[issue]:
            if user not in stances:
                stances[user] = {}
            stances[user][issue] = 'pro'

        for user in con[issue]:
            if user not in stances:
                stances[user] = {}
            stances[user][issue] = 'con'
    return stances, issues

dataset_path = "."
stances, issues = load_issues(
    os.path.join(dataset_path, 'issues_con.json'),
    os.path.join(dataset_path, 'issues_pro.json'))

fname = os.path.join(dataset_path, "debates.json")
debates = json.load(open(fname))

fname = os.path.join(dataset_path, sys.argv[1])
clusters = json.load(open(fname))

statistics = {}
all_users = set([])

statistics = {}
for issue in issues:
    statistics[issue] = set([])

for group in clusters:
    #print "Group:", group

    for dname in clusters[group]:
        title = debates[dname]['title']
        #print "\tTitle:", title

        #print "\tPro User:"
        if 'pro' in debates[dname]:
            user_pro = debates[dname]['pro']
            all_users.add(user_pro)
            for issue in issues:
                if user_pro in stances and issue in stances[user_pro]:
                    #print "\t\t", issue, "|", stances[user_pro][issue]
                    statistics[issue].add(user_pro)
        #print "\tCon User:"
        if 'con' in debates[dname]:
            user_con = debates[dname]['con']
            all_users.add(user_con)
            for issue in issues:
                if user_con in stances and issue in stances[user_con]:
                    #print "\t\t", issue, "|", stances[user_con][issue]
                    statistics[issue].add(user_con)

for issue in statistics:
    print issue, len(statistics[issue]), "/", len(all_users), (1.0 * len(statistics[issue])) / len(all_users)
