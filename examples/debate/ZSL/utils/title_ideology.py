import json
import sys

debates = json.load(open(sys.argv[1]))
folds = json.load(open(sys.argv[2]))
user_info = json.load(open("data/user_info_updated.json"))

print len(debates.keys())
#print len(folds.keys())

all_debates = 0; liberal_debates = 0; conservative_debates = 0
for fold in folds:
    for debate in folds[fold]:
        author = debates[debate]['pro']['author']
        leaning = "Unknown"
        if author in user_info and 'Leaning' in user_info[author]:
            leaning = user_info[author]['Leaning']
        #print debates[debate]['title'], "|", leaning
        if leaning == 'Liberal':
            liberal_debates += 1
        elif leaning == 'Conservative':
            conservative_debates += 1
        all_debates += 1

print "Debates", all_debates
print "Liberal", (1.0 * liberal_debates) / all_debates
print "Conservative", (1.0 * conservative_debates) / all_debates
print "Unknown", (1.0 * (all_debates - liberal_debates - conservative_debates)) / all_debates
