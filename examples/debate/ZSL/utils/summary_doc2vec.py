import glob
import gensim
import json

summaries = glob.glob("/scratch/pachecog/debate-data/summaries/*.txt")
documents = {}

for summary in summaries:
    with open(summary) as fp:
        for line in fp:
            section, doc = line.split(':', 1)
            if section not in documents:
                documents[section] = []
            documents[section].append(doc)

sections = ['Activities', 'TV Shows', 'High School', 'Websites', 'Sports Teams', 'Quotes', 'Movies', 'Books', 'Music', 'College', 'About Me', 'Beliefs']

models = {}
training = {}
for sect in sections:
    print "training model", sect, "..."
    training[sect] = [gensim.models.doc2vec.TaggedDocument(gensim.utils.simple_preprocess(doc), [i]) for i, doc in enumerate(documents[sect])]
    model = gensim.models.doc2vec.Doc2Vec(vector_size=50, min_count=2, epochs=40)
    model.build_vocab(training[sect])
    model.train(training[sect], total_examples=model.corpus_count, epochs=model.epochs)
    models[sect] = model

output = {}
for summary in summaries:
    with open(summary) as fp:
        user = summary[-12:-4]
        output[user] = {}
        for line in fp:
            section, doc = line.split(':', 1)
            if section in models:
                output[user][section] = map(float, list(models[section].infer_vector(gensim.utils.simple_preprocess(doc))))

with open("../data/summary_doc2vec.json", "w") as fp:
    json.dump(output, fp)
