import sys
import os
import numpy as np
import optparse
import cProfile as profile
import random
import torch
import json
from sklearn.metrics import *

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_options():
    parser = optparse.OptionParser()
    parser.add_option('-g', '--gpu', help='gpu index', dest='gpu_index', type='int', default=None)
    parser.add_option('--start-from', help='start from fold', dest='fold_init', type='int', default=0)
    parser.add_option('-d', '--dir', help='directory', dest='dir', type='string')
    parser.add_option('-r', '--rule', help='rule file', dest='rules', type='string',
                       default='rule.dr')
    parser.add_option('-c', '--config', help='config file', dest='config', type='string',
                       default="config.json")
    parser.add_option('-m', help='mode: [global|local|joint]', dest='mode', type='string',
                      default='local')
    parser.add_option('--delta', help='loss augmented inferece', dest='delta', action='store_true',
                      default=False)
    parser.add_option('--lr', help='global learning rate', dest='global_lr', type='float')
    parser.add_option('--very-easy-folds', help='use the easy fold split', dest='very_easy_folds',
                      default=False, action='store_true')
    parser.add_option('--easy-folds', help='use the easy fold split', dest='easy_folds',
                      default=False, action='store_true')
    parser.add_option('--charged-folds', help='use the politically charged split', dest='charged_folds',
                      default=False, action='store_true')
    parser.add_option('--w2v', help='w2v filename', dest='w2v_bin_filename', type='string',
                      default=None)
    parser.add_option('--sentemb', help='sentence emb filename', type='string',
                      default=None, dest='sentence_emb')
    parser.add_option('-e', '--embedding', help='sentence embedding type [sentemb|doc2vec]', type='string',
                      default=None, dest='sentence_emb_type')
    parser.add_option('--savedir', help='savedir', type='string',
                      default=None, dest='savedir')
    parser.add_option('--continue-from-checkpoint', help='continue from checkpoint in savedir',
                      dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_option('--user-mode', help='set user mode', dest='user_mode',
                      default=False, action='store_true')
    parser.add_option('--endorse-mode', help='set endorse mode', dest='endorse_mode',
                      default=False, action='store_true')
    parser.add_option('--post-mode', help='set user mode', dest='post_mode',
                      default=False, action='store_true')
    parser.add_option('--voter-mode', help='set voter mode', dest='voter_mode',
                      default=False, action='store_true')
    parser.add_option('--voter-fixed', help='set voter fixed', dest='voter_fixed',
                      default=False, action='store_true')
    parser.add_option('--inference-only', help='run inference only', dest='inference_only',
                      default=False, action='store_true')
    parser.add_option('--train-only', help='run training only', dest='train_only',
                      default=False, action='store_true')
    parser.add_option('--debug', help='debug mode', dest='debug',
                      default=False, action='store_true')
    parser.add_option('--political-only', help='only use debates labeled as politics', dest='political_only',
                      default=False, action='store_true')
    parser.add_option('--title-emb', type='string', dest='title_emb', default=None)
    (opts, args) = parser.parse_args()

    if check_options(parser, opts) < 0:
        exit(-1)

    return opts

def check_options(parser, opts):
    mandatories = ['dir', 'rules', 'config']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            return -1

    if opts.mode == "global" and not opts.__dict__['global_lr']:
        print "specify learning rate for global model\n"
        parser.print_help()
        return -1

    if opts.continue_from_checkpoint and not opts.savedir:
        print "specify savedir to load checkpoint"
        parser.print_help()
        return -1

    if opts.inference_only and not opts.continue_from_checkpoint:
        print "specify a checkpoint as starting point for inference"
        parser.print_help()
        return -1

    if opts.mode not in ['global', 'local', 'joint']:
        print "specify a valid mode\n"
        parser.print_help()
        return -1

    if (opts.sentence_emb is not None and opts.sentence_emb_type is None) \
            or (opts.sentence_emb_type is not None and opts.sentence_emb is None):
        print "specify a sentence embedding file and the sentence embedding type"
        parser.print_help()
        return -1

    return 0

def train(folds, avoid):
    ret = []
    for j in folds:
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    # seed and cuda
    np.random.seed(1234)
    random.seed(1234)

    opts = parse_options()

    if opts.gpu_index:
        torch.cuda.set_device(opts.gpu_index)
    rule_file = os.path.join(opts.dir, opts.rules)
    conf_file = os.path.join(opts.dir, opts.config)

    summaries = os.path.join(opts.dir, "data/summary_doc2vec.json")
    #w2v_bin_filename = "/scratch/pachecog/GoogleNews-vectors-negative300.bin"
    doc2vec_filename = "/scratch/pachecog/DRaiL/examples/debate/ZSL/data/post_doc2vec.json"

    if opts.easy_folds:
        debate_folds = json.load(open(os.path.join(opts.dir, "data/debate_easy_folds.json")))
        split = "easy_folds"
    elif opts.very_easy_folds:
        debate_folds = json.load(open(os.path.join(opts.dir, "data/debate_very_easy_folds.json")))
        split = "very_easy_folds"
    elif opts.charged_folds:
        debate_folds = json.load(open(os.path.join(opts.dir, "data/debate_charged_folds.json")))
        split = "charged_folds"
    else:
        debate_folds = json.load(open(os.path.join(opts.dir, "data/debate_folds.json")))
        split = "folds"

    print "using split", split

    issues_pro = os.path.join(opts.dir, "data/issues_pro.json")
    issues_con = os.path.join(opts.dir, "data/issues_con.json")
    user_info = os.path.join(opts.dir, "data/user_info.json")
    issues_pred = os.path.join(opts.dir, "data/issues_pred.json")

    ## TO-DO: make it optional to receive None here
    title_vocabulary = os.path.join(opts.dir, "data/title_vocabulary.json")

    #users = list(set(json.load(open(user_info)).keys()) - set(['features']))
    #random.shuffle(users)
    #user_folds = dict((i, users[i::5]) for i in xrange(5))

    if opts.mode == "global":
        learner=GlobalLearner(learning_rate=opts.global_lr, use_gpu=(opts.gpu_index is not None), gpu_index=opts.gpu_index)
    elif opts.mode == "joint":
        learner=JointLearner()
    else:
        learner=LocalLearner()

    learner.compile_rules(rule_file)
    db=learner.create_dataset(opts.dir, dbmodule_path=opts.dir)
    db.user_mode = opts.user_mode
    db.endorse_mode = opts.endorse_mode
    db.post_mode = opts.post_mode
    db.voter_mode = opts.voter_mode
    db.voter_fixed = opts.voter_fixed
    db.sentemb_mode = opts.sentence_emb is not None
    db.set_splits(opts.dir, opts.sentence_emb_type, split)
    db.political_only = opts.political_only

    '''
    issue_folds = {0: ['Abortion'],
                   1: ['Gay Marriage'],
                   2: ['Gun Rights'],
                   3: ['Death Penalty'],
                   4: ['Environmental Protection']}
    '''

    if opts.savedir is not None and not os.path.isdir(opts.savedir):
        os.mkdir(opts.savedir)

    if opts.savedir is not None:
        savedirs = [os.path.join(opts.savedir, "f0"),
                    os.path.join(opts.savedir, "f1"),
                    os.path.join(opts.savedir, "f2"),
                    os.path.join(opts.savedir, "f3"),
                    os.path.join(opts.savedir, "f4")
                    ]

    for i in range(opts.fold_init, 5):
        print "Fold", i
        dev_fold = (i + 1) % 5

        test_debates = debate_folds[str(i)]
        dev_debates = debate_folds[str(dev_fold)]
        train_debates = train(debate_folds, map(str, [i, dev_fold]))
        #train_debates = train_debates[:500]

        print (set(train_debates) & set(dev_debates))

        if opts.debug:
            train_debates = train_debates[:10]
            dev_debates = dev_debates[:10]
            test_debates = test_debates[:10]

        print "Debate folds:", len(train_debates), len(dev_debates), len(test_debates)

        db.add_filters(filters=[
            ("Debate", "isTrain", "debate", train_debates),
            ("Debate", "isDev", "debate", dev_debates),
            ("Debate", "isTest", "debate", test_debates),
            ("Debate", "isTrainDev", "debate", train_debates + dev_debates),
            ("Debate", "allFolds", "debate", train_debates + dev_debates + test_debates),
            ])

        learner.build_feature_extractors(db,
                                         filters=[("D", "allFolds", 1)],
                                         summaries=summaries,
                                         issues_pro=issues_pro,
                                         issues_con=issues_con,
                                         user_info=user_info,
                                         femodule_path=opts.dir,
                                         w2v_bin_filename=opts.w2v_bin_filename,
                                         issues_pred=issues_pred,
                                         sentence_emb=opts.sentence_emb,
                                         debug=opts.debug,
                                         debates=db.debates,
                                         titles=opts.title_emb,
                                         vocabulary=title_vocabulary,
                                         d2v_filename=doc2vec_filename)
        learner.set_savedir(savedirs[i])
        learner.build_models(db, conf_file)
        
        if opts.mode == "local" or opts.mode == "joint":
            if opts.continue_from_checkpoint:
                learner.init_models()

            if not opts.inference_only:
                print "Train procedure.."
                _ = learner.train(
                        db,
                        train_filters=[("D", "isTrain", 1)],
                        dev_filters=[("D", "isDev", 1)],
                        test_filters=[("D", "isTest", 1)])
            if not opts.train_only:
                print "Test procedure..."
                learner.extract_data(db,
                        extract_test=True, extract_dev=False, extract_train=False,
                        test_filters=[("D", "isTest", 1)])
                #learner.init_models()
                #learner.train_lambdas(20, 0.05, 'dev', predicates=['HasLabel'])

                # mark feature things for garbage collection
                del learner.fe

                res, preds = learner.predict(None, fold='test', get_predicates=True)


                with open("results_fold_{0}.txt".format(i), "w") as fp:
                    for pr in preds:
                        if pr.startswith('HasLabel('):
                            parts = pr.split(',')
                            author = parts[0][9:]
                            debate_name = parts[2]
                            post_position = parts[1]
                            label = parts[3][:3]
                            fp.write("p.{0}.{1}.{2}.{3}\n".format(author, debate_name, post_position, label))
        else:
            learner.extract_data(
                    db,
                    train_filters=[("D", "isTrain", 1)],
                    dev_filters=[("D", "isDev", 1)],
                    test_filters=[("D", "isTest", 1)],
                    just_test=opts.inference_only)

            #del learner.fe

            res, heads = learner.train(
                    db,
                    train_filters=[("D", "isTrain", 1)],
                    dev_filters=[("D", "isDev", 1)],
                    test_filters=[("D", "isTest", 1)],
                    opt_predicate='HasLabel',
                    loss_augmented_inference=opts.delta,
                    continue_from_checkpoint=opts.continue_from_checkpoint,
                    inference_only=opts.inference_only,
                    hot_start=False)

        if not opts.train_only:
            if 'HasLabel' in res.metrics:
                y_gold = res.metrics['HasLabel']['gold_data']
                y_pred = res.metrics['HasLabel']['pred_data']


                #print y_gold
                #print y_pred

                print "HasLabel (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score
            
            #print res.metrics.keys()
            if 'HasUserLabel' in res.metrics and opts.user_mode:
                y_gold = res.metrics['HasUserLabel']['gold_data']
                y_pred = res.metrics['HasUserLabel']['pred_data']


                #print y_gold
                #print y_pred

                print "HasUserLabel (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score

            if 'HasStance' in res.metrics and opts.post_mode:
                y_gold = res.metrics['HasStance']['gold_data']
                y_pred = res.metrics['HasStance']['pred_data']

                print "HasStance (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score
            if 'Endorsement' in res.metrics and opts.endorse_mode:
                y_gold = res.metrics['Endorsement']['gold_data']
                y_pred = res.metrics['Endorsement']['pred_data']

                print "Endorsement (Yes/No)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score

            if "VoteFor" in res.metrics and opts.voter_mode:
                y_gold = res.metrics['VoteFor']['gold_data']
                y_pred = res.metrics['VoteFor']['pred_data']

                print "VoteFor (Yes/No)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score
            if "VoteSame" in res.metrics and opts.voter_mode:
                y_gold = res.metrics['VoteSame']['gold_data']
                y_pred = res.metrics['VoteSame']['pred_data']

                print "VoteSame (Yes/No)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score

            if "InheritsLabel" in res.metrics and (opts.voter_mode or opts.voter_fixed):
                y_gold = res.metrics['InheritsLabel']['gold_data']
                y_pred = res.metrics['InheritsLabel']['pred_data']

                print "InheritsLabel (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score
            learner.reset_metrics()

if __name__ == "__main__":
    main()
