from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

from scipy import spatial
import json
import numpy as np
import re
import random
from nltk.tokenize import TweetTokenizer

#TO-DO: sentences are already segmented

class DebateZSL_ft(FeatureExtractor):

    def __init__(self, summaries, issues_pro, issues_con, user_info,
                 w2v_bin_filename, issues_pred, sentence_emb,
                 debates, titles, vocabulary, d2v_filename, debug=False):
        super(DebateZSL_ft, self).__init__()
        self.summaries_f = summaries
        self.issues_pro = issues_pro
        self.issues_con = issues_con
        self.user_info_f = user_info
        self.w2v_bin_filename = w2v_bin_filename
        self.issues_pred_f = issues_pred
        self.sentence_emb = sentence_emb
        self.debug = debug
        self.debates = debates
        self.titles_f = titles
        self.vocabulary_f = vocabulary
        self.d2v_filename = d2v_filename
        np.random.seed(17477)
        random.seed(17477)

    def load_issues(self, filenamecon, filenamepro, include_leaning=True):
        with open(filenamepro) as f:
            pro = json.load(f)
        with open(filenamecon) as f:
            con = json.load(f)

        stances = {}
        self.issues = pro.keys()

        for idx, issue in enumerate(self.issues):
            if not include_leaning and issue in ['Abortion', 'Gay Marriage', 'Drug Legalization',
                                                 'Environmental Protection', 'Global Warming Exists',
                                                 'Medical Marijuana', 'National Health Care',
                                                 'Death Penalty', 'Gun Rights', 'War on Terror',
                                                 'War in Afghanistan', 'Border Fence']:
                continue
            for user in pro[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['pro'][idx] = 1.0
            for user in con[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['con'][idx] = 1.0
        return stances

    def build(self):
        #self.nlp = en_core_web_sm.load()
        self.stance_idx = {"pro":1, "con":0}
        self.leaning_idx = {"Liberal":1, "Conservative": 0}

        if self.w2v_bin_filename:
            self.num_word_vectors,\
            self.word_embedding_size,\
            self.word_embedding = \
                utils.embeddings_dictionary(self.w2v_bin_filename, debug=self.debug)
        '''
        with open(self.issues_pred_f) as f:
            self.issues_pred = json.load(f)
        '''

        with open(self.user_info_f) as f:
            self.user_info = json.load(f)

        self.stances = self.load_issues(self.issues_con, self.issues_pro)
        self.stances_minus_leaning = self.load_issues(self.issues_con, self.issues_pro, include_leaning=False)
        '''
        self.num_religions, self.size_religions, self.onehot_religions = \
                utils.onehot_dictionary(
                        set(self.user_info['features']['Religion']))
        self.num_ideologies, self.size_ideologies, self.onehot_ideologies = \
                utils.onehot_dictionary(
                        set(self.user_info['features']['Ideology']))

        self.num_issues, self.size_issues, self.onehot_issues =\
                utils.onehot_dictionary(self.issues)
        '''
        if self.sentence_emb:
            print("Loading sentence embedding...")
            self.sentence_emb_matrix = np.load(self.sentence_emb)

        self.tknzr = TweetTokenizer()

        self.sections = ['Activities', 'TV Shows', 'High School', 'Websites', 'Sports Teams', 'Quotes', 'Movies', 'Books', 'Music', 'College', 'About Me', 'Beliefs']
        with open(self.summaries_f) as f:
            self.summary_vects = json.load(f)

        if self.titles_f is not None:
            with open(self.titles_f) as f:
                self.title_vects = json.load(f)

        if self.vocabulary_f is not None:
            with open(self.vocabulary_f) as f:
                self.word2idx = json.load(f)
                self.vocab_size = len(self.word2idx)

        if self.d2v_filename is not None:
            with open(self.d2v_filename) as f:
                self.d2v = json.load(f)

    def extract_multiclass_head(self, instance_grd):
        head = instance_grd.get_head_predicate()
        if head['name'] == 'HasLabel' or head['name'] == 'InheritsLabel' or head['name'] == 'HasUserLabel':
            label = head['arguments'][3]
            return self.stance_idx[str(label)]
        elif head['name'] == 'HasStance':
            label = head['arguments'][2]
            return self.stance_idx[str(label)]
        elif head['name'] == 'HasLeaning':
            label = head['arguments'][3]
            return self.leaning_idx[str(label)]

    def extract_multilabel_head(self, instance_grd):
        head = instance_grd.get_head_predicate()
        if head['name'] == 'HasIssues':
            labels = instance_grd.get_head_predicate()['arguments'][3]
        return labels

    def debate_sequence_avg_w2v(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        par_pos = pred['arguments'][0]
        dname = pred['arguments'][1]
        par = pred['arguments'][2]
        #print pred['arguments']
        sentences = self.debates[dname][par]['posts'][par_pos]
        sentences = [self.tknzr.tokenize(s.lower()) for s in sentences][:25]
        ret = []
        for sent in sentences:
            words = []
            for word in sent:
                if word not in self.word_embedding:
                    #print "not found:", word
                    continue
                words.append(self.word_embedding[word])
            ret.append(np.mean(words, axis=0))
        if len(ret) == 0:
            return [np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)]
        return ret

    def debate_sequence_sentemb(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        par_pos = pred['arguments'][0]
        dname = pred['arguments'][1]
        par = pred['arguments'][2]

        sentences = self.debates[dname][par]['posts'][par_pos]
        ret = []
        for sent in sentences[:25]:
            ret.append(self.sentence_emb_matrix[sent])
        if len(ret) == 0:
            return [np.random.uniform(-0.0025, 0.0025, 300)]
        return ret

    def debate_post_doc2vec(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        par_pos = pred['arguments'][0]
        dname = pred['arguments'][1]
        par = pred['arguments'][2]
        return self.d2v[dname][par][par_pos]

    def preprocess_tweet(self, tweet):
        eyes = "[8:=;]"
        nose = "['`\-]?"
        tweet = re.sub(r'RT', "", tweet)
        tweet = re.sub(r'https{0,1}://\S+\b|www\.(\w+\.)+\S*/', "", tweet)
        tweet = re.sub(r'@\w+', "", tweet)
        tweet = re.sub(eyes + nose + r'[)d]+|[)d]+' + nose + eyes, "", tweet)
        tweet = re.sub(eyes + nose + r'p+', "", tweet)
        tweet = re.sub(eyes + nose + r'\(+|\)+' + nose + eyes, "", tweet)
        tweet = re.sub(eyes + nose + r'[\/|l*]', "", tweet)
        tweet = re.sub(r'<3', "<heart>", tweet)
        tweet = re.sub(r'[-+]?[.\d]*[\d]+[:,.\d]*', "", tweet)
        tweet = re.sub(r'#\S+', "", tweet)
        tweet = re.sub(r'([!?.]){2,}', "", tweet)
        tweet = re.sub(r'\b(\S*?)(.)\2{2,}\b', "", tweet)
        return tweet

    def debate_title_sentemb(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        dname = pred['arguments'][1]
        title = self.debates[dname]['title']
        if len(self.sentence_emb_matrix[title]) != 300:
            return np.random.uniform(-0.0025, 0.0025, 300)
        return self.sentence_emb_matrix[title]

    def title_avg_w2v(self, rule_grd):
        return self.title_embedding(rule_grd)

    def title_avg_w2v_sentemb(self, rule_grd):
        return np.concatenate([self.title_embedding(rule_grd), self.debate_title_sentemb(rule_grd)])

    def title_sequence_word2vec(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        dname = pred['arguments'][1]
        if dname.startswith('tw'):
            title = rule_grd.get_body_predicates("IsText")[0]['arguments'][1]
            title = self.preprocess_tweet(title)
        else:
            title = self.debates[dname]['title']
        words = self.tknzr.tokenize(title.lower())
        ret = []
        for word in words:
            if word not in self.word_embedding:
                #print "not found:", word
                self.word_embedding[word] =\
                    np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)
            ret.append(self.word_embedding[word])
        if len(ret) == 0:
            return [np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)]
        return ret

    def title_sequence_wordid(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        dname = pred['arguments'][1]
        if dname.startswith('tw'):
            title = rule_grd.get_body_predicates("IsText")[0]['arguments'][1]
            title = self.preprocess_tweet(title)
        else:
            title = self.debates[dname]['title']
        words = self.tknzr.tokenize(title.lower())
        ret = []
        for word in words:
            ret.append(self.word2idx[word])
        return ret

    def summary_vectors(self, rule_grd):
        ret = []
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]

        for sect in self.sections:
            if author in self.summary_vects and sect in self.summary_vects[author]:
                ret += self.summary_vects[author][sect]
            else:
                ret += map(float, [0]*50)
        return ret

    def title_embedding(self, rule_grd):
        pred = rule_grd.get_body_predicates("InDebate")[0]
        dname = pred['arguments'][1]
        return self.title_vects[dname]

    def user_bigissues_1hot(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]

        if author in self.stances:
            ret = self.stances[author]['pro'] + self.stances[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def voter_bigissues_1hot(self, rule_grd, voter_pos):
        pred = rule_grd.get_body_predicates("Votes")[voter_pos]
        author = pred['arguments'][0]

        if author in self.stances:
            ret = self.stances[author]['pro'] + self.stances[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def voter_bigissues_1hot_minus_leaning(self, rule_grd):
        pred = rule_grd.get_body_predicates("Votes")[0]
        author = pred['arguments'][0]

        if author in self.stances_minus_leaning:
            ret = self.stances_minus_leaning[author]['pro'] + self.stances_minus_leaning[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def user_bigissues_1hot_minus_leaning(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]

        if author in self.stances_minus_leaning:
            ret = self.stances_minus_leaning[author]['pro'] + self.stances_minus_leaning[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def user_bigissues_pred(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]
        ret = [0.0] * len(self.issues) * 2
        # ground truth is included because they have pred of 1 and 0
        for idx, issue in enumerate(self.issues):
            # pro vector
            ret[idx] = self.issues_pred['users'][author][issue]['pred']
            # con vector
            ret[len(self.issues) + idx] = abs(ret[idx] - 1)
        return ret

    def user_bigissues_pred_high_confidence(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]
        ret = [0.0] * len(self.issues) * 2
        # ground truth is included because they have proba of 1 and 0
        for idx, issue in enumerate(self.issues):
            # pro vector
            proba = self.issues_pred['users'][author][issue]['proba']
            pred = self.issues_pred['users'][author][issue]['pred']
            if proba <= 0.30 or proba == 1:
                ret[idx] = pred
            # con vector
            if proba >= 0.70 or proba == 0:
                ret[len(self.issues) + idx] = \
                    abs(1 - proba)
        return ret

    def user_bigissues_pred_good_classifier(self, rule_grd):
        if rule_grd.has_body_predicate("IsAuthor"):
            pred = rule_grd.get_body_predicates("IsAuthor")[0]
        else:
            pred = rule_grd.get_body_predicates("HasUser")[0]
        author = pred['arguments'][0]
        # start with ground truth
        ret = self.user_bigissues_1hot(rule_grd)
        # then also add predictions comming from good classifiers
        for idx, issue in enumerate(self.issues):
            # pro vector
            if self.issues_pred['metrics'][issue]['pro'] >= 0.70 and self.issues_pred['users'][author][issue]['pred'] == 1:
                ret[idx] = self.issues_pred['users'][author][issue]['pred']
            # con vector
            if self.issues_pred['metrics'][issue]['con'] >= 0.70 and self.issues_pred['users'][author][issue]['pred'] == 0:
                ret[len(self.issues) + idx] = \
                    abs(1 - self.issues_pred['users'][author][issue]['pred'])
        return ret

    def user_bigissues_pred_good_confidence(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]
        # start with ground truth
        ret = self.user_bigissues_1hot(rule_grd)
        # then also add predictions comming from good classifiers
        for idx, issue in enumerate(self.issues):
            # pro vector
            proba = self.issues_pred['users'][author][issue]['proba']
            pred = self.issues_pred['users'][author][issue]['pred']

            if self.issues_pred['metrics'][issue]['pro'] >= 0.70 and \
                pred == 1 and (proba <= 0.30 or proba == 1):
                ret[idx] = pred
            # con vector
            if self.issues_pred['metrics'][issue]['con'] >= 0.70 and \
               pred == 0 and (proba >= 70 or proba == 0):
                ret[len(self.issues) + idx] = \
                    abs(1 - pred)
        return ret

    def user_bigissues_1hot_noisy(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]

        if author in self.stances:
            ret = self.stances[author]['pro'] + self.stances[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2

        # flip values in 50% of cases
        for i in range(len(ret)):
            if random.uniform(0, 1) > 0.50:
                ret[i] = abs(1 - ret[i])

        return ret

    def bigissue_1hot(self, rule_grd):
        pred = rule_grd.get_body_predicates("KnownStance")[0]
        issue = pred['arguments'][1]
        return self.onehot_issues[issue]

    def user_issues_and_information(self, rule_grd):
        #print rule_grd
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]
        ret = self.user_bigissues_1hot(rule_grd)
        #ret = self.user_bigissues_pred_good_confidence(rule_grd)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if author in self.user_info and feat in self.user_info[author]:
                index = self.user_info['features'][feat].index(self.user_info[author][feat])
                temp[index] = 1
            ret += temp
        return ret

    def user_issues_and_information_minus_leaning(self, rule_grd):
        pred = rule_grd.get_body_predicates("IsAuthor")[0]
        author = pred['arguments'][0]
        ret = self.user_bigissues_1hot_minus_leaning(rule_grd)
        #ret = self.user_bigissues_pred_good_confidence(rule_grd)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if author in self.user_info and feat in self.user_info[author] and feat not in ['Ideology', 'Party']:
                index = self.user_info['features'][feat].index(self.user_info[author][feat])
                temp[index] = 1
            ret += temp
        return ret

    def user_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd) + self.summary_vectors(rule_grd)

    def voter_features(self, rule_grd):
        return self.voter_issues_and_information(rule_grd, 0) + self.summary_vectors_voter(rule_grd, 0)

    def user_voter_features(self, rule_grd):
        return self.user_features(rule_grd) + self.voter_features(rule_grd)

    def other_voter_features(self, rule_grd):
        return self.voter_issues_and_information(rule_grd, 1) + self.summary_vectors_voter(rule_grd, 1)

    def voter_voter_features(self, rule_grd):
        return self.voter_features(rule_grd) + self.other_voter_features(rule_grd)

    def summary_vectors_voter(self, rule_grd, voter_pos):
        ret = []
        pred = rule_grd.get_body_predicates("Votes")[voter_pos]
        author = pred['arguments'][0]

        for sect in self.sections:
            if author in self.summary_vects and sect in self.summary_vects[author]:
                ret += self.summary_vects[author][sect]
            else:
                ret += map(float, [0]*50)
        return ret

    def voter_issues_and_information(self, rule_grd, voter_pos):
        pred = rule_grd.get_body_predicates("Votes")[voter_pos]
        author = pred['arguments'][0]
        ret = self.voter_bigissues_1hot(rule_grd, voter_pos)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if author in self.user_info and feat in self.user_info[author]:
                index = self.user_info['features'][feat].index(self.user_info[author][feat])
                temp[index] = 1
            ret += temp
        return ret

    def voter_issues_and_information_minus_leaning(self, rule_grd):
        pred = rule_grd.get_body_predicates("Votes")[0]
        author = pred['arguments'][0]
        ret = self.voter_bigissues_1hot_minus_leaning(rule_grd)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if author in self.user_info and feat in self.user_info[author] and feat not in ['Ideology', 'Party']:
                index = self.user_info['features'][feat].index(self.user_info[author][feat])
                temp[index] = 1
            ret += temp
        return ret
