import os
import json
import random
import csv

from drail.model.rule import RuleGrounding
from drail.model.label import LabelType

class SetDecoder(json.JSONDecoder):
    def default(self, obj):
        if isinstance(obj, list):
            return set(obj)
        return json.JSONDecoder.default(self, obj)

class DebateZSL_db():

    def __init__(self):
        self.debates = {}
        self.endorse_mode = False
        self.user_mode = False
        self.post_mode = False
        self.sentemb_mode = False
        self.voter_mode = False
        self.voter_fixed = False
        self.political_only = False
        random.seed(1234)

    def set_splits(self, dataset_path, embedding, split):
        # load relevant debates
        if not self.sentemb_mode:
            fname = os.path.join(dataset_path, "data/debate_posts_noquotes.json")
        else:
            # sentence ids are fixed in this case
            if embedding == "doc2vec":
                fname = os.path.join(dataset_path, "data/debate_posts_noquotes_{0}.json".format(embedding))
            else:
                fname = os.path.join(dataset_path, "data/debate_posts_noquotes_{0}_{1}.json".format(embedding, split))
        self.debates = json.load(open(fname))

    def load_issues(self, filenamecon, filenamepro):
        with open(filenamepro) as f:
            pro = json.load(f, cls=SetDecoder)
        with open(filenamecon) as f:
            con = json.load(f, cls=SetDecoder)
        stances = {}
        issues = set([])
        for issue in pro:
            issues.add(issue)
            for user in pro[issue]:
				if user not in stances:
					stances[user] = {}
				stances[user][issue] = 'pro'

            for user in con[issue]:
				if user not in stances:
					stances[user] = {}
				stances[user][issue] = 'con'
        return stances, issues

    def load_data(self, dataset_path):
        self.dataset_path = dataset_path

        self.bigissues, self.issues = self.load_issues(
                os.path.join(self.dataset_path, 'data/issues_con.json'),
                os.path.join(self.dataset_path, 'data/issues_pro.json')
                )
        self.issues = list(self.issues)

        fname = os.path.join(dataset_path, "data/user_info_updated.json")
        self.user_info = json.load(open(fname))

        fname = os.path.join(dataset_path, "data/summary_doc2vec.json")
        summaries = json.load(open(fname))
        self.has_summary = set([user for user in summaries if len(summaries[user]) > 0])

        with open(os.path.join(self.dataset_path, "data/votes.json")) as fp:
            self.votes = json.load(fp)

        self.tweet_ideological = os.path.join(self.dataset_path, "data/ideological_tweets.csv")

    def add_filters(self, filters=[]):
        self.filters = {}
        for fil in filters:
            self.filters[fil[1]] = fil[3]

    def filter_debates(self, filters, instance_id=None):
        filter_d = []
        for fil in filters:
            if fil[0] == "D":
                filter_d += self.filters[fil[1]]
        if len(filter_d) == 0:
            filter_d = set(self.debates.keys())

        if instance_id != None:
            filter_d = set([instance_id]) & set(filter_d)

        ret = []
        if self.political_only:
            for dname in filter_d:
                if self.debates[dname]['category'] == 'Politics':
                    ret.append(dname)
            return set(ret)
        else:
            return filter_d

    def debate_text_zsl(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                #print dname, self.debates[dname].keys()
                author = self.debates[dname][label]['author']

                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                    len(self.debates[dname][par]['posts']) <= 0:
                       continue

                for par_pos, text in enumerate(self.debates[dname][par]['posts']):

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, par],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    IsAuthor = {
                        "name": "IsAuthor",
                        "arguments": [author, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    if istrain:
                        HasLabel = {
                                "name": "HasLabel",
                                "arguments": [author, par_pos, dname, label],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

                    else:
                        for lbl in ['pro', 'con']:
                            istruth = lbl == label
                            HasLabel = {
                                    "name": "HasLabel",
                                    "arguments": [author, par_pos, dname, lbl],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            body = [InDebate, IsAuthor]
                            head = HasLabel
                            rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        '''
        print "pred"
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret

    # should never be called during training
    def debate_endorse(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            title = self.debates[dname]['title']
            if title is None:
               continue
            for label in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[dname][label]['posts']):
                    author_pro = self.debates[dname]['pro']['author']
                    author_con = self.debates[dname]['con']['author']

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, label],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    for j, author in enumerate([author_pro, author_con]):
                        voters = self.votes[dname][author]
                        for voter in voters:
                            Votes = {
                                "name": "Votes",
                                "arguments": [voter, dname],
                                "ttype": None,
                                "obs": True,
                                "isneg": False,
                                "target_pos": None}
                            Endorsement = {
                                "name": "Endorsement",
                                "arguments": [voter, par_pos, label, dname],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None}
                            istruth = (j == 0 and label == 'pro') or (j == 1 and label == 'con')
                            body = [InDebate, Votes]
                            head = Endorsement
                            rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        '''
        if len(ret) > 0:
            for rg in ret:
                print rg
            exit()
        '''
        random.shuffle(ret)
        return ret

    def debate_authorship(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for label in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[dname][label]['posts']):
                    author_pro = self.debates[dname]['pro']['author']
                    author_con = self.debates[dname]['con']['author']

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, label],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    if istrain:
                        if isneg and label == 'pro':
                            author = author_con
                        if isneg and label == 'con':
                            author = author_pro
                        if not isneg and label == 'pro':
                            author = author_pro
                        if not isneg and label == 'con':
                            author = author_con

                        # We don't want to do this if our author info is unknown
                        if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                            continue

                        HasUser = {
                            "name": "IsAuthor",
                            "arguments": [author, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Authorship = {
                            "name": "Authorship",
                            "arguments": [author, par_pos, label, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": isneg,
                            "target_pos": None}
                        body = [InDebate, HasUser]
                        head = Authorship
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

                        # If train augment training data with endorsement data
                        '''
                        for j, author in enumerate([author_pro, author_con]):
                            voters = self.votes[dname][author]
                            for voter in voters:
                                Votes = {
                                    "name": "IsAuthor",
                                    "arguments": [voter, dname],
                                    "ttype": None,
                                    "obs": True,
                                    "isneg": False,
                                    "target_pos": None}
                                Endorsement = {
                                    "name": "Authorship",
                                    "arguments": [voter, par_pos, label, dname],
                                    "ttype": None,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": None}
                                istruth = (j == 0 and label == 'pro') or (j == 1 and label == 'con')
                                body = [InDebate, Votes]
                                head = Endorsement
                                rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                                rg.build_predicate_index()
                                rg.build_body_predicate_dic()
                                ret.append(rg)
                        '''
                    else:
                        for j, author in enumerate([author_pro, author_con]):
                            HasUser = {
                                "name": "IsAuthor",
                                "arguments": [author, dname],
                                "ttype": None,
                                "obs": True,
                                "isneg": False,
                                "target_pos": None}
                            Authorship = {
                                "name": "Authorship",
                                "arguments": [author, par_pos, label, dname],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None}
                            istruth = (j == 0 and label == 'pro') or (j == 1 and label == 'con')
                            body = [InDebate, HasUser]
                            head = Authorship
                            rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        random.shuffle(ret)
        return ret


    def debate_text_zsl_userissue(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        filter_fold = filters[0][1]

        skipped = 0
        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                title = self.debates[dname]['title']

                if title is None:
                   continue

                #for par_pos, text in enumerate(self.debates[dname][par]['posts']):
                #text = self.debates[dname][par]['posts'][par_pos]
                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, par],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                if istrain:
                    HasLabel = {
                            "name": "HasUserLabel",
                            "arguments": [author, -1, dname, label],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                    body = [InDebate, IsAuthor]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

                    # Enlarge dataset with voters
                    '''
                    if filter_fold == "isTrain":
                        for voter in self.votes[dname][author]:
                            if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                                continue

                            IsVoter = {
                                "name": "IsAuthor",
                                "arguments": [voter, dname],
                                "ttype": None,
                                "obs": True,
                                "isneg": False,
                                "target_pos": None}

                            body = [InDebate, IsVoter]
                            head = HasLabel
                            rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
                    '''

                else:
                    for lbl in ['pro', 'con']:
                        istruth = lbl == label
                        HasLabel = {
                                "name": "HasUserLabel",
                                "arguments": [author, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

        '''
        print "pred"
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret


    def debate_text_zsl_voter(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:

            title = self.debates[dname]['title']

            if title is None:
                continue

            for votee in self.votes[dname]:
                for voter in self.votes[dname][votee]:
                    if (voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary):
                        continue
                    InDebate = {
                        "name": "InDebate",
                        "arguments": [-1, dname, ""],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    Votes = {
                        "name": "Votes",
                        "arguments": [voter, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    if istrain:
                        author_pro = self.debates[dname]['pro']['author']
                        author_con = self.debates[dname]['con']['author']

                        if votee == author_pro:
                            lbl = 'pro'
                        else:
                            lbl = 'con'
                        HasLabel = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, Votes]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                    else:
                        for lbl in ['pro', 'con']:
                            author = self.debates[dname][lbl]['author']
                            istruth = (author == votee)
                            HasLabel = {
                                    "name": "InheritsLabel",
                                    "arguments": [voter, -1, dname, lbl],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            body = [InDebate, Votes]
                            head = HasLabel
                            rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret

    def debate_votefor_user(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']
                title = self.debates[dname]['title']

                if title is None:
                   continue

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, ""],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                for votee in self.votes[dname]:
                    for voter in self.votes[dname][votee]:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue

                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        if istrain:
                            if votee == author and not isneg:

                                VoteFor = {
                                    "name": "VoteFor",
                                    "arguments": [voter, author, dname],
                                    "ttype": None,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": None
                                }
                                body = [InDebate, IsAuthor, Votes]
                                head = VoteFor
                                rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                                rg.build_predicate_index()
                                rg.build_body_predicate_dic()
                                ret.append(rg)
                            elif votee != author and isneg:
                                VoteFor = {
                                    "name": "VoteFor",
                                    "arguments": [voter, author, dname],
                                    "ttype": None,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": None
                                }
                                body = [InDebate, IsAuthor, Votes]
                                head = VoteFor
                                rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                                rg.build_predicate_index()
                                rg.build_body_predicate_dic()
                                ret.append(rg)

                        else:
                            VoteFor = {
                                "name": "VoteFor",
                                "arguments": [voter, author, dname],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None
                            }
                            istruth = (votee == author)
                            body = [InDebate, IsAuthor, Votes]
                            head = VoteFor
                            rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        random.shuffle(ret)
        return ret


    def debate_votesame(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            title = self.debates[dname]['title']

            if title is None:
                continue

            InDebate = {
                "name": "InDebate",
                "arguments": [-1, dname, ""],
                "ttype": None,
                "obs": True,
                "isneg": False,
                "target_pos": None}

            all_voters = []; all_valid = []
            for votee in self.votes[dname]:
                valid = set([])
                for voter in self.votes[dname][votee]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue

                    all_voters.append(voter)
                    valid.add(voter)
                all_valid.append(valid)

            if istrain and not isneg:
                for i in [0, 1]:
                    curr_votes = list(all_valid[i])
                    for j in range(len(curr_votes)):
                        voter = curr_votes[j]
                        for voter_2 in curr_votes[j:]:
                            if voter == voter_2:
                                continue
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Votes_2 = {
                            "name": "Votes",
                            "arguments": [voter_2, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteSame = {
                            "name": "VoteSame",
                            "arguments": [voter, voter_2, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None
                        }
                        body = [InDebate, Votes, Votes_2]
                        head = VoteSame
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
            elif istrain and isneg:
                i = 0; j = 1
                for voter in all_valid[i]:
                    for voter_2 in all_valid[j]:
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Votes_2 = {
                            "name": "Votes",
                            "arguments": [voter_2, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteSame = {
                            "name": "VoteSame",
                            "arguments": [voter, voter_2, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": True,
                            "target_pos": None
                        }
                        body = [InDebate, Votes, Votes_2]
                        head = VoteSame
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

            else:
                for voter in all_voters:
                    for voter_2 in all_voters:
                        if voter == voter_2:
                            continue
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Votes_2 = {
                            "name": "Votes",
                            "arguments": [voter_2, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteSame = {
                            "name": "VoteSame",
                            "arguments": [voter, voter_2, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None
                        }
                        istruth = (voter in all_valid[0] and voter_2 in all_valid[0]) or \
                                  (voter in all_valid[1] and voter_2 in all_valid[1])
                        body = [InDebate, Votes, Votes_2]
                        head = VoteSame
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
        return ret


    def agreement_post_author_fixed(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for par_post in ['pro', 'con']:
                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                    len(self.debates[dname][par_post]['posts']) <= 0:
                       continue

                post_author = self.debates[dname][par_post]['author']
                user = self.debates[dname][par_post]['author']
                for par_pos, text in enumerate(self.debates[dname][par_post]['posts']):

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, par_post],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    IsAuthor = {
                        "name": "IsAuthor",
                        "arguments": [user, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    Authorship = {
                        "name": "Authorship",
                        "arguments": [user, par_pos, par_post, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": True,
                        "target_pos": None}

                    for label in ['pro', 'con']:
                        HasLabel = {
                                "name": "HasLabel",
                                "arguments": [post_author, par_pos, dname, label],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        HasUserLabel = {
                                "name": "HasUserLabel",
                                "arguments": [user, -1, dname, label],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor, Authorship, HasLabel]
                        head = HasUserLabel
                        rg = RuleGrounding(body, head, None, False, None, False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

        random.shuffle(ret)
        return ret

    def agreement_post_author(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for par_post in ['pro', 'con']:
                for par_user in ['pro', 'con']:

                    title = self.debates[dname]['title']

                    #if len(title.split()) <= 0 or \
                    if title is None or \
                        len(self.debates[dname][par_post]['posts']) <= 0:
                           continue

                    post_author = self.debates[dname][par_post]['author']
                    user = self.debates[dname][par_user]['author']

                    for par_pos, text in enumerate(self.debates[dname][par_post]['posts']):

                        InDebate = {
                            "name": "InDebate",
                            "arguments": [par_pos, dname, par_post],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        IsAuthor = {
                            "name": "IsAuthor",
                            "arguments": [user, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Authorship = {
                            "name": "Authorship",
                            "arguments": [user, par_pos, par_post, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": True,
                            "target_pos": None}

                        for label in ['pro', 'con']:
                            HasLabel = {
                                    "name": "HasLabel",
                                    "arguments": [post_author, par_pos, dname, label],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            HasUserLabel = {
                                    "name": "HasUserLabel",
                                    "arguments": [user, -1, dname, label],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            body = [InDebate, IsAuthor, Authorship, HasLabel]
                            head = HasUserLabel
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret

    def agreement_author_post_fixed(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for par_post in ['pro', 'con']:
                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                    len(self.debates[dname][par_post]['posts']) <= 0:
                       continue

                post_author = self.debates[dname][par_post]['author']
                user = self.debates[dname][par_post]['author']

                for par_pos, text in enumerate(self.debates[dname][par_post]['posts']):

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, par_post],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    IsAuthor = {
                        "name": "IsAuthor",
                        "arguments": [user, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    Authorship = {
                        "name": "Authorship",
                        "arguments": [user, par_pos, par_post, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    for label in ['pro', 'con']:
                        HasLabel = {
                                "name": "HasLabel",
                                "arguments": [post_author, par_pos, dname, label],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        HasUserLabel = {
                                "name": "HasUserLabel",
                                "arguments": [user, -1, dname, label],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor, Authorship, HasLabel]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, None, False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

        random.shuffle(ret)
        return ret

    def agreement_author_post(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for par_post in ['pro', 'con']:
                for par_user in ['pro', 'con']:

                    title = self.debates[dname]['title']

                    #if len(title.split()) <= 0 or \
                    if title is None or \
                        len(self.debates[dname][par_post]['posts']) <= 0:
                           continue

                    post_author = self.debates[dname][par_post]['author']
                    user = self.debates[dname][par_user]['author']

                    for par_pos, text in enumerate(self.debates[dname][par_post]['posts']):

                        InDebate = {
                            "name": "InDebate",
                            "arguments": [par_pos, dname, par_post],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        IsAuthor = {
                            "name": "IsAuthor",
                            "arguments": [user, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Authorship = {
                            "name": "Authorship",
                            "arguments": [user, par_pos, par_post, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None}

                        for label in ['pro', 'con']:
                            HasLabel = {
                                    "name": "HasLabel",
                                    "arguments": [post_author, par_pos, dname, label],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            HasUserLabel = {
                                    "name": "HasUserLabel",
                                    "arguments": [user, -1, dname, label],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            body = [InDebate, IsAuthor, Authorship, HasLabel]
                            head = HasLabel
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret


    def voter_agreeswith_voter_vsfixed(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:

            title = self.debates[dname]['title']

            if title is None:
                continue

            InDebate = {
                "name": "InDebate",
                "arguments": [0, dname, ""],
                "ttype": None,
                "obs": True,
                "isneg": False,
                "target_pos": None}

            for votee in self.votes[dname]:
                for voter in self.votes[dname][votee]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue
                    for voter_2 in self.votes[dname][votee]:
                        if voter == voter_2:
                            continue
                        if voter_2 not in self.user_info and voter_2 not in self.bigissues and voter_2 not in self.has_summary:
                            continue
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Votes_2 = {
                            "name": "Votes",
                            "arguments": [voter_2, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteSame = {
                            "name": "VoteSame",
                            "arguments": [voter, voter_2, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None
                        }
                        for lbl in ['pro', 'con']:
                            HasLabel = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            HasLabel2 = {
                                "name": "InheritsLabel",
                                "arguments": [voter_2, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            body = [InDebate, Votes, VoteSame, HasLabel]
                            head = HasLabel2
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret


    def voter_agreeswith_voter(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:

            title = self.debates[dname]['title']

            if title is None:
                continue

            InDebate = {
                "name": "InDebate",
                "arguments": [0, dname, ""],
                "ttype": None,
                "obs": True,
                "isneg": False,
                "target_pos": None}

            all_voters = []
            for votee in self.votes[dname]:
                for voter in self.votes[dname][votee]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue
                    all_voters.append(voter)

            for voter in all_voters:
                for voter_2 in all_voters:
                    if voter == voter_2:
                        continue
                    Votes = {
                        "name": "Votes",
                        "arguments": [voter, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    Votes_2 = {
                        "name": "Votes",
                        "arguments": [voter_2, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    VoteSame = {
                        "name": "VoteSame",
                        "arguments": [voter, voter_2, dname],
                        "ttype": None,
                        "obs": False,
                        "isneg": False,
                        "target_pos": None
                    }
                    for lbl in ['pro', 'con']:
                        HasLabel = {
                            "name": "InheritsLabel",
                            "arguments": [voter, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        HasLabel2 = {
                            "name": "InheritsLabel",
                            "arguments": [voter_2, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        body = [InDebate, Votes, VoteSame, HasLabel]
                        head = HasLabel2
                        rg = RuleGrounding(body, head, None, False, None, False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

        random.shuffle(ret)
        return ret

    def voter_agreeswith_user(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']

                title = self.debates[dname]['title']

                if title is None:
                    continue

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, par],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                for votee in self.votes[dname]:
                    for voter in self.votes[dname][votee]:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue

                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteFor = {
                            "name": "VoteFor",
                            "arguments": [voter, author, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None
                        }

                        for lbl in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasUserLabel",
                                "arguments": [author, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            HasLabel2 = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            body = [InDebate, IsAuthor, Votes, VoteFor, HasLabel]
                            head = HasLabel2
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret

    def voter_agreeswith_user_vffixed(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for label in ['pro', 'con']:

                author = self.debates[dname][label]['author']

                title = self.debates[dname]['title']

                if title is None:
                   continue

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, label],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                for voter in self.votes[dname][author]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue

                    Votes = {
                        "name": "Votes",
                        "arguments": [voter, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    VoteFor = {
                        "name": "VoteFor",
                        "arguments": [voter, author, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                    }

                    for lbl in ['pro', 'con']:
                        HasLabel = {
                            "name": "HasUserLabel",
                            "arguments": [author, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        HasLabel2 = {
                            "name": "InheritsLabel",
                            "arguments": [voter, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        body = [InDebate, IsAuthor, Votes, VoteFor, HasLabel]
                        head = HasLabel2
                        rg = RuleGrounding(body, head, None, False, None, False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
        random.shuffle(ret)
        return ret

    def post_not_agreeswith_voter(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:

            title = self.debates[dname]['title']
            if title is None:
               continue

            for label in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[dname][label]['posts']):
                    author_pro = self.debates[dname]['pro']['author']
                    author_con = self.debates[dname]['con']['author']
                    post_author = self.debates[dname][label]['author']

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, label],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    voters = self.votes[dname][author_pro] + self.votes[dname][author_con]
                    for voter in voters:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Endorsement = {
                            "name": "Endorsement",
                            "arguments": [voter, par_pos, label, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": True,
                            "target_pos": None}
                        for label_ in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasLabel",
                                "arguments": [post_author, par_pos, dname, label_],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            VoterLabel = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, label_],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": True,
                                "target_pos": 3}
                            body = [InDebate, Votes, Endorsement, HasLabel]
                            head = VoterLabel
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        random.shuffle(ret)
        return ret

    def post_agreeswith_voter(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:

            title = self.debates[dname]['title']
            if title is None:
               continue

            for label in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[dname][label]['posts']):
                    author_pro = self.debates[dname]['pro']['author']
                    author_con = self.debates[dname]['con']['author']
                    post_author = self.debates[dname][label]['author']

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, label],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    voters = self.votes[dname][author_pro] + self.votes[dname][author_con]
                    for voter in voters:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue
                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        Endorsement = {
                            "name": "Endorsement",
                            "arguments": [voter, par_pos, label, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None}
                        for label_ in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasLabel",
                                "arguments": [post_author, par_pos, dname, label_],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            VoterLabel = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, label_],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            body = [InDebate, Votes, Endorsement, HasLabel]
                            head = VoterLabel
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        random.shuffle(ret)
        return ret


    def voter_not_agreeswith_user(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']

                title = self.debates[dname]['title']

                if title is None:
                   continue

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, label],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                for votee in self.votes[dname]:
                    for voter in self.votes[dname][votee]:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue

                        Votes = {
                            "name": "Votes",
                            "arguments": [voter, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}
                        VoteFor = {
                            "name": "VoteFor",
                            "arguments": [voter, author, dname],
                            "ttype": None,
                            "obs": False,
                            "isneg": True,
                            "target_pos": None
                        }

                        for lbl in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasUserLabel",
                                "arguments": [author, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            HasLabel2 = {
                                "name": "InheritsLabel",
                                "arguments": [voter, -1, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": True,
                                "target_pos": 3}
                            body = [InDebate, IsAuthor, Votes, VoteFor, HasLabel]
                            head = HasLabel2
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)

        random.shuffle(ret)
        return ret

    def user_agreeswith_voter_vffixed(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []
        for dname in filter_d:
            for label in ['pro', 'con']:

                author = self.debates[dname][label]['author']

                title = self.debates[dname]['title']

                if title is None:
                    continue

                # We don't want to do this if our author info is unknown
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, label],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                for voter in self.votes[dname][author]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue

                    Votes = {
                        "name": "Votes",
                        "arguments": [voter, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    VoteFor = {
                        "name": "VoteFor",
                        "arguments": [voter, author, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                    }

                    for lbl in ['pro', 'con']:
                        HasLabel = {
                            "name": "HasUserLabel",
                            "arguments": [author, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        HasLabel2 = {
                            "name": "InheritsLabel",
                            "arguments": [voter, -1, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        body = [InDebate, IsAuthor, Votes, VoteFor, HasLabel2]
                        head = HasLabel
                        rg = RuleGrounding(body, head, None, False, None, False)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
        random.shuffle(ret)
        return ret

    def post_leaning(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']
                title = self.debates[dname]['title']

                for par_pos, text in enumerate(self.debates[dname][par]['posts']):

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [par_pos, dname, label],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}
                    IsAuthor = {
                        "name": "IsAuthor",
                        "arguments": [author, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    if istrain:
                        if self.user_info[author]["Leaning"] is None:
                            continue

                        lbl = self.user_info[author]["Leaning"]
                        HasLeaning = {
                                "name": "HasLeaning",
                                "arguments": [author, par_pos, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor]
                        head = HasLeaning
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)

                    else:
                        for lbl in ['Liberal', 'Conservative']:
                            istruth = lbl == self.user_info[author]['Leaning']
                            HasLeaning = {
                                    "name": "HasLeaning",
                                    "arguments": [author, par_pos, dname, lbl],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 3}
                            body = [InDebate, IsAuthor]
                            head = HasLeaning
                            rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        '''
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret

    def user_leaning(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:
                label = par[:3]

                author = self.debates[dname][label]['author']
                par_pos = 0

                InDebate = {
                    "name": "InDebate",
                    "arguments": [par_pos, dname, label],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsAuthor = {
                    "name": "IsAuthor",
                    "arguments": [author, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                if istrain:
                    if self.user_info[author]["Leaning"] is None:
                        continue

                    lbl = self.user_info[author]["Leaning"]
                    HasLeaning = {
                            "name": "HasLeaning",
                            "arguments": [author, par_pos, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                    body = [InDebate, IsAuthor]
                    head = HasLeaning
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

                else:
                    for lbl in ['Liberal', 'Conservative']:
                        istruth = lbl == self.user_info[author]['Leaning']
                        HasLeaning = {
                                "name": "HasLeaning",
                                "arguments": [author, par_pos, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                        body = [InDebate, IsAuthor]
                        head = HasLeaning
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
        '''
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret
        pass

    def title_leaning(self, istrain, isneg, filters, split_class, instance_id):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            par = 'pro'
            label = par[:3]

            author = self.debates[dname][label]['author']
            title = self.debates[dname]['title']
            par_pos = 0

            InDebate = {
                "name": "InDebate",
                "arguments": [par_pos, dname, label],
                "ttype": None,
                "obs": True,
                "isneg": False,
                "target_pos": None}
            IsAuthor = {
                "name": "IsAuthor",
                "arguments": [author, dname],
                "ttype": None,
                "obs": True,
                "isneg": False,
                "target_pos": None}
            if istrain:
                if self.user_info[author]["Leaning"] is None:
                    continue

                lbl = self.user_info[author]["Leaning"]
                HasLeaning = {
                        "name": "HasLeaning",
                        "arguments": [author, par_pos, dname, lbl],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": False,
                        "target_pos": 3}
                body = [InDebate, IsAuthor]
                head = HasLeaning
                rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                rg.build_predicate_index()
                rg.build_body_predicate_dic()
                ret.append(rg)

            else:
                for lbl in ['Liberal', 'Conservative']:
                    istruth = lbl == self.user_info[author]['Leaning']
                    HasLeaning = {
                            "name": "HasLeaning",
                            "arguments": [author, par_pos, dname, lbl],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                    body = [InDebate, IsAuthor]
                    head = HasLeaning
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
        '''
        for rg in ret:
            print "\t", rg
        '''
        # Add ideological tweets to training data
        if istrain and filters[0][1] == 'isTrain':
            ret += self.tweet_ideology()

        random.shuffle(ret)
        return ret

    def tweet_ideology(self):
        ret = []
        with open(self.tweet_ideological, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter='|')
            for i, row in enumerate(reader):
                dname = "tw_{0}".format(i)
                InDebate = {
                    "name": "InDebate",
                    "arguments": [0, dname, 'pro'],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                IsText = {
                    "name": "IsText",
                    "arguments": [dname, row[0]],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}
                HasLeaning = {
                        "name": "HasLeaning",
                        "arguments": ['author', 0, dname, row[1]],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": False,
                        "target_pos": 3}
                body = [InDebate, IsText]
                head = HasLeaning
                rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                rg.build_predicate_index()
                rg.build_body_predicate_dic()
                ret.append(rg)
        return ret

    def stance_bigissues_user(self, istrain, isneg, filters, split_class, instance_id):
        filter_u = []
        for fil in filters:
            if fil[0] == "U":
                filter_u += self.filters[fil[1]]

        if len(filter_u) == 0:
            filter_u = self.bigissues.keys()

        ret = []
        for author in filter_u:
            for issue in self.bigissues[author]:
                KnownStance = {
                    "name": "KnownStance",
                    "arguments": [author, issue],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                if istrain:
                    bigi_stance = self.bigissues[author][issue]
                    HasStance = {
                            "name": "HasStance",
                            "arguments": [author, issue, bigi_stance],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 2}
                    body = [KnownStance]
                    head = HasStance
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
                else:
                    for lbl in ['pro', 'con']:
                        istruth = lbl == self.bigissues[author][issue]
                        HasStance = {
                                "name": "HasStance",
                                "arguments": [author, issue, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 2}
                        body = [KnownStance]
                        head = HasStance
                        rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
        random.shuffle(ret)
        return ret

    def agreement_same_author(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in ['pro', 'con']:

                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                   len(self.debates[dname][par]) <= 0:
                       continue

                for i, text in enumerate(self.debates[dname][par]['posts']):
                    for j, text2 in enumerate(self.debates[dname][par]['posts']):
                        if i == j:
                            continue

                        author = self.debates[dname][par[:3]]['author']
                        InDebate = {
                            "name": "InDebate",
                            "arguments": [i, dname, par],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        InDebate2 = {
                            "name": "InDebate",
                            "arguments": [j, dname, par],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        SameAuthor = {
                            "name": "SameAuthor",
                            "arguments": [i, j, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        for lbl in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasLabel",
                                "arguments": [author, i, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            HasLabel2 = {
                                "name": "HasLabel",
                                "arguments": [author, j, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            body = [InDebate, InDebate2, SameAuthor, HasLabel]
                            head = HasLabel2
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        '''
        print "Agree"
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret

    def disagreement_diff_user(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in [('pro', 'con'),
                        ('con', 'pro')]:

                title = self.debates[dname]['title']

                if title is None:
                   continue

                author = self.debates[dname][par[0][:3]]['author']
                author2 = self.debates[dname][par[1][:3]]['author']

                InDebate = {
                    "name": "InDebate",
                    "arguments": [-1, dname, par[0]],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                InDebate2 = {
                    "name": "InDebate",
                    "arguments": [-1, dname, par[1]],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}

                Debaters = {
                    "name": "Debaters",
                    "arguments": [author, author2, dname],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None}


                for lbl in ['pro', 'con']:
                    HasLabel = {
                        "name": "HasUserLabel",
                        "arguments": [author, -1, dname, lbl],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": False,
                        "target_pos": 3}
                    HasLabel2 = {
                        "name": "HasUserLabel",
                        "arguments": [author2, -1, dname, lbl],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": True,
                        "target_pos": 3}
                    body = [InDebate, InDebate2, Debaters, HasLabel]
                    head = HasLabel2
                    rg = RuleGrounding(body, head, None, False, None, False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

        '''
        print "Disagree"
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret

    def disagreement_diff_author(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in [('pro', 'con'),
                        ('con', 'pro')]:

                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                   len(self.debates[dname][par[0]]['posts']) <= 0 or \
                   len(self.debates[dname][par[1]]['posts']) <= 0:
                       continue

                author = self.debates[dname][par[0][:3]]['author']
                author2 = self.debates[dname][par[1][:3]]['author']

                # Comment this when using post level prediction
                #if author not in self.bigissues or author2 not in self.bigissues:
                #    continue

                for i, text in enumerate(self.debates[dname][par[0]]['posts']):
                    for j, text2 in enumerate(self.debates[dname][par[1]]['posts']):

                        InDebate = {
                            "name": "InDebate",
                            "arguments": [i, dname, par[0]],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        InDebate2 = {
                            "name": "InDebate",
                            "arguments": [j, dname, par[1]],
                            "ttype": None,
                            "obs": True,
                            "isneg": False,
                            "target_pos": None}

                        SameAuthor = {
                            "name": "SameAuthor",
                            "arguments": [i, j, dname],
                            "ttype": None,
                            "obs": True,
                            "isneg": True,
                            "target_pos": None}

                        for lbl in ['pro', 'con']:
                            HasLabel = {
                                "name": "HasLabel",
                                "arguments": [author, i, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": False,
                                "target_pos": 3}
                            HasLabel2 = {
                                "name": "HasLabel",
                                "arguments": [author2, j, dname, lbl],
                                "ttype": LabelType.Multiclass,
                                "obs": False,
                                "isneg": True,
                                "target_pos": 3}
                            body = [InDebate, InDebate2, SameAuthor, HasLabel]
                            head = HasLabel2
                            rg = RuleGrounding(body, head, None, False, None, False)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        '''
        print "Disagree"
        for rg in ret:
            print "\t", rg
        '''
        random.shuffle(ret)
        return ret

    def vote_novote(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in [('pro', 'con'),
                        ('con', 'pro')]:

                title = self.debates[dname]['title']

                #if len(title.split()) <= 0 or \
                if title is None or \
                   len(self.debates[dname][par[0]]['posts']) <= 0 or \
                   len(self.debates[dname][par[1]]['posts']) <= 0:
                       continue

                author = self.debates[dname][par[0]]['author']
                author2 = self.debates[dname][par[1]]['author']

                for voter in (self.votes[dname][author] + self.votes[dname][author2]):
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue

                    InDebate = {
                        "name": "InDebate",
                        "arguments": [-1, dname, par[0]],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    InDebate2 = {
                        "name": "InDebate",
                        "arguments": [-1, dname, par[1]],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    Debaters = {
                        "name": "Debaters",
                        "arguments": [author, author2, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    VoteFor = {
                        "name": "VoteFor",
                        "arguments": [voter, author, dname],
                        "ttype": None,
                        "obs": False,
                        "isneg": False,
                        "target_pos": None}

                    VoteFor2 = {
                        "name": "VoteFor",
                        "arguments": [voter, author2, dname],
                        "ttype": None,
                        "obs": False,
                        "isneg": True,
                        "target_pos": None}

                    body = [InDebate, InDebate2, Debaters, VoteFor]
                    head = VoteFor2
                    rg = RuleGrounding(body, head, None, False, None, False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
        random.shuffle(ret)
        return ret

    def novote_vote(self, filters, instance_id=None):
        filter_d = self.filter_debates(filters, instance_id)
        ret = []

        for dname in filter_d:
            for par in [('pro', 'con'),
                        ('con', 'pro')]:

                title = self.debates[dname]['title']

                if title is None:
                   continue

                author = self.debates[dname][par[0]]['author']
                author2 = self.debates[dname][par[1]]['author']

                for voter in (self.votes[dname][author] + self.votes[dname][author2]):
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue
                    InDebate = {
                        "name": "InDebate",
                        "arguments": [-1, dname, par[0]],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    InDebate2 = {
                        "name": "InDebate",
                        "arguments": [-1, dname, par[1]],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    Debaters = {
                        "name": "Debaters",
                        "arguments": [author, author2, dname],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None}

                    VoteFor = {
                        "name": "VoteFor",
                        "arguments": [voter, author, dname],
                        "ttype": None,
                        "obs": False,
                        "isneg": True,
                        "target_pos": None}

                    VoteFor2 = {
                        "name": "VoteFor",
                        "arguments": [voter, author2, dname],
                        "ttype": None,
                        "obs": False,
                        "isneg": False,
                        "target_pos": None}

                    body = [InDebate, InDebate2, Debaters, VoteFor]
                    head = VoteFor2
                    rg = RuleGrounding(body, head, None, False, None, False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
        random.shuffle(ret)
        return ret

    def get_ruleset_instances(self, *args):
        filters = args[2]
        filter_d = []
        for fil in filters:
            if fil[0] == "D":
                filter_d += self.filters[fil[1]]
        #return ['068894', '066248']
        if len(filter_d) > 0:
            return filter_d
        else:
            return self.debates.keys()

    def get_gold_predicates(self, *args):

        instance_id = args[3]
        ret = []

        if self.post_mode:
            for par in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[instance_id][par]['posts']):
                    author = self.debates[instance_id][par]['author']

                    # Comment this when using post ZSL
                    #if author not in self.bigissues:
                    #    continue

                    HasLabel = {
                            "name": "HasLabel",
                            "arguments": [author, par_pos, instance_id, par],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                    ret.append(HasLabel)

        if self.endorse_mode:
            for par in ['pro', 'con']:
                for par_pos, text in enumerate(self.debates[instance_id][par]['posts']):
                    author = self.debates[instance_id][par]['author']
                    for voter in self.votes[instance_id][author]:
                        if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                            continue

                        # Comment this when using post ZSL
                        #if author not in self.bigissues:
                        #    continue

                        Endorse = {
                                "name": "Endorsement",
                                "arguments": [voter, par_pos, par, instance_id],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None}
                        ret.append(Endorse)

        if self.user_mode:
            for par in ['pro', 'con']:
                author = self.debates[instance_id][par]['author']
                if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                    continue

                HasLabel = {
                        "name": "HasUserLabel",
                        "arguments": [author, -1, instance_id, par],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": False,
                        "target_pos": 3}
                ret.append(HasLabel)

        if self.voter_mode or self.voter_fixed:
            all_voters = []; curr_votes = []
            for enum, par in enumerate(['pro', 'con']):
                author = self.debates[instance_id][par]['author']
                curr_votes.append(set([]))
                for voter in self.votes[instance_id][author]:
                    if voter not in self.user_info and voter not in self.bigissues and voter not in self.has_summary:
                        continue

                    all_voters.append(voter)
                    curr_votes[enum].add(voter)
                    if self.voter_mode:
                        if author not in self.user_info and author not in self.bigissues and author not in self.has_summary:
                            continue
                        VoteFor = {
                            "name": "VoteFor",
                            "arguments": [voter, author, instance_id],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None}
                        ret.append(VoteFor)
                    if self.voter_mode or self.voter_fixed:
                        InheritsLabel = {
                            "name": "InheritsLabel",
                            "arguments": [voter, -1, instance_id, par],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 3}
                        ret.append(InheritsLabel)

            # VoteSame
            if self.voter_mode:
                for voter in all_voters:
                    for voter_2 in all_voters:
                        if voter == voter_2:
                            continue
                        if (voter in curr_votes[0] and voter_2 in curr_votes[0]) or \
                            (voter in curr_votes[1] and voter_2 in curr_votes[1]):
                            VoteSame = {
                                "name": "VoteSame",
                                "arguments": [voter, voter_2, instance_id],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None}
                            ret.append(VoteSame)

        random.shuffle(ret)
        return ret

