import json
from collections import Counter
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd

CATEGORIES = ['Politics', 'Society', 'Philosophy', 'Religion']



for CAT in CATEGORIES:
    print "Processing", CAT, "..."

    with open('posts_{0}.json'.format(CAT)) as f:
        posts = json.load(f)
    with open('posts_{0}.json'.format(CAT)) as f:
        posts = json.load(f)

    print "Debates:", len(posts)

    paragraphs_per_debate = []
    sentences_per_paragraph = []
    unique_sentences = []

    for debate in posts:
        #print posts[debate]['con_paragraphs']
        #print posts[debate]['pro_paragraphs']
        paragraphs_per_debate.append(len(posts[debate]['con_paragraphs']) +
                                     len(posts[debate]['pro_paragraphs']))
        for par in posts[debate]['con_paragraphs'] + posts[debate]['pro_paragraphs']:
            sentences_per_paragraph.append(len(par))
            unique_sentences += par

    print "total paragraphs", sum(paragraphs_per_debate)
    print "total sentences", sum(sentences_per_paragraph)
    print "total unique sentences", len(set(unique_sentences))

    labels_par, values_par = zip(*Counter(paragraphs_per_debate).items())
    labels_sen, values_sen = zip(*Counter(sentences_per_paragraph).items())

    indexes_par = np.arange(len(labels_par))
    indexes_sen = np.arange(len(labels_sen))
    width = 1


    plt.bar(indexes_par, values_par, width)
    plt.savefig('paragraph_per_debate_{0}.png'.format(CAT))

    plt.bar(indexes_sen, values_sen, width)
    plt.savefig('sentences_per_paragraph_{0}.png'.format(CAT))

    print "Paragraphs per debate:"
    s = pd.Series(paragraphs_per_debate)
    print s.describe()
    print
    print "Sentences per paragraph:"
    s = pd.Series(sentences_per_paragraph)
    print s.describe()
