import os
from glob import glob
import random
import json

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)

random.seed(4321)
# create a dictionary that maps username to ids
username_id = {}
with open('data/usernames.txt') as f:
    for i, line in enumerate(f):
        username_id[line.strip()] = str(i).zfill(8)

print "Number of users:", len(username_id)

# parse issues
relevant = set(["Gay Marriage", "Drug Legalization",
                "Global Warming Exists", "Environmental Protection",
                "Minimum Wage", "Abortion", "Death Penalty",
                "Gun Rights", "Smoking Ban", "Medical Marijuana"])

issues_pro = {}; issues_con = {}

for username in username_id.values():
    filename = os.path.join("data/issues", "{0}.txt".format(username))
    if os.path.isfile(filename):
        with open(filename) as f:
            for line in f:
                pr = line.split('Comments')
                issue, stance = pr[0].split(':')
                issue = issue.strip()
                stance = stance.strip()

                if issue not in relevant:
                    continue

                if stance == "Pro":
                    if issue not in issues_pro:
                        issues_pro[issue] = set([username])
                    else:
                        issues_pro[issue].add(username)
                elif stance == "Con":
                    if issue not in issues_con:
                        issues_con[issue] = set([username])
                    else:
                        issues_con[issue].add(username)
                else:
                    #print stance
                    pass


with open("issues_pro.json", 'w') as f:
    json.dump(issues_pro, f, cls=SetEncoder)

with open("issues_con.json", 'w') as f:
    json.dump(issues_con, f, cls=SetEncoder)

# count issues
users_with_stances = set([])
print "------ ISSUES -------"
for issue in set(issues_pro.keys()) & relevant:
    total = len(issues_pro[issue]) + len(issues_con[issue])
    print issue, total

    print "\tPro", len(issues_pro[issue])/(1.0 * total)
    print "\tCon", len(issues_con[issue])/(1.0 * total)

    users_with_stances |= issues_pro[issue]
    users_with_stances |= issues_con[issue]
print
print "Number of issues", len(relevant)
print "Users with stances:", len(users_with_stances)
print "---------------------"
# get friend graph

print
print "------ GRAPH -------"
friends = {}
friend_edges = 0
for username in users_with_stances:
    filename = os.path.join("data/friends", "{0}.txt".format(username))
    if os.path.isfile(filename):
        with open(filename) as f:
            for line in f:
                if username not in friends:
                    friends[username] = set([username_id[line.strip()]])
                    friend_edges += 1
                else:
                    friends[username].add(username_id[line.strip()])
                    friend_edges += 1
print "Users in friend graph", len(friends)
print "Friendship edges", friend_edges

# parse debates
argues = {}; debates = {}
argue_edges = 0

for fname in glob("data/debates/*_info.txt"):
    #print fname
    with open(fname) as f:
        pro_username = None; con_username = None; dname = None
        for i, line in enumerate(f):
            fields = line.strip().split(':')

            if i == 2 and fields[0] == "Pro Username":
                instigator = "pro"; contender = "con"
            elif i == 2 and fields[0] == "Con Username":
                instigator = "con"; contender = "pro"

            if fields[0] == "Pro Username":
                pro_username = fields[1]
            if fields[0] == "Con Username":
                con_username = fields[1]
            if fields[0]  == "Debate No":
                dname = fields[1].zfill(6)

            if fields[0] == "Title":
                dtitle = fields[1]
            if fields[0] == "Category":
                dcategory = fields[1]

        if pro_username and con_username and\
           username_id[pro_username] in users_with_stances and\
           username_id[con_username] in users_with_stances:
            if username_id[pro_username] not in argues:
                argues[username_id[pro_username]] = set([(username_id[con_username], dname)])
            else:
                argues[username_id[pro_username]].add((username_id[con_username], dname))
            if username_id[con_username] not in argues:
                argues[username_id[con_username]] = set([(username_id[pro_username], dname)])
            else:
                argues[username_id[con_username]].add((username_id[pro_username], dname))
            argue_edges += 2

        if username_id[pro_username] in users_with_stances:
            f = open("data/debates/{0}_pro.txt".format(dname))
            pro_paragraphs = f.readlines()
            f.close()
            debates[dname] = {'pro': username_id[pro_username],
                              'contender': contender,
                              'instigator': instigator,
                              'title': dtitle,
                              'category': dcategory,
                              'pro_paragraphs': pro_paragraphs}
        if username_id[con_username] in users_with_stances:
            f = open("data/debates/{0}_con.txt".format(dname))
            con_paragraphs = f.readlines()
            f.close()
            if dname not in debates:
                debates[dname] = {'con': username_id[con_username],
                                  'contender': contender,
                                  'instigator': instigator,
                                  'title': dtitle,
                                  'category': dcategory,
                                  'con_paragraphs': con_paragraphs}
            else:
                debates[dname]['con'] = username_id[con_username]
                debates[dname]['con_paragraphs'] = con_paragraphs

print "Users in argue graph", len(argues), "edges", argue_edges
print "Debates", len(debates)

# get votes graph, make it debate agnostic
# I consider a vote to debater assigned more points
votes = {}; vote_edges = 0
# pos 0 username, pos 7 pts instigator, pos 8 pts contender

wrong_files = 0
for dname in debates:
    filename = os.path.join("data/votes", "{0}.txt".format(dname))
    if os.path.isfile(filename):
        with open(filename) as f:
            instigator_pts = None; contender_pts = None
            for line in f:
                fields = line.strip().split()
                if len(fields) != 9:
                    break
                user_id = username_id[fields[0]]
                try:
                    instigator_pts = int(fields[7])
                    contender_pts = int(fields[8])
                except:
                    break

            if instigator_pts == None or contender_pts == None:
                wrong_files +=1
                continue

            if (instigator_pts > contender_pts or instigator_pts == contender_pts) and \
               debates[dname]['instigator'] in debates[dname] and \
               debates[dname][debates[dname]['instigator']] in users_with_stances and \
               user_id in users_with_stances:

                   if user_id not in votes:
                       votes[user_id] = set([(debates[dname][debates[dname]['instigator']], dname)])
                   else:
                       votes[user_id].add((debates[dname][debates[dname]['instigator']], dname))
                   vote_edges += 1
            elif (contender_pts > instigator_pts or contender_pts == instigator_pts) and \
               debates[dname]['contender'] in debates[dname] and \
               debates[dname][debates[dname]['contender']] in users_with_stances and \
               user_id in users_with_stances:

                   if user_id not in votes:
                       votes[user_id] = set([(debates[dname][debates[dname]['contender']], dname)])
                   else:
                       votes[user_id].add((debates[dname][debates[dname]['contender']], dname))
                   vote_edges +=1
print "Users in vote graph", len(votes), "edges", vote_edges
#print "Wrong files", wrong_files, "Total files", len(debates)

print "Isolated users", len(users_with_stances - (set(friends.keys()) | set(argues.keys()) | set(votes.keys()) ))
print "----------------------"
# write friendship graph
f = open("adjedges_friends.txt", "w")
for node in users_with_stances:
    f.write(node)
    if node in friends:
        for friend in friends[node]:
            f.write(" " + friend)
    f.write(" \n")
f.close()

# write argues graph
f = open("adjedges_argues.txt", "w")
for node in users_with_stances:
    f.write(node)
    if node in argues:
        for (debater, dname) in argues[node]:
            f.write(" (" + debater + "," + dname + ")")
            #f.write(" " + debater)
    f.write(" \n")
f.close()

# write argues graph
f = open("adjedges_votes.txt", "w")
for node in users_with_stances:
    f.write(node)
    if node in votes:
        for (cand, dname) in votes[node]:
            f.write(" (" + cand + "," + dname + ")")
            #f.write(" " + cand)
    f.write(" \n")
f.close()

with open("debates.json", 'w') as f:
    json.dump(debates, f)

nodelist = list(users_with_stances)
for i in range(5):
    random.shuffle(nodelist)
    train = nodelist[:len(nodelist)/2]
    test = nodelist[len(nodelist)/2:]

    with open(os.path.join("folds", "train{0}".format(i)), 'w') as f:
        for t in train:
            f.write(t + "\n")

    with open(os.path.join("folds", "test{0}".format(i)), 'w') as f:
        for t in test:
            f.write(t + "\n")

f = open("docs.txt", "w")
f1 = open("labels.txt", "w")
for user in users_with_stances:
    fname = os.path.join("data/summaries", "{0}.txt".format(user))
    doctext = ""
    if os.path.isfile(fname):
        with open(fname) as f2:
            for line in f2:
                doctext += line.strip().split(':')[1]
                doctext += ". "
    f.write(user + " " + doctext.strip() + "\n")
    f1.write(user + " 0\n") # this is a dummy class
