from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

import json
import numpy as np
import re

class Debate_ft(FeatureExtractor):

    def __init__(self,
                 doc2vec_filename,
                 deepWalk_friends_filename,
                 deepWalk_argues_filename,
                 deepWalk_votes_filename):
        super(Debate_ft, self).__init__()
        self.doc2vec_filename = doc2vec_filename
        self.deepWalk_friends_filename = deepWalk_friends_filename
        self.deepWalk_argues_filename = deepWalk_argues_filename
        self.deepWalk_votes_filename = deepWalk_votes_filename
        np.random.seed(17477)

    def build(self):
        self.vocab_size = None
        self.issue_idx = {"Pro":1,
                          "Con":0
                         }

        if self.doc2vec_filename:
            self.num_doc_vectos, self.doc2vec_size, self.doc2vec_dict = \
                    utils.embeddings_dictionary(self.doc2vec_filename)
        if self.deepWalk_friends_filename:
            self.num_deepWalk_friends_vectors,\
            self.deepWalk_friends_size,\
            self.deepWalk_friends_dict = \
                    utils.embeddings_dictionary(self.deepWalk_friends_filename)
        if self.deepWalk_argues_filename:
            self.num_deepWalk_argues_vectors,\
            self.deepWalk_argues_size,\
            self.deepWalk_argues_dict = \
                    utils.embeddings_dictionary(self.deepWalk_argues_filename)
        if self.deepWalk_votes_filename:
            self.num_deepWalk_votes_vectors,\
            self.deepWalk_votes_size,\
            self.deepWalk_votes_dict = \
                    utils.embeddings_dictionary(self.deepWalk_votes_filename)

    def bias(self, rule_grd):
        return [1]

    def doc2vec(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.doc2vec_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[0]['arguments'][0]
        return self.doc2vec_dict[str(doc)]

    def deepWalk_friends(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_friends_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[0]['arguments'][0]
        return self.deepWalk_friends_dict[str(doc)]

    def deepWalk_argues(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_argues_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[0]['arguments'][0]
        return self.deepWalk_argues_dict[str(doc)]

    def deepWalk_votes(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_votes_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[0]['arguments'][0]
        return self.deepWalk_votes_dict[str(doc)]

    def doc2vec_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.doc2vec_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[1]['arguments'][0]
        return self.doc2vec_dict[str(doc)]

    def deepWalk_friends_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_friends_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[1]['arguments'][0]
        return self.deepWalk_friends_dict[str(doc)]

    def deepWalk_argues_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_argues_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[1]['arguments'][0]
        return self.deepWalk_argues_dict[str(doc)]

    def deepWalk_votes_B(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            rndm = np.random.uniform(-0.0025, 0.0025, self.deepWalk_votes_size)
            return map(float, list(rndm))
        doc = rule_grd.get_body_predicates("InInstance")[1]['arguments'][0]
        return self.deepWalk_votes_dict[str(doc)]

    def extract_label(self, instance_grd):
        labels = instance_grd.get_body_predicates("HasLabel")
        labels = [l['arguments'][1] for l in labels]
        label_idxs = [self.issue_idx[str(l)] for l in labels]
        vec = []
        for lbl_idx in label_idxs:
            vec += [(i == lbl_idx)*1 for i in range(len(self.issue_idx))]
        return vec

    # As reference from Ibrahim
    def extract_userinfo(self, instance_grd):
        uids = [user['arguments'][0] for user in instance_grd.get_body_predicates("User")]

        v = []
        for  uid in uids:
            uid = '0'*(8-len(str(uid)))+str(uid)
            v = np.hstack([v, self.userinfo[uid]])

        return v

    def extract_multiclass_head(self, instance_grd):
        label = instance_grd.get_head_predicate()['arguments'][1]
        return self.issue_idx[str(label)]
