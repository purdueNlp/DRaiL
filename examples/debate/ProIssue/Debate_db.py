import os
import json
import networkx as nx

from drail.model.rule import RuleGrounding
from drail.model.label import LabelType
import random

def findPaths(G,u,n,excludeSet = None):
    if excludeSet == None:
        excludeSet = set([u])
    else:
        excludeSet.add(u)
    if n==0:
        return [[u]]
    if u in G:
        paths = [[u]+path for neighbor in G[u] if neighbor not in excludeSet
                for path in findPaths(G,neighbor,n-1,excludeSet)]
        excludeSet.remove(u)
    else:
        paths = []
    return paths

class SetDecoder(json.JSONDecoder):
    def default(self, obj):
        if isinstance(obj, list):
            return set(obj)
        return json.JSONDecoder.default(self, obj)


class Debate_db():

    def __init__(self):
        self.friends = {}
        self.votes = {}
        self.argues = {}
        self.stances = {}
        random.seed(2017)

    def load_graph(self, filename, istuple=False):
        graph = {}
        with open(filename) as f:
            for line in f:
                nodes = line.strip().split()
                if istuple:
                    nodes = [nodes[0]] + [n[1:-1].split(',')[0] for n in nodes[1:]]
                if len(nodes[1:]) > 0:
                    graph[nodes[0]] = nodes[1:]
        return graph

    def load_graph_triplets(self, filename):
        graph = {}
        with open(filename) as f:
            for line in f:
                nodes = line.strip().split()
                nodes = [nodes[0]] + [tuple(n[1:-1].split(',')) for n in nodes[1:]]
                if len(nodes[1:]) > 0:
                    graph[nodes[0]] = nodes[1:]
        return graph

    def load_issues(self, filenamecon, filenamepro, issue):
        with open(filenamepro) as f:
            pro = json.load(f, cls=SetDecoder)
        with open(filenamecon) as f:
            con = json.load(f, cls=SetDecoder)
        stances = {}
        for user in pro[issue]:
            stances[user] = 'Pro'
        for user in con[issue]:
            stances[user] = 'Con'
        return stances

    def load_data(self, dataset_path):
        self.dataset_path = dataset_path
        self.friends = self.load_graph(os.path.join(dataset_path, 'adjedges_friends.txt'))
        self.argues = self.load_graph(os.path.join(dataset_path, 'adjedges_argues.txt'), istuple=True)
        self.votes = self.load_graph(os.path.join(dataset_path, 'adjedges_votes.txt'), istuple=True)

        print "friends", len(self.friends)
        print "argues", len(self.argues)
        print "votes", len(self.votes)

        self.argues_edgeinfo = self.load_graph_triplets(os.path.join(dataset_path, 'adjedges_argues.txt'))
        self.votes_edgeinfo = self.load_graph_triplets(os.path.join(dataset_path, 'adjedges_votes.txt'))

    def add_filters(self, filters=[]):
        self.filters = {}
        for fil in filters:
            self.filters[fil[1]] = fil[3]

    def load_stances(self, issue, K, db_funcs):
        self.stances = self.load_issues(
                os.path.join(self.dataset_path, 'issues_con.json'),
                os.path.join(self.dataset_path, 'issues_pro.json'),
                issue)

        # used to break down the problem in global learning
        self.instances = {}
        portion = self.stances.keys()

        random.shuffle(portion)
        #for j, node in enumerate(portion[:10]):
        for j, node in enumerate(portion):
            self.instances[str(j+1)] = set([])
            if node not in self.friends or\
               node not in self.argues or\
               node not in self.votes:
                self.instances[str(j+1)].add(node)
            for i in range(1, K):
                paths = []
                if 'friends_gr' in db_funcs:
                    paths += findPaths(self.friends, node, i)
                if 'argues_gr' in db_funcs:
                    paths += findPaths(self.argues, node, i)
                if 'votes_gr' in db_funcs:
                    paths += findPaths(self.votes, node, i)
                for p in paths:
                    for v in p:
                        if v in self.stances:
                            self.instances[str(j+1)].add(v)
        self.instances["0"] = set(self.stances.keys())

    def base(self, istrain, isneg, filters, split_class, instance_id):
        if instance_id is None:
            instance_id = "0"
        filter_a = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
        filter_a = set(filter_a) & set(self.stances.keys())

        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())

        ret = []

        for node in filter_a & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }

            if istrain:
                HasLabel = {
                        "name": "HasLabel",
                        "arguments": [node, self.stances[node]],
                        "ttype": LabelType.Multiclass,
                        "obs": False,
                        "isneg": False,
                        "target_pos": 1
                        }
                body = [InInstance]
                head = HasLabel
                rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                rg.build_predicate_index()
                rg.build_body_predicate_dic()
                ret.append(rg)

            else:
                for stance in ["Pro", "Con"]:
                    HasLabel = {
                            "name": "HasLabel",
                            "arguments": [node, stance],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 1
                            }

                    body = [InInstance]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], self.stances[node] == stance)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

        return ret

    def argues_gr(self, istrain, isneg, filters, split_class, instance_id):
        return self.graph_helper(istrain, isneg, filters, split_class, instance_id, "Argues")

    def friends_gr(self, istrain, isneg, filters, split_class, instance_id):
        return self.graph_helper(istrain, isneg, filters, split_class, instance_id, "Friends")

    def votes_gr(self, istrain, isneg, filters, split_class, instance_id):
        return self.graph_helper(istrain, isneg, filters, split_class, instance_id, "Votes")

    def argues_same(self, istrain, isneg, filters, split_class, instance_id):
        return self.agreement_helper(istrain, isneg, filters, split_class, instance_id, "Argues")

    def friends_same(self, istrain, isneg, filters, split_class, instance_id):
        return self.agreement_helper(istrain, isneg, filters, split_class, instance_id, "Friends")

    def votes_same(self, istrain, isneg, filters, split_class, instance_id):
        return self.agreement_helper(istrain, isneg, filters, split_class, instance_id, "Votes")

    def graph_helper(self, istrain, isneg, filters, split_class, instance_id, graph_name):
        if graph_name == "Friends":
            graph = self.friends
        elif graph_name == "Argues":
            graph = self.argues
        elif graph_name == "Votes":
            graph = self.votes
        else:
            exit(-1)

        if instance_id is None:
            instance_id = "0"
        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            elif fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])
        filter_a = set(filter_a) & set(self.stances.keys())
        filter_b = set(filter_b) & set(self.stances.keys())
        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())
        if len(filter_b) == 0:
            filter_b = set(self.stances.keys())

        ret = []
        for node in filter_a & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }
            if node not in graph:
                continue

            for node_ in filter_b & self.instances[instance_id] & set(graph[node]):
                InInstance_ = {
                        "name": "InInstance",
                        "arguments": [node_, instance_id],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                Graph = {
                        "name": graph_name,
                        "arguments": [node, node_],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }

                if istrain:
                    HasLabel = {
                            "name": "HasLabel",
                            "arguments": [node, self.stances[node]],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 1
                            }
                    HasLabel_ = {
                            "name": "HasLabel",
                            "arguments": [node_, self.stances[node_]],
                            "ttype": LabelType.Multiclass,
                            "obs": False,
                            "isneg": False,
                            "target_pos": 1
                            }
                    body = [InInstance, InInstance_, Graph, HasLabel_]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, head['arguments'][0], True)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)

                else:
                    for stance in ["Pro", "Con"]:
                        for stance_ in ["Pro", "Con"]:
                            HasLabel = {
                                    "name": "HasLabel",
                                    "arguments": [node, stance],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 1
                                    }
                            HasLabel_ = {
                                    "name": "HasLabel",
                                    "arguments": [node_, stance_],
                                    "ttype": LabelType.Multiclass,
                                    "obs": False,
                                    "isneg": False,
                                    "target_pos": 1
                                    }

                            istruth = stance == self.stances[node] and stance_ == self.stances[node_]
                            body = [InInstance, InInstance_, Graph, HasLabel_]
                            head = HasLabel
                            rg = RuleGrounding(body, head, None, False, head['arguments'][0], istruth)
                            rg.build_predicate_index()
                            rg.build_body_predicate_dic()
                            ret.append(rg)
        return ret

    def agreement_helper(self, istrain, isneg, filters, split_class, instance_id, graph_name):
        if graph_name == "Friends":
            graph = self.friends
        elif graph_name == "Argues":
            graph = self.argues
        elif graph_name == "Votes":
            graph = self.votes
        else:
            exit(-1)

        if instance_id is None:
            instance_id = "0"
        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            elif fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])
        filter_a = set(filter_a) & set(self.stances.keys())
        filter_b = set(filter_b) & set(self.stances.keys())
        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())
        if len(filter_b) == 0:
            filter_b = set(self.stances.keys())

        ret = []


        for node in filter_a & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }

            if node not in graph:
                continue

            for node_ in filter_b & self.instances[instance_id] & set(graph[node]):
                InInstance_ = {
                        "name": "InInstance",
                        "arguments": [node_, instance_id],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                Graph = {
                        "name": graph_name,
                        "arguments": [node, node_],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                if istrain:
                    if isneg and self.stances[node] != self.stances[node_]:
                        HasSame = {
                                "name": "SameLabel",
                                "arguments": [node, node_],
                                "ttype": None,
                                "obs": False,
                                "isneg": True,
                                "target_pos": None,
                                }
                        body = [InInstance, InInstance_, Graph]
                        head = HasSame
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                    if not isneg and self.stances[node] == self.stances[node_]:
                        HasSame = {
                                "name": "SameLabel",
                                "arguments": [node, node_],
                                "ttype": None,
                                "obs": False,
                                "isneg": False,
                                "target_pos": None,
                                }
                        body = [InInstance, InInstance_, Graph]
                        head = HasSame
                        rg = RuleGrounding(body, head, None, True, head['arguments'][0], True)
                        rg.build_predicate_index()
                        rg.build_body_predicate_dic()
                        ret.append(rg)
                else:
                    HasSame = {
                            "name": "SameLabel",
                            "arguments": [node, node_],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None,
                            }

                    istruth = (self.stances[node] == self.stances[node_])
                    body = [InInstance, InInstance_, Graph]
                    head = HasSame
                    rg = RuleGrounding(body, head, None, True, head['arguments'][0], istruth)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
        return ret

    def agreement_constr_helper(self, filters, instance_id, graph_name):
        if graph_name == "Friends":
            graph = self.friends
        elif graph_name == "Argues":
            graph = self.argues
        elif graph_name == "Votes":
            graph = self.votes
        else:
            exit(-1)

        if instance_id is None:
            instance_id = "0"
        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            elif fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])
        filter_a = set(filter_a) & set(self.stances.keys())
        filter_b = set(filter_b) & set(self.stances.keys())
        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())
        if len(filter_b) == 0:
            filter_b = set(self.stances.keys())

        ret = []
        for node in filter_a & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }
            if node not in graph:
                continue

            for node_ in filter_b & self.instances[instance_id] & set(graph[node]):
                InInstance_ = {
                        "name": "InInstance",
                        "arguments": [node_, instance_id],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                Graph = {
                        "name": graph_name,
                        "arguments": [node, node_],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }

                for stance_ in ['Pro', 'Con']:
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [node_, stance_],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    HasSame = {
                            "name": "SameLabel",
                            "arguments": [node, node_],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None,
                            }
                    HasLabel = {"name": "HasLabel",
                               "arguments": [node, stance_],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }

                    body = [InInstance, InInstance_, Graph, HasLabel_, HasSame]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, None, False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
        return ret

    def disagreement_constr_helper(self, filters, instance_id, graph_name):
        if graph_name == "Friends":
            graph = self.friends
        elif graph_name == "Argues":
            graph = self.argues
        elif graph_name == "Votes":
            graph = self.votes
        else:
            exit(-1)

        if instance_id is None:
            instance_id = "0"
        filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            elif fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])
        filter_a = set(filter_a) & set(self.stances.keys())
        filter_b = set(filter_b) & set(self.stances.keys())
        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())
        if len(filter_b) == 0:
            filter_b = set(self.stances.keys())

        ret = []
        for node in filter_a & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }
            if node not in graph:
                continue

            for node_ in filter_b & self.instances[instance_id] & set(graph[node]):
                InInstance_ = {
                        "name": "InInstance",
                        "arguments": [node_, instance_id],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                Graph = {
                        "name": graph_name,
                        "arguments": [node, node_],
                        "ttype": None,
                        "obs": True,
                        "isneg": False,
                        "target_pos": None
                        }
                for stance_ in ['Pro', 'Con']:
                    HasLabel_ = {"name": "HasLabel",
                               "arguments": [node_, stance_],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": False,
                               "target_pos": 1
                               }
                    HasSame = {
                            "name": "SameLabel",
                            "arguments": [node, node_],
                            "ttype": None,
                            "obs": False,
                            "isneg": True,
                            "target_pos": None,
                            }
                    HasLabel = {"name": "HasLabel",
                               "arguments": [node, stance_],
                               "ttype": LabelType.Multiclass,
                               "obs": False,
                               "isneg": True,
                               "target_pos": 1
                               }

                    body = [InInstance, InInstance_, Graph, HasLabel_, HasSame]
                    head = HasLabel
                    rg = RuleGrounding(body, head, None, False, None, False)
                    rg.build_predicate_index()
                    rg.build_body_predicate_dic()
                    ret.append(rg)
                    #print rg
        return ret

    def multiclass_constr(self, filters, instance_id):
        if instance_id is None:
            instance_id = "0"
        filter_c = [];
        for fil in filters:
            if fil[0] == 'C':
                filter_c += map(str, self.filters[fil[1]])
        filter_c = set(filter_c) & set(self.stances.keys())
        if len(filter_c) == 0:
            filter_c = set(self.stances.keys())


        ret = []
        for node in filter_c & self.instances[instance_id]:
            InInstance = {
                    "name": "InInstance",
                    "arguments": [node, instance_id],
                    "ttype": None,
                    "obs": True,
                    "isneg": False,
                    "target_pos": None
                    }
            for stance in ['Pro', 'Con']:
                HasLabel = {"name": "HasLabel",
                           "arguments": [node, stance],
                           "ttype": LabelType.Multiclass,
                           "obs": False,
                           "isneg": False,
                           "target_pos": 1
                           }
                body = [InInstance]
                head = HasLabel
                rg = RuleGrounding(body, head, None, False, None, False)
                rg.build_predicate_index()
                rg.build_body_predicate_dic()
                ret.append(rg)
        return ret

    def get_gold_predicates(self, *args):
        instance_id = args[3]
        ret = []
        filters = args[1]; filter_a = []; filter_b = []
        for fil in filters:
            if fil[0] == 'A':
                filter_a += map(str, self.filters[fil[1]])
            elif fil[0] == 'B':
                filter_b += map(str, self.filters[fil[1]])

        filter_a = set(filter_a) & set(self.stances.keys())
        filter_b = set(filter_b) & set(self.stances.keys())

        if len(filter_a) == 0:
            filter_a = set(self.stances.keys())
        if len(filter_b) == 0:
            filter_b = set(self.stances.keys())

        for node in filter_a & self.instances[instance_id]:
            HasLabel = {
                    "name": "HasLabel",
                    "arguments": [node, self.stances[node]],
                    "ttype": LabelType.Multiclass,
                    "obs": False,
                    "isneg": False,
                    "target_pos": 1
                    }
            ret.append(HasLabel)

        # WARNING: Hard coded for friends
        for node in filter_a & self.instances[instance_id]:
            if node not in self.friends:
                continue
            for node_ in filter_b & self.instances[instance_id] & set(self.friends[node]):
                if self.stances[node] == self.stances[node_]:
                    SameLabel = {
                            "name": "SameLabel",
                            "arguments": [node, node_],
                            "ttype": None,
                            "obs": False,
                            "isneg": False,
                            "target_pos": None
                            }
                    ret.append(SameLabel)
        return ret

    def seed(self, filters, instance_id=None):
        if instance_id is None:
            instance_id = "0"
        filter_c = []
        for fil in filters:
            if fil[0] == 'C':
                filter_c += map(str, self.filters[fil[1]])

        filter_c = set(filter_c) & set(self.stances.keys())

        if len(filter_c) == 0:
            filter_c = set([])

        ret = []
        for node in filter_c & self.instances[instance_id]:
            InInstance = {"name": "InInstance",
                       "arguments": [node, instance_id],
                       "ttype": None,
                       "obs": True,
                       "isneg": False,
                       "target_pos": None
                       }
            HasLabel = {"name": "HasLabel",
                       "arguments": [node, self.stances[node]],
                       "ttype": None,
                       "obs": True,
                       "isneg": False,
                       "target_pos": 1
                       }
            body = [InInstance, HasLabel]
            head = HasLabel
            rg = RuleGrounding(body, head, None, False, None, True)
            rg.build_predicate_index()
            rg.build_body_predicate_dic()
            ret.append(rg)
        #print "Seeded:", len(ret)
        return ret

    def agreement_friends(self, filters, instance_id=None):
        return self.agreement_constr_helper(filters, instance_id, "Friends")

    def disagreement_friends(self, filters, instance_id=None):
        return self.disagreement_constr_helper(filters, instance_id, "Friends")

    def get_ruleset_instances(self, *args):
        is_global_train = args[3]
        if not is_global_train:
            return ["0"]
        else:
            return set(self.instances.keys()) - set(["0"])

    def get_distinct_values(self, *args):
        return ["Pro", "Con"]
