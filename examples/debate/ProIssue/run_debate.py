import sys
import os
import numpy as np
import optparse
import cProfile as profile
import random
import torch
from sklearn.metrics import *

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner

def get_ids(papers_file):
    papers = []
    with open(papers_file) as f:
        for line in f:
            papers.append(line.strip())
    random.shuffle(papers)
    return papers

def main():
    np.random.seed(1234)
    random.seed(1234)
    torch.cuda.set_device(0)

    parser = optparse.OptionParser()
    parser.add_option('-d', '--dir', help='directory', dest='dir', type='string')
    parser.add_option('-r', '--rule', help='rule file', dest='rules', type='string',
                       default='rule.dr')
    parser.add_option('-c', '--config', help='config file', dest='config', type='string',
                       default="config.json")
    parser.add_option('-m', help='mode: [global|local]', dest='mode', type='string',
                      default='local')
    parser.add_option('-i', help='issue', dest='issue', type='string')
    parser.add_option('--delta', help='loss augmented inferece', dest='delta', action='store_true',
                      default=False)
    parser.add_option('--lr', help='global learning rate', dest='global_lr', type='float')
    (opts, args) = parser.parse_args()

    mandatories = ['dir', 'issue']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    if opts.mode == "global" and not opts.__dict__['global_lr']:
        print "specify learning rate for global model\n"
        parser.print_help()
        exit(-1)

    if opts.mode not in ['global', 'local']:
        print "specify a valid mode\n"
        parser.print_help()
        exit(-1)

    rule_file = os.path.join(opts.dir, opts.rules)
    conf_file = os.path.join(opts.dir, opts.config)
    doc2vec_file = os.path.join(opts.dir, "doc2vec_debate.bin")
    deepWalk_friends_file = os.path.join(opts.dir, "deepwalk_friends.bin")
    deepWalk_argues_file = os.path.join(opts.dir, "deepwalk_argues.bin")
    deepWalk_votes_file = os.path.join(opts.dir, "deepwalk_votes.bin")

    if opts.mode == "global":
        learner=GlobalLearner(learning_rate=opts.global_lr)
    else:
        learner=LocalLearner()

    learner.compile_rules(rule_file)
    db=learner.create_dataset(opts.dir, dbmodule_path=opts.dir)

    db_funcs = [rule.dbfunc for rule in learner.ruleset['rule']]
    db.load_stances(opts.issue, K=2, db_funcs=db_funcs)

    '''
    for i in db.instances:
        print i, db.instances[i]
    exit()
    '''

    macro_f1_scores = []
    for i in range(5):
        print "PROCESSING FOLD {0}...".format(i)

        train_papers = get_ids("{0}/folds/train{1}".format(opts.dir, i))
        test_papers = get_ids("{0}/folds/test{1}".format(opts.dir, i))

        dev_size = int(len(test_papers)*0.2)
        dev_papers = test_papers[:dev_size]
        test_papers = test_papers[dev_size:]

        print len(train_papers), len(dev_papers), len(test_papers)

        db.add_filters(filters=[
            ("Paper", "isTrain", "paperId", train_papers),
            ("Paper", "isDev", "paperId", dev_papers),
            ("Paper", "isTest", "paperId", test_papers),
            ("Paper", "isTrainDev", "paperId", train_papers + dev_papers),
            ("Paper", "allFolds", "paperId", train_papers + dev_papers + test_papers)
            ])

        print "FEATURE EXTRACTOR"
        learner.build_feature_extractors(db,
                                         doc2vec_filename=doc2vec_file,
                                         deepWalk_friends_filename=deepWalk_friends_file,
                                         deepWalk_argues_filename=deepWalk_argues_file,
                                         deepWalk_votes_filename=deepWalk_votes_file,
                                         femodule_path=opts.dir)
        if opts.mode == "global":
            # do we want to add a subset of the train for seeding in the global model?
            learner.build_models(db, conf_file)
            learner.extract_data(db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("C", "isTrain", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "allFolds", 1), ("C", "isTrainDev", 1)])
            # filters on training procedure are for hot start (when passed)
            res = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "allFolds", 1)],
                    opt_predicate='HasLabel',
                    hot_start=False,
                    loss_augmented_inference=opts.delta)

        else:
            learner.build_models(db, conf_file)
            _ = learner.train(
                    db,
                    train_filters=[("A", "isTrain", 1), ("B", "isTrain", 1)],
                    dev_filters=[("A", "isDev", 1), ("B", "isDev", 1)],
                    test_filters=[("A", "isTest", 1), ("B", "allFolds", 1)]
                    )
            learner.extract_data(db, test_filters=[("A", "isTest", 1),
                ("B", "allFolds", 1), ("C", "isTrainDev", 1)])
            res = learner.predict(
                    db, fold='test')


        if 'SameLabel' in res.metrics:
            y_gold_agr = res.metrics['SameLabel']['gold_data']
            y_pred_agr = res.metrics['SameLabel']['pred_data']

            print "Agreement (Same/NotSame)"
            print classification_report(y_gold_agr, y_pred_agr)
            f1_test_macro = f1_score(y_gold_agr, y_pred_agr, average='macro')
            print "TEST F1 macro", f1_test_macro
            print

        if 'HasLabel' in res.metrics:
            y_gold = res.metrics['HasLabel']['gold_data']
            y_pred = res.metrics['HasLabel']['pred_data']

            print "Stance (Pro/Con)"
            print classification_report(y_gold, y_pred)
            f1_test_macro = f1_score(y_gold, y_pred, average='macro')
            print "TEST F1 macro", f1_test_macro

        learner.reset_metrics()
        return
    print macro_f1_scores


if __name__ == "__main__":
    main()
