from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

import numpy as np
import json


class Debate_ft(FeatureExtractor):

    def __init__(self, bert_encoding_f, max_seq_title, max_seq_post,
                 sentemb_encoding_f, sentemb_embedding_f, title_w2v_f,
                 summaries_f, user_info_f, issues_con_f, issues_pro_f,
                 use_bert=True, use_sentemb=True, use_user=True):

        super(Debate_ft, self).__init__()

        # BERT
        self.bert_encoding_f = bert_encoding_f
        self.max_seq_title = max_seq_title
        self.max_seq_post = max_seq_post
        self.CLS = 101
        self.SEP = 102
        self.use_bert = use_bert

        # Sentence embeddings
        self.sentemb_encoding_f = sentemb_encoding_f
        self.sentemb_embedding_f = sentemb_embedding_f
        self.title_w2v_f = title_w2v_f
        self.use_sentemb = use_sentemb
        np.random.seed(17477)

        # User information
        self.summaries_f = summaries_f
        self.user_info_f = user_info_f
        self.issues_pro_f = issues_pro_f
        self.issues_con_f = issues_con_f
        self.use_user = use_user

    def load_issues(self, filenamecon, filenamepro, include_leaning=True):
        with open(filenamepro) as f:
            pro = json.load(f)
        with open(filenamecon) as f:
            con = json.load(f)

        stances = {}
        self.issues = pro.keys()

        for idx, issue in enumerate(self.issues):
            if not include_leaning and issue in ['Abortion', 'Gay Marriage', 'Drug Legalization',
                                                 'Environmental Protection', 'Global Warming Exists',
                                                 'Medical Marijuana', 'National Health Care',
                                                 'Death Penalty', 'Gun Rights', 'War on Terror',
                                                 'War in Afghanistan', 'Border Fence']:
                continue
            for user in pro[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['pro'][idx] = 1.0
            for user in con[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['con'][idx] = 1.0
        return stances


    def build(self):
        self.stance_idx = {'con': 0, 'pro': 1}

        if self.use_bert:
            self.bert_encoding = np.load(self.bert_encoding_f)
            #self.bert_encoding = np.array(self.bert_encoding)
            print("Bert encodings", self.bert_encoding.shape)

        if self.use_sentemb:
            self.sentemb_encoding = np.load(self.sentemb_encoding_f)
            self.sentemb_embedding = np.load(self.sentemb_embedding_f)
            self.title_w2v = json.load(open(self.title_w2v_f))

        if self.use_user:
            self.sections = ['Activities', 'TV Shows', 'High School', 'Websites', 'Sports Teams', 'Quotes', 'Movies', 'Books', 'Music', 'College', 'About Me', 'Beliefs']
            self.summary_vects = json.load(open(self.summaries_f))
            self.user_info = json.load(open(self.user_info_f))
            self.stances = self.load_issues(self.issues_con_f, self.issues_pro_f)

    # The convention in BERT is:
    # (a) For sequence pairs:
    #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
    #  type_ids:   0   0  0    0    0     0       0   0   1  1  1  1   1   1
    # (b) For single sequences:
    #  tokens:   [CLS] the dog is hairy . [SEP]
    #  type_ids:   0   0   0   0  0     0   0
    #
    # Where "type_ids" are used to indicate whether this is the first
    # sequence or the second sequence. The embedding vectors for `type=0` and
    # `type=1` were learned during pre-training and are added to the wordpiece
    # embedding vector (and position vector). This is not *strictly* necessary
    # since the [SEP] token unambiguously separates the sequences, but it makes
    # it easier for the model to learn the concept of sequences.
    #
    # For classification tasks, the first vector (corresponding to [CLS]) is
    # used as as the "sentence vector". Note that this only makes sense because
    # the entire model is fine-tuned.
    def pad_and_mask(self, vector, max_seq_len):
        pad_token = 0
        mask = [1] * len(vector)
        if len(vector) < max_seq_len:
            vector += [pad_token] * (max_seq_len - len(vector))
            mask += [0] * (max_seq_len - len(mask))
        return (vector, mask)

    def bert_post(self, rule_grd, pred_pos=0):
        writes_post = rule_grd.get_body_predicates("WritesPost")[pred_pos]
        post_id = writes_post['arguments'][1]
        post_vec = self.bert_encoding[post_id][:self.max_seq_post]

        post_vec = [self.CLS] + post_vec
        post_vec = post_vec + [self.SEP]

        (post_vec, post_mask) = self.pad_and_mask(post_vec, self.max_seq_post + 2)
        return (post_vec, post_mask)

    def bert_second_post(self, rule_grd):
        return self.bert_post(rule_grd, pred_pos=1)

    def bert_title(self, rule_grd):
        has_title = rule_grd.get_body_predicates("HasTitle")[0]
        title_id = has_title['arguments'][1]
        title_vec = self.bert_encoding[title_id][:self.max_seq_title]
        title_vec = title_vec + [self.SEP]

        (title_vec, title_mask) = self.pad_and_mask(title_vec, self.max_seq_title + 1)
        return (title_vec, title_mask)

    def sentemb_post(self, rule_grd):
        writes_post = rule_grd.get_body_predicates("WritesPost")[0]
        post_id = writes_post['arguments'][1]
        debate_name = writes_post['arguments'][0]

        sentences = self.sentemb_encoding[post_id][:25]
        ret = []

        #if len(sentences) == 0:
        #    print(debate_name, post_id)
        
        for sent in sentences:
            ret.append(self.sentemb_embedding[sent])
        if len(ret) == 0:
            return [np.random.uniform(-0.0025, 0.0025, 300)]
        return ret

    def sentemb_w2v_title(self, rule_grd):
        has_title = rule_grd.get_body_predicates("HasTitle")[0]
        debate_name = has_title['arguments'][0]

        # Average word embedding
        title_vec_w2v = np.array(self.title_w2v[debate_name])

        # Sentence embedding
        title_id = has_title['arguments'][1]

        title_encoding = self.sentemb_encoding[title_id][0]

        if title_encoding is not None:
            title_vec_sentemb = self.sentemb_embedding[title_encoding]
        else:
            title_vec_sentemb = np.random.uniform(-0.0025, 0.0025, 300)

        return np.concatenate([title_vec_w2v, title_vec_sentemb])

    def user_bigissues_1hot(self, author):
        if author in self.stances:
            ret = self.stances[author]['pro'] + self.stances[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def user_issues_and_information(self, rule_grd, user_predicate, user_index):
        #print rule_grd
        pred = rule_grd.get_body_predicates(user_predicate)[user_index]
        author = pred['arguments'][0]
        ret = self.user_bigissues_1hot(author)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if author in self.user_info and feat in self.user_info[author]:
                index = self.user_info['features'][feat].index(self.user_info[author][feat])
                temp[index] = 1
            ret += temp
        return ret

    def summary_vectors(self, rule_grd, user_predicate, user_index):
        ret = []
        pred = rule_grd.get_body_predicates(user_predicate)[user_index]
        author = pred['arguments'][0]

        for sect in self.sections:
            if author in self.summary_vects and sect in self.summary_vects[author]:
                ret += self.summary_vects[author][sect]
            else:
                ret += map(float, [0]*50)
        return ret

    def user_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd, "ParticipatesIn", 0) + \
               self.summary_vectors(rule_grd, "ParticipatesIn", 0)

    def voter_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd, "Votes", 0) + \
               self.summary_vectors(rule_grd, "Votes", 0)

    def other_voter_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd, "Votes", 1) + \
               self.summary_vectors(rule_grd, "Votes", 1)

    def user_voter_features(self, rule_grd):
        return self.user_features(rule_grd) + self.voter_features(rule_grd)

    def voter_voter_features(self, rule_grd):
        return self.voter_features(rule_grd) + self.other_voter_features(rule_grd)

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] in set(["HasLabel", "HasStance", "VoterStance"]):
            label = head['arguments'][-1]
            return self.stance_idx[label]
