# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
from sklearn.metrics import *
import issues

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--lrate", help="learning rate", type=float, default=0.01)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument("--folds", help="folds to use [random|hard]", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument("--bert", action="store_true", default=False)
    parser.add_argument("--sentemb", action="store_true", default=False)
    parser.add_argument("--user", action="store_true", default=False)
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument('--embed_only', help='run embedding extraction only', default=False, action='store_true')
    parser.add_argument('-m', help='mode: [global|local|joint]', dest='mode', type=str, default='local')
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true', default=False)

    args = parser.parse_args()
    return args

def train(folds, avoid):
    ret = []
    for j in folds:
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    # Fixed resources
    BERT_ENCODING_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/bert_encode.npy"
    MODULES_PATH = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/"
    TITLE_W2V = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/word2vec_title.json"
    SUMMARIES_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/summary_doc2vec.json"
    ISSUES_PRO_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/issues_pro.json"
    ISSUES_CON_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/issues_con.json"
    USER_INFO_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/user_info.json"

    args = parse_arguments()

    if args.bert:
        optimizer = "AdamW"
    else:
        optimizer = "SGD"

    # Select what gpu to use
    use_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.mode == 'global':
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=use_gpu, gpu_index=args.gpu_index)
    else:
        learner=LocalLearner()

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    if args.folds == "hard":
        debate_folds = json.load(open(os.path.join(args.dir, "debate_folds_hard.json")))
        sentemb_encoding = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/sentemb_encode_hard.npy"
        sentemb_embedding = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/sentence_embeddings_hard.npy"
    elif args.folds == "random":
        debate_folds = json.load(open(os.path.join(args.dir, "debate_folds_rndm.json")))
        sentemb_encoding = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/sentemb_encode_rndm.npy"
        sentemb_embedding = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/sentence_embeddings_rndm.npy"
    else:
        print("ERROR: specify a valid fold")
        exit(-1)


    embeddings_all = {}

    for i in range(0, 5):
        print "Fold", i
        dev_fold = (i + 1) % 5

        test_debates = debate_folds[str(i)]
        dev_debates = debate_folds[str(dev_fold)]
        train_debates = train(debate_folds, map(str, [i, dev_fold]))

        if args.debug:
            train_debates = train_debates[:10]
            dev_debates = dev_debates[:10]
            test_debates = test_debates[:10]
        print "Debate folds:", len(train_debates), len(dev_debates), len(test_debates)


        db.add_filters(filters=[
            ("HasTitle", "isTrain", "debateId_1", train_debates),
            ("HasTitle", "isDev", "debateId_1", dev_debates),
            ("HasTitle", "isTest", "debateId_1", test_debates),
            ("HasTitle", "isDummy", "debateId_1", train_debates[:10])
        ])

        learner.build_feature_extractors(db,
                                         filters=[("HasTitle", "isDummy", 1)],
                                         bert_encoding_f=BERT_ENCODING_F,
                                         max_seq_title=20,
                                         max_seq_post=100,
                                         sentemb_encoding_f=sentemb_encoding,
                                         sentemb_embedding_f=sentemb_embedding,
                                         title_w2v_f=TITLE_W2V,
                                         summaries_f=SUMMARIES_F,
                                         user_info_f=USER_INFO_F,
                                         issues_con_f=ISSUES_CON_F,
                                         issues_pro_f=ISSUES_PRO_F,
                                         use_bert=args.bert,
                                         use_sentemb=args.sentemb,
                                         use_user=args.user,
                                         femodule_path=MODULES_PATH)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=MODULES_PATH)

        if args.mode == "local":
            if args.continue_from_checkpoint:
                learner.init_models()

            if not args.infer_only and not args.embed_only:
                    learner.train(db,
                              train_filters=[("HasTitle", "isTrain", 1)],
                              dev_filters=[("HasTitle", "isDev", 1)],
                              test_filters=[("HasTitle", "isTest", 1)],
                              scale_data=False,
                              optimizer=optimizer)
            if not args.train_only and not args.embed_only:
                learner.extract_data(db, extract_test=True, test_filters=[("HasTitle", "isTest", 1)])
                del learner.fe
                res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)

            if not args.train_only and not args.infer_only:
                learner.extract_data(db, extract_test=True, test_filters=[("HasTitle", "isTest", 1)])
                del learner.fe
                embeddings = learner.extract_rule_embeddings(None, fold='test')
                for body in embeddings:
                    embeddings_all[body] = {'title': map(float, list(embeddings[body][0])), 'user': map(float, list(embeddings[body][1]))}

        elif args.mode == "global" and not args.infer_only and not args.embed_only:
            learner.extract_data(
                    db,
                    train_filters=[("HasTitle", "isTrain", 1)],
                    dev_filters=[("HasTitle", "isDev", 1)],
                    test_filters=[("HasTitle", "isTest", 1)],
                    extract_train=not args.infer_only,
                    extract_dev=not args.infer_only,
                    extract_test=True)
            res, heads = learner.train(
                    db,
                    train_filters=[("HasTitle", "isTrain", 1)],
                    dev_filters=[("HasTitle", "isDev", 1)],
                    test_filters=[("HasTitle", "isTest", 1)],
                    opt_predicate='HasLabel',
                    loss_augmented_inference=args.delta,
                    continue_from_checkpoint=args.continue_from_checkpoint,
                    inference_only=args.infer_only,
                    hot_start=False,
                    scale_data=False,
                    weight_classes=True,
                    patience=2,
                    optimizer=optimizer)

            '''
            for head in heads:
                print head
            exit()
            '''
        if not args.train_only and not args.embed_only:
            if 'HasLabel' in res.metrics:
                y_gold = res.metrics['HasLabel']['gold_data']
                y_pred = res.metrics['HasLabel']['pred_data']

                print "HasLabel (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score

            if 'HasStance' in res.metrics:
                y_gold = res.metrics['HasStance']['gold_data']
                y_pred = res.metrics['HasStance']['pred_data']

                print "HasStance (Pro/Con)"
                print classification_report(y_gold, y_pred)
                acc_score = accuracy_score(y_gold, y_pred)

                print "TEST Acc", acc_score

    with open("resulting_embeddings.json", "w") as fp:
        json.dump(embeddings_all, fp)


if __name__ == "__main__":
    main()
