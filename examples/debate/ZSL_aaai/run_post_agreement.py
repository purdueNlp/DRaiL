# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
from sklearn.metrics import *
import issues

from drail.model.argument import ArgumentType
from drail.learn.local_learner import LocalLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument("--folds", help="folds to use [random|hard]", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')

    args = parser.parse_args()
    return args

def main():
    random.seed(1234)

    # Fixed resources
    BERT_ENCODING_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/data/bert_encode.npy"
    MODULES_PATH = "/scratch/pachecog/DRaiL/examples/debate/ZSL_aaai/"

    args = parse_arguments()
    optimizer = "AdamW"

    # Select what gpu to use
    use_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    learner=LocalLearner()
    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    relevant_debates = json.load(open(args.folds))['abortion']
    random.shuffle(relevant_debates)

    n = int(len(relevant_debates) * 0.8)
    train_debates = relevant_debates[:n]
    dev_debates = relevant_debates[n:]

    if args.debug:
        train_debates = train_debates[:20]
        dev_debates = dev_debates[:20]

    print(len(train_debates), len(dev_debates))

    db.add_filters(filters=[
        ("HasTitle", "isTrain", "debateId_1", train_debates),
        ("HasTitle", "isDev", "debateId_1", dev_debates),
        ("HasTitle", "isTest", "debateId_1", dev_debates),
        ("HasTitle", "isDummy", "debateId_1", train_debates[:10])
    ])


    learner.build_feature_extractors(db,
                                     filters=[("HasTitle", "isDummy", 1)],
                                     bert_encoding_f=BERT_ENCODING_F,
                                     max_seq_title=20,
                                     max_seq_post=100,
                                     sentemb_encoding_f=None,
                                     sentemb_embedding_f=None,
                                     title_w2v_f=None,
                                     summaries_f=None,
                                     user_info_f=None,
                                     issues_con_f=None,
                                     issues_pro_f=None,
                                     use_bert=True,
                                     use_sentemb=False,
                                     use_user=False,
                                     femodule_path=MODULES_PATH)
    learner.set_savedir(args.savedir)
    learner.build_models(db, args.config, netmodules_path=MODULES_PATH)

    if args.continue_from_checkpoint:
        learner.init_models()

    if not args.infer_only:
        learner.train(db,
                  train_filters=[("HasTitle", "isTrain", 1)],
                  dev_filters=[("HasTitle", "isDev", 1)],
                  test_filters=[("HasTitle", "isTest", 1)],
                  scale_data=False,
                  optimizer=optimizer)

    if not args.train_only:
        learner.extract_data(db, extract_test=True, test_filters=[("HasTitle", "isTest", 1)])
        del learner.fe
        res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)

        if 'Debates' in res.metrics:
            y_gold = res.metrics['HasLabel']['gold_data']
            y_pred = res.metrics['HasLabel']['pred_data']

            print "Debates (Pro/Con)"
            print classification_report(y_gold, y_pred)
            acc_score = accuracy_score(y_gold, y_pred)

            print "TEST Acc", acc_score

if __name__ == "__main__":
    main()
