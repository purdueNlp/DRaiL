# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
import logging.config

from sklearn.metrics import *
from pandas_ml import ConfusionMatrix

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--logging_config', help='logging configuration file', type=str, default=None)
    parser.add_argument('--start', help='start fold index', dest='start_fold_index', type=int, default=0)
    parser.add_argument('--end', help='end fold index', dest='end_fold_index', type=int, default=10)
    parser.add_argument('--drop_scores', help='drop scores', action='store_true', default=False)
    args = parser.parse_args()
    return args

def main():
    # seed
    args = parse_arguments()
    random.seed(4321)

    # FIXED RESOURCES

    # Paths to neural networks and feature classes
    FE_PATH = "examples/debate/ZSL_political/feat"
    NE_PATH = "examples/debate/ZSL_political/neuro"

    # Files needed for feature extraction
    BERT_STATEMENTS = "examples/debate/ZSL_political/data/politician_statements.json"
    optimizer = "AdamW"

    # Select whaIXED RESOURCESuse_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    learner=LocalLearner(inference_limit=10)

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    acc_all = {}; f1_all = {}
    y_pred_all = {}; y_gold_all = {}

    for i in range(args.start_fold_index, args.end_fold_index):
        logger.info("Fold {}".format(i))

        learner.build_feature_extractors(db, bert_ids=BERT_STATEMENTS, femodule_path=FE_PATH)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=NE_PATH)

        if args.continue_from_checkpoint:
            learner.init_models()

        learner.extract_data(db, extract_test=True)
        del learner.fe
        res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
        if args.drop_scores:
            learner.drop_scores(None, fold='test', output="politician_scores_{}.csv".format(i), heads=heads)

        if 'HasIdeology' in res.metrics:
            y_gold = res.metrics['HasIdeology']['gold_data']
            y_pred = res.metrics['HasIdeology']['pred_data']

            logger.info("HasIdeology")
            logger.info(classification_report(y_gold, y_pred))
            acc_score = accuracy_score(y_gold, y_pred)
            f1 = f1_score(y_gold, y_pred, average='macro')
            logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

            if 'HasIdeology' not in acc_all:
                acc_all['HasIdeology'] = []
                f1_all['HasIdeology'] = []
                y_pred_all['HasIdeology'] = []
                y_gold_all['HasIdeology'] = []
            y_pred_all['HasIdeology'] += y_pred
            y_gold_all['HasIdeology'] += y_gold

    print("Accuracy", acc_all)
    print("Macro F1", f1_all)
    if 'HasIdeology' in y_gold_all:
        cm = ConfusionMatrix(y_gold_all['HasIdeology'], y_pred_all['HasIdeology'])
        print(cm)

if __name__ == "__main__":
    args = parse_arguments()
    if args.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(args.logging_config)))
    main()
