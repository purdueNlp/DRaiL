import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel

from drail.neuro.nn_model import NeuralNetworks

class BertClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)
        self.CLS = 101
        self.SEP = 102
        #self.bert_model_name = "bert-base-uncased"
        # testing smaller versions of bert

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = self.config["bert_model_type"]
        #bert_config = BertConfig.from_pretrained("bert-base-uncased")
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        # User embeddings
        #self.user_emebd = torch.nn.Embedding(9313, 100)

        # TO-DO: Share the feature layers
        #self.features2hidden = torch.nn.Linear(self.config["n_input_feats"] + 100, bert_config.hidden_size)
        #self.f_two2hidden = torch.nn.Linear(self.config["n_input_feats"] + 100, bert_config.hidden_size)
        if "shared_user" in self.config:
            name = self.config["shared_user"]
            self.features2hidden = shared_params[name]["layer"]
        else:
            self.features2hidden = torch.nn.Linear(self.config["n_input_feats"], bert_config.hidden_size)

        # Freeze layer
        if "freeze" in self.config and self.config["freeze"]:
            self.features2hidden.weight.requires_grad = False
            self.features2hidden.bias.requires_grad = False

        self.h2l = torch.nn.Linear(bert_config.hidden_size, self.output_dim)

        if self.use_gpu:
            #self.user_emebd = self.user_emebd.cuda()
            self.features2hidden = self.features2hidden.cuda()
            self.h2l = self.h2l.cuda()

    def forward(self, x):
        feats_one = self._get_variable(self._get_float_tensor(x['vector']))

        #feats_one = torch.cat([feats_one, ids_one], 1)
        feats_one = self.features2hidden(feats_one)
        feats_one = F.relu(feats_one)
        logits = self.h2l(feats_one)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

