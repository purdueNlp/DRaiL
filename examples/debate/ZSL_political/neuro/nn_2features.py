import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel

from drail.neuro.nn_model import NeuralNetworks

class BertClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)
        self.CLS = 101
        self.SEP = 102
        #self.bert_model_name = "bert-base-uncased"
        # testing smaller versions of bert

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = self.config["bert_model_type"]
        #bert_config = BertConfig.from_pretrained("bert-base-uncased")
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        # User embeddings
        #self.user_emebd = torch.nn.Embedding(9313, 100)

        # TO-DO: Share the feature layers
        #self.f_one2hidden = torch.nn.Linear(self.config["n_input_feats"] + 100, bert_config.hidden_size)
        #self.f_two2hidden = torch.nn.Linear(self.config["n_input_feats"] + 100, bert_config.hidden_size)
        if "shared_user" in self.config:
            name = self.config["shared_user"]
            self.f_one2hidden = shared_params[name]["layer"]
            self.f_two2hidden = shared_params[name]["layer"]
        else:
            self.f_one2hidden = torch.nn.Linear(self.config["n_input_feats"], bert_config.hidden_size)
            self.f_two2hidden = torch.nn.Linear(self.config["n_input_feats"], bert_config.hidden_size)
        self.concat2hidden = torch.nn.Linear(bert_config.hidden_size*2, self.config["n_hidden_reln"])
        self.hidden2label = torch.nn.Linear(self.config["n_hidden_reln"], self.output_dim)

        if self.use_gpu:
            #self.user_emebd = self.user_emebd.cuda()
            self.f_one2hidden = self.f_one2hidden.cuda()
            self.f_two2hidden = self.f_two2hidden.cuda()
            self.concat2hidden = self.concat2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        feats_one = [elem[0] for elem in x['input']]
        feats_one = self._get_variable(self._get_float_tensor(feats_one))
        feats_two = [elem[1] for elem in x['input']]
        feats_two = self._get_variable(self._get_float_tensor(feats_two))

        '''
        ids_one = [elem[2][0] for elem in x['input']]
        ids_one = self._get_variable(self._get_long_tensor(ids_one))
        ids_two = [elem[3][0] for elem in x['input']]
        ids_two = self._get_variable(self._get_long_tensor(ids_two))

        ids_one = self.user_emebd(ids_one)
        ids_two = self.user_emebd(ids_two)
        '''
        #feats_one = torch.cat([feats_one, ids_one], 1)
        feats_one = self.f_one2hidden(feats_one)
        feats_one = F.relu(feats_one)

        #feats_two = torch.cat([feats_two, ids_two], 1)
        feats_two = self.f_two2hidden(feats_two)
        feats_two = F.relu(feats_two)

        concat = torch.cat([feats_one, feats_two], 1)
        #print(concat.size())
        concat = self.concat2hidden(concat)
        concat = F.relu(concat)

        logits = self.hidden2label(concat)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

