import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel

from drail.neuro.nn_model import NeuralNetworks

class BertClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)
        self.CLS = 101
        self.SEP = 102
        #self.bert_model_name = "bert-base-uncased"
        # testing smaller versions of bert

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = self.config["bert_model_type"]
        #bert_config = BertConfig.from_pretrained("bert-base-uncased")
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        if "shared_bert" in self.config:
            name = self.config["shared_bert"]
            self.bert_model = shared_params[name]["bert"]
        else:
            #self.bert_model = BertModel.from_pretrained("bert-base-uncased")
            self.bert_model = AutoModel.from_pretrained(self.bert_model_name)
        #for name, W in self.bert_model.named_parameters():
        #    print(name)
        #print(bert_config)
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        # TO-DO: Share the feature layers
        self.f_one2hidden = torch.nn.Linear(self.config["n_input_feats"], bert_config.hidden_size)
        self.f_two2hidden = torch.nn.Linear(self.config["n_input_feats"], bert_config.hidden_size)
        self.concat2hidden = torch.nn.Linear(bert_config.hidden_size*3, self.config["n_hidden_reln"])
        self.hidden2label = torch.nn.Linear(self.config["n_hidden_reln"], self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.f_one2hidden = self.f_one2hidden.cuda()
            self.f_two2hidden = self.f_two2hidden.cuda()
            self.concat2hidden = self.concat2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []

        for elem in x['input']:
            input_ids.append(elem[0][0])
            input_mask.append(elem[0][1])
            segment_ids.append([0] * len(elem[0][0]))
        feats_one = [elem[1] for elem in x['input']]
        feats_one = self._get_variable(self._get_float_tensor(feats_one))
        feats_two = [elem[2] for elem in x['input']]
        feats_two = self._get_variable(self._get_float_tensor(feats_two))

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

        #print(input_ids.size(), input_mask.size(), segment_ids.size())

        # Run model
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)

        feats_one = self.f_one2hidden(feats_one)
        feats_one = F.relu(feats_one)

        feats_two = self.f_two2hidden(feats_two)
        feats_two = F.relu(feats_two)

        concat = torch.cat([pooled_output, feats_one, feats_two], 1)
        concat = self.concat2hidden(concat)
        concat = F.relu(concat)

        logits = self.hidden2label(concat)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

