from transformers import BertTokenizer, AutoTokenizer, AutoModel, AutoConfig
import re
import numpy as np
import json
import sys

debates_bert = {}
debates = json.load(open("data/debates_preprocessed_ALL.json"))

sentence_id = 0
bert_text = []

if sys.argv[1] == "google/bert_uncased_L-2_H-128_A-2":
    bert_model = "bert-tiny-uncased"
elif sys.argv[1] == "bert-base-uncased":
    bert_model = "bert-base-uncased"
elif sys.argv[1] == "google/bert_uncased_L-4_H-256_A-4":
    bert_model = "bert-mini-uncased"
elif sys.argv[1] == "google/bert_uncased_L-4_H-512_A-8":
    bert_model = "bert-small-uncased"
elif sys.argv[1] == "bert-base-uncased":
    bert_model = sys.argv[1]
else:
    print("specify a name to record")
    exit()

#tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)
tokenizer = AutoTokenizer.from_pretrained(sys.argv[1])

max_title = 0; max_sentence = 0

for issue in debates:
    debates_bert[issue] = {}

    for debate in debates[issue]:
        title = debates[issue][debate]['title']
        sentence_bert = tokenizer.encode(title)

        max_title = max(max_title, len(sentence_bert))

        bert_text.append(sentence_bert)
        title_id = sentence_id
        sentence_id += 1

        posts_curr = []
        for post in debates[issue][debate]['posts']:
            sentences_curr = []
            if post is not None:
                for sentence in post:
                    sentence_bert = tokenizer.encode(sentence)
                    if len(sentence_bert) > 452:
                        sentence_bert = sentence_bert[:452]
                    max_sentence = max(max_sentence, len(sentence_bert))
                    bert_text.append(sentence_bert)
                    sentences_curr.append(sentence_id)
                    sentence_id += 1
            posts_curr.append(sentences_curr)

        debates_bert[issue][debate] = {
            'title': title_id,
            'posts': posts_curr,
            'stances': debates[issue][debate]['stances'],
            'authors': debates[issue][debate]['authors']
        }


bert_text = np.array(bert_text, dtype=object)
print(sentence_id, bert_text.shape)
print("max_title", max_title, "max_sentence", max_sentence)

with open("data/debate_ids_{}.json".format(bert_model), "w") as fp:
    json.dump(debates_bert, fp)

np.save("data/bert_encode_{}.npy".format(bert_model), bert_text)
