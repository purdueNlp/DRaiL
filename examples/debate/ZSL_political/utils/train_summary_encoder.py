import json
import torch
from transformers import AutoTokenizer, AutoModel, AutoConfig, BertForMaskedLM, AdamW, get_linear_schedule_with_warmup
import random
import numpy as np
from torch.utils.data import DataLoader, TensorDataset
import sys
random.seed(4321)

if sys.argv[1] == "google/bert_uncased_L-2_H-128_A-2":
    bert_model = "bert-tiny-uncased"
elif sys.argv[1] == "bert-base-uncased":
    bert_model = "bert-base-uncased"
elif sys.argv[1] == "google/bert_uncased_L-4_H-256_A-4":
    bert_model = "bert-mini-uncased"
elif sys.argv[1] == "google/bert_uncased_L-4_H-512_A-8":
    bert_model = "bert-small-uncased"
else:
    print("specify a name to record")
    exit()

sections_to_ignore = set(["AIM", "Skype", "Google Talk", "Windows Live", "Yahoo", "Gadu-Gadu", "ICQ", "Websites"])
tokenizer = AutoTokenizer.from_pretrained(sys.argv[1])
mask_rate = 0.3
max_seq_size = 256

bert_summaries = {}

with open("data/user_summary.json") as fp:
    summaries = json.load(fp)

    max_example = 0
    example_lens = []; examples = []; labels = []; segments = []; attention_masks = []
    for user in summaries:
        bert_summaries[user] = []
        sections_text = "[CLS]"

        section_tokens = {}; section_masked_tokens = {}
        for section in summaries[user]:
            if section in sections_to_ignore:
                continue
            curr_text = summaries[user][section]
            tokenized_text = tokenizer.tokenize(curr_text)
            section_tokens[section] = tokenized_text

            bert_summaries[user] += tokenizer.convert_tokens_to_ids(tokenized_text + ["[SEP]"])
            #print(tokenized_text)
            tokenized_text_masked = tokenized_text[:]
            for i in range(0, len(tokenized_text)):
                coin = random.uniform(0, 1)
                if coin <= mask_rate:
                    tokenized_text_masked[i] = '[MASK]'
            section_masked_tokens[section] = tokenized_text_masked

        for section_1 in section_tokens:
            section_tokenized_1 = tokenizer.tokenize(section_1 + ":")
            #print(section_tokenized)

            for section_2 in section_tokens:
                if section_1 == section_2:
                    continue
                example = ["[CLS]"]; label = ["[CLS]"]; segment_ids = []; mask_ids = []
                section_tokenized_2 = tokenizer.tokenize(section_2 + ":")

                example += section_tokenized_1 + section_tokens[section_1] + ["[SEP]"]
                example_len = min(len(example), max_seq_size)
                segment_ids += [0] * example_len

                example += section_tokenized_2 + section_masked_tokens[section_2] + ["[SEP]"]
                example_len = min(len(example), max_seq_size)
                mask_ids = [1] * example_len + [0] * (max_seq_size - example_len)

                label += section_tokenized_1 + section_tokens[section_1] + ["[SEP]"]
                label += section_tokenized_2 + section_tokens[section_2] + ["[SEP]"]

                # Convert token to vocabulary indices
                example = tokenizer.convert_tokens_to_ids(example)[:max_seq_size] + [0] * (max_seq_size - len(example))
                label = tokenizer.convert_tokens_to_ids(label)[:max_seq_size] + [0] * (max_seq_size - len(label))

                segment_ids += [1] * (max_seq_size - len(segment_ids))

                example_lens.append(len(example))
                examples.append(example)
                labels.append(label)
                segments.append(segment_ids)
                attention_masks.append(mask_ids)

with open("data/bert_summaries.json", "w") as fp:
    json.dump(bert_summaries, fp)
exit()


# convert all inputs to token tensors
examples = torch.tensor(examples).cuda()
labels = torch.tensor(labels).cuda()
segments = torch.tensor(segments).cuda()
attention_masks = torch.tensor(attention_masks).cuda()

dataset = TensorDataset(examples, segments, attention_masks, labels)
loader = DataLoader(dataset, batch_size=32)

print(examples.shape, labels.shape, segments.shape, attention_masks.shape)
model = BertForMaskedLM.from_pretrained(sys.argv[1])
model.cuda()
model.train()

# Parameters:
lr = 1e-3
max_grad_norm = 1.0
num_training_steps = 6000
num_warmup_steps = 100
warmup_proportion = float(num_warmup_steps) / float(num_training_steps)  # 0.1

optimizer = AdamW(model.parameters(), lr=lr, correct_bias=False)  # To reproduce BertAdam specific behavior set correct_bias=False
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=num_warmup_steps, num_training_steps=num_training_steps)  # PyTorch scheduler

for i in range(0, 5):
    print("EPOCH", i)
    #model.load_state_dict(torch.load("summary_encoder_epoch{}.pth".format(i-1), map_location=lambda storage, loc: storage))

    epoch_loss = 0
    for batch_id, (example, segment, attention, label) in enumerate(loader):
        loss, _ = model(input_ids=example, attention_mask=attention, token_type_ids=segment, lm_labels=label)
        epoch_loss += loss.item()
        if batch_id % 100 == 0:
            print("\tbatch_id: {}, loss: {}".format(batch_id, epoch_loss / (batch_id + 1)))
            model_state = model.state_dict()
            torch.save(model_state, "summary_encoder_epoch{}.pth".format(i))
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)  # Gradient clipping is not in AdamW anymore (so you can use amp without issue)
        optimizer.step()
        scheduler.step()
        optimizer.zero_grad()
    print(epoch_loss / (batch_id + 1))
    model_state = model.state_dict()
    torch.save(model_state, "summary_encoder_epoch{}.pth".format(i))
