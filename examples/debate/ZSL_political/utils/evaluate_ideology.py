import sys
import json
import os

def obtain_data(lhs_preds):
    for pred in lhs_preds:
        pred = pred.split(')')[0]
        name, args = pred.split('(')
        args = args.split(',')
        if name.startswith('HasUserStance'):
            debate = args[0]
            instigator = args[1]
        elif name.startswith('HasIssue'):
            issue = args[1]
    return issue, debate, instigator

def obtain_output(pred):
    pred = pred.split(')')[0]
    name, args = pred.split('(')
    args = args.split(',')
    ideology = args[1]
    return ideology

def get_pred_ideology(dictionary):
    max_ideo = None; max_score = -1
    for ideo in dictionary:
        score, pred = dictionary[ideo]
        if pred == 1:
            max_ideo = ideo
            max_score = score
            break
    return max_ideo, max_score

def main():
    debates = json.load(open("data/debates_preprocessed_ALL.json"))
    title_scores = {}

    for fold in range(0, 10):
        filename = "ideology_scores_{}.csv".format(fold)
        filename = os.path.join("scores", filename)
        with open(filename) as fp:
            for line in fp:
                rule, score, prediction = line.strip().split(';')
                score = float(score)
                prediction = int(prediction)

                lhs, rhs = rule.strip().split(' => ')
                lhs_preds = lhs.split(' & ')

                for pred in lhs_preds:
                    if pred.startswith('HasUserStance'):
                        issue, debate, instigator = obtain_data(lhs_preds)
                        ideology = obtain_output(rhs)
                        #print(issue, debate, instigator, ideology, score)
                        if issue not in title_scores:
                            title_scores[issue] = {}
                        if debate not in title_scores[issue]:
                            title_scores[issue][debate] = {}
                        title_scores[issue][debate][ideology] = (score, prediction)
                        break

    predictions = {}
    for issue in title_scores:
        for debate in title_scores[issue]:
            max_ideo, max_score = get_pred_ideology(title_scores[issue][debate])
            #print(max_ideo, max_score)
            issue_ = issue
            if issue == "deathpenalty":
                issue_ = "death penalty"

            if issue not in predictions:
                predictions[issue] = {}
            if max_ideo not in predictions[issue]:
                predictions[issue][max_ideo] = []

            predictions[issue][max_ideo].append((max_score, debates[issue_][debate]['title'], debate))

    for issue in predictions:
        for ideo in predictions[issue]:
            if ideo is None:
                continue
            titles = predictions[issue][ideo]
            titles.sort(reverse=True)
            print(issue, ideo)
            for t in titles[:50]:
                title, score, debate = t
                print("\t{0}: {1}".format(title, score))
                for ideo_ in predictions[issue]:
                    if ideo_ is None:
                        continue
                    #print(title_scores[issue].keys())
                    print("\t\t{0}: {1}".format(ideo_, title_scores[issue][debate][ideo_][0]))
            print("------------")

if __name__ == "__main__":
    main()
