import json
import random

debates = json.load(open("data/debates_preprocessed_ALL.json"))

all_debates = []
hard_folds = {}
rand_folds = {}


for issue_id, issue in enumerate(debates):
    hard_folds[issue_id] = []
    for debate_id in debates[issue]:
        all_debates.append(debate_id)
        hard_folds[issue_id].append(debate_id)

random.shuffle(all_debates)


def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

folds = list(split(all_debates, 10))

for i in range(10):
    rand_folds[i] = folds[i]

with open("data/hard_folds.json", "w") as fp:
    json.dump(hard_folds, fp)

with open("data/rand_folds.json", "w") as fp:
    json.dump(rand_folds, fp)
