import sys
import json
import os
import numpy as np
from collections import Counter

def get_pred_ideology(dictionary):
    max_ideo = None; max_score = -1
    for ideo in dictionary:
        score = dictionary[ideo]
        if score > max_score:
            max_ideo = ideo
            max_score = score
    return max_ideo, max_score

def obtain_data(lhs_preds):
    for pred in lhs_preds:
        pred = pred.split(')')[0]
        name, args = pred.split('(')
        args = args.split(',')
        if name.startswith('MakesStatement'):
            issue = args[0]
            politician = args[1]
            statement = args[2]
    return issue, politician, statement

def obtain_output(pred):
    pred = pred.split(')')[0]
    name, args = pred.split('(')
    args = args.split(',')
    ideology = args[1]
    return ideology

def main():
    statement_scores = {}

    for fold in range(0, 10):
        filename = "politician_scores_{}.csv".format(fold)
        filename = os.path.join("scores", filename)
        with open(filename) as fp:
            for line in fp:
                rule, score, prediction = line.strip().split(';')
                score = float(score)
                prediction = int(prediction)

                lhs, rhs = rule.strip().split(' => ')
                lhs_preds = lhs.split(' & ')

                for pred in lhs_preds:
                    if pred.startswith('MakesStatement'):
                        issue, politician, statement = obtain_data(lhs_preds)
                        ideology = obtain_output(rhs)
                        if issue not in statement_scores:
                            statement_scores[issue] = {}
                        if politician not in statement_scores[issue]:
                            statement_scores[issue][politician] = {}
                        if statement not in statement_scores[issue][politician]:
                            statement_scores[issue][politician][statement] = {}
                        if ideology not in statement_scores[issue][politician][statement]:
                            statement_scores[issue][politician][statement][ideology] = []
                        statement_scores[issue][politician][statement][ideology].append(score)

    compact = {}; max_statements = {}; predictions = {}
    for issue in statement_scores:
        compact[issue] = {}
        max_statements[issue] = {}
        predictions[issue] = {}
        for politician in statement_scores[issue]:
            compact[issue][politician] = {}
            max_statements[issue][politician] = []
            predictions[issue][politician] = {}
            for statement in statement_scores[issue][politician]:
                predictions[issue][politician][statement] = {}
                for ideology in statement_scores[issue][politician][statement]:
                    if ideology not in compact[issue][politician]:
                        compact[issue][politician][ideology] = []
                    avg_score = np.mean(statement_scores[issue][politician][statement][ideology])
                    max_statements[issue][politician].append((avg_score, ideology, statement))
                    compact[issue][politician][ideology].append(avg_score)
                    predictions[issue][politician][statement][ideology] = avg_score
                    #print("{0}\t{1}\t{2}\t{3}\t{4}".format(issue, politician, statement, ideology, avg_score))

    '''
    for issue in compact:
        for politician in compact[issue]:
            for ideology in compact[issue][politician]:
                print(issue, politician, ideology, np.mean(compact[issue][politician][ideology]))
    print("======")
    for issue in max_statements:
        for politician in max_statements[issue]:
            all_statements = max_statements[issue][politician]
            all_statements.sort(reverse=True)
            print(all_statements[:5])
    '''
    statement_text = json.load(open("data/politician_statements_orig.json"))
    scores = {}
    for issue in predictions:
        scores[issue] = {}
        for politician in predictions[issue]:
            print(issue, politician)
            scores[issue][politician] = []
            for statement in predictions[issue][politician]:
                #print(predictions[issue][politician][statement])
                max_ideo, max_score = get_pred_ideology(predictions[issue][politician][statement])
                #print(max_ideo, max_score)
                if max_ideo == "Conservative" or max_ideo == "Libertarian":
                    print("\t{0}\t{1}\t{2}".format(statement_text[statement], max_score, max_ideo))
                scores[issue][politician].append(max_ideo)

    for issue in scores:
        for politician in scores[issue]:
            print(issue, politician, Counter(scores[issue][politician]))

if __name__ == "__main__":
    main()
