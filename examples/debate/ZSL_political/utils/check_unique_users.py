import numpy as np
from collections import Counter
import json

def load_issues(filenamecon, filenamepro, include_leaning=True):
    with open(filenamepro) as f:
        pro = json.load(f)
    with open(filenamecon) as f:
        con = json.load(f)

    stances = {}
    issues = pro.keys()

    for idx, issue in enumerate(issues):
        for user in pro[issue]:
            if user not in stances:
                stances[user] = {}
                stances[user]['pro'] = [0.0] * len(issues)
                stances[user]['con'] = [0.0] * len(issues)
            stances[user]['pro'][idx] = 1.0
        for user in con[issue]:
            if user not in stances:
                stances[user] = {}
                stances[user]['pro'] = [0.0] * len(issues)
                stances[user]['con'] = [0.0] * len(issues)
            stances[user]['con'][idx] = 1.0
    return issues, stances

users = {}; voters = {}

user_info = json.load(open("data/user_info.json"))
issues, stances = load_issues("data/issues_con.json", "data/issues_pro.json")

def user_bigissues_1hot(author):
    if author in stances:
        ret = stances[author]['pro'] + stances[author]['con']
    else:
        ret = [0.0] * len(issues) * 2
    return ret

def user_issues_and_information(author):
    # Dealing with one noisy case
    if author == "OpenDebate":
        author = "Open Debate"
    ret = user_bigissues_1hot(author)
    for feat in user_info['features']:
        temp = [0] * len(user_info['features'][feat])
        if author in user_info and feat in user_info[author]:
            index = user_info['features'][feat].index(user_info[author][feat])
            temp[index] = 1
        ret += temp
    #print(len(ret))
    return ret

def ideologies(author):
    # Dealing with one noisy case
    if author == "OpenDebate":
        author = "Open Debate"
    ret = user_bigissues_1hot(author)
    if author in user_info and 'Ideology' in user_info[author]:
        return user_info[author]['Ideology']
    else:
        return 'Not Saying'

with open("data/participates_in.txt") as fp:
    for line in fp:
        debate, user = line.strip().split('\t')
        if user not in users:
            users[user] = []
        users[user].append(debate)

with open("data/votes_in.txt") as fp:
    for line in fp:
        debate, user = line.strip().split('\t')
        if user not in voters:
            voters[user] = []
        voters[user].append(debate)

print("AUTHORS")
print(len(users))
num_debates = [len(users[user]) for user in users]
print(np.mean(num_debates))
print(np.max(num_debates))
print(Counter(num_debates))

user_keys = set(users.keys())
voter_keys = set(voters.keys())
no_feats = set([])
no_vect = set([])

has_ideology = open("data/has_ideology.txt", "w")
has_ideology_binary = open("data/has_ideology_binary.txt", "w")

reports_ideology = open("data/reports_ideology.txt", "w")
reports_ideology_limited = open("data/reports_ideology_limited.txt", "w")
reports_ideology_binary = open("data/reports_ideology_binary.txt", "w")
all_ideologies = set([])

num_right = 0; num_left = 0
for user in user_keys | voter_keys:
    ideo = ideologies(user)
    if ideo not in ['Not Saying', 'Other']:
        all_ideologies.add(ideo)
        has_ideology.write("{0}\t{1}\n".format(user, ideo))
        reports_ideology.write("{0}\n".format(user))
        if ideo in ['Conservative', 'Liberal', 'Libertarian', 'Progressive', 'Moderate']:
            reports_ideology_limited.write("{0}\n".format(user))

        # To create a binary classifier using main classes
        if ideo == 'Conservative':
            reports_ideology_binary.write("{0}\n".format(user))
            has_ideology_binary.write("{0}\t{1}\n".format(user, "Right"))
            num_right += 1
        elif ideo in ['Liberal', 'Progressive']:
            reports_ideology_binary.write("{0}\n".format(user))
            has_ideology_binary.write("{0}\t{1}\n".format(user, "Left"))
            num_left += 1

        #elif ideo == "Moderate":
        #    reports_ideology_binary.write("{0}\n".format(user))

print(num_left, num_right)

has_ideology.close()
has_ideology_binary.close()

reports_ideology.close()
reports_ideology_limited.close()
reports_ideology_binary.close()

with open("data/ideology_label.txt", "w") as fp:
    for ideo in all_ideologies:
        fp.write("{0}\n".format(ideo))

user2id = json.load(open("data/user2id.json"))
id2user = dict(map(reversed, user2id.items()))

friend_emb = {}
with open("data/debateorg_friends.emb") as fp:
    for i, line in enumerate(fp):
        if i == 0:
            continue
        idx, vect = line.strip().split(" ", 1)
        idx = int(idx)
        if idx not in id2user:
            continue
        friend_emb[id2user[int(idx)]] = list(map(float, vect.split()))

print(len(friend_emb))

with open("data/friend_emb.json", "w") as fp:
    json.dump(friend_emb, fp)

has_features = open("data/has_features.txt", "w")
for user in (voter_keys | user_keys):
    if user not in friend_emb:
        no_vect.add(user)
    ret = user_issues_and_information(user)
    if 1.0 not in ret:
        no_feats.add(user)
    if user not in no_feats or user not in no_vect:
        has_features.write(user)
        has_features.write("\n")
has_features.close()

print("NO FEAT USERS", len(no_feats & user_keys))

print("VOTERS")
print(len(voters))
num_debates = [len(voters[user]) for user in voters]
print(np.mean(num_debates))
print(np.max(num_debates))
print(Counter(num_debates))


no_feats_one_debate = 0
for user in voters:
    if user in no_feats and len(voters[user]) == 1:
        no_feats_one_debate += 1


print("NO FEAT VOTERS", len(voter_keys & no_feats))
print("NO FEARS ONE DEBATE", no_feats_one_debate)

print(len(voter_keys - user_keys))
print(len(voter_keys | user_keys))

print("no_vect users(authors and voters)", len(no_vect))
print("no_vect no feats users", len(no_vect & no_feats))
print("no_vect authors", len(no_vect & user_keys), "no_vect no feats authors", len(no_vect & no_feats & user_keys))
print("no_vect voters", len(no_vect & voter_keys), "no_vect no feats voters", len(no_vect & no_feats & voter_keys))
