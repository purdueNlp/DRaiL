from transformers import BertTokenizer, AutoTokenizer, AutoModel, AutoConfig
import json

statements = {}; statement_id = 0; statements_orig = {}
bert_model = "google/bert_uncased_L-4_H-512_A-8"

tokenizer = AutoTokenizer.from_pretrained(bert_model)

politician_ideologies = {
    "bernie": "Progressive",
    "biden": "Liberal",
    "johnson": "Libertarian",
    "pence": "Conservative",
    "trump": "Conservative"
}


has_statement_ideology = open("data/has_statement_ideology.txt", "w")
with open("data/makes_statement.txt", "w") as fp:
    for issue in ["guns", "abortion"]:
        for politician in ["bernie", "biden", "johnson", "pence", "trump"]:
            filename = "data/politicians/{0}_{1}.txt".format(politician, issue)
            with open(filename) as rp:
                for line in rp:
                    statements[statement_id] = tokenizer.encode(line.strip())
                    statements_orig[statement_id] = line.strip()
                    fp.write("{0}\t{1}\t{2}\n".format(issue, politician, statement_id))

                    has_statement_ideology.write("{0}\t{1}\n".format(statement_id, politician_ideologies[politician]))
                    statement_id += 1
has_statement_ideology.close()

with open("data/has_politician_ideology.txt", "w") as fp:
    for politician in politician_ideologies:
        ideology = politician_ideologies[politician]
        fp.write("{0}\t{1}\n".format(politician, ideology))

with open("data/politician_statements.json", "w") as fp:
    json.dump(statements, fp)

with open("data/politician_statements_orig.json", "w") as fp:
    json.dump(statements_orig, fp)
