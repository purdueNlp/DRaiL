import json

has_post = set([]) #(debate, post)
has_sentence = set([]) #(debate, post, sentence)
is_author = set([]) #(debate, post, user)
has_issue = set([]) #(debate, issue)
participates_in = set([]) #(debate, user)

# Will we do stance at the post level or at the sentence level?
has_stance = set([]) #(debate, post, stance)
has_user_stance = set([]) #(debate, user, stance)
different = set([]) #(debate, user, user)
votes_for = set([]) #(debate, user, user)
vote_same = set([]) #(debate, user, user)
votes_in = set([]) #(debate, user)
has_voter_stance = set([]) #(debate, voter)

debates = json.load(open("data/debates_preprocessed_ALL.json"))
votes = json.load(open("data/votes.json"))

n_empty = 0; n_not_empty = 0; n_debates = 0
for issue in debates:
    issue_w = issue
    if issue == "death penalty":
        issue_w = "deathpenalty"
    for debate in debates[issue]:
        n_debates += 1

        has_issue.add((debate, issue_w))

        post_pos = 0

        author_con = debates[issue][debate]['authors']['con']
        author_pro = debates[issue][debate]['authors']['pro']
        # Deal with one particular noisy case
        if author_con == "Open Debate":
            author_con = "OpenDebate"

        different.add((debate, author_pro, author_con))
        different.add((debate, author_con, author_pro))
        for post, stance in zip(debates[issue][debate]['posts'], debates[issue][debate]['stances']):
            #if debate == "091931":
            #    print("post", post_pos, stance)

            if post is None or len(post) <= 0:
                n_empty += 1
                #print("debate", debate, post_pos)
                post_pos += 1
                continue
            n_not_empty += 1

            has_post.add((debate, post_pos))
            has_stance.add((debate, post_pos, stance))
            is_author.add((debate, post_pos, debates[issue][debate]['authors'][stance]))
            participates_in.add((debate, debates[issue][debate]['authors'][stance]))
            has_user_stance.add((debate, debates[issue][debate]['authors'][stance], stance))

            prev_sentence = -1
            for sent_pos, sent in enumerate(post):
                #if debate == "091931":
                #    print("sent", debate, post_pos, sent_pos)
                has_sentence.add((debate, post_pos, sent_pos))
            post_pos += 1

for debate in votes:
    authors = list(votes[debate].keys())
    author0 = authors[0]
    author1 = authors[1]
    for author in votes[debate]:
        for voter in votes[debate][author]:
            # Skip voters that are actually authors
            if voter in votes[debate]:
                continue
            if (debate, author, 'pro') in has_user_stance:
                has_voter_stance.add((debate, voter, 'pro'))
            elif (debate, author, 'con') in has_user_stance:
                has_voter_stance.add((debate, voter, 'con'))
            else:
                continue

            votes_for.add((debate, voter, author))
            votes_for.add((debate, author, voter))
            votes_in.add((debate, voter))

            for voter_ in votes[debate][author]:
                if voter != voter_:
                    vote_same.add((debate, voter, voter_))
                    vote_same.add((debate, voter_, voter))
                    different.add((debate, voter, voter_))
                    different.add((debate, voter_, voter))

    for voter0 in votes[debate][author0]:
        for voter1 in votes[debate][author1]:
            if voter0 != voter1:
                different.add((debate, voter0, voter1))
                different.add((debate, voter1, voter0))

print("n_empty", n_empty, "n_not_empty", n_not_empty, "n_debates", n_debates)

dataset = {
    "has_issue": has_issue, "has_post": has_post,
    "has_stance": has_stance, "has_sentence": has_sentence,
    "is_author": is_author, "participates_in": participates_in,
    "has_user_stance": has_user_stance, "different": different,
    "votes_in": votes_in, "votes_for": votes_for,
    "has_voter_stance": has_voter_stance, "vote_same": vote_same
}

for filename in dataset:
    with open("data/{0}.txt".format(filename), "w") as fp:
        for tup in dataset[filename]:
            tup_str = map(str, tup)
            fp.write("\t".join(tup_str))
            fp.write("\n")
