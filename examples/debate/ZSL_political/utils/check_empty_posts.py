import json
import numpy as np

debates_ids = json.load(open("data/debate_ids.json"))

issues = ["abortion", "death penalty", "environment", "evolution", "feminism",
          "guns", "healthcare", "immigration", "lgbt", "terrorism"]

n_counts = {}
for issue in issues:
    n_counts[issue] = {}
    n_counts[issue]['n_empty'] = 0
    n_counts[issue]['n'] = 0
    n_counts[issue]['n_pro'] = 0
    n_counts[issue]['n_con'] = 0
    n_counts[issue]['debate'] = set([])
    n_counts[issue]['debate_length'] = {}

with open("data/has_stance.txt") as fp:
    for line in fp:
        debate_id, post_pos, stance = line.strip().split("\t")

        for issue in issues:

            if debate_id in debates_ids[issue]:
                if len(debates_ids[issue][debate_id]["posts"][int(post_pos)]) <= 0:
                    n_counts[issue]['n_empty'] += 1
                else:
                    n_counts[issue]['n'] += 1
                    n_counts[issue]['debate'].add(debate_id)
                    if debate_id not in n_counts[issue]['debate_length']:
                        n_counts[issue]['debate_length'][debate_id] = 0
                    n_counts[issue]['debate_length'][debate_id] += 1

                    if stance == "pro":
                        n_counts[issue]['n_pro'] += 1
                    else:
                        n_counts[issue]['n_con'] += 1

for issue in issues:
    print(issue)
    print("n_empty: {}".format(n_counts[issue]['n_empty']))
    print("n: {}".format(n_counts[issue]['n']))
    print("n PRO: {}".format(n_counts[issue]['n_pro']))
    print("n CON: {}".format(n_counts[issue]['n_con']))
    print("n debates: {}".format(len(n_counts[issue]['debate'])))
    num_posts = list(n_counts[issue]['debate_length'].values())
    print("statistics (NUM POSTS) -- max {}, min {}, mean {}, median {}".format(max(num_posts), min(num_posts), np.mean(num_posts), np.median(num_posts)))

