import json
import numpy as np

debates_ids = json.load(open("data/debate_ids.json"))
bert_encode = np.load(open("data/bert_encode.npy"))

print(bert_encode.shape)
