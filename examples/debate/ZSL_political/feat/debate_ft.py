from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import json
import numpy as np

class Debates_ft(FeatureExtractor):
    def __init__(self, debates_f, bert_encoding_f,
                 user_info, issues_pro, issues_con, bert_summaries,
                 friend_emb, max_seq_title=57, max_seq_post=452, max_seq_summary=320):
        super(Debates_ft, self).__init__()
        # Debate ids to look in BERT matrix
        self.debates_f = debates_f
        # BERT stuff
        self.bert_encoding_f = bert_encoding_f
        self.max_seq_title = max_seq_title
        self.max_seq_post = max_seq_post
        self.max_seq_summary = max_seq_summary
        self.CLS = 101
        self.SEP = 102

        self.issues_pro_f = issues_pro
        self.issues_con_f = issues_con
        self.user_info_f = user_info
        self.bert_summaries_f = bert_summaries
        self.friend_emb_f = friend_emb

    def load_issues(self, filenamecon, filenamepro, include_leaning=True):
        with open(filenamepro) as f:
            pro = json.load(f)
        with open(filenamecon) as f:
            con = json.load(f)

        stances = {}
        self.issues = pro.keys()

        for idx, issue in enumerate(self.issues):
            '''
            if not include_leaning and issue in ['Abortion', 'Gay Marriage', 'Drug Legalization',
                                                 'Environmental Protection', 'Global Warming Exists',
                                                 'Medical Marijuana', 'National Health Care',
                                                 'Death Penalty', 'Gun Rights', 'War on Terror',
                                                 'War in Afghanistan', 'Border Fence']:
                continue
            '''
            for user in pro[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['pro'][idx] = 1.0
            for user in con[issue]:
                if user not in stances:
                    stances[user] = {}
                    stances[user]['pro'] = [0.0] * len(self.issues)
                    stances[user]['con'] = [0.0] * len(self.issues)
                stances[user]['con'][idx] = 1.0
        return stances

    def build(self):
        self.stance_idx = {'con': 0, 'pro': 1}
        '''
        self.ideology_idx = {
            "Communist": 0, "Socialist": 1, "Conservative": 2,
            "Apathetic": 3, "Libertarian": 4, "Progressive": 5,
            "Undecided": 6, "Anarchist": 5, "Labor": 6,
            "Green": 7, "Liberal": 8, "Moderate": 9
        }
        self.ideology_idx = {
                "Conservative": 0, "Libertarian": 1,
                "Moderate": 2, "Liberal": 3, "Progressive": 4}
        '''
        self.ideology_idx = {"Left": 0, "Right": 1}
        self.bert_encoding = np.load(self.bert_encoding_f, allow_pickle=True)
        print("Bert encodings", self.bert_encoding.shape)

        self.debates = json.load(open(self.debates_f))
        self.user_info = json.load(open(self.user_info_f))
        self.stances = self.load_issues(self.issues_con_f, self.issues_pro_f)
        self.friend_emb = json.load(open(self.friend_emb_f))

        #self.bert_summaries = json.load(open(self.bert_summaries_f))

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] == 'HasUserStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] == 'HasVoterStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] in ['HasIdeology', 'HasPostIdeology', 'HasTitleIdeology']:
            label = self.ideology_idx[head['arguments'][-1]]
        return label

    def ideology_1hot(self, rule_grd):
        ideology = rule_grd.get_body_predicates("Ideology")[0]['arguments'][0]
        idx = self.ideology_idx[ideology]
        ret = [0] * 5
        ret[idx] = 1
        return ret

    def pad_and_mask(self, vector, max_seq_len):
        pad_token = 0
        mask = [1] * len(vector)
        if len(vector) < max_seq_len:
            vector += [pad_token] * (max_seq_len - len(vector))
            mask += [0] * (max_seq_len - len(mask))
        return (vector, mask)

    def bert_post(self, rule_grd):
        has_post = rule_grd.get_body_predicates("HasPost")[0]
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]

        _, issue_id = has_issue['arguments']
        debate_id, post_id = has_post['arguments']

        if issue_id == "deathpenalty":
            issue_id = "death penalty"
        sentences = self.debates[issue_id][debate_id]["posts"][post_id]

        post_vec = []
        for sent_id in sentences:
            post_vec += self.bert_encoding[sent_id][:self.max_seq_post]
            if len(post_vec) >= self.max_seq_post:
                break

        post_vec = post_vec[:self.max_seq_post]

        post_vec = [self.CLS] + post_vec
        post_vec = post_vec + [self.SEP]

        (post_vec, post_mask) = self.pad_and_mask(post_vec, self.max_seq_post + 2)
        return (post_vec, post_mask)

    def bert_sentence(self, rule_grd):
        has_sentence = rule_grd.get_body_predicates("HasSentence")[0]
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]

        _, issue_id = has_issue['arguments']
        debate_id, post_id, sent_id = has_sentence['arguments']
        sentence = self.bert_encoding[sent_id][:self.max_seq_title]
        sentence = [self.CLS] + sentence
        sentence = sentence + [self.SEP]

        (sent_vec, sent_mask) = self.pad_and_mask(sentence, self.max_seq_title + 2)
        return (sent_vec, sent_mask)

    def bert_summary(self, rule_grd, user_predicate, user_index):
        pred = rule_grd.get_body_predicates(user_predicate)[user_index]
        author = pred['arguments'][1]
        # Dealing with one noisy case
        if author == "OpenDebate":
            author = "Open Debate"

        if author in self.bert_summaries:
            summary_vec = self.bert_summaries[author]
            summary_vec = summary_vec[:self.max_seq_summary]
        else:
            summary_vec = []

        summary_vec = [self.CLS] + summary_vec
        summary_vec = summary_vec + [self.SEP]

        (summary_vec, summary_mask) = self.pad_and_mask(summary_vec, self.max_seq_summary + 2)
        return (summary_vec, summary_mask)

    def bert_summary_user(self, rule_grd):
        return self.bert_summary(rule_grd, "ParticipatesIn", 0)

    def bert_summary_voter(self, rule_grd):
        return self.bert_summary(rule_grd, "VotesIn", 0)

    def user_bigissues_1hot(self, author):
        if author in self.stances:
            ret = self.stances[author]['pro'] + self.stances[author]['con']
        else:
            ret = [0.0] * len(self.issues) * 2
        return ret

    def user_issues_and_information(self, rule_grd, user_predicate, user_index, skip_ideo=False):
        pred = rule_grd.get_body_predicates(user_predicate)[user_index]
        author = pred['arguments'][-1]
        # Dealing with one noisy case
        if author == "OpenDebate":
            author = "Open Debate"
        ret = self.user_bigissues_1hot(author)
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])

            if not skip_ideo or feat != "Ideology":
                if author in self.user_info and feat in self.user_info[author]:
                    index = self.user_info['features'][feat].index(self.user_info[author][feat])
                    temp[index] = 1
            ret += temp
        #print(len(ret))
        return ret

    def user_emb_helper(self, rule_grd, user_predicate, user_index):
        pred = rule_grd.get_body_predicates(user_predicate)[user_index]
        author = pred['arguments'][-1]
        # Dealing with one noisy case
        if author == "OpenDebate":
            author = "Open Debate"
        if author in self.friend_emb:
            #print(author)
            #print(len(self.friend_emb[author]))
            return self.friend_emb[author]
        else:
            return [0.0] * 128

    def user_emb(self, rule_grd):
        if rule_grd.has_predicate("ParticipatesIn"):
            return self.user_emb_helper(rule_grd, "ParticipatesIn", 0)
        elif rule_grd.has_predicate("ParticipatesOrVotesIn"):
            return self.user_emb_helper(rule_grd, "ParticipatesOrVotesIn", 0)
        else:
            return self.user_emb_helper(rule_grd, "IsAuthor", 0)

    def other_user_emb(self, rule_grd):
        if rule_grd.has_predicate("ParticipatesIn"):
            return self.user_emb_helper(rule_grd, "ParticipatesIn", 1)
        else:
            return self.user_emb_helper(rule_grd, "IsAuthor", 1)

    def voter_emb(self, rule_grd):
        return self.user_emb_helper(rule_grd, "VotesIn", 0)

    def other_voter_emb(self, rule_grd):
        return self.user_emb_helper(rule_grd, "VotesIn", 1)

    def user_features(self, rule_grd):
        if rule_grd.has_predicate("ParticipatesIn"):
            return self.user_issues_and_information(rule_grd, "ParticipatesIn", 0)
        elif rule_grd.has_predicate("ParticipatesOrVotesIn"):
            return self.user_issues_and_information(rule_grd, "ParticipatesOrVotesIn", 0, skip_ideo=True)
        else:
            return self.user_issues_and_information(rule_grd, "IsAuthor", 0)

    def user_features_no_ideo(self, rule_grd):
        if rule_grd.has_predicate("ParticipatesIn"):
            return self.user_issues_and_information(rule_grd, "ParticipatesIn", 0, skip_ideo=True)
        elif rule_grd.has_predicate("ParticipatesOrVotesIn"):
            return self.user_issues_and_information(rule_grd, "ParticipatesOrVotesIn", 0, skip_ideo=True)
        else:
            return self.user_issues_and_information(rule_grd, "IsAuthor", 0, skip_ideo=True)

    def user_features_emb(self, rule_grd):
        return self.user_features(rule_grd) + self.user_emb(rule_grd)

    def other_user_features(self, rule_grd):
        if rule_grd.has_predicate("ParticipatesIn"):
            return self.user_issues_and_information(rule_grd, "ParticipatesIn", 1)
        else:
            return self.user_issues_and_information(rule_grd, "IsAuthor", 1)

    def other_user_features_emb(self, rule_grd):
        return self.other_user_features(rule_grd) + self.other_user_emb(rule_grd)

    def voter_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd, "VotesIn", 0)

    def voter_features_emb(self, rule_grd):
        return self.voter_features(rule_grd) + self.voter_emb(rule_grd)

    def other_voter_features(self, rule_grd):
        return self.user_issues_and_information(rule_grd, "VotesIn", 1)

    def other_voter_features_emb(self, rule_grd):
        return self.other_voter_features(rule_grd) + self.other_voter_emb(rule_grd)

    def bert_title_seg1(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']

        if issue_id == "deathpenalty":
            issue_id = "death penalty"
        title_id = self.debates[issue_id][debate_id]["title"]

        title_vec = self.bert_encoding[title_id][:self.max_seq_title]
        title_vec = title_vec + [self.SEP]

        (title_vec, title_mask) = self.pad_and_mask(title_vec, self.max_seq_title + 1)
        return (title_vec, title_mask)

    def bert_title_seg0(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']

        if issue_id == "deathpenalty":
            issue_id = "death penalty"
        title_id = self.debates[issue_id][debate_id]["title"]

        title_vec = self.bert_encoding[title_id][:self.max_seq_title]
        title_vec = [self.CLS] + title_vec + [self.SEP]

        (title_vec, title_mask) = self.pad_and_mask(title_vec, self.max_seq_title + 2)
        return (title_vec, title_mask)
    
    def bert_title_seg0_long(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']

        if issue_id == "deathpenalty":
            issue_id = "death penalty"
        title_id = self.debates[issue_id][debate_id]["title"]

        title_vec = self.bert_encoding[title_id][:self.max_seq_post]
        title_vec = [self.CLS] + title_vec + [self.SEP]

        (title_vec, title_mask) = self.pad_and_mask(title_vec, self.max_seq_post + 2)
        return (title_vec, title_mask)
