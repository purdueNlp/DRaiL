from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import json
import numpy as np

class Politician_ft(FeatureExtractor):
    def __init__(self, bert_ids):
        super(Politician_ft, self).__init__()
        # ids to look in BERT matrix
        self.bert_ids_f = bert_ids
        self.max_seq = 100
        self.CLS = 101
        self.SEP = 102

    def build(self):
        self.ideology_idx = {
                "Conservative": 0, "Libertarian": 1,
                "Moderate": 2, "Liberal": 3, "Progressive": 4}
        self.bert_ids = json.load(open(self.bert_ids_f))

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] == 'HasUserStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] == 'HasVoterStance':
            label = self.stance_idx[head['arguments'][-1]]
        elif head['name'] in ['HasIdeology', 'HasPostIdeology', 'HasTitleIdeology']:
            label = self.ideology_idx[head['arguments'][-1]]
        return label

    def ideology_1hot(self, rule_grd):
        ideology = rule_grd.get_body_predicates("Ideology")[0]['arguments'][0]
        idx = self.ideology_idx[ideology]
        ret = [0] * 5
        ret[idx] = 1
        return ret

    def pad_and_mask(self, vector, max_seq_len):
        pad_token = 0
        mask = [1] * len(vector)
        if len(vector) < max_seq_len:
            vector += [pad_token] * (max_seq_len - len(vector))
            mask += [0] * (max_seq_len - len(mask))
        return (vector, mask)

    def bert_statement(self, rule_grd):
        makes_statement = rule_grd.get_body_predicates("MakesStatement")[0]

        issue_id, politician_id, statement_id = makes_statement['arguments']

        post_vec = self.bert_ids[statement_id][:self.max_seq]
        post_vec = [self.CLS] + post_vec
        post_vec = post_vec + [self.SEP]

        (post_vec, post_mask) = self.pad_and_mask(post_vec, self.max_seq + 2)
        return (post_vec, post_mask)

