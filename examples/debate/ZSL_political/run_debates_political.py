# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
import logging.config

from sklearn.metrics import *
#from pandas_ml import ConfusionMatrix

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--lrate", help="learning rate", type=float, default=1e-3)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument('-m', help='mode: [global|local|joint]', dest='mode', type=str, default='local')
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true', default=False)
    parser.add_argument("--folds", help="folds to use [random|hard]", type=str, required=True)
    parser.add_argument('--logging_config', help='logging configuration file', type=str, required=True)
    parser.add_argument('--start', help='start fold index', dest='start_fold_index', type=int, default=0)
    parser.add_argument('--end', help='end fold index', dest='end_fold_index', type=int, default=10)
    parser.add_argument('--bert', help='path to the saved tokenized bert input', type=str,
                        default="data/bert_encode_bert-small-uncased.npy")
    parser.add_argument('--debates', help='path to the saved tokenized debates input', type=str,
                        default="data/debate_ids_bert-small-uncased.json")
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--drop_scores', help='drop scores', action='store_true', default=False)
    parser.add_argument('--max_seq_title', type=int, default=57)
    parser.add_argument('--ad3', help='use ad3 solver for training', default=False, action='store_true')
    args = parser.parse_args()
    return args

def train(folds, avoid):
    ret = []
    for j in folds:
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    # seed
    args = parse_arguments()
    random.seed(4321)

    # FIXED RESOURCES

    # Extracted BERT encodings
    BERT_ENCODING_F = args.bert

    # Id mapping for debate text to NPY files
    DEBATES_F = args.debates

    # Paths to neural networks and feature classes
    FE_PATH = "./feat"
    NE_PATH = "./neuro"

    # Files needed for feature extraction
    USER_INFO = "./data/user_info.json"
    ISSUES_PRO = "./data/issues_pro.json"
    ISSUES_CON = "./data/issues_con.json"
    BERT_SUMMARIES = "./data/bert_summaries.json"
    FRIEND_EMB = "./data/friend_emb.json"

    optimizer = "AdamW"

    # Select whaIXED RESOURCESuse_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.mode == 'global':
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index,
                              loss_fn=args.lossfn, inference_limit=10, ad3=args.ad3)
    else:
        learner=LocalLearner(inference_limit=10)

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    debate_folds = json.load(open(args.folds))

    acc_all = {}; f1_all = {}
    y_pred_all = {}; y_gold_all = {}

    for i in range(args.start_fold_index, args.end_fold_index):
        logger.info("Fold {}".format(i))
        dev_fold = (i + 1) % 10

        if args.folds.endswith("rand_folds.json"):
            # In the case of equal random folds, take an arbitrary fold as development
            test_debates = debate_folds[str(i)]
            dev_debates = debate_folds[str(dev_fold)]
            train_debates = train(debate_folds, list(map(str, [i, dev_fold])))
        else:
            # In the case of hard folds, just take a portion of the shuffled train set as development
            test_debates = debate_folds[str(i)]
            train_debates = train(debate_folds, list(map(str, [i])))
            n_dev = int(len(train_debates) * 0.1)
            random.shuffle(train_debates)
            dev_debates = train_debates[:n_dev]
            train_debates = train_debates[n_dev:]

        if args.debug:
            train_debates = train_debates[:3]
            dev_debates = dev_debates[:3]
            test_debates = test_debates[:3]
        logger.info("Debate folds: {}, {}, {}".format(len(train_debates), len(dev_debates), len(test_debates)))

        db.add_filters(filters=[
            ("HasIssue", "isTrain", "debateId_1", train_debates),
            ("HasIssue", "isDev", "debateId_1", dev_debates),
            ("HasIssue", "isTest", "debateId_1", test_debates),
            ("HasIssue", "isDummy", "debateId_1", train_debates[:20])
        ])

        learner.build_feature_extractors(db,
                                         filters=[("HasIssue", "isDummy", 1)],
                                         debates_f=DEBATES_F,
                                         bert_encoding_f=BERT_ENCODING_F,
                                         user_info=USER_INFO,
                                         issues_pro=ISSUES_PRO,
                                         issues_con=ISSUES_CON,
                                         bert_summaries=BERT_SUMMARIES,
                                         friend_emb=FRIEND_EMB,
                                         femodule_path=FE_PATH,
                                         max_seq_title=args.max_seq_title)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=NE_PATH)

        if args.mode == "local":
            if args.continue_from_checkpoint:
                learner.init_models()

            if not args.infer_only:
                    learner.train(db,
                              train_filters=[("HasIssue", "isTrain", 1)],
                              dev_filters=[("HasIssue", "isDev", 1)],
                              test_filters=[("HasIssue", "isTest", 1)],
                              scale_data=False,
                              optimizer=optimizer)
            if not args.train_only:
                learner.extract_data(db, extract_test=True, test_filters=[("HasIssue", "isTest", 1)])
                del learner.fe
                res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
                if args.drop_scores:
                    learner.drop_scores(None, fold='test', output="debates_scores_rand_{}.csv".format(i), heads=heads)

        elif args.mode == "global" and not args.infer_only:
            learner.extract_data(
                    db,
                    train_filters=[("HasIssue", "isTrain", 1)],
                    dev_filters=[("HasIssue", "isDev", 1)],
                    test_filters=[("HasIssue", "isTest", 1)],
                    extract_train=not args.infer_only,
                    extract_dev=not args.infer_only,
                    extract_test=True)

            res, heads = learner.train(
                    db,
                    train_filters=[("HasIssue", "isTrain", 1)],
                    dev_filters=[("HasIssue", "isDev", 1)],
                    test_filters=[("HasIssue", "isTest", 1)],
                    opt_predicates=set(['HasStance', 'HasUserStance', 'HasVoterStance']),
                    loss_augmented_inference=args.delta,
                    continue_from_checkpoint=args.continue_from_checkpoint,
                    inference_only=args.infer_only,
                    scale_data=False,
                    weight_classes=True,
                    accum_loss=True,
                    patience=3,
                    optimizer=optimizer)

        if not args.train_only:
            if 'HasStance' in res.metrics:
                y_gold = res.metrics['HasStance']['gold_data']
                y_pred = res.metrics['HasStance']['pred_data']

                logger.info("HasStance (Pro/Con)")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')

                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'HasStance' not in acc_all:
                    acc_all['HasStance'] = []
                    f1_all['HasStance'] = []
                acc_all['HasStance'].append(acc_score)
                f1_all['HasStance'].append(f1)

            if 'HasUserStance' in res.metrics:
                y_gold = res.metrics['HasUserStance']['gold_data']
                y_pred = res.metrics['HasUserStance']['pred_data']

                logger.info("HasUserStance (Pro/Con)")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')

                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'HasUserStance' not in acc_all:
                    acc_all['HasUserStance'] = []
                    f1_all['HasUserStance'] = []
                acc_all['HasUserStance'].append(acc_score)
                f1_all['HasUserStance'].append(f1)
            
            if 'HasVoterStance' in res.metrics:
                y_gold = res.metrics['HasVoterStance']['gold_data']
                y_pred = res.metrics['HasVoterStance']['pred_data']

                logger.info("HasVoterStance (Pro/Con)")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')

                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'HasVoterStance' not in acc_all:
                    acc_all['HasVoterStance'] = []
                    f1_all['HasVoterStance'] = []
                acc_all['HasVoterStance'].append(acc_score)
                f1_all['HasVoterStance'].append(f1)

            if 'VotesFor' in res.metrics:
                y_gold = res.metrics['VotesFor']['gold_data']
                y_pred = res.metrics['VotesFor']['pred_data']

                logger.info("VotesFor (Pro/Con)")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')

                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'VotesFor' not in acc_all:
                    acc_all['VotesFor'] = []
                    f1_all['VotesFor'] = []
                acc_all['VotesFor'].append(acc_score)
                f1_all['VotesFor'].append(f1)

            if 'VoteSame' in res.metrics:
                y_gold = res.metrics['VoteSame']['gold_data']
                y_pred = res.metrics['VoteSame']['pred_data']

                logger.info("VoteSame (Pro/Con)")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')

                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'VoteSame' not in acc_all:
                    acc_all['VoteSame'] = []
                    f1_all['VoteSame'] = []
                acc_all['VoteSame'].append(acc_score)
                f1_all['VoteSame'].append(f1)

            if 'HasIdeology' in res.metrics:
                y_gold = res.metrics['HasIdeology']['gold_data']
                y_pred = res.metrics['HasIdeology']['pred_data']

                logger.info("HasIdeology")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')
                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))
                
                if 'HasIdeology' not in acc_all:
                    acc_all['HasIdeology'] = []
                    f1_all['HasIdeology'] = []
                    y_pred_all['HasIdeology'] = []
                    y_gold_all['HasIdeology'] = []
                y_pred_all['HasIdeology'] += y_pred
                y_gold_all['HasIdeology'] += y_gold
                acc_all['HasIdeology'].append(acc_score)
                f1_all['HasIdeology'].append(f1)

            if 'HasTitleIdeology' in res.metrics:
                y_gold = res.metrics['HasTitleIdeology']['gold_data']
                y_pred = res.metrics['HasTitleIdeology']['pred_data']

                logger.info("HasTitleIdeology")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')
                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'HasTitleIdeology' not in acc_all:
                    acc_all['HasTitleIdeology'] = []
                    f1_all['HasTitleIdeology'] = []
                    y_pred_all['HasTitleIdeology'] = []
                    y_gold_all['HasTitleIdeology'] = []
                y_pred_all['HasTitleIdeology'] += y_pred
                y_gold_all['HasTitleIdeology'] += y_gold
                acc_all['HasTitleIdeology'].append(acc_score)
                f1_all['HasTitleIdeology'].append(f1)

            if 'HasPostIdeology' in res.metrics:
                y_gold = res.metrics['HasPostIdeology']['gold_data']
                y_pred = res.metrics['HasPostIdeology']['pred_data']

                logger.info("HasPostIdeology")
                logger.info(classification_report(y_gold, y_pred))
                acc_score = accuracy_score(y_gold, y_pred)
                f1 = f1_score(y_gold, y_pred, average='macro')
                logger.info("TEST Acc {}, Macro F1 {}".format(acc_score, f1))

                if 'HasPostIdeology' not in acc_all:
                    acc_all['HasPostIdeology'] = []
                    f1_all['HasPostIdeology'] = []
                    y_pred_all['HasPostIdeology'] = []
                    y_gold_all['HasPostIdeology'] = []
                y_pred_all['HasPostIdeology'] += y_pred
                y_gold_all['HasPostIdeology'] += y_gold
                acc_all['HasPostIdeology'].append(acc_score)
                f1_all['HasPostIdeology'].append(f1)

        logger.info('\ninference encoding time:\t{0}\n'.format(learner.train_metrics.metrics['encoding_time']) +\
              'inference optimiz. time:\t{0}\n'.format(learner.train_metrics.metrics['solving_time']) +\
              'total inference time:   \t{0}'.format(learner.train_metrics.total_time()))
        learner.reset_metrics()

    print("Accuracy", acc_all)
    print("Macro F1", f1_all)
    #if 'HasIdeology' in y_gold_all:
    #    cm = ConfusionMatrix(y_gold_all['HasIdeology'], y_pred_all['HasIdeology'])
    #    print(cm)

if __name__ == "__main__":
    args = parse_arguments()
    if args.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(args.logging_config)))
    main()
