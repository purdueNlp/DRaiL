from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import json
import numpy as np
from mf_bert import mf_bert_dict, fr_bert_dict, tp_bert_dict
import random

class DebateFrames_ft(FeatureExtractor):

    def __init__(self, bert_encoding_f, max_seq_title=9, max_seq_post=500,
                 max_seq_text=200, max_seq_moral=100, max_seq_frame=7,
                 max_seq_issue=3):
        super(DebateFrames_ft, self).__init__()
        self.bert_encoding_f = bert_encoding_f
        self.max_seq_title = max_seq_title
        self.max_seq_post = max_seq_post
        self.max_seq_text = max_seq_text
        self.max_seq_moral = max_seq_moral
        self.max_seq_frame = max_seq_frame
        self.max_seq_issue = max_seq_issue
        self.CLS = 101
        self.SEP = 102
        random.seed(1234)

    def build(self):
        self.stance_idx = {'con': 0, 'pro': 1}
        self.issue_idx = {'aca': 0, 'abort': 1, 'guns': 2, 'immig': 3, 'isis': 4, 'lgbt': 5}
        self.bert_encoding = np.load(self.bert_encoding_f)

        self.bert_encoding = np.array(self.bert_encoding)
        print("Bert encodings", self.bert_encoding.shape)

    # The convention in BERT is:
    # (a) For sequence pairs:
    #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
    #  type_ids:   0   0  0    0    0     0       0   0   1  1  1  1   1   1
    # (b) For single sequences:
    #  tokens:   [CLS] the dog is hairy . [SEP]
    #  type_ids:   0   0   0   0  0     0   0
    #
    # Where "type_ids" are used to indicate whether this is the first
    # sequence or the second sequence. The embedding vectors for `type=0` and
    # `type=1` were learned during pre-training and are added to the wordpiece
    # embedding vector (and position vector). This is not *strictly* necessary
    # since the [SEP] token unambiguously separates the sequences, but it makes
    # it easier for the model to learn the concept of sequences.
    #
    # For classification tasks, the first vector (corresponding to [CLS]) is
    # used as as the "sentence vector". Note that this only makes sense because
    # the entire model is fine-tuned.
    def pad_and_mask(self, vector, max_seq_len):
        pad_token = 0
        mask = [1] * len(vector)
        if len(vector) < max_seq_len:
            vector += [pad_token] * (max_seq_len - len(vector))
            mask += [0] * (max_seq_len - len(mask))
        return (vector, mask)

    def bert_post(self, rule_grd):
        in_debate = rule_grd.get_body_predicates("InDebate")[0]
        post_id = in_debate['arguments'][0]
        post_vec = self.bert_encoding[post_id][:self.max_seq_post]

        post_vec = [self.CLS] + post_vec
        post_vec = post_vec + [self.SEP]

        (post_vec, post_mask) = self.pad_and_mask(post_vec, self.max_seq_post + 2)
        return (post_vec, post_mask)

    def bert_title(self, rule_grd):
        has_title = rule_grd.get_body_predicates("HasTitle")[0]
        title_id = has_title['arguments'][1]
        title_vec = self.bert_encoding[title_id][:self.max_seq_title]
        title_vec = title_vec + [self.SEP]

        (title_vec, title_mask) = self.pad_and_mask(title_vec, self.max_seq_title + 1)
        return (title_vec, title_mask)

    def bert_text(self, rule_grd):
        has_text = rule_grd.get_body_predicates("HasText")[0]
        text_id = has_text['arguments'][1]
        text_vec = self.bert_encoding[text_id][:self.max_seq_text]
        title_vec = [self.CLS] + text_vec + [self.SEP]

        (text_vec, text_mask) = self.pad_and_mask(text_vec, self.max_seq_text + 2)
        return (text_vec, text_mask)

    def bert_moral(self, rule_grd):
        is_moral = rule_grd.get_body_predicates("IsMoral")[0]
        moral_id = is_moral['arguments'][0]
        bert_moral = mf_bert_dict[moral_id]
        #random.shuffle(bert_moral)

        moral_vec = bert_moral + [self.SEP]

        (moral_vec, moral_mask) = self.pad_and_mask(moral_vec, self.max_seq_moral + 1)
        return (moral_vec, moral_mask)

    def bert_frame(self, rule_grd):
        has_frame = rule_grd.get_body_predicates("HasFrame")[0]
        frame_id = has_frame['arguments'][2]
        bert_frame = fr_bert_dict[frame_id]

        frame_vec = bert_frame + [self.SEP]
        (frame_vec, frame_mask) = self.pad_and_mask(frame_vec, self.max_seq_frame + 1)
        return (frame_vec, frame_mask)

    def onehot_frame(self, rule_grd):
        has_frame = rule_grd.get_body_predicates("HasFrame")[0]
        frame_id = int(has_frame['arguments'][2])

        frame_vec = np.zeros(19)
        frame_vec[frame_id] = 1.0

        return frame_vec

    def bert_issue(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        issue = has_issue['arguments'][2]
        bert_issue = tp_bert_dict[issue]

        issue_vec = bert_issue + [self.SEP]
        (issue_vec, issue_mask) = self.pad_and_mask(issue_vec, self.max_seq_issue + 1)
        return (issue_vec, issue_mask)

    def onehot_issue(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        issue = has_issue['arguments'][2]

        idx = self.issue_idx[issue]
        issue_vec = np.zeros(6)
        issue_vec[idx] = 1.0

        return issue_vec

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasMoral':
            label = int(head['arguments'][-1])
        elif head['name'] == 'HasFrame':
            label = int(head['arguments'][-1])
        elif head['name'] == 'HasParty':
            label = int(head['arguments'][-1]) - 1
        elif head['name'] == 'HasIssue':
            label = self.issue_idx[head['arguments'][-1]]
        return label
