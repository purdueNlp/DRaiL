import sys
import os
import numpy as np
import argparse
import cProfile as profile
import random
import torch
import json
import csv

from sklearn.metrics import *
from sklearn.model_selection import KFold

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('-f', '--fedir', help='fe directory', dest='fedir', type=str, required=True)
    parser.add_argument('-n', '--netdir', help='net directory', dest='nedir', type=str, required=True)
    parser.add_argument('-m', help='mode: [global|local|joint]', dest='mode', type=str, default='local')
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--inference-only', help='run inference only', dest='inference_only', default=False, action='store_true')
    parser.add_argument('--train-only', help='run training only', dest='train_only', default=False, action='store_true')
    parser.add_argument('--continue-from-checkpoint', help='continue from checkpoint in savedir', dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_argument('--lr', help='global learning rate', dest='lrate', type=float)
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true', default=False)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--congress-only', help='congress data only', dest='congress_only', default=False, action='store_true')
    opts = parser.parse_args()
    return opts

def main():
    opts = parse_options()
    CONGRESS_TWEET_IDS = "/scratch1/pachecog/DRaiL/examples/debate/ZSL_frames/data/ExternalData/congress_instance_ids.npy"

    if not opts.congress_only:
        TWEET_IDS = "/scratch1/pachecog/DRaiL/examples/debate/ZSL_frames/data/ExternalData/instance_ids.npy"
    else:
        TWEET_IDS = CONGRESS_TWEET_IDS

    TEXT_F = "/scratch1/pachecog/DRaiL/examples/debate/ZSL_frames/data/ExternalData/bert_encode.npy"

    # seed and cuda
    np.random.seed(1234)
    random.seed(1234)


    congress_tweet_ids = np.load(CONGRESS_TWEET_IDS)
    tweet_ids = np.load(TWEET_IDS)

    np.random.shuffle(congress_tweet_ids)

    offset_l = 0; offset_r = int(congress_tweet_ids.shape[0] * 0.2)
    n_tweets_dev = offset_r

    f1_scores = {}

    for i in range(0, 5):
        print("FOLD", i)
        dev_tweets = congress_tweet_ids[offset_l:offset_r]
        train_tweets = np.setdiff1d(tweet_ids, dev_tweets)

        offset_l = offset_r
        offset_r += n_tweets_dev

        print(train_tweets.shape, dev_tweets.shape)

        #if i >= 2:
        #    continue

        rule_file = os.path.join(opts.rules)
        conf_file = os.path.join(opts.config)

        if opts.gpu_index:
            torch.cuda.set_device(opts.gpu_index)

        if opts.mode == "local":
            learner = LocalLearner()
        elif opts.mode == "joint":
            learner = JointLearner(gpu=(opts.gpu_index is not None))
        elif opts.mode == "global":
            learner=GlobalLearner(learning_rate=opts.lrate, use_gpu=(opts.gpu_index is not None), gpu_index=opts.gpu_index)

        learner.compile_rules(rule_file)
        db = learner.create_dataset(opts.dir)

        # Debug
        if opts.debug:
            train_tweets = train_tweets[:10]
            dev_tweets = dev_tweets[:10]

        db.add_filters(filters=[
            ("HasText", "isDummy", "articleId_1", train_tweets[0:1]),
            ("HasText", "isTrain", "articleId_1", train_tweets),
            ("HasText", "isDev", "articleId_1", dev_tweets),
            ("HasText", "isTest", "articleId_1", dev_tweets),
            ("HasText", "allFolds", "articleId_1", np.concatenate([train_tweets, dev_tweets])),
            ])

        learner.build_feature_extractors(db,
                                         bert_encoding_f=TEXT_F,
                                         femodule_path=opts.fedir,
                                         filters=[("HasText", "isDummy", 1)])

        learner.set_savedir(os.path.join(opts.savedir, "f{0}".format(i)))
        learner.build_models(db, conf_file, netmodules_path=opts.nedir)

        if opts.mode == "global":
            learner.extract_data(
                    db,
                    train_filters=[("HasText", "isTrain", 1)],
                    dev_filters=[("HasText", "isDev", 1)],
                    test_filters=[("HasText", "isTest", 1)],
                    extract_train=not opts.inference_only,
                    extract_dev=not opts.inference_only,
                    extract_test=True,
                    pickledir=None,
                    from_pickle=False,
                    extract_constraints=True)
            res, heads = learner.train(
                    db,
                    train_filters=[("HasText", "isTrain", 1)],
                    dev_filters=[("HasText", "isDev", 1)],
                    test_filters=[("HasText", "isTest", 1)],
                    opt_predicate='HasStance',
                    loss_augmented_inference=opts.delta,
                    continue_from_checkpoint=opts.continue_from_checkpoint,
                    inference_only=opts.inference_only,
                    hot_start=False,
                    scale_data=False,
                    weight_classes=True,
                    accum_loss=False,
                    patience=3,
                    optimizer='AdamW')

        else:
            if opts.continue_from_checkpoint:
                learner.init_models(scale_data=False)

            if not opts.inference_only:
                learner.train(db, train_filters=[("HasText", "isTrain", 1)], dev_filters=[("HasText", "isDev", 1)], test_filters=[("HasText", "isTest", 1)], scale_data=False,
                              optimizer='AdamW')

            if not opts.train_only:
                learner.extract_data(db, extract_test=True, test_filters=[("HasText", "isTest", 1)])
                del learner.fe
                res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
                for pred in res.metrics:
                    if pred in set(['HasMoral', 'HasFrame']):
                        y_gold = res.metrics[pred]['gold_data']
                        y_pred = res.metrics[pred]['pred_data']

                        print classification_report(y_gold, y_pred, digits=4)
                        report = classification_report(y_gold, y_pred, digits=4, output_dict=True)
                        if pred not in f1_scores:
                            f1_scores[pred]["general"] = []
                            for k in set([y_gold]):
                                f1_scores[pred][k] = 
                        macro_f1 = f1_score(y_gold, y_pred, average='macro')
                        acc = accuracy_score(y_gold, y_pred)
                        print "TEST macro f1", macro_f1
                        print "TEST accuracy", acc

                        f1_scores[pred] = [str(macro_f1)] + f1_scores[pred]

        for pred in f1_scores:
            print pred, "=100*(" + "+".join(f1_scores[pred]) + ")/5"
if __name__ == "__main__":
    main()
