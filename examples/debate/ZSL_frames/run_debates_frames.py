# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
from sklearn.metrics import *
import issues

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--global_learn", default=False, action="store_true", help="global learning")
    parser.add_argument("--lrate", help="learning rate", type=float, default=0.01)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    args = parser.parse_args()
    return args

def train(folds, avoid):
    ret = []
    for j in folds:
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    BERT_ENCODING_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL_frames/data/bert_encode.npy"
    MODULES_PATH = "/scratch/pachecog/DRaiL/examples/debate/ZSL_frames/"
    args = parse_arguments()

    # Select what gpu to use
    use_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.global_learn:
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=use_gpu, gpu_index=args.gpu_index)
    else:
        learner=LocalLearner()

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    debate_folds = json.load(open(os.path.join(args.dir, "debate_easy_folds.json")))

    for i in range(1, 5):
        print "Fold", i
        dev_fold = (i + 1) % 5

        test_debates = debate_folds[str(i)]
        dev_debates = debate_folds[str(dev_fold)]
        train_debates = train(debate_folds, map(str, [i, dev_fold]))

        if args.debug:
            train_debates = train_debates[:10]
            dev_debates = dev_debates[:10]
            test_debates = test_debates[:10]
        print "Debate folds:", len(train_debates), len(dev_debates), len(test_debates)


        db.add_filters(filters=[
            ("InDebate", "isTrain", "debateId_2", train_debates),
            ("InDebate", "isDev", "debateId_2", dev_debates),
            ("InDebate", "isTest", "debateId_2", test_debates),
            ("InDebate", "isDummy", "debateId_2", train_debates[:1])
        ])

        learner.build_feature_extractors(db,
                                         filters=[("InDebate", "isDummy", 1)],
                                         bert_encoding_f=BERT_ENCODING_F,
                                         max_seq_title=9,
                                         max_seq_post=500,
                                         femodule_path=MODULES_PATH)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=MODULES_PATH)

        learner.train(db,
                      train_filters=[("InDebate", "isTrain", 1)],
                      dev_filters=[("InDebate", "isDev", 1)],
                      test_filters=[("InDebate", "isTest", 1)],
                      scale_data=False, optimizer='AdamW')

if __name__ == "__main__":
    main()
