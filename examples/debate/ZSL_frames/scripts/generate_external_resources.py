import json
from pytorch_transformers import BertTokenizer
import numpy as np
import csv
import re
from collections import Counter
import io

tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)

def camel_case_split(string):
    if string.islower():
        return string
    elif string.isupper():
        return string
    else:
        ret = re.sub(r'([a-z]+)([A-Z])', r'\g<1> \g<2>', string)
        ret = re.sub(r'([A-Z])([A-Z][a-z]+)', r'\g<1> \g<2>', ret)
        return ret

def preprocess_tweet(tweet):
    eyes = "[8:=;]"
    nose = "['`\-]?"
    tweet = re.sub(r'RT @\w+:', "", tweet)
    tweet = re.sub(r'\.@\w+:', "", tweet)
    tweet = re.sub(r'\.@\w+ to @\w+:', "", tweet)
    tweet = re.sub(r'https{0,1}://\S+\b|www\.(\w+\.)+\S*/', "", tweet)
    tweet = re.sub(eyes + nose + r'[)d]+|[)d]+' + nose + eyes, "", tweet)
    tweet = re.sub(eyes + nose + r'p+', "", tweet)
    tweet = re.sub(eyes + nose + r'\(+|\)+' + nose + eyes, "", tweet)
    tweet = re.sub(eyes + nose + r'[\/|l*]', "", tweet)
    tweet = re.sub(r'<3', "<heart>", tweet)
    tweet = re.sub(r'@(\w+)', lambda x: camel_case_split(x.group(1)), tweet)
    tweet = re.sub(r'#(\S+)', lambda x: camel_case_split(x.group(1)), tweet)
    tweet = re.sub(r'\s{2,}', r' ', tweet)
    #tweet = re.sub(r'[-+]?[.\d]*[\d]+[:,.\d]*', "", tweet)
    #tweet = re.sub(r'([!?.]){2,}', "", tweet)
    #tweet = re.sub(r'\b(\S*?)(.)\2{2,}\b', "", tweet)
    return tweet.strip()

def process_congressional_tweets(has_text, has_frame, has_ideology, has_moral, has_issue,
                                 in_dataset, single_instance_id, single_text_id,
                                 bert_text, tweets_text):
    frames = []; morals = []; tweet_ids = []
    with open("data/ExternalData/twitter_frames.csv", "rb") as fp:
        reader = csv.reader(fp, delimiter="|")
        for i, row in enumerate(reader):
            row = [entry.decode("utf-8") for entry in row]
            if i == 0:
                continue
            (tweet_id, tweet_text, issue_1, issue_2,
             frame_1, frame_2, frame_3, party, timestamp,
             moral_1, moral_2, polarity) = row

            tweet_ids.append(int(tweet_id))

            frame_1 = int(frame_1)
            moral_1 = int(moral_1)
            if polarity == "":
                polarity = 0
            else:
                polarity = int(polarity)

            if frame_1 >= 15:
                frame_1 += 1

            frames.append(frame_1)

            tweet_text = preprocess_tweet(tweet_text)

            text_tokens = tokenizer.encode(tweet_text)
            bert_text.append(text_tokens)
            tweets_text.append(tweet_text)
            text_id = single_text_id; single_text_id += 1
            instance_id = single_instance_id; single_instance_id += 1

            has_text.append((instance_id, text_id))
            has_frame.append((instance_id, text_id, frame_1))
            has_ideology.append((instance_id, text_id, party))
            has_issue.append((instance_id, text_id, issue_1))
            if moral_1 == 1 and polarity == 1:
                mf = 1
            elif moral_1 == 1 and polarity == 2:
                mf = 2
            elif moral_1 == 2 and polarity == 1:
                mf = 3
            elif moral_1 == 2 and polarity == 2:
                mf = 4
            elif moral_1 == 3 and polarity == 1:
                mf = 5
            elif moral_1 == 3 and polarity == 2:
                mf = 6
            elif moral_1 == 4 and polarity == 1:
                mf = 7
            elif moral_1 == 4 and polarity == 2:
                mf = 8
            elif moral_1 == 5 and polarity == 1:
                mf = 9
            elif moral_1 == 5 and polarity == 2:
                mf = 10
            elif moral_1 == 6:
                mf = 0
            morals.append(mf)

            has_moral.append((instance_id, text_id, mf))
            in_dataset.append((instance_id, "congressTweets"))

    print("Congressional Tweets:")
    print(Counter(frames))
    print(Counter(morals))
    print("tweet-ids", len(tweet_ids), len(set(tweet_ids)))
    return single_instance_id, single_text_id

def process_mftc(has_text, has_moral, has_issue, in_dataset, single_instance_id,
                 single_text_id, bert_text, tweets_text):
    morals = []
    tweets = json.load(open("data/ExternalData/tweets_updated.json"))
    for corpus in tweets:
        for tw in corpus['Tweets']:
            tweet_text = tw['tweet_text']
            if tweet_text == "no tweet text available":
                continue

            tweet_text = preprocess_tweet(tweet_text)
            text_tokens = tokenizer.encode(tweet_text)

            bert_text.append(text_tokens)
            tweets_text.append(tweet_text)

            text_id = single_text_id; single_text_id += 1
            instance_id = single_instance_id; single_instance_id += 1

            has_text.append((instance_id, text_id))

            annotations = []
            for ann in tw['annotations']:
                annotations += ann['annotation'].split(',')
            c = Counter(annotations)
            value, count = c.most_common()[0]

            if value == "care":
                mf = 1
            elif value == "harm":
                mf = 2
            elif value == "fairness":
                mf = 3
            elif value == "cheating":
                mf = 4
            elif value == "loyalty":
                mf = 5
            elif value == "betrayal":
                mf = 6
            elif value == "authority":
                mf = 7
            elif value == "subversion":
                mf = 8
            elif value == "purity":
                mf = 9
            elif value == "degradation":
                mf = 10
            elif value == "non-moral":
                mf = 0

            has_moral.append((instance_id, text_id, mf))
            morals.append(mf)
            in_dataset.append((instance_id, "mftc"))

    print("MFTC Tweets:")
    print(Counter(morals))
    return single_instance_id, single_text_id

def process_media_frame(has_text, has_frame, has_issue, in_dataset, in_section,
                        single_instance_id, single_text_id, bert_text, tweets_text):
    frames = []; headline_frames = []
    TOPICS = ["immigration", "samesex", "tobacco"]
    codes = json.load(open("data/MediaFrameCorpus/codes.json"))

    for topic in TOPICS:
        data = json.load(open("data/MediaFrameCorpus/{0}.json".format(topic)))
        for i, article in enumerate(data):
            text = data[article]['text']
            headline_frame = data[article]['headline_frame']
            primary_frame = data[article]['primary_frame']

            splits = text.split('\n\n')
            article_id = splits[0]
            headline = splits[2]
            text = " ".join(splits[3:])

            headline_tokens = tokenizer.encode(headline)
            text_tokens = tokenizer.encode(text)

            bert_text.append(headline_tokens)
            tweets_text.append(headline)
            bert_text.append(text_tokens)
            tweets_text.append(text)

            headline_id = single_text_id
            text_id = single_text_id + 1
            article_id = single_instance_id

            single_text_id += 2; single_instance_id += 1

            has_text.append((article_id, headline_id))
            has_text.append((article_id, text_id))

            if headline_frame is not None:
                headline_frame = int(headline_frame)
            else:
                headline_frame = 0
            headline_frames.append(headline_frame)

            if primary_frame is not None:
                primary_frame = int(primary_frame)
            else:
                primary_frame = 0
            frames.append(primary_frame)

            has_frame.append((article_id, headline_id, headline_frame))
            has_frame.append((article_id, text_id, primary_frame))

            in_dataset.append((article_id, "mediaFrames"))
            in_section.append((article_id, headline_id, 0))
            in_section.append((article_id, text_id, 1))

    print("Media Frames:")
    print("Headlines", Counter(headline_frames))
    print("Articles", Counter(frames))

    return single_instance_id, single_text_id

def main():
    has_text = []; has_moral = []; has_frame = []; has_ideology = []
    in_dataset = []; in_section = []; has_issue = []
    single_instance_id = 0; single_text_id = 0
    bert_text = []; tweets_text = []

    single_instance_id, single_text_id = \
        process_congressional_tweets(has_text, has_frame, has_ideology, has_moral, has_issue,
                                     in_dataset, single_instance_id, single_text_id,
                                     bert_text, tweets_text)
    single_instance_id_congress = single_instance_id

    single_instance_id, single_text_id = \
        process_mftc(has_text, has_moral, has_issue, in_dataset, single_instance_id,
                     single_text_id, bert_text, tweets_text)

    single_instance_id, single_text_id = \
        process_media_frame(has_text, has_frame, has_issue, in_dataset, in_section, single_instance_id,
                            single_text_id, bert_text, tweets_text)

    files = ["has_text.txt", "has_frame.txt", "has_ideology.txt", "has_moral.txt",
             "has_issue.txt", "in_dataset.txt", "in_section.txt"]
    data = [has_text, has_frame, has_ideology, has_moral, has_issue, in_dataset, in_section]

    for f, d in zip(files, data):
        with open("data/ExternalData/{0}".format(f), "w") as fp:
            for row in d:
                fp.write("\t".join(map(str, row)))
                fp.write("\n")

    instance_ids = range(0, single_instance_id)
    congress_instance_ids = range(0, single_instance_id_congress)

    print("instances:", single_instance_id, "texts:", single_text_id)
    np.save("data/ExternalData/tweets_text.npy", tweets_text, fix_imports=True)
    np.save("data/ExternalData/bert_encode.npy", bert_text, fix_imports=True)
    np.save("data/ExternalData/instance_ids.npy", instance_ids, fix_imports=True)
    np.save("data/ExternalData/congress_instance_ids.npy", congress_instance_ids, fix_imports=True)

if __name__ == "__main__":
    main()
