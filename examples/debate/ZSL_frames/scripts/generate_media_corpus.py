import json
from pytorch_transformers import BertTokenizer
import numpy as np

# MediaFrameCorpus
TOPICS = ["immigration", "samesex", "tobacco"]

codes = json.load(open("data/MediaFrameCorpus/codes.json"))
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)

has_headline = open("data/MediaFrameCorpus/has_headline.txt", "w")
has_text = open("data/MediaFrameCorpus/has_text.txt", "w")
has_frame = open("data/MediaFrameCorpus/has_frame.txt", "w")

single_id = 0; bert_text = []; media_text = []
for topic in TOPICS:
    data = json.load(open("data/MediaFrameCorpus/{0}.json".format(topic)))
    for i, article in enumerate(data):
        text = data[article]['text']
        headline_frame = data[article]['headline_frame']
        primary_frame = data[article]['primary_frame']

        splits = text.split('\n\n')
        article_id = splits[0]
        headline = splits[2]
        text = " ".join(splits[3:])

        headline_tokens = tokenizer.encode(headline)
        bert_text.append(headline_tokens)
        media_text.append(headline)
        headline_id = single_id; single_id += 1

        text_tokens = tokenizer.encode(text)
        bert_text.append(text_tokens)
        media_text.append(text)
        text_id = single_id; single_id += 1

        has_headline.write("{0}\t{1}\n".format(article_id, headline_id))
        if headline_frame is not None:
            headline_frame = int(headline_frame)
        else:
            headline_frame = 0
        has_frame.write("{0}\t{1}\t{2}\n".format(article_id, headline_id, headline_frame))

        has_text.write("{0}\t{1}\n".format(article_id, text_id))
        if primary_frame is not None:
            primary_frame = int(primary_frame)
        else:
            primary_frame = 0
        has_frame.write("{0}\t{1}\t{2}\n".format(article_id, text_id, primary_frame))

has_headline.close()
has_text.close()
has_frame.close()

np.save("data/MediaFrameCorpus/media_text.npy", media_text)
np.save("data/MediaFrameCorpus/bert_encode.npy", bert_text)
