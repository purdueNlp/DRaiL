# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
from sklearn.metrics import *
import issues

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--global_learn", default=False, action="store_true", help="global learning")
    parser.add_argument("--lrate", help="learning rate", type=float, default=0.01)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    USER_INFO_F = "/scratch/pachecog/DRaiL/examples/debate/ZSL/data/user_info.json"
    ISSUES_DIR = "/scratch/pachecog/DRaiL/examples/debate/ZSL_issue/issues"
    GLOVE_F = "/scratch/pachecog/glove.840B.300d.txt"

    args = parse_arguments()

    use_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.global_learn:
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=use_gpu, gpu_index=args.gpu_index)
    else:
        learner=LocalLearner()

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    for i, test_issue in enumerate(issues.test_issues):
        dev_issue = issues.test_issues[(i + 1) % 10]
        train_issues = list(issues.all_issues - set([test_issue, dev_issue]))

        print("dev_issue: {0}, test_issue: {1}".format(dev_issue, test_issue))

        # Fast debugging
        #train_issues = train_issues[0:1]
        #print(train_issues)

        db.add_filters(filters=[
            ("HasIssue", "isDummy", "issueId_2", ["Stimulus Spending"]), # Least frequent
            ("HasIssue", "isTrain", "issueId_2", train_issues),
            ("HasIssue", "isDev", "issueId_2", [dev_issue]),
            ("HasIssue", "isTest", "issueId_2", [test_issue]),
            ("HasIssue", "allFolds", "issueId_2", list(issues.all_issues))])

        learner.build_feature_extractors(db,
                                         user_info_f=USER_INFO_F,
                                         issues_dir=ISSUES_DIR,
                                         embed_f=GLOVE_F,
                                         femodule_path=args.dir,
                                         debug=args.debug,
                                         filters=[("HasIssue", "isDummy", 1)])

        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=args.dir)

        if not args.global_learn:
            learner.train(db,
                          train_filters=[("HasIssue", "isTrain", 1)],
                          dev_filters=[("HasIssue", "isDev", 1)],
                          test_filters=[("HasIssue", "isTest", 1)],
                          scale_data=False)
            learner.extract_data(db, extract_test=True, test_filters=[("HasIssue", "isTest", 1)])
            del learner.fe
            res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)

        for pred in res.metrics:
            if pred in set(["HasStance"]):
                y_gold = res.metrics[pred]['gold_data']
                y_pred = res.metrics[pred]['pred_data']
                print classification_report(y_gold, y_pred, digits=4)
                macro_f1 = f1_score(y_gold, y_pred, average='macro')
                print "TEST macro f1", macro_f1
            learner.reset_metrics()

if __name__ == "__main__":
    main()
