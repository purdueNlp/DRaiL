# -*- coding: utf-8 -*-
import wikipedia
import os
import json

class SetDecoder(json.JSONDecoder):
    def default(self, obj):
        if isinstance(obj, list):
            return set(obj)
        return json.JSONDecoder.default(self, obj)

def load_issues(filenamecon, filenamepro):
	with open(filenamepro) as f:
		pro = json.load(f, cls=SetDecoder)
	with open(filenamecon) as f:
		con = json.load(f, cls=SetDecoder)
	stances = {}
	issues = set([])
	for issue in pro:
		issues.add(issue)
		for user in pro[issue]:
			if user not in stances:
				stances[user] = {}
			stances[user][issue] = 'pro'

		for user in con[issue]:
			if user not in stances:
				stances[user] = {}
			stances[user][issue] = 'con'
	return stances, issues


dataset_path = "/scratch/pachecog/DRaiL/examples/debate/ZSL"
bigissues, issues = load_issues(
    os.path.join(dataset_path, 'data/issues_con.json'),
    os.path.join(dataset_path, 'data/issues_pro.json'))

for issue in issues:
    issue = issue.replace(" ", "_")
    issue_file = "/scratch/pachecog/DRaiL/examples/debate/ZSL_issue/issues/" + issue + ".txt"
    exists = os.path.isfile(issue_file)

    if issue == "Stimulus_Spending":
        issue = "Stimulus_(economics)"
    elif issue == "Redistribution":
        issue = "Redistribution_of_income_and_wealth"
    elif issue == "Border_Fence":
        issue = "MexicoUnited_States_barrier"
    elif issue == "War_in_Afghanistan":
        issue = "War_in_Afghanistan_(2001–present)"
    elif issue == "Death_Penalty":
        issue = "Capital_punishment"
    try:
        p = wikipedia.summary(issue)
        with open(issue_file, "w") as fp:
            fp.write(p.encode('utf-8'))
    except wikipedia.exceptions.DisambiguationError as e:
        print "Error with", issue
        print str(e)

