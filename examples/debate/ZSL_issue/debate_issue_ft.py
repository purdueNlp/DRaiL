from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

from nltk import sent_tokenize, word_tokenize
import json
import os
import re
import numpy as np

class DebateIssue_ft(FeatureExtractor):

    def __init__(self, user_info_f, issues_dir, embed_f, debug=False):
        self.user_info_f = user_info_f
        self.issues_dir = issues_dir
        self.embed_f = embed_f
        np.random.seed(17477)

    def build(self):
        self.stance_idx = {'con': 0, 'pro': 1}

        # Gather user profile data
        with open(self.user_info_f) as f:
            self.user_info = json.load(f)

        # Gather issue information
        print("Processing issue files...")
        issue_text = {}; vocabulary = set([])
        for f in os.listdir(self.issues_dir):
            issue_name = re.sub("_", " ", f)[:-4]
            with open(os.path.join(self.issues_dir, f)) as fp:
                text = fp.read().decode('utf8')
                sentences = sent_tokenize(text)
                issue_text[issue_name] = []
                for s in sentences:
                    tokens = [w.lower() for w in word_tokenize(s)]
                    vocabulary |= set(tokens)
                    issue_text[issue_name].append(tokens)

        # Load pre-trained word embeddings
        if self.embed_f:
            num_words, embed_size, word_embed = utils.embeddings_dictionary_txt(self.embed_f, vocabulary=vocabulary)
            print(num_words, embed_size)

        # Create average vectors
        print("Creating issue vectors...")
        self.issue_vectors = {}
        for issue in issue_text:
            doc_embed = []
            for s in issue_text[issue]:
                words = []
                for word in s:
                    if word in word_embed:
                        words.append(word_embed[word])
                doc_embed.append(np.mean(words, axis=0))
            if len(doc_embed) == 0:
                doc_embed = [np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)]

            self.issue_vectors[issue] = doc_embed

    def issue_sequence_avg_glove(self, rule_grd):
        pred = rule_grd.get_body_predicates("Reports")[0]
        issue = pred['arguments'][1]
        return self.issue_vectors[issue]

    def user_profile(self, rule_grd):
        pred = rule_grd.get_body_predicates("Reports")[0]
        user = pred['arguments'][0]
        ret = []
        for feat in self.user_info['features']:
            temp = [0] * len(self.user_info['features'][feat])
            if user in self.user_info and feat in self.user_info[user]:
                index = self.user_info['features'][feat].index(self.user_info[user][feat])
                temp[index] = 1
            ret += temp
        return ret

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasStance':
            label = head['arguments'][2]
            return self.stance_idx[label]
