import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_subframe_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    args = parser.parse_args()

    has_frame = set([])
    with open(args.has_subframe_file) as fp:
        for line in fp:
            paragraph, subframe = line.strip().split()
            has_frame.add(paragraph)

    with open(args.output_file, "w") as fp:
        for paragraph in has_frame:
            fp.write("{}\n".format(paragraph))

if __name__ == "__main__":
    main()
