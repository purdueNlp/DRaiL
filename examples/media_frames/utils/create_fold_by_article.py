import random
import argparse
import json

def chunks(lst, n):
    k, m = divmod(len(lst), n)
    return (lst[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_paragraph_file', type=str, required=True)
    parser.add_argument('--output_json', type=str, required=True)
    parser.add_argument('--num_folds', type=int, required=True)
    args = parser.parse_args()

    all_articles = set([])
    with open(args.has_paragraph_file) as fp:
        for line in fp:
            article, paragraph = line.strip().split()
            all_articles.add(int(article))

    all_articles = list(all_articles)
    random.shuffle(all_articles)
    print("Num articles:", len(all_articles))

    folds = chunks(all_articles, args.num_folds)
    folds_dict = {}
    for i, fold in enumerate(folds):
        folds_dict[i] = fold
        print("Fold {}: {}".format(i, len(fold)))

    with open(args.output_json, "w") as fp:
        json.dump(folds_dict, fp)

if __name__ == "__main__":
    main()
