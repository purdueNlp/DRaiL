import argparse
from collections import Counter

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_ideology_file', type=str, required=True)
    args = parser.parse_args()

    ideologies = []
    with open(args.has_ideology_file) as fp:
        for line in fp:
            paragraph, ideology = line.strip().split()
            ideologies.append(ideology)


    print(Counter(ideologies))

if __name__ == "__main__":
    main()
