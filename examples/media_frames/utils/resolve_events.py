import argparse
from datetime import datetime, timedelta



def group_by_day_interval(dops, dop2articles, interval, output_file):
    start_date = list(sorted(list(dops)))[0]
    end_date = list(sorted(list(dops)))[-1]
    one_day_delta = timedelta(days=1)
    interval_delta = timedelta(days=interval)


    groups=[]
    while start_date <= end_date:
        group=[]
        temp_end=start_date+interval_delta

        while start_date < temp_end:
            if start_date in dop2articles:
                group+=list(dop2articles[start_date])
            start_date+=one_day_delta
        if len(group)!=0:
            groups.append(group)


    article2event={}
    event_no=70000
    for group in groups:
        for article in group:
            article2event[article]=event_no
        event_no+=1


    with open(output_file, "w") as out_file:
        for article in article2event:
            out_file.write(str(article)+" "+str(article2event[article])+"\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_dop_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument('--num_interval_days', type=int, required=True)
    args = parser.parse_args()

    dop2articles={}
    dops=set()

    f=open(args.has_dop_file, "r")
    while True:
        l=f.readline()
        if not l:
            break
        l=l.strip("\n").split(" ")

        if l[1]=='None':
            date_resolved=l[1]
        else:
            date_resolved=datetime.strptime(l[1], '%Y-%m-%d')
            dops.add(date_resolved)

        if date_resolved in dop2articles:
            dop2articles[date_resolved]=dop2articles[date_resolved] | set([int(l[0])])
        else:
            dop2articles[date_resolved]= set([int(l[0])])

    group_by_day_interval(dops, dop2articles, args.num_interval_days, args.output_file)


if __name__ == "__main__":
    main()