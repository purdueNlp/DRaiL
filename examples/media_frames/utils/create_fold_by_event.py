import random
import argparse
import json

def chunks(lst, n):
    k, m = divmod(len(lst), n)
    return (lst[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_event_file', type=str, required=True)
    parser.add_argument('--output_json', type=str, required=True)
    parser.add_argument('--num_folds', type=int, required=True)
    args = parser.parse_args()

    all_events = set([]); all_articles = set([])
    with open(args.has_event_file) as fp:
        for line in fp:
            article, event = line.strip().split()
            all_events.add(int(event))
            all_articles.add(article)

    all_events = list(all_events)
    random.shuffle(all_events)
    print("Num events:", len(all_events), "Num articles:", len(all_articles))

    folds = chunks(all_events, args.num_folds)
    folds_dict = {}
    for i, fold in enumerate(folds):
        folds_dict[i] = fold
        print("Fold {}: {}".format(i, len(fold)))

    with open(args.output_json, "w") as fp:
        json.dump(folds_dict, fp)

if __name__ == "__main__":
    main()
