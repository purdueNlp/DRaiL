import argparse
import json
import os
from scipy import stats
import numpy as np

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--output_dir', type=str, required=True)
    parser.add_argument('--has_frame_file', type=str, required=True)
    parser.add_argument('--has_paragraph_file', type=str, required=True)
    parser.add_argument('--has_event_file', type=str, required=True)
    parser.add_argument('--max_num_paragraphs', type=int, default=200)
    args = parser.parse_args()

    dictionary = {}
    article2event = {}
    paragraph2article = {}

    with open(args.has_event_file) as fp:
        for line in fp:
            article, event = line.strip().split()
            if event not in dictionary:
                dictionary[event] = {}
            if article not in dictionary[event]:
                dictionary[event][article] = {}
            article2event[article] = event

    with open(args.has_paragraph_file) as fp:
        for line in fp:
            article, paragraph = line.strip().split()
            if article not in article2event:
                continue
            event = article2event[article]
            dictionary[event][article][paragraph] = {}
            paragraph2article[paragraph] = article

    with open(args.has_frame_file) as fp:
        for line in fp:
            paragraph, frame = line.strip().split()
            if paragraph not in paragraph2article:
                continue
            article = paragraph2article[paragraph]
            event = article2event[article]
            dictionary[event][article][paragraph] = frame


    valid_events = set([])

    num_articles = []; num_paragraphs = []
    num_paragraphs_per_article = []
    for event in dictionary:
        num_articles.append(len(dictionary[event]))
        num_par = 0
        for article in dictionary[event]:
            num_paragraphs_per_article.append(len(dictionary[event][article]))
            for paragraph in dictionary[event][article]:
                for frame in dictionary[event][article][paragraph]:
                    pass
            num_par += len(dictionary[event][article])
        num_paragraphs.append(num_par)
        if num_par <= args.max_num_paragraphs:
            valid_events.add(event)

    with open(args.output_dir, "w") as fp:
        for event in valid_events:
            fp.write("{0}\n".format(event))

    print("Article stats")
    print(stats.describe(num_articles))
    print("Paragraph stats")
    print(stats.describe(num_paragraphs))
    #print(np.histogram(num_paragraphs, bins='auto'))
    plt.hist(num_paragraphs, bins=[0, 10, 20, 30, 40, 50, 60, 70, 80, 100, 200, 300, 400, 500, 600, 700])
    plt.title("No. of Events with X Paragraphs")
    plt.savefig("events_num_paragraphs.png")
    print("Paragraph per article stats")
    print(stats.describe(num_paragraphs_per_article))

if __name__ == "__main__":
    main()
