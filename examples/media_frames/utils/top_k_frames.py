import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--has_frame_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument('--k', type=int, required=True)
    args = parser.parse_args()

    has_frame = {}
    with open(args.has_frame_file) as fp:
        for line in fp:
            paragraph, frame, prob = line.strip().split()
            if paragraph not in has_frame:
                has_frame[paragraph] = []
            has_frame[paragraph].append((float(prob), frame))

    with open(args.output_file, "w") as fp:
        for paragraph in has_frame:
            frames = has_frame[paragraph]
            frames.sort(reverse=True)
            for i in range(0, 2):
                fp.write("{}\t{}\n".format(paragraph, frames[i][1]))

if __name__ == "__main__":
    main()
