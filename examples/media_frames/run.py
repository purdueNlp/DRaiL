# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
import logging.config

from sklearn.metrics import *

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.global_latent_learner import GlobalLatentLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="data directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index", default=None)
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--lrate", help="learning rate", type=float, default=1e-3)
    parser.add_argument('--delta', help='loss augmented inference', dest='delta', action='store_true', default=False)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--debug", default=False, action="store_true")
    parser.add_argument('-m', help='mode: [global|local|latent]', dest='mode', type=str, default='local')
    parser.add_argument('--loss', help='mode: [crf|hinge|joint|hinge_latent]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--logging_config', help='logging configuration file', type=str, default="logging_conf.json")
    parser.add_argument("--folds", help="folds to use", type=str, required=True)
    parser.add_argument("--embeddings", help="pre-trained embedding files", type=str)
    parser.add_argument("--bert_ids", help="preprocessed bert files", type=str, default=None)
    parser.add_argument("--start_fold", type=int, default=0)
    parser.add_argument("--end_fold", type=int, default=3)
    parser.add_argument('--drop_scores', help='drop scores', action='store_true', default=False)
    parser.add_argument("--output_scores", help="directory to save scores", type=str)
    args = parser.parse_args()
    return args

def get_data_splits(folds, i):
    train_events = []
    for j in range(0, 3):
        if j != i:
            train_events += folds[str(j)]
    test_events = folds[str(i)]

    dev_num = int(len(train_events) * 0.1)
    random.shuffle(train_events)
    dev_events = train_events[:dev_num]
    train_events = train_events[dev_num:]

    return train_events, dev_events, test_events

def get_predicate_metrics(predicate, predictions, logger, metric_all, metric, average):
    if not isinstance(predictions, dict):
        predictions = predictions.metrics
    #print(predictions.keys())

    if predicate in predictions:
        #print(predictions[predicate].keys())
        #exit()

        if len(predictions[predicate]['gold_data']) == 0:
            print("No predictions for predicate `{}`".format(predicate))
            return

        gold_data = predictions[predicate]['gold_data']
        pred_data = predictions[predicate]['pred_data']

        logger.info("{}".format(predicate))
        logger.info(classification_report(gold_data, pred_data))

        if metric == 'f1':
            metric_value = f1_score(gold_data, pred_data, average=average)
        elif metric == 'f1':
            metric_value = recall_score(gold_data, pred_data, average=average)
        else:
            print("metric not defined")
            exit(-1)

        logger.info("TEST {} {}: {}".format(metric, average, metric_value))
        logger.info(confusion_matrix(gold_data, pred_data))

        if predicate not in metric_all:
            metric_all[predicate] = []
        metric_all[predicate].append(metric_value)

def predict_in_parts(args, learner, test_events, batch_size, n_fold, FE_PATH, NE_PATH, predicates):
    predictions = {}
    for pred in predicates:
        predictions[pred] = {'gold_data': [], 'pred_data': []}

    for batch in range(0, len(test_events), batch_size):
        batch_articles = test_events[batch:batch+batch_size]
        db=learner.create_dataset(args.dir)

        # Add train-dev-test filters to drail database
        db.add_filters(filters=[
            ("HasEvent", "isTest", "eventId_2", batch_articles),
            ("HasEvent", "isDummy", "eventId_2", batch_articles[:10])
        ])

        learner.build_feature_extractors(db,
                                         filters=[("HasEvent", "isDummy", 1)],
                                         embeddings=args.embeddings,
                                         bert_encode=args.bert_ids,
                                         femodule_path=FE_PATH)

        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(n_fold)))
        learner.build_models(db, args.config, netmodules_path=NE_PATH)
        if args.continue_from_checkpoint:
            learner.init_models()

        learner.extract_data(db, extract_test=True, test_filters=[("HasEvent", "isTest", 1)])
        del learner.fe
        res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
        for pred in predicates:
            if pred in res.metrics:
                predictions[pred]['gold_data'] += res.metrics[pred]['gold_data']
                predictions[pred]['pred_data'] += res.metrics[pred]['pred_data']
        if args.drop_scores:
            learner.drop_scores(None, fold='test', output=os.path.join(args.output_scores, "scores_{}.csv".format(n_fold)), heads=heads)
        db.close_database()
    return predictions

def main():
    # Paths to neural networks and feature classes
    FE_PATH = "examples/media_frames/feat"
    NE_PATH = "examples/media_frames/neuro"
    optimizer = "SGD"

    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.mode == 'global':
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index,
                              loss_fn=args.lossfn, inference_limit=10)
    elif args.mode == 'latent':
        learner=GlobalLatentLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index,
                              inference_limit=10)
    else:
        learner=LocalLearner(inference_limit=10)

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    folds = json.load(open(args.folds))
    f1_all = {}; recall_all = {}

    for i in range(args.start_fold, args.end_fold):
        logger.info("Fold {}".format(i))
        train_events, dev_events, test_events = get_data_splits(folds, i)
        if args.debug:
            train_events = train_events[:5]
            dev_events = dev_events[:5]
            test_events = test_events[:5]

        if not args.infer_only:

            # Add train-dev-test filters to drail database
            db.add_filters(filters=[
                ("HasEvent", "isTrain", "eventId_2", train_events),
                ("HasEvent", "isDev", "eventId_2", dev_events),
                ("HasEvent", "isTest", "eventId_2", test_events),
                ("HasEvent", "isDummy", "eventId_2", train_events[:10])
            ])

            learner.build_feature_extractors(db,
                                             filters=[("HasEvent", "isDummy", 1)],
                                             embeddings=args.embeddings,
                                             bert_encode=args.bert_ids,
                                             femodule_path=FE_PATH)
            learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
            learner.build_models(db, args.config, netmodules_path=NE_PATH)

            if args.mode == "local":
                if args.continue_from_checkpoint:
                    learner.init_models()
                learner.train(db,
                        train_filters=[("HasEvent", "isTrain", 1)],
                        dev_filters=[("HasEvent", "isDev", 1)],
                        test_filters=[("HasEvent", "isTest", 1)],
                        scale_data=False,
                        optimizer=optimizer)
            else:
                '''
                learner.extract_data(
                        db,
                        train_filters=[("HasEvent", "isTrain", 1)],
                        dev_filters=[("HasEvent", "isDev", 1)],
                        test_filters=[("HasEvent", "isTest", 1)],
                        extract_train=not args.infer_only,
                        extract_dev=not args.infer_only,
                        extract_test=True)
                '''
                learner.extract_instances(
                        db,
                        train_filters=[("HasEvent", "isTrain", 1)],
                        dev_filters=[("HasEvent", "isDev", 1)],
                        test_filters=[("HasEvent", "isTest", 1)],
                        extract_train=not args.infer_only,
                        extract_dev=not args.infer_only,
                        extract_test=True)
                out = learner.train(
                        db,
                        train_filters=[("HasEvent", "isTrain", 1)],
                        dev_filters=[("HasEvent", "isDev", 1)],
                        test_filters=[("HasEvent", "isTest", 1)],
                        #opt_predicates=set(['HasIdeology']),
                        opt_predicate='HasIdeology',
                        loss_augmented_inference=args.delta,
                        continue_from_checkpoint=args.continue_from_checkpoint,
                        weight_classes=True,
                        patience=10,
                        train_only=args.train_only)
                if out:
                    res, heads = out
                    get_predicate_metrics("HasIdeology", res, logger, f1_all, metric='f1', average='macro')
        if not args.train_only and args.mode == 'local':
            predictions = predict_in_parts(args, learner, test_events, len(test_events), i, FE_PATH, NE_PATH, ['HasIdeology', 'HasSubframe'])
            get_predicate_metrics("HasIdeology", predictions, logger, f1_all, metric='f1', average='macro')
            #get_predicate_metrics("HasSubframe", predictions, logger, recall_all, metric='f1', average='binary')

    print(f1_all)
    print(recall_all)

if __name__ == "__main__":
    args = parse_arguments()
    # Reproducibility
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)
    # Logging
    logger = logging.getLogger()
    logging.config.dictConfig(json.load(open(args.logging_config)))

    main()
