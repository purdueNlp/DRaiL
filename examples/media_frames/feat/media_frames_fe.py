import re
import json
import random
import logging

import numpy as np

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils


class MediaFramesFE(FeatureExtractor):

    def __init__(self, embeddings, bert_encode):
        super(MediaFramesFE, self).__init__()
        self.logger = logging.getLogger(self.__class__.__name__)

        # resources
        self.embeddings = embeddings
        self.bert_encode = bert_encode

    def build(self):
        self.ideology_idx = {
            "0": 0, # Left
            "2": 1 # Right
        }

        # load embeddings
        with open(self.embeddings, "r") as in_file:
            [self.paragraph2embd, self.subframe2embd] = json.load(in_file)
        self.subframe_idx = {}
        for idx, sub in enumerate(self.subframe2embd):
            self.subframe_idx[sub] = idx
        if self.bert_encode:
            self.bert_encode = json.load(open(self.bert_encode))

    def paragraph_embedding(self, rule_grd):
        has_paragraph = rule_grd.get_body_predicates("HasParagraph")[0]
        article_id, paragraph_id = has_paragraph['arguments']
        emb = self.paragraph2embd[str(paragraph_id)]
        return emb

    def paragraph_bert(self, rule_grd):
        has_paragraph = rule_grd.get_body_predicates("HasParagraph")[0]
        article_id, paragraph_id = has_paragraph['arguments']
        out = self.bert_encode[str(paragraph_id)]
        return out['input_ids'], out['attention_mask'], out['token_type_ids']

    def subframe_1hot(self, rule_grd):
        subframe_id = rule_grd.get_body_predicates("FrameHasSubframe")[0]['arguments'][1]
        idx = self.subframe_idx[subframe_id]
        ret = [0] * len(self.subframe2embd)
        ret[idx] = 1
        return ret

    def subframe_embedding(self, rule_grd):
        subframe_id = rule_grd.get_body_predicates("FrameHasSubframe")[0]['arguments'][1]
        emb = self.subframe2embd[str(subframe_id)]
        return emb

    # This function is mandatory when using multiclass rule
    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasIdeology':
            label = head['arguments'][-1]
            return self.ideology_idx[label]
        else:
            self.logger.error("multiclass head not defined for predicate {}".format(head['name']))
            exit(-1)
