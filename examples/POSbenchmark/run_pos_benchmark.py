import sys
import os
import numpy as np
import torch
import optparse
from sklearn.metrics import *
import time

from drail.learn.local_learner import LocalLearner
from drail.learn.global_learner import GlobalLearner

#torch.cuda.set_device(0)

def get_fold_ids(_dir, fold):
    filter_ids = []
    curr_path = os.path.join(_dir, '{0}.txt'.format(fold))
    with open(curr_path) as f:
        for line in f:
            filter_ids.append(line.strip())
    return filter_ids

def main():
    parser = optparse.OptionParser()
    parser.add_option('-d', '--dir', help='directory', dest='dir', type='string')
    parser.add_option('-r', '--rule', help='rule file', dest='rules', type='string',
                       default='rule.dr')
    parser.add_option('--scores', help='test scores', dest='manual_scores', type='string')
    (opts, args) = parser.parse_args()

    mandatories = ['dir', 'manual_scores']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    rule_file = os.path.join(opts.dir, opts.rules)
    test_ids = get_fold_ids(opts.dir, 'test')

    learner = LocalLearner()

    print "Compiling rules..."
    learner.compile_rules(rule_file)

    print "Loading dataset..."
    db = learner.create_dataset(opts.dir)

    # add splits
    db.add_filters(filters=[
        ("Sentence", "isTest", "sentenceId", test_ids)
        ])

    print "Building feature extractors..."
    learner.build_feature_extractors(
            db, filters=[("Y", "isTest", 1)],
            manual_scores=opts.manual_scores,
            femodule_path=opts.dir)

    learner.extract_data(
                db,
                test_filters=[("Y", "isTest", 1)])
    res = learner.predict(
                db,
                fold='test', K=2,
                scmodule_path=opts.dir)

    y_gold = res.metrics['HasLabel']['gold_data']
    y_pred = res.metrics['HasLabel']['pred_data']

    # any sklearn metrics can be used over y_gold and y_pred
    acc_test = accuracy_score(y_gold, y_pred)
    print classification_report(y_gold, y_pred)
    print "TEST acc", acc_test
    print "Enc time", time.strftime("%H:%M:%S", time.gmtime(res.metrics['encoding_time'])), res.metrics['encoding_time']
    print "Sol time", time.strftime("%H:%M:%S", time.gmtime(res.metrics['solving_time'])), res.metrics['solving_time']
    learner.reset_metrics()

if __name__ == '__main__':
    main()
