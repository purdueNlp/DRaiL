import re
import json
import random
import logging
import h5py
import numpy as np

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

class PsychologyFE(FeatureExtractor):

    def __init__(self, predictions, rgcn_embeds, bert_embeds):
        super(PsychologyFE, self).__init__()
        self.logger = logging.getLogger(self.__class__.__name__)

        # resources
        self.predictions = predictions
        self.rgcn_embeds = rgcn_embeds
        self.bert_embeds = bert_embeds

        # maslow 1-hot
        self.maslow_idx = {
            "spiritual_growth": 0,
            "esteem": 1,
            "love": 2,
            "stability": 3,
            "physiological": 4
        }

        # reiss 1-hot
        self.reiss_idx = {
            'status': 0,
            'idealism': 1,
            'power': 2,
            'family': 3,
            'food': 4,
            'independence': 5,
            'belonging': 6,
            'competition': 7,
            'honor': 8,
            'romance': 9,
            'savings': 10,
            'contact': 11,
            'health': 12,
            'serenity': 13,
            'curiosity': 14,
            'approval': 15,
            'rest': 16,
            'tranquility': 17,
            'order': 18
        }

        # plutchik 1-hot
        self.plutchik_idx = {
            'disgust': 0,
            'surprise': 1,
            'anger': 2,
            'trust': 3,
            'sadness': 4,
            'anticipation': 5,
            'joy': 6,
            'fear': 7
        }

    def build(self):
        if self.predictions:
            with open(self.predictions) as fp:
                self.predictions = json.load(fp)
        self.rgcn_embeds = np.load(self.rgcn_embeds)
        self.bert_embeds = np.load(self.bert_embeds)

    def maslow_embedding(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        maslow = rule_grd.get_body_predicates("Maslow")[0]['arguments'][0]
        #print(rule_grd)
        #print(self.predictions[story])
        embed_idx = self.predictions[story][sent][char]['embedding_maslow']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def maslow_embedding_2(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[1]['arguments']
        maslow = rule_grd.get_body_predicates("Maslow")[1]['arguments'][0]
        #print(rule_grd)
        #print(self.predictions[story])
        embed_idx = self.predictions[story][sent][char]['embedding_maslow']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def reiss_embedding(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        reiss = rule_grd.get_body_predicates("Reiss")[0]['arguments'][0]
        embed_idx = self.predictions[story][sent][char]['embedding_reiss']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def reiss_embedding_2(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[1]['arguments']
        reiss = rule_grd.get_body_predicates("Reiss")[1]['arguments'][0]
        embed_idx = self.predictions[story][sent][char]['embedding_reiss']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def plutchik_embedding(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        plutchik = rule_grd.get_body_predicates("Plutchik")[0]['arguments'][0]
        embed_idx = self.predictions[story][sent][char]['embedding_plutchik']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def plutchik_embedding_2(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[1]['arguments']
        plutchik = rule_grd.get_body_predicates("Plutchik")[1]['arguments'][0]
        embed_idx = self.predictions[story][sent][char]['embedding_plutchik']
        #ret = np.concatenate((self.rgcn_embeds[embed_idx], self.bert_embeds[embed_idx]))
        ret = self.rgcn_embeds[embed_idx]
        return ret

    def reiss_1hot(self, rule_grd):
        vec = [0.0] * len(self.reiss_idx)
        pred = rule_grd.get_body_predicates("Reiss")[0]
        x = pred['arguments'][-1]
        vec[self.reiss_idx[x]] = 1.0
        return vec

    def reiss_1hot_2(self, rule_grd):
        vec = [0.0] * len(self.reiss_idx)
        pred = rule_grd.get_body_predicates("Reiss")[1]
        x = pred['arguments'][-1]
        vec[self.reiss_idx[x]] = 1.0
        return vec

    def maslow_1hot(self, rule_grd):
        vec = [0.0] * len(self.maslow_idx)
        pred = rule_grd.get_body_predicates("Maslow")[0]
        x = pred['arguments'][-1]
        vec[self.maslow_idx[x]] = 1.0
        return vec

    def maslow_1hot_2(self, rule_grd):
        vec = [0.0] * len(self.maslow_idx)
        pred = rule_grd.get_body_predicates("Maslow")[1]
        x = pred['arguments'][-1]
        vec[self.maslow_idx[x]] = 1.0
        return vec

    def plutchik_1hot(self, rule_grd):
        vec = [0.0] * len(self.plutchik_idx)
        pred = rule_grd.get_body_predicates("Plutchik")[0]
        x = pred['arguments'][-1]
        vec[self.plutchik_idx[x]] = 1.0
        return vec

    def plutchik_1hot_2(self, rule_grd):
        vec = [0.0] * len(self.plutchik_idx)
        pred = rule_grd.get_body_predicates("Plutchik")[1]
        x = pred['arguments'][-1]
        vec[self.plutchik_idx[x]] = 1.0
        return vec

