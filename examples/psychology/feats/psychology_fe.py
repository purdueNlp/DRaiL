import re
import json
import random
import logging
import h5py
import numpy as np

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils

class PsychologyFE(FeatureExtractor):

    def __init__(self, input_f=None, node_info_f=None, predictions=None):
        super(PsychologyFE, self).__init__()
        self.logger = logging.getLogger(self.__class__.__name__)

        # resources
        self.predictions = predictions
        self.input_f = input_f
        self.node_info_f = node_info_f


        # maslow 1-hot
        self.maslow_idx = {
            "spiritual_growth": 0,
            "esteem": 1,
            "love": 2,
            "stability": 3,
            "physiological": 4
        }

        # reiss 1-hot
        self.reiss_idx = {
            'status': 0,
            'idealism': 1,
            'power': 2,
            'family': 3,
            'food': 4,
            'independence': 5,
            'belonging': 6,
            'competition': 7,
            'honor': 8,
            'romance': 9,
            'savings': 10,
            'contact': 11,
            'health': 12,
            'serenity': 13,
            'curiosity': 14,
            'approval': 15,
            'rest': 16,
            'tranquility': 17,
            'order': 18
        }

        # plutchik 1-hot
        self.plutchik_idx = {
            'disgust': 0,
            'surprise': 1,
            'anger': 2,
            'trust': 3,
            'sadness': 4,
            'anticipation': 5,
            'joy': 6,
            'fear': 7
        }

    def build(self):
        if self.predictions:
            with open(self.predictions) as fp:
                self.predictions = json.load(fp)
        if self.input_f:
            self.fp = h5py.File(self.input_f, 'r')
        if self.node_info_f:
            self.node_info = self.load_node_info(self.node_info_f)

    def load_node_info(self, fpath):
        node_info = {}
        with open(fpath, 'r') as fr:
            for line in fr:
                if line == "\n":
                    continue
                info = json.loads(line)
                node_info[info['sid']] = info['node_info']
        return node_info

    def get_node_id(self, story, sent, char):
        story_info = self.node_info[story]
        for elem in story_info:
            elem_char = elem['character'].replace(' ', '_')
            if elem['sent_idx'] == sent and elem_char == char:
                return elem['nid']

    def psych_features(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        if rule_grd.has_body_predicate("Maslow"):
            task = "maslow"
        elif rule_grd.has_body_predicate("Reiss"):
            task = "reiss"
        elif rule_grd.has_body_predicate("Plutchik"):
            task = "plutchik"
        else:
            print("Need to have a valid task on LHS")
            exit(-1)

        input_ids = self.fp[story][task]['input_ids'][:]
        attention_mask = self.fp[story][task]['input_mask'][:]
        token_type_ids = self.fp[story][task]['token_type_ids'][:]
        edge_src = self.fp[story]['edge_src'][:]
        edge_dest = self.fp[story]['edge_dest'][:]
        edge_types = self.fp[story]['edge_types'][:]
        edge_norms = self.fp[story]['edge_norms'][:]
        node_id = self.get_node_id(story, int(sent), char)
        if node_id is None:
            print(story, sent, char)
            exit()
        return (input_ids, attention_mask, token_type_ids,
                edge_src, edge_dest, edge_types, edge_norms, [node_id])
    
    def psych_features_2(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        story_2, sent_2, char_2 = rule_grd.get_body_predicates("HasSentChar")[1]['arguments']
        if rule_grd.has_body_predicate("Maslow"):
            task = "maslow"
        elif rule_grd.has_body_predicate("Reiss"):
            task = "reiss"
        elif rule_grd.has_body_predicate("Plutchik"):
            task = "plutchik"
        else:
            print("Need to have a valid task on LHS")
            exit(-1)

        input_ids = self.fp[story][task]['input_ids'][:]
        attention_mask = self.fp[story][task]['input_mask'][:]
        token_type_ids = self.fp[story][task]['token_type_ids'][:]
        edge_src = self.fp[story]['edge_src'][:]
        edge_dest = self.fp[story]['edge_dest'][:]
        edge_types = self.fp[story]['edge_types'][:]
        edge_norms = self.fp[story]['edge_norms'][:]
        node_id = self.get_node_id(story, int(sent), char)
        node_id_2 = self.get_node_id(story_2, int(sent_2), char_2)
        return (input_ids, attention_mask, token_type_ids,
                edge_src, edge_dest, edge_types, edge_norms, [node_id], [node_id_2])

    def reiss_indicator(self, rule_grd):
        pred = rule_grd.get_body_predicates("Reiss")[0]
        x = pred['arguments'][-1]
        return [self.reiss_idx[x]]

    def maslow_indicator(self, rule_grd):
        pred = rule_grd.get_body_predicates("Maslow")[0]
        x = pred['arguments'][-1]
        return [self.maslow_idx[x]]

    def plutchik_indicator(self, rule_grd):
        pred = rule_grd.get_body_predicates("Plutchik")[0]
        x = pred['arguments'][-1]
        return [self.plutchik_idx[x]]

    def reiss_indicator_2(self, rule_grd):
        pred = rule_grd.get_body_predicates("Reiss")[1]
        x = pred['arguments'][-1]
        return [self.reiss_idx[x]]

    def maslow_indicator_2(self, rule_grd):
        pred = rule_grd.get_body_predicates("Maslow")[1]
        x = pred['arguments'][-1]
        return [self.maslow_idx[x]]

    def plutchik_indicator_2(self, rule_grd):
        pred = rule_grd.get_body_predicates("Plutchik")[1]
        x = pred['arguments'][-1]
        return [self.plutchik_idx[x]]

    def maslow_prob(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        maslow = rule_grd.get_body_predicates("Maslow")[0]['arguments'][0]
        #print(rule_grd)
        #print(self.predictions[story])
        return self.predictions[story][sent][char]['predicted_maslow'][maslow]

    def reiss_prob(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        reiss = rule_grd.get_body_predicates("Reiss")[0]['arguments'][0]
        return self.predictions[story][sent][char]['predicted_reiss'][reiss]

    def plutchik_prob(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        plutchik = rule_grd.get_body_predicates("Plutchik")[0]['arguments'][0]
        return self.predictions[story][sent][char]['predicted_plutchik'][plutchik]

    def plutchik_prob_2(self, rule_grd):
        story, sent, char = rule_grd.get_body_predicates("HasSentChar")[0]['arguments']
        plutchik = rule_grd.get_body_predicates("Plutchik")[1]['arguments'][0]
        return self.predictions[story][sent][char]['predicted_plutchik'][plutchik]

    def get_unobs_predicates(self, rule_grd, name):
        preds = []
        if rule_grd.has_body_predicate(name):
            preds += rule_grd.get_body_predicates(name)
        if rule_grd.has_head_predicate(name):
            preds.append(rule_grd.get_head_predicate())
        return preds

    def maslow_unobs(self, rule_grd):
        # includes two bits to signal positive / negative predicate
        maslow_vec = [0.0] * len(self.maslow_idx) + [0.0] * 2
        pred = self.get_unobs_predicates(rule_grd, "HasMaslow")[0]
        if pred['isneg']:
            maslow_vec[-1] = 1.0
        else:
            maslow_vec[-2] = 1.0
        maslow = pred['arguments'][-1]
        maslow_vec[self.maslow_idx[maslow]] = 1.0
        return maslow_vec

    def reiss_unobs(self, rule_grd):
        # includes two bits to signal positive / negative predicate
        reiss_vec = [0.0] * len(self.reiss_idx) + [0.0] * 2
        pred = self.get_unobs_predicates(rule_grd, "HasReiss")[0]
        if pred['isneg']:
            reiss_vec[-1] = 1.0
        else:
            reiss_vec[-2] = 1.0
        reiss = pred['arguments'][-1]
        reiss_vec[self.reiss_idx[reiss]] = 1.0
        return reiss_vec

    def plutchik_unobs(self, rule_grd):
        # includes two bits to signal positive / negative predicate
        plutchik_vec = [0.0] * len(self.plutchik_idx) + [0.0] * 2
        pred = self.get_unobs_predicates(rule_grd, "HasPlutchik")[0]
        if pred['isneg']:
            plutchik_vec[-1] = 1.0
        else:
            plutchik_vec[-2] = 1.0
        plutchik = pred['arguments'][-1]
        plutchik_vec[self.plutchik_idx[plutchik]] = 1.0
        return plutchik_vec

    def reiss_1hot(self, rule_grd):
        vec = [0.0] * len(self.reiss_idx)
        pred = rule_grd.get_body_predicates("Reiss")[0]
        x = pred['arguments'][-1]
        vec[self.reiss_idx[x]] = 1.0
        return vec

    def maslow_1hot(self, rule_grd):
        vec = [0.0] * len(self.maslow_idx)
        pred = rule_grd.get_body_predicates("Maslow")[0]
        x = pred['arguments'][-1]
        vec[self.maslow_idx[x]] = 1.0
        return vec

    def plutchik_1hot(self, rule_grd):
        vec = [0.0] * len(self.plutchik_idx)
        pred = rule_grd.get_body_predicates("Plutchik")[0]
        x = pred['arguments'][-1]
        vec[self.plutchik_idx[x]] = 1.0
        return vec

