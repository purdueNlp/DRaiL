# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
import logging.config

from sklearn.metrics import *

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.global_latent_learner import GlobalLatentLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="data directory", type=str, required=True)
    parser.add_argument("--global_loss", help="global_loss", type=str)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--lrate", help="learning rate", type=float, default=1e-3)
    parser.add_argument('--delta', help='loss augmented inference', dest='delta', action='store_true', default=False)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--pred_batch", default=500, type=int)
    parser.add_argument("--extract_embeds_only", help="extract embeds only", default=False, action="store_true")
    parser.add_argument("--debug", default=False, action="store_true")
    parser.add_argument('--logging_config', help='logging configuration file', type=str, default="logging_conf.json")
    parser.add_argument("--folds", help="folds to use", type=str, required=True)
    parser.add_argument("--predictions", help="predictions", type=str)
    parser.add_argument("--local_vers", default=False, action='store_true')
    parser.add_argument("--tune_neg", default=False, action='store_true')
    args = parser.parse_args()
    return args

def get_predicate_metrics(predicate, predictions, logger, metric_all, metric, average):
    if not isinstance(predictions, dict):
        predictions = predictions.metrics
    #print(predictions.keys())

    if predicate in predictions:
        #print(predictions[predicate].keys())
        #exit()

        if len(predictions[predicate]['gold_data']) == 0:
            print("No predictions for predicate `{}`".format(predicate))
            return

        gold_data = predictions[predicate]['gold_data']
        pred_data = predictions[predicate]['pred_data']

        logger.info("{}".format(predicate))
        logger.info(classification_report(gold_data, pred_data, digits=6))

        if metric == 'f1':
            metric_value = f1_score(gold_data, pred_data, average=average)
        elif metric == 'recall':
            metric_value = recall_score(gold_data, pred_data, average=average)
        elif metric == 'precision':
            metric_value = precision_score(gold_data, pred_data, average=average)
        else:
            print("metric not defined")
            exit(-1)

        logger.info("TEST {} {}: {}".format(metric, average, metric_value))

        if predicate not in metric_all:
            metric_all[predicate] = []
        metric_all[predicate].append(metric_value)

def predict_in_parts(args, learner, test_stories, batch_size, FE_PATH, NE_PATH, predicates):
    predictions = {}
    for pred in predicates:
        predictions[pred] = {'gold_data': [], 'pred_data': []}

    for batch in range(0, len(test_stories), batch_size):
        batch_stories = test_stories[batch:batch+batch_size]
        db=learner.create_dataset(args.dir)

        # Add train-dev-test filters to drail database
        db.add_filters(filters=[
            ("HasSentChar", "isTest", "storyId_1", batch_stories),
            ("HasSentChar", "isDummy", "storyId_1", batch_stories[:1])])

        learner.build_feature_extractors(db,
            filters=[("HasSentChar", "isDummy", 1)],
            predictions=args.predictions,
            rgcn_embeds=args.rgcn_embed,
            bert_embeds=args.bert_embed,
            #input_f=args.input_f,
            #node_info_f=args.node_info_f,
            femodule_path=FE_PATH)

        learner.build_models(db, args.config, netmodules_path=NE_PATH)
        learner.build_models(db, args.config, netmodules_path=NE_PATH)
        if args.continue_from_checkpoint:
            learner.init_models()

        learner.extract_data(db, extract_test=True, test_filters=[("HasSentChar", "isTest", 1)])
        del learner.fe
        res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
        for pred in predicates:
            if pred in res.metrics:
                predictions[pred]['gold_data'] += res.metrics[pred]['gold_data']
                predictions[pred]['pred_data'] += res.metrics[pred]['pred_data']
        db.close_database()
    return predictions



def main():
    # Paths to neural networks and feature classes
    FE_PATH = "examples/psychology/feats"
    NE_PATH = "examples/psychology/neuro"
    SC_PATH = "examples/psychology/score"
    optimizer = "SGD"

    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    #if not args.infer_only:
    if not args.local_vers:
        learner=GlobalLearner(learning_rate=args.lrate, use_gpu=(args.gpu_index is not None), gpu_index=args.gpu_index,
                              loss_fn=args.global_loss, inference_limit=10)
    else:
        learner=LocalLearner(inference_limit=10)

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    folds = json.load(open(args.folds))
    f1_all = {}; precision_all = {}; recall_all = {}

    train_stories = folds['train']
    dev_stories = folds['dev']
    test_stories = folds['test']
    all_stories = train_stories + dev_stories + test_stories

    print("train stories:", len(train_stories), "dev stories", len(dev_stories), "test_stories", len(test_stories))

    if args.debug:
        train_stories = train_stories[:5]
        dev_stories = dev_stories[:5]
        test_stories = test_stories[:5]
        all_stories = all_stories[:15]

    # Add train-dev-test filters to drail database
    db.add_filters(filters=[
        ("HasSentChar", "isTrain", "storyId_1", train_stories),
        ("HasSentChar", "isDev", "storyId_1", dev_stories),
        ("HasSentChar", "isTest", "storyId_1", test_stories),
        ("HasSentChar", "isDummy", "storyId_1", train_stories[:1]),
        ("HasSentChar", "allFolds", "storyId_1", all_stories)])

    learner.build_feature_extractors(db,
            filters=[("HasSentChar", "isDummy", 1)],
            predictions=args.predictions,
            rgcn_embeds=args.rgcn_embed,
            bert_embeds=args.bert_embed,
            #input_f=args.input_f,
            #node_info_f=args.node_info_f,
            femodule_path=FE_PATH)

    learner.set_savedir(args.savedir)
    learner.build_models(db, args.config, netmodules_path=NE_PATH)
    if not args.local_vers:
        learner.extract_data(
                db,
                train_filters=[("HasSentChar", "isTrain", 1)],
                dev_filters=[("HasSentChar", "isDev", 1)],
                test_filters=[("HasSentChar", "isTest", 1)],
                extract_train=not args.infer_only,
                extract_dev=not args.infer_only,
                extract_test=not args.train_only)
        '''
        learner.extract_instances(
                db,
                train_filters=[("HasSentChar", "isTrain", 1)],
                dev_filters=[("HasSentChar", "isDev", 1)],
                test_filters=[("HasSentChar", "isTest", 1)],
                extract_train=not args.infer_only,
                extract_dev=not args.infer_only,
                extract_test=True)
        '''
        out = learner.train(
                db,
                train_filters=[("HasSentChar", "isTrain", 1)],
                dev_filters=[("HasSentChar", "isDev", 1)],
                test_filters=[("HasSentChar", "isTest", 1)],
                loss_augmented_inference=args.delta,
                continue_from_checkpoint=args.continue_from_checkpoint,
                weight_classes=True,
                patience=5,
                train_only=args.train_only,
                inference_only=args.infer_only,
                optimizer=optimizer)
        predictions, heads = out
    else:
        if args.continue_from_checkpoint:
            learner.init_models()

        if not args.infer_only and not args.extract_embeds_only:
            learner.train(
                db,
                train_filters=[("HasSentChar", "isTrain", 1)],
                dev_filters=[("HasSentChar", "isDev", 1)],
                test_filters=[("HasSentChar", "isTest", 1)],
                scale_data=False,
                optimizer=optimizer
            )
        if not args.train_only and not args.extract_embeds_only:
            '''
            learner.binary_classifiers.add("HasMaslow")
            learner.binary_classifiers.add("HasReiss")
            learner.binary_classifiers.add("HasPlutchik")
            '''
            #learner.extract_data(db, extract_test=True, test_filters=[("HasSentChar", "isTest", 1)])
            #out = learner.predict(None, fold='test', get_predicates=True, scale_data=False, scmodule_path=SC_PATH)
            #out = learner.predict(None, fold='test', get_predicates=True, scale_data=False)
            if not args.tune_neg:
                predictions = predict_in_parts(args, learner, test_stories, args.pred_batch, FE_PATH, NE_PATH, ['HasMaslow', 'HasReiss', 'HasPlutchik'])
            else:
                predictions = predict_in_parts(args, learner, dev_stories, len(dev_stories), FE_PATH, NE_PATH, ['HasMaslow', 'HasReiss', 'HasPlutchik'])

        if args.extract_embeds_only:
            learner.extract_data(db, extract_test=True, test_filters=[("HasSentChar", "allFolds", 1)])
            all_embs = learner.extract_rule_embeddings(None, fold='test')
            with open("examples/psychology/data/rule_embeddings.json", "w") as fp:
                json.dump(all_embs, fp)

    if not args.train_only and not args.extract_embeds_only:
        # Precision
        #get_predicate_metrics("HasMaslow", res, logger, f1_all, metric='precision', average='binary')
        #get_predicate_metrics("HasReiss", res, logger, f1_all, metric='precision', average='binary')
        #get_predicate_metrics("HasPlutchik", res, logger, f1_all, metric='precision', average='binary')
        # Recall
        #get_predicate_metrics("HasMaslow", res, logger, recall_all, metric='recall', average='binary')
        #get_predicate_metrics("HasReiss", res, logger, recall_all, metric='recall', average='binary')
        #get_predicate_metrics("HasPlutchik", res, logger, recall_all, metric='recall', average='binary')
        # F1
        get_predicate_metrics("HasMaslow", predictions, logger, f1_all, metric='f1', average='binary')
        get_predicate_metrics("HasReiss", predictions, logger, f1_all, metric='f1', average='macro')
        get_predicate_metrics("HasPlutchik", predictions, logger, f1_all, metric='f1', average='macro')

if __name__ == "__main__":
    args = parse_arguments()
    #args.input_f = "examples/psychology/data/features_merge.h5"
    #args.node_info_f = "examples/psychology/data/node_info_merge.json"

    args.rgcn_embed = "examples/psychology/data/rgcn_embeds_v2.npy"
    args.bert_embed = "examples/psychology/data/bert_embeds_v2.npy"

    # Reproducibility
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)
    # Logging
    logger = logging.getLogger()
    logging.config.dictConfig(json.load(open(args.logging_config)))
    main()

