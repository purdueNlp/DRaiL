import h5py
import json
import glob

DIR = "data/feature_pngv4_roberta_base_160_pointplutchik/"

with open('data/node_info_merge.json', "w") as fp:
    for jsonname in glob.glob('{}*.json'.format(DIR)):
        with open(jsonname) as curr_fp:
            for line in curr_fp:
                fp.write(line)
                fp.write('\n')

with h5py.File('data/features_merge.h5',mode='w') as h5fw:
    for h5name in glob.glob('{}*.h5'.format(DIR)):
        h5fr = h5py.File(h5name,'r')
        for obj in h5fr.keys():
            h5fr.copy(obj, h5fw)

