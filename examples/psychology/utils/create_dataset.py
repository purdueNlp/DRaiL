import json
import argparse
import os

def update_predictions(predictions_all, elem, psych_type, psych_name):
    #print(elem)
    #exit()
    if elem['sid'] not in predictions_all:
        predictions_all[elem['sid']] = {}
    if elem['sent_idx'] not in predictions_all[elem['sid']]:
        predictions_all[elem['sid']][elem['sent_idx']] = {}
    if elem['character'].replace(' ', '_') not in predictions_all[elem['sid']][elem['sent_idx']]:
        predictions_all[elem['sid']][elem['sent_idx']][elem['character'].replace(' ', '_')] = {}
    if psych_type not in predictions_all[elem['sid']][elem['sent_idx']][elem['character'].replace(' ', '_')]:
        predictions_all[elem['sid']][elem['sent_idx']][elem['character'].replace(' ', '_')][psych_type] = {}
    predictions_all[elem['sid']][elem['sent_idx']][elem['character'].replace(' ', '_')][psych_type][psych_name.replace(' ', '_')] = elem[psych_type][psych_name]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True)
    args = parser.parse_args()

    has_sent_char = open("data/has_sent_char.txt", "w")

    has_label = {
        'maslow': open("data/has_maslow.txt", "w"),
        'reiss': open("data/has_reiss.txt", "w"),
        'plutchik': open("data/has_plutchik.txt", "w")
    }

    elems = {
        'maslow': set([]),
        'reiss': set([]),
        'plutchik': set([])
    }

    predictions_all = {}
    folds = {'train': set([]), 'dev': set([]), 'test': set([])}
    for filename in os.listdir(args.input_dir):
        with open(os.path.join(args.input_dir, filename, "predictions.json")) as fp:
            for line in fp:
                elem = json.loads(line)
                _, label, fold, _ = filename.split('_')

                folds[fold].add(elem['sid'])
                has_sent_char.write("{0}\t{1}\t{2}\n".format(
                    elem['sid'],
                    elem['sent_idx'],
                    elem['character'].replace(' ', '_')
                    )
                )

                for x in elem[label]:
                    x = x.replace(' ', '_')
                    has_label[label].write("{0}\t{1}\t{2}\t{3}\n".format(
                        elem['sid'],
                        elem['sent_idx'],
                        elem['character'].replace(' ', '_'),
                        x
                        )
                    )
                for x in elem['predicted_{}'.format(label)]:
                    update_predictions(predictions_all, elem, 'predicted_{}'.format(label), x)
                    x = x.replace(' ', '_')
                    elems[label].add(x)

    # Convert back to lists to be able to dump json
    for split in folds:
        folds[split] = list(folds[split])

    has_sent_char.close()
    for label in has_label:
        has_label[label].close()

    maslow = open("data/maslow.txt", "w")
    reiss = open("data/reiss.txt", "w")
    plutchik = open("data/plutchik.txt", "w")

    for m in elems['maslow']:
        maslow.write("{0}\n".format(m))

    for r in elems['reiss']:
        reiss.write("{0}\n".format(r))

    for p in elems['plutchik']:
        plutchik.write("{0}\n".format(p))

    maslow.close()
    reiss.close()
    plutchik.close()

    with open("data/folds.json", "w") as fp:
        json.dump(folds, fp)

    with open("data/predictions_all.json", "w") as fp:
        json.dump(predictions_all, fp)

if __name__ == "__main__":
    main()
