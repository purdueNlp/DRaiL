import json
from tqdm import tqdm
import traceback
import numpy as np

predictions_all = json.load(open("data/predictions_all.json"))

rule_embeds = json.load(open("data/rule_embeddings.json"))

rgcn_embeds = []; bert_embeds = []
embedding_index = 0


pbar = tqdm(total=len(rule_embeds))
for rule in rule_embeds:
    try:
        has_sent_char, label_type = rule.split(' & ')
        _, values = has_sent_char[:-1].split("(", 1)
        story_idx, sent_idx, character = values.split(",", 2)
        label_name, _ = label_type[:-1].split("(")
        label_name = label_name.lower()

        predictions_all[story_idx][sent_idx][character]['embedding_{}'.format(label_name)] = embedding_index
        rgcn_embeds.append(rule_embeds[rule][0])
        bert_embeds.append(rule_embeds[rule][1])
        embedding_index += 1
    except:
        print(rule)
        print(traceback.format_exc())
        exit()
    pbar.update(1)
pbar.close()

with open("data/predictions_all_expanded.json", "w") as fp:
    json.dump(predictions_all, fp)

rgcn_embeds = np.array(rgcn_embeds)
bert_embeds = np.array(bert_embeds)

print(rgcn_embeds.shape, bert_embeds.shape)
np.save("data/rgcn_embeds.npy", rgcn_embeds)
np.save("data/bert_embeds.npy", bert_embeds)
