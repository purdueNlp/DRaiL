import json
import argparse
import os
import numpy as np
from collections import Counter

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True)
    args = parser.parse_args()

    gold = {}; output_space = {'maslow': set([]), 'reiss': set([]), 'plutchik': set([])}
    folds = {'train': set([]), 'dev': set([]), 'test': set([])}
    for filename in os.listdir(args.input_dir):
        # calculate correlation only on training set
        if not filename.startswith('pred_train'):
            continue

        with open(os.path.join(args.input_dir, filename, "predictions.json")) as fp:
            for line in fp:

                elem = json.loads(line)
                label = filename.split('_')[-1]

                key = "{}_{}_{}".format(elem['sid'], elem['sent_ids'], elem['character'])
                if key not in gold:
                    gold[key] = {'maslow': [], 'reiss': [], 'plutchik': []}

                for x in elem[label]:
                    gold[key][label].append(x)

                    output_space[label].add(x)

    maslow_counts = []
    for key in gold:
        maslow_counts.append(len(gold[key]['maslow']))

    print("Total:", len(maslow_counts))
    print(Counter(maslow_counts))

if __name__ == "__main__":
    main()
