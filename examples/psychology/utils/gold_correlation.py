import json
import argparse
import os
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import seaborn as sn
import matplotlib.pyplot as plt


def corr(df1, df2):
    n = len(df1)
    v1, v2 = df1.values, df2.values
    sums = np.multiply.outer(v2.sum(0), v1.sum(0))
    stds = np.multiply.outer(v2.std(0), v1.std(0))
    return pd.DataFrame((v2.T.dot(v1) - sums / n) / stds / n,
            df2.columns, df1.columns)

def get_correlation(type_1, type_2, output_space, observations):
    variables_1 = {}; variables_2 = {}
    for var in output_space[type_1]:
        variables_1[var] = []
    for var in output_space[type_2]:
        variables_2[var] = []

    for key in observations:
        for var in variables_1:
            if var in observations[key][type_1]:
                variables_1[var].append(1)
            else:
                variables_1[var].append(0)

        for var in variables_2:
            if var in observations[key][type_2]:
                variables_2[var].append(1)
            else:
                variables_2[var].append(0)


    df1 = pd.DataFrame(variables_1, columns=variables_1.keys())
    df2 = pd.DataFrame(variables_2, columns=variables_2.keys())
    corrMatrix = corr(df1, df2)
    return corrMatrix

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, required=True)
    parser.add_argument("--var1", type=str, required=True)
    parser.add_argument("--var2", type=str, required=True)
    parser.add_argument("--output", type=str, required=True)
    parser.add_argument("--annot", action="store_true", default=False)
    args = parser.parse_args()

    gold = {}; output_space = {'maslow': set([]), 'reiss': set([]), 'plutchik': set([])}
    folds = {'train': set([]), 'dev': set([]), 'test': set([])}
    for filename in os.listdir(args.input_dir):
        # calculate correlation only on training set
        if not filename.startswith('pred_train'):
            continue

        with open(os.path.join(args.input_dir, filename, "predictions.json")) as fp:
            for line in fp:

                elem = json.loads(line)
                label = filename.split('_')[-1]

                key = "{}_{}_{}".format(elem['sid'], elem['sent_ids'], elem['character'])
                if key not in gold:
                    gold[key] = {'maslow': [], 'reiss': [], 'plutchik': []}

                for x in elem[label]:
                    gold[key][label].append(x)

                    output_space[label].add(x)




    #print(gold)
    #print(output_space)
    corrMatrix = get_correlation(args.var1, args.var2, output_space, gold)
    sn.heatmap(corrMatrix, annot=args.annot)
    plt.plot()
    plt.savefig(args.output)

if __name__ == "__main__":
    main()
