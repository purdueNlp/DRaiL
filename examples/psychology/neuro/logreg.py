import logging

import torch
import numpy as np
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks


class LogReg(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(LogReg, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.layer1 = torch.nn.Linear(self.config['n_input'], self.output_dim)

        if self.use_gpu:
            self.layer1 = self.layer1.cuda()

    def forward(self, x):
        # Receives just one input
        inputs = self._get_float_tensor(x['vector'])
        logits = self.layer1(inputs)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
