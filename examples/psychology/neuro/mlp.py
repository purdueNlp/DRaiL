import logging

import torch
import numpy as np
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks


class MLP(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(MLP, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        if "shared_node" in self.config:
            name = self.config["shared_node"]
            self.n_input_node = shared_params[name]["nin"]
            self.n_hidden_node = shared_params[name]["nout"]
            self.node2hidden = shared_params[name]["layer"]
        else:
            self.n_input_node = self.config["n_input_node"]
            self.n_hidden_node = self.config["n_hidden_node"]
            self.node2hidden = torch.nn.Linear(self.config['n_input_node'], self.config['n_hidden_node'])

        if "shared_labl" in self.config:
            name = self.config["shared_labl"]
            self.n_input_labl = shared_params[name]["nin"]
            self.n_hidden_labl = shared_params[name]["nout"]
            self.labl2hidden = shared_params[name]["layer"]
        else:
            self.n_input_labl = self.config["n_input_labl"]
            self.n_hidden_labl = self.config["n_hidden_labl"]
            self.labl2hidden = torch.nn.Linear(self.config['n_input_labl'], self.config['n_hidden_labl'])

        self.concat2hidden = torch.nn.Linear(self.n_hidden_node + self.n_hidden_labl, self.config['n_hidden_concat'])
        self.classifier = torch.nn.Linear(self.config['n_hidden_concat'], self.output_dim)

    def forward(self, x):
        # Get node input
        node_feats = self._get_float_tensor([elem[0] for elem in x['input']])
        # Get label input
        labl_feats = self._get_float_tensor([elem[1] for elem in x['input']])

        node_embed = self.node2hidden(node_feats)
        node_embed = F.relu(node_embed)

        labl_embed = self.labl2hidden(labl_feats)
        labl_embed = F.relu(labl_embed)

        concat = torch.cat([node_embed, labl_embed], 1)
        concat = self.concat2hidden(concat)
        concat = F.relu(concat)

        logits = self.classifier(concat)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

