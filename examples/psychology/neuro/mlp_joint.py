import logging

import torch
import numpy as np
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks


class MLP(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(MLP, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        if "shared_node" in self.config:
            name = self.config["shared_node"]
            self.n_input_node = shared_params[name]["nin"]
            self.n_hidden_node = shared_params[name]["nout"]
            self.node2hidden = shared_params[name]["layer"]
        else:
            self.n_input_node = self.config["n_input_node"]
            self.n_hidden_node = self.config["n_hidden_node"]
            self.node2hidden = torch.nn.Linear(self.config['n_input_node'], self.config['n_hidden_node'])

        if "shared_labl" in self.config:
            name = self.config["shared_labl"]
            self.n_input_labl = shared_params[name]["nin"]
            self.n_hidden_labl = shared_params[name]["nout"]
            self.labl2hidden = shared_params[name]["layer"]
        else:
            self.n_input_labl = self.config["n_input_labl"]
            self.n_hidden_labl = self.config["n_hidden_labl"]
            self.labl2hidden = torch.nn.Linear(self.config['n_input_labl'], self.config['n_hidden_labl'])
        self.concat2hidden = torch.nn.Linear(self.n_hidden_node * 2 + self.n_hidden_labl * 2, self.config['n_hidden_concat'])
        self.classifier = torch.nn.Linear(self.config['n_hidden_concat'], self.output_dim)

    def forward(self, x):
        # Get node input
        node_feats_1 = self._get_float_tensor([elem[0] for elem in x['input']])
        node_feats_2 = self._get_float_tensor([elem[1] for elem in x['input']])
        # Get label input
        labl_feats_1 = self._get_float_tensor([elem[2] for elem in x['input']])
        labl_feats_2 = self._get_float_tensor([elem[3] for elem in x['input']])

        node_embed_1 = self.node2hidden(node_feats_1)
        node_embed_1 = F.relu(node_embed_1)
        node_embed_2 = self.node2hidden(node_feats_2)
        node_embed_2 = F.relu(node_embed_2)

        labl_embed_1 = self.labl2hidden(labl_feats_1)
        labl_embed_1 = F.relu(labl_embed_1)
        labl_embed_2 = self.labl2hidden(labl_feats_2)
        labl_embed_2 = F.relu(labl_embed_2)

        concat = torch.cat([node_embed_1, node_embed_2, labl_embed_1, labl_embed_2], 1)
        concat = self.concat2hidden(concat)
        concat = F.relu(concat)

        logits = self.classifier(concat)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

