import os
import sys
import json
import time
import math
import random
import logging
from copy import deepcopy

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import dgl
from dgl import DGLGraph
from dgl.data import MiniGCDataset
import dgl.function as fn
from dgl.nn.pytorch import RelGraphConv
from dgl.nn.pytorch import utils as dgl_utils
from transformers import BertModel, AutoModel

from drail.neuro.nn_model import NeuralNetworks

class RGCNLayer(nn.Module):
    def __init__(self, in_feat, out_feat, num_rels,
                 bias=True, activation=None, self_loop=False,
                 dropout=0.2, use_gate=False):
        super(RGCNLayer, self).__init__()
        self.in_feat = in_feat
        self.out_feat = out_feat
        self.num_rels = num_rels
        self.bias = bias
        self.activation = activation
        self.self_loop = self_loop
        self.use_gate = use_gate

        # relation weights
        self.weight = nn.Parameter(torch.Tensor(self.num_rels, self.in_feat, self.out_feat))
        nn.init.xavier_uniform_(self.weight, gain=nn.init.calculate_gain('relu'))

        # message func
        self.message_func = self.basis_message_func

        # bias
        if self.bias:
            self.h_bias = nn.Parameter(torch.Tensor(out_feat))
            nn.init.zeros_(self.h_bias)

        # weight for self loop
        if self.self_loop:
            self.loop_weight = nn.Parameter(torch.Tensor(in_feat, out_feat))
            #logger.info('loop_weight: {}'.format(self.loop_weight.shape))
            nn.init.xavier_uniform_(self.loop_weight,
                                    gain=nn.init.calculate_gain('relu'))

        self.dropout = nn.Dropout(dropout)

        if self.use_gate:
            self.gate_weight = nn.Parameter(torch.Tensor(self.num_rels, self.in_feat, 1))
            nn.init.xavier_uniform_(self.gate_weight, gain=nn.init.calculate_gain('sigmoid'))

    def basis_message_func(self, edges):
        msg = dgl_utils.bmm_maybe_select(edges.src['h'], self.weight, edges.data['rel_type'])
        if 'norm' in edges.data:
            msg = msg * edges.data['norm']
        if self.use_gate:
            gate = dgl_utils.bmm_maybe_select(edges.src['h'], self.gate_weight, edges.data['rel_type']).reshape(-1, 1)
            gate = torch.sigmoid(gate)
            msg = msg * gate
        return {'msg': msg}

    def forward(self, g):
        assert g.is_homograph(), \
            "not a homograph; convert it with to_homo and pass in the edge type as argument"
        with g.local_scope():
            if self.self_loop:
                loop_message = dgl_utils.matmul_maybe_select(g.ndata['h'], self.loop_weight)

            # message passing
            g.update_all(self.message_func, fn.sum(msg='msg', out='h'))

            # apply bias and activation
            node_repr = g.ndata['h']
            if self.bias:
                node_repr = node_repr + self.h_bias
            if self.self_loop:
                node_repr = node_repr + loop_message
            if self.activation:
                node_repr = self.activation(node_repr)
            node_repr = self.dropout(node_repr)
            return node_repr


class RGCNModel(nn.Module):
    def __init__(self, in_dim, h_dim, num_rels,
                 num_hidden_layers=2, dropout=0.2,
                 use_self_loop=True, use_gate=True):
        super(RGCNModel, self).__init__()
        self.in_dim = in_dim
        self.h_dim = h_dim
        self.num_rels = num_rels
        self.num_hidden_layers = num_hidden_layers
        self.dropout = dropout
        self.use_self_loop = use_self_loop
        self.use_gate = use_gate

        # create rgcn layers
        self.build_model()

    def build_model(self):
        self.layers = nn.ModuleList()
        for idx in range(self.num_hidden_layers):
            h2h = self.build_hidden_layer(idx)
            self.layers.append(h2h)

    def build_hidden_layer(self, idx):
        act = F.relu if idx < self.num_hidden_layers - 1 else None
        in_dim = self.in_dim if idx == 0 else self.h_dim
        #self.logger.debug('RGCN Layer {}: {}, {}, {}'.format(idx, in_dim, self.h_dim, act))
        return RGCNLayer(in_dim, self.h_dim, self.num_rels,
                            activation=act, self_loop=True,
                            dropout=self.dropout, use_gate=self.use_gate)

    def forward(self, g):
        for layer in self.layers:
            h = layer(g)
            g.ndata['h'] = h
        return h

class TwoLayerClassifier(nn.Module):
    def __init__(self, in_dim, h1_dim, h2_dim, n_classes, dropout=0.2):
        super(TwoLayerClassifier, self).__init__()
        self.in_dim = in_dim
        self.h1_dim = h1_dim
        self.h2_dim = h2_dim
        self.n_classes = n_classes

        self.d1 = nn.Dropout(dropout)
        self.d2 = nn.Dropout(dropout)
        self.d3 = nn.Dropout(dropout)

        self.l1 = nn.Linear(self.in_dim, self.h1_dim)
        self.l2 = nn.Linear(self.h1_dim, self.h2_dim)
        self.lout = nn.Linear(self.h2_dim, self.n_classes)
        nn.init.xavier_uniform_(self.l1.weight, gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self.l2.weight, gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self.lout.weight, gain=nn.init.calculate_gain('sigmoid'))

    def forward(self, h_e):
        h_1 = F.relu(self.l1(self.d1(h_e)))
        h_2 = F.relu(self.l2(self.d2(h_1)))
        out = self.lout(self.d3(h_2))
        return out

class BaseNaivePsychologyModel(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BaseNaivePsychologyModel, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def forward(*pargs):
        raise NotImplementedError('{}'.format(self.__class__.__name__))

    @classmethod
    def from_pretrained(cls, model_dir, strict=False):
        fpath = os.path.join(model_dir, 'model_config.json')
        model_config = json.load(open(fpath))
        model = cls(**model_config)
        fpath = os.path.join(model_dir, 'model.pt')
        model.load_state_dict(torch.load(fpath,
                                         map_location='cpu'),
                              strict=strict)
        return model

    def save_pretrained(self, save_dir):
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
        self.save_config(save_dir)
        fpath = os.path.join(save_dir, 'model.pt')
        model_to_save = self.module if hasattr(self, 'module') else self
        torch.save(model_to_save.state_dict(), fpath)

    def save_config(self, save_dir):
        raise NotImplementedError('{}'.format(self.__class__.__name__))

    def average_words(self, word_emb, attention_mask):
        word_emb[attention_mask == 0] = 0
        avg = word_emb.sum(1) / attention_mask.sum(1).unsqueeze(1)
        return avg

    def lstm_forward(self, word_emb, attention_mask, lstm_layer, pad_total_length=False):
        # LSTM packed forward
        orig_len = word_emb.shape[1] if pad_total_length else None
        unsorted_seq_lengths = attention_mask.sum(1)
        seq_lengths, sorted_idx = unsorted_seq_lengths.sort(0, descending=True)
        _, unsorted_idx = torch.sort(sorted_idx, dim=0)
        sorted_word_emb = word_emb.index_select(0, sorted_idx)

        packed_input = pack_padded_sequence(sorted_word_emb, seq_lengths, batch_first=True)
        packed_out, (rnn_h, rnn_c) = lstm_layer(packed_input)
        unpacked_out, input_sizes = pad_packed_sequence(packed_out, batch_first=True, total_length=orig_len)

        unsorted_out = unpacked_out.index_select(0, unsorted_idx)
        return unsorted_out, unsorted_seq_lengths

class RGCNNaivePsychology(BaseNaivePsychologyModel):
    '''Version 2
    '''
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(RGCNNaivePsychology, self).__init__(config, nn_id, use_gpu, output_dim)

        self.n_hidden_layers = self.config['n_hidden_layers']
        self.freeze_lm = self.config['freeze_lm']
        self.weight_name = self.config['weight_name']
        self.n_classes = self.config['n_classes']
        self.dropout = self.config['dropout']
        self.n_rtypes = self.config['n_rtypes']
        self.use_gate = self.config['use_gate']
        self.use_rgcn = self.config['use_rgcn']
        self.reg_param = self.config['reg_param']

    def build_architecture(self, rule_template, fe, shared_params={}):
        # language model
        self.lm = AutoModel.from_pretrained(
            self.weight_name,
            cache_dir=None
        )
        self.lm_dim = self.lm.config.hidden_size
        if self.freeze_lm:
            for p in self.lm.parameters():
                p.requires_grad = False

        # rgcn
        self.rgcn = RGCNModel(self.lm_dim, self.lm_dim, self.n_rtypes,
                              num_hidden_layers=self.n_hidden_layers,
                              dropout=self.dropout,
                              use_gate=self.use_gate)

        # classifier
        self.h1_dim = self.lm_dim // 2
        self.h2_dim = self.h1_dim // 2
        self.classifier = TwoLayerClassifier(
            self.lm_dim, self.h1_dim, self.h2_dim, self.n_classes,
            self.dropout
        )

    def get_embedding_dim(self):
        return self.lm_dim

    def save_config(self, save_dir):
        fpath = os.path.join(save_dir, 'model_config.json')
        config = {
            'weight_name': self.weight_name,
            'n_classes': self.n_classes,
            'dropout': self.dropout,
            'freeze_lm': self.freeze_lm,
            'n_rtypes': self.n_rtypes,
            'n_hidden_layers': self.n_hidden_layers,
            'use_gate': self.use_gate,
            'use_rgcn': self.use_rgcn,
            'reg_param': self.reg_param
        }
        json.dump(config, open(fpath, 'w'))

    def get_embeddings(self, x):
        # instantiate
        input_ids = []; attention_mask = []; token_type_ids = []
        edge_src = []; edge_dest = []; edge_types = []; edge_norms = []
        node_ids = []

        for elem in x['input']:
            input_ids.append(self._get_long_tensor(elem[0][0]))
            attention_mask.append(self._get_long_tensor(elem[0][1]))
            token_type_ids.append(self._get_long_tensor(elem[0][2]))
            edge_src.append(self._get_long_tensor(elem[0][3]))
            edge_dest.append(self._get_long_tensor(elem[0][4]))
            edge_types.append(self._get_long_tensor(elem[0][5]))
            edge_norms.append(self._get_long_tensor(elem[0][6]))
            node_ids.append(elem[0][7][0])

        n_instances = [ii.shape[0] for ii in input_ids]

        # language model
        input_ids = torch.cat(input_ids, dim=0)
        attention_mask = torch.cat(attention_mask, dim=0)
        token_type_ids = torch.cat(token_type_ids, dim=0)
        lm_output = self.lm(
            input_ids, attention_mask, token_type_ids=token_type_ids)
        pooled = lm_output[1]
        node_feat = [pooled[sum(n_instances[:i]): sum(n_instances[:i+1])] for i in range(len(n_instances))]

        # build graphs
        gs = []
        for i in range(len(n_instances)):
            g = dgl.DGLGraph()
            g.add_nodes(n_instances[i])
            if edge_src[i].nelement() != 0:
                g.add_edges(edge_src[i], edge_dest[i])

            g.ndata['h'] = node_feat[i]

            g.edata.update(
                {
                    'rel_type': edge_types[i],
                    'norm': edge_norms[i].unsqueeze(1)
                }
            )
            gs.append(g)

        bg = dgl.batch(gs)

        # rgcn forward
        self.rgcn(bg)

        rgcn_embs = []
        for g in dgl.unbatch(bg):
            rgcn_embs.append(g.ndata['h'])

        bert_embs = node_feat

        rgcn_embs_ret = []; bert_embs_ret = []

        for i, (r, b) in enumerate(zip(rgcn_embs, bert_embs)):
            rgcn_embs_ret.append(r[node_ids[i]])
            bert_embs_ret.append(b[node_ids[i]])
        rgcn_embs_ret = torch.stack(rgcn_embs_ret)
        bert_embs_ret = torch.stack(bert_embs_ret)

        return rgcn_embs_ret, bert_embs_ret

    def forward(self, x):
        # instantiate
        input_ids = []; attention_mask = []; token_type_ids = []
        edge_src = []; edge_dest = []; edge_types = []; edge_norms = []
        node_ids = []; task_ids = []

        for elem in x['input']:
            input_ids.append(self._get_long_tensor(elem[0][0]))
            attention_mask.append(self._get_long_tensor(elem[0][1]))
            token_type_ids.append(self._get_long_tensor(elem[0][2]))
            edge_src.append(self._get_long_tensor(elem[0][3]))
            edge_dest.append(self._get_long_tensor(elem[0][4]))
            edge_types.append(self._get_long_tensor(elem[0][5]))
            edge_norms.append(self._get_long_tensor(elem[0][6]))
            node_ids.append(elem[0][7][0])
            task_ids.append(elem[1][0])

        #print("Reached forward function")
        n_instances = [ii.shape[0] for ii in input_ids]

        # Get the offsets for the node_ids
        offset = 0
        for i in range(0, len(n_instances)):
            node_ids[i] += offset
            offset += n_instances[i]

		# language model
        input_ids = torch.cat(input_ids, dim=0)
        attention_mask = torch.cat(attention_mask, dim=0)
        token_type_ids = torch.cat(token_type_ids, dim=0)
        lm_output = self.lm(
            input_ids, attention_mask, token_type_ids=token_type_ids)
        pooled = lm_output[1]
        node_feat = [pooled[sum(n_instances[:i]): sum(n_instances[:i+1])] for i in range(len(n_instances))]

        # build graphs
        gs = []
        for i in range(len(n_instances)):
            g = dgl.DGLGraph()
            g.add_nodes(n_instances[i])
            if edge_src[i].nelement() != 0:
                g.add_edges(edge_src[i], edge_dest[i])

            g.ndata['h'] = node_feat[i]

            g.edata.update(
                {
                    'rel_type': edge_types[i],
                    'norm': edge_norms[i].unsqueeze(1)
                }
            )
            gs.append(g)

        bg = dgl.batch(gs)

        # rgcn forward
        self.rgcn(bg)

        emb = bg.ndata['h']

        # output layer
        logits = self.classifier(emb)
        score = torch.sigmoid(logits)
        logits = score[node_ids,task_ids]
        score = score[node_ids,task_ids]
        return logits, score

