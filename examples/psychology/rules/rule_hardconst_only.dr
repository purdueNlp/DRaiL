// Entities
entity: "Story", arguments: ["storyId"::ArgumentType.UniqueString];
entity: "Sentence", arguments: ["sentenceId"::ArgumentType.UniqueString];
entity: "Character", arguments: ["characterId"::ArgumentType.UniqueString];
entity: "MaslowNeed", arguments: ["maslowId"::ArgumentType.UniqueString];
entity: "ReissMotive", arguments: ["reissId"::ArgumentType.UniqueString];
entity: "PlutchikEmotion", arguments: ["plutchikId"::ArgumentType.UniqueString];
entity: "Sentiment", arguments: ["sentimentId"::ArgumentType.UniqueString];

// Relations
predicate: "HasSentChar", arguments: [Story, Sentence, Character];
predicate: "HasMaslow", arguments: [Story, Sentence, Character, MaslowNeed];
predicate: "HasReiss", arguments: [Story, Sentence, Character, ReissMotive];
predicate: "HasPlutchik", arguments: [Story, Sentence, Character, PlutchikEmotion];
predicate: "Maslow", arguments: [MaslowNeed];
predicate: "Reiss", arguments: [ReissMotive];
predicate: "ReissClass", arguments: [ReissMotive, MaslowNeed];
predicate: "Plutchik", arguments: [PlutchikEmotion];
predicate: "PlutchikSentiment", arguments: [PlutchikEmotion, Sentiment];

// Load data
load: "HasSentChar", file: "has_sent_char.txt";
load: "HasMaslow", file: "has_maslow.txt";
load: "HasReiss", file: "has_reiss.txt";
load: "HasPlutchik", file: "has_plutchik.txt";
load: "Maslow", file: "maslow.txt";
load: "Reiss", file: "reiss.txt";
load: "ReissClass", file: "reiss_class.txt";
load: "Plutchik", file: "plutchik.txt";
load: "PlutchikSentiment", file: "plutchik_class.txt";

// Feature classes
femodule: "psychology_fe";
feclass: "PsychologyFE";

// Scoring classes
scmodule: "psychology_sc";

ruleset {
    // Emissions
    rule: HasSentChar(S, T, C) & Maslow(M) => HasMaslow(S, T, C, M)^?,
    lambda: 1.0,
    score: "class_prob",
    fefunctions: [
      vector("maslow_prob")
    ],
    target: S;

    rule: HasSentChar(S, T, C) & Reiss(R) => HasReiss(S, T, C, R)^?,
    lambda: 1.0,
    score: "class_prob",
    fefunctions: [
      vector("reiss_prob")
    ],
    target: S;

    rule: HasSentChar(S, T, C) & Plutchik(P) => HasPlutchik(S, T, C, P)^?,
    lambda: 1.0,
    score: "class_prob",
    fefunctions: [
      vector("plutchik_prob")
    ],
    target: S;

    // Reiss-Maslow alignment
    hardconstr: HasSentChar(S, T, C) & ReissClass(R, N) & Maslow(M) & (M != N) & HasMaslow(S, T, C, M)^? => ~HasReiss(S, T, C, R)^?;
    hardconstr: HasSentChar(S, T, C) & ReissClass(R, N) & Maslow(M) & (M != N) & HasReiss(S, T, C, R)^? => ~HasMaslow(S, T, C, M)^?;

    // No more than one maslow
    hardconstr: HasSentChar(S, T, C) & Maslow(M) & Maslow(N) & (M != N) & HasMaslow(S, T, C, M)^? => ~HasMaslow(S, T, C, N)^?;

    // Plutchik-Plutchik alignment
    hardconstr: HasSentChar(S, T, C) & PlutchikSentiment(P, "positive") & PlutchikSentiment(Q, "negative") & HasPlutchik(S, T, C, P)^? => ~HasPlutchik(S, T, C, Q)^?;
    hardconstr: HasSentChar(S, T, C) & PlutchikSentiment(P, "positive") & PlutchikSentiment(Q, "negative") & HasPlutchik(S, T, C, Q)^? => ~HasPlutchik(S, T, C, P)^?;

} groupby: HasSentChar.1;
