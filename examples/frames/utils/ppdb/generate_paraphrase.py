import pandas as pd
from pytorch_transformers import BertTokenizer
import random
import numpy as np

sentences = {}; n_sentence = 0
bert_text = []

paraphrases = []

tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)

with open("data/ppdb/ppdb-2.0-s-all") as fp:
    for line in fp:
        parts = line.split('|||')
        s1 = parts[1].strip()
        s2 = parts[2].strip()

        if s1 not in sentences:
            sentences[s1] = n_sentence
            n1 = n_sentence
            n_sentence += 1
        if s2 not in sentences:
            sentences[s2] = n_sentence
            n2 = n_sentence
            n_sentence += 1

        s1_bert = tokenizer.encode(s1)
        s2_bert = tokenizer.encode(s2)

        bert_text.append(s1_bert)
        bert_text.append(s2_bert)

        paraphrases.append((n1, n2, 1.0))

print("Done.")
print("Number of sentences: {0}".format(n_sentence))

bert_text = np.array(bert_text, dtype=object)
np.save("data/ppdb/bert_paraphrase_encode_small.npy", bert_text)

print("Generating negative examples....")

neg_paraphrases = []
for (n1, n2, _) in paraphrases:
    m1 = random.randint(0, n_sentence)
    m2 = random.randint(0, n_sentence)
    neg_paraphrases.append((n1, m1, 0.0))
    neg_paraphrases.append((n2, m2, 0.0))

with open("data/ppdb/paraphrases_small.txt", "w") as fp:
    for (a, b, c) in paraphrases:
        fp.write("{0}\t{1}\t{2}\n".format(a, b, c))
    for (a, b, c) in neg_paraphrases:
        fp.write("{0}\t{1}\t{2}\n".format(a, b, c))

print("Done.")

in_instance = open("data/ppdb/in_instance_small.txt", "w")
instance_id = 0

with open("data/ppdb/paraphrases_small.txt") as fp:
    for line in fp:
        s1, s2, _ = line.strip().split('\t')
        in_instance.write("{0}\t{1}\t{2}\n".format(s1, s2, instance_id))
        instance_id += 1

in_instance.close()

