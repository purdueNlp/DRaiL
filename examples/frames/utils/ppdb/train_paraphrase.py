import numpy as np
from nn_bert_paraphrase import BertClassifier
import sys
import random
from pytorch_transformers import AdamW, WarmupLinearSchedule
import torch
from sklearn.metrics import *
import time
import json
import progressbar
import argparse

random.seed(1234)
torch.manual_seed(1234)

parser = argparse.ArgumentParser()
parser.add_argument("--gpu_index",  type=int, help="gpu index")
parser.add_argument("--config", help="config file", type=str, required=True)
parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
args = parser.parse_args()

BERT_ENCODING_F = "/scratch1/pachecog/DRaiL/examples/frames/data/bert_paraphrase_encode_small.npy"
PARAPHRASE = "/scratch1/pachecog/DRaiL/examples/frames/data/paraphrases_small.txt"
MAX_SENT = 33
CONFIG = json.load(open(args.config))['models'][0]

torch.cuda.set_device(args.gpu_index)

bert_encoding = np.load(BERT_ENCODING_F)
bert_encoding = np.array(bert_encoding)
print("Bert encodings", bert_encoding.shape)

def measure(y, y_, measurement, average, pos_label):
    if measurement == "acc":
        acc = accuracy_score(y, y_.cpu())
        #print "accuracy_score", acc
        return acc
    elif measurement == "f1":
        f1 = f1_score(y, y_.cpu(), average = average, pos_label= pos_label)
        #print "f1_score", f1
        return f1

def get_model_params(model):
    n_batchsize = model.config["batch_size"]
    patience = model.config["patience"]
    learning_rate = model.config["learning_rate"]
    n_predsize = None

    # defaults. TO-DO add defaults for the rest of the params
    measurement = "acc"; average = "binary"; weighted_examples=False
    pos_label = 1; sort=False; sort_by=None; output='softmax'
    weight_decay=0.0; adam_epsilon=1e-8; max_grad_norm = 1.0;
    clipping = False; balance_classes = False

    if "measurement" in model.config:
        measurement = model.config["measurement"]
    if "average" in model.config:
        average = model.config["average"]
    if "weighted_examples" in model.config:
        weighted_examples = model.config["weighted_examples"]
    print_stat = measurement
    if "print_stat" in model.config:
        print_stat = model.config['print_stat']
    if "pos_label" in model.config:
        pos_label = int(model.config["pos_label"])
    if "sort_sequence" in model.config:
        sort=True; sort_by=model.config["sort_by"]
    if "output" in model.config:
        output=model.config["output"]
    if "weight_decay" in model.config:
         weight_decay = model.config["weight_decay"]
    if "adam_epsilon" in model.config:
        adam_epsilon = model.config["adam_epsilon"]
	if "max_grad_norm" in model.config:
		max_grad_norm = model.config["max_grad_norm"]
    if "clipping" in model.config:
        clipping = model.config["clipping"]
    if "balance_classes" in model.config:
        balance_classes = model.config["balance_classes"]
    return (n_batchsize, patience, n_predsize, measurement, average,
            weighted_examples, pos_label, sort, sort_by, print_stat,
            output, weight_decay, learning_rate, adam_epsilon,
            max_grad_norm, clipping, balance_classes)

def pad_and_mask(vector, max_seq_len):
    pad_token = 0
    mask = [1] * len(vector)
    if len(vector) < max_seq_len:
        vector += [pad_token] * (max_seq_len - len(vector))
        mask += [0] * (max_seq_len - len(mask))
    return (vector, mask)

def get_features(batch):
    CLS = 101
    SEP = 102
    X = {'input': []}; Y = []
    for s1, s2, label in batch:
        s1 = [CLS] + bert_encoding[int(s1)] + [SEP]
        s2 = bert_encoding[int(s2)] + [SEP]

        X['input'].append((pad_and_mask(s1, MAX_SENT + 2), pad_and_mask(s2, MAX_SENT + 1)))
        Y.append(int(label))
    return X, Y

paraphrases = []; labels = {'0': 0, '1': 0}
with open(PARAPHRASE) as fp:
    for line in fp:
        s1, s2, label = line.strip().split('\t')
        paraphrases.append((s1,s2,label))
        labels[label] += 1
print(len(paraphrases))
print(labels)

random.shuffle(paraphrases)
n = len(paraphrases)

n_train = int(n * 0.8)
n_dev = int(n * 0.2)

train = paraphrases[:n_train]
dev = paraphrases[n_train:]
n_train = len(train)
n_dev = len(dev)

print("train:", len(train), "dev:", len(dev))

model = BertClassifier(CONFIG, 0, True, 2)
model.build_architecture()

n_batchsize, patience, n_predsize, measurement, average, \
weighted_examples, pos_label, sort, sort_by, \
print_stat, output, weight_decay, learning_rate, \
adam_epsilon, max_grad_norm, clipping, balance_classes = \
	get_model_params(model)

no_decay = ['bias', 'LayerNorm.weight']

optimizer_grouped_parameters = [
    {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
    {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
                                ]
optimizer = AdamW(optimizer_grouped_parameters, lr=learning_rate, eps=adam_epsilon)
t_total = n_train * 3.0; warmup_steps=0
scheduler = WarmupLinearSchedule(optimizer, warmup_steps=warmup_steps, t_total=t_total)

class_weights = torch.from_numpy(np.array([2.0, 1.0])).float()
if model.use_gpu:
	class_weights = class_weights.cuda()

loss_fn = torch.nn.CrossEntropyLoss(weight=class_weights)
print("class_weights", class_weights)

epoch = 0
done_looping = False
best_val_measure = -float("infinity")
patience_counter = 0

def evaluate(model, dev, n_dev, measurement, average, pos_label, loss_fn, n_batches):
    print("Evaluating...")
    bar = progressbar.ProgressBar(maxval=n_batches, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    random.shuffle(dev)
    eval_measure_sum = 0; eval_loss_sum = 0; eval_batches = 0
    model.eval()
    batch_index = 0
    bar.start()
    while batch_index < n_dev:
        dev_batch = dev[batch_index:batch_index+n_batchsize]
        batch_index += n_batchsize

        DevX, DevY = get_features(dev_batch)
        out = model(DevX)
        logits = out[0]; probas = out[1]
        _, y_pred_batch = torch.max(probas, 1)
        eval_measure_sum += measure(DevY, y_pred_batch, measurement, average, pos_label)
        eval_loss = model.pytorch_loss(probas, DevY, loss_fn, 'softmax')
        eval_loss_sum += eval_loss.item()

        eval_batches += 1
        bar.update(eval_batches)
        if eval_batches >= n_batches:
            break
    return eval_measure_sum, eval_loss_sum, eval_batches


print("Training...")
bar = progressbar.ProgressBar(maxval=10000, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
while (not done_looping):
    epoch += 1
    batch_index = 0
    train_loss_sum = 0

    epoch_start_time = time.time()
    model.train()
    train_batches = 0; train_measure_sum = 0
    while batch_index < n_train:
        model.zero_grad()
        train_batch = train[batch_index:batch_index+n_batchsize]
        batch_index += n_batchsize

        TrainX, TrainY = get_features(train_batch)
        out = model(TrainX)
        logits = out[0]; probas = out[1]

        _, train_Y_pred = torch.max(probas, 1)

        train_loss = model.pytorch_loss(probas, TrainY, loss_fn, 'softmax')

        train_loss_sum += train_loss.item()
        #train_measure_sum += measure(TrainY, train_Y_pred, measurement, average, pos_label)
        train_loss.backward()

        if clipping:
            torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)

        if scheduler is not None:
			scheduler.step()

        optimizer.step()

        if train_batches % 10000 == 0 and train_batches > 0:
            epoch_end_time = time.time()
            eval_measure_sum, eval_loss_sum, eval_batches = \
                evaluate(model, dev, n_dev, measurement, average, pos_label, loss_fn, 1000)

            measure_end_time = time.time()
            train_loss = train_loss_sum / (1.0 * train_batches)
            train_measure = train_measure_sum / (1.0 * train_batches)
            val_loss = eval_loss_sum / (1.0 * eval_batches)
            val_measure = eval_measure_sum / (1.0 * eval_batches)

            print "epoch", epoch, "batch", train_batches, "train_loss", train_loss, \
                  "val_loss", val_loss, "val_measure", val_measure, \
                  "time {:.3f}s".format(measure_end_time - epoch_start_time)
            epoch_start_time = time.time()

            if val_measure > best_val_measure:
                patience_counter = 0
                best_val_measure = val_measure
                best_epoch = epoch
                model_state = model.state_dict()
                torch.save(model_state, args.savedir)
            else:
                patience_counter += 1
                if patience_counter >= patience:
                    done_looping = True
                    break
            print("Training...")
            bar = progressbar.ProgressBar(maxval=train_batches+10000, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
            bar.start()

        train_batches += 1
        if train_batches % 100:
            bar.update(train_batches)

