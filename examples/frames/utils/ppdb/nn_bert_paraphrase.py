import torch
import numpy as np
import torch.nn.functional as F
from pytorch_transformers import BertModel, BertConfig

torch.manual_seed(1234)

class BertClassifier(torch.nn.Module):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__()
        self.config = config
        self.nn_id = nn_id
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        print("output_dim", output_dim)
        self.CLS = 101
        self.SEP = 102

    def build_architecture(self):
        bert_config = BertConfig.from_pretrained("bert-base-uncased")
        self.bert_model = BertModel.from_pretrained("bert-base-uncased")
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)
        self.hidden2label = torch.nn.Linear(bert_config.hidden_size, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def _get_variable(self, tensor):
        var = torch.autograd.Variable(tensor)
        if self.use_gpu:
            var = var.cuda()
        return var

    def _get_float_tensor(self, inp):
        if self.use_gpu:
            return torch.cuda.FloatTensor(inp)
        return torch.FloatTensor(inp)

    def _get_long_tensor(self, inp):
        if self.use_gpu:
            return torch.cuda.LongTensor(inp)
        return torch.LongTensor(inp)

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []
        if len(x['input'][0]) == 2:
            for elem in x['input']:
                input_ids.append(elem[0][0] + elem[1][0])
                input_mask.append(elem[0][1] + elem[1][1])
                segment_ids.append([0] * len(elem[0][0]) + [1] * len(elem[1][0]))
        elif len(x['input'][0]) == 1:
            for elem in x['input']:
                input_ids.append(elem[0][0])
                input_mask.append(elem[0][1])
                segment_ids.append([0] * len(elem[0][0]))
        else:
            print("Current BERT doesn't support more than 2 sequences")
            exit()

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

        #print(input_ids.size(), input_mask.size(), segment_ids.size())

        # Run model
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)
        logits = self.hidden2label(pooled_output)

        probas = F.softmax(logits, dim=1)
        return logits, probas

    def pytorch_loss(self, logits, Y, loss_fn, output):
        if self.use_gpu:
            if output == 'softmax':
                y = torch.cuda.LongTensor(Y)
            else:
                y = torch.cuda.FloatTensor(Y)
            y_gold = torch.autograd.Variable(y).cuda()
        else:
            if output == 'softmax':
                y = torch.LongTensor(Y)
            else:
                y = torch.FloatTensor(Y)
            y_gold = torch.autograd.Variable(y)
        return loss_fn(logits, y_gold)

