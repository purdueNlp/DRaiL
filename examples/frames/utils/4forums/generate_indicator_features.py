from __future__ import unicode_literals
import json
import os
import math
from tqdm import tqdm 
import nltk
import string
from inflection import singularize
import re
import sys
import pickle
import json
import operator
from nltk.stem import LancasterStemmer
from nltk.util import ngrams
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk import word_tokenize
import operator
from nltk.corpus import wordnet as wn
from generate_lexicon import LexiconGenerator


def create_features(data_folder):
    upper_bounds = [50, 60, 70, 80]
    issues = ["abortion", "gun_control"]
    for bound in upper_bounds:
        print("--------------Generating ngram features for upper bound: {}---------------------------".format(bound))

        lexicon_generator = LexiconGenerator(data_folder, issues, 0.2, bound)
        pmi_scores = lexicon_generator.get_pmi()

        features = {
            "abortion": [],
            "gun_control": []
        }
        used_ngrams = {
            "abortion": [],
            "gun_control": []
        }
        for issue in issues:
            for frame in pmi_scores[issue]:
                for ngram in pmi_scores[issue][frame]:
                    if issue == "abortion":
                        if ngram not in used_ngrams["gun_control"]:
                            pmi = pmi_scores[issue][frame][ngram]
                            if pmi > 0:
                                features[issue].append(ngram)
                                used_ngrams[issue].append(ngram)
                    elif issue == "gun_control":
                        if ngram not in used_ngrams["abortion"]:
                            #import pdb; pdb.set_trace()
                            pmi = pmi_scores[issue][frame][ngram]
                            if pmi > 0:
                                features[issue].append(ngram)
                                used_ngrams[issue].append(ngram)


        with open('../../data/4forums/indicator_files/feature_file_{}.json'.format(str(bound)), 'w') as fp:
            json.dump(features, fp)


    
if __name__ == "__main__":
    create_features(sys.argv[1])