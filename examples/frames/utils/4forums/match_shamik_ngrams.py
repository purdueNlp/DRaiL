import json
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import word_tokenize
from collections import Counter
import random

random.seed(42)

debates = json.load(open("data/4forums/posts.json"))
n_grams = json.load(open("data/subframe_dictionary.json"))

stances = {}
with open("data/4forums/abortion/has_stance.txt") as fp:
    for line in fp:
        _, post, stance = line.strip().split('\t')
        stances[post] = stance

subframe_name = {};
with open("data/subframe2name.mapping") as fp:
    for line in fp:
        subframe_id, name = line.strip().split(" ", 1)
        subframe_name[subframe_id] = name

def stem_sentence(sentence, ps):
    words = word_tokenize(sentence)
    stems = []
    for w in words:
        stem = ps.stem(w.lower())
        stems.append(stem)
    stems = "_".join(stems)
    return stems

def find_subframe(sentence, n_grams, ps):
    sentence = stem_sentence(sentence, ps)
    ret = set([])
    for subf in n_grams:
        for phrase in n_grams[subf]:
            if phrase in sentence:
                ret.add(subf)
    return ret


ps = LancasterStemmer()

n_paragraphs = 0; n_paragraphs_w_subframe = 0
n_posts_w_subframe = 0; n_posts = 0
subframes_seen = []

subframe_ideology = {}

for issue in debates:
    if issue != "abortion":
        print(issue)
        continue
    for post in debates[issue]:

        post_text = debates[issue][post]
        has_post_subframe = False
        for paragraph in post_text.split('[NL]'):

            subframes = find_subframe(paragraph, n_grams, ps)
            subframes_seen += [subframe_name[s] for s in list(subframes)]
            if len(subframes) > 0:
                n_paragraphs_w_subframe += 1
                has_post_subframe = True

                for subf in subframes:
                    if subf not in subframe_ideology:
                        subframe_ideology[subf] = []
                    subframe_ideology[subf].append(stances[post])

            n_paragraphs += 1

        if has_post_subframe:
            n_posts_w_subframe += 1
        n_posts += 1

print("paragraph with subframe: {} / {}, {}%".format(n_paragraphs_w_subframe, n_paragraphs, n_paragraphs_w_subframe / n_paragraphs))
print("posts with subframe: {} / {}, {}%".format(n_posts_w_subframe, n_posts, n_posts_w_subframe / n_posts))

print(Counter(subframes_seen))


for subf in subframe_ideology:
    print(subframe_name[subf])
    print(Counter(subframe_ideology[subf]))

'''
for subf in subframe_issue_stance['Abortion']:
    print(subframe_name[subf], Counter(subframe_issue_stance['Abortion'][subf]))
'''
