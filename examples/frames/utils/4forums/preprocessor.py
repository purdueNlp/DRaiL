from spacy.tokenizer import Tokenizer
from transformers import AutoTokenizer
from spacy.lang.en import English
import os
import sys
import json
import nltk
#nltk.download('punkt')

class PreProcessor():
    def __init__(self, issues, folder):
        self.issues = issues
        self.folder = folder
        self.counter = 0
        self.word2idx = {}
        self.post2thread = {}
        self.post2seq = {}
        for issue in self.issues:
            if issue == "gun_control":
                issue = "guns"
            self.post2seq[issue] = {}
        self.build_threads()
        self.build_mapping()

    def tokenize(self, post):
        tokens = nltk.word_tokenize(post.lower())
        for token in tokens:
            if token in self.word2idx:
                pass
            else:
                self.word2idx[token] = self.counter
                self.counter += 1

        id_sequence = self.generate_post_sequence(tokens)
        return id_sequence

    def build_threads(self):
        with open("../../../4forums/data/abortion/in_thread.txt", "r") as fp:
            print("abortion")
            lines = fp.readlines()
            for line in lines:
                line = line.replace("  ", ":")
                line = line.replace("\t", ":")
                line = line.replace(" ", ":")
                ids = line.split(":")
                thread_id = ids[0]
                post_id = ids[1].replace("\n", "")
                self.post2thread[post_id] = thread_id

        with open("../../../4forums/data/gun_control/in_thread.txt", "r") as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.replace("  ", ":")
                line = line.replace("\t", ":")
                line = line.replace(" ", ":")
                ids = line.split(":")
                thread_id = ids[0]
                post_id = ids[1].replace("\n", "")
                self.post2thread[post_id] = thread_id

        
    def build_mapping(self):
        for issue in self.issues:
            subdir = self.folder + issue
            if issue == "gun_control":
                issue = "guns"
            for filename in os.listdir(subdir):
                post = ""
                with open(os.path.join(subdir, filename), 'r') as fp:
                    lines = fp.readlines()
                    counter = 0
                    while lines[counter].startswith("ID:") == False:
                        post += lines[counter]
                        counter += 1
                    post_id = lines[counter].split(":")[1].replace("\n", "")
                    
                id_sequence = self.tokenize(post)
                thread_id = self.post2thread[post_id]
                if thread_id not in self.post2seq[issue]:
                    self.post2seq[issue][thread_id] = {}
                self.post2seq[issue][thread_id][post_id] = id_sequence

    def generate_post_sequence(self, tokens):
        sequence = []
        for token in tokens:
            sequence.append(self.word2idx[token])
        
        return sequence

    def get_post_sequences(self):
        return self.post2seq
    def get_word2idx(self):
        return self.word2idx


if __name__ == "__main__":
    issues = sys.argv[1:3]
    folder = sys.argv[3]
    preprocessor = PreProcessor(issues, folder)
    sequences = preprocessor.get_post_sequences()
    word2idx = preprocessor.get_word2idx()

    #import pdb; pdb.set_trace()
    with open("../../data/4forums/word2id.json", "w") as fp:
        json.dump(word2idx, fp)
    with open("../../data/4forums/debates_embeddings.json", "w") as fp:
        json.dump(sequences, fp)

        

                    



        

            



