import json
from nltk.stem import LancasterStemmer
from nltk.tokenize import word_tokenize
import sys
import os
from tqdm import tqdm, trange

class FeatureExtractor():
    def __init__(self, issues, folder):
        self.issues = issues
        self.folder = folder
        self.lancaster = LancasterStemmer()
        with open("../../data/frame_subframe_lexicon.json") as f:
            self.indicator_list = json.load(f)
        #import pdb; pdb.set_trace()
        self.frame_features = {}
        self.subframe_features = {}
        for issue in self.issues:
            _issue = issue
            if issue == "gun_control":
                _issue = "guns"
            self.frame_features[_issue] = {}
            self.subframe_features[_issue] = {}
        self.build_indicator_features()


    def generate_post_features(self, post, post_id, issue):
        feature_vector = []
        tokens = word_tokenize(post.lower())
        stemmed_tokens = []
        for token in tokens:
            stemmed_tokens.append(self.lancaster.stem(token))
        stemmed_tokens = "_" + "_".join(stemmed_tokens) + "_"

        for idx in range(0, len(self.indicator_list)):
            if idx == 0:
                #frames
                '''
                ML: I am getting rid of this for now (slight change of plans)
                if issue == "abortion":
                    relevant_frames = ['External_Regulation_and_Reputation', 'Capacity_and_Resources', 'Cultural_Identity', 'Security_and_Defense', 'Political', 'Policy_Prescription_and_Evaluation']
                else:
                    relevant_frames = ['External_Regulation_and_Reputation', 'Political', 'Public_Sentiment', 'Fairness_and_Equality', 'Quality_of_Life']
                '''
                vector = []
                for frame in self.indicator_list[idx]:
                    #if frame in relevant_frames:
                    indicators = self.indicator_list[idx][frame]
                    for indicator in indicators:
                        if indicator in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)

                #if any(vector):
                #    print(vector)
                _issue = issue
                if issue == "gun_control":
                    _issue = "guns"
                self.frame_features[_issue][post_id] = vector

            elif (issue == 'abortion' and idx == 1) or (issue == 'gun_control' and idx == 3):
                vector = []
                for subframe in self.indicator_list[idx]:
                    indicators = self.indicator_list[idx][subframe]
                    for indicator in indicators:
                        indicator_tokens = "_" + indicator.replace(" ", "_") + "_"
                        if indicator_tokens in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)

                #if any(vector):
                #    print(vector)
                _issue = issue
                if issue == "gun_control":
                    _issue = "guns"
                self.subframe_features[_issue][post_id] = vector

    def check_indicators(self, array, subarray, n, m):
        i = 0; j = 0; 
  
        while (i < n and j < m): 
            if (array[i] == subarray[j]): 
                i += 1; 
                j += 1; 
                if (j == m): 
                    return True; 
            else: 
                i = i - j + 1; 
                j = 0; 
            
        return False; 

    def build_indicator_features(self):
        for issue in self.issues:
            subdir = self.folder + issue

            print(issue, subdir)
            n = len(os.listdir(subdir))
            print(n)
            pbar = tqdm(total=n)
            for filename in os.listdir(subdir):
                post = ""
                with open(os.path.join(subdir, filename), 'r') as fp:
                    lines = fp.readlines()
                    counter = 0
                    while lines[counter].startswith("ID:") == False:
                        post += lines[counter]
                        counter += 1
                    post_id = lines[counter].split(":")[1].replace("\n", "")
                self.generate_post_features(post, post_id, issue)
                pbar.update(1)
            pbar.close()

    def get_features(self):
        return self.frame_features, self.subframe_features

if __name__ == "__main__":
    issues = sys.argv[1:-1]
    folder = sys.argv[-1]
    feature_extractor = FeatureExtractor(issues, folder)
    frame_features, subframe_features = feature_extractor.get_features()
    with open("../../data/4forums/frame_features.json", "w") as fp:
        json.dump(frame_features, fp)
    with open("../../data/4forums/subframe_features.json", "w") as fp:
        json.dump(subframe_features, fp)
