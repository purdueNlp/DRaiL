import sys
import json
def get_topk_indicators(k, issue):
    k = int(k)
    with open("../../data/4forums/pmi_scores.json") as f:
        pmi_scores = json.load(f)

    top_k = {}
    frame_scores = pmi_scores[issue]
    for frame in frame_scores:
        top_k[frame] = frame_scores[frame][:k]
    

    return top_k

if __name__ == "__main__":
    print(get_topk_indicators(sys.argv[1], sys.argv[2]))


    


    

    