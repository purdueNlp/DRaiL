import json
import sys
from nltk.stem import LancasterStemmer
from nltk.tokenize import word_tokenize
from collections import Counter
from tqdm import tqdm, trange

def stem_paragraph(paragraph):
    lancaster = LancasterStemmer()
    tokens = word_tokenize(paragraph.lower())
    stemmed_tokens = []
    for token in tokens:
        stemmed_tokens.append(lancaster.stem(token))
    stemmed_tokens = "_" + "_".join(stemmed_tokens) + "_"
    return stemmed_tokens

def score_frames(stemmed_tokens, lexicon):
    scores = []
    for frame in lexicon["general_frames"]:
        frame_inds = 0
        for indicator in lexicon["general_frames"][frame]:
            if indicator in stemmed_tokens:
                frame_inds += 1
        scores.append((frame_inds, frame))
    return scores

def get_subframe_inds(stemmed_tokens, lexicon, issue, frame):
    ret = []
    indicators = lexicon[issue]['bigrams'][frame] + lexicon[issue]['trigrams'][frame]
    for indicator in indicators:
        indicator_tokens = "_" + indicator.replace(" ", "_") + "_"
        if indicator_tokens in stemmed_tokens:
            ret.append(indicator_tokens[1:-1])
    return ret

def main():
    try:
        data = sys.argv[1]
        lexicon = sys.argv[2]
    except:
        print("Run as: python3 label_frames.py [data_json] [lexicon_json]")
        exit(-1)

    data = json.load(open(data))
    lexicon = json.load(open(lexicon))

    has_indicator = set(); is_indicator = set()
    has_art_indicator = set()
    has_frame = set()
    #all_frames = []

    for issue in ['abortion', 'gun', 'immigration']:
        for frame in lexicon['general_frames']:
            for ind in lexicon[issue]['bigrams'][frame]:
                is_indicator.add((issue, frame.replace(' ', '_'), ind.replace(' ', '_')))
            for ind in lexicon[issue]['trigrams'][frame]:
                is_indicator.add((issue, frame.replace(' ', '_'), ind.replace(' ', '_')))

    for issue in data:
        print(issue)
        pbar = tqdm(total=len(data[issue]))
        for article_id in data[issue]:
            for paragraph_id in data[issue][article_id]:
                paragraph = data[issue][article_id][paragraph_id]
                stemmed_par = stem_paragraph(paragraph)
                #scores = score_frames(stemmed_par, lexicon)
                #scores.sort(reverse=True)
                #for _, label in scores[0:2]:
                    #has_frame.add((paragraph_id, label.replace(' ', '_')))
                    #all_frames.append(label.replace(' ', '_'))
                for frame in lexicon['general_frames']:
                    sub_indicators = get_subframe_inds(stemmed_par, lexicon, issue, frame)
                    for ind in sub_indicators:
                        has_indicator.add((article_id, paragraph_id, frame.replace(' ', '_'), ind))
                        has_art_indicator.add((article_id, frame.replace(' ', '_'), ind))
                        has_frame.add((article_id, paragraph_id, frame.replace(' ', '_')))
            pbar.update(1)
        pbar.close()

    data_files = {
        "has_indicator": has_indicator,
        "has_art_indicator": has_art_indicator,
        "is_indicator": is_indicator,
        "has_frame": has_frame,
    }
    #print(Counter(all_frames))

    for filename in data_files:
        with open("data/media_bias/{0}.txt".format(filename), "w") as fp:
            for tup in data_files[filename]:
                tup_str = map(str, tup)
                fp.write("\t".join(tup_str))
                fp.write("\n")


if __name__ == "__main__":
    main()
