from spacy.tokenizer import Tokenizer
from transformers import AutoTokenizer
from spacy.lang.en import English
import os
import sys
import json
import nltk
from tqdm import tqdm, trange
from transformers import AutoTokenizer

#nltk.download('punkt')

class PreProcessor():
    def __init__(self, data):
        self.data = json.load(open(data))
        self.counter = 0
        self.word2idx = {}
        self.paragraph2seq = {}
        self.paragraph2bert = {}
        self.bert_tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
        for issue in self.data:
            self.paragraph2seq[issue] = {}
            self.paragraph2bert[issue] = {}
        self.build_mapping()

    def tokenize(self, paragraph):
        tokens = nltk.word_tokenize(paragraph.lower())
        for token in tokens:
            if token in self.word2idx:
                pass
            else:
                self.word2idx[token] = self.counter
                self.counter += 1

        id_sequence = self.generate_paragraph_sequence(tokens)
        return id_sequence

    def build_mapping(self):
        for issue in self.data:
            print(issue)
            pbar = tqdm(total=len(self.data[issue]))
            for article_id in self.data[issue]:
                self.paragraph2seq[issue][article_id] = {}
                self.paragraph2bert[issue][article_id] = {}
                for paragraph_id in self.data[issue][article_id]:
                    paragraph = self.data[issue][article_id][paragraph_id]

                    # ids for word embeddings
                    id_sequence = self.tokenize(paragraph)
                    self.paragraph2seq[issue][article_id][paragraph_id] = id_sequence

                    # ids for bert embeddings
                    bert_sequence = self.bert_tokenizer(paragraph)['input_ids'][1:-1]
                    self.paragraph2bert[issue][article_id][paragraph_id] = bert_sequence

                pbar.update(1)
            pbar.close()

    def generate_paragraph_sequence(self, tokens):
        sequence = []
        for token in tokens:
            sequence.append(self.word2idx[token])

        return sequence

    def get_paragraph_sequences(self):
        return self.paragraph2seq

    def get_word2idx(self):
        return self.word2idx

    def get_paragraph_bert(self):
        return self.paragraph2bert

if __name__ == "__main__":
    data = sys.argv[1]
    preprocessor = PreProcessor(data)
    sequences = preprocessor.get_paragraph_sequences()
    bert = preprocessor.get_paragraph_bert()
    word2idx = preprocessor.get_word2idx()

    #import pdb; pdb.set_trace()
    with open("data/media_bias/word2id.json", "w") as fp:
        json.dump(word2idx, fp)
    with open("data/media_bias/article_embeddings.json", "w") as fp:
        json.dump(sequences, fp)
    with open("data/media_bias/article_bert.json", "w") as fp:
        json.dump(bert, fp)
