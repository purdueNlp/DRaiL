import json
import numpy as np
from scipy import stats

def main():
    data = json.load(open("data/media_bias/article_embeddings.json"))
    data_bert = json.load(open("data/media_bias/article_bert.json"))
    frame_ngrams = json.load(open("data/media_bias/frame_features_ngrams.json"))
    subframe_ngrams = json.load(open("data/media_bias/subframe_features_ngrams.json"))

    num_pars = []
    num_words = []; num_bert_tokens = []
    num_words_art = []; num_bert_tokens_art = []
    num_frameind = []; num_subframeind = []

    zero_frame_ind = 0; zero_subframe_ind = 0
    zero_frame_ind_art = 0; zero_subframe_ind_art = 0
    total_articles = 0

    for issue in data:
        for article in data[issue]:
            has_subframe_ind = False; has_frame_ind = False
            num_pars.append(len(data[issue][article]))
            n_words_art = 0; n_tokens_art = 0
            for par in data[issue][article]:
                n_words_art += len(data[issue][article][par])
                n_tokens_art += len(data_bert[issue][article][par])

                num_words.append(len(data[issue][article][par]))
                num_bert_tokens.append(len(data_bert[issue][article][par]))

                frame_inds = frame_ngrams[issue][par]
                if sum(frame_inds) == 0:
                    zero_frame_ind += 1
                else:
                    has_frame_ind = True

                num_frameind.append(sum(frame_inds))

                subframe_inds = subframe_ngrams[issue][par]
                if sum(subframe_inds) == 0:
                    zero_subframe_ind += 1
                else:
                    has_subframe_ind = True
                num_subframeind.append(sum(subframe_inds))
            total_articles += 1

            if not has_frame_ind:
                zero_frame_ind_art += 1
            if not has_subframe_ind:
                zero_subframe_ind_art += 1

            num_words_art.append(n_words_art)
            num_bert_tokens_art.append(n_tokens_art)

    print("Paragraphs per Article")
    print(stats.describe(np.array(num_pars)))

    print("Words per Paragraph")
    print(stats.describe(np.array(num_words)))

    print("Bert tokens per Paragraph")
    print(stats.describe(np.array(num_bert_tokens)))

    print("FrameInds per Paragraph")
    print(stats.describe(np.array(num_frameind)))

    print("SubframeInds per Paragraph")
    print(stats.describe(np.array(num_subframeind)))

    print("Pars with ZERO FrameInds {}; with ZERO SubframeInds {}".format(zero_frame_ind, zero_subframe_ind))
    print("Arts with ZERO FrameInds {}; with ZERO SubframeInds {}".format(zero_frame_ind_art, zero_subframe_ind_art))

    print("Words per Article")
    print(stats.describe(np.array(num_words_art)))

    print("Bert tokens per Article")
    print(stats.describe(np.array(num_bert_tokens_art)))

if __name__ == "__main__":
    main()
