import json
from scipy import stats
import numpy as np
from tqdm import tqdm, trange
import random 
import sys 

def flatten(t):
    flat_list = [item for sublist in t for item in sublist]
    return flat_list

def main():
    random.seed(42)
    par2indicators = {}; par2art = {}; art2indicator = {}
    candidates = {}; cand2frame = {}; indicator2id = {}; idx = {}
    with open(sys.argv[1]) as fp:
        for line in fp:
            art, par_id, frame, indicator = line.strip().split()
            issue = art.split('_')[0]

            if issue not in idx:
                idx[issue] = 0

            if issue not in indicator2id:
                indicator2id[issue] = {}
                cand2frame[issue] = {}

            if indicator not in indicator2id[issue]:
                indicator2id[issue][indicator] = idx[issue]
                idx[issue] += 1
                cand2frame[issue][indicator] = frame

            if par_id not in par2indicators:
                par2indicators[par_id] = set()
            if art not in art2indicator:
                art2indicator[art] = set()
            par2indicators[par_id].add(indicator)
            par2art[par_id] = art
            art2indicator[art].add(indicator)

    print(len(par2indicators))

    candidates = {}
    for topic in ['gun', 'abortion', 'immigration']:
        # Only consider relevant indicators
        candidates[topic] = list(indicator2id[topic].keys())

    is_candidate = set(); is_art_candidate = set()
    negative_samples = {}; negative_samples_art = {}

    pbar = tqdm(total=len(par2indicators))
    for par in par2indicators:
        topic = par.split('_')[0]
        negative_samples[par] = set()
        for ind in par2indicators[par]:
            is_candidate.add((par2art[par], par, cand2frame[topic][ind], ind))

            # how many negative examples to generate
            for i in range(0, 5):
                cand = random.choice(candidates[topic])
                while cand in par2indicators[par] or cand in negative_samples[par]:
                    cand = random.choice(candidates[topic])
                negative_samples[par].add(cand)
                is_candidate.add((par2art[par], par, cand2frame[topic][cand], cand))
        pbar.update(1)
    pbar.close()

    '''
    pbar = tqdm(total=len(art2indicators))
    for art in art2indicator:
        topic = art.split('_')[0]
        negative_samples_art[art] = set()
        for ind in art2indicators[art]:
            is_art_candidate.add((art, cand2frame[topic][ind], ind))
            cand = random.choice(candidates[topic])
            while cand in art2indicators[]
    '''

    data_files = {
        sys.argv[2]: is_candidate,
    }

    for filename in data_files:
        with open(sys.argv[2], "w") as fp:
            for tup in data_files[filename]:
                tup_str = map(str, tup)
                fp.write("\t".join(tup_str))
                fp.write("\n")

    with open(sys.argv[3], "w") as fp:
        json.dump(indicator2id, fp)

if __name__ == "__main__":
    main()
