import json
from nltk.stem import LancasterStemmer
from nltk.tokenize import word_tokenize
import sys
import os
from tqdm import tqdm, trange

class FeatureExtractor():
    def __init__(self, data, lexicon):
        self.data = json.load(open(data))
        self.lancaster = LancasterStemmer()
        with open(lexicon) as f:
            self.indicator_list = json.load(f)

        #import pdb; pdb.set_trace()
        self.frame_features = {}
        self.subframe_features = {}
        for issue in self.data:
            self.frame_features[issue] = {}
            self.subframe_features[issue] = {}
        self.build_indicator_features()


    def generate_paragraph_features(self, paragraph, paragraph_id, issue):
        feature_vector = []
        tokens = word_tokenize(paragraph.lower())
        stemmed_tokens = []
        for token in tokens:
            stemmed_tokens.append(self.lancaster.stem(token))
        stemmed_tokens = "_" + "_".join(stemmed_tokens) + "_"

        for idx in self.indicator_list:
            if idx == "general_frames":
                #frames
                vector = []
                for frame in self.indicator_list[idx]:
                    #if frame in relevant_frames:
                    indicators = self.indicator_list[idx][frame]
                    for indicator in indicators:
                        if indicator in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)

                #if any(vector):
                #    print(vector)
                self.frame_features[issue][paragraph_id] = vector

            elif idx == issue:
                vector = []
                for frame in self.indicator_list["general_frames"]:
                    indicators = self.indicator_list[idx]['bigrams'][frame] + self.indicator_list[idx]['trigrams'][frame]
                    for indicator in indicators:
                        indicator_tokens = "_" + indicator.replace(" ", "_") + "_"
                        if indicator_tokens in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)
                #if any(vector):
                #    print(vector)
                self.subframe_features[issue][paragraph_id] = vector

    def build_indicator_features(self):
        for issue in self.data:
            print(issue)
            pbar = tqdm(total=len(self.data[issue]))
            for article_id in self.data[issue]:
                for paragraph_id in self.data[issue][article_id]:
                    paragraph = self.data[issue][article_id][paragraph_id]
                    self.generate_paragraph_features(paragraph, paragraph_id, issue)
                pbar.update(1)
            pbar.close()

    def get_features(self):
        return self.frame_features, self.subframe_features

if __name__ == "__main__":
    data = sys.argv[1]
    lexicon = sys.argv[2]
    feature_extractor = FeatureExtractor(data, lexicon)
    frame_features, subframe_features = feature_extractor.get_features()
    with open("../../data/media_bias/frame_features_ngrams.json", "w") as fp:
        json.dump(frame_features, fp)
    with open("../../data/media_bias/subframe_features_ngrams.json", "w") as fp:
        json.dump(subframe_features, fp)
