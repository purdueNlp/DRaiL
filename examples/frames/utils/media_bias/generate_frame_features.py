import json
from nltk.stem import LancasterStemmer
from nltk.tokenize import word_tokenize
import sys
import os
from tqdm import tqdm, trange

class FeatureExtractor():
    def __init__(self, data):
        self.data = json.load(open(data))
        self.lancaster = LancasterStemmer()
        with open("../../data/frame_subframe_lexicon.json") as f:
            self.indicator_list = json.load(f)

        #import pdb; pdb.set_trace()
        self.frame_features = {}
        self.subframe_features = {}
        for issue in self.data:
            self.frame_features[issue] = {}
            self.subframe_features[issue] = {}
        self.build_indicator_features()


    def generate_paragraph_features(self, paragraph, paragraph_id, issue):
        feature_vector = []
        tokens = word_tokenize(paragraph.lower())
        stemmed_tokens = []
        for token in tokens:
            stemmed_tokens.append(self.lancaster.stem(token))
        stemmed_tokens = "_" + "_".join(stemmed_tokens) + "_"

        for idx in range(0, len(self.indicator_list)):
            if idx == 0:
                #frames
                '''
                if issue == "abortion":
                    relevant_frames = ['External_Regulation_and_Reputation', 'Capacity_and_Resources', 'Cultural_Identity', 'Security_and_Defense', 'Political', 'Policy_Prescription_and_Evaluation']
                else:
                    relevant_frames = ['External_Regulation_and_Reputation', 'Political', 'Public_Sentiment', 'Fairness_and_Equality', 'Quality_of_Life']
                '''
                vector = []
                for frame in self.indicator_list[idx]:
                    #if frame in relevant_frames:
                    indicators = self.indicator_list[idx][frame]
                    for indicator in indicators:
                        if indicator in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)

                #if any(vector):
                #    print(vector)
                self.frame_features[issue][paragraph_id] = vector

            elif (issue == 'abortion' and idx == 1) or (issue == 'gun' and idx == 3) or (issue == 'immigration' and idx == 2):
                for subframe in self.indicator_list[idx]:
                    indicators = self.indicator_list[idx][subframe]
                    for indicator in indicators:
                        indicator_tokens = "_" + indicator.replace(" ", "_") + "_"
                        if indicator_tokens in stemmed_tokens:
                            vector.append(1)
                        else:
                            vector.append(0)
                #if any(vector):
                #    print(vector)
                self.subframe_features[issue][paragraph_id] = vector

    def build_indicator_features(self):
        for issue in self.data:
            print(issue)
            pbar = tqdm(total=len(self.data[issue]))
            for article_id in self.data[issue]:
                for paragraph_id in self.data[issue][article_id]:
                    paragraph = self.data[issue][article_id][paragraph_id]
                    self.generate_paragraph_features(paragraph, paragraph_id, issue)
                pbar.update(1)
            pbar.close()

    def get_features(self):
        return self.frame_features, self.subframe_features

if __name__ == "__main__":
    data = sys.argv[1]
    feature_extractor = FeatureExtractor(data)
    frame_features, subframe_features = feature_extractor.get_features()
    with open("../../data/media_frames/frame_features.json", "w") as fp:
        json.dump(frame_features, fp)
    with open("../../data/media_frames/subframe_features.json", "w") as fp:
        json.dump(subframe_features, fp)
