import pickle
import os
import argparse
from collections import Counter
import json
import random

def read_files(data_dir):
    with open(os.path.join(data_dir, "article_data.pkl"), "rb") as in_file:
        [article2URL, article2dop, article2headline, article2text, art2label, art2segment_ids, seg_id2text] = pickle.load(in_file)
    with open(os.path.join(data_dir, "frame_data.pkl"), "rb") as in_file:
        [frame2bigrams, frame2trigrams, frame2sfs] = pickle.load(in_file)

    return article2dop, article2headline, article2text, art2label, art2segment_ids, seg_id2text,\
           frame2bigrams, frame2trigrams, frame2sfs

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', help='data directory', type=str, required=True)
    args = parser.parse_args()

    subdirs = [os.path.join(args.data_dir, f) for f in os.listdir(args.data_dir) if os.path.isdir(os.path.join(args.data_dir, f))]

    dataset = {}
    has_issue = set()
    has_paragraph = set()
    has_bias = set()
    has_dop = set()

    articles = []; paragraphs = []
    frame_ngram_lexicon = {}
    article_strs = []

    for subdir in subdirs:
        _, issue = subdir.rsplit('/', 1)
        issue, _ = issue.split('_')
        dataset[issue] = {}

        article2dop, article2headline, article2text, art2label, art2segment_ids, seg_id2text, \
        frame2bigrams, frame2trigrams, frame2sfs = read_files(subdir)

        frame_ngram_lexicon[issue] = {'bigrams': frame2bigrams, 'trigrams': frame2trigrams}

        labels = []; num_pars = 0
        for art in art2label:
            _art = "{}_{}".format(issue, art)
            articles.append(_art)
            dataset[issue][_art] = {}
            has_issue.add((_art, issue))
            has_dop.add((_art, article2dop[art]))
            labels.append(art2label[art])
            has_bias.add((_art, art2label[art]))
            num_pars += len(art2segment_ids[art])
            #article_text = article2headline[art] + ". "
            for seg in art2segment_ids[art]:
                _seg = "{}_{}".format(issue, seg)
                dataset[issue][_art][_seg] = seg_id2text[seg]
                #article_text += seg_id2text[seg]
                has_paragraph.add((_art, _seg))
                paragraphs.append(_seg)
                article_strs.append({"text": seg_id2text[seg], "id": _seg, "topic": issue})

    print(len(articles), len(set(articles)))
    print(len(paragraphs), len(set(paragraphs)))


    with open(os.path.join(args.data_dir, "general_frame_data.pkl"), "rb") as in_file:
        [frame_name2lexicon, stem2word] = pickle.load(in_file)
    frame_ngram_lexicon['general_frames'] = frame_name2lexicon

    data_files = {
        "has_issue": has_issue,
        "has_paragraph": has_paragraph,
        "has_bias": has_bias,
        "has_dop": has_dop
    }

    for filename in data_files:
        with open("data/media_bias/{0}.txt".format(filename), "w") as fp:
            for tup in data_files[filename]:
                tup_str = map(str, tup)
                fp.write("\t".join(tup_str))
                fp.write("\n")


    with open("data/media_bias/data.json", "w") as fp:
        json.dump(dataset, fp)

    with open("data/media_bias/frame_ngram_lexicon.json", "w") as fp:
        json.dump(frame_ngram_lexicon, fp)


    random.seed(42)
    random.shuffle(article_strs)
    with open("data/media_bias/lm_train.json", "w") as fp:
        for line in article_strs:
            fp.write(json.dumps(line))
            fp.write("\n")

if __name__ == "__main__":
    main()
