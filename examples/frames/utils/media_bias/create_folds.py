import json
import random
import sys

output = sys.argv[1].split('.')[0]
output = output.rsplit('/', 1)[1]
output = output.rsplit('_', 1)[1]

events = {}; articles = {}
with open("{}".format(sys.argv[1])) as fp:
    for line in fp:
        article, event = line.strip().split('\t')
        issue = article.split('_')[0]
        if issue not in articles:
            articles[issue] = []
            events[issue] = []
        articles[issue].append(article)
        events[issue].append(event)

all_articles = []; all_events = []

def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

for issue_id, issue in enumerate(articles):
    issue_articles = []; issue_events = []
    for article_id in articles[issue]:
        all_articles.append(article_id)
        issue_articles.append(article_id)
    for event_id in events[issue]:
        all_events.append(event_id)
        issue_events.append(event_id)

    random.shuffle(issue_articles)
    random.shuffle(issue_events)
    art_folds = list(split(issue_articles, 5))
    event_folds = list(split(issue_events, 5))
    art_issue_folds = {}
    for i in range(5):
        art_issue_folds[i] = art_folds[i]
    event_issue_folds = {}
    for i in range(5):
        event_issue_folds[i] = event_folds[i]

    '''
    with open("data/media_bias/folds/{}_folds.json".format(issue), "w") as fp:
        json.dump(art_issue_folds, fp)
    '''

    with open("data/media_bias/folds/{}_{}_folds.json".format(issue, output), "w") as fp:
        json.dump(event_issue_folds, fp)
