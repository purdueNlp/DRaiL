import json
from transformers import AutoTokenizer
import sys

subframe2name = {}
with open("data/subframe2name.mapping") as fp:
    for line in fp:
        subf, name = line.strip().split(" ", 1)
        subframe2name[subf] = name
subframe_phrases = json.load(open("data/subframe_phrases.json"))

if sys.argv[1] == "bert-base":
    tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)
elif sys.argv[1] == "bert-tiny":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-2_H-128_A-2", do_lower_case=True)
elif sys.argv[1] == "bert-mini":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-4_H-256_A-4", do_lower_case=True)
elif sys.argv[1] == "bert-small":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-4_H-512_A-8", do_lower_case=True)

phrases = []
is_phrase_subframe = []
phrase2bert = {}

phrase_id = 0
for subframe in subframe2name:
    for phrase in subframe_phrases['abortion'][subframe2name[subframe]]:
        phrases.append((phrase_id,))
        is_phrase_subframe.append((phrase_id, subframe))

        phrase_bert = tokenizer.encode(phrase)
        phrase2bert[phrase_id] = phrase_bert

        phrase_id += 1


dataset = {
    "phrases": phrases, "is_phrase_subframe": is_phrase_subframe
}

for filename in dataset:
    with open("data/{0}.txt".format(filename), "w") as fp:
        for tup in dataset[filename]:
            tup_str = map(str, tup)
            fp.write("\t".join(tup_str))
            fp.write("\n")


with open("data/phrase2bert.json", "w") as fp:
    json.dump(phrase2bert, fp)
