
issues = ["abortion", "guns", "immigration"]
agreement = {"abortion": [], "guns": [], "immigration": []}
less_than = []

min_pos = 100000; max_pos = 0

for issue in issues:
    debates = {}
    if issue == "immigration":
        continue
    with open("data/debate.org/{}_stance_all.txt".format(issue)) as fp:
        for line in fp:
            debate, post_id, post_pos, stance = line.strip().split()

            min_pos = min(min_pos, int(post_pos))
            max_pos = max(max_pos, int(post_pos))

            if debate not in debates:
                debates[debate] = {'pro': [], 'con': []}

            debates[debate][stance].append(int(post_pos))


    for debate in debates:
        #print(debates[debate])
        for stance in debates[debate]:
            posts = debates[debate][stance]
            posts.sort()

            for i in range(0, len(posts)):
                for j in range(i, len(posts)):
                    post_id_1 = "{}_{}".format(debate, posts[i])
                    post_id_2 = "{}_{}".format(debate, posts[j])
                    if post_id_1 != post_id_2:
                        agreement[issue].append((debate, post_id_1, post_id_2))


for i in range(min_pos, max_pos + 1):
    for j in range(i + 1, max_pos + 1):
        less_than.append((str(i), str(j)))

for issue in issues:
    with open("data/debate.org/{}_agreement_all.txt".format(issue), "w") as fp:
        for elem in agreement[issue]:
            fp.write("\t".join(elem))
            fp.write("\n")

with open("data/debate.org/less_than.txt", "w") as fp:
    for elem in less_than:
        fp.write("\t".join(elem))
        fp.write("\n")
