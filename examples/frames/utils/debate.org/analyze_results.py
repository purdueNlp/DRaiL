import csv
import numpy as np

def load_stances():
    stances = {}
    with open("data/debate.org/has_stance.txt") as fp:
        for line in fp:
            elems = line.strip().split('\t')
            stances[elems[1]] = elems[-1]
    return stances

def load_known_subframes():
    has_keyword = {}
    with open("data/debate.org/post_has_subframe_seed_ALL_guns.txt") as fp:
        for line in fp:
            elems = line.strip().split('\t')
            has_keyword[elems[1]] = elems[-1]
    return has_keyword

def load_known_frames():
    has_keyword = {}
    with open("data/debate.org/has_frame_seed_top2_guns.txt") as fp:
        for line in fp:
            elems = line.strip().split('\t')
            has_keyword[elems[1]] = elems[-1]
    return has_keyword

def parse_rule(rule_grd):
    body, head = rule_grd.split(' => ')
    head_name, head_args = head.split('(')
    head_args, _ = head_args.split(')')
    head_args = head_args.split(',')
    return head_name, head_args

mentioned_before = {}

def analyze_predictions(template_file, pred_stances):
    # For later
    sub_frame_scores = {'pro': {}, 'con': {}}
    frame_scores = {'pro': {}, 'con': {}}
    
    sub_frame_preds = {'pro': {}, 'con': {}}
    frame_preds = {'pro': {}, 'con': {}}

    sub_frame_preds_first_time = {'pro': {}, 'con': {}}
    frame_preds_first_time = {'pro': {}, 'con': {}}
    
    observed_in_debate = {'frame': {}, 'subframe': {}}

    has_subframe = {'pro': set([]), 'con': set([])}
    has_frame = {'pro': set([]), 'con': set([])}
    is_pro = set([]); is_con = set([])

    for i in range(0, 5):
        template_file = template_file.format(i)
        with open(template_file, newline='') as csvfile:
            spamreader = csv.reader(csvfile)
            for row in spamreader:
                score = float(row[1])
                pred = int(row[2])
                head_name, head_args = parse_rule(row[0])
                if head_name == "GunsStance":
                    if pred:
                        pred_stances[head_args[1]] = head_args[-1]
                        if head_args[-1] == 'pro':
                            is_pro.add(head_args[1])
                        else:
                            is_con.add(head_args[1])
                elif head_name == 'HasFrame':
                    frame_name = head_args[-1]
                    post_id = head_args[1]
                    debate_id, post_pos = post_id.split('_')
                
                    if pred:
                        if debate_id not in observed_in_debate['frame']:
                            observed_in_debate['frame'][debate_id] = set([])

                        if frame_name not in observed_in_debate['frame'][debate_id]:
                            if frame_name not in frame_preds_first_time[pred_stances[post_id]]:
                                frame_preds_first_time[pred_stances[post_id]][frame_name] = set([])
                            frame_preds_first_time[pred_stances[post_id]][frame_name].add(post_id)
                            observed_in_debate['frame'][debate_id].add(frame_name)


                        has_frame[pred_stances[post_id]].add(post_id)
                        if frame_name not in frame_preds[pred_stances[post_id]]:
                            frame_preds[pred_stances[post_id]][frame_name] = set([])
                        frame_preds[pred_stances[post_id]][frame_name].add(post_id)
                
                elif head_name == 'HasSubFrame':
                    frame_name = head_args[-1]
                    post_id = head_args[1]
                    debate_id, post_pos = post_id.split('_')
                    post_pos = int(post_pos)
                    if pred:
                        if debate_id not in observed_in_debate['subframe']:
                            observed_in_debate['subframe'][debate_id] = set([])

                        if frame_name not in observed_in_debate['subframe'][debate_id]:
                            if frame_name not in sub_frame_preds_first_time[pred_stances[post_id]]:
                                sub_frame_preds_first_time[pred_stances[post_id]][frame_name] = set([])
                            sub_frame_preds_first_time[pred_stances[post_id]][frame_name].add(post_id)
                            observed_in_debate['subframe'][debate_id].add(frame_name)

                        has_subframe[pred_stances[post_id]].add(post_id)
                        if frame_name not in sub_frame_preds[pred_stances[post_id]]:
                            sub_frame_preds[pred_stances[post_id]][frame_name] = set([])
                        sub_frame_preds[pred_stances[post_id]][frame_name].add(post_id)
    return is_pro, is_con, has_frame, has_subframe, frame_preds, sub_frame_preds, sub_frame_preds_first_time, frame_preds_first_time

def analyze_annotations(stances, known_frames, known_subframes):
    is_pro = set([]); is_con = set([])
    has_subframe = {'pro': set([]), 'con': set([])}
    has_frame = {'pro': set([]), 'con': set([])}
    sub_frame_preds = {'pro': {}, 'con': {}}
    frame_preds = {'pro': {}, 'con': {}}
    for post_id in stances:
        if stances[post_id] == 'pro':
            is_pro.add(post_id)
        else:
            is_con.add(post_id)

        if post_id in known_subframes:
            frame_name = known_subframes[post_id]
            #print(post_id, frame_name, stances[post_id])

            has_subframe[stances[post_id]].add(post_id)
            if frame_name not in sub_frame_preds[stances[post_id]]:
                sub_frame_preds[stances[post_id]][frame_name] = set([])
            sub_frame_preds[stances[post_id]][frame_name].add(post_id)

        if post_id in known_frames:
            frame_name = known_frames[post_id]
            #print(post_id, frame_name, stances[post_id])

            has_frame[stances[post_id]].add(post_id)
            if frame_name not in frame_preds[stances[post_id]]:
                frame_preds[stances[post_id]][frame_name] = set([])
            frame_preds[stances[post_id]][frame_name].add(post_id)

    return is_pro, is_con, has_frame, has_subframe, frame_preds, sub_frame_preds

def main():
    frame_names = set([]); subframe_names = set([])
    id2name = {}
    with open("data/subframe2name.mapping") as fp:
        for line in fp:
            idx, name = line.strip().split(' ', 1)
            id2name[idx] = name
            subframe_names.add(idx)

    with open("data/debate.org/subframe_type_guns.txt") as fp:
        for line in fp:
            _, frame = line.strip().split()
            frame_names.add(frame)

    template_file = "data/debate.org/LSTM_WKND/LATENT_scores_f{}_subframe_guns.csv"
    pred_stances = {}
    stances = load_stances()
    known_subframes = load_known_subframes()
    known_frames = load_known_frames()

    #is_pro, is_con, has_frame, has_subframe, frame_preds, sub_frame_preds, sub_frame_preds_first_time, frame_preds_first_time = analyze_predictions(template_file, pred_stances)
    is_pro, is_con, has_frame, has_subframe, frame_preds, sub_frame_preds = analyze_annotations(stances, known_frames, known_subframes)

    subframe_names = set([])
    for name in sub_frame_preds['pro']:
        subframe_names.add(name)
    for name in sub_frame_preds['con']:
        subframe_names.add(name)

    num_pro = len(is_pro)
    num_con = len(is_con)

    print("PRO", num_pro, "CON", num_con)
    print("PRO - HasSubFrame", len(has_subframe['pro']), "CON - HasSubFrame", len(has_subframe['con']))
    print("PRO - HasFrame", len(has_frame['pro']), "CON - HasFrame", len(has_frame['con']))
    for frame in frame_names:
        print(frame)
        if frame in frame_preds['pro']:
            print("pro", (len(frame_preds['pro'][frame]) / num_pro) * 100)
        if frame in frame_preds['con']:
            print("con", (len(frame_preds['con'][frame]) / num_con) * 100)
        print("----------------------------------------")
    print("OVERALL SUBFRAMES")
    for frame in subframe_names:
        print(frame)
        #print(id2name[frame])
        if frame in sub_frame_preds['pro']:
            print("pro", (len(sub_frame_preds['pro'][frame]) / num_pro) * 100 )
        if frame in sub_frame_preds['con']:
            print("con", (len(sub_frame_preds['con'][frame]) / num_con) * 100 )
        print("----------------------------------------")
    print("=================================================")
    print("SUBFRAMES FIRST TIME")
    for frame in subframe_names:
        print(frame)
        #print(id2name[frame])
        if frame in sub_frame_preds_first_time['pro']:
            print("pro", (len(sub_frame_preds_first_time['pro'][frame]) / num_pro) * 100 )
        if frame in sub_frame_preds_first_time['con']:
            print("con", (len(sub_frame_preds_first_time['con'][frame]) / num_con) * 100 )
        print("----------------------------------------")
    print("FRAMES FIRST TIME")
    for frame in frame_names:
        print(frame)
        if frame in frame_preds_first_time['pro']:
            print("pro", (len(frame_preds_first_time['pro'][frame]) / num_pro) * 100)
        if frame in frame_preds_first_time['con']:
            print("con", (len(frame_preds_first_time['con'][frame]) / num_con) * 100)
        print("----------------------------------------")

    print("PRO - no subframe", (len(is_pro - has_subframe['pro']) / num_pro) * 100)
    print("CON - no subframe", (len(is_con - has_subframe['con']) / num_con) * 100)
    print("NO subframe total", ( ( len(is_pro - has_subframe['pro']) + len(is_con - has_subframe['con'] ) ) / (num_pro + num_con)) * 100)

if __name__ == "__main__":
    main()
