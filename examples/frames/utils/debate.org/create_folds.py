import json
import random

debates = json.load(open("data/debate.org/debates_preprocessed.json"))

all_debates = []
hard_folds = {}
rand_folds = {}
issue_name = {}

def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

for issue_id, issue in enumerate(debates):
    print(issue_id, issue)
    issue_name[issue_id] = issue
    hard_folds[issue_id] = []
    for debate_id in debates[issue]:
        #if debate_id in ['030493', '078559']:
        #    print(issue_id, issue, debate_id)
        all_debates.append(debate_id)
        hard_folds[issue_id].append(debate_id)

for issue in hard_folds:
    issue_debates = hard_folds[issue]
    #if issue == 0:
    #    print(issue_debates)
    #print(issue_name[issue])
    #exit()
    random.shuffle(issue_debates)
    folds = list(split(hard_folds[issue], 5))
    issue_folds = {}
    for i in range(5):
        issue_folds[i] = folds[i]
    with open("data/debate.org/folds/{}_folds.json".format(issue_name[issue]), "w") as fp:
        json.dump(issue_folds, fp)
#exit()


'''
#folds = list(split(all_debates, 3))
for i in range(3):
    rand_folds[i] = folds[i]

with open("data/debate.org/folds/hard_folds.json", "w") as fp:
    json.dump(hard_folds, fp)

with open("data/debate.org/folds/rand_folds.json", "w") as fp:
    json.dump(rand_folds, fp)
'''
