import json
import numpy as np

debates_ids = json.load(open("data/debate.org/debates_ids.json"))

n_empty_abortion = 0; n_empty_immigration = 0; n_empty_guns = 0
n_abortion = 0; n_immigration = 0; n_guns = 0
n_abortion_pro = 0; n_immigration_pro = 0; n_guns_pro = 0
n_abortion_con = 0; n_immigration_con = 0; n_guns_con = 0
num_debates = {'abortion': set([]), 'immigration': set([]), 'guns': set([])}
debate_length = {'abortion': {}, 'immigration': {}, 'guns': {}}

with open("data/debate.org/has_stance.txt") as fp:
    for line in fp:
        debate_id, post_pos, stance = line.strip().split("\t")

        if debate_id in debates_ids["abortion"]:
            if len(debates_ids["abortion"][debate_id]["posts"][int(post_pos)]) <= 0:
                n_empty_abortion += 1
            else:
                n_abortion += 1
                num_debates["abortion"].add(debate_id)
                if debate_id not in debate_length['abortion']:
                    debate_length['abortion'][debate_id] = 0
                debate_length['abortion'][debate_id] += 1

                if stance == "pro":
                    n_abortion_pro += 1
                else:
                    n_abortion_con += 1

        elif debate_id  in debates_ids["immigration"]:
            if len(debates_ids["immigration"][debate_id]["posts"][int(post_pos)]) <= 0:
                n_empty_immigration += 1
            else:
                n_immigration += 1
                num_debates["immigration"].add(debate_id)
                if debate_id not in debate_length['immigration']:
                    debate_length['immigration'][debate_id] = 0
                debate_length['immigration'][debate_id] += 1

                if stance == "pro":
                    n_immigration_pro += 1
                else:
                    n_immigration_con += 1

        elif debate_id in debates_ids["guns"]:
            if len(debates_ids["guns"][debate_id]["posts"][int(post_pos)]) <= 0:
                n_empty_guns += 1
            else:
                n_guns += 1
                num_debates["guns"].add(debate_id)
                if debate_id not in debate_length['guns']:
                    debate_length['guns'][debate_id] = 0
                debate_length['guns'][debate_id] += 1

                if stance == "pro":
                    n_guns_pro += 1
                else:
                    n_guns_con += 1

print("Num empty -- abortion {}, immigration {}, guns {}".format(n_empty_abortion, n_empty_immigration, n_empty_guns))
print("Num posts -- abortion {}, immigration {}, guns {}".format(n_abortion, n_immigration, n_guns))
print("Num posts PRO-- abortion {}, immigration {}, guns {}".format(n_abortion_pro, n_immigration_pro, n_guns_pro))
print("Num posts CON-- abortion {}, immigration {}, guns {}".format(n_abortion_con, n_immigration_con, n_guns_con))
print("Num debates -- abortion {}, immigration {}, guns {}".format(len(num_debates["abortion"]), len(num_debates["immigration"]), len(num_debates["guns"])))

num_posts = debate_length['abortion'].values()
print("Statistics abortion (NUM POSTS) -- max {}, min {}, mean {}, median {}".format(max(num_posts), min(num_posts), np.mean(num_posts), np.median(num_posts)))

num_posts = debate_length['immigration'].values()
print("Statistics immigration (NUM POSTS) -- max {}, min {}, mean {}, median {}".format(max(num_posts), min(num_posts), np.mean(num_posts), np.median(num_posts)))

num_posts = debate_length['guns'].values()
print("Statistics guns (NUM POSTS) -- max {}, min {}, mean {}, median {}".format(max(num_posts), min(num_posts), np.mean(num_posts), np.median(num_posts)))

