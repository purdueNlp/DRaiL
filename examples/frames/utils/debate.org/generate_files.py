import json

has_post = set([]) #(debate, post)
has_sentence = set([]) #(debate, post, sentence)
is_author = set([]) #(debate, post, author)
has_issue = set([]) #(debate, issue)
frame_relevant = set([]) #(issue, frame)
has_frame = set([]) #(debate, post, sentence, frame)

# A sequence model for the frame of each sentence
next_sentence = set([]) #(debate, post, sentence, sentence)

# Will we do stance at the post level or at the sentence level?
has_stance = set([]) #(debate, post, stance)

debates = json.load(open("data/debate.org/debates_preprocessed.json"))
issues_pro = json.load(open("data/debate.org/issues_pro.json"))
issues_con = json.load(open("data/debate.org/issues_con.json"))
user_info = json.load(open("data/debate.org/user_info.json"))
abortion_stance = set([])
guns_stance = set([])
immigration_stance = set([])

relevant_frames = {
    "immigration": ["crime", "economic", "fairness", "resources"],
    "abortion": ["morality", "health", "fairness", "constitutionality"],
    "guns": ["health", "policy", "security", "crime"]
}

for issue in debates:
    for debate in debates[issue]:
        has_issue.add((debate, issue))
        for frame in relevant_frames[issue]:
            frame_relevant.add((issue, frame))
            post_pos = 1
            for post, stance in zip(debates[issue][debate]['posts'], debates[issue][debate]['stances']):
                if post is None:
                    post_pos += 1
                    continue
                post_id = "{}_{}".format(debate, post_pos)

                author = debates[issue][debate]['authors'][stance]
                if issue == "abortion":
                    if author in issues_pro['Abortion']:
                        abortion_stance.add((debate, post_id, post_pos, 'pro'))
                    elif author in issues_con['Abortion']:
                        abortion_stance.add((debate, post_id, post_pos, 'con'))

                if issue == "guns":
                    if author in issues_pro['Gun Rights']:
                        guns_stance.add((debate, post_id, post_pos, 'pro'))
                    elif author in issues_con['Gun Rights']:
                        guns_stance.add((debate, post_id, post_pos, 'con'))
                    '''
                    elif author not in issues_con['Gun Rights'] and \
                         author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] in ["Conservative", "Libertarian"]:
                             guns_stance.add((debate, post_id, post_pos, 'pro'))
                    elif author not in issues_con['Gun Rights'] and \
                        author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] in ["Liberal", "Progressive"]:
                             guns_stance.add((debate, post_id, post_pos, 'con'))
                    '''
                if issue == "immigration":
                    if author in issues_pro['Border Fence']:
                        immigration_stance.add((debate, post_id, post_pos, 'pro'))
                    elif author in issues_con['Border Fence']:
                        immigration_stance.add((debate, post_id, post_pos, 'con'))
                    '''
                    elif author not in issues_con['Border Fence'] and \
                         author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] in ["Conservative"]:
                             immigration_stance.add((debate, post_id, post_pos, 'pro'))
                    elif author not in issues_con['Border Fence'] and \
                        author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] in ["Liberal", "Progressive"]:
                             immigration_stance.add((debate, post_id, post_pos, 'con'))
                    '''
                has_post.add((debate, post_id, post_pos))
                has_stance.add((debate, post_id, post_pos, stance))
                is_author.add((debate, post_id, post_pos, author))

                prev_sentence = -1
                for sent_pos, sent in enumerate(post):
                    has_sentence.add((debate, post_id, post_pos, sent_pos))
                    next_sentence.add((debate, post_id, post_pos, prev_sentence, sent_pos))
                    prev_sentence = sent_pos

                post_pos += 1

dataset = {
    #"has_issue": has_issue, "frame_relevant": frame_relevant, "has_post": has_post,
    #"has_stance": has_stance, "has_sentence": has_sentence, "next_sentence": next_sentence,
    #"is_author": is_author,
    "abortion_stance": abortion_stance,
    "guns_stance": guns_stance,
    "immigration_stance": immigration_stance
}

for filename in dataset:
    with open("data/debate.org/{0}.txt".format(filename), "w") as fp:
        for tup in dataset[filename]:
            tup_str = map(str, tup)
            fp.write("\t".join(tup_str))
            fp.write("\n")
