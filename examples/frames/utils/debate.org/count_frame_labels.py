from collections import Counter
import json

debates = json.load(open("data/debate.org/debates_preprocessed.json"))

labels = []

with open("data/debate.org/has_frame_seed.txt") as fp:
    for line in fp:
        debate, post_id, post_pos, label = line.split()
        if label == "Crime_and_Punishment":
            print(debates['abortion'][debate]['posts'][int(post_pos) - 1])

        labels.append(label)


print(Counter(labels))
