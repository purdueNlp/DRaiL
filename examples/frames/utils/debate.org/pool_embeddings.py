import numpy as np
import argparse

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--embed_matrix", type=str, required=True)
    parser.add_argument("--embed_sizes", type=str, required=True)
    parser.add_argument("--name", type=str, required=True)
    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()
    embedding_matrix = np.load(args.embed_matrix)
    embedding_sizes = np.load(args.embed_sizes)
    print(embedding_matrix.shape, embedding_sizes.shape)

    num_samples, seq_len, embed_dim = embedding_matrix.shape

    pooled_matrix = np.zeros((num_samples, embed_dim))

    for i in range(0, num_samples):
        curr_len = int(embedding_sizes[i])
        curr_seq = embedding_matrix[i]

        curr_seq = curr_seq[:curr_len]
        curr_seq = np.average(curr_seq, axis=0)
        pooled_matrix[i] = curr_seq

    np.save("data/debate.org/{0}_pooled.npy".format(args.name), pooled_matrix)

if __name__ == "__main__":
    main()
