import numpy as np
from nn_bert_paraphrase import BertClassifier
import sys
import random
from pytorch_transformers import AdamW, WarmupLinearSchedule
import torch
from sklearn.metrics import *
import time
import json
import argparse
from pytorch_transformers import BertModel, BertConfig
import progressbar

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument("--embed_model", help="name of the embedding", type=str, required=True)
    parser.add_argument("--maxsent", help="maxsent", type=int, required=True)
    parser.add_argument("--avgpool", help="maxsent", default=False, action="store_true")
    args = parser.parse_args()
    return args

def pad_and_mask(vector, max_seq_len):
    pad_token = 0
    mask = [1] * len(vector)
    if len(vector) < max_seq_len:
        vector += [pad_token] * (max_seq_len - len(vector))
        mask += [0] * (max_seq_len - len(mask))
    return (vector, mask)

def _get_long_tensor(inp):
    return torch.cuda.LongTensor(inp)

def _get_variable(tensor):
    var = torch.autograd.Variable(tensor)
    var = var.cuda()
    return var

def embed_sentence(bert_model, bert_encoding, sent_id, CLS, SEP, MAX_SENT):
    bert_sent = [CLS] + bert_encoding[sent_id][:MAX_SENT] + [SEP]
    bert_sent, bert_mask = pad_and_mask(bert_sent, MAX_SENT + 2)
    n_sent = len(bert_sent)
    bert_segment = [0] * n_sent


    input_ids = _get_variable(_get_long_tensor([bert_sent]))
    input_mask = _get_variable(_get_long_tensor([bert_mask]))
    segment_ids = _get_variable(_get_long_tensor([bert_segment]))

    hidden_states, output = bert_model(input_ids, attention_mask=input_mask,
                         token_type_ids=segment_ids, position_ids=None,
                         head_mask=None)
    return (hidden_states[0].detach().cpu().numpy(), n_sent)


def main():
    args = parse_arguments()

    MAX_SENT = args.maxsent
    CLS = 101
    SEP = 102
    CONFIG = json.load(open(args.config))['models'][0]
    BERT_ENCODING_F = "/scratch1/pachecog/DRaiL/examples/frames/data/debate.org/bert_encode/bert_encode.npy"
    DEBATES_F = "/scratch1/pachecog/DRaiL/examples/frames/data/debate.org/debates_ids.json"
    DEBATES = json.load(open(DEBATES_F))

    torch.cuda.set_device(args.gpu_index)
    bert_encoding = np.load(BERT_ENCODING_F)
    bert_encoding = np.array(bert_encoding)
    print("Bert encodings", bert_encoding.shape)

    pretrained_dict = torch.load(args.savedir, map_location=lambda storage, loc: storage)

    bert_config = BertConfig.from_pretrained("bert-base-uncased")
    bert_model = BertModel.from_pretrained("bert-base-uncased")

    bert_model_dict = bert_model.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in bert_model_dict}
    bert_model_dict.update(pretrained_dict)
    bert_model.load_state_dict(bert_model_dict)
    bert_model = bert_model.cuda()

    if not args.avgpool:
        resulting_embeddings =\
            np.zeros((bert_encoding.shape[0], MAX_SENT + 2, bert_config.hidden_size))
        resulting_sizes = np.zeros((bert_encoding.shape[0]))
    else:
        resulting_embeddings =\
            np.zeros((bert_encoding.shape[0], bert_config.hidden_size))


    for issue in DEBATES:
        print(issue)
        bar = progressbar.ProgressBar(maxval=len(DEBATES[issue]),
                widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
        bar.start()
        for d_id, dname in enumerate(DEBATES[issue]):
            #print(d_id, dname)
            title_id = DEBATES[issue][dname]['title']
            embed_sent, n_sent = embed_sentence(bert_model, bert_encoding, title_id, CLS, SEP, MAX_SENT)
            if not args.avgpool:
                resulting_embeddings[title_id] = embed_sent
                resulting_sizes[title_id] = n_sent
            else:
                resulting_embeddings[title_id] = np.average(embed_sent[:n_sent], axis=0)

            for post_id, post in enumerate(DEBATES[issue][dname]['posts']):
                for sent_id in post:
                    embed_sent, n_sent = embed_sentence(bert_model, bert_encoding, sent_id, CLS, SEP, MAX_SENT)

                    if not args.avgpool:
                        resulting_embeddings[sent_id] = embed_sent
                        resulting_sizes[sent_id] = n_sent
                    else:
                        resulting_embeddings[sent_id] = np.average(embed_sent[:n_sent], axis=0)
            bar.update(d_id)
    if not args.avgpool:
        np.save("data/debate.org/embeddings/bert_{0}.npy".format(args.embed_model), resulting_embeddings)
        np.save("data/debate.org/embeddings/bert_{0}_sz.npy".format(args.embed_model), resulting_sizes)
    else:
        np.save("data/debate.org/embeddings/{0}_pooled.npy".format(args.embed_model), resulting_embeddings)

if __name__ == "__main__":
    main()

