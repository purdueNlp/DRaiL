from transformers import AutoTokenizer
import re
import numpy as np
import json
from collections import OrderedDict
import sys
import nltk

debates_bert = {}
debates = json.load(open("data/debate.org/debates_preprocessed.json"))
sentence_id = 0
token_id = 0

word2id = {}
debates_embeddings = {}

max_title = 0
max_post = 0

post_lengths = []

for issue in debates:
    debates_embeddings[issue] = {}

    for debate in debates[issue]:
        title = debates[issue][debate]['title']
        tokens = nltk.word_tokenize(title.lower())
        max_title = max(max_title, len(tokens))

        title_tokens = []
        for token in tokens:
            if token not in word2id:
                word2id[token] = token_id
                token_id += 1
            title_tokens.append(word2id[token])


        posts_curr = []; words_curr = []
        for post in debates[issue][debate]['posts']:
            post_tokens = []; post_words = []
            if post is not None:
                for sentence in post:
                    tokens = nltk.word_tokenize(sentence.lower())
                    sentence_tokens = []
                    sentence_words = []
                    for token in tokens:
                        if token not in word2id:
                            word2id[token] = token_id
                            token_id +=1
                        sentence_tokens.append(word2id[token])
                        sentence_words.append(token)
                    post_tokens += sentence_tokens
                    post_words += sentence_words
                post_lengths.append(len(post_tokens))
            posts_curr.append(post_tokens)
            words_curr.append(post_words)

        debates_embeddings[issue][debate] = {
            'title': title_tokens,
            'posts': posts_curr,
            'post_words': words_curr,
            'stances': debates[issue][debate]['stances'],
            'authors': debates[issue][debate]['authors']
        }


print(max_title)
print(max(post_lengths))
print(np.mean(post_lengths))
print(np.median(post_lengths))

with open("data/debate.org/debates_embeddings.json", "w") as fp:
    json.dump(debates_embeddings, fp)

with open("data/debate.org/word2id.json", "w") as fp:
    json.dump(word2id, fp)
