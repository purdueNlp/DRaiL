import json
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import word_tokenize
from collections import Counter
import random
import sys


if sys.argv[1] not in ['abortion', 'guns', 'immigration']:
    print("Wrong issue, specify one in [abortion|guns|immigration]")
    exit(-1)

random.seed(42)

debates = json.load(open("data/debate.org/debates_preprocessed.json"))
lexicon = json.load(open("data/frame_subframe_lexicon.json"))

if sys.argv[1] == 'abortion':
    n_grams = json.load(open("data/subframe_dictionary.json"))
elif sys.argv[1] == 'guns':
    n_grams = lexicon[1]
elif sys.argv[1] == 'immigration':
    n_grams = lexicon[2]

user_info = json.load(open("data/debate.org/user_info.json"))
issues_pro = json.load(open("data/debate.org/issues_pro.json"))
issues_con = json.load(open("data/debate.org/issues_con.json"))


issue_mapping = {
        'abortion': 'Abortion',
        'guns': 'Gun Rights',
        'immigration': 'Border Fence'
}

frame_indicators = lexicon[0]

subframe_name = {}; subframe_cands = set([])
post_ngram_features = {}
num_ngrams = 0
for subf in n_grams:
    num_ngrams += len(n_grams[subf])

post_frameind_features = {}
num_frame_ind = 0
for frame in frame_indicators:
    num_frame_ind += len(frame_indicators[frame])

post_stemind_features = {}

with open("data/subframe2name.mapping") as fp:
    for line in fp:
        subframe_id, name = line.strip().split(" ", 1)
        subframe_name[subframe_id] = name

def stem_sentence(sentence, ps):
    words = word_tokenize(sentence)
    stems = []
    for w in words:
        stem = ps.stem(w.lower())
        stems.append(stem)
    stems = "_".join(stems)
    return stems

def find_subframe(sentence, n_grams, ps, stance, post_vector):
    sentence_stem = stem_sentence(sentence, ps)
    ret = {}; 
    index = 0
    for subf in n_grams:
        #if subframe_name[subf] in ['Stem Cell Research', 'Sale of Fetal Tissue', 'Pregnancy Centers', 'Planned Parenthood', 'Health Care']:
        #    continue

        for phrase in n_grams[subf]:
            phrase = phrase.replace(' ', '_')
            if phrase in sentence_stem:
                post_vector[index] = 1.0
                if subf not in ret:
                    ret[subf] = 0
                ret[subf] += 1
            index += 1
    k = Counter(ret) 
    ret_mc = {}
    #for elem in k.most_common(1):
    for elem in k.items():
        ret_mc[elem[0].replace(' ', '_')] = elem[1]
    return ret_mc

def find_frame(issue, sentence, n_grams, ps, stance, post_vector):
    relevant_words = {}
    sentence_stem = stem_sentence(sentence, ps)
    sentence_stem = sentence_stem.split('_')
    ret = {};
    index = 0
    for subf in n_grams:
        subf_rep = subf.replace(' ', '_').replace(',', '')
        relevant_words[subf_rep] = []
        if subf == 'Other':
            continue
        elif issue == "abortion" and subf_rep in ['External_Regulation_and_Reputation', 'Capacity_and_Resources', 'Cultural_Identity', 'Security_and_Defense', 'Political', 'Policy_Prescription_and_Evaluation']:
            continue
        elif issue == "guns" and subf_rep in ['External_Regulation_and_Reputation', 'Political', 'Public_Sentiment', 'Fairness_and_Equality', 'Quality_of_Life']:
            continue
        elif issue == 'immigration' and subf_rep in ['External_Regulation_and_Reputation', 'Capacity_and_Resources', 'Health_and_Safety', 'Political', 'Public_Sentiment', 'Morality', 'Quality_of_Life']:
            continue
        for phrase in n_grams[subf]:
            if phrase in sentence_stem:
                post_vector[index] = 1.0
                if subf_rep not in ret:
                    ret[subf_rep] = 0
                ret[subf_rep] += 1
                relevant_words[subf_rep].append(phrase)

            index += 1

    k = Counter(ret)
    ret_mc = {}
    for i, elem in enumerate(k.most_common(2)):
        ret_mc[elem[0]] = elem[1]
    return ret_mc

def find_stem(sentence, all_stems, ps, stance, post_vector):
    sentence_stem = stem_sentence(sentence, ps)
    index = 0
    for phrase in all_stems:
        if phrase in sentence_stem:
            post_vector[index] = 1.0

ps = LancasterStemmer()

n_sentences = 0
n_titles_w_subframe = 0; n_titles_w_frame = 0; n_titles = 0
n_posts_w_subframe = 0; n_posts_w_frame = 0; n_posts = 0
n_debates_w_both_subframe = 0; n_debates_w_both_frame = 0; n_debates = 0
n_debates_w_one_subframe = 0; n_debates_w_one_frame = 0

subframes_seen = []; frames_seen = []
subframe_ideology = {}; frame_ideology = {}
subframe_issue_stance = {'abortion': {}, 'guns': {}, 'immigration': {}}; 
frame_issue_stance = {'abortion': {}, 'guns': {}, 'immigration': {}}

has_subframe_seed = []; has_frame_seed = [] # frame will be saved at the post-level
has_subframe_cand = []
has_post_subframe_seed = []; has_post_subframe_cand = []

def get_all_stems(ps, debates):
    all_stems = set([])
    for issue in debates:
        if issue != sys.argv[1]:
            continue
        for debate in debates[issue]:
            for post, stance in zip(debates[issue][debate]['posts'], debates[issue][debate]['stances']):
                if post is None:
                    continue
                post_str = " ".join(post)
                sentence_stem = stem_sentence(post_str, ps)
                for stem in sentence_stem.split('_'):
                    all_stems.add(stem)
    return all_stems

all_stems = get_all_stems(ps, debates)

for issue in debates:
    if issue != sys.argv[1]:
        continue
    for debate in debates[issue]:
        title = debates[issue][debate]['title']
        author = debates[issue][debate]['authors']['pro']

        if author in issues_pro[issue_mapping[issue]]:
            issue_stance = 'pro'
        elif author in issues_con[issue_mapping[issue]]:
            issue_stance = 'con'
        else:
            issue_stance = None

        title_vector = [0.0] * num_ngrams
        subframes = find_subframe(title, n_grams, ps, issue_stance, title_vector)

        if issue == 'abortion':
            subframes_seen += [subframe_name[s] for s in list(subframes)]
        else:
            subframes_seen += list(subframes)

        #frames = find_frame(title, frame_indicators, ps, issue_stance, title_vector)
        #frames_seen += [s for s in list(frames)]

        '''
        THIS NEEDS TO BE UPDATED
        if len(subframes) > 0:
            subframe_cands_rand = subframe_cands - set(subframes.keys())
            subframe_cands_rand = list(subframe_cands)
            random.shuffle(subframe_cands_rand)
            for subf in subframe_cands_rand[:5] + list(subframes):
                post_id = "{}_{}".format(debate, 0)
                #has_subframe_cand.append((debate, post_id, 0, 0, subf))
        for subf in subframes:
            post_id = "{}_{}".format(debate, 0)
            has_subframe_seed.append((debate, post_id, 0, 0, subf))
            for issue_x in ['Abortion']:
                if subf not in subframe_issue_stance[issue_x]:
                    subframe_issue_stance[issue_x][subf] = []
                if author in issues_pro[issue_x]:
                    subframe_issue_stance[issue_x][subf].append('pro')
                elif author in issues_con[issue_x]:
                    subframe_issue_stance[issue_x][subf].append('con')

            if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                ideology_of_author = user_info[author]["Ideology"]
                if subf not in subframe_ideology:
                    subframe_ideology[subf] = []
                subframe_ideology[subf].append(ideology_of_author)
        '''
        '''
        for frame in frames:
            post_id = "{}_{}".format(debate, 0)
            has_frame_seed.append((debate, post_id, 0, 0, frame))
            for issue_x in ['Abortion']:
                if frame not in frame_issue_stance[issue_x]:
                    frame_issue_stance[issue_x][frame] = []
                if author in issues_pro[issue_x]:
                    frame_issue_stance[issue_x][frame].append('pro')
                elif author in issues_con[issue_x]:
                    frame_issue_stance[issue_x][frame].append('con')

            if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                ideology_of_author = user_info[author]["Ideology"]
                if frame not in frame_ideology:
                    frame_ideology[frame] = []
                frame_ideology[frame].append(ideology_of_author)
        '''
        if len(subframes) > 0:
            n_titles_w_subframe += 1
        n_titles += 1
        n_sentences += 1

        found_stem_pro = False; found_stem_con = False
        found_stem_frame_pro = False; found_stem_frame_con = False
        post_pos = 1
        for post, stance in zip(debates[issue][debate]['posts'], debates[issue][debate]['stances']):
            if post is not None:
                post_vector = [0.0] * num_ngrams
                post_vector_frame = [0.0] * num_frame_ind
                post_vector_stem = [0.0] * len(all_stems)
                
                subframes_post = set([]); subframes_cand_post = set([])
                author = debates[issue][debate]['authors'][stance]
                found_stem = False; found_stem_frame = False

                if author in issues_pro[issue_mapping[issue]]:
                    issue_stance = 'pro'
                elif author in issues_con[issue_mapping[issue]]:
                    issue_stance = 'con'
                else:
                    issue_stance = None

                post_str = " ".join(post)
                frames = find_frame(issue, post_str, frame_indicators, ps, issue_stance, post_vector_frame)
                find_stem(post_str, all_stems, ps, issue_stance, post_vector_stem)
                #if 'Crime and Punishment' in frames:
                #    print(post_str)
                #    print(frames)
                #    print("---------")
                frames_seen += [s for s in list(frames)]

                if len(frames) > 0:
                    found_stem_frame = True
                    if stance == "pro":
                        found_stem_frame_pro = True
                    else:
                        found_stem_frame_con = True

                for frame in frames:
                    post_id = "{}_{}".format(debate, post_pos)
                    has_frame_seed.append((debate, post_id, post_pos, frame))
                    for issue_x in [issue]:
                        if frame not in frame_issue_stance[issue_x]:
                            frame_issue_stance[issue_x][frame] = []
                        if author in issues_pro[issue_mapping[issue_x]]:
                            frame_issue_stance[issue_x][frame].append('pro')
                        elif author in issues_con[issue_mapping[issue_x]]:
                            frame_issue_stance[issue_x][frame].append('con')

                    if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                        ideology_of_author = user_info[author]["Ideology"]
                        if frame not in frame_ideology:
                            frame_ideology[frame] = []
                        frame_ideology[frame].append(ideology_of_author)

                subframes = find_subframe(post_str, n_grams, ps, issue_stance, post_vector)
                
                if issue == 'abortion':
                    subframes_seen += [subframe_name[s] for s in list(subframes)]
                else:
                    subframes_seen += list(subframes)


                if len(subframes) > 0:
                    subframe_cands_rand = subframe_cands - set(subframes.keys())
                    subframe_cands_rand = list(subframe_cands)
                    random.shuffle(subframe_cands_rand)
                    for subf in subframe_cands_rand[:5] + list(subframes):
                        post_id = "{}_{}".format(debate, post_pos)
                        has_post_subframe_cand.append((debate, post_id,post_pos, subf))
                for subf in subframes:
                    post_id = "{}_{}".format(debate, post_pos)
                    has_post_subframe_cand.append((debate, post_id, post_pos, subf))
                    has_post_subframe_seed.append((debate, post_id, post_pos, subf))
                    subframes_post.add((debate, post_pos, subf))
                    for issue_x in [issue]:
                        if subf not in subframe_issue_stance[issue_x]:
                            subframe_issue_stance[issue_x][subf] = []
                        if author in issues_pro[issue_mapping[issue_x]]:
                            subframe_issue_stance[issue_x][subf].append('pro')
                        elif author in issues_con[issue_mapping[issue_x]]:
                            subframe_issue_stance[issue_x][subf].append('con')

                    if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                        ideology_of_author = user_info[author]["Ideology"]
                        if subf not in subframe_ideology:
                            subframe_ideology[subf] = []
                        subframe_ideology[subf].append(ideology_of_author)


                if len(subframes) > 0:
                    found_stem = True
                    if stance == "pro":
                        found_stem_pro = True
                    else:
                        found_stem_con = True

                    n_sentences += 1
                if found_stem:
                    n_posts_w_subframe += 1
                if found_stem_frame:
                    n_posts_w_frame += 1

                post_id = "{}_{}".format(debate, post_pos)
                post_ngram_features[post_id] = post_vector
                post_frameind_features[post_id] = post_vector_frame
                post_stemind_features[post_id] = post_vector_stem
                n_posts += 1
            post_pos += 1

        if found_stem_pro and found_stem_con:
            n_debates_w_both_subframe += 1
        elif found_stem_pro or found_stem_con:
            n_debates_w_one_subframe += 1
        
        if found_stem_frame_pro and found_stem_frame_con:
            n_debates_w_both_frame += 1
        elif found_stem_frame_pro or found_stem_frame_con:
            n_debates_w_one_frame += 1
        
        n_debates += 1

dataset = {
        #'has_subframe_seed': has_subframe_seed,
        'post_has_subframe_seed_ALL': has_post_subframe_seed,
        'has_frame_seed_top2': has_frame_seed
}

for filename in dataset:
    with open("data/debate.org/{0}/{1}.txt".format(sys.argv[1], filename), "w") as fp:
        for tup in dataset[filename]:
            tup_str = map(str, tup)
            fp.write("\t".join(tup_str))
            fp.write("\n")

print("titles with subframe: {}, {}%".format(n_titles_w_subframe, n_titles_w_subframe / n_titles))
print("posts with subframe: {}, {}%".format(n_posts_w_subframe, n_posts_w_subframe / n_posts))
print("debates with both authors expressing subframe: {}, {}%".format(n_debates_w_both_subframe, n_debates_w_both_subframe / n_debates))
print("debates with at least one author expressing subframe: {}, {}%".format(n_debates_w_one_subframe, n_debates_w_one_subframe / n_debates))

print(Counter(subframes_seen))
print("Num sentences (titles + posts)", n_sentences)

print("titles with frame: {}, {}%".format(n_titles_w_frame, n_titles_w_frame / n_titles))
print("posts with frame: {}, {}%".format(n_posts_w_frame, n_posts_w_frame / n_posts))
print("debates with both authors expressing frame: {}, {}%".format(n_debates_w_both_frame, n_debates_w_both_frame / n_debates))
print("debates with at least one author expressing frame: {}, {}%".format(n_debates_w_one_frame, n_debates_w_one_frame / n_debates))

'''
NEEDS TO BE UPDATED
for subf in subframe_ideology:
    print(subframe_name[subf])
    print(Counter(subframe_ideology[subf]))

for subf in subframe_issue_stance['Abortion']:
    print(subframe_name[subf], Counter(subframe_issue_stance['Abortion'][subf]))

for frame in frame_issue_stance['Abortion']:
    print(frame, Counter(frame_issue_stance['Abortion'][frame]))
'''
with open("data/debate.org/{}/post_ngram_feats.json".format(sys.argv[1]), "w") as fp:
    json.dump(post_ngram_features, fp)

with open("data/debate.org/{}/post_frameind_feats.json".format(sys.argv[1]), "w") as fp:
    json.dump(post_frameind_features, fp)

'''
with open("data/debate.org/{}/post_stemind_features.json".format(sys.argv[1]), "w") as fp:
    json.dump(post_stemind_features, fp)
'''
