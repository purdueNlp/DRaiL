from transformers import AutoTokenizer
import re
import numpy as np
import json
from collections import OrderedDict
import sys

debates_bert = {}
debates = json.load(open("data/debate.org/debates_preprocessed.json"))

sentence_id = 0
bert_text = []

if sys.argv[1] == "bert-base":
    tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)
elif sys.argv[1] == "bert-tiny":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-2_H-128_A-2", do_lower_case=True)
elif sys.argv[1] == "bert-mini":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-4_H-256_A-4", do_lower_case=True)
elif sys.argv[1] == "bert-small":
    tokenizer = AutoTokenizer.from_pretrained("google/bert_uncased_L-4_H-512_A-8", do_lower_case=True)

else:
    print("use: python3 generate_bert.py [bert-base|bert-tiny|bert-mini|bert-small]")
    exit(-1)

max_title = 0; max_sentence = 0

for issue in debates:
    debates_bert[issue] = {}

    for debate in debates[issue]:
        title = debates[issue][debate]['title']
        sentence_bert = tokenizer.encode(title)

        max_title = max(max_title, len(sentence_bert))

        bert_text.append(sentence_bert)
        title_id = sentence_id
        sentence_id += 1

        posts_curr = []
        for post in debates[issue][debate]['posts']:
            sentences_curr = []
            if post is not None:
                for sentence in post:
                    sentence_bert = tokenizer.encode(sentence)
                    if len(sentence_bert) > 150:
                        sentence_bert = sentence_bert[:150]
                    max_sentence = max(max_sentence, len(sentence_bert))
                    bert_text.append(sentence_bert)
                    sentences_curr.append(sentence_id)
                    sentence_id += 1
            posts_curr.append(sentences_curr)

        debates_bert[issue][debate] = {
            'title': title_id,
            'posts': posts_curr,
            'stances': debates[issue][debate]['stances'],
            'authors': debates[issue][debate]['authors']
        }


bert_text = np.array(bert_text, dtype=object)
print(sentence_id, bert_text.shape)
print("max_title", max_title, "max_sentence", max_sentence)

if sys.argv[1] == "bert-base":
    with open("data/debate.org/debates_bert.json", "w") as fp:
        json.dump(debates_bert, fp)
    np.save("data/debate.org/bert_encode.npy", bert_text)
elif sys.argv[1]  == "bert-tiny":
    with open("data/debate.org/debates_bert_tiny.json", "w") as fp:
        json.dump(debates_bert, fp)
    np.save("data/debate.org/bert_encode_tiny.npy", bert_text)
elif sys.argv[1]  == "bert-mini":
    with open("data/debate.org/debates_bert_mini.json", "w") as fp:
        json.dump(debates_bert, fp)
    np.save("data/debate.org/bert_encode_mini.npy", bert_text)
elif sys.argv[1]  == "bert-small":
    with open("data/debate.org/debates_bert_small.json", "w") as fp:
        json.dump(debates_bert, fp)
    np.save("data/debate.org/bert_encode_small.npy", bert_text)
