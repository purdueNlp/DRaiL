import json
from collections import Counter

debates = json.load(open("data/debate.org/debates_preprocessed.json"))
user_info = json.load(open("data/debate.org/user_info.json"))
issues_pro = json.load(open("data/debate.org/issues_pro.json"))
issues_con = json.load(open("data/debate.org/issues_con.json"))

guessed_stance = {'abortion': set([]), 'guns': set([])}

topics = {
    'abortion': {
        'file': 'data/debate.org/abortion_stance.txt',
        'debates': {},
        'ideologies': {'pro': [], 'con': []},
        'immigration': {'pro': [], 'con': []},
        'guns': {'pro': [], 'con': []},
    },
    'guns': {
        'file': 'data/debate.org/guns_stance.txt',
        'debates': {},
        'ideologies': {'pro': [], 'con': []},
        'abortion': {'pro': [], 'con': []},
        'immigration': {'pro': [], 'con': []},
    },
}
'''
'immigration': {
    'file': 'data/debate.org/immigration_stance.txt',
    'debates': {},
    'ideologies': {'pro': [], 'con': []},
    'abortion': {'pro': [], 'con': []},
    'guns': {'pro': [], 'con': []},
}
'''

def get_points_issues(author):
    conservative_points = 0; liberal_points = 0
    for issue in issues_pro:
        if author in issues_pro[issue]:
            if issue in ['Abortion', 'Affirmative Action', 'Animal Rights', 'Barack Obama', 'Civil Unions', 'Drug Legalization', 'Environmental Protection',
                         'Euthanasia', 'Gay Marriage', 'Global Warming Exists', 'Globalization', 'Labor Union', 'Legalized Prostitution', 'Medicaid & Medicare',
                         'Medical Marijuana', 'Minimum Wage', 'National Health Care', 'Social Programs', 'Social Security', 'Term Limits', 'Welfare', 'Occupy Movement',
                         'Redistribution']:
                liberal_points += 1
            elif issue in ['Gun Rights', 'Border Fence', 'Death Penalty', 'Electoral College', 'Military Intervention', 'Racial Profiling', 'War in Afghanistan', 'War on Terror']:
                conservative_points += 1
    for issue in issues_con:
        if author in issues_con[issue]:
            if issue in ['Abortion', 'Affirmative Action', 'Animal Rights', 'Barack Obama', 'Civil Unions', 'Drug Legalization', 'Environmental Protection',
                         'Euthanasia', 'Gay Marriage', 'Global Warming Exists', 'Globalization', 'Labor Union', 'Legalized Prostitution', 'Medicaid & Medicare',
                         'Medical Marijuana', 'Minimum Wage', 'National Health Care', 'Social Programs', 'Social Security', 'Term Limits', 'Welfare', 'Occupy Movement',
                         'Redistribution']:
                conservative_points += 1
            elif issue in ['Gun Rights', 'Border Fence', 'Death Penalty', 'Electoral College', 'Military Intervention', 'Racial Profiling', 'War in Afghanistan', 'War on Terror']:
                liberal_points += 1
    return liberal_points, conservative_points

for topic in topics:
    with open(topics[topic]['file']) as fp:
        for line in fp:
            debate, post_id, post_pos, stance = line.strip().split()
            if debate not in topics[topic]['debates']:
                topics[topic]['debates'][debate] = {'even': None, 'odd': None}
            if int(post_pos) % 2 == 0:
                author = debates[topic][debate]['authors']['con']
                topics[topic]['debates'][debate]['even'] = stance
                if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                    if int(post_pos) == 2:
                        topics[topic]['ideologies'][stance].append(user_info[author]['Ideology'])
            else:
                author = debates[topic][debate]['authors']['pro']
                topics[topic]['debates'][debate]['odd'] = stance
                if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                    if int(post_pos) == 1:
                        topics[topic]['ideologies'][stance].append(user_info[author]['Ideology'])

                if author in issues_pro['Abortion'] and topic != "abortion":
                    topics[topic]['abortion'][stance].append('pro-choice')
                elif author in issues_con['Abortion'] and topic != "abortion":
                    topics[topic]['abortion'][stance].append('pro-life')

                if author in issues_pro['Gun Rights'] and topic != "guns":
                    topics[topic]['guns'][stance].append('gun-rights')
                elif author in issues_con['Gun Rights'] and topic != "guns":
                    topics[topic]['guns'][stance].append('gun-control')

                if author in issues_pro['Border Fence'] and topic != "immigration":
                    topics[topic]['immigration'][stance].append('border-fence')
                elif author in issues_con['Gun Rights'] and topic != "immigration":
                    topics[topic]['immigration'][stance].append('no-border-fence')

    new_ideos = []

    has_two = 0; agree = 0; disagree = 0; not_issue_but_ideo = 0; not_issue_but_other_issue = 0
    for debate in topics[topic]['debates']:
        if topics[topic]['debates'][debate]['even'] is not None and topics[topic]['debates'][debate]['odd'] is not None:
            has_two += 1
            if topics[topic]['debates'][debate]['even'] == topics[topic]['debates'][debate]['odd']:
                agree += 1
            else:
                disagree += 1
        else:
            if topics[topic]['debates'][debate]['even'] is None:
                ideo = None
                author = debates[topic][debate]['authors']['con']
                if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                    not_issue_but_ideo += 1
                    new_ideos.append(user_info[author]["Ideology"])
                    if user_info[author]["Ideology"] in ["Liberal", "Conservative", "Progressive", "Libertarian", "Socialist"]:
                        for post_id in range(1, len(debates[topic][debate]['posts']), 2):
                            if topic == "abortion" and user_info[author]["Ideology"] in ["Liberal", "Progressive", "Socialist"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                            elif topic == "abortion" and user_info[author]["Ideology"] == "Conservative":
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                            elif topic == 'guns' and user_info[author]["Ideology"] in ["Liberal", "Progressive", "Socialist"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                            elif topic == 'guns' and user_info[author]["Ideology"] in ["Conservative", "Libertarian"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                        ideo = user_info[author]["Ideology"]
                lib, con = get_points_issues(author)
                if ideo is None and (lib > con or con > lib):
                    for post_id in range(1, len(debates[topic][debate]['posts']), 2):
                        if topic == "abortion" and lib > con:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                        elif topic == "abortion" and con > lib:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                        elif topic == 'guns' and lib > con:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                        elif topic == 'guns' and con > lib:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
            else:
                ideo = None
                author = debates[topic][debate]['authors']['pro']
                if author in user_info and "Ideology" in user_info[author] and user_info[author]["Ideology"] != "Not Saying":
                    not_issue_but_ideo += 1
                    new_ideos.append(user_info[author]["Ideology"])
                    if user_info[author]["Ideology"] in ["Liberal", "Conservative", "Progressive", "Libertarian", "Socialist"]:
                        for post_id in range(0, len(debates[topic][debate]['posts']), 2):
                            if topic == "abortion" and user_info[author]["Ideology"] in ["Liberal", "Progressive", "Socialist"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                            elif topic == "abortion" and user_info[author]["Ideology"] == "Conservative":
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                            elif topic == 'guns' and user_info[author]["Ideology"] in ["Liberal", "Progressive", "Socialist"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                            elif topic == 'guns' and user_info[author]["Ideology"] in ["Conservative", "Libertarian"]:
                                guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                        ideo = user_info[author]["Ideology"]
                lib, con = get_points_issues(author)
                if ideo is None and (lib > con or con > lib):
                    for post_id in range(0, len(debates[topic][debate]['posts']), 2):
                        if topic == "abortion" and lib > con:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))
                        elif topic == "abortion" and con > lib:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                        elif topic == 'guns' and lib > con:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'con'))
                        elif topic == 'guns' and con > lib:
                            guessed_stance[topic].add((debate, "{}_{}".format(debate, post_id + 1), str(post_id + 1), 'pro'))


    print("Has-Two", topic, has_two, "/", len(topics[topic]['debates']), has_two / len(topics[topic]['debates']))
    print("Agree", topic, agree, "/", has_two, agree / has_two)
    print("Disagree", topic, disagree, "/", has_two, disagree / has_two)

    print("PRO ideo", Counter(topics[topic]['ideologies']['pro']))
    print("CON ideo", Counter(topics[topic]['ideologies']['con']))

    for _stance in ["pro", "con"]:
        for _topic in ['abortion', 'guns', 'immigration']:
            if _topic in topics[topic]:
                print(_stance, _topic, Counter(topics[topic][_topic][_stance]))

    print("Not issue but ideo", not_issue_but_ideo)
    print("Not issue but other", not_issue_but_other_issue)
    print(Counter(new_ideos))
    print("----------------------------")

print(len(guessed_stance['abortion']), len(guessed_stance['guns']))

for issue in guessed_stance:
    with open("data/debate.org/{}_guessed_stance.txt".format(issue), "w") as fp:
        for elem in guessed_stance[issue]:
            fp.write("\t".join(elem) + "\n")
