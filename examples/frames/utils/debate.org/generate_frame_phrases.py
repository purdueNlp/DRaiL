from pytorch_transformers import BertTokenizer
import re
import numpy as np
import json
import argparse
from pytorch_transformers import BertModel, BertConfig
import torch

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    args = parser.parse_args()
    return args

def pad_and_mask(vector, max_seq_len):
    pad_token = 0
    mask = [1] * len(vector)
    if len(vector) < max_seq_len:
        vector += [pad_token] * (max_seq_len - len(vector))
        mask += [0] * (max_seq_len - len(mask))
    return (vector, mask)

def _get_long_tensor(inp):
    return torch.cuda.LongTensor(inp)

def _get_variable(tensor):
    var = torch.autograd.Variable(tensor)
    var = var.cuda()
    return var

def embed_sentence(bert_model, bert_encoding, sent_id, CLS, SEP, MAX_SENT):
    bert_sent = [CLS] + bert_encoding[sent_id][:MAX_SENT] + [SEP]
    bert_sent, bert_mask = pad_and_mask(bert_sent, MAX_SENT + 2)
    n_sent = len(bert_sent)
    bert_segment = [0] * n_sent


    input_ids = _get_variable(_get_long_tensor([bert_sent]))
    input_mask = _get_variable(_get_long_tensor([bert_mask]))
    segment_ids = _get_variable(_get_long_tensor([bert_segment]))

    hidden_states, output = bert_model(input_ids, attention_mask=input_mask,
                         token_type_ids=segment_ids, position_ids=None,
                         head_mask=None)
    return (hidden_states[0].detach().cpu().numpy(), n_sent)

def main():
    args = parse_arguments()
    MAX_SENT = 33
    CLS = 101
    SEP = 102
    CONFIG = json.load(open(args.config))['models'][0]

    frames_bert = {}
    frames = json.load(open("data/debate.org/frame_phrases.json"))

    sentence_id = 0
    bert_text = []
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)

    max_phrase = 0

    for issue in frames:
        print(issue)
        frames_bert[issue] = {}
        for frame in frames[issue]:
            print("\t" + frame)
            frames_bert[issue][frame] = {}
            for perspective in frames[issue][frame]:
                print("\t\t" + perspective)
                frames_bert[issue][frame][perspective] = []

                for phrase in frames[issue][frame][perspective]:

                    phrase_bert = tokenizer.encode(phrase)
                    max_phrase = max(max_phrase, len(phrase_bert))

                    bert_text.append(phrase_bert)

                    frames_bert[issue][frame][perspective].append(sentence_id)
                    sentence_id += 1

    bert_text = np.array(bert_text, dtype=object)
    print(sentence_id, bert_text.shape)
    print("max_phrase", max_phrase)

    with open("data/debate.org/frames_bert.json", "w") as fp:
        json.dump(frames_bert, fp)

    np.save("data/debate.org/bert_frames_encode.npy", bert_text)

    pretrained_dict = torch.load(args.savedir, map_location=lambda storage, loc: storage)
    bert_config = BertConfig.from_pretrained("bert-base-uncased")
    bert_model = BertModel.from_pretrained("bert-base-uncased")

    bert_model_dict = bert_model.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in bert_model_dict}
    bert_model_dict.update(pretrained_dict)
    bert_model.load_state_dict(bert_model_dict)
    bert_model = bert_model.cuda()

    resulting_embeddings =\
        np.zeros((bert_text.shape[0], MAX_SENT + 2, bert_config.hidden_size))
    resulting_sizes = np.zeros((bert_text.shape[0]))

    for issue in frames_bert:
        print(issue)
        for frame in frames_bert[issue]:
            print("\t" + frame)
            for perspective in frames_bert[issue][frame]:
                print("\t\t" + perspective)
                for phrase_id in frames_bert[issue][frame][perspective]:
                    embed_sent, n_sent = embed_sentence(bert_model, bert_text, phrase_id, CLS, SEP, MAX_SENT)
                    resulting_embeddings[phrase_id] = embed_sent
                    resulting_sizes[phrase_id] = n_sent

    np.save("data/debate.org/bert_frames_paraembed.npy", resulting_embeddings)
    np.save("data/debate.org/bert_frames_paraembed_sz.npy", resulting_sizes)

if __name__ == "__main__":
    main()
