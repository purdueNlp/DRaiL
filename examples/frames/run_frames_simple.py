# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse
import random
import torch
import json
import logging.config

from sklearn.metrics import *

from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.global_latent_learner import GlobalLatentLearner
from drail.learn.local_learner import LocalLearner

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory", type=str, required=True)
    parser.add_argument("--gpu_index",  type=int, help="gpu index")
    parser.add_argument("--infer_only", help="inference only", default=False, action="store_true")
    parser.add_argument("--rule", help="rule file", type=str, required=True)
    parser.add_argument("--config", help="config file", type=str, required=True)
    parser.add_argument("--loss_fn", help="loss_fn", type=str, required=True)
    parser.add_argument("--debug", help='debug mode', default=False, action="store_true")
    parser.add_argument("--lrate", help="learning rate", type=float, default=1e-3)
    parser.add_argument("--savedir", help="directory to save model", type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', default=False, action='store_true')
    parser.add_argument('--train_only', help='run training only', default=False, action='store_true')
    parser.add_argument('-m', help='mode: [global|local]', dest='mode', type=str, default='local')
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true', default=False)
    parser.add_argument('--latent', help='use latent variables', dest='latent', action='store_true', default=False)
    parser.add_argument("--folds", help="path to folds json", type=str, required=True)
    parser.add_argument("--filter_on", help="predicate to filter on", type=str, required=True)
    parser.add_argument("--opt_pred", help="predicate to optimize", type=str, required=True)
    parser.add_argument("--data_feats", help="path to data feats", type=str, default=None)
    parser.add_argument("--frame_feats", help="path to frame feats", type=str, required=False, default=None)
    parser.add_argument("--subframe_feats", help="path to subframe feats", type=str, required=False, default=None)
    parser.add_argument('--logging_config', help='logging configuration file', type=str, default=None)
    parser.add_argument('--start_fold', type=int, default=0)
    parser.add_argument('--end_fold', type=int, default=None)
    parser.add_argument('--drop_scores', help='drop predicted scores', dest='drop_scores', default=False, action='store_true')
    parser.add_argument("--scoredir", help="directory to save scores", type=str, required=False)
    parser.add_argument("--word_emb", help="word_emb", type=str, required=False, default=None)
    parser.add_argument('--extract_embeddings', help='extract_embeddings', dest='extract_embeddings', default=False, action='store_true')
    parser.add_argument('--use_elmo', help='use elmo', dest='use_elmo', default=False, action='store_true')
    parser.add_argument('--ad3', help='use ad3', dest='use_ad3', default=False, action='store_true')
    args = parser.parse_args()
    return args

def train(folds, avoid):
    ret = []
    for j in folds:
        if j not in avoid:
            ret += folds[j]
    return ret

def main():
    args = parse_arguments()

    WORD_EMB_F = args.word_emb
    WORD2ID_F = os.path.join(args.dir, "word2id.json")
    INDICATOR2ID_F = os.path.join(args.dir, "indicator2id_0.01.json")
    DATA_F =  args.data_feats
    #EMBEDDINS_F = "data/subframe_media_embeddings.json"
    #MAPPINGS_F = "data/mappings.json"

    FE_PATH = args.dir.replace('data', 'feat').replace('media_bias/', '').replace('4forums/', '')
    NE_PATH = args.dir.replace('data', 'neuro').replace('media_bias/', '').replace('4forums/', '')
    print("FE_PATH", FE_PATH)
    print("NE_PATH", NE_PATH)

    optimizer = "AdamW"
    #optimizer = "SGD"

    # Select what gpu to use
    use_gpu = False
    if args.gpu_index is not None:
        use_gpu = True
        torch.cuda.set_device(args.gpu_index)

    if args.mode == 'global':
        if not args.latent:
            learner=GlobalLearner(learning_rate=args.lrate, use_gpu=use_gpu, gpu_index=args.gpu_index, loss_fn=args.loss_fn, inference_limit=None, ad3=args.use_ad3)
        else:
            learner=GlobalLatentLearner(learning_rate=args.lrate, use_gpu=use_gpu, gpu_index=args.gpu_index, inference_limit=None, ad3=args.use_ad3, loss_fn=args.loss_fn)
    else:
        infer_algorithm = "ilp"
        if args.use_ad3:
            infer_algorithm = "ad3"
        learner=LocalLearner(inference_limit=None, infer_algorithm=infer_algorithm)

    learner.compile_rules(args.rule)
    db=learner.create_dataset(args.dir)

    curr_folds = json.load(open(args.folds))

    if args.end_fold is None:
        args.end_fold = len(curr_folds)

    embeddings = {}

    all_f1s = {}; all_precisions = {}; all_recalls = {}

    for i in range(args.start_fold, args.end_fold):
        logger.info("Fold {}".format(i))

        dev_fold = (i + 1) % args.end_fold

        test_elems = curr_folds[str(i)]
        dev_elems = curr_folds[str(dev_fold)]
        train_elems = train(curr_folds, list(map(str, [i, dev_fold])))

        if args.debug:
            train_elems = train_elems[:3]
            dev_elems = dev_elems[:3]
            test_elems = test_elems[:3]
        logger.info("Debate folds: {}, {}, {}".format(len(train_elems), len(dev_elems), len(test_elems)))

        print(len(train_elems), len(dev_elems), len(test_elems))
        #print(train_elems)
        #exit()

        # debate.org folds are on debate, whereas 4forums are on posts (chang's folds)
        if args.dir.endswith('debate.org') or args.dir.endswith('debate.org/'):
            db.add_filters(filters=[
                (args.filter_on, "isTrain", "debateId_1", train_elems),
                (args.filter_on, "isDev", "debateId_1", dev_elems),
                (args.filter_on, "isTest", "debateId_1", test_elems),
                (args.filter_on, "isDummy", "debateId_1", train_elems[:10])
            ])
        elif args.dir.endswith('4forums') or args.dir.endswith('4forums/'):
            db.add_filters(filters=[
                (args.filter_on, "isTrain", "postId_2", train_elems),
                (args.filter_on, "isDev", "postId_2", dev_elems),
                (args.filter_on, "isTest", "postId_2", test_elems),
                (args.filter_on, "isDummy", "postId_2", train_elems[:10])
            ])
        elif args.filter_on == "HasEvent":
            db.add_filters(filters=[
                (args.filter_on, "isTrain", "eventId_2", train_elems),
                (args.filter_on, "isDev", "eventId_2", dev_elems),
                (args.filter_on, "isTest", "eventId_2", test_elems),
                (args.filter_on, "isDummy", "eventId_2", train_elems[:10])
            ])
        else:
            # media bias case
            db.add_filters(filters=[
                (args.filter_on, "isTrain", "articleId_1", train_elems),
                (args.filter_on, "isDev", "articleId_1", dev_elems),
                (args.filter_on, "isTest", "articleId_1", test_elems),
                (args.filter_on, "isDummy", "articleId_1", train_elems[:10])
            ])


        learner.build_feature_extractors(db,
                                         filters=[(args.filter_on, "isDummy", 1)],
                                         data_f=DATA_F,
                                         word2id_f=WORD2ID_F,
                                         word_emb_f=WORD_EMB_F,
                                         frame2feat_f=args.frame_feats,
                                         subframe2feat_f=args.subframe_feats,
                                         #mapping_f=MAPPINGS_F,
                                         #embeddings_f=EMBEDDINS_F,
                                         #use_elmo=args.use_elmo,
                                         indicator2id_f=INDICATOR2ID_F,
                                         femodule_path=FE_PATH)
        learner.set_savedir(os.path.join(args.savedir, "f{0}".format(i)))
        learner.build_models(db, args.config, netmodules_path=NE_PATH)

        if args.mode == "local":
            if args.continue_from_checkpoint:
                learner.init_models()

            if not args.infer_only:
                    learner.train(db,
                              train_filters=[(args.filter_on, "isTrain", 1)],
                              dev_filters=[(args.filter_on, "isDev", 1)],
                              test_filters=[(args.filter_on, "isTest", 1)],
                              scale_data=False,
                              optimizer=optimizer)
            if not args.train_only:
                learner.extract_data(db, extract_test=True, test_filters=[(args.filter_on, "isTest", 1)])
                res, heads = learner.predict(None, fold_filters=[(args.filter_on, "isTest", 1)], fold='test', get_predicates=True, scale_data=False)
                if args.extract_embeddings:
                    embeddings_test = learner.extract_rule_embeddings(None, fold='test')
                    embeddings.update(embeddings_test)
                if args.drop_scores:
                    learner.drop_scores(db, fold='test', fold_filters=[(args.filter_on, "isTest", 1)],
                                        output=os.path.join("{}".format(args.scoredir), "f{}.csv".format(i)), heads=heads)

        elif args.mode == "global":
            learner.extract_instances(db, train_filters=[(args.filter_on, "isTrain", 1)],
                                          dev_filters=[(args.filter_on, "isDev", 1)],
                                          test_filters=[(args.filter_on, "isTest", 1)],
                                          extract_train=not args.infer_only,
                                          extract_dev=not args.infer_only,
                                          extract_test=True)
            '''
            learner.extract_data(
                    db,
                    train_filters=[(args.filter_on, "isTrain", 1)],
                    dev_filters=[(args.filter_on, "isDev", 1)],
                    test_filters=[(args.filter_on, "isTest", 1)],
                    extract_train=not args.infer_only,
                    extract_dev=not args.infer_only,
                    extract_test=True)
            '''

            res, heads = learner.train(
                    db,
                    train_filters=[(args.filter_on, "isTrain", 1)],
                    dev_filters=[(args.filter_on, "isDev", 1)],
                    test_filters=[(args.filter_on, "isTest", 1)],
                    opt_predicates=set([args.opt_pred]),
                    loss_augmented_inference=args.delta,
                    continue_from_checkpoint=args.continue_from_checkpoint,
                    inference_only=args.infer_only,
                    #hot_start=False,
                    scale_data=False,
                    weight_classes=True,
                    patience=5,
                    optimizer=optimizer)

        for m in res.metrics:
            if m == "encoding_time" or m == "solving_time" or m == "HasSubframe":
                continue
            y_gold = res.metrics[m]['gold_data']
            y_pred = res.metrics[m]['pred_data']
            if len(y_gold) > 0 and (len(y_gold) == len(y_pred)):
                logger.info(classification_report(y_gold, y_pred, digits=4))

                acc_score = accuracy_score(y_gold, y_pred)
                prec, recall, f1, _= precision_recall_fscore_support(y_gold, y_pred, average='macro')
                logger.info("TEST Acc {}, Macro Prec {}, Recall {}, F1 {}".format(acc_score, prec, recall, f1))
                if m not in all_f1s:
                    all_f1s[m] = []; all_precisions[m] = []; all_recalls[m] = []
                all_f1s[m].append(f1)
                all_precisions[m].append(prec)
                all_recalls[m].append(recall)
            else:
                logger.info("Skipping predicate: {}".format(m))


    print("PREC", all_precisions)
    print("RECALL", all_recalls)
    print("F1", all_f1s)

if __name__ == "__main__":
    args = parse_arguments()
    if args.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(args.logging_config)))
    # Reproducibility
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)
    main()
