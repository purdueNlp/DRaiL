from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import json
import numpy as np

class DebateFrames_ft(FeatureExtractor):
    def __init__(self, data_f, word2id_f, word_emb_f,
                 frame2feat_f, subframe2feat_f,
                 max_seq_title=55, max_seq_sent=150,
                 max_seq_post=450):
        super(DebateFrames_ft, self).__init__()

        self.data_f = data_f
        self.max_seq_title = max_seq_title
        self.max_seq_sent = max_seq_sent
        self.max_seq_post = max_seq_post

        # word2id for GloVe
        self.word2id_f = word2id_f
        self.word_emb_f = word_emb_f
        
        # Features
        self.frame2feat_f = frame2feat_f
        self.subframe2feat_f = subframe2feat_f

    def build(self):
        self.stance_idx = {'con': 0, 'pro': 1}

        if self.data_f:
            self.debates = json.load(open(self.data_f))

        if self.word_emb_f:
            # Load word embeddings from txt file
            self.word2idx = json.load(open(self.word2id_f))
            vocabulary = set(self.word2idx.keys())
            self.vocab_size = len(vocabulary)

            _, self.word_emb_size, self.word_dict =\
                utils.embeddings_dictionary_txt(self.word_emb_f, vocabulary=vocabulary, debug=False)

        if self.frame2feat_f:
            self.frame2feat = json.load(open(self.frame2feat_f))
        if self.subframe2feat_f:
            self.subframe2feat = json.load(open(self.subframe2feat_f))

    def frame_feats(self, rule_grd, index=0):
        has_post = rule_grd.get_body_predicates("HasPost")[index]
        has_issue = rule_grd.get_body_predicates("HasIssue")[index]
        debate_id, issue_id = has_issue['arguments']
        debate_id, post_id, post_pos = has_post['arguments']
        return self.frame2feat[issue_id][post_id]

    def subframe_feats(self, rule_grd, index=0):
        has_post = rule_grd.get_body_predicates("HasPost")[index]
        has_issue = rule_grd.get_body_predicates("HasIssue")[index]
        debate_id, issue_id = has_issue['arguments']
        debate_id, post_id, post_pos = has_post['arguments']
        return self.subframe2feat[issue_id][post_id]

    def frame_subframe_feats(self, rule_grd):
        return self.frame_feats(rule_grd, 0) + self.subframe_feats(rule_grd, 0)

    def frame_subframe_feats_2(self, rule_grd):
        return self.frame_feats(rule_grd, 1) + self.subframe_feats(rule_grd, 1)

    def extract_multiclass_head(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']
        head = rule_grd.get_head_predicate()
        if head['name'] in ['HasStance', 'AbortionStance', 'GunsStance', 'ImmigrationStance']:
            label = self.stance_idx[head['arguments'][-1]]
        return label

    def post_ids(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']
        has_post = rule_grd.get_body_predicates("HasPost")[0]
        debate_id, post_id, post_pos = has_post['arguments']
        if post_pos > 0:
            ret = self.debates[issue_id][debate_id]["posts"][post_pos - 1][:self.max_seq_post]
        else:
            ret = self.debates[issue_id][debate_id][post_id][:self.max_seq_post]
        #print(ret)
        return ret

    def post_words(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']
        has_post = rule_grd.get_body_predicates("HasPost")[0]
        debate_id, post_id, post_pos = has_post['arguments']
        ret = self.debates[issue_id][debate_id]["post_words"][post_pos - 1][:self.max_seq_post]
        #print(ret)
        return ret

    def post_ids_2(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']
        has_post = rule_grd.get_body_predicates("HasPost")[1]
        debate_id, post_id, post_pos = has_post['arguments']
        if post_pos > 0:
            ret = self.debates[issue_id][debate_id]["posts"][post_pos - 1][:self.max_seq_post]
        else:
            ret = self.debates[issue_id][debate_id][post_id][:self.max_seq_post]
        #print(ret)
        return ret

    def post_words_2(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        debate_id, issue_id = has_issue['arguments']
        has_post = rule_grd.get_body_predicates("HasPost")[1]
        debate_id, post_id, post_pos = has_post['arguments']
        ret = self.debates[issue_id][debate_id]["post_words"][post_pos - 1][:self.max_seq_post]
        #print(ret)
        return ret
