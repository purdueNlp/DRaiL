from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
import json
import numpy as np

class ArticleFrames_ft(FeatureExtractor):
    def __init__(self, data_f, word2id_f, word_emb_f,
                 frame2feat_f, subframe2feat_f,
                 indicator2id_f,
                 max_seq_par=512, max_seq_art=512):
        super(ArticleFrames_ft, self).__init__()

        self.data_f = data_f
        self.max_seq_par = max_seq_par
        self.max_seq_art = max_seq_art

        # word2id for GloVe
        self.word2id_f = word2id_f
        self.word_emb_f = word_emb_f

        # Features
        self.frame2feat_f = frame2feat_f
        self.subframe2feat_f = subframe2feat_f

        self.indicator2id_f = indicator2id_f

    def build(self):
        self.bias_idx = {'left': 0, 'right': 1}

        if self.data_f:
            self.articles = json.load(open(self.data_f))
        if self.indicator2id_f:
            self.indicator2idx = json.load(open(self.indicator2id_f))

        if self.word_emb_f:
            # Load word embeddings from txt file
            self.word2idx = json.load(open(self.word2id_f))
            vocabulary = set(self.word2idx.keys())
            self.vocab_size = len(vocabulary)

            _, self.word_emb_size, self.word_dict =\
                utils.embeddings_dictionary_txt(self.word_emb_f, vocabulary=vocabulary, debug=False)

        if self.frame2feat_f:
            self.frame2feat = json.load(open(self.frame2feat_f))
        if self.subframe2feat_f:
            self.subframe2feat = json.load(open(self.subframe2feat_f))

    def frame_feats(self, rule_grd, index=0):
        has_issue = rule_grd.get_body_predicates("HasIssue")[index]
        article_id, issue = has_issue['arguments']
        ret = None
        for par in self.articles[issue][article_id]:
            if ret is None:
                ret = self.frame2feat[issue][par]
            else:
                ret = [x or y for (x, y) in zip(ret, self.frame2feat[issue][par])]
        return ret

    def subframe_feats(self, rule_grd, index=0):
        has_issue = rule_grd.get_body_predicates("HasIssue")[index]
        article_id, issue = has_issue['arguments']
        ret = None
        for par in self.articles[issue][article_id]:
            if ret is None:
                ret = self.subframe2feat[issue][par]
            else:
                ret = [x or y for (x, y) in zip(ret, self.subframe2feat[issue][par])]
        return ret

    def bert_paragraph(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        article_id, issue = has_issue['arguments']

        has_par = rule_grd.get_body_predicates("HasPar")[0]
        _, par = has_par['arguments']

        ret = self.articles[issue][article_id][par]
        ret = [101] + ret[:510] + [102]
        mask = [1] * len(ret)
        if len(ret) < self.max_seq_art:
            ret += [0] * (self.max_seq_art - len(ret))
            mask += [0] * (self.max_seq_art - len(mask))
        return (ret, mask)

    def bert_article(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        article_id, issue = has_issue['arguments']
        ret = []
        for par in self.articles[issue][article_id]:
            ret += self.articles[issue][article_id][par]
        # Truncating and adding [CLS] text [SEP]
        ret = [101] + ret[:510] + [102]
        mask = [1] * len(ret)
        # Padding
        if len(ret) < self.max_seq_art:
            ret += [0] * (self.max_seq_art - len(ret))
            mask += [0] * (self.max_seq_art - len(mask))
        return (ret, mask)

    def hbilstm_article(self, rule_grd):
        has_issue = rule_grd.get_body_predicates("HasIssue")[0]
        article_id, issue = has_issue['arguments']
        ret = []
        for par in self.articles[issue][article_id]:
            ret.append(self.articles[issue][article_id][par])
        return ret

    def bilstm_paragraph(self, rule_grd):
        if rule_grd.has_body_predicate("HasIssue"):
            issue = rule_grd.get_body_predicates("HasIssue")[0]['arguments'][-1]
        else:
            issue = rule_grd.get_body_predicates("IsIssue")[0]['arguments'][-1]
        has_par = rule_grd.get_body_predicates("HasPar")[0]
        article_id, par = has_par['arguments']
        return self.articles[issue][article_id][par]

    def frame_subframe_feats(self, rule_grd):
        return self.frame_feats(rule_grd, 0) + self.subframe_feats(rule_grd, 0)

    def frame_subframe_feats_2(self, rule_grd):
        return self.frame_feats(rule_grd, 1) + self.subframe_feats(rule_grd, 1)

    def sub_indicator(self, rule_grd):
        if rule_grd.has_body_predicate("HasIssue"):
            issue = rule_grd.get_body_predicates("HasIssue")[0]['arguments'][-1]

        if rule_grd.has_body_predicate("IsCandidate"):
            pred = "IsCandidate"
        elif rule_grd.has_body_predicate("HasIndicator"):
            pred = "HasIndicator"
        elif rule_grd.has_body_predicate("IsIndicator"):
            issue = rule_grd.get_body_predicates("IsIndicator")[0]['arguments'][0]
            pred = "IsIndicator"

        pred = rule_grd.get_body_predicates(pred)[0]
        indicator = pred['arguments'][-1]
        ret = [0.0] * len(self.indicator2idx[issue])
        ret[self.indicator2idx[issue][indicator]] = 1.0
        #print(len(ret))
        return ret

    def subframe(self, rule_grd):
        if rule_grd.has_body_predicate("IsSubframe"):
            pred = "IsSubframe"
        elif rule_grd.has_body_predicate("HasSubframe"):
            pred = "HasSubframe"
        pred = rule_grd.get_body_predicates(pred)[0]
        subframe = pred['arguments'][-1]
        ret = [0.0] * 15 * 3
        ret[int(subframe)] = 1.0
        return ret

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] in ['HasBias']:
            label = self.bias_idx[head['arguments'][-1]]
        return label
