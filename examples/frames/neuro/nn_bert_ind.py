import logging

import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel, BertModel

from drail.neuro.nn_model import NeuralNetworks


class BertClassifier(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BertClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        #self.output_dim = 3
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.bert_model_name = self.config['bert_model']
        if "bert_model_type" in self.config:
            self.bert_model_name = self.config["bert_model_type"]
        bert_config = AutoConfig.from_pretrained(self.bert_model_name)

        if "shared_encoder" in self.config:
            name = self.config["shared_encoder"]
            self.bert_model = shared_params[name]["bert"]
        else:
            self.bert_model = AutoModel.from_pretrained(self.bert_model_name, add_pooling_layer=True)

        if "bert_freeze" in self.config and self.config["bert_freeze"]:
            for name, W in self.bert_model.named_parameters():
                print("freezing", name)
                W.requires_grad = False
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        if not "shared_input" in self.config:
            self.input2hidden = torch.nn.Linear(self.config["n_input"], self.config["n_hidden"])
        else:
            name = self.config["shared_input"]
            self.input2hidden = shared_params[name]["layer"]
            self.config["n_input"] = shared_params[name]["nin"]
            self.config["n_hidden"] = shared_params[name]["nout"]

        hidden_dim=bert_config.hidden_size + self.config["n_hidden"]

        if "shared_output" in self.config:
            name = self.config["shared_output"]
            self.hidden2label = shared_params[name]["layer"]
        else:
            self.hidden2label = torch.nn.Linear(hidden_dim, self.output_dim)

        if self.use_gpu:
            self.bert_model = self.bert_model.cuda()
            self.dropout = self.dropout.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        # Prepare input for bert
        input_ids = []; input_mask = []; segment_ids = []
        input_indicators = []
        for elem in x['input']:
            input_ids.append(elem[0][0])
            input_mask.append(elem[0][1])
            segment_ids.append([0] * len(elem[0][0]))
            input_indicators.append(elem[1])

        input_ids = self._get_variable(self._get_long_tensor(input_ids))
        input_mask = self._get_variable(self._get_long_tensor(input_mask))
        segment_ids = self._get_variable(self._get_long_tensor(segment_ids))

        input_indicators = self._get_variable(self._get_float_tensor(input_indicators))

        #print(input_ids.size(), input_mask.size(), segment_ids.size())

        # Run model
        #print(input_ids.size())
        outputs = self.bert_model(input_ids, attention_mask=input_mask,
                                  token_type_ids=segment_ids, position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)

        input_indicators = self.input2hidden(input_indicators)
        input_indicators = F.relu(input_indicators)

        concat = torch.cat([pooled_output, input_indicators], dim=1)
        logits = self.hidden2label(concat)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas


