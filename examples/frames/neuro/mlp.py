import logging

import torch
import numpy as np
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks


class MLP(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(MLP, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        #bert_config = AutoConfig.from_pretrained('bert-base-uncased')
        #self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        if not "shared_input_1" in self.config:
            self.layer_inp_1 = torch.nn.Linear(self.config['n_input_1'], self.config['n_hidden_1'])
            self.n_hidden_1 = self.config['n_hidden_1']
        else:
            name = self.config["shared_input_1"]
            self.layer_inp_1 = shared_params[name]["layer"]
            self.n_hidden_1 = shared_params[name]["nout"]


        if "n_input_2" in self.config or "shared_input_2" in self.config:
            if not "shared_input_2" in self.config:
                self.layer_inp_2 = torch.nn.Linear(self.config['n_input_2'], self.config['n_hidden_2'])
                self.n_hidden_2 = self.config['n_hidden_2']
            else:
                name = self.config["shared_input_2"]
                self.layer_inp_2 = shared_params[name]["layer"]
                self.n_hidden_2 = shared_params[name]["nout"]

            if "n_concat_hidden" in self.config:
                self.layer_concat = torch.nn.Linear(self.n_hidden_1 + self.n_hidden_2, self.config['n_concat_hidden'])
                self.layer_classifier = torch.nn.Linear(self.config['n_concat_hidden'], self.output_dim)
            else:
                self.layer_classifier = torch.nn.Linear(self.n_hidden_1 + self.n_hidden_2, self.output_dim)

            if self.use_gpu:
                self.layer_inp_1 = self.layer_inp_1.cuda()
                self.layer_inp_2 = self.layer_inp_2.cuda()
                self.layer_classifier = self.layer_classifier.cuda()
                if "n_concat_hidden" in self.config:
                    self.layer_concat = self.layer_concat.cuda()
        else:
            self.layer_classifier = torch.nn.Linear(self.n_hidden_1, self.output_dim)

            if self.use_gpu:
                self.layer_inp_1 = self.layer_inp_1.cuda()
                self.layer_classifier = self.layer_classifier.cuda()


    def forward(self, x):
        if len(x['input'][0])  > 0:
            # Receives just one input
            inputs = [elem[0] for elem in x['input']]
            inputs = self._get_float_tensor(inputs)
            h = self.layer_inp_1(inputs)
            h = F.relu(h)

            if len(x['input'][0]) == 2:
                other_inputs = [elem[1] for elem in x['input']]
                other_inputs = self._get_float_tensor(other_inputs)
                h_2 = self.layer_inp_2(other_inputs)
                h_2 = F.relu(h_2)

                h = torch.cat([h, h_2], 1)
                if "n_concat_hidden" in self.config:
                    h = self.layer_concat(h)
                    h = F.relu(h)
        elif len(x['vector']) > 0:
            inputs = self._get_float_tensor(x['vector'])
            #print(inputs.shape)
            h = self.layer_inp_1(inputs)
            h = F.relu(h)

        #h = self.dropout(h)
        logits = self.layer_classifier(h)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
