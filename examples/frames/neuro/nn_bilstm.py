import torch
import numpy as np
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks

from drail.neuro.nn_utils import create_elmo
from allennlp.modules.elmo import batch_to_ids

class BiLSTM(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(BiLSTM, self).__init__(config, nn_id)
        #self.type = ModelType.Sequence
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        #print "output_dim", output_dim

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.dropout = torch.nn.Dropout(0.1)
        self.minibatch_size = self.config['batch_size']

        if "use_elmo" not in self.config or not self.config["use_elmo"]:
            self.embedding_layers, emb_dims = \
                self._embedding_inputs(rule_template, fe)
        else:
            if "shared_elmo" in self.config:
                name = self.config["shared_elmo"]
                self.elmo = shared_params[name]["elmo"]
            else:
                self.elmo = create_elmo(options_file=self.config["options_file"],
                                        weight_file=self.config["weight_file"])
            if "freeze_elmo" or self.config["freeze_elmo"]:
                for param in self.elmo.parameters():
                    param.requires_grad = False


        if "shared_lstm" in self.config:
            name = self.config["shared_lstm"]
            self.n_input_sequence = shared_params[name]["nin"]
            self.n_hidden_sequence = shared_params[name]["nout"]
            self.sequence_lstm = shared_params[name]["lstm"]
        else:
            self.n_input_sequence = self.config['n_input_sequence']
            self.n_hidden_sequence = self.config["n_hidden_sequence"]

            # LSTM for the sequence
            self.sequence_lstm =\
                    torch.nn.LSTM(input_size=self.n_input_sequence,
                                  hidden_size=self.n_hidden_sequence,
                                  bidirectional=True,
                                  batch_first=True)

        self.hidden2label =\
                torch.nn.Linear(self.n_hidden_sequence*2, self.output_dim)

        if self.use_gpu:
            self.sequence_lstm = self.sequence_lstm.cuda()
            self.hidden2label = self.hidden2label.cuda()

        self.hidden_bilstm = self.init_hidden_bilstm()

    def init_hidden_bilstm(self):
        var1 = torch.autograd.Variable(torch.zeros(2, self.minibatch_size,
                                                    self.n_hidden_sequence))
        var2 = torch.autograd.Variable(torch.zeros(2, self.minibatch_size,
                                                    self.n_hidden_sequence))

        if self.use_gpu:
            var1 = var1.cuda()
            var2 = var2.cuda()

        return (var1, var2)

    def forward(self, x):
        # Assuming only one embedding input
        if 'use_elmo' not in self.config or not self.config['use_elmo']:
            key = list(x['embedding'].keys())[0]
            seqs = x['embedding'][key]
            self.minibatch_size = len(seqs)

            # get the length of each seq in your batch
            seq_lengths = self._get_long_tensor(list(map(len, seqs)))

            # dump padding everywhere, and place seqs on the left.
            # NOTE: you only need a tensor as big as your longest sequence
            max_seq_len = seq_lengths.max()

            # Sort according to lengths
            seq_len_sorted, sorted_idx = seq_lengths.sort(descending=True)

            tensor_seq = torch.zeros((len(seqs), max_seq_len)).long()
            if self.use_gpu:
                tensor_seq = tensor_seq.cuda()
            for idx, (seq, seqlen) in enumerate(zip(seqs, seq_lengths)):
                seq = self._get_long_tensor(seq)
                tensor_seq[idx, :seqlen] = self._get_long_tensor(seq)

            # sort inputs
            tensor_seq = tensor_seq[sorted_idx]

            var_seq = self._get_variable(tensor_seq)
            seq_lengths = self._get_variable(seq_lengths)
            var_seq = self.embedding_layers[key](var_seq)

        else:
            x = [elem[0] for elem in x['input']]
            x = batch_to_ids(x)
            if self.use_gpu:
                x = x.cuda()
            x = self.elmo(x)
            tensor_seq = x['elmo_representations'][0]
            seq_lengths = (x['mask'] == True).sum(dim=1)
            # sort
            seq_len_sorted, sorted_idx = seq_lengths.sort(descending=True)
            tensor_seq = tensor_seq[sorted_idx]
            var_seq = self._get_variable(tensor_seq)
            seq_lengths = self._get_variable(seq_lengths)

        # pack padded sequences
        packed_input_seq = pack_padded_sequence(var_seq, list(seq_len_sorted.data), batch_first=True)
        # run lstm over sequence
        self.hidden_bilstm = self.init_hidden_bilstm()
        packed_output, self.hidden_bilstm = \
                self.sequence_lstm(packed_input_seq, self.hidden_bilstm)
        # unpack the output
        unpacked_output, _ = pad_packed_sequence(packed_output, batch_first=True)

        # Reverse sorting
        unpacked_output = torch.zeros_like(unpacked_output).scatter_(0, sorted_idx.unsqueeze(1).unsqueeze(1).expand(-1, unpacked_output.shape[1], unpacked_output.shape[2]), unpacked_output)

        # extract last timestep, since doing [-1] would get the padded zeros
        '''
        idx = (seq_lengths - 1).view(-1, 1).expand(
            unpacked_output.size(0), unpacked_output.size(2)).unsqueeze(1)
        lstm_output = unpacked_output.gather(1, idx).squeeze()

        if len(list(lstm_output.size())) == 1:
            lstm_output = lstm_output.unsqueeze(0)
        '''
        # Do global max pooling over timesteps
        lstm_output, _ = torch.max(unpacked_output, dim=1)
        lstm_output = self.dropout(lstm_output)
        logits = self.hidden2label(lstm_output)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas

