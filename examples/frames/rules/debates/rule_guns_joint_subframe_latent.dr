entity: "Debate",  arguments: ["debateId"::ArgumentType.UniqueString];
entity: "Post",  arguments: ["debateId"::ArgumentType.UniqueID];
entity: "PostId",  arguments: ["postId"::ArgumentType.UniqueString];
entity: "Issue", arguments: ["issue"::ArgumentType.UniqueString];
entity: "StanceType", arguments: ["stance"::ArgumentType.UniqueString];
entity: "Author", arguments: ["author"::ArgumentType.UniqueString];
entity: "Frame", arguments: ["frame"::ArgumentType.UniqueString];
entity: "SubFrame", arguments: ["frame"::ArgumentType.UniqueString];

predicate: "HasPost", arguments: [Debate, PostId, Post];
predicate: "HasIssue", arguments: [Debate, Issue];
predicate: "GunsStance", arguments: [Debate, PostId, Post, StanceType];
predicate: "IsAuthor", arguments: [Debate, PostId, Post, Author];
predicate: "Agree", arguments: [Debate, PostId, PostId];
predicate: "HasNext", arguments: [Post, Post];
predicate: "HasFrame", arguments: [Debate, PostId, Post, Frame];
predicate: "FrameCand", arguments: [Frame];
predicate: "HasSubFrame", arguments: [Debate, PostId, Post, SubFrame];
predicate: "SubFrameType", arguments: [SubFrame, Frame];

label: "StanceLabel", classes: 2, type: LabelType.Multiclass;
label: "FrameLabel", classes: 9, type: LabelType.Multiclass;
label: "SubFrameLabel", classes: 19, type: LabelType.Multiclass;

load: "HasPost", file: "has_post.txt";
load: "HasIssue", file: "has_issue.txt";
load: "GunsStance", file: "guns_stance_all.txt";
load: "StanceType", file: "stance_label.txt";
load: "StanceLabel", file: "stance_label.txt";
load: "IsAuthor", file: "is_author.txt";
load: "Agree", file: "guns_agreement_all.txt";
load: "HasNext", file: "has_next.txt";
load: "Frame", file: "frames_guns.txt";
load: "FrameCand", file: "frames_guns.txt";
load: "FrameLabel", file: "frames_guns.txt";
load: "HasFrame", file: "has_frame_seed_top2_guns.txt";
load: "SubFrame", file: "subframes_guns.txt";
load: "SubFrameLabel", file: "subframes_guns.txt";
load: "HasSubFrame", file: "post_has_subframe_seed_ALL_guns.txt";
load: "SubFrameType", file: "subframe_type_guns.txt";

femodule: "debate_frames_ft_v2";
feclass: "DebateFrames_ft";

latent: "HasSubFrame";

ruleset {
    // Post -> Stance
    rule: HasIssue(D, "guns") & HasPost(D, P, I) & GunsStance(D, P, I, S) => GunsStance(D, P, I, X^StanceLabel?),
    lambda: 1.0,
    network: "config.json",
    fefunctions: [
      embedding("post_ids", "word_dict", "vocab_size", "word_emb_size", "word2idx")
    ],
    target: P;

    // Post & Post -> Agree
    rule: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) => Agree(D, P, Q)^?,
    lambda: 1.0,
    network: "config.json",
    fefunctions: [
      embedding("post_ids", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
      embedding("post_ids_2", "word_dict", "vocab_size", "word_emb_size", "word2idx")
    ],
    target: P;

    // Post -> SubFrame
    rule: HasIssue(D, "guns") & HasPost(D, P, I) & GunsStance(D, P, I, S) & HasFrame(D, P, I, F) & SubFrameType(G, F) => HasSubFrame(D, P, I, G)^?,
    lambda: 1.0,
    network: "config.json",
    fefunctions: [
      embedding("post_ids", "word_dict", "vocab_size", "word_emb_size", "word2idx"),
      //vector("subframe_1hot")
      vector("subframe_embedding")
    ],
    target: P;

    // SubFrame -> Stance
    rule: HasIssue(D, "guns") & HasPost(D, P, I) & GunsStance(D, P, I, S) & HasSubFrame(D, P, I, F^SubFrameLabel?) => GunsStance(D, P, I, X^StanceLabel?),
    lambda: 1.0,
    network: "config.json",
    fefunctions: [
      //input("has_subframe_mc")
      input("subframe_embedding")
    ],
    target: P;

    // SubFrame & SubFrame -> Agree
    rule: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J)
          & HasSubFrame(D, P, I, F^SubFrameLabel?) & HasSubFrame(D, Q, J, G^SubFrameLabel?) => Agree(D, P, Q)^?,
    lambda: 1.0,
    network: "config.json",
    fefunctions: [
      //input("has_subframe_mc"),
      //input("has_subframe_mc_2")
      input("subframe_embedding"),
      input("subframe_embedding_2")
    ],
    target: P;

    // Agreement Constraints
    hardconstr: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) & Agree(D, P, Q)^? & GunsStance(D, P, I, "pro")^? => GunsStance(D, Q, J, "pro")^?;
    hardconstr: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) & Agree(D, P, Q)^? & GunsStance(D, P, I, "con")^? => GunsStance(D, Q, J, "con")^?;
    hardconstr: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) & ~Agree(D, P, Q)^? & GunsStance(D, P, I, "pro")^? => GunsStance(D, Q, J, "con")^?;
    hardconstr: HasIssue(D, "guns") & HasPost(D, P, I) & HasPost(D, Q, J) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) & ~Agree(D, P, Q)^? & GunsStance(D, P, I, "con")^? => GunsStance(D, Q, J, "pro")^?;

    // Get just the most prominent of each
    hardconstr: HasIssue(D, "guns") & HasPost(D, P, I) & GunsStance(D, P, I, S) => HasSubFrame(D, P, I, X^SubFrameLabel?);

    // Different ideo, different sub-frame
    //hardconstr: HasIssue(D, "guns") & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & HasNext(I, J) & SubFrameType(X, F) & (F != "Policy_Prescription_and_Evaluation") & (F != "Capacity_and_Resources") & ~Agree(D, P, Q)^? & HasSubFrame(D, P, I, X)^? => ~HasSubFrame(D, Q, J, X)^?;

    // Seed known subframes
    //hardconstr: HasIssue(D, "guns") & GunsStance(D, P, I, S) & HasFrame(D, P, I, F) & SubFrameType(G, F) & HasSubFrame(D, P, I, G) => HasSubFrame(D, P, I, G)^?;
    //hardconstr: HasIssue(D, "guns") & GunsStance(D, P, I, S) & HasSubFrame(D, P, I, G) => HasSubFrame(D, P, I, G)^?;

    // Author Constraints
    //hardconstr: HasIssue(D, "guns") & IsAuthor(D, P, I, A) & IsAuthor(D, Q, J, A) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & GunsStance(D, P, I, "pro")^? => GunsStance(D, Q, J, "pro")^?;
    //hardconstr: HasIssue(D, "guns") & IsAuthor(D, P, I, A) & IsAuthor(D, Q, J, A) & GunsStance(D, P, I, S) & GunsStance(D, Q, J, T) & GunsStance(D, P, I, "con")^? => GunsStance(D, Q, J, "con")^?;

} groupby: GunsStance.1;
