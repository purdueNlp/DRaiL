import torch
import numpy as np
from drail.neuro.nn_model import NeuralNetworks
import torch.nn.functional as F

class FF_1input(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(FF_1input, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim

    def build_architecture(self, rule_template, fe, shared_params):
        self.minibatch_size = self.config["batch_size"]

        # Can sum assuming only one of them is being used
        if "n_input_1" not in self.config:
            if len(rule_template.feat_inputs_sz) > 0:
                self.n_input_1 = rule_template.feat_inputs_sz[0]
            else:
                self.n_input_1 = rule_template.feat_vector_sz

        if "shared_layer1" in self.config:
            name = self.config["shared_layer1"]
            self.layer1 = shared_params[name]["layer"]
            n_out1 = shared_params[name]["nout"]
        elif "n_hidden_1" in self.config:
            self.layer1 = torch.nn.Linear(
                self.n_input_1, self.config["n_hidden_1"])
            n_out1 = self.config["n_hidden_1"]
        else:
            self.layer1 = None
            n_out1 = self.n_input_1

        self.hidden2label = torch.nn.Linear(
                    n_out1, self.output_dim)

        if self.use_gpu:
            if self.layer1 is not None:
                self.layer1 = self.layer1.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        if len(x['vector']) > 0:
            x1 = x['vector']
        else:
            x1 = [elem[0] for elem in x['input']]

        tensor1 = self._get_float_tensor(x1)
        var1 = self._get_grad_variable(tensor1)
        if self.layer1 is not None:
            hidden1 = self.layer1(var1)
        else:
            hidden1 = var1

        out = F.relu(hidden1)
        logits = self.hidden2label(out)
        probas = F.softmax(logits, dim=1)
        return logits, probas

