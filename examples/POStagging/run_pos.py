import sys
import os
import numpy as np
import torch
import argparse
from sklearn.metrics import *
import logging.config
import json
import random

from drail.learn.local_learner import LocalLearner
from drail.learn.global_learner import GlobalLearner

#torch.cuda.set_device(0)

def get_fold_ids(_dir, fold):
    filter_ids = []
    curr_path = os.path.join(_dir, '{0}.txt'.format(fold))
    with open(curr_path) as f:
        for line in f:
            filter_ids.append(line.strip())
    return filter_ids

def build_word_dic(filename):
    word_dic = {}
    with open(filename) as f:
        for line in f:
            try:
                wordid, word = line.strip().split()
                word_dic[int(wordid)] = word
            except:
                pass
    logger.info("word_dic lenght: {}".format(len(word_dic)))
    return word_dic

def get_train_words(filename_sent,
                    filename_tags, train_ids):
    train_words = []
    with open(filename_sent) as f:
        for line in f:
            wordid, sentenceid = line.strip().split()
            if sentenceid in train_ids:
                train_words.append(int(wordid))

    ret = []
    with open(filename_tags) as f:
        for line in f:
            wordid, tag = line.strip().split()
            if wordid in train_words:
                ret.append((wordid, tag))
    return ret

def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str,
                       required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str,
                       required=True)
    parser.add_argument('-m', help='mode: [global|local]', dest='mode', type=str,
                      default='local')
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('--delta', help='loss augmented inferece', dest='delta', action='store_true',
                      default=False)
    parser.add_argument('--lr', help='global learning rate', dest='global_lr', type=float)
    parser.add_argument('--te', help='twitter embedding',
                      dest='twitter_w2vbin_filename', default=None)
    parser.add_argument('--we', help='word embedding',
                      dest='w2vbin_filename', default=None)
    parser.add_argument('--pe', help='POS embedding',
                      dest='pos_w2vbin_filename', default=None)
    parser.add_argument('--debug', help='debug mode', dest='debug',
                      default=False, action='store_true')
    parser.add_argument('--logging_config', help='logging configuration file', type=str, default=None)
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--continue_from_checkpoint', help='continue from checkpoint in savedir', dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_argument('--inference-only', help='run inference only', dest='inference_only', default=False, action='store_true')
    parser.add_argument('--train-only', help='run training only', dest='train_only', default=False, action='store_true')
    parser.add_argument('--infer_algorithm', help='[AD3|ILP|viterbi]', dest='infer_algorithm', default="ILP")
    opts = parser.parse_args()

    if opts.mode == "global" and not opts.__dict__['global_lr']:
        logger.error("specify learning rate for global model")
        parser.print_help()
        exit(-1)

    if opts.mode not in ['global', 'local']:
        logger.error("specify a valid mode")
        parser.print_help()
        exit(-1)

    return opts

def seed_torch(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed) # if you are using multi-GPU.

def main():
    if opts.gpu_index:
        torch.cuda.set_device(opts.gpu_index)

    # Trying to make training deterministic
    seed_torch(1234)

    DATA_DIR = os.path.join(opts.dir, "data")
    train_ids = get_fold_ids(DATA_DIR, 'train')
    dev_ids = get_fold_ids(DATA_DIR, 'dev')
    test_ids = get_fold_ids(DATA_DIR, 'test')

    word_dic = build_word_dic(os.path.join(DATA_DIR, "is_word.txt"))
    train_words = []
    '''
    TO-DO: Refactor this so that it is deterministic across runs
    train_words = get_train_words(os.path.join(DATA_DIR, "in_sentence.txt"),
                                  os.path.join(DATA_DIR, "has_tag.txt"),
                                  train_ids)
    '''

    ## Now use learner class here
    if opts.mode == "global":
        learner=GlobalLearner(learning_rate=opts.global_lr,
                              use_gpu=(opts.gpu_index is not None),
                              gpu_index=opts.gpu_index,
                              ad3=opts.ad3,
                              loss_fn=opts.lossfn,
                              inference_limit=10)
    else:
        learner = LocalLearner(infer_algorithm=opts.infer_algorithm, inference_limit=10)

    logger.info("Compiling rules...")
    learner.compile_rules(opts.rules)

    logger.info("Loading dataset...")
    db = learner.create_dataset(DATA_DIR)

    if opts.debug:
        train_ids = train_ids[:20]
        dev_ids = dev_ids[:20]
        test_ids = test_ids[:20]

    # add splits
    db.add_filters(filters=[
        ("InSentence", "isDummy", "sentenceId_2", train_ids[0:1]),
        ("InSentence", "isTrain", "sentenceId_2", train_ids),
        ("InSentence", "isDev", "sentenceId_2", dev_ids),
        ("InSentence", "isTest", "sentenceId_2", test_ids)
        ])

    logger.info("Building feature extractors...")
    learner.build_feature_extractors(
            db,
            w2v_bin_filename=opts.w2vbin_filename,
            twitter_bin_filename=opts.twitter_w2vbin_filename,
            pos_bin_filename=opts.pos_w2vbin_filename,
            word_dic=word_dic,
            train_words=train_words,
            femodule_path=opts.dir,
            filters=[("InSentence", "isDummy", 1)])

    logger.info("Building neural nets...")
    learner.set_savedir(opts.savedir)
    learner.build_models(db, opts.config,
                         netmodules_path=opts.dir)

    if opts.mode == "global":
        learner.extract_data(
                db,
                train_filters=[("InSentence", "isTrain", 1)],
                dev_filters=[("InSentence", "isDev", 1)],
                test_filters=[("InSentence", "isTest", 1)],
                extract_train=not opts.inference_only,
                extract_dev=not opts.inference_only,
                extract_test=not opts.train_only)
        res, heads = learner.train(
                db,
                train_filters=[("InSentence", "isTrain", 1)],
                dev_filters=[("InSentence", "isDev", 1)],
                test_filters=[("InSentence", "isTest", 1)],
                opt_predicate='HasLabel',
                loss_augmented_inference=opts.delta,
                continue_from_checkpoint=opts.continue_from_checkpoint,
                weight_classes=True,
                patience=10,
                train_only=opts.train_only,
                inference_only=opts.inference_only)
    else:
        if opts.continue_from_checkpoint:
            learner.init_models()
        if not opts.inference_only:
            learner.train(
                    db,
                    train_filters=[("InSentence", "isTrain", 1)],
                    dev_filters=[("InSentence", "isDev", 1)],
                    test_filters=[("InSentence", "isTest", 1)])
        # locally we only need inference data for prediction
        if not opts.train_only:
            learner.extract_data(
                        db,
                        test_filters=[("InSentence", "isTest", 1)])
            #learner.drop_scores(db, fold='test', output='scores.csv')
            #exit()
            res, heads = learner.predict(
                        db,
                        fold='test')

    if not opts.train_only:
        y_gold = res.metrics['HasLabel']['gold_data']
        y_pred = res.metrics['HasLabel']['pred_data']

        # any sklearn metrics can be used over y_gold and y_pred
        acc_test = accuracy_score(y_gold, y_pred)
        logger.info("\n" + classification_report(y_gold, y_pred))
        logger.info("TEST acc: {}".format(acc_test))
        learner.reset_metrics()

if __name__ == '__main__':
    opts = parse_options()
    if opts.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(opts.logging_config)))
    main()
