in_sentence = {}
with open("in_sentence.txt") as fp:
    for line in fp:
        word, sent = line.strip().split()
        in_sentence[word] = sent

fw = open("prev_word_sent.txt", "w")
with open("prev_word.txt") as fp:
    for line in fp:
        word, prev_word = line.strip().split()
        sent = in_sentence[word]
        fw.write("{}\t{}\t{}\n".format(word, prev_word, sent))
