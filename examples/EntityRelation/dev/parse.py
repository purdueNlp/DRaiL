
fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = -1
sentTable = True
rel = False

maxLen = 0
maxWord = ""
maxPos = ""

for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
        preLen = len(words.split('/'))
        if (preLen > maxLen):
            maxLen = preLen
            maxWord = words
            maxPos = pos

        wordId += 1
        if (sentId >= 2942 or sentId < 2500):
            continue 
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words)
    else:
        if (sentId >= 2942 or sentId < 2500):
            continue 
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = rel

print(maxLen, maxWord, maxPos)

fout = open("phrases.txt", 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):
        fout.write(str(sentence[i][0]) + '\n')
fout.close()

fout = open("sentences.txt", 'w')
for sentId in corpus.keys():
    fout.write(str(sentId) + '\n')
fout.close()

fout = open("types.txt", 'w')
types = set()
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):    
        types.add(sentence[i][2])
for t in types:
    fout.write(t + '\n')
fout.close()

fout = open("in_sentence.txt", 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):
        fout.write(str(sentence[i][0]) + ' ' + str(sentId) + '\n')
fout.close()
        
fout = open("entity.txt", 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):    
        fout.write(str(sentence[i][0]) + ' ' + sentence[i][2] + '\n')
fout.close()

fout = open("live_in.txt", 'w')
for key in relation.keys():
    trel = relation[key]
    if (trel == "Live_In"):
        vals = key.split('-')
        fout.write(vals[0] + ' ' + vals[1] + '\n')
fout.close()

fout = open("work_for.txt", 'w')
for key in relation.keys():
    trel = relation[key]
    if (trel == "Work_For"):
        vals = key.split('-')
        fout.write(vals[0] + ' ' + vals[1] + '\n')
fout.close()
