import random

fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = -1
sentTable = True
rel = False

random.seed(12345)
shuffled = range(5926)
random.shuffle(shuffled)
portion = []
portion.append(shuffled[:1185])
portion.append(shuffled[1185:1185*2])
portion.append(shuffled[1185*2:1185*3])
portion.append(shuffled[1185*3:1185*4])
portion.append(shuffled[1185*4:])

fold = 0

for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
            
        wordId += 1
        if (sentId not in portion[fold]):
            continue 
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words)
    else:
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        if (sentId not in portion[fold]):
            continue 
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = (rel, sentId)


fout = open("phrases{}.txt".format(fold), 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):
        fout.write(str(sentence[i][0]) + '\n')
fout.close()

fout = open("sentences{}.txt".format(fold), 'w')
for sentId in corpus.keys():
    fout.write(str(sentId) + '\n')
fout.close()

fout = open("types{}.txt".format(fold), 'w')
types = set()
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):    
        types.add(sentence[i][2])
for t in types:
    fout.write(t + '\n')
fout.close()

fout = open("in_sentence{}.txt".format(fold), 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):
        fout.write(str(sentence[i][0]) + ' ' + str(sentId) + '\n')
fout.close()
        
fout = open("entity{}.txt".format(fold), 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):    
        fout.write(str(sentence[i][0]) + ' ' + sentence[i][2] + '\n')
fout.close()

fout = open("live_in{}.txt".format(fold), 'w')
for key in relation.keys():
    trel = relation[key][0]
    if (trel == "Live_In"):
        vals = key.split('-')
        fout.write(vals[0] + ' ' + vals[1] + '\n')
fout.close()

fout = open("work_for{}.txt".format(fold), 'w')
for key in relation.keys():
    trel = relation[key][0]
    if (trel == "Work_For"):
        vals = key.split('-')
        fout.write(vals[0] + ' ' + vals[1] + '\n')
fout.close()

fout = open("noun{}.txt".format(fold), 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    for i in range(len(sentence)):
        if (sentence[i][3].find('NN') > -1):
            fout.write(str(sentence[i][0]) + '\n')
fout.close()

