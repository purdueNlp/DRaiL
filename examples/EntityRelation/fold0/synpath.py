#from drail.features import utils
import numpy as np 

def onehot_dictionary(vocabulary):
    '''
    Helper function to build a one hot dictionary
    from a set of vocabulary of words

    Returns the number of elements extracted
    the size of the vector
    and the key-value mapping from word to vector
    '''
    dictionary = {}
    size = len(vocabulary)
    num = len(vocabulary)
    for i, v in enumerate(vocabulary):
        dictionary[v] = np.zeros(size)
        dictionary[v][i] = 1
    return (num, size, dictionary)

fin = open("depPathFeaturesRaw.txt" , 'r')
synPathDict = {}
edgeset = set()

for line in fin.readlines():
    values = line.split()
    if len(values) <= 3:
        continue
    pathlen = len(values)
    id1 = values[0]
    id2 = values[1]
    # print(values)
    for i in range(2, pathlen-2, 2):
        edge = ''.join([values[i], values[i+1], values[i+2]])
        edgeset.add(edge)

        # print(values[i], values[i+1], values[i+2], edge)

    # print('\n')

num_edge, size_edge, edge_onehot = onehot_dictionary(edgeset)
print(size_edge)

fin = open("depPathFeaturesRaw.txt" , 'r')
fout = open("depPathFeatures.txt", 'w')
for line in fin.readlines():
    values = line.split()
    if len(values) <= 3:
        continue
    pathlen = len(values)
    id1 = values[0]
    id2 = values[1]
    edgeOneHot = np.zeros(size_edge)
    for i in range(2, pathlen-2, 2):
        # print(values[i], values[i+1], values[i+2])

        edge = ''.join([values[i], values[i+1], values[i+2]])
        if (edge in edge_onehot):
            edgeOneHot = np.maximum(edgeOneHot, edge_onehot[edge])
    fout.write(id1 + ' ' + id2)
    for i in range(size_edge):
        fout.write(' ' + str(edgeOneHot[i]))
    fout.write('\n')

fout.close()