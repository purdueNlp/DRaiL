
fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = -1
sentTable = True
rel = False

for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
            
        wordId += 1
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words)
    else:
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = (rel, sentId)

personGaze = []
fin = open("person_female_lower.lst", 'r')
for line in fin.readlines():
    personGaze.append(' ' + line.strip().lower() + ' ')
fin = open("person_male_lower.lst", 'r')
for line in fin.readlines():
    personGaze.append(' ' + line.strip().lower() + ' ')

orgGaze = []
fin = open("org_base.lst", 'r')
for line in fin.readlines():
    orgGaze.append(' ' + line.strip().lower() + ' ')
fin = open("org_key.lst", 'r')
for line in fin.readlines():
    orgGaze.append(' ' + line.strip().lower() + ' ')
fin = open("org_pre.lst", 'r')
for line in fin.readlines():
    orgGaze.append(' ' + line.strip().lower() + ' ')

locGaze = []
fin = open("country.lst", 'r')
for line in fin.readlines():
    locGaze.append(' ' + line.strip().lower() + ' ')
fin = open("province.lst", 'r')
for line in fin.readlines():
    locGaze.append(' ' + line.strip().lower() + ' ')
fin = open("region.lst", 'r')
for line in fin.readlines():
    locGaze.append(' ' + line.strip().lower() + ' ')

locKey = []
fin = open("loc_key.lst", 'r')
for line in fin.readlines():
    locKey.append(' ' + line.strip().lower() + ' ')
fin = open("loc_generalkey.lst", 'r')
for line in fin.readlines():
    locKey.append(' ' + line.strip().lower() + ' ')

print(len(personGaze), len(orgGaze), len(locGaze), len(locKey))

fout = open("gazeFeatures.txt", 'w')
for sentId in corpus.keys():
    sentence = corpus[sentId]
    if (sentId % 100 == 0):
        print(sentId)
    for i in range(len(sentence)):
        words = sentence[i][4]
        wordId = sentence[i][0]
        phrase = ' ' + ' '.join(words.split('/')).lower() + ' '
        feature = [0, 0, 0, 0] # personGaze, orgGaze, locGazeEqual, locGazeContain
        for gaze in personGaze:
            if (phrase.find(gaze) > -1):
                feature[0] = 1
                break
        for gaze in orgGaze:
            if (phrase.find(gaze) > -1):
                feature[1] = 1
                break
        for gaze in locGaze:
            if (phrase == gaze):
                feature[2] = 1
                break
        for gaze in (locGaze + locKey):
            if (phrase.find(gaze) > -1):
                feature[3] = 1
                break
        fout.write(str(wordId) + ' ' + str(feature[0]) + ' ' + str(feature[1]) + ' ' + str(feature[2]) + ' ' + str(feature[3]) + '\n')
fout.close()
