fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = -1
sentTable = True
rel = False
relSet = set()

for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
            
        wordId += 1
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words, sentId)
    else:
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = (rel, sentId)
        relSet.add(sentId)

fin = open("gazeFeatures.txt", 'r')
pred = [0, 0, 0, 0]
corr = [0, 0, 0, 0]
total = [0, 0, 0, 0]
for line in fin.readlines():
    vals = line.strip().split(' ')
    wordId = int(vals[0])
    if (wordDict[wordId][4] not in relSet):
        continue

    if (vals[1] == '1'):
        pred[0] += 1
    if (wordDict[wordId][1] == "Peop"):
        total[0] += 1
        if (vals[1] == '1'):
            corr[0] += 1

    if (vals[2] == '1'):
        pred[1] += 1
        # if (wordDict[wordId][1] != "Org"):
        #    print(wordDict[wordId])
    if (wordDict[wordId][1] == "Org"):
        total[1] += 1
        if (vals[2] == '1'):
            corr[1] += 1

    if (vals[3] == '1'):
        pred[2] += 1
        #if (wordDict[wordId][1] != "Loc"):
        #    print(wordDict[wordId])
    if (wordDict[wordId][1] == "Loc"):
        total[2] += 1
        if (vals[3] == '1'):
            corr[2] += 1

    if (vals[4] == '1'):
        pred[3] += 1
    if (wordDict[wordId][1] == "Loc"):
        total[3] += 1
        if (vals[4] == '1'):
            corr[3] += 1

    # if (vals[5] == '1'):
    #     pred[4] += 1
    # if (wordDict[wordId][1] == "Loc"):
    #     total[4] += 1
    #     if (vals[2] == '1'):
    #         corr[4] += 1

print(corr)
print(pred)
print(total)

