import random
import json
from pycorenlp import StanfordCoreNLP

labelDict = {}
tokenCount = 0
synRes = []

def addLabel(label):
    if (label not in labelDict):
        labelDict[label] = 0
    labelDict[label] += 1
    
    return label + '-' + str(labelDict[label]-1)
    
def parse(synResRaw):
    global labelDict
    global tokenCount
    global synRes

    synResRaw = synResRaw.strip()[1:-1]
    pos = synResRaw.find('(')                    
    if (pos == -1):
        pair = synResRaw.split(' ')
        parentLabel = addLabel(pair[0].strip())
        tokenCount += 1
        sonLabel = str(tokenCount)
        #print("========== min case ", parentLabel, pair[1], sonLabel)
        synRes.append({'parent': parentLabel, 'son': sonLabel})
        return parentLabel
                         
    pair = [synResRaw[:pos], synResRaw[pos:]]
    parentLabel = addLabel(pair[0].strip())

    sons = pair[1]
    i = 0
    start = 0
    paraCount = 0
    while (i < len(sons)):
        if (sons[i] == '('):
            paraCount += 1
        elif (sons[i] == ')'):
            paraCount -= 1
            if (paraCount == 0):
                sonLabel = parse(sons[start:i+1])
                synRes.append({'parent': parentLabel, 'son': sonLabel})
                #print("--- Pair ", parentLabel, sonLabel)
                start = i+1
        i += 1
        
    return parentLabel

fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = 0
sentTable = True
rel = False
relSet = set()

entities = ['Peop', 'Loc', 'Org']
for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
            
        relSet.add(sentId)

        wordId += 1
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words)
    else:
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = (rel, sentId)

nlp = StanfordCoreNLP('http://localhost:9000')

#fout = open("depFeatures.txt", 'w')
#fout_synpath = open("synPathFeatures.txt", 'w')
fout_deppath = open("depPathFeaturesRaw.txt", 'w')

token_id = {}
dependencies = {}
paths = {}
pathLabels  = {}
pathTexts = {}

for sentId in range(6000):
    # 1574 1615 1929 3029 3525
    if (sentId not in relSet):
        continue
    print(sentId)
    sentence = corpus[sentId]
    preSentence = ""
    for i in range(len(sentence)):
        if (preSentence != ''):
            preSentence += ' '
        words = sentence[i][4]
        if (words == '/'):
            preSentence += '/'
        else:
            preSentence += ' '.join(words.split('/'))

    preSentence = preSentence.replace(" COMMA ", " , ")
    # print(preSentence)
    output = nlp.annotate(preSentence, properties={
        'annotators': 'tokenize,ssplit,pos,depparse,parse',
        'outputFormat': 'json'
    })

    # tokens
    tokens = output['sentences'][0]['tokens']
    # print(tokens)
    numTok = len(tokens)
    #if (numTok != tokenCount):
    #    raise
    preTok = 0
    token = tokens[preTok]['originalText']
    tokList = []

    # syn
    # synResRaw = output['sentences'][0]['parse'].replace('\n', '')
    # global labelDict 
    # global tokenCount
    # global synRes
    # labelDict = {}
    # tokenCount = 0
    # synRes = []
    # # print(synResRaw)
    # parse(synResRaw)
    # numSyn = len(synRes)
    
    # dep
    depRes = output['sentences'][0]['basicDependencies']

    dependencies[sentId]= depRes
    # print(depRes)
    numDep = len(depRes)
    
    # find token number for each phrase
    for i in range(len(sentence)):
        word = sentence[i][4]
        # print(i, word)
        if (word == '/'):
            wordList = ['/']
        else:
            word = word.replace('COMMA', ',')
            wordList = word.split('/')
        lastWord = wordList[-1]
        preWord = 0

        # TODO: use the token number for head word
        phraseTokenList = []
        headTokenList = []
        while (preTok < numTok):
            # print(token, wordList[preWord], token == wordList[preWord])
            if (token == wordList[preWord]):
                phraseTokenList.append(tokens[preTok]['index'])
                    
                if (preWord == len(wordList)-1):
                    # TODO
                    if (len(phraseTokenList) > 1):
                        for j in phraseTokenList:
                            flag = True
                            for k in range(numDep):
                                if (depRes[k]['dependent'] == j and depRes[k]['governor'] in phraseTokenList):
                                    flag = False
                                    break
                            if (flag):
                                headTokenList.append(j)
                    else:
                        headTokenList.append(phraseTokenList[0])
                    tokList.append(headTokenList[-1])
                    if (len(headTokenList) > 1):
                        if (tokens[headTokenList[-1]-1]['originalText'] == '.'):
                            tokList[-1] = headTokenList[-2]   
                    #     if (sentence[i][2] != 'O' and sentence[i][2] != 'Other'):
                    #         print(wordList)
                    #         print(headTokenList)
                    #         raise
                    preTok += 1
                    if (preTok < numTok):
                        token = tokens[preTok]['originalText']
                    break
                preTok += 1
                token = tokens[preTok]['originalText']
                preWord += 1
            elif (wordList[preWord].startswith(token)):
                phraseTokenList.append(tokens[preTok]['index'])
                wordList[preWord] = wordList[preWord][len(token):]
                preTok += 1
                if preTok < numTok:
                    token = tokens[preTok]['originalText']
            # one token contains multiple phrases
            elif (token.startswith(wordList[preWord])):
                token = token[len(wordList[preWord]):].strip()
                if (preWord == len(wordList)-1):
                    tokList.append(tokens[preTok]['index'])
                    break
                preWord += 1
            else:
                raise 

        token_id[sentence[i][0]] = tokList[-1]
            
    print(" token list ", tokList)    
    print len(tokList), len(sentence)


    # compute synPath using BFS
    # wordIdBase = sentence[0][0]
    # for i in range(len(sentence)):
    #     # TODO
    #     if (i >= len(tokList)):
    #         break
    #     queue = [str(tokList[i])]

    #     visited = set()
    #     synPath = {queue[0]: queue[0]}
    #     while (True):
    #         if (len(queue) == 0):
    #             break
    #         tword = queue[0]
    #         visited.add(queue[0])
    #         # print(queue[0])
    #         queue.remove(queue[0])
    #         for j in range(numSyn):
    #             nextword = None
    #             if (synRes[j]['son'] == tword):
    #                 nextword = synRes[j]['parent']
    #             elif (synRes[j]['parent'] == tword):
    #                 nextword = synRes[j]['son']
    #             if (nextword != None and nextword not in visited):
    #                 queue.append(nextword)
    #                 visited.add(nextword)
    #                 if (synRes[j]['son'] == tword):
    #                     arrow = " <- "
    #                 else:
    #                     arrow = " -> "
    #                 hypenPos = nextword.find('-')
    #                 if (hypenPos == -1):
    #                     wordStr = nextword
    #                 else:
    #                     wordStr = nextword[:hypenPos]
    #                 synPath[nextword] = synPath[tword] + arrow + wordStr      
    #                 # print(nextword, synPath[nextword])

    #     for j in range(len(sentence)):
    #         # TODO
    #         if (j >= len(tokList) or str(tokList[j]) not in synPath):
    #             continue
    #         # print(wordIdBase+i, wordIdBase+j, tokList[j], synPath[str(tokList[j])])
    #         fout.write(str(wordIdBase+i) + ' ' + str(wordIdBase+j) + ' ' + synPath[str(tokList[j])] + '\n')

    # compute depPath using BFS
    wordIdBase = sentence[0][0]
    for i in range(len(sentence)):
        # TODO
        if (i >= len(tokList)):
            break
        queue = [tokList[i]]

        visited = set()
        depPath = {queue[0]: tokens[queue[0]-1]['pos']}
        pathLab = {queue[0]: ''}
        pathTxt = {queue[0]: [tokens[queue[0]-1]['originalText']]}
        while (True):
            if (len(queue) == 0):
                break
            tword = queue[0]
            visited.add(queue[0])
            # print(queue[0])
            queue.remove(queue[0])
            for j in range(numDep):
                nextword = None
                if (depRes[j]['dependent'] == tword):
                    nextword, text = depRes[j]['governor'], depRes[j]['governorGloss']
                elif (depRes[j]['governor'] == tword):
                    nextword, text = depRes[j]['dependent'], depRes[j]['dependentGloss']
                if (nextword != None and nextword not in visited):
                    queue.append(nextword)
                    visited.add(nextword)
                    if (depRes[j]['dependent'] == tword):
                        arrow = " <- "
                    else:
                        arrow = " -> "
                    # print(tword, nextword, depPath[tword])
                    depPath[nextword] = depPath[tword] + arrow + tokens[nextword-1]['pos']      
                    pathLab[nextword] = pathLab[tword] + arrow + depRes[j]['dep']
                    pathTxt[nextword] = pathTxt[tword] + [tokens[nextword-1]['originalText']]
                    # print(nextword, depPath[nextword])

        for j in range(len(sentence)):
            # TODO
            if (j >= len(tokList) or tokList[j] not in depPath or i==j or sentence[j][2] not in entities or sentence[i][2] not in entities):
                continue
            # print(wordIdBase+i, wordIdBase+j, tokList[j], depPath[tokList[j]])
            fout_deppath.write(str(wordIdBase+i) + ' ' + str(wordIdBase+j) + ' ' + depPath[tokList[j]] + '\n')
            key = str(wordIdBase+i) + '-' + str(wordIdBase+j)
            paths[key] = depPath[tokList[j]]
            pathLabels[key] = pathLab[tokList[j]]
            pathTexts[key] = pathTxt[tokList[j]]

            print key, ' : ', paths[key]
            print key, ' : ', pathLabels[key]
            print key, ' : ', pathTexts[key],'\n' 

    # shareVerb
    # for i in range(len(sentence)):
    #     # print('\t', lastWord, queue)
    #     findv = False
    #     while (True):
    #         if (len(queue) == 0):
    #             break
    #         tword = queue[0]
    #         # print(queue[0])
    #         queue.remove(queue[0])
    #         for j in range(numDep):
    #             if (depRes[j]['dependent'] == tword):
    #                 queue.append(depRes[j]['governor'])
    #                 if (depRes[j]['dep'].find('subj') != -1 or depRes[j]['dep'].find('obj') != -1 or (queue[-1] > 0 and tokens[queue[-1]-1]['pos'][0] == 'V')):
    #                     findv = True
    #                     break

    #         if (findv):
    #             break

    #     if (findv):
    #         fout.write(str(sentId) + ' ' + str(i) + ' ' + str(queue[-1]) + ' ' + tokens[queue[-1]-1]['word'] + '\n')
    #         print(str(sentId) + ' ' + str(i) + ' ' + str(queue[-1]) + ' ' + tokens[queue[-1]-1]['word'])
    #     else:
    #         fout.write(str(sentId) + ' ' + str(i) + ' ' + str(0) + '\n')
    #     print('\n')
        
fout_deppath.close()

#with open('../../EntityRelation_Summer/phrase_token_id.json', 'w') as f:
#    json.dump(token_id, f)

#with open('../../EntityRelation_Summer/sent_dependencies.json', 'w') as f:
#    json.dump(dependencies, f)

with open('../../EntityRelation_Summer/pos_paths.json', 'w') as f:
    json.dump(paths, f)

with open('../../EntityRelation_Summer/path_labels.json', 'w') as f:
    json.dump(pathLabels, f)

with open('../../EntityRelation_Summer/path_text.json', 'w') as f:
    json.dump(pathTexts, f)
