import random

fin = open("conll04.corp", 'r')
corpus = {}
wordDict = {}
relation = {}
sentId = 0
wordId = -1
sentTable = True
rel = False
relSentSet = set()

fold = 3

for line in fin.readlines():
    if (line == '\n'):
        if (sentTable):
            sentTable = False
        else:
            sentTable = True
            #print(" ======= ", sentId)
            #print(corpus[sentId])
            #if (sentId in relation):
            #    print(relation[sentId])
        continue
    
    values = line.strip().split()

    if (sentTable):
        sentId = int(values[0])
        ner = values[1]
        wordIndex = int(values[2])
        pos = values[4]
        words = values[5]
            
        wordId += 1
        if (sentId not in corpus):
            corpus[sentId] = []
        corpus[sentId].append((wordId, wordIndex, ner, pos, words))    
        wordDict[wordId] = (wordIndex, ner, pos, words)
    else:
        arg1 = int(values[0])
        arg2 = int(values[1])
        rel = values[2]
        relation[str(wordId - wordIndex + arg1) + '-' + str(wordId - wordIndex + arg2)] = (rel, sentId)
        relSentSet.add(sentId)

relSentList = list(relSentSet)
foldSize = len(relSentList) / 5
random.seed(12345)
random.shuffle(relSentList)
portion = []
portion.append(relSentList[:foldSize])
portion.append(relSentList[foldSize:foldSize*2])
portion.append(relSentList[foldSize*2:foldSize*3])
portion.append(relSentList[foldSize*3:foldSize*4])
portion.append(relSentList[foldSize*4:])
relSent = portion[fold]
if (fold == 2):
    relSent = portion[2] + portion[3] + portion[4]
elif (fold == 3):
    relSent = portion[1] + portion[2] + portion[3] + portion[4]    

print(foldSize)
for i in range(5):
    print(len(portion[i]))
print(len(relSent))

result = 0
for sentId in relSentSet:
    sentence = corpus[sentId]
    count = 0
    for i in range(len(sentence)):
        if (sentence[i][2] != 'O'):
            count += 1
    result += count * count - count
print(result)       
 
# fout = open("phrases{}.txt".format(fold), 'w')
# for sentId in relSent:
#     sentence = corpus[sentId]
#     for i in range(len(sentence)):
#         fout.write(str(sentence[i][0]) + '\n')
# fout.close()

# fout = open("sentences{}.txt".format(fold), 'w')
# for sentId in relSent:
#     fout.write(str(sentId) + '\n')
# fout.close()

# fout = open("types{}.txt".format(fold), 'w')
# types = set()
# for sentId in relSent:
#     sentence = corpus[sentId]
#     for i in range(len(sentence)):    
#         types.add(sentence[i][2])
# for t in types:
#     fout.write(t + '\n')
# fout.close()

# fout = open("in_sentence{}.txt".format(fold), 'w')
# for sentId in relSent:
#     sentence = corpus[sentId]
#     for i in range(len(sentence)):
#         fout.write(str(sentence[i][0]) + ' ' + str(sentId) + '\n')
# fout.close()
        
# fout = open("entity{}.txt".format(fold), 'w')
# for sentId in relSent:
#     sentence = corpus[sentId]
#     for i in range(len(sentence)):    
#         fout.write(str(sentence[i][0]) + ' ' + sentence[i][2] + '\n')
# fout.close()

# fout = open("live_in{}.txt".format(fold), 'w')
# for key in relation.keys():
#     trel = relation[key][0]
#     sentId = relation[key][1]
#     if (sentId not in relSent):
#         continue
#     if (trel == "Live_In"):
#         vals = key.split('-')
#         fout.write(vals[0] + ' ' + vals[1] + '\n')
# fout.close()

# fout = open("work_for{}.txt".format(fold), 'w')
# for key in relation.keys():
#     trel = relation[key][0]
#     sentId = relation[key][1]
#     if (sentId not in relSent):
#         continue
#     if (trel == "Work_For"):
#         vals = key.split('-')
#         fout.write(vals[0] + ' ' + vals[1] + '\n')
# fout.close()

# fout = open("noun{}.txt".format(fold), 'w')
# for sentId in relSent:
#     sentence = corpus[sentId]
#     for i in range(len(sentence)):
#         if (sentence[i][3].find('NN') > -1):
#             fout.write(str(sentence[i][0]) + '\n')
# fout.close()

# fout = open("content{}.txt".format(fold), 'w')
# for sentId in relSent:
#     sentence = corpus[sentId]
#     preSentence = ""
#     for i in range(len(sentence)):
#         if (preSentence != ''):
#             preSentence += ' '
#         words = sentence[i][4]
#         if (words == '/'):
#             preSentence += '/'
#         else:
#             preSentence += ' '.join(words.split('/'))
#     fout.write(preSentence + '\n')
# fout.close()
