import numpy as np
import operator
import re
from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
from nltk.stem import WordNetLemmatizer


class EntityFE(FeatureExtractor):

    def __init__(self, instance_grds, w2v_bin_filename, num_wordEmbed, embedding_size, word_embedding, size_synPath, synPathFeatures, size_depPath, depPathFeatures):
        '''
        additional parameters can be passed to build the extractor
        '''
        super(EntityFE, self).__init__(instance_grds)
        self.w2v_bin_filename = w2v_bin_filename
        self.num_wordEmbed, self.embedding_size, self.word_embedding = num_wordEmbed, embedding_size, word_embedding 
        self.corpus = {}
        self.phraseDict = {}
        self.relation = {}
        self.sentences = {}
        self.depFeatures = {}
        self.gazeFeatures = {}
        self.size_synPath, self.synPathFeatures = size_synPath, synPathFeatures
        self.size_depPath, self.depPathFeatures = size_depPath, depPathFeatures
        self.jobtitle = set()
        self.lemmatizer = WordNetLemmatizer()
        #self.build()

    def build(self, data_file_path):
        print "building"
        # renamed to have the same name as parent class
        # it is a method override
      
        # corp_filename - "conll04.corp"
        corp_filename = data_file_path + "conll04.corp"
        fin = open(corp_filename, 'r')
        sentId = 0
        phraseId = -1
        sentTable = True
        rel = False
        preSentence = ''

        for line in fin.readlines():
            if (line == '\n'):
                if (sentTable):
                    sentTable = False
                    self.sentences[sentId] = preSentence.lower()
                    preSentence = ''
                else:
                    sentTable = True
                    #print(" ======= ", sentId)
                    #print(self.corpus[sentId])
                    #if (sentId in self.relation):
                    #    print(self.relation[sentId])
                continue
            
            values = line.strip().split()

            if (sentTable):
                sentId = int(values[0])
                ner = values[1]
                phraseIndex = int(values[2])
                pos = values[4]
                words = values[5]
                phraseId += 1
                if (sentId not in self.corpus):
                    self.corpus[sentId] = []
                self.corpus[sentId].append((phraseId, phraseIndex, ner, pos, words))    
                self.phraseDict[phraseId] = (phraseIndex, ner, pos, words)
                if (preSentence != ''):
                    preSentence += ' '
                if (words == '/'):
                    preSentence += '/'
                else:
                    preSentence += ' '.join(words.split('/'))
            else:
                arg1 = int(values[0])
                arg2 = int(values[1])
                rel = values[2]
                self.relation[sentId] = (arg1, arg2, rel)

        depfea_filename = data_file_path + "depFeatures.txt"
        fin = open(depfea_filename, 'r')
        for line in fin.readlines():
            values = line.strip().split()
            if (len(values) == 4):
                sentId = int(values[0])
                wordIndex = int(values[1])
                verbIndex = int(values[2])
                verbCont = values[3]
                if (sentId not in self.depFeatures):
                    self.depFeatures[sentId] = {}
                if (wordIndex not in self.depFeatures[sentId]):
                    self.depFeatures[sentId][wordIndex] = (verbIndex, verbCont)

        gazefea_filename = data_file_path + "gazeFeatures.txt"
        fin = open(gazefea_filename, 'r')
        for line in fin.readlines():
            vals = line.strip().split(' ')
            phraseId = int(vals[0])
            gazeVec = np.asarray([int(vals[1]), int(vals[2]), int(vals[3]), int(vals[4])])
            self.gazeFeatures[phraseId] = gazeVec

        jobtitle_filename = data_file_path + "jobtitle.lst"
        fin = open(jobtitle_filename, 'r')
        for line in fin.readlines():
            self.jobtitle.add(line.strip().lower())
       
        # Build set for nerTag, posTag and words
        nerTag = set([])
        posTag = set([])
        vocabulary = {} 
        tags = set([])
        prefix = {}
        suffix = {}
        for rule_grd in self.instance_grds:
            # word
            if rule_grd.has_body_predicate("InSentence"):
                ret = rule_grd.get_body_predicates("InSentence")
                # print ret
                for item in ret:
                    # print "in_sentence", item, type(item)
                    ner = self.phraseDict[item.arguments[0]][1]
                    pos = self.phraseDict[item.arguments[0]][2].split('/')
                    words = self.phraseDict[item.arguments[0]][3].split('/')
                    
                    nerTag.add(ner)
                    for p in pos:
                        posTag.add(p)
                    for word in words:
                        lemma = self.lemmatizer.lemmatize(word.lower())
                        if (lemma not in vocabulary):
                            vocabulary[lemma] = 0
                        vocabulary[lemma] += 1 
                        if (len(word) < 3):
                            tPrefix = word.lower()
                            tSuffix = word.lower()
                        else:
                            tPrefix = word[:3].lower()
                            tSuffix = word[-3:].lower()
                        if (tPrefix not in prefix):
                            prefix[tPrefix] = 0
                        prefix[tPrefix] += 1
                        if (tSuffix not in suffix):
                            suffix[tSuffix] = 0
                        suffix[tSuffix] += 1


            # head
            item = rule_grd.get_head_predicate()
            # TODO:
            if (item.name == "Entity"):
                tags.add(item.arguments[1])

        sorted_pre = sorted(prefix.items(), key=operator.itemgetter(1))
        sorted_suf = sorted(suffix.items(), key=operator.itemgetter(1))
        sorted_voca = sorted(vocabulary.items(), key=operator.itemgetter(1))

        print("prefix ", len(prefix.items()), sorted_pre[-1500], sorted_pre[-1000], sorted_pre[-500])
        print("suffix ", len(suffix.items()), sorted_suf[-1500], sorted_suf[-1000], sorted_suf[-500])
        print("vocabulary ", len(vocabulary.items()), sorted_voca[-4000], sorted_voca[-2000], sorted_voca[-1000])

        preSize = 2093
        if (len(sorted_pre) < 2093):
            preSize = len(sorted_pre)
        sufSize = 2063
        if (len(sorted_suf) < 2063):
            sufSize = len(sorted_suf)
        vocaSize = 6589 
        if (len(sorted_voca) < 6589):
            vocaSize = len(sorted_voca)
        prefix = [item[0] for item in sorted_pre[-preSize:]]
        suffix = [item[0] for item in sorted_suf[-sufSize:]]
        vocabulary = [item[0] for item in sorted_voca[-vocaSize:]]

        self.tagdic = {}
        for i, v in enumerate(tags):
            # print i, v
            self.tagdic[v] = i
        print(self.tagdic)
        
        # self.num_wordEmbed, self.embedding_size, self.word_embedding = utils.embeddings_dictionary(self.w2v_bin_filename)
        self.num_words, self.size_word, self.word_onehot = utils.onehot_dictionary(vocabulary)
        print "Using onehot: number of words: {}; ".format(self.num_words), "word dimension: {}".format(self.size_word)
        self.num_pos, self.size_pos, self.posTag_onehot = utils.onehot_dictionary(posTag)
        self.num_prefix, self.size_prefix, self.prefix_onehot = utils.onehot_dictionary(prefix)
        self.num_suffix, self.size_suffix, self.suffix_onehot = utils.onehot_dictionary(suffix)
        self.num_tags, self.size_tags, self.tag_onehot = utils.onehot_dictionary(tags)

    def extract_phrase(self, phraseId, sentId, targetFlag=False):
        phraseIndex = self.phraseDict[phraseId][0]
        sentLen = len(self.corpus[sentId])

        wordOneHot = np.zeros(self.size_word)
        pos = self.phraseDict[phraseId][2].split('/')
        posVec = utils.vec_average([self.posTag_onehot[p] if p in self.posTag_onehot else np.zeros(self.size_pos) for p in pos])
        if (self.phraseDict[phraseId][3] == '/'):
            words = ['/']
        else:
            words = self.phraseDict[phraseId][3].split('/')
        wordVec = utils.vec_average([self.word_embedding[word] if word in self.word_embedding else np.random.uniform(-0.0025, 0.0025, self.embedding_size) for word in words])
        wordLen = len(words)
        for word in words:
            lemma = self.lemmatizer.lemmatize(word.lower())
            if (lemma in self.word_onehot):
                wordOneHot = np.maximum(wordOneHot, self.word_onehot[lemma])
        
        if(not targetFlag):
            return (np.hstack([posVec, wordVec]), wordOneHot)

        prefixVec = np.zeros(self.size_prefix)
        suffixVec = np.zeros(self.size_suffix)
        icap = 0
        acap = 0
        incap = 0

        for word in words:
            if word.isupper():
                acap = 1
            elif word[0].isupper():
                icap = 0
                if (word[1:].lower != word[1:]):
                    incap = 1

            if (len(word) < 3):
                tPrefix = word.lower()
                tSuffix = word.lower()
            else:
                tPrefix = word[:3].lower()
                tSuffix = word[-3:].lower()     
            if (tPrefix in self.prefix_onehot):
                prefixVec = np.maximum(prefixVec, self.prefix_onehot[tPrefix])
            if (tSuffix in self.suffix_onehot):
                suffixVec = np.maximum(suffixVec, self.suffix_onehot[tSuffix])
        
        gazeVec = self.gazeFeatures[phraseId]
        return (np.hstack([posVec, wordVec, prefixVec, suffixVec, icap, acap, incap, gazeVec]), wordOneHot)
            
    def extract_livein(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return None

        inSentences = rule_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0].arguments[0]
        arg2 = inSentences[1].arguments[0]
        sentId = inSentences[0].arguments[1]
        preSentence = self.sentences[sentId]
        phrase1 = ' '.join(self.phraseDict[arg1][3].split('/')).lower()
        phrase2 = ' '.join(self.phraseDict[arg2][3].split('/')).lower()

        vector = np.asarray([])
        indicators = ['live', 'in', 'at', 'native', 'president', 'work', 'for', 'employ', 'officer', 'comma']
        for indicator in indicators:
            ind = (preSentence.find(indicator) >= 0) * 1
            indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6 = 0, 0, 0, 0, 0, 0
            if (phrase1 != '?' and phrase2 != '?'):
                indOrder1 = (re.search('.*'.join([phrase1, phrase2, indicator]), preSentence) != None) * ind
                indOrder2 = (re.search('.*'.join([phrase2, phrase1, indicator]), preSentence) != None) * ind
                indOrder3 = (re.search('.*'.join([phrase1, indicator, phrase2]), preSentence) != None) * ind
                indOrder4 = (re.search('.*'.join([phrase2, indicator, phrase1]), preSentence) != None) * ind
                indOrder5 = (re.search('.*'.join([indicator, phrase1, phrase2]), preSentence) != None) * ind
                indOrder6 = (re.search('.*'.join([indicator, phrase2, phrase1]), preSentence) != None) * ind
            vector = np.hstack([vector, ind, indOrder1, indOrder2, indOrder3, indOrder4, indOrder5, indOrder6])

        return vector

    def extract_rel(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return None

        inSentences = rule_grd.get_body_predicates("InSentence")
        arg1 = inSentences[0].arguments[0]
        arg2 = inSentences[1].arguments[0]
        sentId = inSentences[0].arguments[1]
        preSentence = self.sentences[sentId]
        phrase1 = ' '.join(self.phraseDict[arg1][3].split('/')).lower()
        phrase2 = ' '.join(self.phraseDict[arg2][3].split('/')).lower()
        
        wordInBetween = abs(arg1 - arg2)
        sameWordPos = (arg1 == arg2) * 1
        sameWordContent = (self.phraseDict[arg1][3] == self.phraseDict[arg2][3]) * 1
        
        # whether arg is begin/end
        arg1Index = self.phraseDict[arg1][0]
        arg2Index = self.phraseDict[arg2][0]
        sentLen = len(self.corpus[sentId])
        arg1Begin = (arg1Index == 0) * 1
        arg2Begin = (arg2Index == 0) * 1
        arg1End = (arg1Index == sentLen-1) * 1
        arg2End = (arg2Index == sentLen-1) * 1
        former = (arg1 < arg2) * 1
        latter = (arg1 > arg2) * 1
        
        commaSep1 = (preSentence.find(phrase1 + " comma " + phrase2) > -1 or preSentence.find(phrase1 + " , " + phrase2) > -1) * 1
        commaSep2 = (preSentence.find(phrase2 + " comma " + phrase1) > -1 or preSentence.find(phrase2 + " , " + phrase1) > -1) * 1
        
        nounInBetween = 0
        contInBetween = ""
        startId = min(arg1, arg2)
        endId = max(arg1, arg2)
        for mid in range(startId + 1, endId):
            if (self.phraseDict[mid][2].find('NN') > -1):
                nounInBetween += 1
            words = self.phraseDict[mid][3]
            if (contInBetween != ''):
                contInBetween += ' '
            if (words == '/'):
                contInBetween += '/'
            else:
                contInBetween += ' '.join(words.split('/')).lower()
        titleInBetween = 0
        for jobt in self.jobtitle:
            if (contInBetween.find(jobt) > -1):
                titleInBetween = 1
                break

        # depFeatures
        shareVInd = 0
        shareVCont = np.random.uniform(-0.0025, 0.0025, self.embedding_size)
        if (sentId in self.depFeatures and arg1Index in self.depFeatures[sentId] and arg2Index in self.depFeatures[sentId]):
            arg1VInd, arg1VCont = self.depFeatures[sentId][arg1Index]
            arg2VInd, arg2VCont = self.depFeatures[sentId][arg2Index]
            if (arg1VInd == arg2VInd):
                shareVInd = 1
                if arg1VCont in self.word_embedding:
                    shareVCont = self.word_embedding[arg1VCont] 

        # synPathFeatures
        idPair = str(arg1) + '-' + str(arg2)
        if (idPair in self.synPathFeatures):
            synEdgeVec = self.synPathFeatures[idPair]
        else:
            synEdgeVec = np.zeros(self.size_synPath)


        # depPathFeatures
        if (idPair in self.depPathFeatures):
            depEdgeVec = self.depPathFeatures[idPair]
        else:
            depEdgeVec = np.zeros(self.size_depPath)

        vector = np.hstack([wordInBetween, sameWordPos, sameWordContent, arg1Index, arg2Index, arg1Begin, arg2Begin, arg1End, arg2End, former, latter, commaSep1, commaSep2, nounInBetween, titleInBetween, shareVInd, shareVCont, synEdgeVec, depEdgeVec])

        return vector    

    def extract_feature(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return None

        inSentences = rule_grd.get_body_predicates("InSentence")
        wholeVec = []

        for item in inSentences:
            phraseId = item.arguments[0]
            sentId = item.arguments[1]
            phraseIndex = self.phraseDict[phraseId][0]
            sentLen = len(self.corpus[sentId])
            toConcat, toMaxi = self.extract_phrase(phraseId, sentId, targetFlag=True)
            phraseVec = [toConcat]
            wordOneHot = toMaxi
            featureSize = len(toConcat) # self.size_pos + self.embedding_size 

            if (phraseIndex-1 >= 0):
                toConcat, toMaxi = self.extract_phrase(phraseId-1, sentId, targetFlag=True)
                phraseVec.append(toConcat)
                wordOneHot = np.maximum(wordOneHot, toMaxi)
            else:
                phraseVec.append(np.zeros(featureSize))
            if (phraseIndex-2 >= 0):
                toConcat, toMaxi = self.extract_phrase(phraseId-2, sentId, targetFlag=True)
                phraseVec.append(toConcat)
                wordOneHot = np.maximum(wordOneHot, toMaxi)
            else:
                phraseVec.append(np.zeros(featureSize))
            if (phraseIndex+1 <= sentLen-1):
                toConcat, toMaxi = self.extract_phrase(phraseId+1, sentId, targetFlag=True)
                phraseVec.append(toConcat)
                wordOneHot = np.maximum(wordOneHot, toMaxi)
            else:
                phraseVec.append(np.zeros(featureSize))
            if (phraseIndex+2 <= sentLen-1):
                toConcat, toMaxi = self.extract_phrase(phraseId+2, sentId, targetFlag=True)
                phraseVec.append(toConcat)
                wordOneHot = np.maximum(wordOneHot, toMaxi)
            else:
                phraseVec.append(np.zeros(featureSize))
            
            wholeVec.append(np.hstack(phraseVec))
            wholeVec.append(wordOneHot)

        return np.hstack(wholeVec)

    def extract_multiclass_head(self, rule_grd):

        ret = rule_grd.get_head_predicate()
        # print self.tagdic[ret.arguments[1]]

        return self.tagdic[ret.arguments[1]]

