train = {}; test = {}
seeds = {}


with open("citeseer_splits.txt") as f:
    training = False; testing = False; seeding = False
    for line in f:
        if line.strip().startswith("Train"):
            training = True
            testing = False
            if line.strip().endswith("seeds"):
                seeding = True
            else:
                seeding = False
            #print line.strip(), "seeding={0}".format(seeding)
            continue
        elif line.strip().startswith("Test"):
            testing = True
            training = False
            if line.strip().endswith("seeds"):
                seeding = True
            else:
                seeding = False
            #print line.strip(), "seeding={0}".format(seeding)
            continue

        (fold, paper) = line.strip().split('\t')
        if training:
            if fold in train:
                train[fold].append(paper)
            else:
                train[fold] = [paper]
        if testing:
            if fold in test:
                test[fold].append(paper)
            else:
                test[fold] = [paper]
        if seeding:
            if fold in seeds:
                seeds[fold].append(paper)
            else:
                seeds[fold] = [paper]


for x in range(0, 20):
    fold = str(x)
    print "FOLD", fold
    print "train", len(set(train[fold])), len(train[fold])
    print "test", len(set(test[fold])), len(test[fold])
    print "seeds", len(set(seeds[fold])), len(seeds[fold])
    print "train & seeds", len(set(train[fold]) & set(seeds[fold]))
    print "test & seeds", len(set(test[fold]) & set(seeds[fold]))
    print "train & test", len(set(test[fold]) & set(train[fold]))
    print

for fold in train:
    with open("train{0}.txt".format(fold), 'w') as f:
        for paper in set(train[fold]):
            f.write(paper + "\n")
for fold in test:
    with open("test{0}.txt".format(fold), 'w') as f:
        for paper in set(test[fold]):
            f.write(paper + "\n")
for fold in seeds:
    with open("seeds{0}.txt".format(fold), 'w') as f:
        for paper in set(seeds[fold]):
            f.write(paper + "\n")
