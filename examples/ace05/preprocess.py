import xml.etree.ElementTree as ET
import os
import re
import sys
import json

rootdir = '/scratch3/pachecog/DRaiL_Summer/LDC2006T06/data/English'

docs = []
entities = {}
ent_text = {}
relations = {}
inDocument = []

for dirpath, dirnames, filenames in os.walk(rootdir):
    for filename in filenames:
        if dirpath[-3:]!='adj' or re.match(r'.*\.apf\.xml$', filename) == None:
            continue

        print filename
         
        f = os.path.join(dirpath, filename)

        root = ET.parse(f).getroot()

        document = list(root)[0]

        doc_id = document.get('DOCID')

        #print doc_id
        docs.append(doc_id)

        for entity in root.iter('entity'):

            ent_type = entity.attrib['TYPE']

            if ent_type not in entities:
                entities[ent_type] = []

            for entity_mention in entity.findall('entity_mention'):
                
                id = entity_mention.attrib['ID']
                entities[ent_type].append(id)
                inDocument.append((id, doc_id))

                ent_text[id] = entity_mention[1][0].text

                print ent_type, id, ent_text[id]

        for relation in root.iter('relation'):

            rel_type = relation.attrib['TYPE']

            if rel_type not in relations:
                relations[rel_type] = []

            for rel_mention in relation.findall('relation_mention'):

                refids, roles = [], []
                for arg in rel_mention.findall('relation_mention_argument'):
                    refid, role = arg.get('REFID'), arg.get('ROLE')
                    refids.append(refid)
                    roles.append(role)

                refids = tuple(refid for (role, refid) in sorted(zip(roles,refids)))

                relations[rel_type].append(refids)

                print rel_type, relations[rel_type]


for ent_type in entities:
    with open(ent_type+'.txt', 'w') as f:
        for id in entities[ent_type]:
            f.write(id+'\n')

for rel_type in relations:
    with open(rel_type+'.txt', 'w') as f:
        for args in relations[rel_type]:
            for arg in args:
                f.write(arg+' ')
            f.write('\n')

with open('documents.txt', 'w') as f:
    for id in docs:
        f.write(id+'\n')

with open('inDocument.txt', 'w') as f:
    for eid, did in inDocument:
        f.write(eid+' '+did+'\n')

with open('entity_text.json', 'w') as f:
    json.dump(ent_text, f)
