import logging

import torch
import numpy as np
import torch.nn.functional as F

from drail.neuro.nn_model import NeuralNetworks

class ShallowClassifier(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(ShallowClassifier, self).__init__(config, nn_id)
        self.use_gpu = use_gpu
        self.output_dim = output_dim
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info("output_dim = {}".format(output_dim))

    def build_architecture(self, rule_template, fe, shared_params={}):
        self.minibatch_size = self.config['batch_size']
        self.n_input = rule_template.feat_vector_sz
        self.classifier = torch.nn.Linear(self.n_input, self.output_dim)

        if self.use_gpu:
            self.classifier = self.classifier.cuda()

    def forward(self, x):
        feats = self._get_float_tensor(x['vector'])
        feats = self._get_variable(feats)

        logits = self.classifier(feats)

        if 'output' not in self.config or self.config['output'] == "softmax":
            probas = F.softmax(logits, dim=1)
        elif self.config['output'] == 'sigmoid':
            probas = F.sigmoid(logits)
        return logits, probas
