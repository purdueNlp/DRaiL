import itertools

RELATIONS  = 'data/reln.txt'
PARAGRAPHS = 'data/in_par.txt'

essays = dict()
links  = dict()

with open(PARAGRAPHS) as f:
    for line in f.readlines():
        essay, comp, par = line.split()

        if essay not in essays:
            essays[essay] = dict()
        
        if par not in essays[essay]:
            essays[essay][par] = []

        essays[essay][par].append(comp)


with open(RELATIONS) as f:
    for line in f.readlines():
        essay, l_from, l_to = line.split()

        if essay not in links:
            links[essay] = []

        links[essay].append((l_from, l_to))

# looks like:
# essays: 'essay109': {'1': ['T2'], '3': ['T10', 'T11', 'T9'], '2': ['T3', 'T8', 'T5', 'T6', 'T4', 'T7'], '4': ['T1']}
# links:  'essay109': [('T11', 'T9'), ('T8', 'T6'), ('T4', 'T3'), ('T5', 'T4'), ('T10', 'T9'), ('T7', 'T6')]

co_parents = []
grand_parents = []
less_than = []
different = []

for essay in links:
    for l_from, l_to in links[essay]:
        children = []

        # grandparent
        for l_from_inner, l_to_inner in links[essay]:
            if l_to == l_to_inner:
                children.append(l_from_inner)

            if l_to == l_from_inner:
                grand_parents.append([essay, l_from, l_to, l_to_inner])

        # co-parent
        if len(children) >= 2:
            for child1, child2 in itertools.combinations(children, 2):
                # l_to is coparent of child1 and child2
                c1_value = int(child1[1:])
                c2_value = int(child2[1:])

                if c1_value < c2_value and [essay, l_to, child1, child2] not in co_parents:
                    co_parents.append([essay, l_to, child1, child2])
                elif c2_value < c1_value and [essay, l_to, child2, child1] not in co_parents:
                    co_parents.append([essay, l_to, child2, child1])

maxT = 1

# find max Ti
for essay in essays:
    for par in essays[essay]:
        for comp in essays[essay][par]:
            if int(comp[1:]) > maxT:
                maxT = int(comp[1:])

Ts = ['T' + str(x) for x in list(range(1, maxT + 1))]

# less than
for comp1, comp2 in itertools.combinations(Ts, 2):
    if int(comp1[1:]) < int(comp2[1:]):
        less_than.append([comp1, comp2])
    else:
        less_than.append([comp2, comp1])

# different
for combination in itertools.combinations(Ts, 3):
    for permutation in itertools.permutations(combination):
        different.append(list(permutation))


print('max Ti: {}'.format(maxT))
print('co_parents: {}'.format(len(co_parents)))
print('grand_parents: {}'.format(len(grand_parents)))
print('less_than: {}'.format(len(less_than)))
print('different: {}'.format(len(different)))


with open('data/grandparent.txt', 'w') as f:
    for essay, grand_child, parent, grand_parent in grand_parents:
        f.write(' '.join([essay, grand_parent, parent, grand_child]) + '\n')

with open('data/co_parent.txt', 'w') as f:
    for item in co_parents:
        f.write(' '.join(item) + '\n')

with open('data/less_than.txt', 'w') as f:
    for item in less_than:
        f.write(' '.join(item) + '\n')

with open('data/different.txt', 'w') as f:
    for item in different:
        f.write(' '.join(item) + '\n')
