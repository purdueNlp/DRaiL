import json
import os
import collections
from nltk.parse.corenlp import CoreNLPServer
from nltk.parse import CoreNLPParser
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from allennlp.modules.elmo import batch_to_ids

STANFORD = os.path.join("/scratch/pachecog", "stanford-corenlp-full-2017-06-09")

with open("data/data.json") as fp:
    data = json.load(fp)

def get_wordnet_pos(treebank_tag):

    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN

def productions_between_leaves(tree, start=0):
    productions = set()
    start_pos = tree.leaf_treeposition(start)

    end_pos = None
    foul_end_pos = None

    hit_start = False

    for position in tree.treepositions():

        # if the start leaf is THE FIRST LEAF in our span, turn on hit_start
        if (position == start_pos[:len(position)] and
                all(k == 0 for k in start_pos[len(position):])):
            hit_start = True

        # if the end leaf is in our span at all, ignore the tree
        if (foul_end_pos is not None and
                foul_end_pos[:len(position)] == position):
            continue

        if hit_start:
            subtree = tree[position]
            if hasattr(subtree, 'productions'):
                productions.update(subtree.productions())

        if end_pos is not None and position == end_pos:
            break

    return productions


def find_sub_list(sl,l):
    sll=len(sl)
    for ind in (i for i,e in enumerate(l) if e==sl[0]):
        if l[ind:ind+sll]==sl:
            return ind,ind+sll-1

productions = {}; all_pos = set(); lemmas = {}

extract_prod_rules = False
extract_pos = False
extract_unigrams = False
extract_elmo = True

lemmatizer = WordNetLemmatizer()

for essay_id in data:

    if extract_prod_rules:
        # Create the server
        server = CoreNLPServer(
        os.path.join(STANFORD, "stanford-corenlp-3.8.0.jar"),
        os.path.join(STANFORD, "stanford-corenlp-3.8.0-models.jar"),)

        # Start the server in the background
        server.start()
        parser = CoreNLPParser()
        print(essay_id)

        num_pars = data[essay_id]['num_pars']
        for i in range(1, int(num_pars)):
            if str(i) not in data[essay_id]:
                continue
            for comp in data[essay_id][str(i)]:
                text = data[essay_id][str(i)][comp]['text']
                try:
                    parse_tree = next(parser.raw_parse(text))
                    prod_rules = productions_between_leaves(parse_tree)
                    prod_rules = map(str, list(prod_rules))
                    for rule in prod_rules:
                        if rule not in productions:
                            productions[rule] = 0
                        productions[rule] += 1
                    data[essay_id][str(i)][comp]['prod_rules'] = prod_rules
                except Exception as e:
                    print(e.message, e.args)
                    print("Couldn't parse", str(i), essay_id)
                    break

        print("Stopping server")
        server.stop()
        print("Server stopped")
    if extract_pos:
        num_pars = data[essay_id]['num_pars']
        for i in range(1, int(num_pars)):
            if str(i) not in data[essay_id]:
                continue
            for comp in data[essay_id][str(i)]:
                text = data[essay_id][str(i)][comp]['text']
                tokens = nltk.word_tokenize(text)
                pos_tags = nltk.pos_tag(tokens)
                pos_tags = [t for (w, t) in pos_tags]
                all_pos.update(pos_tags)
                data[essay_id][str(i)][comp]['pos'] = pos_tags
    if extract_unigrams:
        num_pars = data[essay_id]['num_pars']
        for i in range(1, int(num_pars)):
            if str(i) not in data[essay_id]:
                continue
            for comp in data[essay_id][str(i)]:
                text = data[essay_id][str(i)][comp]['text']
                tokens = nltk.word_tokenize(text)
                pos_tags = nltk.pos_tag(tokens)
                curr_lemmas = [lemmatizer.lemmatize(w.lower(), pos=get_wordnet_pos(t)) for (w, t) in pos_tags]
                data[essay_id][str(i)][comp]['lemmas'] = curr_lemmas

                for l in curr_lemmas:
                    if l not in lemmas:
                        lemmas[l] = 0
                    lemmas[l] += 1

    if extract_elmo:
        num_pars = data[essay_id]['num_pars']
        offsets = 1

        full_text = data[essay_id]['full_text']
        if "doing.In" in full_text:
            full_text = full_text.replace("doing.In", "doing. In")
            data[essay_id]['full_text'] = full_text

        for i in range(1, int(num_pars)):
            if str(i) not in data[essay_id]:
                continue
            else:
                offset_l = data[essay_id]['par_offsets'][offsets]
                if offsets + 1 >= len(data[essay_id]['par_offsets']):
                    offset_r = len(data[essay_id]['full_text'])
                else:
                    offset_r = data[essay_id]['par_offsets'][offsets + 1]
                par_text =  data[essay_id]['full_text'][offset_l:]
                offsets += 1
                par_tokens = nltk.word_tokenize(par_text)
                character_ids = batch_to_ids([par_tokens])
                character_ids = character_ids[0].tolist()
                data[essay_id][str(i)]['elmo_par'] = character_ids


            for comp in data[essay_id][str(i)]:
                if comp == 'elmo_par':
                    continue

                text = data[essay_id][str(i)][comp]['text']

                # Correcting some processing erros
                if text.endswith('responsibl'):
                    text += "e"
                    data[essay_id][str(i)][comp]['text'] = text
                    data[essay_id][str(i)][comp]['offset_r'] += 1
                elif text.endswith('environmen') or text.endswith('governmen'):
                    text += "t"
                    data[essay_id][str(i)][comp]['text'] = text
                    data[essay_id][str(i)][comp]['offset_r'] += 1
                elif text.endswith('educatio') or text.endswith('communicatio'):
                    text += "n"
                    data[essay_id][str(i)][comp]['text'] = text
                    data[essay_id][str(i)][comp]['offset_r'] += 1

                preceding_text = data[essay_id][str(i)][comp]['preceding_text']

                tokens = nltk.word_tokenize(text)
                offset_l_comp, offset_r_comp = find_sub_list(tokens, par_tokens)
                data[essay_id][str(i)][comp]['offset_l_par'] = offset_l_comp
                data[essay_id][str(i)][comp]['offset_r_par'] = offset_r_comp

                preceding_tokens = nltk.word_tokenize(preceding_text)

                character_ids = batch_to_ids([tokens])
                character_ids = character_ids[0].tolist()
                data[essay_id][str(i)][comp]['elmo'] = character_ids

                character_ids = batch_to_ids([preceding_tokens])
                character_ids = character_ids[0].tolist()
                data[essay_id][str(i)][comp]['preceding_elmo'] = character_ids


with open("data/data_updated.json", "w") as fp:
    json.dump(data, fp)

if extract_prod_rules:
    print("Extracted {} prod_rules".format(len(productions)))
    with open("data/prod_rules.json", "w") as fp:
        json.dump(productions, fp)

if extract_unigrams:
    print("Extracted {} unigram lemmas".format(len(lemmas)))
    with open("data/lemmas.json", "w") as fp:
        json.dump(lemmas, fp)

if extract_pos:
    with open("data/pos_tags.txt", "w") as fp:
        for pos in all_pos:
            fp.write(pos)
            fp.write('\n')
