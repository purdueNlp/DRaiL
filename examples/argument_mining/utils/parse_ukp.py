# encoding: utf-8

import glob
from collections import Counter
import json
import re
import nltk
import sys
import codecs
import spacy
import textacy

reload(sys)
sys.setdefaultencoding('utf8')


has_component = set([]) # has_component(essay, component)
is_type = set([]) # is_type(essay, component, type)
in_par = set([])

supports = set([]) # support(essay, component, component)
attacks = set([])

stances = set([])

is_for = set([]) # is_for(essay, component)
is_against = set([])

all_types = []

data = {}

word2idx = {}; curr_idx = 0
extra_abbreviations = set(['etc', 'e.g'])
sentence_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
sentence_tokenizer._params.abbrev_types.update(extra_abbreviations)

nlp = spacy.load('en')
pattern = r'<VERB>?<ADV>*<VERB>+'

for fname in glob.glob("/scratch/pachecog/ArgumentAnnotatedEssays-2.0/brat-project-final/*.ann"):
    fname_text = fname[:-3] + "txt"
    text = open(fname_text).readlines()
    text_full = open(fname_text).read().decode('utf-8', 'strict')

    #print
    par_char_offset = 0; paragraph_offsets = []; paragraph_tokens = []
    sentence_offsets = {}; sentence_tokens = {}; sentence_text = {}

    curr_par = 0
    prev_offset = None
    for t in text:
        t = t.decode('utf-8', 'strict')
        if re.match('.*\w+.*', t):
            sentence_offsets[curr_par] = []
            sentence_tokens[curr_par] = []
            sentence_text[curr_par] = []
            paragraph_offsets.append(par_char_offset)
            paragraph_tokens.append(len(nltk.word_tokenize(t)))

            debug = False
            for sent in sentence_tokenizer.tokenize(t):
                regex = re.escape(sent)
                sent_char_offsets = [m.start() for m in re.finditer(regex, text_full)]
                #print sent_char_offsets, len(sent_char_offsets), sent
                if len(sent_char_offsets) == 1:
                    sent_char_offset = sent_char_offsets[0]
                else:
                    #print sent_char_offsets, prev_offset
                    sent_char_offset = sent_char_offsets[0]
                    for enum, off in enumerate(sent_char_offsets):
                        #print enum, off, prev_offset
                        if off > prev_offset and off >= par_char_offset:
                            sent_char_offset = sent_char_offsets[enum]
                            break
                    #print sent_char_offset
                    # check that we obtain the right offsets
                    #print sent
                    #print text_full[sent_char_offset : sent_char_offset + len(sent)]
                    #debug = True
                assert(sent == text_full[sent_char_offset : sent_char_offset + len(sent)])

                sentence_offsets[curr_par].append(sent_char_offset)
                sentence_tokens[curr_par].append(len(nltk.word_tokenize(sent)))
                sentence_text[curr_par].append(sent)
                prev_offset = sent_char_offset

            #if debug:
            #    print "Debugging"
            #    print sentence_offsets[curr_par]
            #    print "===="

            curr_par += 1
        par_char_offset += len(t)
    #continue

    with open(fname) as fp:
        essay_id = fname[-12:-4]

        data[essay_id] = {}
        data[essay_id]['full_text'] = text_full
        data[essay_id]['num_pars'] = len(paragraph_offsets)
        data[essay_id]['par_offsets'] = paragraph_offsets

        text_intro = text_full[paragraph_offsets[1]:paragraph_offsets[2]]
        text_concl = text_full[paragraph_offsets[-1]:]

        doc_intro = nlp(text_intro)
        doc_concl = nlp(text_concl)
        np_intro = [np.text.lower() for np in doc_intro.noun_chunks]
        np_concl = [np.text.lower() for np in doc_concl.noun_chunks]

        doc_intro = textacy.Doc(text_intro, lang=u'en_core_web_sm')
        doc_concl = textacy.Doc(text_concl, lang=u'en_core_web_sm')
        vp_intro = [vp.text.lower() for vp in textacy.extract.pos_regex_matches(doc_intro, pattern)]
        vp_concl = [vp.text.lower() for vp in textacy.extract.pos_regex_matches(doc_concl, pattern)]

        data[essay_id]['np_intro'] = np_intro
        data[essay_id]['vp_intro'] = vp_intro
        data[essay_id]['np_concl'] = np_concl
        data[essay_id]['vp_concl'] = vp_concl

        for line in fp:
            component_id, ann = line.split('\t', 1)

            if component_id.startswith('T'):
                has_component.add((essay_id, component_id))\

                #print ann
                typ, offset_l, offset_r, text = re.split("\s+", ann.strip(), 3)
                text_tok = nltk.word_tokenize(text.lower())
                text_doc = nlp(unicode(text.lower()))
                text_np = [np.text.lower() for np in text_doc.noun_chunks]
                text_vp = [vp.text.lower() for vp in textacy.extract.pos_regex_matches(text_doc, pattern)]

                tokens = []

                for word in text_tok:
                    if word.decode('utf-8') not in word2idx:
                        word2idx[word.decode('utf-8')] = curr_idx
                        curr_idx += 1
                    tokens.append(word2idx[word.decode('utf-8')])

                #print text

                curr_par = -1
                for n_par, offs in enumerate(paragraph_offsets):
                    if int(offset_r) < offs:
                        curr_par = n_par - 1
                        break

                if curr_par == -1:
                    curr_par = len(paragraph_offsets) - 1

                #print offset_l, offset_r
                #print curr_par

                curr_sent = -1

                n_sen = 0; offs = sentence_offsets[curr_par][n_sen]
                while int(offset_l) >= offs:
                    curr_sent = n_sen
                    n_sen += 1
                    if n_sen == len(sentence_offsets[curr_par]):
                        break
                    offs = sentence_offsets[curr_par][n_sen]

                if curr_sent == -1:
                    curr_sent = len(sentence_offsets[curr_par]) - 1

                is_type.add((essay_id, component_id, typ))
                all_types.append(typ)
                in_par.add((essay_id, component_id, curr_par))

                if curr_par not in data[essay_id]:
                    data[essay_id][curr_par] = {}

                '''
                if typ == 'MajorClaim':
                    print curr_par, typ, text, offset_l, offset_r
                '''

                text_l, text_r = sentence_text[curr_par][curr_sent].split(text.decode('utf-8', 'strict'))
                text_l_tokens = nltk.word_tokenize(text_l.lower())
                n_tokens_left = len(text_l_tokens)
                n_tokens_right = len(nltk.word_tokenize(text_r))

                preceding_tokens = []
                for word in text_l_tokens:
                    if word.decode('utf-8') not in word2idx:
                        word2idx[word.decode('utf-8')] = curr_idx
                        curr_idx += 1
                    preceding_tokens.append(word2idx[word.decode('utf-8')])

                preceding_text_par = text_full[paragraph_offsets[curr_par]:int(offset_l)]

                if curr_par + 1 < len(paragraph_offsets):
                    following_text_par = text_full[int(offset_r):paragraph_offsets[curr_par+1]]
                else:
                    following_text_par = text_full[int(offset_r):]

                data[essay_id][curr_par][component_id] =\
                        {'offset_l': int(offset_l), 'offset_r': int(offset_r), 'text': text,
                         'curr_par': int(curr_par), 'curr_sent': int(curr_sent),
                         'tokens': tokens, 'n_tokens_cover_par': paragraph_tokens[curr_par],
                         'n_tokens_cover_sent': sentence_tokens[curr_par][curr_sent],
                         'n_tokens_left': n_tokens_left, 'n_tokens_right': n_tokens_right,
                         'preceding_text': text_l, 'preceding_text_par': preceding_text_par,
                         'following_text_par': following_text_par,
                         'preceding_tokens': preceding_tokens, 'text_np': text_np, 'text_vp': text_vp}
            elif component_id.startswith('A'):
                if ann.strip().endswith('For'):
                    _, curr_id, _ = ann.strip().split()
                    is_for.add((essay_id, curr_id))
                else:
                    _, curr_id, _ = ann.strip().split()
                    is_against.add((essay_id, curr_id))

            elif component_id.startswith('R'):
                if ann.startswith('supports'):
                    _, arg1, arg2 = ann.strip().split()
                    _, arg1_id = arg1.split(':')
                    _, arg2_id = arg2.split(':')
                    supports.add((essay_id, arg1_id, arg2_id))
                    stances.add((essay_id, arg1_id, arg2_id, 'Support'))
                else:
                    _, arg1, arg2 = ann.strip().split()
                    _, arg1_id = arg1.split(':')
                    _, arg2_id = arg2.split(':')
                    attacks.add((essay_id, arg1_id, arg2_id))
                    stances.add((essay_id, arg1_id, arg2_id, 'Attack'))

        components = set([])
        for par in range(1, data[essay_id]['num_pars']):
            if par in data[essay_id]:
                components |= set(data[essay_id][par].keys())

        claims = set([]); major_claims = set([])
        for comp in components:

            is_claim_comp = False
            if (essay_id, comp, 'Claim') in is_type:
                claims.add(comp); is_claim_comp = True
            elif (essay_id, comp, 'MajorClaim') in is_type:
                major_claims.add(comp)

            for comp2 in components:
                is_major_comp2 = (essay_id, comp2, 'MajorClaim') in is_type
                if (essay_id, comp, comp2) not in (supports | attacks)\
                    and not (is_claim_comp and is_major_comp2):
                    stances.add((essay_id, comp, comp2, 'None'))

        for claim in claims:
            for major in major_claims:
                if (essay_id, claim) in is_for:
                    stances.add((essay_id, claim, major, 'Support'))
                elif (essay_id, claim) in is_against:
                    stances.add((essay_id, claim, major, 'Attack'))

with open("data/has_component.txt", "w") as fp:
    for (essay_id, component_id) in has_component:
        fp.write("{0} {1}\n".format(essay_id, component_id))

with open("data/has_stance.txt", "w") as fp:
    for (essay_id, comp_id, comp2_id, typ) in stances:
        fp.write("{0} {1} {2} {3}\n".format(essay_id, comp_id, comp2_id, typ))

with open("data/is_argument_type.txt", "w") as fp:
    for (essay_id, component_id, typ) in is_type:
        fp.write("{0} {1} {2}\n".format(essay_id, component_id, typ))

with open("data/supports.txt", "w") as fp:
    for (essay_id, arg1_id, arg2_id) in supports:
        fp.write("{0} {1} {2}\n".format(essay_id, arg1_id, arg2_id))

with open("data/reln.txt", "w") as fp:
    for (essay_id, arg1_id, arg2_id) in supports | attacks:
        fp.write("{0} {1} {2}\n".format(essay_id, arg1_id, arg2_id))

with open("data/is_for.txt", "w") as fp:
    for (essay_id, component_id) in is_for:
        fp.write("{0} {1}\n".format(essay_id, component_id))

with open("data/in_par.txt", "w") as fp:
    for (essay_id, component_id, n_par) in in_par:
        fp.write("{0} {1} {2}\n".format(essay_id, component_id, n_par))

with open("data/data.json", "w") as fp:
    json.dump(data, fp)


vocab_one = set(word2idx.keys())

with open("data/word2idx.json", "w") as fp:
    json.dump(word2idx, fp)

with open("data/word2idx.json", "r") as fp:
    vocab_json = json.load(fp)

vocab_two = set(vocab_json.keys())

print len(vocab_one), len(vocab_two)
print len(vocab_one - vocab_two)


print "argument components", len(has_component)
print "argument components - par info", len(in_par)
print "claims (for)", len(is_for)
print "claims (against)", len(is_against)
print "support relns", len(supports)
print "attack relns", len(attacks)
print Counter(all_types)
