argtype_dict = {}
output = []

with open('data/is_argument_type.txt', 'r') as f:
    for line in f.readlines():
        essay, comp, argtype = line.split()

        if essay not in argtype_dict:
            argtype_dict[essay] = {}

        argtype_dict[essay][comp] = argtype

with open('data/has_stance.txt', 'r') as f:
    for line in f.readlines():
        essay, c1, c2, stance = line.split()
        out = ' '.join([essay, c1, stance])

        if stance != 'None' and out not in output:
            output.append(out)

        if argtype_dict[essay][c1] == 'MajorClaim' and out not in output:
            output.append(out)

with open('data/node_stance.txt', 'w') as f:
    for out in output:
        f.write(out + '\n')