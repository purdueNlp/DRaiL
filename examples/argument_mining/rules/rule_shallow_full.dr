entity:    "Component",    arguments: ["componentId"::ArgumentType.UniqueString];
entity:    "Essay",        arguments: ["essayId"::ArgumentType.UniqueString];
entity:    "ArgType",      arguments: ["argType"::ArgumentType.UniqueString];
entity:    "StanceType",   arguments: ["argType"::ArgumentType.UniqueString];
entity:    "Par",          arguments: ["parId"::ArgumentType.UniqueID];

predicate: "HasComponent", arguments: [Essay, Component];
predicate: "IsArgType",    arguments: [Essay, Component, ArgType];
predicate: "HasStance",    arguments: [Essay, Component, Component, StanceType];
predicate: "Stance",       arguments: [Essay, Component, StanceType];
predicate: "Reln",         arguments: [Essay, Component, Component];
predicate: "Supports",     arguments: [Essay, Component, Component];
predicate: "IsFor",        arguments: [Essay, Component];
predicate: "InPar",        arguments: [Essay, Component, Par];

predicate: "LessThan",     arguments: [Component, Component];
predicate: "Different",    arguments: [Component, Component, Component];
predicate: "CoParent",     arguments: [Essay, Component, Component, Component];
predicate: "Grandparent",  arguments: [Essay, Component, Component, Component];

label: "ArgLabel", classes: 3, type: LabelType.Multiclass;
label: "StanceLabel", classes: 3, type: LabelType.Multiclass;

load: "HasComponent", file: "has_component.txt";
load: "IsArgType",    file: "is_argument_type.txt";
load: "HasStance",    file: "has_stance.txt";
load: "Stance",       file: "node_stance.txt";
load: "Reln",         file: "reln.txt";
load: "Supports",     file: "supports.txt";
load: "IsFor",        file: "is_for.txt";
load: "ArgType",      file: "argument_label.txt";
load: "ArgLabel",     file: "argument_label.txt";
load: "StanceType",   file: "stance_label.txt";
load: "StanceLabel",  file: "stance_label.txt";
load: "InPar",        file: "in_par.txt";
load: "LessThan",     file: "less_than.txt";
load: "Different",    file: "different.txt";
load: "CoParent",     file: "co_parent.txt";
load: "Grandparent",  file: "grandparent.txt";

femodule: "Argument_ft";
feclass: "Argument_ft";

ruleset {

  rule: HasComponent(E,C) & InPar(E,C,P) => IsArgType(E,C,T^ArgLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [vector("bow_component_left"),
                vector("structural_feats"), vector("indicator_feats"), vector("context_feats")],
  target: C;

  rule: HasComponent(E,A) & HasComponent(E,B) & InPar(E,A,P) & InPar(E,B,P) => Reln(E,A,B)^?
  lambda: 1.0,
  network: "config.json",
  fefunctions: [vector("bow_component_left"),
                vector("bow_component_right"),
                input("component_feats_left"),
                input("component_feats_right"),
                vector("reln_feats")],
  target: C;

  rule: HasComponent(E,A) & InPar(E,A,P) => Stance(E,A,T^StanceLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [vector("bow_component_left"),
                vector("structural_feats"), vector("indicator_feats"), vector("context_feats")],
  target: C;

  // C <- B <- A
  rule: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C) & InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Different(A,B,C) => Grandparent(E,C,A,B)^?
  lambda: 1.0,
  network: "config.json",
  fefunctions: [vector("bow_component_left"),
                vector("bow_component_right"),
                vector("bow_component_third"),
                input("component_feats_left"),
                input("component_feats_right"),
                input("component_feats_third"),
                vector("reln_triple_feats")],
  target: C;

  // A -> B <- C
  rule: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C) & InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Different(A,B,C) & LessThan(A,C) => CoParent(E,B,A,C)^?
  lambda: 1.0,
  network: "config.json",
  fefunctions: [vector("bow_component_left"),
                vector("bow_component_right"),
                vector("bow_component_third"),
                input("component_feats_left"),
                input("component_feats_right"),
                input("component_feats_third"),
                vector("reln_triple_feats")],
  target: C;

  // Grandparent constraints
  hardconstr: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C)
                & InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Grandparent(E,C,B,A)^?
                & Different(A,B,C) & Reln(E,B,C) => Reln(E,A,B)^?;

  hardconstr: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C)
                & InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Grandparent(E,C,B,A)^?
                & Different(A,B,C) & Reln(E,B,C) => Reln(E,B,C)^?;


  // CoParent constraints
  hardconstr: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C) &
              InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Different(A,B,C) 
              & Reln(E,C,B) & LessThan(A,C) & CoParent(E,B,A,C)^?
              => Reln(E,A,B)^?;

  hardconstr: HasComponent(E,A) & HasComponent(E,B) & HasComponent(E,C) &
              InPar(E,A,P) & InPar(E,B,P) & InPar(E,C,P) & Different(A,B,C) 
              & Reln(E,A,B) & LessThan(A,C) & CoParent(E,B,A,C)^?
              => Reln(E,C,B)^?;


  // source is always a premise
  hardconstr: HasComponent(E,A) & HasComponent(E,B)
              & InPar(E,A,P) & InPar(E,B,P) & Reln(E,A,B)^?
                => IsArgType(E,A,"Premise")^?;

  // target is either a premise or a claim
  hardconstr: HasComponent(E,A) & HasComponent(E,B)
              & InPar(E,A,P) & InPar(E,B,P) & Reln(E,A,B)^?
                => ~IsArgType(E,B,"MajorClaim")^?;

  // major claim has no stance
  hardconstr: HasComponent(E,A) & InPar(E,A,P) & IsArgType(E,A,"MajorClaim")^?
              => Stance(E,A,"None")^?;

  // claim must have stance
  hardconstr: HasComponent(E,A) & InPar(E,A,P) & IsArgType(E,A,"Claim")^?
              => ~Stance(E,A,"None")^?;

  // premise must have stance
  hardconstr: HasComponent(E,A) & InPar(E,A,P) & IsArgType(E,A,"Premise")^?
              => ~Stance(E,A,"None")^?;

  // define tree structure
  treeconstr: HasComponent(E,A) & HasComponent(E,B) & InPar(E,A,P) & InPar(E,B,P) => Reln(E,A,B)^?,
  root: ~IsArgType(E,A,"Premise"),
  notRoot: IsArgType(E,A,"Premise"),
  leaf: IsArgType(E,B,"MajorClaim");

} groupby: HasComponent.1;
