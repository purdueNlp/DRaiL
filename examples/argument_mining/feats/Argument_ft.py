import json
import logging
import re
import random

import numpy as np
from nltk import word_tokenize

from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils


class Argument_ft(FeatureExtractor):

    def __init__(self, word_emb_f, data_f, word2idx_f, indicators_f, prodrules_f, pos_f, lemmas_f, debug=False):
        super(Argument_ft, self).__init__()
        self.word_emb_f = word_emb_f
        self.data_f = data_f
        self.word2idx_f = word2idx_f
        self.indicators_f = indicators_f
        self.prodrules_f = prodrules_f
        self.lemmas_f = lemmas_f
        self.pos_f = pos_f
        self.debug = debug
        self.logger = logging.getLogger(self.__class__.__name__)

    def build(self):
        # Labels for components
        self.component_idx = {'MajorClaim': 0, 'Claim': 1, 'Premise': 2}
        self.stance_idx = {'Support': 1, 'Attack': 0, 'None': 2}

        self.num_types, self.size_types, self.types_onehot \
                = utils.onehot_dictionary(self.component_idx.keys())

        # Load POS tags
        if self.pos_f:
            with open(self.pos_f) as fp:
                pos_tags = fp.readlines()
                self.pos_tags = {t.strip():v for (t,v) in zip(pos_tags, range(0, len(pos_tags)))}

        # Load actual text and other data for features
        if self.data_f:
            with open(self.data_f) as fp:
                self.data = json.load(fp)

        # Load word indices
        if self.word2idx_f:
            with open(self.word2idx_f) as fp:
                self.word2idx = json.load(fp)

        # Load indicators
        if self.indicators_f:
            with open(self.indicators_f) as fp:
                self.indicators = json.load(fp)

        for typ in self.indicators:
            self.indicators[typ] = "|".join(self.indicators[typ]).lower()

        # Load word embeddings from txt file
        vocabulary = set(self.word2idx.keys())
        self.vocab_size = len(vocabulary)
        self.logger.info("vocab_size: {}".format(self.vocab_size))
        if self.word_emb_f:
            _, self.word_emb_size, self.word_dict =\
                utils.embeddings_dictionary_txt(self.word_emb_f, vocabulary=vocabulary, debug=self.debug)

        # Load productions rules
        if self.prodrules_f:
            with open(self.prodrules_f) as fp:
                prod_rules = json.load(fp)

        prod_rules = [(k, prod_rules[k]) for k in prod_rules]
        prod_rules.sort(reverse=True)
        prod_rules = prod_rules[:500]
        prod_rules = [k for (v,k) in prod_rules]
        self.prod_rules_idx = dict({k:v for (k,v) in zip(prod_rules, range(0, 500))})

        # Load lemmas
        if self.lemmas_f:
            with open(self.lemmas_f) as fp:
                lemmas = json.load(fp)

        lemmas = [(k, lemmas[k]) for k in lemmas]
        lemmas.sort(reverse=True)
        lemmas = lemmas[:500]
        lemmas = [k for (v,k) in lemmas]
        self.lemmas_idx = dict({k:v for (k,v) in zip(lemmas, range(0, 500))})

    def bow_component_left(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[0]
        essay_id, component_id, n_par = in_par['arguments']
        bow_vector = [0.0] * len(self.word2idx)
        for token in self.data[essay_id][str(n_par)][component_id]['preceding_tokens']:
            bow_vector[token] = 1.0
        for token in self.data[essay_id][str(n_par)][component_id]['tokens']:
            bow_vector[token] = 1.0
        return bow_vector

    def bow_component_right(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[1]
        essay_id, component_id, n_par = in_par['arguments']
        bow_vector = [0.0] * len(self.word2idx)
        for token in self.data[essay_id][str(n_par)][component_id]['preceding_tokens']:
            bow_vector[token] = 1.0
        for token in self.data[essay_id][str(n_par)][component_id]['tokens']:
            bow_vector[token] = 1.0
        return bow_vector

    def bow_component_third(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[2]
        essay_id, component_id, n_par = in_par['arguments']
        bow_vector = [0.0] * len(self.word2idx)
        for token in self.data[essay_id][str(n_par)][component_id]['preceding_tokens']:
            bow_vector[token] = 1.0
        for token in self.data[essay_id][str(n_par)][component_id]['tokens']:
            bow_vector[token] = 1.0
        return bow_vector

    def words_component_left(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[0]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_tokens'] + \
               self.data[essay_id][str(n_par)][component_id]['tokens']

    def words_component_right(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[1]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_tokens'] + \
               self.data[essay_id][str(n_par)][component_id]['tokens']

    def words_component_third(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[2]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_tokens'] + \
               self.data[essay_id][str(n_par)][component_id]['tokens']

    def elmo_paragraph(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[0]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)]['elmo_par']

    def component_offsets_left(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[0]
        essay_id, component_id, n_par = in_par['arguments']
        # i, j, i-1, j+1
        return [self.data[essay_id][str(n_par)][component_id]['offset_l_par'],
                self.data[essay_id][str(n_par)][component_id]['offset_r_par'],
                self.data[essay_id][str(n_par)][component_id]['offset_l_par'] - 1,
                self.data[essay_id][str(n_par)][component_id]['offset_r_par'] + 1]

    def component_offsets_right(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[1]
        essay_id, component_id, n_par = in_par['arguments']
        # i, j, i-1, j+1
        return [self.data[essay_id][str(n_par)][component_id]['offset_l_par'],
                self.data[essay_id][str(n_par)][component_id]['offset_r_par'],
                self.data[essay_id][str(n_par)][component_id]['offset_l_par'] - 1,
                self.data[essay_id][str(n_par)][component_id]['offset_r_par'] + 1]

    def elmo_component_left(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[0]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_elmo'] + \
               self.data[essay_id][str(n_par)][component_id]['elmo']


    def elmo_component_right(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[1]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_elmo'] + \
               self.data[essay_id][str(n_par)][component_id]['elmo']

    def elmo_component_third(self, rule_grd):
        in_par = rule_grd.get_body_predicates("InPar")[2]
        essay_id, component_id, n_par = in_par['arguments']
        return self.data[essay_id][str(n_par)][component_id]['preceding_elmo'] + \
               self.data[essay_id][str(n_par)][component_id]['elmo']

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        if head['name'] == 'IsArgType':
            label = head['arguments'][2]
            return self.component_idx[label]
        if head['name'] == 'HasStance':
            label = head['arguments'][3]
            return self.stance_idx[label]
        if head['name'] == 'Stance':
            label = head['arguments'][2]
            return self.stance_idx[label]

    def context_feats(self, rule_grd, index=0):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']

        preceding_text = self.data[essay_id][str(n_par)][component_id]['preceding_text_par'].lower()
        following_text = self.data[essay_id][str(n_par)][component_id]['following_text_par'].lower()

        has_forward_follows = int(re.search(self.indicators["forward"], following_text) is not None)
        has_backward_follows = int(re.search(self.indicators["backward"], following_text) is not None)
        has_thesis_follows = int(re.search(self.indicators["thesis"], following_text) is not None)
        has_rebuttal_follows = int(re.search(self.indicators["rebuttal"], following_text) is not None)

        has_forward_precedes = int(re.search(self.indicators["forward"], preceding_text) is not None)
        has_backward_precedes = int(re.search(self.indicators["backward"], preceding_text) is not None)
        has_thesis_precedes = int(re.search(self.indicators["thesis"], preceding_text) is not None)
        has_rebuttal_precedes = int(re.search(self.indicators["rebuttal"], preceding_text) is not None)

        np_intro = set(self.data[essay_id]['np_intro'])
        vp_intro = set(self.data[essay_id]['vp_intro'])
        np_concl = set(self.data[essay_id]['np_concl'])
        vp_concl = set(self.data[essay_id]['vp_concl'])
        np_curr = set(self.data[essay_id][str(n_par)][component_id]['text_np'])
        vp_curr = set(self.data[essay_id][str(n_par)][component_id]['text_vp'])

        np_overlap_intro = int(len(np_intro & np_curr) > 0)
        np_overlap_intro_count = len(np_intro & np_curr)
        np_overlap_concl = int(len(np_concl & np_curr) > 0)
        np_overlap_concl_count = len(np_concl & np_curr)

        vp_overlap_intro = int(len(vp_intro & vp_curr) > 0)
        vp_overlap_intro_count = len(vp_intro & vp_curr)
        vp_overlap_concl = int(len(vp_concl & vp_curr) > 0)
        vp_overlap_concl_count = len(vp_concl & vp_curr)

        return [has_forward_follows, has_backward_follows, has_thesis_follows, has_rebuttal_follows,
                has_forward_precedes, has_backward_precedes, has_thesis_precedes, has_rebuttal_precedes,
                np_overlap_intro, np_overlap_concl, np_overlap_intro_count, np_overlap_concl_count,
                vp_overlap_intro, vp_overlap_concl, vp_overlap_intro_count, vp_overlap_concl_count]

    def indicator_feats(self, rule_grd, index=0):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']

        text = self.data[essay_id][str(n_par)][component_id]['text'].lower()
        preceeding_text = self.data[essay_id][str(n_par)][component_id]['preceding_text'].lower()

        has_forward = int(re.search(self.indicators["forward"], text) is not None or re.search(self.indicators["forward"], preceeding_text) is not None)
        has_backward = int(re.search(self.indicators["backward"], text) is not None or re.search(self.indicators["backward"], preceeding_text) is not None)
        has_thesis = int(re.search(self.indicators["thesis"], text) is not None or re.search(self.indicators["thesis"], preceeding_text) is not None)
        has_rebuttal = int(re.search(self.indicators["rebuttal"], text) is not None or re.search(self.indicators["rebuttal"], preceeding_text) is not None)

        first_person = int(re.search('(^|\s)(i|me|my|mine|myself)($|\s)', text) is not None or
                           re.search('(^|\s)(i|me|my|mine|myself)($|\s)', preceeding_text) is not None)

        return [has_forward, has_backward, has_thesis, has_rebuttal, first_person]

    def structural_feats(self, rule_grd, index=0):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']


        curr_offset = self.data[essay_id][str(n_par)][component_id]['offset_l']

        other_offsets = [self.data[essay_id][str(n_par)][cid]['offset_l'] \
                            for cid in self.data[essay_id][str(n_par)] if cid != "elmo_par"]

        min_offset = min(other_offsets)
        max_offset = max(other_offsets)

        is_component_frst_inpar = 0
        is_component_last_inpar = 0
        if curr_offset < min_offset:
            is_component_frst_inpar = 1
        if curr_offset > max_offset:
            is_component_last_inpar = 1

        relative_pos_inpar = curr_offset / (1.0 * max_offset)

        n_preceding_components = sum(x < curr_offset for x in other_offsets)
        n_following_components = sum(x > curr_offset for x in other_offsets)

        last_par = self.data[essay_id]['num_pars'] - 1
        is_component_in_intro = int(n_par == 1)
        is_component_in_concl = int(n_par == last_par)


        n_tokens = len(self.data[essay_id][str(n_par)][component_id]['tokens'])
        n_tokens_cover_par = self.data[essay_id][str(n_par)][component_id]['n_tokens_cover_par']
        n_tokens_cover_sent = self.data[essay_id][str(n_par)][component_id]['n_tokens_cover_sent']
        component_sentence_ratio = n_tokens / (1.0 * n_tokens_cover_sent)
        n_tokens_left = self.data[essay_id][str(n_par)][component_id]['n_tokens_left']
        n_tokens_right = self.data[essay_id][str(n_par)][component_id]['n_tokens_right']


        return [is_component_frst_inpar, is_component_last_inpar, relative_pos_inpar,
                n_preceding_components, n_following_components, is_component_in_intro,
                is_component_in_concl, n_tokens, n_tokens_cover_par, n_tokens_cover_sent,
                component_sentence_ratio, n_tokens_left, n_tokens_right]

    def component_feats_left(self, rule_grd, pad=True):
        ret =  self.structural_feats(rule_grd, 0) + \
               self.indicator_feats(rule_grd, 0) + self.context_feats(rule_grd, 0)
        return ret

    def component_feats_right(self, rule_grd, pad=True):
        ret = self.structural_feats(rule_grd, 1) + \
               self.indicator_feats(rule_grd, 1) + self.context_feats(rule_grd, 1)
        return ret

    def component_feats_third(self, rule_grd, pad=True):
        ret = self.structural_feats(rule_grd, 2) + \
               self.indicator_feats(rule_grd, 2) + self.context_feats(rule_grd, 2)
        return ret

    def reln_indicators(self, rule_grd):
        in_par_source = rule_grd.get_body_predicates("InPar")[0]
        essay_id_source, component_id_source, n_par_source = in_par_source['arguments']

        in_par_target = rule_grd.get_body_predicates("InPar")[1]
        essay_id_target, component_id_target, n_par_target = in_par_target['arguments']

        offset_l_source = self.data[essay_id_source][str(n_par_source)][component_id_source]['offset_l']
        offset_r_source = self.data[essay_id_source][str(n_par_source)][component_id_source]['offset_r']

        offset_l_target = self.data[essay_id_target][str(n_par_target)][component_id_target]['offset_l']
        offset_r_target = self.data[essay_id_target][str(n_par_target)][component_id_target]['offset_r']

        if offset_l_source < offset_l_target:
            text_in_between = self.data[essay_id_source]['full_text'][offset_r_source:offset_l_target]
        else:
            text_in_between = self.data[essay_id_source]['full_text'][offset_r_target:offset_l_source]

        has_forward = int(re.search(self.indicators["forward"], text_in_between) is not None)
        has_backward = int(re.search(self.indicators["backward"], text_in_between) is not None)
        has_thesis = int(re.search(self.indicators["thesis"], text_in_between) is not None)
        has_rebuttal = int(re.search(self.indicators["rebuttal"], text_in_between) is not None)

        first_person = int(re.search('(^|\s)(i|me|my|mine|myself)($|\s)', text_in_between) is not None)

        return [has_forward, has_backward, has_thesis, has_rebuttal, first_person]

    def reln_structural(self, rule_grd):
        in_par_source = rule_grd.get_body_predicates("InPar")[0]
        essay_id_source, c_id_source, n_par_source = in_par_source['arguments']

        in_par_target = rule_grd.get_body_predicates("InPar")[1]
        essay_id_target, c_id_target, n_par_target = in_par_target['arguments']

        source_offset = self.data[essay_id_source][str(n_par_source)][c_id_source]['offset_l']
        target_offset = self.data[essay_id_target][str(n_par_target)][c_id_target]['offset_l']

        source_sent = self.data[essay_id_source][str(n_par_source)][c_id_source]['curr_sent']
        target_sent = self.data[essay_id_target][str(n_par_target)][c_id_target]['curr_sent']

        n_components_in_between = 0

        n_par_begin = min([n_par_source, n_par_target]); begin_offset = min([source_offset, target_offset])
        n_par_end   = max([n_par_source, n_par_target]); end_offset   = max([source_offset, target_offset])
        for in_betw_par in range(n_par_begin, n_par_end + 1):
            if str(in_betw_par) not in self.data[essay_id_source]:
                continue
            for c_id in self.data[essay_id_source][str(in_betw_par)]:
                if c_id == "elmo_par":
                    continue
                offset_l = self.data[essay_id_source][str(in_betw_par)][c_id]['offset_l']
                if offset_l > begin_offset and offset_l < end_offset:
                    n_components_in_between += 1

        same_par = int(n_par_source == n_par_target)
        same_sen = int(source_sent == target_sent)

        target_before_source = int(n_par_target < n_par_source or
                                   target_sent < source_sent or
                                   target_offset < source_offset)
        target_after_source = int(n_par_target > n_par_source or
                                   target_sent > source_sent or
                                   target_offset > source_offset)

        both_in_intro = int(n_par_source == 1 and n_par_target == 1)
        last_par = self.data[essay_id_source]['num_pars'] - 1
        both_in_concl = int(n_par_source == last_par and n_par_target == last_par)

        return [n_components_in_between, same_par, same_sen, target_before_source,
                target_after_source, both_in_intro, both_in_concl]

    def reln_shared_nouns(self, rule_grd):
        in_par_source = rule_grd.get_body_predicates("InPar")[0]
        essay_id_source, c_id_source, n_par_source = in_par_source['arguments']

        in_par_target = rule_grd.get_body_predicates("InPar")[1]
        essay_id_target, c_id_target, n_par_target = in_par_target['arguments']

        np_source = set(self.data[essay_id_source][str(n_par_source)][c_id_source]['text_np'])
        np_target = set(self.data[essay_id_target][str(n_par_target)][c_id_target]['text_np'])

        np_overlap = int(len(np_source & np_target) > 0)
        np_overlap_count = len(np_source & np_target)

        return [np_overlap, np_overlap_count]

    def reln_entity_type(self, rule_grd, index):
        is_type = rule_grd.get_body_predicates("IsArgType")[index]
        essay_id, component_id, component_type = is_type['arguments']
        return self.types_onehot[component_type]

    def reln_lemmas(self, rule_grd, index):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']

        ret = [0] * 500
        for lemma in self.data[essay_id][str(n_par)][component_id]['lemmas']:
            if lemma in self.lemmas_idx:
                ret[self.lemmas_idx[lemma]] = 1
        return ret

    def reln_prod_rules(self, rule_grd, index):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']

        ret = [0] * 500
        for prod_rule in self.data[essay_id][str(n_par)][component_id]['prod_rules']:
            if prod_rule in self.prod_rules_idx:
                ret[self.prod_rules_idx[prod_rule]] = 1
        return ret

    def reln_pos(self, rule_grd, index):
        in_par = rule_grd.get_body_predicates("InPar")[index]
        essay_id, component_id, n_par = in_par['arguments']

        ret = [0] * 40
        for pos in self.data[essay_id][str(n_par)][component_id]['pos']:
            if pos in self.pos_tags:
                ret[self.pos_tags[pos]] = 1
        return ret


    def reln_feats(self, rule_grd):
        ret = self.reln_structural(rule_grd) + self.reln_indicators(rule_grd) + \
              self.reln_shared_nouns(rule_grd) + self.reln_prod_rules(rule_grd, 0) +\
              self.reln_prod_rules(rule_grd, 1)
              #+ self.reln_pos(rule_grd, 0) + self.reln_pos(rule_grd, 1) + self.reln_lemmas(rule_grd, 0) + self.reln_lemmas(rule_grd, 1)
        #print len(ret)
        #exit()
        return ret

    def reln_triple_feats(self, rule_grd):
        return self.reln_prod_rules(rule_grd, 0) + self.reln_prod_rules(rule_grd, 1) + self.reln_prod_rules(rule_grd, 2)

    def reln_transitions(self, rule_grd):
        ret = self.reln_entity_type(rule_grd, 0) +\
              self.reln_entity_type(rule_grd, 1)
        return ret
