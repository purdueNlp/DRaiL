import numpy as np
import argparse
import os
import cProfile as profile
import random
import torch
import csv
import json
import logging.config
from pathlib import Path

from sklearn.metrics import *
from sklearn.model_selection import KFold

from drail import database
from drail.model.argument import ArgumentType
from drail.learn.global_learner import GlobalLearner
from drail.learn.local_learner import LocalLearner
from drail.learn.joint_learner import JointLearner

from drail.inference.randomized.purepython.infer_argmining import RInfArgMining # pure python
# from drail.inference.randomized.infer_argmining import RInfArgMining              # C++

def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('-f', '--fedir', help='fe directory', dest='fedir', type=str, required=True)
    parser.add_argument('-n', '--netdir', help='net directory', dest='nedir', type=str, required=True)
    parser.add_argument('-m', help='mode: [global|local|joint]', dest='mode', type=str, default='local')
    parser.add_argument('--we_file', help='word embedding file in txt form (e.g. glove)', dest='we_file', type=str, required=True)
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--splits', help='splits', dest='splits', type=str, required=True)
    parser.add_argument('--inference-only', help='run inference only', dest='inference_only', default=False, action='store_true')
    parser.add_argument('--train-only', help='run training only', dest='train_only', default=False, action='store_true')
    parser.add_argument('--drop-scores', help='drop predicted scores', dest='drop_scores', default=False, action='store_true')
    parser.add_argument('--continue-from-checkpoint', help='continue from checkpoint in savedir', dest='continue_from_checkpoint', default=False, action='store_true')
    parser.add_argument('--lr', help='global learning rate', dest='global_lr', type=float)
    parser.add_argument('--delta', help='loss augmented inference', dest='delta', action='store_true', default=False)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--folds', help='number of folds', dest='folds', type=int, default=1)
    parser.add_argument('--randomized', help='randomized inference on training', dest='rand_inf', default=False, action='store_true')
    parser.add_argument('--ad3', help='use ad3 solver for training', default=False, action='store_true')
    parser.add_argument('--ad3_only', help='use ad3 solver rather than ilp', default=False, action='store_true')
    parser.add_argument('-p', '--pkldir', help='pkl directory', dest='pkldir', type=str, default=None)
    parser.add_argument('--loss', help='mode: [crf|hinge|hinge_smooth]', dest='lossfn', type=str, default="joint")
    parser.add_argument('--logging_config', help='logging configuration file', type=str, required=True)
    parser.add_argument('--hot-start-joint', help='hot start with joint model', dest='hot_start_joint', default=False, action='store_true')
    parser.add_argument('--hot-start-local', help='hot start with joint model', dest='hot_start_local', default=False, action='store_true')
    parser.add_argument('--hot-start-lr', help='local learning rate', dest='local_lr', type=float, default=0.05)
    parser.add_argument('--repeats', help='repetitions of the experiment', dest='repeats', type=int, default=1)
    parser.add_argument('--no_randinf_constraints', help='deactivates constraints on randomized inference', default=False, action='store_true')

    opts = parser.parse_args()
    return opts

def get_folds(folds_file, n_folds):
    own_folds = True if n_folds > 1 else False

    with open(folds_file, 'rU') as csvfile:
        readcsv = csv.reader(csvfile)
        train_essays = []; test_essays = []

        # skip first line
        for row in list(readcsv)[1:]:
            essay, fold = row[0].split(';')
            if fold == '"TRAIN"' or own_folds:
                train_essays.append(essay)
            else:
                test_essays.append(essay)
    random.shuffle(train_essays)

    if own_folds:
        essays = train_essays
        train_essays, dev_essays, test_essays = [], [], []
        k_folds = KFold(n_splits=n_folds, shuffle=True)

        for train, test in k_folds.split(essays):
            dev_split = int(len(train) * 0.8)

            train_essays.append([essays[i] for i in train[:dev_split]])
            dev_essays.append([essays[i] for i in train[dev_split:]])
            test_essays.append([essays[i] for i in test])
    else:
        dev_split    = int(len(train_essays) * 0.8)
        dev_essays   = train_essays[dev_split:]
        train_essays = train_essays[:dev_split]

    return train_essays, dev_essays, test_essays


def main():
    opts = parse_options()
    realpath = os.path.dirname(os.path.realpath(__file__)) + '/'
    continue_from_checkpoint_ORIGINAL = opts.continue_from_checkpoint

    # External resources for features
    # These were generated running extract_features.py + txt file for glove embeddings
    DATA_F       = realpath + "data/data_updated.json"
    WORD2IDX_F   = realpath + "data/word2idx.json"
    INDICATORS_F = realpath + "data/indicators.json"
    PRODRULES_F  = realpath + "data/prod_rules.json"
    POS_F        = realpath + "data/pos_tags.txt"
    LEMMAS_F     = realpath + "data/lemmas.json"

    # seed and cuda
    np.random.seed(1234)
    random.seed(1234)

    train_inference_solver = "randomized" if opts.rand_inf else "AD3" if opts.ad3_only or opts.ad3 else "ILP"
    test_inference_solver  = "ILP" if not opts.ad3_only else "AD3"

    logger.info("train inference solver: {}".format(train_inference_solver))
    logger.info("test inference solver: {}".format(test_inference_solver))
    logger.info("no randomized inference constraints: {}".format(opts.no_randinf_constraints))

    if opts.gpu_index:
        torch.cuda.set_device(opts.gpu_index)

    rule_file = os.path.join(opts.rules)
    conf_file = os.path.join(opts.config)

    # TO-DO: Generalize loading new inference classes and its parameters
    if opts.mode == "global":
        learner = GlobalLearner(learning_rate=opts.global_lr,
                                use_gpu=(opts.gpu_index is not None),
                                gpu_index=opts.gpu_index,
                                rand_inf=opts.rand_inf,
                                inferencer=RInfArgMining,
                                ad3=opts.ad3,
                                ad3_only=opts.ad3_only,
                                loss_fn=opts.lossfn,
                                no_randinf_constraints=opts.no_randinf_constraints)
    elif opts.mode == "joint":
        learner = JointLearner()
    else:
        learner = LocalLearner()

    learner.compile_rules(rule_file)
    db = learner.create_dataset(opts.dir)

    train, dev, test = get_folds(opts.splits, opts.folds)
    avg_metrics = {}

    for repeat in range(opts.repeats):
        for i in range(opts.folds):
            curr_savedir = os.path.join(opts.savedir, "r{}".format(repeat), "f{}".format(i))
            Path(curr_savedir).mkdir(parents=True, exist_ok=True)

            if opts.folds > 1:
                train_essays, dev_essays, test_essays = train[i], dev[i], test[i]
            else:
                train_essays, dev_essays, test_essays = train, dev, test

            # Debug
            if opts.debug:
                train_essays = train_essays[:10]
                dev_essays = dev_essays[:10]
                test_essays = test_essays[:10]

            logger.info("train {}, dev {}, test {}".format(len(train_essays), len(dev_essays), len(test_essays)))

            db.add_filters(filters=[
                ("HasComponent", "isDummy", "essayId_1", train_essays[0:1]),
                ("HasComponent", "isTrain", "essayId_1", train_essays),
                ("HasComponent", "isDev", "essayId_1", dev_essays),
                ("HasComponent", "isTest", "essayId_1", test_essays),
                #("HasComponent", "isTrainDev", "essayId_1", train_essays + dev_essays),
                ("HasComponent", "allFolds", "essayId_1", train_essays + dev_essays + test_essays),
                ])

            learner.build_feature_extractors(db,
                                            word_emb_f=opts.we_file,
                                            data_f=DATA_F,
                                            word2idx_f=WORD2IDX_F,
                                            indicators_f=INDICATORS_F,
                                            prodrules_f=PRODRULES_F,
                                            pos_f=POS_F,
                                            debug=opts.debug,
                                            femodule_path=opts.fedir,
                                            lemmas_f=LEMMAS_F,
                                            filters=[("HasComponent", "isDummy", 1)])
            learner.set_savedir(curr_savedir)
            learner.build_models(db, conf_file, netmodules_path=opts.nedir)

            pklpath = None
            if opts.pkldir is not None:
                pklpath = os.path.join(opts.pkldir, "f{0}".format(i))

            if opts.mode == "local" or opts.mode == "joint":
                if opts.continue_from_checkpoint:
                    learner.init_models(scale_data=True)

                if not opts.inference_only:
                    learner.train(db, train_filters=[("HasComponent", "isTrain", 1)], dev_filters=[("HasComponent", "isDev", 1)], test_filters=[("HasComponent", "isTest", 1)], scale_data=True)

                if not opts.train_only:
                    if opts.drop_scores:
                        learner.extract_data(db, extract_test=True, extract_dev=True, extract_train=True,
                                             test_filters=[("HasComponent", "isTest", 1)],
                                             dev_filters=[("HasComponent", "isDev", 1)],
                                             train_filters=[("HasComponent", "isTrain", 1)])
                        learner.drop_scores(None, fold='test', output='test_scores.csv', scale_data=True)
                        learner.drop_scores(None, fold='train', output='train_scores.csv', scale_data=True)
                        learner.drop_scores(None, fold='dev', output='dev_scores.csv', scale_data=True)
                    else:
                        learner.extract_data(db, extract_test=True,
                                             test_filters=[("HasComponent", "isTest", 1)])
                    del learner.fe
                    res, heads = learner.predict(None, fold='test', get_predicates=True, scale_data=True)
            else:
                learner.extract_data(
                        db,
                        train_filters=[("HasComponent", "isTrain", 1)],
                        dev_filters=[("HasComponent", "isDev", 1)],
                        test_filters=[("HasComponent", "isTest", 1)],
                        extract_train=not opts.inference_only,
                        extract_dev=not opts.inference_only,
                        extract_test=True,
                        pickledir=pklpath,
                        from_pickle=(opts.pkldir is not None))

                if (opts.hot_start_joint and not opts.inference_only):
                    logger.info("Hot starting model using 'joint' training")
                    # Set params for local learning, re-build models
                    learner.loss_fn = "joint"
                    learner.learning_rate = opts.local_lr
                    learner.build_models(db, conf_file,
                                         netmodules_path=opts.nedir)
                    learner.train(
                            db,
                            train_filters=[("HasComponent", "isTrain", 1)],
                            dev_filters=[("HasComponent", "isDev", 1)],
                            test_filters=[("HasComponent", "isTest", 1)],
                            opt_predicate='IsArgType',
                            loss_augmented_inference=opts.delta,
                            continue_from_checkpoint=opts.continue_from_checkpoint,
                            scale_data=True,
                            weight_classes=True,
                            patience=10,
                            train_only=True)
                    # Check the continue from checkpoint flag
                    # and reset original params, re-build models
                    learner.loss_fn = opts.lossfn
                    learner.learning_rate = opts.global_lr
                    learner.build_models(db, conf_file,
                                         netmodules_path=opts.nedir)
                    opts.continue_from_checkpoint = True
                elif (opts.hot_start_local and not opts.inference_only):
                    logger.info("Hot starting model using 'local' training")
                    learner.hot_start_local(db,
                            train_filters=[("HasComponent", "isTrain", 1)],
                            dev_filters=[("HasComponent", "isDev", 1)],
                            test_filters=[("HasComponent", "isTest", 1)],
                            scale_data=True)
                    opts.continue_from_checkpoint = True

                res, heads = learner.train(
                        db,
                        train_filters=[("HasComponent", "isTrain", 1)],
                        dev_filters=[("HasComponent", "isDev", 1)],
                        test_filters=[("HasComponent", "isTest", 1)],
                        opt_predicate='IsArgType',
                        loss_augmented_inference=opts.delta,
                        continue_from_checkpoint=opts.continue_from_checkpoint,
                        inference_only=opts.inference_only,
                        scale_data=True,
                        weight_classes=True,
                        patience=10)
                opts.continue_from_checkpoint = continue_from_checkpoint_ORIGINAL

            if not opts.train_only:
                for pred in res.metrics:
                    if pred in set(['IsArgType', 'Reln', 'Supports', 'IsFor', 'HasStance', 'Grandparent', 'CoParent', 'Stance']):
                        y_gold = res.metrics[pred]['gold_data']
                        y_pred = res.metrics[pred]['pred_data']

                        if pred not in avg_metrics:
                            avg_metrics[pred] = {}
                            avg_metrics[pred]['gold'] = y_gold
                            avg_metrics[pred]['pred'] = y_pred
                        else:
                            avg_metrics[pred]['gold'].extend(y_gold)
                            avg_metrics[pred]['pred'].extend(y_pred)

                        logger.info('\n'+ pred + ':\n' + classification_report(y_gold, y_pred, digits=4))
                        macro_f1 = f1_score(y_gold, y_pred, average='macro')
                        logger.info("TEST macro f1 {}".format(macro_f1))

                learner.reset_metrics()

            if not opts.inference_only and opts.mode == 'global':
                # pure inference time during training
                logger.info('\ninference encoding time:\t{0}\n'.format(learner.train_metrics.metrics['encoding_time']) +\
                      'inference optimiz. time:\t{0}\n'.format(learner.train_metrics.metrics['solving_time']) +\
                      'total inference time:   \t{0}'.format(learner.train_metrics.total_time()))

                learner.reset_train_metrics()

    if opts.folds > 1:
        logger.info(40 * '=' + '\navg classif. report of {0} folds\n'.format(opts.folds) + 40 * '=')
        for pred in avg_metrics:
            logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))
            logger.info("TEST macro f1 {}".format(f1_score(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], average='macro')))

if __name__ == "__main__":
    opts = parse_options()
    if opts.logging_config:
        logger = logging.getLogger()
        logging.config.dictConfig(json.load(open(opts.logging_config)))
    main()
