import os
import torch
import time
import random
import joblib
import numpy as np
import logging
from collections import OrderedDict
from tqdm import tqdm, trange
import csv
from sklearn.metrics import *
from transformers import AdamW
import pickle

from .learner import Learner
from .metrics import Metrics

from ..model.rule import RuleGrounding
from ..neuro.nn_model_global import NeuralHub
from ..neuro import nn_utils
from ..inference.ilp_inferencer import *
from ..inference.ad3_inferencer import *
from ..model.scoring_function import ScoringType

#from memory_profiler import profile

class GlobalLatentLearner(Learner):

    def __init__(self, learning_rate, use_gpu, gpu_index, rand_inf=False, inference_limit=None, ad3=False, loss_fn="hinge"):
        super(GlobalLatentLearner, self).__init__()
        self.learning_rate = learning_rate
        self.reset()
        self.timestamp = time.time()
        self.use_gpu = use_gpu
        self.gpu_index = gpu_index
        self.rand_inference = rand_inf
        self.inference_limit = inference_limit
        self.logger = logging.getLogger(self.__class__.__name__)
        self.loss_fn = loss_fn
        self.ad3 = ad3

    def reset(self):
        super(GlobalLatentLearner, self).reset()
        self.brain = None; self.optimizer = None
        self.train_metrics = Metrics()
        self.dev_metrics = Metrics()
        self.test_metrics = Metrics()

    def build_models(self, db, config_path, netmodules_path=None, module=None, isdic=False):
        self.nns_ls = []; brain_nns_lambdas_ls = []; brain_nns_open_lhs = []
        self._build_models(db, config_path, self.nns_ls, brain_nns_open_lhs, brain_nns_lambdas_ls,
                           netmodules_path, module, isdic)
        # TO-DO pass lambda parameters as input to GL
        self.brain = NeuralHub(self.nns_ls, brain_nns_open_lhs, brain_nns_lambdas_ls,
                               learning_rate=self.learning_rate,
                               use_gpu=self.use_gpu, l1_lambda=0, l2_lambda=0)

    def extract_observed_data(self, db, train_filters=[], dev_filters=[], test_filters=[]):
        # extract training data
        observed_train = self.get_observed_data(
                db, train_filters)
        observed_dev = self.get_observed_data(
                db, dev_filters)
        observed_test = self.get_observed_data(
                db,  test_filters)
        return observed_train, observed_dev, observed_test

    def get_class_weights(self, db, fold_filters):
        print("Calculating class weights...")
        classes = {}
        N = len(self.instances['train'])
        pbar = tqdm(total=N)
        for i, inst in enumerate(self.instances['train']):
            instance_groundings_inst, gold_heads_inst, _, _, _ =\
                self.get_instance_data(db, fold_filters, 'train', inst, mode='train')
            gold_heads_str = set([RuleGrounding.pred_str(h) for h in gold_heads_inst])

            y_gold_ls_i, y_gold_index_ls_i = \
                    self.get_structures(instance_groundings_inst, gold_heads_str)

            for index, y in enumerate(y_gold_ls_i):
                if index not in classes:
                    classes[index] = []

                classes[index] += list(y)
            pbar.update(1)
        pbar.close()

        class_weights = [None] * len(classes)
        for index in classes:
            n_classes = self.brain.potentials[index].output_dim

            # n_classes must be at least 2:
            if n_classes == 1:
                n_classes += 1

            #print(self.par.ruleset['rule'][index].head.name in self.latent_predicates, self.par.ruleset['rule'][index].head.name)
            # if latent, we don't know what class weights to use, so use a uniform distribution
            if self.par.ruleset['rule'][index].head.name in self.latent_predicates:
                class_weights[index] = torch.from_numpy(np.ones((n_classes,))).float()
            else:
                class_weights[index] = torch.from_numpy(nn_utils.compute_weights(classes[index], n_classes))

            if self.use_gpu:
                class_weights[index] = class_weights[index].cuda()

        '''
        class_weights = [None] * len(self.brain.potentials)
        for index, nn in enumerate(self.brain.potentials):
            if 'class_weights' not in nn.config:
                self.logger.warning("You must include class weights in the configuration file, or turn off weighting")
            else:
                class_weights[index] = torch.FloatTensor(nn.config['class_weights'])
                if self.use_gpu:
                    class_weights[index] = class_weights[index].cuda()
        '''
        return class_weights

    def get_structures(self, instance_groundings, active_heads, get_class_weights=False, istrain=False):
        y_ls = []; y_index_ls = [];

        '''
        for head in active_heads:
            print head
        exit()
        '''

        #print "\n----- Structures for instance -----"
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            # TO-DO: skip non-neural rules
            scref = rule_template.scoring_function.scref
            #print ruleidx, rule_template, rule_template.head.isobs, scref
            for index in range(0, len(self.nnindex_ls[scref])):
                y = []; y_index = []
                if len(instance_groundings[ruleidx]) > 0:
                    bgs = instance_groundings[ruleidx][index]
                    for b_index, body in enumerate(bgs):
                        # if the rule is active

                        #print "==== body", b_index, body
                        # Separate unobserved predicates in positives and negations
                        negations = [b for b in bgs[body]['unobs'] if b.startswith('~')]
                        negations_complement = set([b[1:] for b in negations])
                        positives = set([b for b in bgs[body]['unobs'] if not b.startswith('~')])

                        # If the positives are part of the active heads and the complement of the negations are NOT part of the active heads
                        if len(positives & active_heads) == len(positives) and len(negations_complement & active_heads) == 0:
                            # binary head
                            if not rule_template.head.isobs:
                                positive_index = bgs[body]['heads_index'].index(1)
                                if bgs[body]['heads'][positive_index] in active_heads:
                                    y.append(1)
                                else:
                                    y.append(0)
                            else:
                                for head, head_index in zip(bgs[body]['heads'], bgs[body]['heads_index']):
                                    #print "\thead", head, head_index, head in active_heads
                                    if head in active_heads:
                                        #print "\thead, head_index", head, head_index
                                        y.append(head_index)
                            y_index.append(b_index)

                    #print("scref", "len(y_ls)", scref, len(y_ls))

                if scref >= len(y_ls):
                    y_ls.append(np.asarray(y).astype(np.int64))
                    y_index_ls.append(np.asarray(y_index).astype(np.int64))
                else:
                    #print("prev y_ls", y_ls)
                    #print("prev y_index_ls", y_index_ls)
                    y_ls[scref] = np.append(y_ls[scref], np.array(y, dtype=np.int64), axis=0)
                    if len(y_index_ls[scref]) > 0:
                        y_index_ls[scref] = np.append(y_index_ls[scref], np.array(y_index + (y_index_ls[scref][-1] + 1), dtype=np.int64), axis=0)
                    else:
                        y_index_ls[scref] = np.append(y_index_ls[scref], np.array(y_index, dtype=np.int64), axis=0)

        return y_ls, y_index_ls

    def hot_start(self, db, observed_train, observed_dev):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                nnet = self.brain.potentials[nnidx]
                X_train, Y_train = observed_train[ruleidx][index]
                X_dev, Y_dev = observed_dev[ruleidx][index]
                nn_utils.train_local_on_epoches(nnet, X_train, Y_train, X_dev, Y_dev, None, None, modeldir)

    def load_models(self, scale_data=False):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            scref = rule_template.scoring_function.scref
            #print ruleidx, rule_template, scref
            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                self.logger.info("loading {}...".format(modeldir))
                if os.path.exists(modeldir):
                    if self.gpu_index is None:
                        loc = "cpu"
                    else:
                        loc = "cuda:{0}".format(self.gpu_index)
                    self.brain.potentials[nnidx].load_state_dict(torch.load(modeldir, map_location=loc), strict=False)

                else:
                    self.logger.info("path doesn't exist, using random initialization")


    def save_models(self):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            scref = rule_template.scoring_function.scref
            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                model_state = self.brain.potentials[nnidx].state_dict()
                torch.save(model_state, modeldir)

    def predict_fold(self, db, epoch, K, opt_predicates, fold_filters, fold, y_gold_dev=False, scale_data=False, accum_loss=True, istrain=False):
        self.logger.info("Predicting {0} fold...".format(fold))
        N = len(self.instances[fold])
        pbar = tqdm(total=N)
        current_metrics = Metrics()

        loss_accum = 0; n_steps = 0
        for i, inst in enumerate(self.instances[fold]):
            #print("instance #{}: {}".format(i, inst))
            instance_groundings_inst, gold_heads_inst,\
            constraint_groundings_inst, constraint_heads_inst,\
            tree_constraint_groundings_inst =\
                self.get_instance_data(db, fold_filters, fold, inst, mode='test', istrain=istrain)

            active_heads, xs_ls, metrics = \
                self.predict(instance_groundings_inst,
                            gold_heads_inst,
                            constraint_groundings_inst,
                            tree_constraint_groundings_inst,
                            get_active_heads = True,
                            K=K, instance_index=i, scale_data=scale_data,
                            instance=inst)
            current_metrics.load_metrics(metrics)

            if y_gold_dev is not False:
                active_heads_gold, _, _ = \
                    self.predict(instance_groundings_inst,
                                gold_heads_inst,
                                constraint_groundings_inst,
                                tree_constraint_groundings_inst,
                                get_active_heads=True,
                                K=K, instance_index=i,
                                scale_data=scale_data,
                                instance=inst,
                                fix_y=True)

                y_pred_ls_i, y_pred_index_ls_i = \
                        self.get_structures(instance_groundings_inst, active_heads)
                y_gold_ls_i, y_gold_index_ls_i = \
                        self.get_structures(instance_groundings_inst, active_heads_gold)

                dev_loss = nn_utils.eval_loss(self.brain, xs_ls, y_pred_ls_i, y_pred_index_ls_i,
                                              y_gold_ls_i, y_gold_index_ls_i,
                                              None, None,
                                              accum_loss,
                                              self.loss_fn)
                loss_accum += dev_loss
                n_steps += 1
            pbar.update(1)
            #for head in dev_active_heads:
            #    print head
        pbar.close()

        if n_steps > 0:
            loss_accum = loss_accum / n_steps
        # this is hardcoded here, ideally we want to separate by predicate

        # reset metrics
        n_classifs = 0; perf_string = ""; acc = 0
        current_metrics_classifiers = set(current_metrics.metrics.keys())
        for classif_name in current_metrics.metrics:
            if classif_name not in set(["encoding_time", "solving_time"]):
                # I am recording all performances but only considering opt_predicates (if avail) for stopping criteria
                #if len(opt_predicates) > 0 and classif_name not in opt_predicates and len(opt_predicates & current_metrics_classifiers) > 0:
                #    continue
                # TO-DO: make this not be hardcoded!!
                if classif_name in ["Reln", "HasMaslow", "HasReiss", "HasPlutchik", "HasSubFrame", "HasFrame"]:
                    curr_acc = f1_score(current_metrics.metrics[classif_name]["gold_data"], current_metrics.metrics[classif_name]["pred_data"], average='binary')
                else:
                    curr_acc = f1_score(current_metrics.metrics[classif_name]["gold_data"], current_metrics.metrics[classif_name]["pred_data"], average='macro')
                    #curr_acc = accuracy_score(current_metrics.metrics[classif_name]["gold_data"], current_metrics.metrics[classif_name]["pred_data"])

                perf_string += ", %s: %f" % (classif_name, curr_acc)

                if (classif_name in opt_predicates) or (len(opt_predicates) <= 0) or (len(opt_predicates & current_metrics_classifiers) <= 0):
                    acc += curr_acc; n_classifs += 1

        acc = (acc * 1.0)/n_classifs
        perf_string = 'epoch %d %s | opt: %f, loss %f' % (epoch, fold, acc , loss_accum) + perf_string
        self.logger.info(perf_string)

        return acc, current_metrics

    def _set_optimizer(self, optimizer_name):
        if optimizer_name == 'SGD':
            self.optimizer = torch.optim.SGD(self.brain.parameters(),
                                        lr=self.brain.lr)
        elif optimizer_name == 'Adam':
            optimizer = torch.optim.Adam(self.brain.parameters(),
                                     lr=self.brain.lr)
        elif optimizer_name == 'AdamW':
            self.optimizer = torch.optim.AdamW(filter(lambda p: p.requires_grad, self.brain.parameters()),
                                     lr=self.brain.lr)
        # This is transformer-specific
        elif optimizer_name == 'AdamW-transformers':
            weight_decay=0.0; adam_epsilon=1e-8; max_grad_norm = 1.0
            # Prepare optimizer and schedule (linear warmup and decay)
            no_decay = ['bias', 'LayerNorm.weight']
            optimizer_grouped_parameters = [
                {'params': [p for n, p in self.brain.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
                {'params': [p for n, p in self.brain.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
                                            ]
            self.optimizer = AdamW(optimizer_grouped_parameters, lr=self.brain.lr, eps=adam_epsilon)

    #@profile
    def train(self, db, train_filters, dev_filters, test_filters, opt_predicates, n_epoch=1,
              hot_start=False, loss_augmented_inference=False, K=None,
              continue_from_checkpoint=False, inference_only=False, scale_data=False,
              weight_classes=False, accum_loss=True, patience = 20, optimizer='SGD', train_only=False,
              drop_scores=False, score_file="scores.csv"):

        self._set_optimizer(optimizer)
        self.logger.info("training...")
        if scale_data:
            if not self.scalers_ls:
                self.get_observed_data(db, train_filters, scale_data, scale_role='train')
            else:
                scalerdir = os.path.join(self.savedir, "scaler.joblib")
                self.logger.info("Loading scalers from {}".format(scalerdir))
                self.scalers_ls = joblib.load(scalerdir)

        if not inference_only:
            #modeldir = os.path.join(self.savedir, "model.pt")
            if continue_from_checkpoint:
                #self.brain.load_state_dict(torch.load(modeldir))
                print("Loading models")
                self.load_models()

            if hot_start:
                # get observed data to check local nets
                # and hot start each instance if hs is on
                observed_train, observed_dev, observed_test = \
                self.extract_observed_data(db, train_filters, dev_filters, test_filters)
                self.hot_start(db, observed_train, observed_dev)

            # mark feature things for garbage collection
            #del self.fe

            # weight classes in loss functions
            # TO-DO find a new way to measure the weights
            class_weights = [None] * len(self.brain.potentials)
            if weight_classes:
                class_weights = self.get_class_weights(db, train_filters)

            #exit()
            loss = None
            best_val_measure = -float("infinity")
            done_looping = False
            patience_counter = 0
            self.brain.set_local_losses(class_weights)

            random.seed(1234)

            # check starting point
            #print "starting point..."
            #print "K", K
            #best_val_measure, _ = self.predict_fold(db, 0, K, opt_predicates, dev_filters, 'dev', scale_data=scale_data, accum_loss=accum_loss)
            #self.save_models()
            #_ = self.predict_fold(0, K, opt_predicates, 'test', scale_data=scale_data)

            loss_accum = 0
            total_t = 0
            n_steps = 0
            epoch = 1

            dev_f1, _ = self.predict_fold(db, epoch, K, opt_predicates, dev_filters, 'dev', y_gold_dev=True, scale_data=scale_data, accum_loss=accum_loss)

            while not done_looping:
                self.logger.info("Training epoch {0}...".format(epoch))
                instance_ids = self.instances['train']
                #print(self.instances)
                #exit()
                N = len(instance_ids)
                pbar = tqdm(total=N)
                epoch_start  = time.time()
                random.shuffle(instance_ids)

                for i, inst in enumerate(instance_ids):
                    # run inference
                    instance_groundings_inst, gold_heads_inst,\
                    constraint_groundings_inst, constraint_heads_inst,\
                    tree_constraint_groundings_inst =\
                        self.get_instance_data(db, train_filters, 'train', inst, mode='train', istrain=True)

                    #for c in constraint_groundings_inst:
                    #    print("\t{}".format(c))

                    if self.loss_fn != "joint":
                        active_heads, _, train_metrics = \
                                self.predict(instance_groundings_inst,
                                        gold_heads_inst,
                                        constraint_groundings_inst,
                                        tree_constraint_groundings_inst,
                                        get_active_heads=True,
                                        loss_augmented_inference=loss_augmented_inference,
                                        K=K, istrain=True,
                                        scale_data=scale_data,
                                        instance=inst)

                        if len(active_heads) == 0:
                            continue

                        y_pred_ls_i, y_pred_index_ls_i = \
                                self.get_structures(instance_groundings_inst, active_heads)

                    else:
                        y_pred_ls_i = None; y_pred_index_ls_i = None

                    active_heads_gold, xs_ls, _ = \
                            self.predict(
                                    instance_groundings_inst,
                                    gold_heads_inst,
                                    constraint_groundings_inst,
                                    tree_constraint_groundings_inst,
                                    get_active_heads=True,
                                    loss_augmented_inference=loss_augmented_inference,
                                    K=K, istrain=True,
                                    scale_data=scale_data,
                                    instance=inst,
                                    fix_y=True)

                    y_gold_ls_i, y_gold_index_ls_i = \
                            self.get_structures(instance_groundings_inst, active_heads_gold)

                    loss = nn_utils.backpropagate_global(
                            self.brain, self.optimizer, xs_ls,
                            y_pred_ls_i, y_pred_index_ls_i,
                            y_gold_ls_i, y_gold_index_ls_i, 
                            None, None, accum_loss, self.loss_fn)

                    #print i, ": ", inst, " loss: ", loss
                    loss_accum += loss
                    n_steps += 1
                    pbar.update(1)
                pbar.close()
                epoch_t  = time.time() - epoch_start
                total_t += epoch_t

                #train_f1, _ = self.predict_fold(epoch, K, opt_predicates, 'train', scale_data=scale_data)
                self.logger.info("took: {0} - train loss: {1}".format(epoch_t, loss_accum / n_steps))
                dev_f1, _ = self.predict_fold(db, epoch, K, opt_predicates, dev_filters, 'dev', y_gold_dev=True, scale_data=scale_data, accum_loss=accum_loss)
                #test_f1, _ = self.predict_fold(epoch, K, opt_predicates, 'test', scale_data=scale_data)

                if dev_f1 > best_val_measure:
                    patience_counter = 0
                    best_val_measure = dev_f1
                    best_epoch = epoch
                    #model_state = self.brain.state_dict()
                    #torch.save(model_state, modeldir)
                    self.save_models()
                else:
                    patience_counter += 1
                    # hardcoded for now
                    if patience_counter >= patience:
                        done_looping = True

                epoch += 1
            self.logger.info("BEST F1: {0} - Run took: {1}".format(best_val_measure, total_t))

        #self.brain.load_state_dict(torch.load(modeldir))
        if not train_only:
            csvwriter = None; score_fp = None
            if drop_scores:
                self.logger.info("droping scores...")
                score_fp = open(score_file, "w")
                csvwriter = csv.writer(score_fp)

            self.load_models()

            # predict the final thing
            active_heads = set([])
            for i, inst in enumerate(self.instances['test']):
                instance_groundings_inst, gold_heads_inst,\
                constraint_groundings_inst, constraint_heads_inst,\
                tree_constraint_groundings_inst =\
                    self.get_instance_data(db, test_filters, 'test', inst, mode='test', get_constr_heads=False, istrain=False)

                # run inference
                test_active_heads, _, test_metrics = \
                        self.predict(instance_groundings_inst,
                                gold_heads_inst,
                                constraint_groundings_inst,
                                tree_constraint_groundings_inst,
                                get_active_heads = True,
                                K=K, scale_data=scale_data,
                                drop_scores=drop_scores, writer_scores=csvwriter)
                self.test_metrics.load_metrics(test_metrics)
                active_heads |= test_active_heads

            if drop_scores:
                score_fp.close()

            return self.test_metrics, active_heads

    # TO-DO: set custom Ks per rule, now just using cardinality
    def predict(self, instance_groundings,
                gold_heads, constraint_groundings, tree_constraint_groundings,
                get_active_heads=True, time_limit=None,
                loss_augmented_inference=False, K=None, istrain=False, instance_index=1,
                scale_data=False, instance=None, fix_y=False, drop_scores=False, writer_scores=None):

        inference_weights = OrderedDict()
        xs_ls = []; potentials_ls = []

        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            #print "Rule", ruleidx, rule_template
            K = rule_template.head_cardinality()

            # get neural netw reference
            scref = rule_template.scoring_function.scref
            #print(scref)
            # TO-DO: deal with manual scoring rules

            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                #print "NNet", index, nnidx

                X = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []};
                y_pred = []; pivots = []

                has_bodies = (len(instance_groundings[ruleidx]) > 0) and \
                             (len(instance_groundings[ruleidx][index].keys()) > 0)

                if has_bodies:

                    # predict with neural nets
                    nnet = self.brain.potentials[nnidx]
                    for body in instance_groundings[ruleidx][index]:
                        #print("predicting", body)
                        X = self._add_features(
                                instance_groundings[ruleidx][index][body]['feat_repr'],
                                X)

                    if scale_data:
                        self._scale_features(nnidx, X, scale_role='test')
                    y_pred = nn_utils.predict_local_scores(nnet, X)
                    #print(y_pred)

                    # we want only the top k solutions, find the pivots to discriminate
                    partial_order = np.partition(y_pred, y_pred.shape[1] - K)
                    pivots = partial_order[:,y_pred.shape[1] - K]
                else:
                    X = []

                if scref >= len(xs_ls):
                    xs_ls.append(X)
                elif has_bodies and len(xs_ls[scref]) > 0:
                    xs_ls[scref]['vector'] += X['vector']
                    xs_ls[scref]['input'] += X['input']
                    xs_ls[scref]['nonuminput'] += X['nonuminput']
                    for emb in xs_ls[scref]['embedding']:
                        xs_ls[scref]['embedding'][emb] += X['embedding'][emb]
                elif has_bodies and len(xs_ls[scref]) == 0:
                    xs_ls[scref] = X

                # instantiate weights
                if len(instance_groundings[ruleidx]) > 0:
                    for body, scores, pivot in zip(instance_groundings[ruleidx][index], y_pred, pivots):
                    #for body, scores in zip(instance_groundings[ruleidx][index], y_pred):
                        for gr_i, gr in enumerate(instance_groundings[ruleidx][index][body]['groundings']):
                            #print(gr_i, gr)
                            if gr.is_binary_head and not gr.head['isneg']:
                                idx_in_nn_out = 1
                            elif gr.is_binary_head and gr.head['isneg']:
                                idx_in_nn_out = 0
                            else:
                                idx_in_nn_out = instance_groundings[ruleidx][index][body]['heads_index'][gr_i]
                            #print(body, gr, scores, idx_in_nn_out)
                            weight = scores[idx_in_nn_out]

                            # if weight not in the top K solutions, do not add it to the problem
                            if weight >= pivot:
                                inference_weights[gr] = weight * rule_template.lambda_
                                #print("ADDING", gr, inference_weights[gr])
                                if loss_augmented_inference and not gr.isgroundtruth and istrain:
                                    inference_weights[gr] += 1
                            #else:
                            #    print weight, pivot
                            #    print gr

                            #if not istrain and instance_index < 5:
                            #    print gr, inference_weights[gr]
                            #inference_weights[gr] = weight * instance_groundings[ruleidx][index][body]['lambda']
        #exit()
        start = time.time()

        # AD3 inference
        if self.ad3:
            inferencer = AD3Inferencer(inference_weights, constraint_groundings, tree_constraint_groundings, self.tree_info, exact=False)
            inferencer.encode(gold_heads=gold_heads, fix_gold=fix_y, latent_predicates=self.latent_predicates)
        # ILP inference
        else:
            inferencer = ILPInferencer(inference_weights, constraint_groundings, tree_constraint_groundings, self.tree_info, False, inferenceLimit=self.inference_limit)
            inferencer.encode(gold_heads=gold_heads, fix_gold=fix_y, latent_predicates=self.latent_predicates)

        end_encode = time.time()
        inferencer.optimize()
        end = time.time()

        if istrain:
            self.train_metrics.metrics['encoding_time'] += end_encode - start
            self.train_metrics.metrics['solving_time']  += end - end_encode

        #print(gold_heads)

        metrics, active_heads = \
            inferencer.evaluate(gold_heads, binary_predicates=self.binary_classifiers,
                                get_active_heads=get_active_heads, latent_predicates=self.latent_predicates)


        '''
        for h in active_heads:
            print(h)
        #exit()
        print("------------------")
        '''

        if drop_scores:
            for gr in inference_weights:
                if active_heads:
                    curr_head = RuleGrounding.pred_str(gr.head)
                    unobs = set([curr_head] + gr.body_unobs_str)
                    if  len(unobs & active_heads) == len(unobs):
                        writer_scores.writerow([gr, inference_weights[gr], "1"])
                    else:
                        writer_scores.writerow([gr, inference_weights[gr], "0"])
                else:
                    writer_scores.writerow([gr, inference_weights[gr]])

        return active_heads, xs_ls, metrics

    def reset_metrics(self):
        self.test_metrics = Metrics()

    def reset_train_metrics(self):
        self.train_metrics = Metrics()
