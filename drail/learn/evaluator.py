class EvaluatorBase(object):
    def __init__(self):
        return NotImplementedError

    def measure(self, y_preds, y_golds, ids):
        '''
            return a float number as the measure
        '''
        return NotImplementedError
