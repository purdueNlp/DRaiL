import numpy as np
import os
import json
import sys
from collections import OrderedDict
import torch
import time
import random
import time
from sklearn.metrics import *
from transformers import AdamW
from transformers import get_linear_schedule_with_warmup

from .local_learner import LocalLearner
from ..neuro import nn_utils, nn_model
from ..inference.ilp_inferencer import *
from ..model.rule import RuleGrounding
from ..model.label import LabelType
from ..model.scoring_function import ScoringType

class JointLearner(LocalLearner):

    def __init__(self, gpu=False):
        super(JointLearner, self).__init__()

    def reset(self):
        super(JointLearner, self).reset()

    def _get_optimizers(self, optimizer_name, model):
        if optimizer_name == 'SGD':
            optimizer = torch.optim.SGD(model.parameters(),
                                        lr=float(model.config['learning_rate']))
        elif optimizer_name == 'Adam':
            optimizer = torch.optim.Adam(model.parameters(),
                                        lr=float(model.config['learning_rate']))
        elif optimizer_name == 'AdamW':
            weight_decay=0.0; adam_epsilon=1e-8; max_grad_norm = 1.0
            # Prepare optimizer and schedule (linear warmup and decay)
            no_decay = ['bias', 'LayerNorm.weight']
            optimizer_grouped_parameters = [
                {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
                {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
                                            ]
            optimizer = AdamW(optimizer_grouped_parameters, lr=float(model.config['learning_rate']), eps=adam_epsilon)
        return optimizer

    def build_models(self, db, config_path, netmodules_path=None, isdic=False):
        self.nns_ls = []; self.nns_lambda_ls = []; self.nns_open_lhs = []
        self.class_weights = []
        self._build_models(db, config_path, self.nns_ls, self.nns_open_lhs, self.nns_lambda_ls, netmodules_path, isdic)

    def set_optimizers_and_patience(self, optimizer_name):
        self.optimizers = []
        for model in self.nns_ls:
            n_batchsize, patience, n_predsize, measurement, average, \
            weighted_examples, pos_label, sort, sort_by, \
            print_stat, output, weight_decay, learning_rate, \
            adam_epsilon, max_grad_norm, clipping = \
                nn_utils.get_model_params(model)
            scheduler = None
            if optimizer_name == 'SGD':
                optimizer = torch.optim.SGD(model.parameters(),
                                            lr=learning_rate)
            elif optimizer_name == 'Adam':
                optimizer = torch.optim.Adam(model.parameters(),
                                         lr=learning_rate)
            # This is transformer-specific
            elif optimizer_name == 'AdamW':
                # Prepare optimizer and schedule (linear warmup and decay)
                no_decay = ['bias', 'LayerNorm.weight']
                optimizer_grouped_parameters = [
                    {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
                    {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
                                                ]
                optimizer = AdamW(optimizer_grouped_parameters, lr=learning_rate, eps=adam_epsilon)
                # TO-DO: how to deal with scheduler when the number of epochs is unknown and we rely on patience?
                # Hard-coding 3 epochs and 0 warmup steps
                #t_total = len(TrainY) * 3.0; warmup_steps=0
                #scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=warmup_steps, num_training_steps=t_total)

            self.optimizers.append(optimizer)
            self.class_weights.append(None)

    def load_models(self):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            scref = rule_template.scoring_function.scref
            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                print("loading", modeldir, "...")
                self.nns_ls[nnidx].load_state_dict(torch.load(modeldir))

    def save_models(self):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            scref = rule_template.scoring_function.scref
            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                model_state = self.nns_ls[nnidx].state_dict()
                torch.save(model_state, modeldir)

    def _get_class_weights(self, model, weighted_examples, TrainY):
        if weighted_examples:
            class_weights = torch.from_numpy(nn_utils.compute_weights(TrainY, model.output_dim))
            if model.use_gpu:
                class_weights = class_weights.cuda()
        else:
            class_weights = None
        return class_weights

    def set_patiences(self, target_ruleidx):
        self.patience_counters = [0] * len(self.patiences)
        if target_ruleidx is not None:
            for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
                if rule_template.scoring_function.sctype != ScoringType.NNet:
                    continue
                scref = rule_template.scoring_function.scref
                for index, nnidx in enumerate(self.nnindex_ls[scref]):
                    if ruleidx == target_ruleidx:
                        continue
                    else:
                        self.patience_counters[nnidx] = self.patiences[nnidx]

    def eval_models(self, observed_dev, observed_train, target_ruleidx, epoch, loss_fn):
        all_measures = 0.0; num_rules = 0.0
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            scref = rule_template.scoring_function.scref
            for index, nnidx in enumerate(self.nnindex_ls[scref]):
                curr_nn = self.nns_ls[nnidx]
                curr_nn_lambda = self.nns_lambda_ls[nnidx]

                n_batchsize, patience, n_predsize, measurement, average, \
                weighted_examples, pos_label, sort, sort_by, \
                print_stat, output, weight_decay, learning_rate, adam_epsilon, \
                max_grad_norm, clipping, balance_classes= \
                    nn_utils.get_model_params(curr_nn)

               # test train
                X_train, Y_train, train_ids = observed_train[ruleidx][index]

                train_batches, train_loss_sum, train_measure_sum = \
                    nn_utils.eval_local(curr_nn, X_train, Y_train, train_ids, loss_fn,
                               measurement, average, pos_label, sort, sort_by, output)

                train_loss = curr_nn_lambda * (train_loss_sum / (1.0 * train_batches))
                train_measure = train_measure_sum / (1.0 * train_batches)

                # test dev
                X_dev, Y_dev, dev_ids = observed_dev[ruleidx][index]

                eval_batches, eval_loss_sum, eval_measure_sum = \
                    nn_utils.eval_local(curr_nn, X_dev, Y_dev, dev_ids, loss_fn,
                               measurement, average, pos_label, sort, sort_by, output)

                val_loss = curr_nn_lambda * (eval_loss_sum / (1.0 * eval_batches))
                val_measure = eval_measure_sum / (1.0 * eval_batches)

                print("NNIDX", nnidx, "epoch", epoch, \
                        "train_loss", train_loss, "train_measure", train_measure, \
                        "val_loss", val_loss, "val_measure", val_measure)

                all_measures += val_measure
                num_rules += 1

        all_measures = all_measures / num_rules

        if (all_measures > self.best_measures):
            self.best_measures = all_measures
            self.save_models()
            self.patience_counter = 0
        else:
            self.patience_counter += 1
        print("---")

    # We want to alternate training between different nets
    def train(self, db, train_filters=[], dev_filters=[], test_filters=[], limit=-1, continue_from_checkpoint=False,
              eval_after_epochs=1, target_ruleidx=0, scale_data=False, optimizer='SGD'):
        self.set_optimizers_and_patience(optimizer)

        if continue_from_checkpoint:
            self.load_models()

        # extract training data
        print("extract train...")
        observed_train = self.get_observed_data(
                db, train_filters)
        print("extract dev...")
        observed_dev = self.get_observed_data(
                db, dev_filters)
        print("extract test...")
        observed_test = self.get_observed_data(
                db,  test_filters)

        #self.best_measures = [0] * len(self.patiences)
        #self.best_epochs = [0] * len(self.patiences)
        #self.set_patiences(target_ruleidx)
        #print("patiences", self.patience_counters)

        # Create probability samples based on dataset size
        ruleprobas = []
        for ruleidx in range(0, len(self.par.ruleset['rule'])):
            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                _, y_train, _ = observed_train[ruleidx][index]
                ruleprobas += [ruleidx] * len(y_train)
        #ruleprobas = [0] * 20 + [1] * 20 + [2] * 20 + [3] * 20 + [4] * 20

        epoch = 0

        # While there is some patience left
        timestamp = time.time()

        self.patience_counter = 0; self.patience = 3
        self.best_measures = 0.0
        while self.patience_counter < self.patience:
            # select a task at random
            ruleidx = random.choice(ruleprobas)
            rule_template = self.par.ruleset['rule'][ruleidx]

            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):

                nn = self.nns_ls[nnidx]
                lambda_ = self.nns_lambda_ls[nnidx]

                X_train, Y_train, train_ids = observed_train[ruleidx][index]

                n_batchsize, patience, n_predsize, measurement, average, \
                weighted_examples, pos_label, sort, sort_by, \

                print_stat, output, weight_decay, learning_rate, adam_epsilon, \
                max_grad_norm, clipping, balance_classes= \
                    nn_utils.get_model_params(nn)

                # checks for checkpoint
                savedir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                #nn_utils.initialize_model(nn, savedir)

                self.class_weights[nnidx] = self._get_class_weights(nn, weighted_examples, Y_train)
                #print(ruleidx, self.class_weights[nnidx])
                # Use cross entropy if multiclass or binary
                if (rule_template.head.label_type() == LabelType.Multiclass or \
                    rule_template.head.label_type() == LabelType.Binary or \
                    (not rule_template.head.isobs)):
                    #print "class_weights", class_weights
                    loss_fn = torch.nn.CrossEntropyLoss(weight=self.class_weights[nnidx])
                else:
                    # If multilabel
                    # TO-DO: class weights
                    loss_fn = torch.nn.MultiLabelSoftMarginLoss()

                epoch_start_time = time.time()

                train_batches, train_loss_sum, train_measure_sum = \
                    nn_utils.train_local(nn, lambda_, self.optimizers[nnidx], loss_fn,
                                X_train, Y_train, train_ids, n_batchsize, self.class_weights[nnidx],
                                measurement, average, pos_label, sort, sort_by, output,
                                num_batches=1)

                measure_end_time = time.time()

                #train_loss = train_loss_sum / (1.0 * train_batches)
                #train_measure = train_measure_sum / (1.0 * train_batches)
                #print "TRAIN ----  NNIDX", nnidx, "epoch", epoch, "train_loss", train_loss, "train_measure", train_measure, "time {:.3f}s".format(measure_end_time - epoch_start_time)

                if epoch % eval_after_epochs == 0:
                    self.eval_models(observed_dev, observed_train, target_ruleidx, epoch, loss_fn)
            #exit()
            epoch += 1

        # Report results on test set
        self.load_models()
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                nn = self.nns_ls[nnidx]
                X_test, Y_test, test_ids = observed_test[ruleidx][index]

                n_batchsize, patience, n_predsize, measurement, average, \
                weighted_examples, pos_label, sort, sort_by, \
                print_stat, output, weight_decay, learning_rate, \

                adam_epsilon, max_grad_norm, clipping, balance_classes= \
                    nn_utils.get_model_params(nn)

                test_Y_pred = nn_utils.predict_local(nn, X_test, sort, sort_by, output)
                print("\tTest scores")

                Y_test = np.array(Y_test); test_Y_pred = np.array(test_Y_pred)

                print(classification_report(Y_test, test_Y_pred))
                print("f1 macro", f1_score(Y_test, test_Y_pred, average='macro'))
                print("acc", accuracy_score(Y_test, test_Y_pred))

        print("\nTraining {} Epoches took {:.3f}s".format(epoch, time.time() - timestamp))

