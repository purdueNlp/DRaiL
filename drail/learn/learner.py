import sys
from collections import OrderedDict
from collections import Counter
import json
from importlib import import_module
import imp
import os
import tempfile
import joblib
import time
import pickle
import logging
from tqdm import tqdm, trange
import csv
import numpy as np
import torch
from sklearn import preprocessing
import random

from .metrics import Metrics
from ..database_new import Database
from ..parser import parser
from ..features.feature_extractor import FeatureExtractor
from ..model.label import LabelType
from ..model.rule import RuleGrounding
from ..model.scoring_function import ScoringType
from ..neuro import nn_utils


class Learner(object):

    def __init__(self):
        self.par = parser.Parser()
        # will be created during method calls
        self.latent_predicates = set([])
        self.reset()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.concept_support = {}

    def set_savedir(self, savedir=None):
        if savedir is None:
            self.savedir = tempfile.mkdtemp()
        else:
            self.savedir = savedir
            if not os.path.isdir(self.savedir):
                os.mkdir(self.savedir)

    def clear_savedir(self):
        if self.savedir is not None:
            os.rmdir(self.savedir)

    def reset(self):
        self.nnindex_ls = {}
        self.scalers_ls = {}
        self.fe = None
        self.sc = None
        self.binary_classifiers = set([])
        torch.manual_seed(12345)
        torch.cuda.manual_seed(12345)
        random.seed(12345)
        self.savedir = None

        # Data containers for when data is extracted once
        self.instance_groundings = {}

    def build_parser(self):
        self.par.build()

    def add_instruction(self, instruction):
        self.par.parse_text(instruction)

    def define_entity(self, entity_name, support=None):
        if support:
            self.concept_support[entity_name] = support

        instruction = 'entity: "{0}", arguments: ["{0}Id"::ArgumentType.UniqueString];'
        instruction = instruction.format(entity_name)
        self.add_instruction(instruction)

    def get_entities(self):
        return self.par.entity_arguments

    def define_predicate(self, predicate_name, entities, data_file):
        instr_1 = 'predicate: "{0}", arguments: [{1}];'
        instr_1 = instr_1.format(predicate_name, ",".join(entities))
        self.add_instruction(instr_1)
        instr_2 = 'load: "{0}", file: "{1}"'
        instr_2 = instr_2.format(predicate_name, data_file)
        self.add_instruction(instr_2)

    def get_predicates(self):
        return self.par.predicate_entities

    def get_data_files(self):
        return self.par.files

    def define_label(self, label_name, n_classes, data_file, multi_label=False):
        if multi_label:
            instr_1 = 'label: "{0}", classes: {1}, type: LabelType.Multilabel;'
        else:
            instr_1 = 'label: "{0}", classes: {1}, type: LabelType.Multiclass;'
        instr_1 = instr_1.format(label_name, n_classes)
        self.add_instruction(instr_1)
        instr_2 = 'load: "{0}", file: "{1}"'.format(label_name, data_file)
        self.add_instruction(instr_2)

    def get_labels(self):
        return self.par.label_classes

    def define_rule(self, rule_str, lmd, features):
        # TO-DO: remove assumption of things being INPUT type, and allow vector and embedding
        fefunctions = ", ".join(['input("{}")'.format(x) for x in features])
        instr_1 = 'rule: {0}, lambda: {1}, network: "config.json", fefunctions: [{2}];'
        instr_1 = instr_1.format(rule_str, lmd, fefunctions)
        self.add_instruction(instr_1)

    def define_manual_rule(self, rule_str, lmd, scoring_function, features):
        fefunctions = ", ".join(['input("{}")'.format(x) for x in features])
        instr_1 = 'rule: {0}, lambda: {1}, score: "{2}", fefunctions: [{3}];'
        instr_1 = instr_1.format(rule_str, lmd, scoring_function, fefunctions)
        #print(instr_1)
        self.add_instruction(instr_1)

    def define_hardconstr(self, rule_str):
        instr_1 = 'hardconstr: {0};'.format(rule_str)
        self.add_instruction(instr_1)

    def define_manual_softconstr(self, rule_str, lmd, scoring_function, features):
        fefunctions = ", ".join(['input("{}")'.format(x) for x in features])
        instr_1 = 'softconstr: {0}, lambda: {1}, score: "{2}", fefunctions: [{3}];'
        instr_1 = instr_1.format(rule_str, lmd, scoring_function, fefunctions)
        self.add_instruction(instr_1)

    def define_arithmetic_constr(self, rule_str):
        instr_1 = "arithmconstr: {0};".format(rule_str)
        self.add_instruction(instr_1)

    def define_treeconstr(self, rule_grd):
        instr_1 = 'treeconstr: {0};'.format(rule_str)
        self.add_instruction(instr_1)

    def define_scope(self, pred_name, entity_name):
        index = self.par.predicate_entities[pred_name].index(entity_name)
        instr_1 = "groupby: {0}.{1};".format(pred_name, index+1)
        self.add_instruction(instr_1)

    def get_scope(self):
        return self.par.groupby

    def get_rules(self):
        return self.par.ruleset

    def define_latent_predicate(self, predicate_name, entities):
        instr_1 = 'predicate: "{0}", arguments: [{1}];'
        instr_1 = instr_1.format(predicate_name, ",".join(entities))
        self.add_instruction(instr_1)
        instr_2 = 'latent: "{}";'.format(predicate_name)
        self.add_instruction(instr_2)
        self.latent_predicates = self.par.latent_predicates

    def get_latent_predicates(self):
        return self.latent_predicates

    def compile_rules(self, rule_path):
        self.par.build()
        self.par.parse(rule_path)
        self.ruleset = self.par.ruleset
        self.group_by = self.par.groupby
        self.latent_predicates = self.par.latent_predicates
        self.logger.info("Latent predicates={}".format(self.latent_predicates))

    def create_dataset(self, dataset_path, **kwargs):
        if self.par.dbmodule is None:
            db = Database()
            self.logger.info('self.par.files={}'.format(self.par.files))
            db.load_predicates(self.par.predicate_arguments,
                               dataset_path, self.par.files)
            db.load_predicates(self.par.entity_arguments,
                               dataset_path, self.par.files)
            db.load_labels(self.par.label_types.keys(),
                           dataset_path, self.par.files)
            db.predicate_entities = self.par.predicate_entities

        else:
            dbmodule_path = kwargs.pop('dbmodule_path', '.')
            module_path = os.path.join(dbmodule_path, "{0}.py".format(self.par.dbmodule))
            mod = imp.load_source(self.par.dbmodule, module_path)
            db_class = getattr(mod, self.par.dbclass)
            db = db_class()
            db.load_data(dataset_path)
        return db

    def update_dataset(self, db, dataset_path, **kwargs):
        self.logger.info('self.par.files={}'.format(self.par.files))
        db.load_predicates(self.par.predicate_arguments,
                           dataset_path, self.par.files)
        db.load_predicates(self.par.entity_arguments,
                           dataset_path, self.par.files)
        db.load_labels(self.par.label_types.keys(),
                       dataset_path, self.par.files)
        db.predicate_entities = self.par.predicate_entities
        return db

    def _get_multiclass_train(self, db, filters, ruleidx, rule_template, split_class):
        if self.par.dbmodule is None:
            train_groundings = db.unfold_train_groundings(
                    rule_template, ruleidx, split_class,
                    filter_by=filters, latent_preds=self.latent_predicates)
        else:
            train_groundings = getattr(db, rule_template.dbfunc)(
                    istrain=True,
                    isneg=False,
                    filters=filters,
                    split_class=None,
                    instance_id=None)
        return train_groundings

    def _get_binary_train(self, db, filters, ruleidx, rule_template, split_class):
        if self.par.dbmodule is None:
            neg_train_groundings = db.unfold_train_groundings(
                    rule_template, ruleidx, split_class,
                    filter_by=filters, neg_head=True, latent_preds=self.latent_predicates)
            pos_train_groundings = db.unfold_train_groundings(
                    rule_template, ruleidx, None,
                    filter_by=filters, neg_head=False, latent_preds=self.latent_predicates)
        else:
            neg_train_groundings = \
                    getattr(db, rule_template.dbfunc)(istrain=True,
                          isneg=True,
                          filters=filters,
                          split_class=split_class,
                          instance_id=None)
            pos_train_groundings = \
                    getattr(db, rule_template.dbfunc)(istrain=True,
                          isneg=False,
                          filters=filters,
                          split_class=split_class,
                          instance_id=None)
        return neg_train_groundings, pos_train_groundings

    def load_feature_class(self, kwargs):
        if not self.fe and self.par.femodule:
            femodule_path = kwargs.pop('femodule_path', '.')
            module_path = os.path.join(femodule_path, "{0}.py".format(self.par.femodule))
            mod = imp.load_source(self.par.femodule, module_path)
            fe_class = getattr(mod, self.par.feclass)
            self.fe = fe_class(**kwargs)
        elif self.fe:
            pass
        else:
            self.logger.error('A feature class needs to be defined')
            exit(-1)

    def load_scoring_class(self):
        if not self.sc and self.par.scmodule:
            # TO-DO: Fill this in
            pass
        elif self.sc:
            pass
        else:
            self.logger.error('A scoring class needs to be defined')
            exit(-1)

    def build_feature_extractors(self, db, filters=[], **kwargs):
        self.load_feature_class(kwargs)
        self.fe.concept_support = self.concept_support
        self.fe.build()

        '''
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            self.logger.info("{}".format(rule_template))
            # we don't need a NNet or feat extractor for constraint rules
            #if rule_template.isconstr:
            #    continue

            if rule_template.scoring_function.sctype == ScoringType.NNetRef:
                scref = rule_template.scoring_function.scref
                rule_template.feat_vector_sz = self.par.ruleset['rule'][scref].feat_vector_sz
            elif rule_template.scoring_function.sctype == ScoringType.NNet:
                if rule_template.head.isobs or rule_template.split_on is not None:
                    #print("MC")
                    # multiclass
                    train_groundings = \
                            self._get_multiclass_train(db, filters, ruleidx,
                                    rule_template, split_class=None)
                    #print(len(train_groundings))
                else:
                    # binary
                    neg_train_groundings, pos_train_groundings = \
                            self._get_binary_train(db, filters, ruleidx,
                                    rule_template, split_class=None)
                    train_groundings = neg_train_groundings + pos_train_groundings

                self.logger.info("Train groundings {}".format(len(train_groundings)))
                self.logger.info(rule_template.feat_functions)
                #feats = self.fe.extract(train_groundings[0], rule_template.feat_functions)
                #rule_template.feat_vector_sz = len(feats['vector'])
                #rule_template.feat_inputs_sz = [len(inp) for inp in feats['input']]
            # in the scoring function case I don't care about feature vector size
        '''

    def _build_model(self, rule_template, ruleidx, configs,
                     nn_index, nns_ls, nns_open_lhs, nns_lambda_ls,
                     shared_params, netmodules_path, mod=None):
        output_dim = rule_template.head_cardinality()
        config = configs["models"][ruleidx]
        use_gpu = config['use_gpu']

        # If no custom net is defined, load one of the presets from drail.neuro
        try:
            if netmodules_path is None and mod is None:
                module = "drail.neuro.{0}".format(config['module'])
                mod = import_module(module)
            elif mod is None:
                module_path = os.path.join(netmodules_path, "{0}.py".format(config['module']))
                self.logger.info(module_path)
                mod = imp.load_source(config['module'], module_path)

            neuro_class = getattr(mod, config['model'])
        except Exception as e:
            import pdb; pdb.set_trace()
            self.logger.error("Couldn't load neural net module")
            raise e
        nn = neuro_class(config, nn_index, use_gpu, output_dim)
        if use_gpu:
            nn.cuda()


        nn.build_architecture(rule_template, self.fe, shared_params)
        if use_gpu:
            nn.cuda()
        nns_ls.append(nn)

        nns_open_lhs.append(rule_template.has_open_lhs())
        nns_lambda_ls.append(rule_template.lambda_)

    def _create_shared_params(self, config):
        shared_params = {}
        if "shared_params" in config:
            for shared in config["shared_params"]:
                shared_params[shared] = {}

                if config["shared_params"][shared]['type'] == 'layer':
                    shared_params[shared]['layer'] = \
                            nn_utils.create_layer(config["shared_params"][shared])
                    shared_params[shared]['nin'] = \
                            config["shared_params"][shared]["nin"]
                    shared_params[shared]['nout'] = \
                            config["shared_params"][shared]["nout"]
                elif config["shared_params"][shared]['type'] == 'lstm':
                    shared_params[shared]['lstm'] = \
                            nn_utils.create_lstm(config["shared_params"][shared])
                    shared_params[shared]['nin'] = \
                            config["shared_params"][shared]["nin"]
                    shared_params[shared]['nout'] = \
                            config["shared_params"][shared]["nout"]
                elif config["shared_params"][shared]['type'] == 'bert':
                    if "bert_model_type" not in config["shared_params"][shared]:
                        shared_params[shared]['bert'] = \
                                nn_utils.create_bert()
                    else:
                        shared_params[shared]['bert'] = \
                                nn_utils.create_bert(bert_model=config["shared_params"][shared]['bert_model_type'])
                elif config["shared_params"][shared]['type'] == 'elmo':
                    shared_params[shared]['elmo'] = nn_utils.create_elmo(
                            options_file=config["shared_params"][shared]["options_file"],
                            weight_file=config["shared_params"][shared]["weight_file"])


        return shared_params

    def _add_neural_model(self, ruleidx, rule_template, neuro_class, config,
                         nns_ls, nns_open_lhs, nns_lambda_ls,
                         shared_params):
        if not rule_template.head.isobs:
            self.binary_classifiers.add(rule_template.head.name)
        rule_template.scoring_function.scref = ruleidx
        output_dim = rule_template.head_cardinality()
        use_gpu = config["use_gpu"]
        nn = neuro_class(config, ruleidx, use_gpu, output_dim)
        if use_gpu:
            nn.cuda()
        nn_build_architecture(rule_template, self.fe, shared_params)
        nns_ls.append(nn)
        nns_open_lhs.append(rule_template.has_open_lhs())
        nns_lambda_ls.append(rule_template.lambda_)

    def _add_models(self, neural_models, configs, nns_ls, nns_open_lhs,
                    nns_lambda_ls):
        nn_index = 0

        multi_seen = set()
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            template_nnets = []
            if not rule_template.head.isobs and not rule_template.head.name in multi_seen:
                self.binary_classifiers.add(rule_template.head.name)
            else:
                multi_seen.add(rule_template.head.name)

            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            self._add_neural_model(ruleidx, rule_template,
                                   neural_models[nn_index], configs[nn_index],
                                   nns_ls, nns_open_lhs, nns_lambda_ls)
            template_nnets[ruleidx] = nn_index
            self.nnindex_ls[ruleidx] = template_nnets

    def _build_models(self, db, config_path, nns_ls, nns_open_lhs,
                      nns_lambda_ls, netmodules_path=None,
                      module=None, isdic=False):
        if not isdic:
            with open(config_path, 'r') as f:
                self.configs = json.load(f)
        else:
            self.configs = config_path

        if self.savedir is None:
            self.set_savedir()

        shared_params = self._create_shared_params(self.configs)

        nn_index = 0
        multi_seen = set()
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            self.logger.info(rule_template)
            if not rule_template.head.isobs and not rule_template.head.name in multi_seen:
                self.binary_classifiers.add(rule_template.head.name)
            else:
                multi_seen.add(rule_template.head.name)

            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            rule_template.scoring_function.scref = ruleidx

            # obtain classifier split if it exists
            if rule_template.split_on is not None:
                classes = db.get_distinct_values(rule_template.head, rule_template.split_on)
                rule_template.split_on_classes = [class_[0] for class_ in classes]

            template_nnets = []
            if len(rule_template.split_on_classes) > 0:
                for class_ in rule_template.split_on_classes:
                    self.logger.info("Building nnet for predicate {0} class {1}".format(
                            rule_template.head.name, class_))
                    self._build_model(rule_template, ruleidx, self.configs, nn_index, nn_ls,
                                      nns_open_lhs, nns_lambda_ls, shared_params, netmodules_path,
                                      module)
                    template_nnets.append(nn_index)
                    nn_index += 1
                    self.test_metrics.add_classifier(rule_template.head.name)

                    if rule_template.split_on_ttype == LabelType.Binary:
                        self.binary_classifiers.add(rule_template.head.name)
            else:
                self.logger.info("Building nnet for predicate {0}".format(
                        rule_template.head.name))

                nn = self._build_model(rule_template, ruleidx, self.configs, nn_index, nns_ls,
                                       nns_open_lhs, nns_lambda_ls, shared_params, netmodules_path,
                                       module)

                template_nnets.append(nn_index)
                nn_index += 1
                self.test_metrics.add_classifier(rule_template.head.name)

                if not rule_template.head.isobs:
                    self.binary_classifiers.add(rule_template.head.name)

            self.nnindex_ls[ruleidx] = template_nnets

    def _get_groundings(self, db, filters, ruleidx, rule_template,
                       inst, class_split, gold_heads, mode):
        if self.par.dbmodule is None:
            gold_heads_set = set([RuleGrounding.pred_str(head) for head in gold_heads])
            rule_groundings = db.unfold_rule_groundings(
                    rule_template, ruleidx, self.par.groupby[mode],
                    inst, class_split, gold_heads_set, filter_by=filters, ground_negs=True,
                    latent_preds=self.latent_predicates)
        else:
            rule_groundings = \
                getattr(db, rule_template.dbfunc)(
                        istrain=False,
                        isneg=False,
                        filters=filters,
                        split_class=class_split,
                        instance_id=inst)
        return rule_groundings

    def _get_instance_groundings(self, db, ruleidx, rule_template,
                                 filters, class_split, gold_heads, inst, mode):
        dic = OrderedDict()
        rule_groundings = self._get_groundings(
            db, filters, ruleidx, rule_template, inst, class_split, gold_heads, mode)
        '''
        for rg in rule_groundings:
            print rg
        '''
        for rg in rule_groundings:
            # create a unique repr for the body
            body_str = ' & '.join([RuleGrounding.pred_str(predicate) for predicate
                        in rg.body])

            # extract heads in left hand side
            unobs = set([RuleGrounding.pred_str(predicate) for predicate in rg.body if
                        not predicate['obs']])

            # extract current head and indexes
            head = RuleGrounding.pred_str(rg.head)

            if not rg.is_binary_head:
                head_index = self.fe.extract_multiclass_head(rg)
            else:
                if rg.head['isneg']:
                    head_index = 0
                else:
                    head_index = 1

            # if we havent seen this body before
            if body_str not in dic:
                # extract features (only once per body)
                rule_grd_x = self.fe.extract(rg, rule_template.feat_functions)
                lmbd = 1
                dic[body_str] = \
                    {'groundings': [rg], 'feat_repr': rule_grd_x,
                      'heads': [head], 'heads_index': [head_index],
                      'unobs': unobs, 'lambda': lmbd}
            else:
                # append info if we have seen this body before and this is a brand new grounding
                if rg not in dic[body_str]['groundings']:
                    dic[body_str]['groundings'].append(rg)
                    dic[body_str]['heads_index'].append(head_index)
                    dic[body_str]['heads'].append(head)

        return dic

    def extract_rules_instance(self, db, filters, inst, mode, istrain=False):
        gold_heads = db.get_gold_predicates(self.par.ruleset['rule'], filters,
                                            self.par.groupby[mode], inst,
                                            self.latent_predicates)
        instance_groundings_inst = []
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            #print(ruleidx)
            instance_groundings_inst.append([])

            # do not extract groundings of rules that aren't marked for inference
            if not rule_template.inference and not istrain:
                continue
            #print
            #print "\t", ruleidx, rule_template
            # if associated to a neural network, take care of subnet splits
            scref = rule_template.scoring_function.scref
            if scref is not None:
                for index, nnidx in enumerate(self.nnindex_ls[scref]):
                    class_split = None
                    if len(rule_template.split_on_classes) > 0:
                        class_split = rule_template.split_on_classes[index]

                    dic = self._get_instance_groundings(
                        db, ruleidx, rule_template, filters, class_split,
                        gold_heads, inst, mode)
                    instance_groundings_inst[ruleidx].append(dic)
                    #print "\t\t", len(dic)
            else:
                class_split = None
                dic = self._get_instance_groundings(
                    db, ruleidx, rule_template, filters, class_split,
                    gold_heads, inst, mode)
                instance_groundings_inst[ruleidx].append(dic)
        return gold_heads, instance_groundings_inst

    def extract_rules(self, db, filters, mode, istrain=False):
        self.logger.info("Extracting rules...")
        instances = db.get_ruleset_instances(self.par.ruleset['rule'], self.par.groupby[mode],
                filters)
        #instances = set(list(instances)[:10])
        num = len(instances)

        instance_groundings = {}; gold_heads_ret = {}
        # extraction of data

        pbar = tqdm(total=len(instances))
        for i, inst in enumerate(instances):
            #print i, inst
            gold_heads_inst, instance_groundings_inst = \
                    self.extract_rules_instance(db, filters, inst, mode, istrain=istrain)

            gold_heads_ret[inst] = gold_heads_inst
            instance_groundings[inst] = instance_groundings_inst
            pbar.update(1)
        pbar.close()
        self.logger.info("Done")
        return instances, instance_groundings, gold_heads_ret

    def get_tree_info(self):
        tree_info = {}

        if 'treeconstr' in self.par.ruleset:
            for constidx, constr_template in enumerate(self.par.ruleset['treeconstr']):
                tree_info[constr_template.head.name] =\
                   {'variables': [var.arg for var in constr_template.head.variables],
                    'root': constr_template.root,
                    'notRoot': constr_template.not_root,
                    'leaf': constr_template.leaf}
        return tree_info

    def extract_constraints_instance(self, db, filters, inst, mode):
        constraint_heads_ret_inst = set([])
        constraint_groundings_inst = []
        tree_constraint_groundings_inst = {}
        arithmetic_constraint_groundings_inst = {}
        # extract constriints for the instance
        if 'constr' in self.par.ruleset:
            for constidx, constr_template in enumerate(self.par.ruleset['constr']):
                start_time = time.time()
                #print "\t", constidx, constr_template
                if self.par.dbmodule is None:
                    cg = db.unfold_rule_groundings(
                            constr_template, constidx, self.par.groupby[mode], inst,
                            None, set([]), isconstr=True, filter_by=filters)
                else:
                    cg = getattr(db, constr_template.dbfunc)(filters, inst)
                #for gr in cg:
                    #print "\t\t", gr
                #print "\t\t", len(cg)
                constraint_groundings_inst += cg
                # add enforced head constraints to gold dataset
                n_unobs = sum(1 for pred in constr_template.body if not pred.isobs)
                if n_unobs == 0:
                    for cg in constraint_groundings_inst:
                        if (cg.head['ttype'] != 1):
                            constraint_heads_ret_inst.add(RuleGrounding.pred_str(cg.head))
                #print "\t", constidx, constr_template, "time {:.3f}s".format(time.time() - start_time)
        if 'treeconstr' in self.par.ruleset:
            for constidx, constr_template in enumerate(self.par.ruleset['treeconstr']):
                #print "\t", constidx, constr_template
                if self.par.dbmodule is None:
                    cg = db.unfold_rule_groundings(
                            constr_template, constidx, self.par.groupby[mode], inst,
                            None, set([]), isconstr=True, filter_by=filters)
                else:
                    cg = getattr(db, constr_template.dbfunc)(filters, inst)

                if constr_template.head.name not in tree_constraint_groundings_inst:
                    tree_constraint_groundings_inst[constr_template.head.name] = []
                tree_constraint_groundings_inst[constr_template.head.name] += [c.head for c in cg]

        if 'arithmetic' in self.par.ruleset:
            for constidx, constr_template in enumerate(self.par.ruleset['arithmetic']):
                if self.par.dbmodule is None and constr_template.body is not None:
                    cg = db.unfold_rule_groundings(
                            constr_template, constidx, self.par.groupby[mode], inst,
                            None, set([]), isconstr=True, filter_by=filters)
                elif constr_template.body is not None:
                    cg = getattr(db, constr_template.dbfunc)(filters, inst)
                else:
                    cg = []

                # TO-DO: Make sure that we will only have one per head-name (likely not)
                # If not, adapt accordingly (now support only for pred+)
                arithmetic_constraint_groundings_inst[str(constr_template.head.name)] =\
                    {'heads': set([RuleGrounding.pred_str(x.head) for x in cg]), 'comparison': constr_template.comparison,\
                     'number': constr_template.number, 'repr': str(constr_template), 
                     'headVars': []}

        return constraint_groundings_inst, constraint_heads_ret_inst,\
               tree_constraint_groundings_inst, arithmetic_constraint_groundings_inst

    def extract_constraints(self, db, filters, mode):
        # obtain inference instances
        self.logger.info("Extracting constraints...")
        instances = db.get_ruleset_instances(self.par.ruleset['rule'], self.par.groupby[mode])
        num = len(instances)
        constraint_groundings = {}; constraint_heads_ret = {}
        tree_constraint_groundings = {}; arithmetic_constraint_groundings = {}

        pbar = tqdm(total=len(instances))
        for i, inst in enumerate(instances):
            #print i, inst
            constraint_groundings_inst, constraint_heads_ret_inst, tree_constraint_groundings_inst,\
            arithmetic_constraint_groundings_inst =\
                    self.extract_constraints_instance(db, filters, inst, mode)
            constraint_groundings[inst] = constraint_groundings_inst
            constraint_heads_ret[inst] = constraint_heads_ret_inst
            tree_constraint_groundings[inst] = tree_constraint_groundings_inst
            arithmetic_constraint_groundings[inst] = arithmetic_constraint_groundings_inst
            pbar.update(1)
        pbar.close()
        return constraint_groundings, constraint_heads_ret, tree_constraint_groundings, arithmetic_constraint_groundings

    def get_instance_data(self, db, filters, fold, inst, mode, get_constr_heads=True, istrain=False):
        if fold in self.instance_groundings and inst in self.instance_groundings[fold]:
            instance_groundings_inst = self.instance_groundings[fold][inst]
            gold_heads_inst = self.gold_heads[fold][inst]

            # By default there might be no constraints
            tree_constraint_groundings_inst = {}
            constraint_groundings_inst = {}
            arithmetic_constraint_groundings_inst = {}
            if fold in self.tree_constraint_groundings and inst in self.tree_constraint_groundings[fold]:
                tree_constraint_groundings_inst = self.tree_constraint_groundings[fold][inst]
            if fold in self.constraint_groundings and inst in self.constraint_groundings[fold]:
                constraint_groundings_inst = self.constraint_groundings[fold][inst]
            if fold in self.arithmetic_constraint_groundings and inst in self.arithmetic_constraint_groundings[fold]:
                arithmetic_constraint_groundings_inst = self.arithmetic_constraint_groundings[fold][inst]
            if get_constr_heads and fold in self.constraint_heads and inst in self.constraint_heads[fold]:
                constraint_heads_ret_inst = self.constraint_heads[fold][inst]
            else:
                constraint_heads_ret_inst = None
        else:
            gold_heads_inst, instance_groundings_inst = self.extract_rules_instance(db, filters, inst, mode, istrain=istrain)
            constraint_groundings_inst, constraint_heads_ret_inst, tree_constraint_groundings_inst, \
            arithmetic_constraint_groundings_inst = \
                    self.extract_constraints_instance(db, filters, inst, mode)

            if not get_constr_heads:
                constraint_heads_ret_inst = None

        return instance_groundings_inst, gold_heads_inst,\
               constraint_groundings_inst, constraint_heads_ret_inst,\
               tree_constraint_groundings_inst, arithmetic_constraint_groundings_inst


    def _add_features(self, feats, X):
        X['vector'].append(feats['vector'])
        X['input'].append(feats['input'])
        if 'nonuminput' in feats:
            X['nonuminput'].append(feats['nonuminput'])
        for emb in feats['embedding']:
            if emb not in X['embedding']:
                X['embedding'][emb] = []
            X['embedding'][emb].append(feats['embedding'][emb])
        return X

    def _extract_features(self, ruleidx, rule_template, observed_groundings, Y=[]):
        X = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []}
        head_ids = []
        for rg in observed_groundings:
            feats = self.fe.extract(rg, rule_template.feat_functions)
            X = self._add_features(feats, X)

            if rule_template.head.label_type() == LabelType.Multiclass:
                head_index = self.fe.extract_multiclass_head(rg)
                Y.append(head_index)
            elif rule_template.head.label_type() == LabelType.Multilabel:
                labels = self.fe.extract_multilabel_head(rg)
                Y.append(labels)
            head_args = ','.join([str(a) for a in rg.head['arguments'][:-1]])
            head_ids.append(head_args)
        return X, Y, head_ids

    def _observed_data(self, db, rule_template, ruleidx,
            filters, split_class, balance_data, limit=-1, debug=False):
        #print(debug)
        Y = []
        if rule_template.head.isobs:
            # multiclass
            observed_groundings = self._get_multiclass_train(
                    db, filters, ruleidx, rule_template, split_class)
            if limit > 0:
                random.shuffle(observed_groundings)
                observed_groundings = observed_groundings[:limit]

        else:
            # binary
            neg_obs_groundings, pos_obs_groundings = \
                    self._get_binary_train(db, filters, ruleidx,
                        rule_template, split_class)
            #for gr in pos_obs_groundings:
            #    print gr
            #print("====")
            #for gr in neg_obs_groundings:
            #    print gr

            if balance_data:
                limit = min(len(pos_obs_groundings), len(neg_obs_groundings))
                if limit > 0 and len(neg_obs_groundings) > limit:
                    random.shuffle(neg_obs_groundings)
                    neg_obs_groundings = neg_obs_groundings[:limit]
                if limit > 0 and len(pos_obs_groundings) > limit:
                    random.shuffle(pos_obs_groundings)
                    pos_obs_groundings = pos_obs_groundings[:limit]

            observed_groundings = neg_obs_groundings + pos_obs_groundings
            Y = [0] * len(neg_obs_groundings) + [1] * len(pos_obs_groundings)

        X, Y, head_ids = self._extract_features(ruleidx, rule_template, observed_groundings, Y)
        return X, Y, head_ids

    def _scale_features(self, index, X, scale_role):
        # Do vector feats for now, add input later if necessary
        X['vector'] = np.array(X['vector'])
        X['input'] = np.array(X['input'])

        if scale_role == 'train':

            if X['vector'].shape[1] > 0:
                if 'vector' not in self.scalers_ls:
                    self.scalers_ls['vector'] = {}
                self.scalers_ls['vector'][index] = preprocessing.MinMaxScaler()
                X['vector'] = self.scalers_ls['vector'][index].fit_transform(X['vector'])

            if X['input'].shape[1] > 0:
                if 'input' not in self.scalers_ls:
                    self.scalers_ls['input'] = {}
                self.scalers_ls['input'][index] = {}

                inputs = []
                for i in range(X['input'].shape[1]):
                    self.scalers_ls['input'][index][i] = preprocessing.MinMaxScaler()
                    X_scale = X['input'][:,i,:]
                    X_scale = self.scalers_ls['input'][index][i].fit_transform(X_scale)
                    inputs.append(X_scale)

                X['input'] = np.stack(inputs, axis=1)
        else:
            if X['vector'].shape[1] > 0:
                X['vector'] = self.scalers_ls['vector'][index].transform(X['vector'])

            if X['input'].shape[1] > 0:
                inputs = []
                for i in range(X['input'].shape[1]):
                    X_scale = X['input'][:,i,:]
                    X_scale = self.scalers_ls['input'][index][i].transform(X_scale)
                    inputs.append(X_scale)
                X['input'] = np.stack(inputs, axis=1)

    def get_observed_data(self, db, fold_filters, scale_feats=False, scale_role='train',
                          balance_data=False, debug=False):
        #print(debug)
        observed= {};
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            #print ruleidx, rule_template

            # if no training will be done we don't need to get observed data
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            observed[ruleidx] = []
            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                #print "nnet", index, nnidx
                if len(rule_template.split_on_classes) == 0:
                    class_split = None
                else:
                    class_split = rule_template.split_on_classes[index]

                X, Y, head_ids = self._observed_data(db, rule_template, ruleidx,
                                          fold_filters, class_split, balance_data, debug=debug)
                if scale_feats:
                    #print "scaling feats", nnidx
                    self._scale_features(nnidx, X, scale_role)

                observed[ruleidx].append((X, Y, head_ids))

        if scale_feats and scale_role == 'train':
            scalerdir = os.path.join(self.savedir, "scaler.joblib")
            joblib.dump(self.scalers_ls, scalerdir)

        return observed

    def extract_observed_data(self, db, train_filters=[], dev_filters=[], test_filters=[], scale_data=False, balance_data=False):
        # extract training data
        self.logger.info("extract train...")
        observed_train = self.get_observed_data(
                db, train_filters, scale_data, scale_role='train', balance_data=balance_data)
        self.logger.info("extract dev...")
        observed_dev = self.get_observed_data(
                db, dev_filters, scale_data, scale_role='test', balance_data=balance_data)
        self.logger.info("extract test...")
        observed_test = self.get_observed_data(
                db,  test_filters, scale_data, scale_role='test', balance_data=balance_data)
        return observed_train, observed_dev, observed_test

    def extract_instances(self, db, test_filters=[], dev_filters=[], train_filters=[],
                          extract_test=True, extract_dev=False, extract_train=False):
        self.instances = {}
        if extract_test:
            self.instances['test'] = db.get_ruleset_instances(self.par.ruleset['rule'], self.par.groupby['test'], test_filters)
        if extract_dev:
            self.instances['dev'] = db.get_ruleset_instances(self.par.ruleset['rule'], self.par.groupby['test'], dev_filters)
        if extract_train:
            self.instances['train'] = db.get_ruleset_instances(self.par.ruleset['rule'], self.par.groupby['train'], train_filters)
        # get tree info
        #print(self.par.groupby)
        #print(self.instances)
        #exit()
        self.tree_info = self.get_tree_info()

    def extract_data(self, db, test_filters=[], dev_filters=[], train_filters=[],
                     extract_test=True, extract_dev=False, extract_train=False,
                     pickledir=None, from_pickle=False, pickleit=False, extract_constraints=True):
        # extract data once
        self.constraint_groundings = {}
        self.tree_constraint_groundings = {}
        self.arithmetic_constraint_groundings = {}
        self.constraint_heads = {}

        # get tree info
        self.tree_info = self.get_tree_info()

        if not from_pickle:
            self.instances = {}; self.gold_heads = {}
            self.instance_groundings = {};
            if extract_test:
                self.logger.info("Extracting test rules...")
                start_time = time.time()
                self.instances['test'], self.instance_groundings['test'],\
                self.gold_heads['test'] = \
                        self.extract_rules(db, test_filters, mode='test', istrain=False)
                self.constraint_groundings['test'], _, self.tree_constraint_groundings['test'],\
                self.arithmetic_constraint_groundings['test'] = \
                        self.extract_constraints(db, test_filters, mode='test')
                self.logger.info("time {:.3f}s".format(time.time() - start_time))

            if extract_dev:
                self.logger.info("Extracting dev rules...")
                start_time = time.time()
                self.instances['dev'], self.instance_groundings['dev'],\
                self.gold_heads['dev'] = \
                        self.extract_rules(db, dev_filters, mode='test', istrain=False)
                self.constraint_groundings['dev'], self.constraint_heads['dev'], self.tree_constraint_groundings['dev'],\
                self.arithmetic_constraint_groundings['dev'] = \
                        self.extract_constraints(db, dev_filters, mode='test')
                self.logger.info("time {:.3f}s".format(time.time() - start_time))

            if extract_train:
                self.logger.info("Extracting train rules...")
                start_time = time.time()
                self.instances['train'], self.instance_groundings['train'],\
                self.gold_heads['train'] = \
                        self.extract_rules(db, train_filters, mode='train', istrain=True)
                self.constraint_groundings['train'], self.constraint_heads['train'], self.tree_constraint_groundings['train'],\
                self.arithmetic_constraint_groundings['train'] = \
                        self.extract_constraints(db, train_filters, mode='train')
                self.logger.info("time {:.3f}s".format(time.time() - start_time))

        elif pickledir is not None:
            self.logger.info("Loading data from pkl...")
            start_time = time.time()
            self.instances = pickle.load(open(os.path.join(pickledir, "instances.pkl"), "rb"))
            self.instance_groundings = pickle.load(open(os.path.join(pickledir, "instance_groundings.pkl"), "rb"))
            self.gold_heads = pickle.load(open(os.path.join(pickledir, "gold_heads.pkl"), "rb"))

            if extract_constraints:
                self.constraint_groundings = pickle.load(open(os.path.join(pickledir, "constraint_groundings.pkl"), "rb"))
                self.tree_constraint_groundings = pickle.load(open(os.path.join(pickledir, "tree_constraint_groundings.pkl"), "rb"))
                self.constraint_heads = pickle.load(open(os.path.join(pickledir, "constraint_heads.pkl"), "rb"))
                self.arithmetic_constraint_groundings = pickle.load(open(os.path.join(pickledir, "arithmetic_constraint_groundings.pkl"), "rb"))
            self.logger.info("time {:.3f}s".format(time.time() - start_time))
        else:
            self.logger.info("Need to specify a pickledir")
            exit(-1)

        if pickleit and pickledir is not None:
            self.logger.info("Dumping data into pkl...")
            start_time = time.time()
            with open(os.path.join(pickledir, "instances.pkl"), "wb") as fp:
                pickle.dump(self.instances, fp)

            with open(os.path.join(pickledir, "instance_groundings.pkl"), "wb") as fp:
                pickle.dump(self.instance_groundings, fp)

            with open(os.path.join(pickledir, "constraint_groundings.pkl"), "wb") as fp:
                pickle.dump(self.constraint_groundings, fp)

            with open(os.path.join(pickledir, "tree_constraint_groundings.pkl"), "wb") as fp:
                pickle.dump(self.tree_constraint_groundings, fp)

            with open(os.path.join(pickledir, "arithmetic_constraint_groundings.pkl"), "wb") as fp:
                pickle.dump(self.arithmetic_constraint_groundings, fp)

            with open(os.path.join(pickledir, "gold_heads.pkl"), "wb") as fp:
                pickle.dump(self.gold_heads, fp)

            with open(os.path.join(pickledir, "constraint_heads.pkl"), "wb") as fp:
                pickle.dump(self.constraint_heads, fp)
            self.logger.info("time {:.3f}s".format(time.time() - start_time))
        elif pickleit and pickledir is None:
            self.logger.error("Need to specify a pickledir")
            exit(-1)

    # Prediction stuff that I am moving from local_learner
    def _load_weights(self, instance_groundings, y_pred, pivots, inference_weights, lmbd):
        # instantiate weights
        for body, scores, pivot in zip(instance_groundings, y_pred, pivots):
            for gr_i, gr in enumerate(instance_groundings[body]['groundings']):
                if type(scores) is np.ndarray:
                    if gr.is_binary_head and not gr.head['isneg']:
                        idx_in_nn_out = 1
                    elif gr.is_binary_head and gr.head['isneg']:
                        idx_in_nn_out = 0
                    else:
                        idx_in_nn_out = instance_groundings[body]['heads_index'][gr_i]
                    #weight = 0.0
                    #if idx_in_nn_out < len(scores):
                    #print(gr, idx_in_nn_out, type(idx_in_nn_out), scores)
                    weight = scores[idx_in_nn_out]
                elif gr.is_binary_head and not gr.head['isneg']:
                    weight = scores
                elif gr.is_binary_head and gr.head['isneg']:
                    weight = 1 - scores
                else:
                    print("You can't return a single logit if predicate is non-binary")
                    exit(-1)

                # if weight not in the top K solutions, do not add it to the problem
                if weight >= pivot:
                    inference_weights[gr] = weight * lmbd

    def _score_neural(self, scref, instance_groundings, inference_weights, K, lambda_, scale_data, probas=False):
        for index, nnidx in enumerate(self.nnindex_ls[scref]):
            nn = self.nns_ls[nnidx]
            x = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []};
            y_pred = []

            if len(instance_groundings[index].keys()) > 0:

                # predict with neural nets
                for body in instance_groundings[index]:
                    x = self._add_features(
                            instance_groundings[index][body]['feat_repr'],
                            x)

                if scale_data:
                    self._scale_features(nnidx, x, scale_role='test')
                y_pred = nn_utils.predict_local_scores(nn, x, probas=probas)

                if len(y_pred.shape) > 1:
                    partial_order = np.partition(y_pred, y_pred.shape[1] - K)
                    pivots = partial_order[:,y_pred.shape[1] - K]
                else:
                    pivots = y_pred

                self._load_weights(instance_groundings[index], y_pred, pivots,
                                   inference_weights, lambda_)

    def _score_manual(self, scoring_func, instance_groundings,
                      inference_weights, K, lambda_):
        x = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []};
        y_pred = []
        index = 0
        if len(instance_groundings[index].keys()) > 0:

            # predict with neural nets
            for body in instance_groundings[index]:
                x = self._add_features(
                        instance_groundings[index][body]['feat_repr'],
                        x)
            #print(self.sc)
            y_pred = self.sc.apply(function_name=scoring_func, features=x, fe_class=self.fe)
            #y_pred = getattr(scmodule, scoring_func)(x, self.fe)
            #print(y_pred, y_pred.shape[1], K)
            partial_order = np.partition(y_pred, y_pred.shape[1] - K)
            pivots = partial_order[:,y_pred.shape[1] - K]

            self._load_weights(instance_groundings[index], y_pred, pivots,
                               inference_weights, lambda_)

    # TO-DO: set a K per predicate, can't be a single one
    def _instance_scores(self, instance_groundings_inst, K=None, scmodule_path=None, lmb_weights=None, scale_data=False, probas=False):
        # load module if available
        '''
        Removing this, needs to be moved to load_scoring_class function
        if scmodule_path is not None:
            module_path = os.path.join(scmodule_path, "{0}.py".format(self.par.scmodule))
            scmodule = imp.load_source(self.par.scmodule, module_path)
        '''

        inference_weights = OrderedDict()
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if not rule_template.inference:
                continue

            if lmb_weights is not None:
                lmbd_weight = lmb_weights[ruleidx]
            else:
                lmbd_weight = 1.0


            #print "ruleidx", ruleidx, "lmbd_weight", lmbd_weight

            if K is None:
                K_curr = rule_template.head_cardinality()
            else:
                K_curr = K

            scref = rule_template.scoring_function.scref

            if scref is not None:
                self._score_neural(scref, instance_groundings_inst[ruleidx],
                                   inference_weights, K_curr, lmbd_weight * rule_template.lambda_, scale_data,
                                   probas=probas)
            else:
                # TO-DO: retrieve scoring function from script and execute
                self._score_manual(rule_template.scoring_function.scfunc,
                                   instance_groundings_inst[ruleidx],
                                   inference_weights, K_curr, lmbd_weight * rule_template.lambda_)

        return inference_weights

    def drop_scores(self, db, fold_filters, fold='test', K=None, output="scores.csv", scmodule_path=None, lmbd_weights=None, scale_data=False, heads=None, output_probas=False):
        self.logger.info("droping scores...")
        num = len(self.instances[fold])

        with open(output, 'w') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            for i, inst in enumerate(self.instances[fold]):
                instance_groundings_inst, gold_heads_inst,\
                constraint_groundings_inst, constraint_heads_inst,\
                tree_constraint_groundings_inst =\
                    self.get_instance_data(db, fold_filters, fold, inst, mode='test')

                inference_weights = \
                    self._instance_scores(instance_groundings_inst, K, scmodule_path, lmbd_weights, scale_data, probas=output_probas)
                for gr in inference_weights:
                    if heads:
                        curr_head = RuleGrounding.pred_str(gr.head)
                        unobs = set([curr_head] + gr.body_unobs_str)
                        if  len(unobs & heads) == len(unobs):
                            csvwriter.writerow([gr, inference_weights[gr], "1"])
                        else:
                            csvwriter.writerow([gr, inference_weights[gr], "0"])
                    else:
                        csvwriter.writerow([gr, inference_weights[gr]])
