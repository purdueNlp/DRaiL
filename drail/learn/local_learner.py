import os
import sys
import imp
import csv
import json
import time
import random
import logging
import traceback
from collections import OrderedDict
from random import shuffle
from importlib import import_module
import torch
from sklearn.metrics import *
from sklearn.preprocessing import scale
import numpy as np
import joblib
from tqdm import tqdm, trange

from .learner import Learner
from .metrics import Metrics
from ..neuro import nn_utils, nn_model
from ..inference.ilp_inferencer import *
from ..inference.ad3_inferencer import *
from ..inference.viterbi_inferencer import *
from ..model.rule import RuleGrounding
from ..model.label import LabelType
from ..model.scoring_function import ScoringType


class LocalLearner(Learner):

    def __init__(self, gpu=False, inference_limit=None,
                 infer_algorithm='ILP'):
        super(LocalLearner, self).__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.reset()
        self.inference_algorithm = infer_algorithm
        self.inference_limit = inference_limit
        if self.inference_limit:
            self.logger.info("Setting an inference time limit of {} seconds".format(self.inference_limit))

    def reset(self):
        super(LocalLearner, self).reset()
        self.observed_predicates = set([])
        self.train_metrics = Metrics()
        self.dev_metrics = Metrics()
        self.test_metrics = Metrics()

    def build_models(self, db, config_path, netmodules_path=None, module=None, isdic=False):
        self.nns_ls = []; self.nns_open_lhs = []; self.nns_lambda_ls = []
        self._build_models(db, config_path, self.nns_ls, self.nns_open_lhs, self.nns_lambda_ls,
                           netmodules_path, module, isdic)

    def add_models(self, neural_models, configs):
        self.nns_ls = []; self.nns_open_lhs = []; self.nns_lambda_ls = []
        self._add_models(neural_models, configs)

    def init_models(self, scale_data=False):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue

            #if ruleidx == 1:
            #    continue

            self.logger.info('Initializing models for rule #{} : {}'.format(ruleidx, rule_template))

            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                nn = self.nns_ls[nnidx]
                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))
                if os.path.isfile(modeldir):
                    self.logger.info("Initializing model from {}...".format(modeldir))
                    nn.load_state_dict(torch.load(modeldir))
                else:
                    self.logger.warning("File '{}' doesn't exist, using random initialization".format(modeldir))

        if scale_data:
            scalerdir = os.path.join(self.savedir, "scaler.joblib")
            self.logger.info("Loading scalers from %s" %(scalerdir))
            self.scalers_ls = joblib.load(scalerdir)

    def train(self, db, train_filters=[], dev_filters=[], test_filters=[], limit=-1, scale_data=False, optimizer='SGD',
            evaluator=None, tb_writer=None, tb_prefix='', balance_data=False, n_epochs=-1):
        perf = {}
        instance_map = {}

        # extract training data
        observed_train, observed_dev, observed_test = \
            self.extract_observed_data(db, train_filters, dev_filters, test_filters, scale_data, balance_data=balance_data)

        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            if rule_template.scoring_function.sctype != ScoringType.NNet:
                continue
            self.logger.info('\nTraining/Testing for rule #{} : {}'.format(ruleidx, rule_template))

            perf[rule_template]={}
            for index, nnidx in enumerate(self.nnindex_ls[ruleidx]):
                nn = self.nns_ls[nnidx]
                lambda_ = self.nns_lambda_ls[nnidx]

                X_train, Y_train, train_ids = observed_train[ruleidx][index]
                X_dev, Y_dev, dev_ids = observed_dev[ruleidx][index]
                X_test, Y_test, test_ids = observed_test[ruleidx][index]

                class_split = None
                if len(rule_template.split_on_classes) > 0:
                    class_split = rule_template.split_on_classes[index]


                self.logger.info("{} Y_train, {} Y_dev, {} Y_test".format(len(Y_train), len(Y_dev), len(Y_test)))

                modeldir = os.path.join(self.savedir, "model_{0}.pt".format(nnidx))

                perf[rule_template][class_split] = \
                        [nn_utils.train_local_on_epoches(nn, lambda_,
                            X_train, Y_train, train_ids,
                            X_dev, Y_dev, dev_ids,
                            X_test, Y_test, test_ids,
                            modeldir, optimizer, self.fe, rule_template,
                            evaluator=evaluator, tb_writer=tb_writer, tb_prefix=tb_prefix+'_nn{}'.format(nnidx), n_epochs=n_epochs),\
                         nn]
        return perf


    def _run_inference(self, tree_constraint_groundings_inst, constraint_groundings_inst,
                       arithmetic_constraint_groundings_inst,
                       gold_heads_inst, inference_weights, get_active_heads=False, debug=False):
        '''
        for const in constraint_groundings:
            print("const", const)
        '''

        # AD3 inference
        if self.inference_algorithm.lower() == "ad3":
            inferencer = AD3Inferencer(inference_weights, constraint_groundings_inst, tree_constraint_groundings_inst, self.tree_info, arithmetic_constraint_groundings_inst, exact=False)
        elif self.inference_algorithm.lower() == "ilp":
            inferencer = ILPInferencer(inference_weights, constraint_groundings_inst, tree_constraint_groundings_inst, self.tree_info, arithmetic_constraint_groundings_inst, inferenceLimit=self.inference_limit)
        elif self.inference_algorithm.lower() == "viterbi":
            self.logger.error("Viterbi not supported")
            exit(-1)
            #inferencer = ViterbiInferencer(inference_weights)
        else:
            self.logger.error("{} not supported".format(self.inference_algorithm))
            exit(-1)


        t0 = time.time()
        inferencer.encode(debug=debug)
        t1 = time.time()
        inferencer.optimize()
        t2 = time.time()

        '''
        seed_heads = [RuleGrounding.pred_str(cnstr.head) for cnstr in self.constraint_groundings[fold][inst]\
                      if sum(1 for pred in cnstr.body if not pred['obs']) == 0]
        seed_heads = set(seed_heads)
        '''

        metrics, active_heads = inferencer.evaluate(gold_heads_inst, binary_predicates=self.binary_classifiers,
                                                    get_active_heads=get_active_heads, latent_predicates=self.latent_predicates)

        #print(metrics)
        #exit()
        encoding_time = t1 - t0
        solving_time = t2 - t1
        return metrics, active_heads, encoding_time, solving_time


    def _init_lmbd_weights(self):
        weights = {}
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            weights[ruleidx] = random.uniform(0, 0.0025)
            #weights[ruleidx] = 1.0
        #weights[0] = 100000.0
        return weights

    def _rule_lambdas(self, rule_template, bgs, active_heads):
        lambdas = 0
        for b_index, body in enumerate(bgs):
            if len(bgs[body]['unobs'] & active_heads) == len(bgs[body]['unobs']):
                if not rule_template.head.isobs and \
                   bgs[body]['heads'][0] in active_heads:
                    lambdas += rule_template.lambda_
                else:
                    for head, head_index in zip(bgs[body]['heads'], bgs[body]['heads_index']):
                        if head in active_heads:
                           lambdas += rule_template.lambda_
        return lambdas

    def _create_lambdas(self, instance_groundings, active_heads):
        phi = {}
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            phi[ruleidx] = 0
            scref = rule_template.scoring_function.scref
            if scref is not None:
                for index, nnidx in enumerate(self.nnindex_ls[scref]):
                    bgs = instance_groundings[ruleidx][index]
                    phi[ruleidx] += self._rule_lambdas(rule_template, bgs, active_heads)
            else:
                bgs = instance_groundings[ruleidx][0]
                phi[ruleidx] += self._rule_lambdas(rule_template, bgs, active_heads)
        return phi

    def _update_lmbd_weights(self, weights, phi_prime, phi_hat, lr):
        for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
            weights[ruleidx] += lr * (phi_prime[ruleidx] - phi_hat[ruleidx])

    def _accuracy(self, metrics, predicates=[]):
        accuracy = {}
        y_gold_all = []; y_pred_all = []
        for pred in predicates:
            y_gold = metrics.metrics[pred]['gold_data']
            y_pred = metrics.metrics[pred]['pred_data']

            y_gold_all += y_gold
            y_pred_all += y_pred

            acc_test = accuracy_score(y_gold, y_pred)
            accuracy[pred] = acc_test
        acc_test = accuracy_score(y_gold_all, y_pred_all)
        accuracy['all'] = acc_test
        return accuracy

    # Train lambdas on specified fold
    def train_lambdas(self, iterations, lr, fold_filters, fold, K=None, scmodule_path=None,
                      predicates=[]):
        weights = self._init_lmbd_weights()

        for _iter in range(iterations):
            dev_metrics = Metrics()
            phi_prime = {}; phi_hat = {}
            for i, inst in enumerate(self.instances[fold]):
                instance_groundings_inst, gold_heads_inst,\
                constraint_groundings_inst, constraint_heads_inst,\
                tree_constraint_groundings_inst, arithmetic_constraint_groundings_inst =\
                    self.get_instance_data(db, fold_filters, fold, inst)

                inference_weights = \
                    self._instance_scores(instance_groundings_inst, K, scmodule_path, weights, scale_data=False, probas=False)

                _, active_heads, _, _ = \
                    self._run_inference(tree_constraint_groundings_inst, constraint_groundings_inst,
                                        arithmetic_constraint_groundings_inst,
                                        gold_heads_inst, inference_weights,
                                        get_active_heads=True)

                gold_heads = set([RuleGrounding.pred_str(h) for h in self.gold_heads[fold][inst]])

                # correct predictions
                res = self._create_lambdas(self.instance_groundings[fold][inst], active_heads & gold_heads)
                for ruleidx in res:
                    if ruleidx not in phi_prime:
                        phi_prime[ruleidx] = 0.0
                    phi_prime[ruleidx] += res[ruleidx]

                # incorrect
                res = self._create_lambdas(self.instance_groundings[fold][inst], active_heads - gold_heads)
                for ruleidx in res:
                    if ruleidx not in phi_hat:
                        phi_hat[ruleidx] = 0.0
                    phi_hat[ruleidx] += res[ruleidx]

            self._update_lmbd_weights(weights, phi_prime, phi_hat, lr)

            self.logger.info("weights {}".format(weights))
            for i, inst in enumerate(self.instances[fold]):
                inference_weights = \
                    self._instance_scores(instance_groundings_inst, K, scmodule_path, weights, scale_data=False, probas=False)
                metrics, _, _, _ = \
                    self._run_inference(tree_constraint_groundings_inst, constraint_groundings_inst,
                                        arithmetic_constraint_groundings_inst,
                                        gold_heads_inst, inference_weights)
                dev_metrics.load_metrics(metrics)

            self.logger.info(_iter, self._accuracy(dev_metrics, predicates))
            self.logger.info()
        return weights

    def predict_local_topK(self, rules, ridx, index, nnindex, K=10):
        inference_weights = OrderedDict()
        rule_template = self.par.ruleset['rule'][ridx]

        for rule in rules:
            nn = self.nns_ls[nnindex]
            x = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []};
            y_pred = []

            feats = self.fe.extract(rule, rule_template.feat_functions)
            x = self._add_features(feats, x)
            y_pred = nn_utils.predict_local_scores(nn, x)
            # WARNING: HARD CODED FOR BINARY
            weight = y_pred[0][1]
            inference_weights[rule] = weight
            self.logger.info(rule, weight)
        return inference_weights


    def predict(self, db, fold_filters, fold='test', K=None, scmodule_path=None, lmbd_weights=None, get_predicates=False, scale_data=False, debug_inference=False):
        self.logger.info("predicting...")
        num = len(self.instances[fold])
        test_metrics = Metrics()

        all_predictions = set([])
        pbar = tqdm(total=len(self.instances[fold]))
        for i, inst in enumerate(self.instances[fold]):

            instance_groundings_inst, gold_heads_inst,\
            constraint_groundings_inst, constraint_heads_inst,\
            tree_constraint_groundings_inst, arithmetic_constraint_groundings_inst =\
                self.get_instance_data(db, fold_filters, fold, inst, mode='test')

            #print
            #print i, inst
            inference_weights = \
                self._instance_scores(instance_groundings_inst, K, scmodule_path, lmbd_weights, scale_data, probas=False)
            #for gr in inference_weights:
            #    print(gr, inference_weights[gr])
            #exit()
            metrics, active_heads, enc_time, solv_time =\
                    self._run_inference(tree_constraint_groundings_inst, constraint_groundings_inst,
                                        arithmetic_constraint_groundings_inst,
                                        gold_heads_inst, inference_weights,
                                        get_active_heads=get_predicates,
                                        debug=debug_inference)
            #print(active_heads)
            #print(metrics)
            #exit()

            if active_heads:
                all_predictions |= active_heads
            test_metrics.load_metrics(metrics)
            test_metrics.metrics['encoding_time'] += enc_time
            test_metrics.metrics['solving_time'] += solv_time
            #exit()
            pbar.update(1)
        pbar.close()
        if get_predicates:
            return test_metrics, all_predictions
        else:
            return test_metrics, None

    def extract_rule_embeddings(self, db, fold='test', scale_data=False):
        self.logger.info("Extracting rule embeddings...")
        num = len(self.instances[fold])

        embeddings = {}

        pbar = tqdm(total=len(self.instances[fold]))
        for i, inst in enumerate(self.instances[fold]):
            for ruleidx, rule_template in enumerate(self.par.ruleset['rule']):
                scref = rule_template.scoring_function.scref
                if scref is None:
                    continue
                for index, nnidx in enumerate(self.nnindex_ls[scref]):
                    nn = self.nns_ls[nnidx]
                    x = {'vector': [], 'embedding': {}, 'input': [], 'nonuminput': []};
                    if len(self.instance_groundings[fold][inst][ruleidx][index].keys()) > 0:
                        for body in self.instance_groundings[fold][inst][ruleidx][index]:
                            x = self._add_features(
                                    self.instance_groundings[fold][inst][ruleidx][index][body]['feat_repr'],
                                    x)
                        if scale_data:
                            self._scale_features(nnidx, x, scale_role='test')
                        vectors = nn_utils.predict_local_embeddings(nn, x)
                        if len(vectors) > 0:
                            for i, body in enumerate(self.instance_groundings[fold][inst][ruleidx][index]):
                                embeddings[body] = vectors[i]
            pbar.update(1)
        pbar.close()
        return embeddings


    def get_test_rule_templates(self):
        return self.par.ruleset['rule']

    def reset_metrics(self):
        self.test_metrics = Metrics()
