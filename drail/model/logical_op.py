class LogicalOp(object):

    def __init__(self, arg0, arg1, operator):
        self.arg0 = arg0
        self.arg1 = arg1
        self.operator = operator

class LogicalOpType(object):
    Diff, Eq, Lt, Gt, Leq, Geq = range(1, 7)
