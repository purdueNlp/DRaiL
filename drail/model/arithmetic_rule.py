# TO-DO: This needs to be expanded to handle more stuff, like filter clauses
#        and sumations on arguments (look at PSL)

class ArithmeticRuleTemplate(object):

    def __init__(self, predicate, comparison, number, body, logical_ops):
        self.head = predicate
        self.comparison = comparison
        self.number = int(number)
        self.body = body
        self.logical_ops = logical_ops

    def __str__(self):
        ret = ""
        ret = "{0}+ {1} {2}".format(self.head, self.comparison, self.number)
        if len(self.body) > 0:
            ret += "{" + str(self.body[0])
            for i in range(0, len(self.body)):
                ret += " & " + str(self.body[i])
            ret += "}"

        return ret

    def __repr__(self):
        return str(self)
