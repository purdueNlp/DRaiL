import os
import itertools
import re
import logging
import sqlite3
import copy

from .model.rule import RuleGrounding
from .model.predicate import PredicateTemplate
from .model.argument import ArgumentType
from .model.argument import sqlite_type
from .model.logical_op import LogicalOp, LogicalOpType


class Database(object):

    def __init__(self):
        self.conn = sqlite3.connect(":memory:")
        self.cur = self.conn.cursor()
        self.table_index = {}
        self.table_index_types = {}
        self.predicate_entities = {}
        self.logger = logging.getLogger(self.__class__.__name__)
        self.cache = {}

    def close_database(self):
        self.conn.close()

    def drop_table(self, table_name):
        query = "DROP TABLE IF EXISTS {0}".format(table_name)
        self.cur.execute(query)

    def table_exists(self, table_name):
        query = "SELECT count(name) FROM sqlite_master WHERE type='table' AND name='{}'".format(table_name)
        self.cur.execute(query)
        if self.cur.fetchone()[0] == 1:
            return True
        else:
            return False

    def create_indexes(self, table_name, columns):
        for col in columns:
            query = "CREATE INDEX {0}_{1} ON {0} ({1})"
            query = query.format(table_name, col)
            #print query
            self.cur.execute(query)

    def create_table_predicate(self, table_name, arguments):
        self.table_index[table_name] = [a.name for a in arguments]
        self.table_index_types[table_name] = [a.typ for a in arguments]
        args = ", ".join([a.name + " " + a.sqlite_type() for a in arguments])
        indexes = [a.name for a in arguments if a.typ == ArgumentType.UniqueID or a.typ == ArgumentType.UniqueString]

        query = "CREATE TABLE {0} ({1})"
        query = query.format(table_name, args)
        self.logger.info(query)
        self.cur.execute(query)
        self.create_indexes(table_name, indexes)

    # WARNING: right now assuming all labels are categorical
    def create_table_label(self, table_name):
        self.table_index[table_name] = ["label"]
        query = "CREATE TABLE {0} (label TEXT)"
        query = query.format(table_name)
        # print query
        self.cur.execute(query)
        self.create_indexes(table_name, ["label"])

    def create_table_filter(self, table_name, args, arg_types, arg_data):
        self.table_index[table_name] = [a for a in args]
        self.table_index_types[table_name] = [t for t in arg_types]
        args_ = ", ".join([a + " " + sqlite_type(t) for (a,t) in zip(args, arg_types)])
        query = "CREATE TABLE {0} ({1})"
        query = query.format(table_name, args_)
        # print query
        self.cur.execute(query)

    def add_column(self, table_name, column_name, column_type, default):
        query = "ALTER TABLE {0} ADD COLUMN {1} {2} DEFAULT {3}"
        query = query.format(table_name, column_name, column_type, default)
        #print(query)
        self.cur.execute(query)

    def clear_column(self, table_name, column_name, default):
        query = "UPDATE {0} SET {1} = {2}"
        query = query.format(table_name, column_name, default)
        print(query)
        self.cur.execute(query)

    def insert_row(self, table_name, values):
        args = self.table_index[table_name]
        args = ", ".join([a for a in args])
        values = [v.replace('"', "'") for v in values]
        values = ", ".join(['"' + v + '"' for v in values])
        query = "INSERT INTO {0} ({1}) VALUES ({2})"
        query = query.format(table_name, args, values)
        #print(query)
        self.cur.execute(query)

    def load_data(self, path, parser):
        self.load_predicates(parser.predicate_arguments,
                             path, parser.files)
        self.load_predicates(parser.entity_arguments,
                             path, parser.files)
        self.predicate_entities = parser.predicate_entities


    def load_predicates(self, predicate_arguments, path, files):
        # create all.name tables
        for pred in predicate_arguments:
            arguments = predicate_arguments[pred]

            if pred in files:

                if self.table_exists(pred):
                    self.logger.warning("Predicate '{}' already exists, skipping it. If you want to update it, delete it first".format(pred))
                else:
                    fullpath = os.path.join(path, files[pred])
                    with open(fullpath, encoding='utf-8') as f:
                        name = files[pred].split(".")[0]
                        self.create_table_predicate(pred, arguments)

                        content = f.readlines()
                        for line in content:
                            values = re.split("\s+", line.strip())
                            self.insert_row(pred, values)
            else:
                pass
                #print "Couldn't find {0}".format(pred)

    def load_labels(self, labels, path, files):
        for label in labels:

            if self.table_exists(label):
                self.logger.warning("Label '{}' already exists, skipping it. If you want to update it, delete it first".format(label))
            else:
                fullpath = os.path.join(path, files[label])
                with open(fullpath) as f:
                    name = files[label].split(".")[0]
                    self.create_table_label(label)
                    content = f.readlines()
                    for line in content:
                        values = re.split("\s+", line.strip())
                        self.insert_row(label, values)

    def add_filters(self, filters):
        #print("running add_filters")
        for (filter_table, filter_col, filter_ids, filter_values) in filters:
            try:
                self.add_column(filter_table, filter_col, 'INTEGER', 0)
                self.create_indexes(filter_table, [filter_col])
            except sqlite3.OperationalError as e:
                self.clear_column(filter_table, filter_col, 0)
                # Clear cache -- when filter columns are changed, queries need to be re-generated
                self.cache = {}

            if not isinstance(filter_ids, tuple) and not isinstance(filter_ids, list):
                filter_ids = (filter_ids,)
                filter_values = [(v,) for v in filter_values]

            for values in filter_values:
                query = ""; first_where = True

                for _id, _val in zip(filter_ids, values):
                    col_index = self.table_index[filter_table].index(_id)
                    col_type = self.table_index_types[filter_table][col_index]

                    if col_type == ArgumentType.UniqueString and first_where:
                        curr_query = 'UPDATE {0} SET {1} = 1 WHERE {2} = "{3}"'
                        first_where = False
                    elif col_type != ArgumentType.UniqueString and first_where:
                        curr_query = 'UPDATE {0} SET {1} = 1 WHERE {2} = {3}'
                        first_where = False
                    elif col_type == ArgumentType.UniqueString and not first_where:
                        curr_query = ' AND {2} = "{3}"'
                    else:
                        curr_query = ' AND {2} = {3}'
                    #print(filter_table, filter_col, _id, _val)
                    curr_query = curr_query.format(filter_table, filter_col, _id, _val)
                    query += curr_query
                #print(query)
                self.cur.execute(query)

    def _next_table(self, pred_name, alias, pos):
        if pos == 0:
            return " as {} ".format(alias)
        return " as {} ".format(alias)

    def _select_all(self, pred_names, aliases):
        columns = []
        for p, a in zip(pred_names, aliases):
            for col in self.table_index[p]:
                columns.append("{}.{}".format(a, col))
        return "SELECT DISTINCT {} ".format(", ".join(columns))

    def _select_curr(self, pred_name):
        columns = []
        for col in self.table_index[pred_name]:
            columns.append("{}.{}".format(pred_name, col))
        return "SELECT DISTINCT {} ".format(", ".join(columns))

    def _join_condition(self, alias_curr, column_curr, alias_prev, column_prev,
                        first_condition):
        condition = "{}.{} = {}.{}".format(alias_curr, column_curr, alias_prev, column_prev)
        if first_condition:
            return "ON {} ".format(condition)
        return "AND {} ".format(condition)


    def _all_where_conditions(self, table, where_conditions):
        first_condition = True
        query = ""
        for (alias, column, value, isconstant) in where_conditions:
            if isconstant:
                query += self._where_condition_constant(
                        table, column, value, first_condition)
                first_condition = False
            else:
                query += self._where_condition(
                        table, column, value, first_condition)
                first_condition = False
        return query

    def _where_condition_constant(self, alias, column, constant, first_condition):
        condition = '{}.{} = "{}"'.format(alias, column, constant)
        if first_condition:
            return "WHERE {} ".format(condition)
        return "AND {} ".format(condition)

    def _where_condition(self, alias, column, value, first_condition):
        condition = "{}.{} = {}".format(alias, column, value)
        if first_condition:
            return "WHERE {} ".format(condition)
        return "AND {} ".format(condition)

    def _where_condition_crosstab_diff(self, alias_1, column_1, alias_2, column_2, first_condition):
        condition = "{}.{} != {}.{}".format(alias_1, column_1, alias_2, column_2)
        if first_condition:
            return "WHERE {} ".format(condition)
        return "AND {} ".format(condition)

    def _null_condition(self, alias, column, first_condition):
        condition = "{}.{} IS NULL".format(alias, column)
        if first_condition:
            return "WHERE {} ".format(condition)
        return "AND {} ".format(condition)


    def _cond_instance(self, group_by, group_id, seen_predicates, conditions={}):
        if group_by is not None:
            (group_by_pred, index) = group_by
            if group_by_pred in seen_predicates:
                column = self.table_index[group_by_pred][int(index) - 1]
                for alias in seen_predicates[group_by_pred]:
                    if alias not in conditions:
                        conditions[alias] = []
                    # use true when string
                    column_type = self.table_index_types[group_by_pred][int(index) - 1]
                    conditions[alias].append([alias, column, group_id, column_type == ArgumentType.UniqueString])
                    #query += self._where_condition(alias, column, group_id, first_where_condition)
            else:
                self.logger.warning("GroupBy predicate not found on rule, proceeding without it.")

    def _cond_constant(self, constants, conditions={}):
        for (alias, column, constant) in constants:
            if alias not in conditions:
                conditions[alias] = []
            conditions[alias].append([alias, column, constant, True])
            #query += self._where_condition_constant(alias, column, constant, first_where_condition)

    def _cond_filters(self, filter_by, seen_predicates, conditions={}):
        for (table, col, value) in filter_by:
            if table in seen_predicates:
                for alias in seen_predicates[table]:
                    if alias not in conditions:
                        conditions[alias] = []
                    conditions[alias].append([alias, col, value, False])

    def _left_join_filters(self, filters, conditions={}):
        for (alias, col) in filters:
            if alias not in conditions:
                conditions[alias] = []
            conditions[alias].append([alias, col, "NULL", False])

    def _negations(self, inadmisible_rows, inadmisible_constants, first_condition):
        query = ""
        for pred_name in inadmisible_rows:
            if len(inadmisible_rows[pred_name]) == 0:
                continue
            if first_condition:
                query += " WHERE"
                first_condition = False
            else:
                query += " AND"

            # Create subquery
            subquery_where_conds = []
            subquery = "( SELECT 1 FROM {0} ".format(pred_name)
            for (column_curr, alias_prev, column_prev) in inadmisible_rows[pred_name]:
                subquery_where_conds.append((alias_prev, column_prev, column_curr))

            # Create where condition in subquery
            first_cond = True
            for (alias_prev, column_prev, column_curr) in subquery_where_conds:
                subquery += self._where_condition(alias_prev, column_prev, column_curr, first_cond)
                first_cond = False
            if pred_name in inadmisible_constants and len(inadmisible_constants[pred_name]) > 0:
                for (column_curr, constant) in inadmisible_constants[pred_name]:
                    subquery += self._where_condition_constant(pred_name, column_curr, constant, first_cond)
                    first_cond = False

            subquery += ")"
            query += " NOT EXISTS {0} ".format(subquery)

        return query, first_condition

    def _logical_wheres(self, logical_ops, seen_variables, first_condition):
        query = ""
        for op in logical_ops:
            if op.operator == LogicalOpType.Diff:

                if not op.arg0.isconstant and not op.arg1.isconstant:
                    query += self._where_condition_crosstab_diff(
                            seen_variables[op.arg0.arg][0],
                            seen_variables[op.arg0.arg][1],
                            seen_variables[op.arg1.arg][0],
                            seen_variables[op.arg1.arg][1],
                            first_condition
                            )
                elif not op.arg0.isconstant and op.arg1.isconstant:
                    query += self._where_condition_constant(
                            seen_variables[op.arg0.arg][0],
                            seen_variables[op.arg0.arg][1],
                            op.arg1.arg,
                            first_condition
                            )
                else:
                    self.logger.error("First argument in diff operation can't be a constant")
                    exit(-1)
                first_condition = False
            else:
                self.logger.error("Operator not defined in the framework")
                exit(-1)
        return query

    def select_data(self, predicate_templates, logical_ops, join_vars, filter_by, group_by,
                    group_id, split_on_var, split_on_value, has_head, neg_head, latent_preds, debug=False):
        #print(latent_preds)
        #exit()
        seen_variables = {}; seen_predicates = {}
        inadmisible_rows = {}; inadmisible_constants = {}
        constants = []
        query = ""

        pred_names = []; aliases = []

        # Join all predicates in rule template
        join_order = []; table_order = []
        join_tables = {}
        head_alias = None; left_join_conds = []

        for i, pred in enumerate(predicate_templates):
            if pred.name in latent_preds:
                #print(pred.name)
                continue

            alias = "p{}".format(i)

            if not pred.isneg or (has_head and i == len(predicate_templates) - 1):
                # Non negations or heads for training

                # Save head alias
                if has_head and i == len(predicate_templates) - 1:
                    head_alias = alias

                curr_query = self._next_table(pred.name, alias, i)
                join_order.append(alias)
                table_order.append(pred.name)

                pred_names.append(pred.name)
                aliases.append(alias)

                if pred.name not in seen_predicates:
                    seen_predicates[pred.name] = []
                seen_predicates[pred.name].append(alias)

                # Make variables consistent using join conditions
                # Keep track of constants
                first_condition = True
                for j, var in enumerate(pred.variables):
                    #print(pred.name, pred.label_pos(), pred.label_var())
                    #print(self.table_index)
                    #print(alias, self.table_index, pred.name, j)
                    (alias_curr, column_curr) = (alias, self.table_index[pred.name][j])
                    if var.arg not in seen_variables and not var.isconstant:
                        seen_variables[var.arg] = (alias_curr, column_curr)
                    elif not var.isconstant:
                        (alias_prev, column_prev) = seen_variables[var.arg]

                        curr_query += self._join_condition(alias_curr, column_curr,
                                                      alias_prev, column_prev, first_condition)
                        if has_head and alias == head_alias and neg_head:
                            left_join_conds.append((alias_curr, column_curr))

                        first_condition = False
                    else:
                        constants.append((alias_curr, column_curr, var.arg))

                join_tables[alias] = curr_query
            else:
                # Negations
                if pred.name not in inadmisible_rows:
                    inadmisible_rows[pred.name] = []
                if pred.name not in inadmisible_constants:
                    inadmisible_constants[pred.name] = []

                for j, var in enumerate(pred.variables):
                    column_curr = self.table_index[pred.name][j]
                    # Assume var is always seen, if not launch error
                    if var.arg not in seen_variables and not var.isconstant:
                        self.logger.error("Error on predicate ~{0}, var {1}: variables for negation of known predicate must be part of other non-negated predicates".format(pred.name, var.arg))
                        exit(-1)

                    elif not var.isconstant:
                        (alias_prev, column_prev) = seen_variables[var.arg]

                        inadmisible_rows[pred.name].append((column_curr, alias_prev, column_prev))
                    else:
                        inadmisible_constants[pred.name].append((column_curr, var.arg))

        # Add WHERE conditions
        where_conditions = {}
        first_where_condition = True
        ## WHERE instance
        self._cond_instance(group_by, group_id, seen_predicates, where_conditions)
        # WHERE constants
        self._cond_constant(constants, where_conditions)
        # WHERE filters
        self._cond_filters(filter_by, seen_predicates, where_conditions)

        query = self._select_all(pred_names, aliases) + "FROM "
        first_join = True

        for table, alias in zip(table_order, join_order):
            if not first_join and (not has_head or alias != head_alias or not neg_head):
                query += "INNER JOIN "
            elif has_head and alias == head_alias and neg_head:
                query += "LEFT JOIN "
            if alias in where_conditions:
                query += "( "
                query += self._select_curr(table)
                query += "FROM {} ".format(table)
                query += self._all_where_conditions(table, where_conditions[alias])
                query += ") "
            else:
                query += "{} ".format(table)

            query += join_tables[alias]
            first_join = False

        # LEFT JOIN filters (neg head)
        first_left = True
        for (alias, col) in left_join_conds:
            query += self._null_condition(alias, col, first_left)
            first_left = False

        # Predicate negations
        q, first_left = self._negations(inadmisible_rows, inadmisible_constants, first_left)
        query += q

        # Where conditions on logical operations
        query += self._logical_wheres(logical_ops, seen_variables, first_left)

        # TO-DO
        # Split on
        if debug:
            self.logger.info("QUERY === {}".format(query))
        self.cur.execute(query)

        if query in self.cache:
            #print("===================================== USING CACHE!!!!!")
            rows = self.cache[query]
        else:
            rows = self.cur.fetchall()
            self.cache[query] = rows
        return rows

    def select_label(self, label):
        query = "SELECT DISTINCT * FROM {}".format(label)
        self.cur.execute(query)
        return self.cur.fetchall()

    def _create_observed_predicate(self, data, index, predicate_templ, observed_vars):
        args = []
        for j, var in enumerate(predicate_templ.variables):
            if data is not None and data[index] is not None:
                observed_vars[var.arg] = data[index]
            args.append(observed_vars[var.arg])
            index += 1

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': None,
                         'obs': True, 'isneg': predicate_templ.isneg,
                         'target_pos': None}
        return index, new_predicate

    def _create_observed_head(self, data, index, predicate_templ, observed_vars, neg_head):
        args = []
        #print("predicate_templ", predicate_templ)
        #print("observed_vars", observed_vars)
        #print("neg_head", neg_head)
        #print("data", data)
        #print("index", index)

        target_pos = None; target_type = None; obs = True
        for j, var in enumerate(predicate_templ.variables):
            if data[index] is not None and not var.isconstant:
                observed_vars[var.arg] = data[index]
            if not var.isconstant:
                args.append(observed_vars[var.arg])
            else:
                args.append(var.arg)
            index += 1

            if not predicate_templ.isobs and not var.isobs:
                target_pos = j
                target_type = var.label
                obs = False
            elif predicate_templ.isobs and predicate_templ.label_type():
                target_pos = None
                target_type = predicate_templ.label_type()
                obs = False

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': target_type,
                         'obs': obs, 'isneg': neg_head,
                         'target_pos': target_pos}
        return index, new_predicate

    def _create_negated_observed_predicate(self, data, predicate_templ, observed_vars):
        args = []
        for j, var in enumerate(predicate_templ.variables):
            if var.isconstant:
                args.append(var.arg)
            else:
                args.append(observed_vars[var.arg])

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': None,
                         'obs': True, 'isneg': predicate_templ.isneg,
                         'target_pos': None}
        return new_predicate

    def _create_unobserved_binary_predicate(self, data, predicate_templ, observed_vars,
                                            permutations, var_idents):
        args = []
        for j, var in enumerate(predicate_templ.variables):
            if var.isconstant:
                args.append(var.arg)
            else:
                args.append(observed_vars[var.arg])

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()

        if str(predicate_templ) in var_idents:
            new_predicate = {'name': predicate_templ.name,
                             'arguments': args, 'ttype': None,
                             'obs': False, 'isneg': bool(permutations[var_idents[str(predicate_templ)]]),
                             'target_pos': None}
        else:
            new_predicate = {'name': predicate_templ.name,
                             'arguments': args, 'ttype': None,
                             'obs': False, 'isneg': predicate_templ.isneg,
                             'target_pos': None}
        return new_predicate

    def _create_unobserved_variable_predicate(self, data, predicate_templ, observed_vars,
                                              permutation, var_idents):
        args = []
        for j, var in enumerate(predicate_templ.variables):
            if var.arg in var_idents:
                args.append(permutation[var_idents[var.arg]])
            elif var.isconstant:
                args.append(var.arg)
            else:
                args.append(observed_vars[var.arg])

        ttype=predicate_templ.variables[j].label.l_type
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': ttype,
                         'obs': False, 'isneg': predicate_templ.isneg,
                         'target_pos': j}
        return new_predicate

    def _create_body(self, data, rule_body, observed_vars,
                     permutation, var_idents, head_ground_truth):
        index = 0; new_body = []
        isgroundtruth = True
        for pred_template in rule_body:
            if pred_template.isobs and not pred_template.label_type() and not pred_template.isneg:
                index, new_predicate = \
                    self._create_observed_predicate(data, index, pred_template, observed_vars)
            elif pred_template.isobs and not pred_template.label_type() and pred_template.isneg:
                new_predicate = \
                    self._create_negated_observed_predicate(data, pred_template, observed_vars)
            elif not pred_template.isobs:
                new_predicate = \
                    self._create_unobserved_binary_predicate(data, pred_template, observed_vars, permutation, var_idents)
                head_str = RuleGrounding.pred_str(new_predicate)
                if head_str.startswith('~') and head_str[1:] in head_ground_truth:
                    isgroundtruth = False
                elif not head_str.startswith('~') and head_str not in head_ground_truth:
                    isgroundtruth = False
            else:
                new_predicate = \
                    self._create_unobserved_variable_predicate(data, pred_template, observed_vars,
                                                               permutation, var_idents)
                head_str = RuleGrounding.pred_str(new_predicate)
                if head_str not in head_ground_truth:
                    isgroundtruth = False
            new_body.append(new_predicate)
        return index, new_body, isgroundtruth


    def _create_groundings(self, data, rule_template, uid_template, var_idents, permutations, head_ground_truth, latent_preds):
        groundings = []

        perm_len = len(permutations)
        n = max(1, perm_len)
        for perm_idx in range(0, n):
            # Initial empty rule
            observed_vars = {}
            # Build rule grounding
            isgroundtruth = True

            if perm_len > 0:
                index, new_body, isgroundtruth = self._create_body(data, rule_template.body, observed_vars, permutations[perm_idx], var_idents, head_ground_truth)
            else:
                index, new_body, isgroundtruth = self._create_body(data, rule_template.body, observed_vars, None, var_idents, head_ground_truth)

            new_rule = RuleGrounding([], None, uid_template, not rule_template.head.isobs, None, isgroundtruth=isgroundtruth)
            new_rule.body = new_body

            if rule_template.head.isobs and not rule_template.head.label_type():
                self.logger.error("Unsupported case: observed head")
                exit()
            elif not rule_template.head.isobs:
                if perm_len > 0:
                    new_rule.head = \
                        self._create_unobserved_binary_predicate(
                            data, rule_template.head, observed_vars, permutations[perm_idx], var_idents)
                else:
                    new_rule.head = \
                        self._create_unobserved_binary_predicate(
                            data, rule_template.head, observed_vars, None, var_idents)
                head_str = RuleGrounding.pred_str(new_rule.head)
                if head_str.startswith('~') and head_str[1:] in head_ground_truth:
                    new_rule.isgroundtruth = False
                elif not head_str.startswith('~') and head_str not in head_ground_truth:
                    new_rule.isgroundtruth = False
                elif new_rule.head['name'] in latent_preds:
                    new_rule.isgroundtruth = False

            else:
                new_rule.head = \
                    self._create_unobserved_variable_predicate(
                        data, rule_template.head, observed_vars, permutations[perm_idx], var_idents)
                head_str = RuleGrounding.pred_str(new_rule.head)
                if head_str not in head_ground_truth:
                    new_rule.isgroundtruth = False
                elif new_rule.head['name'] in latent_preds:
                    new_rule.isgroundtruth = False

            new_rule.build_predicate_index()
            new_rule.build_body_predicate_dic()
            new_rule.create_hash()
            groundings.append(new_rule)
            #print(new_rule)

        return groundings


    def unfold_train_groundings(self, rule_template, uid_template, split_on_value=None, filter_by=[],
                                neg_head=False, group_by=None, instance_id=None, latent_preds=set([])):
        #print("unfold_train_groundings", latent_preds)
        predicate_templates = rule_template.body + [rule_template.head]
        logical_ops = rule_template.logical_ops
        #print(predicate_templates)

        # When extracting observed data, no predicate should be treated as latent
        selected = \
            self.select_data(predicate_templates, logical_ops, None, filter_by, group_by, instance_id, None, None,
                             has_head=True, neg_head=neg_head, latent_preds=set([]))
        #print(len(selected))
        '''
        print rule_template
        for s in selected:
            print s
        '''
        ret = []

        for s in selected:
            #print(s)
            # Initial empty rule

            observed_vars = {}
            new_rule = RuleGrounding([], None, uid_template, not rule_template.head.isobs, None)

            # Build rule grounding
            index = 0
            for pred_template in rule_template.body:
                index, new_predicate = self._create_observed_predicate(s, index, pred_template, observed_vars)
                new_rule.body.append(new_predicate)
            _, new_rule.head = self._create_observed_head(s, index, rule_template.head, observed_vars, neg_head)

            new_rule.build_predicate_index()
            new_rule.build_body_predicate_dic()
            new_rule.create_hash()
            #print("new_rule", new_rule)
            ret.append(new_rule)
        return ret

    def _get_observed_preds_and_labels(self, predicate_templates):
        preds = []; labels = set([])
        for pred in predicate_templates:
            all_variables_observed = True
            for var in pred.variables:
                if not var.isobs:
                    labels.add(var.label.name)
                    all_variables_observed = False
            if pred.isobs and all_variables_observed:
                preds.append(pred)
        return preds, labels


    '''
        For now, deactivate the explicit grounding of negations on the LHS of the rule. Use a flag to turn this on
    '''
    def unfold_rule_groundings(self, rule_template, uid_template, group_by, instance_id,
            split_on_value, head_ground_truth, isconstr=False, filter_by=[], latent_preds=set([]), debug=False, ground_negs=False):
        #isconstr = True
        #print(rule_template)
        #print(latent_preds)
        predicate_templates = rule_template.body + [rule_template.head]
        predicate_templates, label_names = self._get_observed_preds_and_labels(predicate_templates)

        logical_ops = rule_template.logical_ops

        if debug:
            self.logger.info("UNFOLDING rule === {}".format(rule_template))
            self.logger.info("UNOBSERVED preds, label_names === {}, {}".format(predicate_templates, label_names))

        selected = \
            self.select_data(predicate_templates, logical_ops, None, filter_by, group_by, instance_id, None, None,
                             has_head=False, neg_head=False, debug=debug, latent_preds=latent_preds)
        #print(len(selected))
        label_data = {}
        for label in label_names:
            label_data[label] = self.select_label(label)

        ret = []

        # Accumulate the labels that we have in the data
        var_idents = {}; var_index = 0; var_values = []

        preds = rule_template.body + [rule_template.head]
        n_preds = len(preds)
        for pred_index, predicate_templ in enumerate(preds):

            if predicate_templ.label_type():
                for var_pos, var in enumerate(predicate_templ.variables):

                    # No need to permutate twice or deal with observed variables
                    if var.arg in var_idents or var.isobs:
                        continue

                    name = predicate_templ.variables[var_pos].label.name
                    values = [l[0] for l in label_data[name]]
                    var_values.append(values)
                    var_idents[var.arg] = var_index; var_index += 1
            elif (not predicate_templ.isobs and not isconstr and ground_negs and not rule_template.is_soft_constr):
                    #or (rule_template.is_soft_constr and ground_negs and pred_index == n_preds - 1):
                # For binary predicates, permutations correspond to true/false
                # In the case of constraints, we don't want to unfold the permutations
                var_values.append([1, 0])
                #print(var_values)
                var_idents[str(predicate_templ)] = var_index; var_index += 1

        permutations = []
        if len(var_values) > 0:
            #print(var_values)
            permutations = list(itertools.product(*var_values))
            #print("permutations", permutations)

        for s in selected:
            # Build rule groundings
            groundings = self._create_groundings(s, rule_template, uid_template, var_idents, permutations, head_ground_truth, latent_preds)
            ret += groundings
        #print(rule_template, len(ret))
        return ret

    # TO-DO: rewrite this function
    def get_ruleset_instances(self, ruleset, group_by, filter_by=[]):
        rule_template = ruleset[0]
        tables = {}; columns = {}
        group_by_col = self.table_index[group_by[0]][int(group_by[1]) - 1]
        query = "SELECT DISTINCT {0} FROM {1} ".format(
                    group_by_col, group_by[0])
        for pred in rule_template.body:
            if pred.name == group_by[0]:
                for j, var in enumerate(pred.variables):
                    #print self.predicate_entities
                    tables[var.arg] = self.predicate_entities[group_by[0]][j]
                    columns[var.arg] = self.table_index[group_by[0]][j]

        first_where = True
        for table, col, value in filter_by:
            if table == group_by[0]:
                #query += "INNER JOIN {0} ".format(tables[var])
                #query += " ON {0}.{1} = {2}.{3} ".format(
                #        group_by[0], columns[var], tables[var],
                #        self.table_index[tables[var]][0])
                if first_where:
                    query += " WHERE {0}.{1} = {2}".format(
                            group_by[0], col, value)
                    first_where = False
                else:
                    query += " AND {0}.{1} = {2}".format(
                            group_by[0], col, value)

        #print(query)
        self.cur.execute(query)
        ret = self.cur.fetchall()
        #print(ret)
        ret = [r[0] for r in ret]
        return ret


    def get_gold_predicates(self, rule_templates, filter_by, group_by, instance_id, latent_preds):
        ret = []; gold_data = set([])
        for rule_template in rule_templates:
            #print(rule_template)

            #group_by_preds = rule_template.predicate_index[group_by[0]]
            # use all body as filter, not only group by
            if rule_template.head.isneg:
                head = copy.copy(rule_template.head)
                head.isneg = not rule_template.head.isneg
            else:
                head = rule_template.head

            predicate_templates = [head] + rule_template.body

            pred_variables = [var.arg for var in head.variables]
            pred = head
            logical_ops = rule_template.logical_ops

            if pred.name in latent_preds:
                continue

            selected = \
                self.select_data(predicate_templates, logical_ops, None, filter_by, group_by, instance_id, None, None,
                                 has_head=False, neg_head=False, latent_preds=latent_preds)

            for s in selected:
                arguments = s[:len(pred_variables)]
                head_grounding = {'name': pred.name, 'arguments': arguments, 'isneg': pred.isneg, 'obs': pred.isobs,
                                  'target_pos': pred.label_pos()}
                str_ = RuleGrounding.pred_str(head_grounding)
                if str_ not in gold_data:
                    gold_data.add(str_)
                    ret.append(head_grounding)
        return ret
