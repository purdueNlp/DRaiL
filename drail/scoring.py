class ScoringClass(object):

    def __init__(self):
        pass

    def apply(self, function_name, features, fe_class):
        return getattr(self, function_name)(features, fe_class)
