import logging
from collections import OrderedDict
import copy
from tqdm import tqdm, trange

import gurobipy as grb
import numpy as np
import ast

from ..model.rule import RuleGrounding
from ..model.label import LabelType
from .inferencer_base import InferencerBase


class ILPInferencer(InferencerBase):

    def __init__(self, ruleGroundingsInput, constraintGroundingsInput, treeConstraintGroundingsInput, treeInfo,
                 arithmeticConstraintsInput, iscrf=False, inferenceLimit=None, solution_pool=10):
        super(ILPInferencer, self).__init__(ruleGroundingsInput, constraintGroundingsInput, treeConstraintGroundingsInput, treeInfo)
        """
        Initializes a Gurobi ILP Inferencer given a set of rule groundings.

        Args:
            ruleGroundingsInput: a list of rule groundings
            relaxation: optimization relaxation ('MIP', 'ILP', 'LP')
        """
        self.model = None
        self.relaxation = 'ILP'
        self.iscrf = iscrf
        self.logger = logging.getLogger(self.__class__.__name__)
        self.inferenceLimit = inferenceLimit
        self.solution_pool = solution_pool
        self.constraintMap = {}
        #print("Finished initialization")
        #print("Updated rule weights")
        #print("# of rule groundings is {}".format(len(self.ruleGroundings)))

        self.arithmetic_placeholders = arithmeticConstraintsInput


    def createModel(self, modelName, timeLimit = None):
        """
        Creates the Gurobi model.

        Args:
            modelName: model name, string type
        """
        self.model = grb.Model(modelName)
        self.model.setParam('OutputFlag', 0)

        if timeLimit is not None:
            self.model.setParam('TimeLimit', timeLimit)
        # Set the sense to maximization
        self.model.setAttr("ModelSense", -1)
        #print("Created model")
        if self.iscrf:
            self.model.setParam("PoolSearchMode", 1)
            self.model.setParam("PoolSolutions", self.solution_pool)

    def addVariables(self, debug):
        """
        Adds variables based on all rule groundings in this instance.
        TO-DO: modularize code for readability
        """
        #print ("Adding Variables")
        self.headDict = OrderedDict()
        self.multiClassDict = OrderedDict()
        self.multiLabelDict = OrderedDict()
        self.binaryDict = OrderedDict()
        self.numRules = 0

        self.multi_predicates = set([])

        self.ruleVarCounter = 0
        #debugout = open('ruleWeights.txt', 'w')

        pbar = tqdm(total=len(self.ruleGroundings), disable=not debug, desc="Adding variables")
        for ruleGrounding in self.ruleGroundings:
            #print(ruleGrounding)
            '''
            debugout.write(str(ruleGrounding) + ' ' +
                    str(self.ruleGroundings[ruleGrounding]) + ' ' +
                    str(ruleVarCounter) + '\n')
            '''
            coeffi = self.ruleGroundings[ruleGrounding]
            rvar = self.addRuleVariable(coeffi)
            #print("Rule: {}, weight: {}".format(ruleGrounding, coeffi))

            if (ruleGrounding.head_str not in self.headDict and
                ruleGrounding.neg_head_str not in self.headDict):
                #print(ruleGrounding.head_str)
                #print(ruleGrounding.neg_head_str)
                #print("-------")
                #exit()
                headVar, negVar = self.addHeadVariable(
                    ruleGrounding.head_str,
                    ruleGrounding.neg_head_str,
                    rvar)

                #print("H2R ->", ruleGrounding.head_str, rvar)

                if ruleGrounding.is_binary_head:
                    if ruleGrounding.head['name'] not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']] = {}
                    negated_predicate = ruleGrounding.head.copy()
                    negated_predicate['isneg'] = not ruleGrounding.head['isneg']
                    if headVar not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']][headVar] = ruleGrounding.head
                    if negVar not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']][negVar] = negated_predicate

                elif ruleGrounding.head['target_pos'] is not None and ruleGrounding.head['ttype'] == LabelType.Multiclass:
                    # Multiclass
                    self.multi_predicates.add(ruleGrounding.head['name'])
                    if (ruleGrounding.head_abs_str not in self.multiClassDict):
                        self.multiClassDict[ruleGrounding.head_abs_str] = {}
                    if headVar not in self.multiClassDict[ruleGrounding.head_abs_str]:
                        self.multiClassDict[ruleGrounding.head_abs_str][headVar] = ruleGrounding.head
                elif ruleGrounding.head['target_pos'] is not None and ruleGrounding.head['ttype'] == LabelType.Multilabel:
                    # Multilabel
                    if (ruleGrounding.head_abs_str not in self.multiLabelDict):
                        self.multiLabelDict[ruleGrounding.head_abs_str] = {}
                    if headVar not in self.multiLabelDict[ruleGrounding.head_abs_str]:
                        self.multiLabelDict[ruleGrounding.head_abs_str][headVar] = ruleGrounding.head
                else:
                    print("Head type not supported")
                    exit(-1)

            else:
                #print("H2R ->", ruleGrounding.head_str, rvar)
                self.headDict[ruleGrounding.head_str][2].append(rvar)
            pbar.update(1)
        pbar.close()
        '''
        debugout.write(str(self.headDict) + '\n')
        debugout.write(str(self.multiClassDict) + '\n')
        debugout.write(str(self.multiLabelDict) + '\n')
        debugout.write(str(self.binaryDict) + '\n')
        debugout.close()
        '''
        #print ("Added {} rule variables".format(self.ruleVarCounter))
        #print ("Added {} head variables".format(len(self.headDict)))

    def addRuleVariable(self, coeffi):
        if self.relaxation != 'ILP':
            rvar = self.model.addVar(
                    obj=coeffi,
                    lb = 0.0, ub = 1.0, vtype=grb.GRB.CONTINUOUS,
                    name='r_'+str(self.ruleVarCounter))
        else:
            rvar = self.model.addVar(
                    obj=coeffi,
                    lb = 0.0, ub = 1.0, vtype=grb.GRB.BINARY,
                    name='r_'+str(self.ruleVarCounter))

        self.model.update()
        self.ruleVarCounter += 1
        return rvar

    def addHeadVariable(self, head_str, neg_head_str, rvar):
        headCounter = len(self.headDict)

        if self.relaxation == 'LP':
            headVar = self.model.addVar(obj=0, lb = 0.0, ub = 1.0,
                    vtype=grb.GRB.CONTINUOUS,
                    name='h_'+str(headCounter))
        else:
            headVar = self.model.addVar(obj=0, lb = 0.0, ub = 1.0,
                    vtype=grb.GRB.BINARY,
                    name='h_'+str(headCounter))
        headCounter += 1
        if self.relaxation == 'LP':
            negVar = self.model.addVar(obj=0, lb = 0.0, ub = 1.0,
                    vtype=grb.GRB.CONTINUOUS,
                    name='h_'+str(headCounter))
        else:
            negVar = self.model.addVar(obj=0, lb = 0.0, ub = 1.0,
                    vtype=grb.GRB.BINARY,
                    name='h_'+str(headCounter))
        headCounter += 1

        self.headDict[head_str] = [headVar, negVar, [rvar]]
        self.headDict[neg_head_str] = [negVar, headVar, []]
        self.model.update()
        return headVar, negVar

    def addHardConstraintHeads(self, debug):
        pbar = tqdm(total=len(self.constraintGroundings), disable=not debug, desc='Adding constraint heads')
        #print "heads before constraints", len(self.headDict)
        constraintHeads = set([])
        for constrGrounding in self.constraintGroundings:
            if constrGrounding.head_str not in self.headDict and constrGrounding.neg_head_str not in self.headDict:
                rvar = self.addRuleVariable(0)
                headVar, negVar = self.addHeadVariable(constrGrounding.head_str, constrGrounding.neg_head_str, rvar)
                constraintHeads.add(constrGrounding.head_str)

                '''
                    Adding this to be able to keep track of activations
                    of predicates that are introduced in constraints only
                '''
                negated_predicate = constrGrounding.head.copy()
                negated_predicate['isneg'] = not constrGrounding.head['isneg']

                #print(constrGrounding, constrGrounding.is_binary_head)

                if constrGrounding.is_binary_head:
                    if constrGrounding.head['name'] not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']] = {}
                    if headVar not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']][headVar] = constrGrounding.head
                    if negVar not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']][negVar] = negated_predicate
            pbar.update(1)
        pbar.close()
        #print constraintHeads
        #print "heads after constraints", len(self.headDict)

    def addAuxiliaryTreeVars(self, debug):
        auxCounter = 0
        self.auxiliaryVars = {}
        pbar = tqdm(total=len(self.treeConstraintGroundings), disable=not debug, desc='Adding tree auxiliary vars')
        for pred in self.treeConstraintGroundings:
            self.auxiliaryVars[pred] = {}
            for reln in self.treeConstraintGroundings[pred]:
                i, j = reln['arguments'][-2:]
                self.auxiliaryVars[pred]["{0},{1}".format(i,j)] =\
                        self.model.addVar(obj=0, lb=0.0, ub = 1.0, vtype=grb.GRB.BINARY, name="b_{0}".format(auxCounter))
                auxCounter += 1
            pbar.update(1)
        self.model.update()

    def _node_types(self, template, type_dict, reln_name, reln, hvar):
        observed_data = dict(zip(self.treeInfo[reln_name]['variables'], reln['arguments']))
        if template is not None:
            cand = RuleGrounding.pred_str(
                self._create_observed_predicate(template, observed_data))
            if cand not in type_dict:
                type_dict[cand] = []
            type_dict[cand].append(hvar)

    def _create_observed_predicate(self, predicate_templ, observed_vars):
        args = []
        for j, var in enumerate(predicate_templ.variables):
            if var.isconstant:
                args.append(var.arg)
            else:
                args.append(observed_vars[var.arg])

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': None,
                         'obs': True, 'isneg': predicate_templ.isneg,
                         'target_pos': None}
        return new_predicate

    def addTreeConstraints(self, constrCounter, debug):
        pbar = tqdm(total=len(self.treeConstraintGroundings), disable=not debug, desc='Adding tree constraits')

        for pred in self.treeConstraintGroundings:
            matrix = {}; matrix_id = {}
            connected_components = []
            roots = {}; notRoots = {}; leaves = {}

            for reln in self.treeConstraintGroundings[pred]:
                i, j = reln['arguments'][-2:]
                reln_str = RuleGrounding.pred_str(reln)
                reln_str_aux = "{0},{1}".format(i,j)

                # collect constraints on outgoing and incomming links
                hvar = self.headDict[reln_str][0]
                if self.treeInfo[pred]['root'] is not None:
                    self._node_types(self.treeInfo[pred]['root'], roots, pred, reln, hvar)
                if self.treeInfo[pred]['notRoot'] is not None:
                    self._node_types(self.treeInfo[pred]['notRoot'], notRoots, pred, reln, hvar)
                if self.treeInfo[pred]['leaf'] is not None:
                    self._node_types(self.treeInfo[pred]['leaf'], leaves, pred, reln, hvar)

                found = False
                for comp in connected_components:
                    if i in comp:
                        found = True
                        comp.add(j)
                        break

                if not found:
                    connected_components.append(set([i, j]))

                if i not in matrix:
                    matrix[i] = []; matrix_id[i] = []
                matrix[i].append(reln_str)
                matrix_id[i].append(j)

                # Source and target can't be the same
                if i == j:
                    hvar = self.headDict[reln_str][0]
                    self.model.addConstr(hvar, grb.GRB.EQUAL, 0, "c_{0}".format(constrCounter))
                    constrCounter += 1

                    # Prevent cycles -- constr 3
                    self.model.addConstr(self.auxiliaryVars[pred][reln_str_aux], grb.GRB.EQUAL, 0, "c_{0}".format(constrCounter))
                    constrCounter += 1

                # Prevent cycles -- constr 1
                self.model.addConstr(hvar - self.auxiliaryVars[pred][reln_str_aux], grb.GRB.LESS_EQUAL, 0,
                                    "c_{0}".format(constrCounter))
                constrCounter += 1
                pbar.update(1)
            pbar.close()

            # At least one root per connected component
            pbar = tqdm(total=len(connected_components), disable=not debug, desc='At least one root')
            for comp in connected_components:
                n = len(comp)
                reln_variables = []
                for node in comp:
                    reln_variables += [self.headDict[pr][0] for pr in matrix[node]]
                self.model.addConstr(grb.quicksum(reln_variables), grb.GRB.LESS_EQUAL, n - 1, "c_{0}".format(constrCounter))
                constrCounter += 1
                pbar.update(1)
            pbar.close()

            pbar = tqdm(total=len(matrix), disable=not debug, desc='<= 1 outgoing links, no cycles')
            for source in matrix:
                # Only one or zero outgoing links
                target_variables = [self.headDict[pr][0] for pr in matrix[source]]
                self.model.addConstr(grb.quicksum(target_variables), grb.GRB.LESS_EQUAL, 1,
                                    " c_{0}".format(constrCounter))
                constrCounter += 1

                # Prevent cycles -- constr 2
                for k in matrix_id[source]:
                    for j in matrix_id[source]:
                        ik_index = "{0},{1}".format(source, k)
                        ij_index = "{0},{1}".format(source, j)
                        jk_index = "{0},{1}".format(j, k)

                        b_ik = self.auxiliaryVars[pred][ik_index]
                        b_ij = self.auxiliaryVars[pred][ij_index]
                        b_jk = self.auxiliaryVars[pred][jk_index]

                    self.model.addConstr(b_ik - b_ij - b_jk, grb.GRB.GREATER_EQUAL, -1,
                                        "c_{0}".format(constrCounter))
                    constrCounter += 1
                pbar.update(1)
            pbar.close()
            # Constraints on source/target

            # zero outgoing links for roots
            pbar = tqdm(total=len(roots), disable=not debug, desc='Adding root constraints')
            for node in roots:
                nvar = self.headDict[node][0]
                self.model.addConstr(nvar + grb.quicksum(roots[node]), grb.GRB.LESS_EQUAL, 1,
                                     "c_{0}".format(constrCounter))
                constrCounter += 1
                pbar.update(1)
            pbar.close()

            # exactly one outgoing link for not-roots
            pbar = tqdm(total=len(notRoots), disable=not debug, desc='Adding non-root constraints')
            for node in notRoots:
                nvar = self.headDict[node][0]
                self.model.addConstr(nvar - grb.quicksum(notRoots[node]), grb.GRB.LESS_EQUAL, 0,
                                     "c_{0}".format(constrCounter))
                constrCounter += 1
                pbar.update(1)
            pbar.close()

            # zero incoming links for leaves
            pbar = tqdm(total=len(leaves), disable=not debug, desc='Adding leaves constraints')
            for node in leaves:
                nvar = self.headDict[node][0]
                self.model.addConstr(nvar * grb.quicksum(leaves[node]), grb.GRB.LESS_EQUAL, 0,
                                     "c_{0}".format(constrCounter))
                constrCounter += 1
                pbar.update(1)
            pbar.close()

            # number of links >= # not_roots

            if len(roots) > 0:
                notroot_vars = [self.headDict[r][0] for r in notRoots]
                reln_vars = [self.headDict[RuleGrounding.pred_str(r)][0] for r in self.treeConstraintGroundings[pred]]

                self.model.addConstr(grb.quicksum(reln_vars) - grb.quicksum(notroot_vars), grb.GRB.GREATER_EQUAL, 0,
                                     "c_{0}".format(constrCounter))
                constrCounter += 1
        return constrCounter

    def addHardConstraints(self, constrCounter, debug):
        conditionalMulticlass = {}; conditionalMultilabel = {}
        seed_set = set([])
        same_constaints = 0
        pbar = tqdm(total=len(self.constraintGroundings), disable=not debug, desc='Adding hard constraints')
        for constrGrounding in self.constraintGroundings:
            #print(len(self.headDict.keys()))
            body_variables = []

            #unobserved = [pred for pred in constrGrounding.body if not pred['obs']]
            unobserved = constrGrounding.body_unobs_str
            '''
            inheads = [pred for pred in unobserved if pred in self.headDict]
            if len(unobserved) != len(inheads):
                continue
            '''
            for pred in unobserved:
                if pred not in self.headDict:
                    self.logger.info(self.headDict.keys())
            body_variables = [self.headDict[pred][0] for pred in unobserved]
            hvar = self.headDict[constrGrounding.head_str][0]
            n_body_variables = len(body_variables)

            if n_body_variables > 0 and constrGrounding.head['ttype'] not in [1, 2]:
                self.model.addConstr(grb.quicksum(body_variables), grb.GRB.LESS_EQUAL,
                                     n_body_variables-1 + hvar, "c_"+str(constrCounter))
                self.constraintMap["c_"+str(constrCounter)] = constrGrounding
                constrCounter += 1
                same_constaints += 1
                #print constrGrounding

            elif n_body_variables > 0 and constrGrounding.head['ttype'] == 1:
                headVar = self.headDict[constrGrounding.head_str][0]
                curr_hash = " ".join(unobserved) + constrGrounding.head_abs_str
                if curr_hash not in conditionalMulticlass:
                    conditionalMulticlass[curr_hash] = {'keys': {}, 'body_vars': body_variables}
                if headVar not in conditionalMulticlass[curr_hash]['keys']:
                    conditionalMulticlass[curr_hash]['keys'][headVar] = constrGrounding.head
            elif n_body_variables > 0 and constrGrounding.head['ttype'] == 2:
                headVar = self.headDict[constrGrounding.head_str][0]
                curr_hash = " ".join(unobserved) + constrGrounding.head_abs_str
                if curr_hash not in conditionalMultilabel:
                    conditionalMultilabel[curr_hash] = {'keys': {}, 'body_vars': body_variables}
                if headVar not in conditionalMultilabel[curr_hash]['keys']:
                    conditionalMultilabel[curr_hash]['keys'][headVar] = constrGrounding.head
            elif constrGrounding.head['ttype'] == 1:
                #print(constrGrounding)
                # keep track of multiclass elements in head of rule
                # constraint will be added later
                headVar = self.headDict[constrGrounding.head_str][0]
                if constrGrounding.head_abs_str not in self.multiClassDict:
                    self.multiClassDict[constrGrounding.head_abs_str] = {}
                if headVar not in self.multiClassDict[constrGrounding.head_abs_str]:
                    self.multiClassDict[constrGrounding.head_abs_str][headVar] = constrGrounding.head
            elif constrGrounding.head['ttype'] == 2:
                # keep track of multilabel elements in head of rule
                # constraint will be added later
                headVar = self.headDict[constrGrounding.head_str][0]
                if constrGrounding.head_abs_str not in self.multiLabelDict:
                    self.multiLabelDict[constrGrounding.head_abs_str] = {}
                if headVar not in self.multiLabelDict[constrGrounding.head_abs_str]:
                    self.multiLabelDict[constrGrounding.head_abs_str][headVar] = constrGrounding.head

            else:
                #print(constrGrounding)
                # enforcing the head
                #n_seeds += 1
                self.model.addConstr(hvar, grb.GRB.EQUAL, 1, "c_"+str(constrCounter))
                self.constraintMap["c_"+str(constrCounter)] = constrGrounding
                constrCounter += 1
                seed_set.add(hvar)
                # self.logger.debug("Enforcing seed for hvar {} head {}".format(hvar, constrGrounding.head_str))
            pbar.update(1)
        pbar.close()
        #print "SEEDED", n_seeds
        #print "AGR/DGR", same_constaints
        #print len(constraintHeads)
        #exit()
        #print("========")
        # Add the conditional MC stuff
        auxCounter = 0
        #print(conditionalMulticlass)

        pbar = tqdm(total=len(conditionalMulticlass), disable=not debug, desc='Adding conditional MC constraints')
        for abstra in conditionalMulticlass:
            head_vs = list(conditionalMulticlass[abstra]['keys'].keys())
            body_vs = conditionalMulticlass[abstra]['body_vars']

            aux_var_head = self.model.addVar(obj=0, lb=0.0, ub = 1.0, vtype=grb.GRB.BINARY, name="condMC_aux_{0}".format(auxCounter))
            auxCounter += 1

            self.model.addConstr(grb.quicksum(head_vs), grb.GRB.EQUAL, aux_var_head, "c_"+str(constrCounter))
            self.constraintMap["c_"+str(constrCounter)] = constrGrounding
            constrCounter += 1

            self.model.addConstr(grb.quicksum(body_vs), grb.GRB.LESS_EQUAL, len(body_vs) - 1 + aux_var_head, "c_"+str(constrCounter))
            self.constraintMap["c_"+str(constrCounter)] = constrGrounding
            constrCounter += 1
            pbar.update(1)
        pbar.close()

        auxCounter = 0
        pbar = tqdm(total=len(conditionalMulticlass), disable=not debug, desc='Adding conditional ML constraints')
        for abstra in conditionalMultilabel:
            head_vs = list(conditionalMultilabel[abstra]['keys'].keys())
            body_vs = conditionalMultilabel[abstra]['body_vars']
            # sum(body_vars) <= n_body_vars - 1 + sum(hvar)
            aux_var_head = self.model.addVar(obj=0, lb=0.0, ub = 1.0, vtype=grb.GRB.BINARY, name="condML_aux_{0}".format(auxCounter))
            auxCounter += 1

            self.model.addConstr(grb.quicksum(head_vs), grb.GRB.GREATER_EQUAL, aux_var_head, "c_"+str(constrCounter))
            self.constraintMap["c_"+str(constrCounter)] = constrGrounding
            constrCounter += 1

            self.model.addConstr(grb.quicksum(body_vs), grb.GRB.LESS_EQUAL, len(body_vs) - 1 + aux_var_head, "c_"+str(constrCounter))
            self.constraintMap["c_"+str(constrCounter)] = constrGrounding
            constrCounter += 1

            pbar.update(1)
        pbar.close()

        return constrCounter

    def addImplyConstraints(self, constrCounter, debug):
        # imply constr
        ruleVarCounter = 0

        seeded_B = 0
        deactivated_B = 0
        pbar = tqdm(total=len(self.ruleGroundings), disable=not debug, desc='Adding imply constraints')
        for ruleGrounding in self.ruleGroundings:
            #print(ruleGrounding)
            r = self.model.getVarByName('r_'+str(ruleVarCounter))

            #unobs = [pred for pred in gr.body if not pred['obs']]
            unobs = ruleGrounding.body_unobs_str
            found = [pred for pred in unobs if pred in self.headDict]

            if len(unobs) != len(found):
                self.model.addConstr(r, grb.GRB.EQUAL, 0, "c_"+str(constrCounter))
                self.constraintMap["c_"+str(constrCounter)] = "({}) = 0 (deactivate [unobs pred in body has no rule])".format(ruleGrounding)
                constrCounter += 1
                deactivated_B += len(unobs)
            else:
                for pred in found:
                    #if pred in constraintHeads:
                    #    seeded_B += 1
                    h = self.headDict[pred][0]
                    #print(h, r)
                    self.model.addConstr(h, grb.GRB.GREATER_EQUAL,
                                         r, "c_"+str(constrCounter))
                    constrCounter += 1

            ruleVarCounter += 1

            #print(ruleGrounding, ruleGrounding.is_binary_head)

            '''
            # WHAT WAS THIS? do not need to have imply constr for negation of binary head
            if (ruleGrounding.is_binary_head and
                    (ruleGrounding.head['target_pos'] is None or
                     ruleGrounding.head['ttype'] == LabelType.Binary)
                ):
                ruleVarCounter += 1
            '''
            pbar.update(1)
        pbar.close()
        #print "seeded_B", seeded_B
        #print "deactivated_B"
        return constrCounter

    def addNegationAndRuleHeadConstraints(self, constrCounter, debug):
        neg_constr = 0; impl_constraints = 0

        pbar = tqdm(total=len(self.headDict), disable=not debug, desc='Adding negation and rule/head constraints')
        for head in self.headDict:
            #print(head)
            # h
            headVar = self.headDict[head][0]
            if (head.startswith("~")):
                # neg
                negVar = self.headDict[head][1]
                self.model.addConstr(headVar + negVar, grb.GRB.EQUAL,
                                     1, "c_"+str(constrCounter))
                constrCounter += 1
                neg_constr += 1
            # [r1, r2, ]
            rs = self.headDict[head][2]
            if (len(rs) == 0):
                continue
            # r1 + r2 + 
            rsum = grb.quicksum(rs)
            for r in rs:
                # h >= r_i
                self.model.addConstr(headVar, grb.GRB.GREATER_EQUAL,
                                     r, "c_"+str(constrCounter))
                constrCounter += 1
            # h <= sum(r_i)
            self.model.addConstr(headVar, grb.GRB.LESS_EQUAL,
                                 rsum, "c_"+str(constrCounter))
            constrCounter += 1
            pbar.update(1)
        pbar.close()

        #print "Negation constr", neg_constr
        return constrCounter

    def addMulticlassMultilabelConstraints(self, constrCounter, debug):
        n_multiclass = 0; n_multilabel = 0
        pbar = tqdm(total=len(self.multiClassDict), disable=not debug, desc='Adding MC constraints')
        for abstra in self.multiClassDict:
            vs = self.multiClassDict[abstra].keys()
            vsum = grb.quicksum(vs)

            # THIS IS A HACK FOR NOW, WILL NEED TO SUPPORT EXPRESSING THIS IN LANGUAGE
            '''
            if abstra.startswith('HasFrame') or abstra.startswith('HasSubFrame'):
                self.model.addConstr(vsum, grb.GRB.EQUAL, 2,
                                     "c_"+str(constrCounter))
            else:
            '''
            self.model.addConstr(vsum, grb.GRB.EQUAL, 1,
                                 "c_"+str(constrCounter))

            hs = [RuleGrounding.pred_str(item) for item in self.multiClassDict[abstra].values()]
            self.constraintMap["c_"+str(constrCounter)] = " + ".join(hs) + " = 1 ({})".format(len(hs))
            constrCounter += 1
            n_multiclass += 1
            pbar.update(1)
        pbar.close()

        pbar = tqdm(total=len(self.multiLabelDict), disable=not debug, desc='Adding ML constraints')
        for abstra in self.multiLabelDict:
            vs = self.multiLabelDict[abstra].keys()
            #print(abstra, len(vs))
            vsum = grb.quicksum(vs)
            self.model.addConstr(vsum, grb.GRB.GREATER_EQUAL, 1,
                                 "c_"+str(constrCounter))

            hs = [RuleGrounding.pred_str(item) for item in self.multiLabelDict[abstra].values()]
            self.constraintMap["c_"+str(constrCounter)] = " + ".join(hs) + " >= 1 ({})".format(len(hs))
            constrCounter += 1
            n_multilabel += 1
            pbar.update(1)
        pbar.close()
        #exit()
        #print "multiclass constraints", n_multiclass
        #print "multilabel constraints", n_multilabel
        return constrCounter

    def addArithmeticConstraints(self, constrCounter, debug):
        pbar = tqdm(total=len(self.headDict), disable=not debug, desc='Tracking arithmetic constraints')
        if len(self.arithmetic_placeholders) > 0:
            for head in self.headDict:
                head_pred, _ = head.split('(')
                # Only consider positive atoms
                if head_pred.startswith('~'):
                    continue

                if head_pred in self.arithmetic_placeholders and \
                        (len(self.arithmetic_placeholders[head_pred]['heads']) == 0 or
                         head in self.arithmetic_placeholders[head_pred]['heads']):
                    head_var = self.headDict[head][0]
                    self.arithmetic_placeholders[head_pred]['headVars'].append(head_var)

                pbar.update(1)
            pbar.close()

            pbar = tqdm(total=len(self.arithmetic_placeholders), disable=not debug, desc='Adding arithmetic constraints')
            for constr in self.arithmetic_placeholders:
                if self.arithmetic_placeholders[constr]['comparison'] == ">=":
                    self.model.addConstr(
                        grb.quicksum(self.arithmetic_placeholders[constr]['headVars']),
                        grb.GRB.GREATER_EQUAL,
                        self.arithmetic_placeholders[constr]['number'])
                    self.constraintMap["c_"+str(constrCounter)] = self.arithmetic_placeholders[constr]['repr']
                    constrCounter += 1
                elif self.arithmetic_placeholders[constr]['comparison'] == "<=":
                    self.model.addConstr(
                        grb.quicksum(self.arithmetic_placeholders[constr]['headVars']),
                        grb.GRB.LESS_EQUAL,
                        self.arithmetic_placeholders[constr]['number'])
                    self.constraintMap["c_"+str(constrCounter)] = self.arithmetic_placeholders[constr]['repr']
                    constrCounter += 1
                else:
                    pass
                pbar.update(1)
            pbar.close()

        return constrCounter

    def addFixGoldConstraints(self, constrCounter, gold_heads, latent_predicates, debug):
        pbar = tqdm(total=len(self.headDict), disable=not debug, desc='Adding fix-gold constraints for latent learning')
        gold_heads_str = set([RuleGrounding.pred_str(h) for h in gold_heads])
        for head in self.headDict:
            head_pred, _ = head.split('(')
            if head_pred.startswith('~'):
                head_pred = head_pred[1:]
            if (head in gold_heads_str) or \
               (head.startswith('~') and (head[1:] not in gold_heads_str) and (head_pred) not in latent_predicates):
                head_var = self.headDict[head][0]
                self.model.addConstr(head_var, grb.GRB.GREATER_EQUAL, 1,
                                     "c_"+str(constrCounter))
                self.constraintMap["c_"+str(constrCounter)] = "{} = 1".format(head)
                constrCounter += 1
            pbar.update(1)
        pbar.close()

        return constrCounter

    def addConstraints(self, gold_heads, fix_gold=False, latent_predicates=set(), debug=False):
        """
        Adds constraints based on all rule groundings in this instance.
        This method should be applied after addVariables().
        """
        #print("Adding Constraints")
        constrCounter = 0

        # add variables needed for constraints
        self.addHardConstraintHeads(debug)
        self.addAuxiliaryTreeVars(debug)

        self.constraintMap = {}
        # hard constraints
        constrCounter = self.addHardConstraints(constrCounter, debug)

        # imply constr
        constrCounter = self.addImplyConstraints(constrCounter, debug)

        # negation constr, rule/head constr
        constrCounter = self.addNegationAndRuleHeadConstraints(constrCounter, debug)

        # Binary/Multi-Class/Multi-Label constr
        constrCounter = self.addMulticlassMultilabelConstraints(constrCounter, debug)

        # Tree constraints
        constrCounter = self.addTreeConstraints(constrCounter, debug)

        # Arithmetic constraints
        constrCounter = self.addArithmeticConstraints(constrCounter, debug)

        # Fix constraints for latent variable learning
        if fix_gold:
            constrCounter = self.addFixGoldConstraints(constrCounter, gold_heads, latent_predicates, debug)

        #print "NUMBER OF VARIABLES", len(self.headDict)
        self.model.update()
        # print("Added " + str(constrCounter) + " constraints")


    def encode(self, gold_heads=[], fix_gold=False, latent_predicates=set(), debug=False):
        self.createModel("testModel", timeLimit=self.inferenceLimit)
        self.addVariables(debug)
        self.addConstraints(gold_heads, fix_gold, latent_predicates, debug)

    def optimize(self):
        """
        Optimizes the current model.
        """
        self.model.optimize()
        # If model is infeasible
        if self.model.status == 3:
            print("==== Solution is infeasible")
            self.model.computeIIS()
            print('==== The following constraints cannot be simultaneously satisfied:')
            for c in self.model.getConstrs():
                if c.IISConstr:
                    if c.constrName in self.constraintMap:
                        print(self.constraintMap[c.constrName])
            print('====')
            exit(-1)

    def get_beam_solutions(self):
        solutions = []
        #print(self.model.getAttr('SolCount'))
        for i in range(0, self.model.getAttr('SolCount')):
            self.model.setParam('SolutionNumber', i)
            curr_sol = self.get_solution('xn')
            #print(curr_sol)
            solutions.append(curr_sol)
        return solutions

    def get_solution(self, param):
        predictions = set([])
        for head in self.headDict:
            h = self.headDict[head][0]

            try:
                pred_value = h.getAttr(param)
                if round(pred_value) == 1 and not head.startswith('~'):
                    predictions.add(head)
            except:
                continue
        return predictions

    def get_predictions(self):
        return self.get_solution('x')

    # TO-DO: Find a better way to determine what is latent and what is observed that doesn't depend on activations (particularly for binary case)
    def get_binary_metrics(self, dictionary, gold_heads_dict, latent_predicates):
        gold_heads = set([])
        for head in gold_heads_dict:
            gold_heads.add(RuleGrounding.pred_str(head))


        metrics = {}; predictions = set([])
        for (key, value_ls) in dictionary:
            for var, head in value_ls.items():
                if head['name'] in self.multi_predicates:
                    continue

                if head['name'] not in metrics and head['name'] not in latent_predicates:
                    metrics[head['name']] = {'gold_data': [], 'pred_data': [], 'ids': []}

                head_str = RuleGrounding.pred_str(head)
                pred_value = var.getAttr('x')

                # Gold label
                # Keep track of non-negated version only for metrics
                if not head_str.startswith('~'):
                    #print(head_str, var)
                    # Skip predicates latent predicates when calculating metrics
                    if head['name'] not in latent_predicates:
                        if head_str in gold_heads:
                            metrics[head['name']]['gold_data'].append(1)
                        else:
                            metrics[head['name']]['gold_data'].append(0)
                        metrics[head['name']]['ids'].append(head_str)

                        # Pred label
                        metrics[head['name']]['pred_data'].append(int(pred_value))

                    # We add latent predicates to the active heads
                    # Should we also add negations????? --> THINK ABOUT IT
                    if round(pred_value) == 1:
                        predictions.add(head_str)

        #print(metrics)
        #print(predictions)
        return predictions, metrics

    def get_multi_metrics(self, dictionary, gold_heads, binary_predicates):
        metrics = {}
        predictions = set([])

        # load dictionary of gold predictions
        gold_data = {}

        for elem in gold_heads:
            classif_name = elem['name']
            # skip binary predicates
            if classif_name not in self.multi_predicates:
                continue

            if classif_name not in gold_data:
                gold_data[classif_name] = {}
            id_ = ",".join(str(elem['arguments'][i]) for i in range(0, len(elem['arguments'])) if i != elem['target_pos'])
            label = elem['arguments'][elem['target_pos']]
            gold_data[classif_name][id_] = label

        # load dictionary of pred labels
        pred_data = {}

        for (key, value_ls) in dictionary:
            for var, head in value_ls.items():
                if head['name'] not in gold_data:
                # If we are not evaluating on it but there is still a prediction (latent pred)
                # Keep track of the prediction but do not add to metrics
                    pred_value = var.getAttr('x')
                    if round(pred_value) == 1:
                        predictions.add(RuleGrounding.pred_str(head))
                else:
                    if head['name'] in binary_predicates and head['name'] not in gold_data:
                        gold_data[head['name']] = {}
                        for g_head in gold_heads:
                            if g_head['name'] == head['name']:
                                k_class = g_head['arguments'][head['target_pos']]
                                curr_id = ",".join(str(g_head['arguments'][i]) for i in range(0, len(g_head['arguments'])) if i != head['target_pos'])
                                gold_data[head['name']][curr_id] = k_class

                    if head['name'] not in metrics:
                        metrics[head['name']] = {'gold_data': [], 'pred_data': [], 'ids': []}
                        pred_data[head['name']] = {}

                    k_class = head['arguments'][head['target_pos']]
                    curr_id = ",".join(str(head['arguments'][i]) for i in range(0, len(head['arguments'])) if i != head['target_pos'])
                    #print var, head
                    pred_value = var.getAttr('x')
                    if round(pred_value) == 1:
                        pred_data[head['name']][curr_id] = k_class
                        predictions.add(RuleGrounding.pred_str(head))
        '''
        print "--- Pred"
        for h in predictions:
            print h
        '''
        # create ordered list of predictions to be used in sklearn
        for classif_name in gold_data:

            if classif_name not in pred_data:
                self.logger.error("Gold multiclass relation {} doesn't have any predictions".format(classif_name))
                exit(-1)

            if len(pred_data[classif_name]) != len(gold_data[classif_name]):
                self.logger.error("Multiclass relation {} doesn't have the same name of gold ({}) and predicted ({}) instances. Ignoring predicate evaluation".format(
                    classif_name, len(gold_data[classif_name]), len(pred_data[classif_name])))
                #exit(-1)
                continue

            for id_ in gold_data[classif_name]:
                gold_label = gold_data[classif_name][id_]
                pred_label = pred_data[classif_name][id_]
                metrics[classif_name]['gold_data'].append(gold_label)
                metrics[classif_name]['pred_data'].append(pred_label)
                metrics[classif_name]['ids'].append(id_)

        return predictions, metrics


    # TO-DO: re-add and test binary case
    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False, latent_predicates=set()):
        # Evaluate multiclass classifiers
        metrics = {}

        heads_multiclass, metrics_multiclass =\
                self.get_multi_metrics(self.multiClassDict.items(),
                gold_heads, binary_predicates)
        metrics.update(metrics_multiclass)

        heads_binary, metrics_binary = \
                self.get_binary_metrics(self.binaryDict.items(),
                                        gold_heads, latent_predicates)

        if get_active_heads:
            active_heads = heads_multiclass | heads_binary
        else:
            active_heads = None

        metrics.update(metrics_binary)

        return metrics, active_heads
