class InferencerBase(object):

    def __init__(self, ruleGroundings, constraintGroundings, treeConstraintGroundings,
                 treeInfo):
        self.ruleGroundings = ruleGroundings
        self.constraintGroundings = constraintGroundings
        self.treeConstraintGroundings = treeConstraintGroundings
        self.treeInfo = treeInfo

    def encode(self):
        raise NotImplementedError()

    def optimize(self):
        raise NotImplementedError()

    def evaluate(self, gold_heads, binary_predicates=[]):
        raise NotImplementedError()
