#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <map>
#include <string>
#include <tuple>
#include <vector>
#include <iostream>
#include <stdexcept>

template <typename T>
struct Node {
    T id;
    double score {0.0};
    std::string arg;
    std::string stance {"None"};

    Node(T id_) : id(id_) {}
    Node(T id_, std::string arg_) : id(id_), arg(arg_) {}
    Node(T id_, double score_) : id(id_), score(score_) {}
    Node(T id_, double score_, std::string arg_) : id(id_), score(score_), arg(arg_) {}
};

template <typename T>
struct Edge {
    std::tuple <T, T> id;
    double score {0.0};
    std::string arg;

    Edge(std::tuple <T, T> id_) : id(id_) {}
    Edge(std::tuple <T, T> id_, std::string arg_) : id(id_), arg(arg_) {}
    Edge(std::tuple <T, T> id_, double score_) : id(id_), score(score_) {}
    Edge(std::tuple <T, T> id_, double score_, std::string arg_) : id(id_), score(score_), arg(arg_) {}

    inline T from() const { return std::get<0>(id); }
    inline T to()   const { return std::get<1>(id); }
};

template <typename T>
class DiGraph {
    std::map<T, Node<T>> nodes;
    std::map<std::tuple <T, T>, Edge<T>> edges;

    public:
        const std::map<T, Node<T>>& get_nodes()                 { return nodes; }
        const std::map<std::tuple <T, T>, Edge<T>>& get_edges() { return edges; }

        bool has_node(const Node<T>& n) const { return nodes.find(n.id) != nodes.end(); }
        bool has_node(const T n_id)     const { return nodes.find(n_id) != nodes.end(); }
        bool has_edge(const Edge<T>& e) const { return edges.find(e.id) != edges.end(); }

        Node<T>& node(const T& n_id)                 { return nodes.at(n_id); }
        Edge<T>& edge(const std::tuple <T, T>& e_id) { return edges.at(e_id); }

        void add_node(const Node<T>& n) {
            nodes.insert({n.id, n});
        }

        void add_edge(const Edge<T>& e) {
            if (has_edge(e))
                throw std::invalid_argument("edge does already exist");

            if (!has_node(e.from()))
                add_node(Node<T>(e.from()));

            if (!has_node(e.to()))
                add_node(Node<T>(e.to()));

            edges.insert({e.id, e});
        }

        void remove_edge(const Edge<T>& e) {
            edges.erase(e.id);
        }

        void remove_node(const Node<T>& n) {
            nodes.erase(n.id);

            // also remove linked edges
            std::vector<std::tuple <T, T>> to_erase;

            for (auto& element : edges)
                if (element.second.from() == n.id || element.second.to() == n.id)
                    to_erase.push_back(element.first);

            for (auto& key : to_erase)
                edges.erase(key);
        }

        std::vector<Node<T>> predecessors(const Node<T>& n) const {
            std::vector<Node<T>> predecessors;

            for (auto& element : edges)
                if (n.id == element.second.to())
                    predecessors.push_back(Node<T>(element.second.from()));

            return predecessors;
        }

        std::vector<Node<T>> successors(const Node<T>& n) const {
            std::vector<Node<T>> successors;

            for (auto& element : edges)
                if (n.id == element.second.from())
                    successors.push_back(Node<T>(element.second.to()));

            return successors;
        }

        double sum_node_scores() const {
            double score = 0;

            for (auto& element : nodes)
                score += element.second.score;

            return score;
        }

        double sum_edge_scores() const {
            double score = 0;

            for (auto& element : edges)
                score += element.second.score;

            return score;
        }

        int varying_node_args(const std::unique_ptr<DiGraph>& graph) const {
            int differences = 0;

            for (auto& element : nodes)
                if (element.second.arg != graph->node(element.first).arg)
                    differences += 1;

            return differences;
        }

        int varying_edge(const std::unique_ptr<DiGraph>& graph) const {
            int differences = 0;

            for (auto& element : edges)
                if (!graph->has_edge(element.first))
                    differences += 1;

            return differences;
        }

        int get_node_id(T node) const {
            // assert that node is of type string
            return std::stoi(node.substr(1, node.size()));
        }

        std::vector<std::tuple<T, T, T>> get_grandparents() const {
            std::vector<std::tuple<T, T, T>> grandparents;

            for (auto& outer_edge : edges) {
                auto& e1 = std::get<0>(outer_edge.first);
                auto& e2 = std::get<1>(outer_edge.first);

                for (auto& inner_edge : edges) {
                    auto& e3 = std::get<0>(inner_edge.first);
                    auto& e4 = std::get<1>(inner_edge.first);

                    if (e2 == e3)
                        grandparents.push_back(std::make_tuple(e4, e3, e1));
                }
            }

            return grandparents;
        }

        std::vector<std::tuple<T, T, T>> get_coparents() const {
            std::vector<std::tuple<T, T, T>> co_parents;

            for (auto& outer_edge : edges) {
                auto& e1 = std::get<0>(outer_edge.first);
                auto& e2 = std::get<1>(outer_edge.first);

                for (auto& inner_edge : edges) {
                    auto& e3 = std::get<0>(inner_edge.first);
                    auto& e4 = std::get<1>(inner_edge.first);

                    if (e2 == e4 && get_node_id(e1) < get_node_id(e3))
                        co_parents.push_back(std::make_tuple(e2, e1, e3));
                }
            }

            return co_parents;
        }

        void show() {
            std::cout << "==========\nnodes:\n";
            for (auto element : nodes)
                std::cout << "key: " << element.first << " - score: " << element.second.score << " - arg: " << element.second.arg << " - stance: " << element.second.stance << "\n";

            std::cout << "==========\nedges:\n";
            for (auto element : edges)
                std::cout << "from: " << element.second.from() << " - to: " << element.second.to() << " - score: " << element.second.score << " - arg: " << element.second.arg << "\n";
        }
};

// make_unique as in <memory> package (c++14 feature)
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

#endif