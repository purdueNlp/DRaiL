#ifndef CORE_H
#define CORE_H

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <memory>
#include <random>
#include <stdexcept>
#include <set>
#include "di_graph.h"

namespace py = pybind11;

typedef std::map<int, std::vector<std::tuple<std::string, double>>> pynodes;
typedef std::map<std::tuple<int, int>, double> pyedges;
typedef std::vector<std::tuple<int, std::string>> pygold_n;
typedef std::vector<std::tuple<int, int>> pygold_e;
typedef std::unique_ptr<DiGraph<int>> u_ptr;

typedef std::map<std::string, std::set<int>> AuthorMap;
typedef std::map<int, std::string> PostMap;

namespace Label {
    static const std::string Node  = "HasStance";
    static const std::string Edge  = "Disagree";
    static const std::string Other = "other";

    static std::string Liberal(std::string issue) {
        if (issue == "abortion")
            return "pro-choice";
        if (issue == "evolution")
            return "pro-evolution";
        if (issue == "gay_marriage")
            return "pro-legal";
        if (issue == "gun_control")
            return "pro-strict-control";
        else
            throw std::invalid_argument("invalid issue: " + issue);
    }

    static std::string Conservative(std::string issue) {
        if (issue == "abortion")
            return "pro-life";
        if (issue == "evolution")
            return "pro-intelligent-design";
        if (issue == "gay_marriage")
            return "con-legal";
        if (issue == "gun_control")
            return "con-strict-control";
        else
            throw std::invalid_argument("invalid issue: " + issue);
    }
}

namespace EdgeLabel {
    static const std::string Agree     = "agree";
    static const std::string Disagree  = "disagree";
}

class RandomizedInferencer {
    pynodes p_nodes;
    pyedges p_edges;

    pygold_n gold_nodes;
    pygold_e gold_edges;
    u_ptr gold_tree;

    bool is_train;
    int restarts;
    int arg0;

    std::string issue;
    bool author_constraints;
    bool local_init;
    bool no_constraints;

    AuthorMap author_mapping;
    PostMap post_to_author;

    u_ptr prediction;

    std::uniform_real_distribution<double> r_dist;
    std::default_random_engine re;

    public:
        RandomizedInferencer(pynodes nodes, pyedges edges, int restarts_, bool is_train_, int arg0_, pygold_n g_nodes, pygold_e g_edges, std::string issue_, bool author_constraints_, AuthorMap author_mapping_, PostMap post_to_author_, bool local_init_, bool no_constraints_);

        void optimize();
        std::vector<std::vector<std::string>> evaluate();
        std::vector<std::string> get_predictions();

    private:
        u_ptr init_graph() const;
        u_ptr build_gold_tree();
        void label_graph(u_ptr& graph);

        std::string get_local_optimal_stance(const Node<int>& node) const;
        double get_node_score(const Node<int>& node, std::string& label) const;
        double score_graph(const u_ptr& graph) const;
        double hamming_distance(const u_ptr& graph, const u_ptr& gold) const;
        u_ptr improve_graph(const u_ptr& graph);
        void adjust_edges(u_ptr& graph, const Node<int>& node, const std::string new_arg);

        std::string draw_random_label();
        std::string draw_uniform_random_label(const std::string old_label);
        void seed_random_generators();
};

#endif