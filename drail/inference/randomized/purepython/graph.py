import networkx as nx
import matplotlib.pyplot as plt
import logging

class DiGraph(object):
    def __init__(self):
        self.nodes = {}
        self.edges = {}
        self.logger = logging.getLogger(self.__class__.__name__)

    def add_node(self, node, **kwargs):
        if node in self.nodes:
            raise Exception('node does already exist')

        self.nodes[node] = kwargs

    def add_edge(self, e1, e2, **kwargs):
        if (e1, e2) in self.edges:
            raise Exception('edge does already exist')

        if e1 not in self.nodes:
            self.add_node(e1)
        if e2 not in self.nodes:
            self.add_node(e2)
            
        self.edges[(e1, e2)] = kwargs

    def has_node(self, node):
        return True if node in self.nodes else False

    def has_edge(self, e1, e2):
        return True if (e1, e2) in self.edges else False

    def remove_edge(self, e1, e2):
        del self.edges[(e1, e2)]

    def remove_node(self, node):
        del self.nodes[node]
        to_keep = {}

        # also remove linked edges
        for (e1, e2), attrs in self.edges.items():
            if not (node == e1 or node == e2):
                to_keep[(e1, e2)] = attrs

        self.edges = to_keep

    def predecessors(self, node):
        return [e1 for (e1, e2) in self.edges if node == e2]

    def successors(self, node):
        for (e1, e2) in self.edges:
            if node == e1:
                yield e2

    def all_neighbors(self, node):
        for (e1, e2) in self.edges:
            if node == e1:
                yield e2
            elif node == e2:
                yield e1

    def show(self):
        self.logger.info('==========\nnodes: {}'.format(self.nodes))
        self.logger.info('----------\nedges: {}'.format(self.edges))
        self.logger.info('==========')

    def draw_graph(self):
        nx_graph = nx.DiGraph()

        for node in self.nodes:
            nx_graph.add_node(node)

        for (e1, e2) in self.edges:
            nx_graph.add_edge(e1, e2)

        nx.draw(nx_graph, with_labels=True)
        self.show()
        plt.show()

    def copy(self):
        g = DiGraph()

        for node, attributes in self.nodes.items():
            copied_attributes = {}
            for key, value in attributes.items():
                copied_attributes[key] = value
            
            g.nodes[node] = copied_attributes

        for edge, attributes in self.edges.items():
            copied_attributes = {}
            for key, value in attributes.items():
                copied_attributes[key] = value

            g.edges[edge] = copied_attributes

        return g

    def sum_node_attribute(self, attr_name):
        return sum([attr[attr_name] for (_, attr) in self.nodes.items()])

    def sum_edge_attribute(self, attr_name):
        return sum([attr[attr_name] for (_, attr) in self.edges.items()])

    def varying_node_attrs(self, graph, attr_name):
        return sum([1 for node, attr in self.nodes.items() if attr[attr_name] != graph.nodes[node][attr_name]])

    def varying_edges(self, graph):
        return sum([1 for edge in self.edges if edge not in graph.edges])

    def get_grandparents(self):
        grandparents = []

        for (e1, e2) in self.edges:
            for (e3, e4) in self.edges:
                if e2 == e3:
                    grandparents.append((e4, e3, e1))

        return grandparents

    def get_coparents(self):
        co_parents = []

        for (e1, e2) in self.edges:
            for (e3, e4) in self.edges:
                if e2 == e4 and int(e1[1:]) < int(e3[1:]):
                    co_parents.append((e2, e1, e3))

        return co_parents
