import random

from .randomized_inferencer import RandomizedInferencer
from .graph import DiGraph

class RInf4Forums(RandomizedInferencer):

    def optimize(self):
        REPETITIONS  = self.configs['repetitions']
        global_score = float('-inf')

        for _ in range(REPETITIONS):
            graph = self.init_graph()
            improvement = True

            self.label_graph(graph)
            score = self.score_graph(graph, self.gold_trees)

            while improvement:
                local_graph = self.improve_graph(graph)
                local_score = self.score_graph(local_graph, self.gold_trees)

                if local_score > score:
                    score = local_score
                    graph = local_graph
                else:
                    improvement = False

            if score > global_score:
                best_graph = graph
                global_score = score

        self.predictions = best_graph

    def improve_graph(self, graph):
        # modify random node and check if the tree improves
        node = random.choice(graph.nodes.keys())
        arg  = graph.nodes[node]['arg']

        new_arg = random.choice([Component.Conservative(self.issue), Component.Other]) if arg == Component.Liberal(self.issue) else \
                  random.choice([Component.Liberal(self.issue), Component.Other]) if arg == Component.Conservative(self.issue) else \
                  random.choice([Component.Liberal(self.issue), Component.Conservative(self.issue)])

        graph.nodes[node]['arg']   = new_arg
        graph.nodes[node]['score'] = self.get_node_score(node, new_arg)

        if self.author_constraints:
            affected_author = self.post_to_author[node]

            # find other posts having the same author,
            # change there the stance as well and adjust edge labels
            for r_node in graph.nodes:
                if self.post_to_author[r_node] == affected_author:
                    graph.nodes[r_node]['arg']   = new_arg
                    graph.nodes[r_node]['score'] = self.get_node_score(r_node, new_arg)

                    for successor in graph.successors(r_node):
                        e_type, score = self.get_edge_type_and_score(graph, r_node, successor)
                        
                        graph.edges[(r_node, successor)]['arg']   = e_type
                        graph.edges[(r_node, successor)]['score'] = score

                    for predecessor in graph.predecessors(r_node):
                        e_type, score = self.get_edge_type_and_score(graph, predecessor, r_node)
                        
                        graph.edges[(predecessor, r_node)]['arg']   = e_type
                        graph.edges[(predecessor, r_node)]['score'] = score
            
        # only adjust edges of successing and predecessing nodes
        else:
            for successor in graph.successors(node):
                e_type, score = self.get_edge_type_and_score(graph, node, successor)
                
                graph.edges[(node, successor)]['arg']   = e_type
                graph.edges[(node, successor)]['score'] = score

            for predecessor in graph.predecessors(node):
                e_type, score = self.get_edge_type_and_score(graph, predecessor, node)
                
                graph.edges[(predecessor, node)]['arg']   = e_type
                graph.edges[(predecessor, node)]['score'] = score

        return graph
    
    def get_edge_type_and_score(self, graph, n1, n2):
        if (not self.no_constraints and graph.nodes[n1]['arg'] == graph.nodes[n2]['arg']) or (self.no_constraints and random.random() < 0.5):
            return EdgeType.Agreement, 1 - self.edges[(n1, n2)]
        else:
            return EdgeType.Disagreement, self.edges[(n1, n2)]

    def get_node_score(self, node, component):
        return [d for d in self.nodes[node] if d['arg'] == component][0]['score']

    def label_graph(self, graph):
        author_to_stance = {}

        for node in graph.nodes:
            if self.issue == 'abortion':
                distribution = [0.55, 0.42, 0.03]
            elif self.issue == 'evolution':
                distribution = [0.63, 0.35, 0.02]
            elif self.issue == 'gay_marriage':
                distribution = [0.64, 0.34, 0.02]
            elif self.issue == 'gun_control':
                distribution = [0.30, 0.66, 0.04]

            if self.author_constraints:
                author = self.post_to_author[node]

                if author in author_to_stance:
                    component = author_to_stance[author]
                else:
                    if self.local_init:
                        component = max(self.nodes[node], key=lambda x: x['score'])['arg']
                    else:
                        component = self.draw_randomly([Component.Liberal(self.issue), Component.Conservative(self.issue), Component.Other], distribution)

                    author_to_stance[author] = component
            else:
                if self.local_init:
                    component = max(self.nodes[node], key=lambda x: x['score'])['arg']
                else:
                    component = self.draw_randomly([Component.Liberal(self.issue), Component.Conservative(self.issue), Component.Other], distribution)

            score = self.get_node_score(node, component)

            graph.nodes[node]['arg']   = component
            graph.nodes[node]['score'] = score

        for (e1, e2), attrs in graph.edges.items():
            attrs['arg'], attrs['score'] = self.get_edge_type_and_score(graph, e1, e2)
     
    def hamming_distance(self, g1, g2):
        # returns normalized hamming distance
        n_nodes = len(g2.nodes)
        dist    = g2.varying_node_attrs(g1, attr_name='arg')

        return dist / float(n_nodes)

    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False):
        metrics = {LabelName.Node: {'gold_data': [], 'pred_data': []}, \
                   LabelName.Edge: {'gold_data': [], 'pred_data': []}}

        for node in self.gold_trees.nodes:
            metrics[LabelName.Node]['gold_data'].append(self.gold_trees.nodes[node]['arg'])
            metrics[LabelName.Node]['pred_data'].append(self.predictions.nodes[node]['arg'])

        for edge in self.gold_trees.edges:
            gold = 1 if self.gold_trees.edges[edge]['arg']  == EdgeType.Disagreement else 0
            pred = 1 if self.predictions.edges[edge]['arg'] == EdgeType.Disagreement else 0

            metrics[LabelName.Edge]['gold_data'].append(gold)
            metrics[LabelName.Edge]['pred_data'].append(pred)
        
        active_heads = None
        if get_active_heads:
            active_heads = self.get_predictions()

        return metrics, active_heads

    def get_predictions(self):
        predictions = set([])

        for node, attrs in self.predictions.nodes.items():
            predictions.add(self.build_head(LabelName.Node, [str(self.arg0), str(node), attrs['arg']]))

            for successor in self.predictions.successors(node):
                if self.predictions.edges[(node, successor)]['arg'] == EdgeType.Disagreement:
                    predictions.add(self.build_head(LabelName.Edge, [str(self.arg0), str(node), str(successor)]))

        return predictions

    def separate_node_and_edges(self):
        self.nodes, self.edges = {}, {}

        for rule in self.ruleGroundings:
            if rule.get_head_predicate()['name'] == LabelName.Edge:
                args  = rule.get_head_predicate()['arguments']
                score = self.ruleGroundings[rule]

                self.edges[(args[1], args[2])] = score

            elif rule.get_head_predicate()['name'] == LabelName.Node:
                args  = rule.get_head_predicate()['arguments']
                score = self.ruleGroundings[rule]

                self.insert_into(self.nodes, args[1], {'arg': args[2], 'score': score})

    def init_graph(self):
        graph = DiGraph()

        for node in self.nodes:
            graph.add_node(node)
        
        for e1, e2 in self.edges:
            graph.add_edge(e1, e2)

        return graph

    def build_gold_trees(self):
        graph = DiGraph()

        for head in self.gold_heads:
            arg1 = head['arguments'][1]
            arg2 = head['arguments'][2]

            if head['name'] == LabelName.Node:
                graph.add_node(arg1, arg=arg2)
            elif head['name'] == LabelName.Edge:
                graph.add_edge(arg1, arg2, arg=EdgeType.Disagreement)

        for e1, e2 in self.edges:
            if not graph.has_edge(e1, e2):
                graph.add_edge(e1, e2, arg=EdgeType.Agreement)

        self.gold_trees = graph

    def score_graph(self, graph, gold_tree=None):
        node_score = graph.sum_node_attribute('score')
        edge_score = graph.sum_edge_attribute('score')

        total_score = node_score + edge_score

        if self.is_train:
            # scale-up inverse hamming distance w.r.t. score
            hamming_d = self.hamming_distance(graph, gold_tree)

            if total_score >= 0:
                total_score += total_score * (1 - hamming_d)
            else:
                total_score -= total_score * (1 - hamming_d)

        return total_score

    def create_author_mapping(self):
        # create a mapping from authors to their post_ids
        for c in self.constraintGroundings:
            if c.has_body_predicate('IsAuthor'):
                for ac in c.get_body_predicates('IsAuthor'):
                    post_id, author = ac['arguments']

                    if author not in self.author_mapping:
                        self.author_mapping[author] = set()

                    self.author_mapping[author].add(post_id)
                    self.post_to_author[post_id] = author

class LabelName(object):
    Node, Edge, EdgeLabel = 'HasStance', 'Disagree', None

class Component(object):
    @staticmethod
    def Liberal(issue):
        if issue == 'abortion':
            return u'pro-choice'
        if issue == 'evolution':
            return u'pro-evolution'
        if issue == 'gay_marriage':
            return u'pro-legal'
        if issue == 'gun_control':
            return u'pro-strict-control'

    @staticmethod
    def Conservative(issue):
        if issue == 'abortion':
            return u'pro-life'
        if issue == 'evolution':
            return u'pro-intelligent-design'
        if issue == 'gay_marriage':
            return u'con-legal'
        if issue == 'gun_control':
            return u'con-strict-control'

    Other = u'other'

class EdgeType(object):
    Agreement, Disagreement = u'agree', u'disagree'

    @staticmethod
    def flip_edge(edge):
        return EdgeType.Agreement     if edge == EdgeType.Disagreement else \
               EdgeType.Disagreement  if edge == EdgeType.Agreement else None
