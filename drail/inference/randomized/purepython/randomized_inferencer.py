import random

from ...inferencer_base import InferencerBase
from .graph import DiGraph

class RandomizedInferencer(InferencerBase):

    def __init__(self, ruleGroundings, constraintGroundings, configs, gold_heads=[], is_train=False, issue=None, author_constraints=None, local_init=False, no_constraints=False):
        super(RandomizedInferencer, self).__init__(ruleGroundings, constraintGroundings, None, None)

        self.predictions  = {}
        self.pred_stances = {}
        self.gold_trees   = {}
        self.gold_heads   = gold_heads
        self.is_train     = is_train
        self.configs      = configs
        self.issue        = issue
        self.local_init   = local_init

        self.no_constraints     = no_constraints
        self.author_constraints = author_constraints if not no_constraints else False
        self.author_mapping     = {}
        self.post_to_author     = {}

    def encode(self):
        self.separate_node_and_edges()
        self.build_gold_trees()

        if self.author_constraints:
            self.create_author_mapping()

        self.arg0 = next(iter(self.ruleGroundings.keys())).head['arguments'][0]

    def build_head(self, name, args):
        return name + '(' + ','.join(args) + ')'

    def draw_randomly(self, labels, distribution):
        assert len(labels) == len(distribution), 'labels must have the same length as distribution'

        r_var  = random.random()
        zipped = zip(labels, distribution)
        t_dist = 0.0

        zipped.sort(key=lambda x: x[1])

        for label, dist in zipped:
            t_dist += dist
            if r_var < t_dist:
                return label

        return zipped[-1][0]

    def insert_into(self, dictionary, key, element):
        if key in dictionary:
            dictionary[key].append(element)
        else: 
            dictionary[key] = [element]
