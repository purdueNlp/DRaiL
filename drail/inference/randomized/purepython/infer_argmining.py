import random

from .randomized_inferencer import RandomizedInferencer
from .graph import DiGraph

class RInfArgMining(RandomizedInferencer):

    def optimize(self):
        REPETITIONS  = self.configs['repetitions']
        global_score = 0.0

        for _ in range(REPETITIONS):
            local_score, local_graphs   = 0.0, {}
            major_claim_found, last_par = False, max(self.pars)

            for par in self.pars:
                # introduction or conclusion
                if par == 1 or par == last_par:
                    unique_nodes = list(set([n['node'] for n in self.nodes[par]]))

                    # node can only be MajorClaim
                    if len(unique_nodes) == 1:
                        graph = self.single_node_graph(par)
                        major_claim_found = True

                    # might be the case that only a single MajorClaim in the essay
                    else:
                        # MajorClaim is more often in last paragraph, also force to have at least one MC
                        has_major_claim   = True if not major_claim_found and par == last_par else \
                                            True if random.random() < 0.8 and par == last_par else \
                                            True if random.random() < 0.2 and par == 1 else False

                        major_claim_found = True if has_major_claim else major_claim_found

                        graph, score = self.randomized_hill_climbing(par, has_major_claim)
                        local_score += score

                # body paragraph
                else:
                    graph, score = self.randomized_hill_climbing(par)
                    local_score += score

                local_graphs[par] = graph

            # after determining nodes and edges, label edges and compute overall score
            graphs, score = self.randomized_edge_labeling(local_graphs, local_score)

            if score > global_score:
                self.predictions = graphs
                global_score = score

    def single_node_graph(self, par):
        node = [n for n in self.nodes[par] if n['arg'] == Component.MajorClaim][0]

        graph = DiGraph()
        graph.add_node(node['node'], arg=node['arg'], score=node['score'])

        return graph

    def randomized_hill_climbing(self, par, has_major_claim=False):
        graph = self.init_random_graph(par)
        improvement = True

        self.label_graph(graph, par, 'phantom', 0, has_major_claim)
        self.fix_edge_scores(graph, has_major_claim)

        bfs_list = self.bfs_node_list(['phantom'], graph)
        score    = self.score_graph(graph, self.gold_trees[par])

        while improvement:  
            for i, node in enumerate(bfs_list):
                adapted_graphs = []

                for predecessor in reversed(bfs_list[:i]):
                    if not graph.has_edge(node, predecessor):
                        new_graph = graph.copy()
                        successor = next(graph.successors(node))

                        edge_score = [e for e in self.edges[par] \
                                    if e['from'] == node and e['to'] == predecessor][0]['score'] \
                                    if predecessor != 'phantom' else 0.0

                        new_graph.remove_edge(node, successor)
                        new_graph.add_edge(node, predecessor, score=edge_score)

                        self.label_graph(new_graph, par, 'phantom', 0, has_major_claim)
                        self.fix_edge_scores(new_graph, has_major_claim)

                        adapted_graphs.append((new_graph, self.score_graph(new_graph, self.gold_trees[par])))

                if adapted_graphs:
                    local_optimum = max(adapted_graphs, key=lambda x: x[1])

                    if local_optimum[1] > score:
                        graph = local_optimum[0]
                        score = local_optimum[1]
                    else:
                        improvement = False

        return graph, score

    def randomized_edge_labeling(self, graphs, local_score):
        if self.stances:
            stance_scores = {}
            major_claims  = self.get_major_claims(graphs)

            if not self.no_constraints:
                assert len(major_claims) > 0, 'no MajorClaim in essay initialized'

            # random stance initialization
            for par in graphs:
                for _, attr in graphs[par].nodes.items():
                    if attr['arg'] == Component.Claim or attr['arg'] == Component.Premise:
                        attr['stance'] = StanceType.get_random_stance()
                    elif attr['arg'] == Component.MajorClaim:
                        attr['stance'] = StanceType.HasNone

                stance_scores[par] = self.score_stances(graphs[par], par, major_claims)

            # try to improve locally
            for par in graphs:
                for _, attr in graphs[par].nodes.items():
                    if attr['arg'] == Component.Claim or attr['arg'] == Component.Premise:
                        attr['stance'] = StanceType.flip_stance(attr['stance'])
                        current_score  = self.score_stances(graphs[par], par, major_claims)

                        if current_score > stance_scores[par]:
                            stance_scores[par] = current_score
                        else:
                            attr['stance'] = StanceType.flip_stance(attr['stance'])

                local_score += stance_scores[par]

        return graphs, local_score

    def bfs_node_list(self, predecessors, graph):
        children = []

        for node in predecessors:
            children.extend(self.bfs_node_list(graph.predecessors(node), graph))

        return predecessors + children if predecessors else []
    
    def fix_edge_scores(self, graph, has_major_claim):
        # edges to MajorClaims do not exist, therefore the
        # score of edges to MCs will be inverted (1 - score)
        if has_major_claim:
            mc_nodes = []

            for node, attributes in graph.nodes.items():
                if attributes['arg'] == Component.MajorClaim:
                    mc_nodes.append(node)

            for (_, e2), attributes in graph.edges.items():
                if e2 in mc_nodes:
                    attributes['score'] = 1 - attributes['score']

    def label_graph(self, graph, par, root, level, has_major_claim):
        if self.no_constraints:
            arg = random.choice([Component.MajorClaim, Component.Claim, Component.Premise])
        elif has_major_claim:
            arg = Component.MajorClaim if level == 0 else \
                  Component.Claim if level == 1 else Component.Premise
        else:
            arg = Component.Claim if level == 0 else Component.Premise
        
        for pre in graph.predecessors(root):
            score = [n['score'] for n in self.nodes[par] if n['node'] == pre and n['arg'] == arg][0]

            graph.nodes[pre]['score'] = score
            graph.nodes[pre]['arg']   = arg

            # label recursively
            self.label_graph(graph, par, pre, level + 1, has_major_claim)

    def score_stances(self, graph, par, major_claims):
        score, dist = 0.0, 0

        for node, attr in graph.nodes.items():
            if node != 'phantom':
                score += self.get_stance_score(node, par, attr['stance'])

                # naively count hamming distance
                if self.is_train:
                    cand_stance = attr['stance']
                    gold_stance = self.gold_trees[par].nodes[node]['stance']

                    if cand_stance != gold_stance:
                        dist += 1

                    norm_hd = float(dist) / len(self.gold_trees[par].nodes)
                    score  += 1 - norm_hd

        return score

    def get_stance_score(self, node, par, stance):
        for d in self.stances[par]:
            if d['node'] == node and d['stance'] == stance:
                return d['score']
            
    def hamming_distance(self, g1, g2):
        # returns normalized hamming distance
        n_nodes = len(g2.nodes)
        dist    = g2.varying_node_attrs(g1, attr_name='arg') + g2.varying_edges(g1)

        return dist / float(n_nodes + n_nodes - 1)

    def get_gold_node(self, gold_heads, node):
        for head in gold_heads:
            if head['name'] == LabelName.Node and head['arguments'][1] == node:
                return head

    def has_gold_edge(self, gold_heads, e1, e2):
        for head in gold_heads:
            if head['name'] == LabelName.Edge and \
               head['arguments'][1] == e1 and head['arguments'][2] == e2:
               return True

        return False

    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False):
        metrics = {LabelName.Node: {'gold_data': [], 'pred_data': []}, \
                   LabelName.Edge: {'gold_data': [], 'pred_data': []}}

        gold_grandparents = []
        gold_co_parents   = []

        pred_grandparents = []
        pred_co_parents   = []

        if self.stances:
            metrics[LabelName.EdgeLabel] = {'gold_data': [], 'pred_data': []}
                
        for par in self.pars:
            graph = self.predictions[par]
            gold_tree = self.gold_trees[par]

            if graph.has_node('phantom'):
                graph.remove_node('phantom')

            for node, attrs in graph.nodes.items():
                gold = self.get_gold_node(gold_heads, node)

                metrics[LabelName.Node]['pred_data'].append(attrs['arg'])
                metrics[LabelName.Node]['gold_data'].append(gold['arguments'][gold['target_pos']])

                if self.stances:
                    metrics[LabelName.EdgeLabel]['pred_data'].append(attrs['stance'])
                    metrics[LabelName.EdgeLabel]['gold_data'].append(gold_tree.nodes[node]['stance'])

            for edge in self.edges.get(par, []) + self.loops.get(par, []):
                e1, e2 = edge['from'], edge['to']

                metrics[LabelName.Edge]['pred_data'].append(int(graph.has_edge(e1, e2)) if graph.nodes[e2]['arg'] != Component.MajorClaim else 0)
                metrics[LabelName.Edge]['gold_data'].append(int(self.has_gold_edge(gold_heads, e1, e2)))

            if self.grandparents:
                gold_grandparents.extend(gold_tree.get_grandparents())
                pred_grandparents.extend(graph.get_grandparents())

            if self.coparents:
                gold_co_parents.extend(gold_tree.get_coparents())
                pred_co_parents.extend(graph.get_coparents())

        if self.grandparents:
            metrics[LabelName.Grandparent] = {'gold_data': [], 'pred_data': []}

            for grandparent in self.grandparents:
                metrics[LabelName.Grandparent]['pred_data'].append(1 if grandparent in pred_grandparents else 0)
                metrics[LabelName.Grandparent]['gold_data'].append(1 if grandparent in gold_grandparents else 0)

        if self.coparents:
            metrics[LabelName.CoParent] = {'gold_data': [], 'pred_data': []}

            for coparent in self.coparents:
                metrics[LabelName.CoParent]['pred_data'].append(1 if coparent in pred_co_parents else 0)
                metrics[LabelName.CoParent]['gold_data'].append(1 if coparent in gold_co_parents else 0)

        active_heads = None
        if get_active_heads:
            active_heads = self.get_predictions()

        return metrics, active_heads

    def get_predictions(self):
        predictions = set([])

        for par in self.pars:
            graph = self.predictions[par]

            if graph.has_node('phantom'):
                graph.remove_node('phantom')

            for node in graph.nodes:
                predictions.add(self.build_head(LabelName.Node, [self.arg0, node, graph.nodes[node]['arg']]))

                if self.stances:
                    predictions.add(self.build_head(LabelName.EdgeLabel, [self.arg0, node, graph.nodes[node]['stance']]))

            for (e1, e2) in graph.edges:
                if graph.nodes[e2]['arg'] != Component.MajorClaim:
                    predictions.add(self.build_head(LabelName.Edge, [self.arg0, e1, e2]))

            if self.grandparents:
                for (gp, p, child) in graph.get_grandparents():
                    predictions.add(self.build_head(LabelName.Grandparent, [self.arg0, gp, p, child]))

            if self.coparents:
                for (cp, c1, c2) in graph.get_coparents():
                    predictions.add(self.build_head(LabelName.CoParent, [self.arg0, cp, c1, c2]))

        return predictions

    def separate_node_and_edges(self):
        self.nodes, self.edges   = {}, {} 
        self.loops, self.stances = {}, {}
        self.grandparents, self.coparents = {}, {}
        self.pars = set()

        for rule in self.ruleGroundings:
            coeffi = self.ruleGroundings[rule]
            par1, par2 = self.get_paragraphs(rule)

            if rule.get_head_predicate()['name'] == LabelName.Edge:
                if not self.has_loop(rule):
                    self.insert_into(self.edges, par1, self.get_compact_representation(rule, node=False))
                else:
                    self.insert_into(self.loops, par1, self.get_compact_representation(rule, node=False))

            elif rule.get_head_predicate()['name'] == LabelName.Node:
                self.insert_into(self.nodes, par1, self.get_compact_representation(rule, node=True))

            elif rule.get_head_predicate()['name'] == LabelName.EdgeLabel:
                self.insert_into(self.stances, par1, self.get_compact_representation(rule, node=False, stance=True))

            elif rule.get_head_predicate()['name'] == LabelName.Grandparent:
                args = rule.get_head_predicate()['arguments']
                self.grandparents[(args[1], args[2], args[3])] = self.ruleGroundings[rule]

            elif rule.get_head_predicate()['name'] == LabelName.CoParent:
                args = rule.get_head_predicate()['arguments']
                self.coparents[(args[1], args[2], args[3])] = self.ruleGroundings[rule]

            self.pars.add(par1)

    def get_compact_representation(self, rule, node, stance=False):
        args  = rule.get_head_predicate()['arguments']
        score = self.ruleGroundings[rule]
        
        return {'node': args[1], 'arg': args[2], 'score': score} if node \
          else {'from': args[1], 'to':  args[2], 'score': score} if not stance \
          else {'node': args[1], 'stance':  args[2], 'score': score}   

    def get_paragraphs(self, rule):
        present_pars = [pred['arguments'][2] for pred in rule.get_body_predicates('InPar')]

        return (present_pars[0], present_pars[1]) if len(present_pars) > 1 else (present_pars[0], None)

    def has_loop(self, rule):
        components = [pred['arguments'][1] for pred in rule.get_body_predicates('HasComponent')]
        
        return True if components.count(components[0]) > 1 else False

    def has_stance(self, rule):
        return True if rule.get_head_predicate()['arguments'][3] != StanceType.HasNone else False

    def init_random_graph(self, par):
        graph = DiGraph()
        edges = self.edges.get(par, [])
        edge  = random.choice(edges)

        # add phantom node - allows more than one Claim / MajorClaim
        graph.add_node('phantom', arg='root', score=0.0)
        graph.add_edge(edge['to'], 'phantom', score=0.0)
        graph.add_edge(edge['from'], edge['to'], score=edge['score'])

        edges = [e for e in edges if e['from'] not in graph.nodes]

        while edges:
            edge = random.choice([e for e in edges if e['to'] in graph.nodes])
            graph.add_edge(edge['from'], edge['to'], score=edge['score'])

            edges = [e for e in edges if e['from'] not in graph.nodes]

        return graph

    def build_gold_trees(self):
        for par in self.nodes:
            graph = DiGraph()
            unique_nodes = list(set([n['node'] for n in self.nodes[par]]))

            for head in self.gold_heads:
                arg1 = head['arguments'][1]
                arg2 = head['arguments'][2]

                if head['name'] == LabelName.Node and arg1 in unique_nodes:
                    graph.add_node(arg1, arg=arg2)

                elif head['name'] == LabelName.Edge and arg1 in unique_nodes:
                    graph.add_edge(arg1, arg2)

                elif head['name'] == LabelName.EdgeLabel and arg1 in unique_nodes:
                    graph.nodes[arg1]['stance'] = arg2

            self.gold_trees[par] = graph

    def score_graph(self, graph, gold_tree=None):
        node_score = graph.sum_node_attribute('score')
        edge_score = graph.sum_edge_attribute('score')

        total_score = node_score + edge_score

        if self.grandparents:
            for grandparent in graph.get_grandparents():
                if grandparent[0] != 'phantom':
                    total_score += self.grandparents[grandparent]

        if self.coparents:
            for coparent in graph.get_coparents():
                if coparent[0] != 'phantom':
                    total_score += self.coparents[coparent]

        if self.is_train:
            # scale-up inverse hamming distance w.r.t. score
            hamming_d = self.hamming_distance(graph, gold_tree)
            total_score += total_score * (1 - hamming_d)

        return total_score

    def build_predicted_stances(self):
        if not self.pred_stances:
            major_claims = self.get_major_claims(self.predictions)

            for par in self.pars:
                graph = self.predictions[par]

                for node, attrs in graph.nodes.items():
                    if attrs['arg'] == Component.Claim:
                        for m_node, _ in major_claims:
                            self.pred_stances[(node, m_node)] = attrs.get('stance', StanceType.HasNone)

                    elif attrs['arg'] == Component.Premise:
                        self.pred_stances[(node, next(graph.successors(node)))] = attrs.get('stance', StanceType.HasNone)

    def get_major_claims(self, graphs):
        return [(n, par) for par in graphs for n, attr in graphs[par].nodes.items() if attr['arg'] == Component.MajorClaim]

class LabelName(object):
    Node, Edge, EdgeLabel, Grandparent, CoParent = 'IsArgType', 'Reln', 'Stance', 'Grandparent', 'CoParent'

class Component(object):
    Premise, Claim, MajorClaim = u'Premise', u'Claim', u'MajorClaim'

class StanceType(object):
    Attack, Support, HasNone = u'Attack', u'Support', u'None'

    @staticmethod
    def get_random_stance():
        return StanceType.Support if random.random() < 0.9 else StanceType.Attack

    @staticmethod
    def flip_stance(stance):
        return StanceType.Support if stance == StanceType.Attack else \
               StanceType.Attack  if stance == StanceType.Support else None
