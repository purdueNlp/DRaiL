import random

from ..inferencer_base import InferencerBase
from core_4forums import RandomizedInferencer


class RInf4Forums(InferencerBase):
    """ Runs the C++ implementation of the randomized
        inferencer for the 4forums problem
        Use the Makefile in 'drail/inference/randomized'
        to compile the C++ model depending on your OS
    """

    def __init__(self, ruleGroundings, constraintGroundings, configs, gold_heads=[], is_train=False, issue=None, author_constraints=None, local_init=False, no_constraints=False):
        super(RInf4Forums, self).__init__(ruleGroundings, constraintGroundings, None, None)

        self.gold_heads = gold_heads
        self.is_train   = is_train
        self.configs    = configs
        self.issue      = issue
        self.local_init = local_init

        self.author_constraints = author_constraints
        self.no_constraints     = no_constraints
        self.author_mapping     = {}
        self.post_to_author     = {}

        random.seed(1234)

    def encode(self):
        self.separate_node_and_edges()

        restarts = self.configs['repetitions']
        arg0     = next(self.ruleGroundings.iterkeys()).head['arguments'][0]

        gold_nodes, gold_edges = self.convert_heads()

        if self.author_constraints:
            self.create_author_mapping()

        # create the C++ model
        self.inferencer = RandomizedInferencer(self.nodes, self.edges, restarts, self.is_train, arg0, gold_nodes, gold_edges, \
                                               self.issue, self.author_constraints, self.author_mapping, self.post_to_author, \
                                               self.local_init, self.no_constraints)

    def optimize(self):
        self.inferencer.optimize()

    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False):
        metrics = {LabelName.Node: {'gold_data': [], 'pred_data': []}, \
                   LabelName.Edge: {'gold_data': [], 'pred_data': []}}

        result = self.inferencer.evaluate()

        metrics[LabelName.Node]['gold_data'] = result[0]
        metrics[LabelName.Node]['pred_data'] = result[1]

        metrics[LabelName.Edge]['gold_data'] = [1 if res == 'disagree' else 0 for res in result[2]]
        metrics[LabelName.Edge]['pred_data'] = [1 if res == 'disagree' else 0 for res in result[3]]

        active_heads = None
        if get_active_heads:
            active_heads = self.get_predictions()

        return metrics, active_heads

    def get_predictions(self):
        return set(self.inferencer.get_predictions())

    def convert_heads(self):
        node_heads = []
        edge_heads = []

        for head in self.gold_heads:
            if head['name'] == LabelName.Node:
                node_heads.append((head['arguments'][1], head['arguments'][2]))
            elif head['name'] == LabelName.Edge:
                edge_heads.append((head['arguments'][1], head['arguments'][2]))

        return node_heads, edge_heads

    def separate_node_and_edges(self):
        self.nodes, self.edges = {}, {}

        for rule in self.ruleGroundings:
            if rule.get_head_predicate()['name'] == LabelName.Edge:
                args  = rule.get_head_predicate()['arguments']
                score = self.ruleGroundings[rule]

                self.edges[(args[1], args[2])] = score

            elif rule.get_head_predicate()['name'] == LabelName.Node:
                args  = rule.get_head_predicate()['arguments']
                score = self.ruleGroundings[rule]

                self.insert_into(self.nodes, args[1], (args[2], score))

    def insert_into(self, dictionary, key, element):
        if key in dictionary:
            dictionary[key].append(element)
        else: 
            dictionary[key] = [element]

    def create_author_mapping(self):
        # create a mapping from authors to their post_ids
        for c in self.constraintGroundings:
            if c.has_body_predicate('IsAuthor'):
                for ac in c.get_body_predicates('IsAuthor'):
                    post_id, author = ac['arguments']

                    if author not in self.author_mapping:
                        self.author_mapping[author] = set()

                    self.author_mapping[author].add(post_id)
                    self.post_to_author[post_id] = author

class LabelName(object):
    Node, Edge, = 'HasStance', 'Disagree'
