#ifndef CORE_H
#define CORE_H

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <memory>
#include <random>
#include "di_graph.h"

namespace py = pybind11;

typedef std::map<int, std::map<std::string, std::vector<std::tuple<std::string, double>>>> pynodes;
typedef std::map<int, std::map<std::tuple<std::string, std::string>, double>> pyedges;
typedef std::map<std::tuple<std::string, std::string, std::string>, double> py2ndorder;

typedef std::vector<std::vector<std::string>> pygold;
typedef std::unique_ptr<DiGraph<std::string>> u_ptr;

namespace Label {
    static const std::string Node   = "IsArgType";
    static const std::string Edge   = "Reln";
    static const std::string Stance = "Stance";
    
    static const std::string Grandparent = "Grandparent";
    static const std::string CoParent    = "CoParent";

    static const std::string Premise    = "Premise";
    static const std::string Claim      = "Claim";
    static const std::string MajorClaim = "MajorClaim";
}

namespace StanceType {
    static const std::string Attack  = "Attack";
    static const std::string Support = "Support";
    static const std::string HasNone = "None";
}

class RandomizedInferencer {
    pynodes p_nodes;
    pyedges p_edges;
    pynodes p_stances;
    pyedges p_loops;

    py2ndorder p_grandparents;
    py2ndorder p_co_parents;

    pygold gold_nodes;
    pygold gold_edges;
    pygold gold_stances;

    bool is_train;
    int restarts;
    bool no_constraints;

    std::vector<int> pars;
    std::string arg0;

    std::map<int, u_ptr> gold_trees;
    std::map<int, u_ptr> predictions;

    std::uniform_real_distribution<double> r_dist;
    std::default_random_engine re;

    Node<std::string> phantom = Node<std::string>("phantom");
    std::map<std::tuple<std::string, std::string>, std::string> pred_stances;

    public:
        RandomizedInferencer(pynodes nodes, pyedges edges, pynodes stances, pyedges loops, 
                             py2ndorder grandparents, py2ndorder co_parents, std::vector<int> pars_, 
                             int restarts_, bool is_train_, std::string arg0_, pygold g_nodes, 
                             pygold g_edges, pygold g_stances, bool no_constraints_);
        void optimize();
        std::vector<std::vector<std::string>> evaluate();
        std::vector<std::string> get_predictions();

    private:
        void seed_random_generators();
        void build_gold_trees();
        std::vector<std::string> get_nodes_in_par(int& par) const;

        template <typename T>
        bool contains(std::vector<T>& vec, T element);

        u_ptr single_node_graph(int& par) const;
        double get_node_score(std::string& node_id, int& par, const std::string& label) const;

        u_ptr randomized_hill_climbing(int& par, bool has_major_claim = false);
        u_ptr init_random_graph(int& par);

        void label_graph(u_ptr& graph, int& par, Node<std::string>& root, int level, bool& has_major_claim);
        void fix_edge_scores(u_ptr& graph, bool& has_major_claim);

        std::vector<Node<std::string>> bfs_nodes(std::vector<Node<std::string>> predecessors, u_ptr& graph);
        double score_graph(const u_ptr& graph, const u_ptr& gold_tree) const;
        double hamming_distance(const u_ptr& graph, const u_ptr& gold) const;

        double randomized_edge_labeling(std::map<int, u_ptr>& graphs, double score);
        std::vector<std::tuple<std::string, int>> get_major_claims(std::map<int, u_ptr>& graphs) const;

        std::string get_random_stance();
        std::string flip_stance(std::string stance) const;

        double score_stances(u_ptr& graph, int& par, std::vector<std::tuple<std::string, int>>& major_claims) const;
        double get_stance_score(std::string node, int par, std::string stance) const;
        void build_predicted_stances();
};

#endif