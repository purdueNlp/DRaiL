#include "core_4forums.h"
#include <utility>
#include <chrono>
#include <stdlib.h>
#include <limits>

using infer = RandomizedInferencer;

infer::RandomizedInferencer(pynodes nodes, pyedges edges, int restarts_, bool is_train_, int arg0_, pygold_n g_nodes, pygold_e g_edges, std::string issue_, bool author_constraints_, AuthorMap author_mapping_, PostMap post_to_author_, bool local_init_, bool no_constraints_) {
    p_nodes  = nodes;
    p_edges  = edges;
    restarts = restarts_;
    is_train = is_train_;
    arg0     = arg0_;

    gold_nodes = g_nodes;
    gold_edges = g_edges;
    issue      = issue_;
    local_init = local_init_;

    author_constraints = author_constraints_;
    no_constraints     = no_constraints_;

    author_mapping = author_mapping_;
    post_to_author = post_to_author_;

    if (is_train)
        gold_tree = build_gold_tree();

    r_dist = std::uniform_real_distribution<double>(0, 1);
    seed_random_generators();
}

void infer::optimize() {
    bool improvement;

    double global_score = std::numeric_limits<double>::lowest();
    double local_score  = std::numeric_limits<double>::lowest();
    double best_score   = std::numeric_limits<double>::lowest();

    u_ptr global_graph;

    for (int i = 0; i < restarts; i++) {
        u_ptr best_graph = init_graph();
        improvement = true;

        label_graph(best_graph);
        best_score = score_graph(best_graph);

        while (improvement) {
            auto local_graph = improve_graph(best_graph);
            local_score = score_graph(local_graph);

            if (local_score > best_score) {
                best_score = local_score;
                best_graph = std::move(local_graph);
            }
            else
                improvement = false;
        }

        if (best_score > global_score) {
            global_score = best_score;
            global_graph = std::move(best_graph);
        }
    }

    prediction = std::move(global_graph);
}

u_ptr infer::init_graph() const {
    u_ptr graph = make_unique<DiGraph<int>>();

    for (auto& element : p_nodes)
        graph->add_node(element.first);

    for (auto& element : p_edges)
        graph->add_edge(element.first);

    return graph;
}

u_ptr infer::build_gold_tree() {
    u_ptr graph = make_unique<DiGraph<int>>();

    for (auto& element : gold_nodes)
        graph->add_node(Node<int>(std::get<0>(element), std::get<1>(element)));

    for (auto& element : gold_edges)
        graph->add_edge(Edge<int>(element, EdgeLabel::Disagree));

    for (auto& element : p_edges)
        if (!graph->has_edge(element.first))
            graph->add_edge(Edge<int>(element.first, EdgeLabel::Agree));

    return graph;
}

void infer::label_graph(u_ptr& graph) {
    std::map<std::string, std::string> author_to_stance;

    // label nodes randomly
    for (auto& element : graph->get_nodes()) {
        std::string stance, author; 

        if (author_constraints) {
            author = post_to_author.at(element.first);

            if (author_to_stance.find(author) != author_to_stance.end())
                stance = author_to_stance.at(author);
            else {
                stance = (local_init) ? get_local_optimal_stance(element.second) : draw_random_label();
                author_to_stance.insert({ author, stance });
            }
        }
        else
            stance = (local_init) ? get_local_optimal_stance(element.second) : draw_random_label();

        double score = get_node_score(element.second, stance);

        graph->node(element.first).score = score;
        graph->node(element.first).arg   = stance;
    }

    // label edges depending on nodes
    for (auto& element : graph->get_edges()) {
        double r_var = r_dist(re);

        if ((!no_constraints && graph->node(element.second.from()).arg == graph->node(element.second.to()).arg) || (no_constraints && r_var < 0.5)) {
            graph->edge(element.first).score = 1 - p_edges.at(element.first);
            graph->edge(element.first).arg   = EdgeLabel::Agree;
        }
        else {
            graph->edge(element.first).score = p_edges.at(element.first);
            graph->edge(element.first).arg   = EdgeLabel::Disagree;
        }
    }
}

std::string infer::get_local_optimal_stance(const Node<int>& node) const {
    double max = std::numeric_limits<double>::lowest();
    std::string stance;

    for (auto& element : p_nodes.at(node.id)) {
        if (std::get<1>(element) > max) {
            stance = std::get<0>(element);
            max    = std::get<1>(element);
        }
    }

    return stance;
}

double infer::get_node_score(const Node<int>& node, std::string& label) const {
    for (auto& element : p_nodes.at(node.id))
        if (std::get<0>(element) == label)
            return std::get<1>(element);

    return 0.0;
}

double infer::score_graph(const u_ptr& graph) const {
    double score = graph->sum_node_scores() + graph->sum_edge_scores();

    if (is_train) {
        double hamming_d = hamming_distance(graph, gold_tree);

        if (score >= 0)
            score += score * (1 - hamming_d);
        else
            score -= score * (1 - hamming_d);
    }

    return score;
}

double infer::hamming_distance(const u_ptr& graph, const u_ptr& gold) const {
    // returns normalized hamming distance
    int n_nodes  = gold->get_nodes().size();
    int distance = gold->varying_node_args(graph);

    return distance / static_cast<double>(n_nodes);
}

u_ptr infer::improve_graph(const u_ptr& graph) {
    // modify random node and check if the tree improves
    u_ptr local_graph = make_unique<DiGraph<int>>(*graph);

    std::map<int, Node<int>>::const_iterator rand_iter = graph->get_nodes().begin();
    std::advance(rand_iter, std::rand() % graph->get_nodes().size());

    Node<int> rand_node = rand_iter->second;
    std::string new_arg = draw_uniform_random_label(rand_node.arg);

    local_graph->node(rand_node.id).arg   = new_arg;
    local_graph->node(rand_node.id).score = get_node_score(rand_node, new_arg);

    if (author_constraints) {
        std::string affected_author = post_to_author.at(rand_node.id);

        // find other posts having the same author,
        // change there the stance as well and adjust edge labels
        for (auto& element : local_graph->get_nodes()) {
            auto& current_node = element.second;

            if (post_to_author.at(current_node.id) == affected_author) {
                local_graph->node(current_node.id).arg   = new_arg;
                local_graph->node(current_node.id).score = get_node_score(current_node, new_arg);

                adjust_edges(local_graph, current_node, new_arg);
            }
        }
    }
    // only adjust edges of successing and predecessing nodes
    else
        adjust_edges(local_graph, rand_node, new_arg);

    return local_graph;
}

void infer::adjust_edges(u_ptr& graph, const Node<int>& node, const std::string new_arg) {
    for (auto& successor : graph->successors(node)) {
        auto edge_id = std::make_tuple(node.id, successor.id);
        double r_var = r_dist(re);

        if ((!no_constraints && graph->node(successor.id).arg == new_arg) || (no_constraints && r_var < 0.5)) {
            graph->edge(edge_id).score = 1 - p_edges.at(edge_id);
            graph->edge(edge_id).arg   = EdgeLabel::Agree;
        }
        else {
            graph->edge(edge_id).score = p_edges.at(edge_id);
            graph->edge(edge_id).arg   = EdgeLabel::Disagree;
        }
    }

    for (auto& predecessor : graph->predecessors(node)) {
        auto edge_id = std::make_tuple(predecessor.id, node.id);
        double r_var = r_dist(re);

        if ((!no_constraints && graph->node(predecessor.id).arg == new_arg) || (no_constraints && r_var < 0.5)) {
            graph->edge(edge_id).score = 1 - p_edges.at(edge_id);
            graph->edge(edge_id).arg   = EdgeLabel::Agree;
        }
        else {
            graph->edge(edge_id).score = p_edges.at(edge_id);
            graph->edge(edge_id).arg   = EdgeLabel::Disagree;
        }
    }
}

std::string infer::draw_random_label() {
    double r_var = r_dist(re);

    if (issue == "abortion")
        return (r_var < 0.03) ? Label::Other : (r_var < 0.42) ? Label::Conservative(issue) : Label::Liberal(issue);
    if (issue == "evolution")
        return (r_var < 0.02) ? Label::Other : (r_var < 0.35) ? Label::Conservative(issue) : Label::Liberal(issue);
    if (issue == "gay_marriage")
        return (r_var < 0.02) ? Label::Other : (r_var < 0.34) ? Label::Conservative(issue) : Label::Liberal(issue);
    if (issue == "gun_control")
        return (r_var < 0.04) ? Label::Other : (r_var < 0.66) ? Label::Conservative(issue) : Label::Liberal(issue);
    else 
        throw std::invalid_argument("invalid issue: " + issue);
}

std::string infer::draw_uniform_random_label(const std::string old_label) {
    double r_var = r_dist(re);

    if (old_label == Label::Conservative(issue))
        return (r_var < 0.5) ? Label::Liberal(issue) : Label::Other;
    else if (old_label == Label::Liberal(issue))
        return (r_var < 0.5) ? Label::Conservative(issue) : Label::Other;
    else
        return (r_var < 0.5) ? Label::Conservative(issue) : Label::Liberal(issue);
}

void infer::seed_random_generators() {
    int seed = std::chrono::system_clock::now().time_since_epoch().count();

    re.seed(seed);
    std::srand(seed);
}

std::vector<std::vector<std::string>> infer::evaluate() {
    std::vector<std::vector<std::string>> return_vector;

    std::vector<std::string> node_gold_data;
    std::vector<std::string> node_pred_data;

    std::vector<std::string> edge_gold_data;
    std::vector<std::string> edge_pred_data;

    for (auto& element : gold_nodes) {
        node_gold_data.push_back(std::get<1>(element));
        node_pred_data.push_back(prediction->node(std::get<0>(element)).arg);
    }

    for (auto& element : prediction->get_edges()) {
        edge_pred_data.push_back(element.second.arg);

        if (std::find(gold_edges.begin(), gold_edges.end(), element.first) != gold_edges.end())
            edge_gold_data.push_back(EdgeLabel::Disagree);
        else
            edge_gold_data.push_back(EdgeLabel::Agree);
    }

    // notice that this order is important
    return_vector.push_back(node_gold_data);
    return_vector.push_back(node_pred_data);

    return_vector.push_back(edge_gold_data);
    return_vector.push_back(edge_pred_data);

    return return_vector;
}

std::vector<std::string> infer::get_predictions() {
    std::vector<std::string> return_vector;
    std::string head;

    for (auto& element : prediction->get_nodes()) {
        Node<int> node = element.second;
        head = Label::Node + "(" + std::to_string(arg0) + "," + std::to_string(node.id) + "," + node.arg + ")";

        return_vector.push_back(head);

        for (auto& successor : prediction->successors(node)) {
            auto edge_id = std::make_tuple(node.id, successor.id);

            if (prediction->edge(edge_id).arg == EdgeLabel::Disagree) {
                head = Label::Edge + "(" + std::to_string(arg0) + "," + std::to_string(node.id) + "," + std::to_string(successor.id) + ")";
                return_vector.push_back(head);
            }
        }
    }

    return return_vector;
}

PYBIND11_MODULE(core_4forums, m) {
    py::class_<RandomizedInferencer>(m, "RandomizedInferencer")
        .def(py::init<pynodes, pyedges, int, bool, int, pygold_n, pygold_e, std::string, bool, AuthorMap, PostMap, bool, bool>())
        .def("optimize", &infer::optimize)
        .def("evaluate", &infer::evaluate)
        .def("get_predictions", &infer::get_predictions);
}