import random

from ..inferencer_base import InferencerBase
from .core_argmining import RandomizedInferencer


class RInfArgMining(InferencerBase):
    """ Runs the C++ implementation of the randomized
        inferencer for the argument mining problem
        Use the Makefile in 'drail/inference/randomized'
        to compile the C++ model depending on your OS
    """

    def __init__(self, ruleGroundings, constraintGroundings, configs, gold_heads=[], is_train=False, issue=None, author_constraints=None, local_init=False, no_constraints=False):
        super(RInfArgMining, self).__init__(ruleGroundings, constraintGroundings, None, None)

        self.gold_heads     = gold_heads
        self.is_train       = is_train
        self.configs        = configs
        self.no_constraints = no_constraints

        random.seed(1234)

    def encode(self):
        self.separate_node_and_edges()

        restarts  = self.configs['repetitions']
        arg0      = next(self.ruleGroundings.iterkeys()).head['arguments'][0]
        self.pars = list(self.pars)

        gold_nodes, gold_edges, gold_stances = self.convert_heads()

        # create the C++ model
        self.inferencer = RandomizedInferencer(self.nodes, self.edges, self.stances, self.loops, 
                                               self.grandparents, self.co_parents, self.pars, restarts, 
                                               self.is_train, arg0, gold_nodes, gold_edges, gold_stances, self.no_constraints)

    def optimize(self):
        self.inferencer.optimize()

    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False):
        metrics = {LabelName.Node: {'gold_data': [], 'pred_data': []}, \
                   LabelName.Edge: {'gold_data': [], 'pred_data': []}}

        result = self.inferencer.evaluate()

        metrics[LabelName.Node]['gold_data'] = result[0]
        metrics[LabelName.Node]['pred_data'] = result[1]

        metrics[LabelName.Edge]['gold_data'] = [int(res) for res in result[2]]
        metrics[LabelName.Edge]['pred_data'] = [int(res) for res in result[3]]

        if self.stances:
            metrics[LabelName.EdgeLabel] = {'gold_data': [], 'pred_data': []}

            metrics[LabelName.EdgeLabel]['gold_data'] = result[4]
            metrics[LabelName.EdgeLabel]['pred_data'] = result[5]

        if self.grandparents:
            metrics[LabelName.Grandparent] = {'gold_data': [], 'pred_data': []}

            metrics[LabelName.Grandparent]['gold_data'] = [int(res) for res in result[6]]
            metrics[LabelName.Grandparent]['pred_data'] = [int(res) for res in result[7]]

        if self.co_parents:
            metrics[LabelName.CoParent] = {'gold_data': [], 'pred_data': []}

            metrics[LabelName.CoParent]['gold_data'] = [int(res) for res in result[8]]
            metrics[LabelName.CoParent]['pred_data'] = [int(res) for res in result[9]]

        active_heads = None
        if get_active_heads:
            active_heads = self.get_predictions()

        return metrics, active_heads

    def get_predictions(self):
        return set(self.inferencer.get_predictions())

    def convert_heads(self):
        node_heads   = []
        edge_heads   = []
        stance_heads = []

        for head in self.gold_heads:
            if head['name'] == LabelName.Node:
                node_heads.append([head['arguments'][1], head['arguments'][2]])
            elif head['name'] == LabelName.Edge:
                edge_heads.append([head['arguments'][1], head['arguments'][2]])
            elif head['name'] == LabelName.EdgeLabel:
                stance_heads.append([head['arguments'][1], head['arguments'][2]])

        return node_heads, edge_heads, stance_heads

    def get_paragraphs(self, rule):
        present_pars = [pred['arguments'][2] for pred in rule.get_body_predicates('InPar')]

        return (present_pars[0], present_pars[1]) if len(present_pars) > 1 else (present_pars[0], None)

    def has_loop(self, rule):
        components = [pred['arguments'][1] for pred in rule.get_body_predicates('HasComponent')]
        
        return True if components.count(components[0]) > 1 else False

    def has_stance(self, rule):
        return True if rule.get_head_predicate()['arguments'][3] != StanceType.HasNone else False

    def separate_node_and_edges(self):
        self.nodes, self.edges   = {}, {} 
        self.loops, self.stances = {}, {}
        self.grandparents, self.co_parents = {}, {}
        self.pars = set()

        for rule in self.ruleGroundings:
            par1, par2 = self.get_paragraphs(rule)

            args  = rule.get_head_predicate()['arguments']
            score = self.ruleGroundings[rule]

            # separate edges
            if rule.get_head_predicate()['name'] == LabelName.Edge:
                key = (args[1], args[2])

                if not self.has_loop(rule):
                    if par1 in self.edges:
                        self.edges[par1][key] = score
                    else:
                        self.edges[par1] = {key: score}
                else:
                    if par1 in self.loops:
                        self.loops[par1][key] = score
                    else:
                        self.loops[par1] = {key: score}

            # separate nodes
            elif rule.get_head_predicate()['name'] == LabelName.Node:
                to_insert = (args[2], score)

                if par1 in self.nodes and args[1] in self.nodes[par1]:
                    self.nodes[par1][args[1]].append(to_insert)
                elif par1 in self.nodes:
                    self.nodes[par1][args[1]] = [to_insert]
                else:
                    self.nodes[par1] = {args[1]: [to_insert]}

            # separate stances
            elif rule.get_head_predicate()['name'] == LabelName.EdgeLabel:
                to_insert = (args[2], score)

                if par1 in self.stances and args[1] in self.stances[par1]:
                    self.stances[par1][args[1]].append(to_insert)
                elif par1 in self.stances:
                    self.stances[par1][args[1]] = [to_insert]
                else:
                    self.stances[par1] = {args[1]: [to_insert]}

            # separate grandparents
            elif rule.get_head_predicate()['name'] == LabelName.Grandparent:
                key = (args[1], args[2], args[3])
                self.grandparents[key] = score

            # separate co-parents
            elif rule.get_head_predicate()['name'] == LabelName.CoParent:
                key = (args[1], args[2], args[3])
                self.co_parents[key] = score

            self.pars.add(par1)

class LabelName(object):
    Node, Edge, EdgeLabel, Grandparent, CoParent = 'IsArgType', 'Reln', 'Stance', 'Grandparent', 'CoParent'

class StanceType(object):
    Attack, Support, HasNone = u'Attack', u'Support', u'None'
