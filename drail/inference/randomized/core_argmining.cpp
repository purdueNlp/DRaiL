#include "core_argmining.h"
#include <utility>
#include <chrono>
#include <stdlib.h>
#include <algorithm>
#include <cassert>

using infer = RandomizedInferencer;

infer::RandomizedInferencer(pynodes nodes, pyedges edges, pynodes stances, pyedges loops, py2ndorder grandparents, py2ndorder co_parents, std::vector<int> pars_, int restarts_, bool is_train_, std::string arg0_, pygold g_nodes, pygold g_edges, pygold g_stances, bool no_constraints_) {
    p_nodes   = nodes;
    p_edges   = edges;
    p_stances = stances;
    p_loops   = loops;

    p_grandparents = grandparents;
    p_co_parents   = co_parents;

    restarts = restarts_;
    is_train = is_train_;
    arg0     = arg0_;
    pars     = pars_;

    gold_nodes   = g_nodes;
    gold_edges   = g_edges;
    gold_stances = g_stances;

    no_constraints = no_constraints_;

    build_gold_trees();

    r_dist = std::uniform_real_distribution<double>(0, 1);
    seed_random_generators();
}

std::vector<std::string> infer::get_nodes_in_par(int& par) const {
    std::vector<std::string> nodes;

    for (auto& element : p_nodes.at(par))
        if (std::find(nodes.begin(), nodes.end(), element.first) == nodes.end())
            nodes.push_back(element.first);

    return nodes;
}

template <typename T>
bool infer::contains(std::vector<T>& vec, T element) {
    return std::find(vec.begin(), vec.end(), element) != vec.end();
}

void infer::build_gold_trees() {
    for (auto& par : pars) {
        std::vector<std::string> par_nodes = get_nodes_in_par(par);
        u_ptr graph = make_unique<DiGraph<std::string>>();

        for (auto& node : gold_nodes)
            if (contains(par_nodes, node.at(0)))
                graph->add_node(Node<std::string>(node.at(0), node.at(1)));

        for (auto& edge : gold_edges)
            if (contains(par_nodes, edge.at(0)))
                graph->add_edge(Edge<std::string>(std::make_tuple(edge.at(0), edge.at(1))));

        for (auto& stance : gold_stances)
            if (contains(par_nodes, stance.at(0)))
                // every Premise has a stance to another node within the paragraph and
                // every Claim has a stance to the MajorClaim(s) within the essay
                graph->node(stance.at(0)).stance = stance.at(1);

        gold_trees.insert({par, std::move(graph)});
    }
}

void infer::seed_random_generators() {
    int seed = std::chrono::system_clock::now().time_since_epoch().count();

    re.seed(seed);
    std::srand(seed);
}

void infer::optimize() {
    double global_score = 0.0;

    for (int i = 0; i < restarts; i++) {
        bool major_claim_found = false;
        double local_score     = 0.0;
        
        int last_par = *std::max_element(pars.begin(), pars.end());
        std::map<int, u_ptr> local_graphs;

        for (auto& par : pars) {
            u_ptr graph;

            if (par == 1 || par == last_par) {
                std::vector<std::string> par_nodes = get_nodes_in_par(par);

                // node can only be MajorClaim
                if (par_nodes.size() == 1) {
                    graph = single_node_graph(par);
                    major_claim_found = true;
                }

                // might be the case that only a single MajorClaim in the essay
                else {
                    // MajorClaim is more often in last paragraph, also force to have at least one MC
                    bool has_major_claim = (!major_claim_found && par == last_par) ? true :
                                           (r_dist(re) < 0.8 && par == last_par)   ? true :
                                           (r_dist(re) < 0.2 && par == 1)          ? true : false;

                    major_claim_found = has_major_claim ? true : major_claim_found;

                    graph = randomized_hill_climbing(par, has_major_claim);
                    local_score += score_graph(graph, gold_trees.at(par));
                }
            }

            // body paragraph
            else {
                graph = randomized_hill_climbing(par);
                local_score += score_graph(graph, gold_trees.at(par));
            }

            local_graphs.insert({par, std::move(graph)});
        }

        // after determining nodes and edges, label edges and compute overall score
        local_score = randomized_edge_labeling(local_graphs, local_score);

        if (local_score > global_score) {
            predictions  = std::move(local_graphs);
            global_score = local_score;
        }
    }
}

double infer::get_node_score(std::string& node_id, int& par, const std::string& label) const {
    for (auto& element : p_nodes.at(par).at(node_id))
        if (std::get<0>(element) == label)
            return std::get<1>(element);

    return 0.0;
}

u_ptr infer::single_node_graph(int& par) const {
    u_ptr graph    = make_unique<DiGraph<std::string>>();
    std::string id = p_nodes.at(par).begin()->first;
    double score   = get_node_score(id, par, Label::MajorClaim);

    graph->add_node(Node<std::string>(id, score, Label::MajorClaim));

    return graph;
}

u_ptr infer::randomized_hill_climbing(int& par, bool has_major_claim) {
    u_ptr graph = init_random_graph(par);
    bool improvement = true;

    label_graph(graph, par, phantom, 0, has_major_claim);
    fix_edge_scores(graph, has_major_claim);

    auto bfs_vec = bfs_nodes(std::vector<Node<std::string>> { phantom }, graph);
    double score = score_graph(graph, gold_trees.at(par));

    while (improvement) {
        for (unsigned i = 0; i < bfs_vec.size(); i++) {
            Node<std::string> node = bfs_vec.at(i);
            std::vector<u_ptr>  adapted_graphs;
            std::vector<double> adapted_scores;

            // remove first i elements and reverse the list
            auto bfs_reversed = bfs_vec;
            bfs_reversed.erase(bfs_reversed.begin() + i, bfs_reversed.end());
            std::reverse(bfs_reversed.begin(),bfs_reversed.end());

            for (auto& predecessor : bfs_reversed) {
                if (!graph->has_edge(Edge<std::string>(std::make_tuple(node.id, predecessor.id)))) {
                    Node<std::string> successor = *graph->successors(node).begin();

                    u_ptr new_graph   = make_unique<DiGraph<std::string>>(*graph);
                    double edge_score = 0.0;

                    if (predecessor.id != phantom.id)
                        for (auto& element : p_edges.at(par))
                            if (element.first == std::make_tuple(node.id, predecessor.id))
                                edge_score = element.second;

                    new_graph->remove_edge(Edge<std::string>(std::make_tuple(node.id, successor.id)));
                    new_graph->add_edge(Edge<std::string>(std::make_tuple(node.id, predecessor.id), edge_score));

                    label_graph(new_graph, par, phantom, 0, has_major_claim);
                    fix_edge_scores(new_graph, has_major_claim);

                    adapted_scores.push_back(score_graph(new_graph, gold_trees.at(par)));
                    adapted_graphs.push_back(std::move(new_graph));
                }
            }

            if (!adapted_graphs.empty()) {
                auto local_optimum = std::max_element(adapted_scores.begin(), adapted_scores.end());
                int index = std::distance(adapted_scores.begin(), local_optimum);

                if (*local_optimum > score) {
                    graph = std::move(adapted_graphs.at(index));
                    score = *local_optimum;
                }
                else
                    improvement = false;
            }
        }
    }

    return graph;
}

std::vector<std::tuple<std::string, int>> infer::get_major_claims(std::map<int, u_ptr>& graphs) const {
    std::vector<std::tuple<std::string, int>> major_claims;

    for (auto& par : pars)
        for (auto& node : graphs.at(par)->get_nodes())
            if (node.second.arg == Label::MajorClaim)
                major_claims.push_back(std::make_tuple(node.first, par));

    return major_claims;
}

std::string infer::get_random_stance() {
    return (r_dist(re) < 0.9) ? StanceType::Support : StanceType::Attack;
}

std::string infer::flip_stance(std::string stance) const {
    return (stance == StanceType::Attack)  ? StanceType::Support :
           (stance == StanceType::Support) ? StanceType::Attack  : StanceType::HasNone;
}

double infer::randomized_edge_labeling(std::map<int, u_ptr>& graphs, double score) {
    if (!p_stances.empty()) {
        std::map<int, double> stance_scores;
        auto major_claims = get_major_claims(graphs);

        // no MajorClaim in essay initialized if assertion fails
        if (!no_constraints)
            assert(!major_claims.empty());

        // random stance initialization
        for (auto& par : pars) {
            for (auto& node : graphs.at(par)->get_nodes())
                if (node.second.arg == Label::Claim || node.second.arg == Label::Premise)
                    graphs.at(par)->node(node.first).stance = get_random_stance();
                else if (node.second.arg == Label::MajorClaim)
                    graphs.at(par)->node(node.first).stance = StanceType::HasNone;

            stance_scores.insert({par, score_stances(graphs.at(par), par, major_claims)});
        }

        // try to improve locally
        for (auto& par : pars) {
            for (auto& node : graphs.at(par)->get_nodes()) {
                if (node.second.arg == Label::Claim || node.second.arg == Label::Premise) {
                    graphs.at(par)->node(node.first).stance = flip_stance(graphs.at(par)->node(node.first).stance);
                    double current_score = score_stances(graphs.at(par), par, major_claims);

                    if (current_score > stance_scores.at(par))
                        stance_scores.at(par) = current_score;
                    else
                        graphs.at(par)->node(node.first).stance = flip_stance(graphs.at(par)->node(node.first).stance);
                }
            }
            score += stance_scores.at(par);
        }
    }
    return score;
}

double infer::score_stances(u_ptr& graph, int& par, std::vector<std::tuple<std::string, int>>& major_claims) const {
    double score = 0.0;
    int distance = 0;

    for (auto& node : graph->get_nodes()) {
        if (node.first != phantom.id){
            score += get_stance_score(node.first, par, node.second.stance);

            // naively count hamming distance
            if (is_train && node.first != phantom.id) {
                std::string cand_stance = node.second.stance;
                std::string gold_stance = gold_trees.at(par)->node(node.first).stance;

                if (cand_stance != gold_stance)
                    distance++;

                double norm_hd = static_cast<double>(distance) / gold_trees.at(par)->get_nodes().size();
                score += 1 - norm_hd;
            }
        }
    }

    return score;
}

double infer::get_stance_score(std::string node, int par, std::string stance) const {
    // returns the score of a labeled edge
    for (auto& map : p_stances.at(par))
        for (auto& element : map.second)
            if (map.first == node && stance == std::get<0>(element))
                return std::get<1>(element);

    return 0.0;
}

u_ptr infer::init_random_graph(int& par) {
    std::map<std::tuple<std::string, std::string>, double> edges;
    u_ptr graph = make_unique<DiGraph<std::string>>();

    // draw random edge
    auto rand_iter = p_edges.at(par).begin();
    std::advance(rand_iter, std::rand() % p_edges.at(par).size());

    auto edge = Edge<std::string>(rand_iter->first, rand_iter->second);

    // add phantom node - allows more than one Claim / MajorClaim
    graph->add_node(phantom);
    graph->add_edge(Edge<std::string>(std::make_tuple(edge.to(), phantom.id)));
    graph->add_edge(edge);

    // choose only from edges where the from-node is not anymore in the graph
    for (auto& element : p_edges.at(par))
        if (!graph->has_node(std::get<0>(element.first)))
            edges.insert(element);

    while (!edges.empty()) {
        std::map<std::tuple<std::string, std::string>, double> conn_edges;
        std::map<std::tuple<std::string, std::string>, double> temp_edges;

        // choose only from edges where the to-node is already in the graph
        for (auto& element : edges)
            if (graph->has_node(std::get<1>(element.first)))
                conn_edges.insert(element);
        
        auto rand_iter2 = conn_edges.begin();
        std::advance(rand_iter2, std::rand() % conn_edges.size());

        edge = Edge<std::string>(rand_iter2->first, rand_iter2->second);

        graph->add_edge(edge);

        // choose only from edges where the from-node is not anymore in the graph
        for (auto& element : edges)
            if (!graph->has_node(std::get<0>(element.first)))
                temp_edges.insert(element);

        edges = temp_edges;
    }

    return graph;
}

void infer::label_graph(u_ptr& graph, int& par, Node<std::string>& root, int level, bool& has_major_claim) {
    std::string arg;

    if (no_constraints) {
        double rval = r_dist(re);
        arg = (rval < 0.33) ? Label::MajorClaim : (rval < 0.66) ? Label::Claim : Label::Premise;
    }
    else if (has_major_claim)
        arg = (level == 0) ? Label::MajorClaim :
              (level == 1) ? Label::Claim : Label::Premise;
    else
        arg = (level == 0) ? Label::Claim : Label::Premise;

    for (auto& pre : graph->predecessors(root)) {
        graph->node(pre.id).score = get_node_score(pre.id, par, arg);
        graph->node(pre.id).arg   = arg;

        // label recursively
        label_graph(graph, par, pre, level + 1, has_major_claim);
    }
}

void infer::fix_edge_scores(u_ptr& graph, bool& has_major_claim) {
    // edges to MajorClaims do not exist, therefore the
    // score of edges to MCs will be inverted (1 - score)
    if (has_major_claim)
        for (auto& element: graph->get_edges())
            if (graph->node(element.second.to()).arg == Label::MajorClaim)
                graph->edge(element.first).score = 1 - element.second.score;
}

std::vector<Node<std::string>> infer::bfs_nodes(std::vector<Node<std::string>> predecessors, u_ptr& graph) {
    std::vector<Node<std::string>> children;

    for (auto& element : predecessors) {
        auto vec = bfs_nodes(graph->predecessors(element), graph);
        children.insert(children.end(), vec.begin(), vec.end());
    }

    auto to_return = predecessors;
    to_return.insert(to_return.end(), children.begin(), children.end());

    return to_return;
}

double infer::score_graph(const u_ptr& graph, const u_ptr& gold_tree) const {
    double score = graph->sum_node_scores() + graph->sum_edge_scores();

    if (!p_grandparents.empty())
        for (auto& grandparent : graph->get_grandparents())
            if (std::get<0>(grandparent) != "phantom")
                score += p_grandparents.at(grandparent);

    if (!p_co_parents.empty())
        for (auto& co_parent : graph->get_coparents())
            if (std::get<0>(co_parent) != "phantom")
                score += p_co_parents.at(co_parent);

    if (is_train) {
        double hamming_d = hamming_distance(graph, gold_tree);
        score += score * (1 - hamming_d);
    }

    return score;
}

double infer::hamming_distance(const u_ptr& graph, const u_ptr& gold) const {
    // returns normalized hamming distance
    int n_nodes  = gold->get_nodes().size();
    int distance = gold->varying_node_args(graph) + gold->varying_edge(graph);

    return distance / static_cast<double>(n_nodes + n_nodes - 1);
}

void infer::build_predicted_stances() {
    if (pred_stances.empty()) {
        auto major_claims = get_major_claims(predictions);

        for (auto& par : pars) {
            for (auto& node : predictions.at(par)->get_nodes()) {
                if (node.second.arg == Label::Claim)
                    for (auto& m_node : major_claims)
                        pred_stances.insert({std::make_tuple(node.first, std::get<0>(m_node)), node.second.stance});
                
                else if (node.second.arg == Label::Premise)
                    pred_stances.insert({std::make_tuple(node.first, predictions.at(par)->successors(node.second).begin()->id), node.second.stance});
            }
        }
    }
} 

std::vector<std::vector<std::string>> infer::evaluate() {
    std::vector<std::vector<std::string>> return_vector;

    std::vector<std::string> node_gold_data;
    std::vector<std::string> node_pred_data;

    std::vector<std::string> edge_gold_data;
    std::vector<std::string> edge_pred_data;

    std::vector<std::string> stance_gold_data;
    std::vector<std::string> stance_pred_data;

    std::vector<std::string> grandparent_gold_data;
    std::vector<std::string> grandparent_pred_data;

    std::vector<std::string> co_parent_gold_data;
    std::vector<std::string> co_parent_pred_data;

    std::vector<std::tuple<std::string, std::string, std::string>> actual_gold_grandparents;
    std::vector<std::tuple<std::string, std::string, std::string>> actual_pred_grandparents;
    std::vector<std::tuple<std::string, std::string, std::string>> actual_gold_co_parents;
    std::vector<std::tuple<std::string, std::string, std::string>> actual_pred_co_parents;

    for (auto& par : pars) {
        if (predictions.at(par)->has_node(phantom))
            predictions.at(par)->remove_node(phantom);

        for (auto& node : predictions.at(par)->get_nodes()) {
            node_pred_data.push_back(node.second.arg);
            node_gold_data.push_back(gold_trees.at(par)->node(node.first).arg);

            if (!p_stances.empty()) {
                stance_pred_data.push_back(node.second.stance);
                stance_gold_data.push_back(gold_trees.at(par)->node(node.first).stance);
            }
        }

        // also handle loops as edges for metrics
        auto all_edges = p_loops.at(par);

        if (p_edges.find(par) != p_edges.end())
            all_edges.insert(p_edges.at(par).begin(), p_edges.at(par).end());

        for (auto& element : all_edges) {
            auto edge = Edge<std::string>(element.first);

            if (predictions.at(par)->has_edge(edge) && predictions.at(par)->get_nodes().at(edge.to()).arg != Label::MajorClaim)
                edge_pred_data.push_back("1");
            else
                edge_pred_data.push_back("0");

            if (gold_trees.at(par)->has_edge(edge))
                edge_gold_data.push_back("1");
            else
                edge_gold_data.push_back("0");
        }

        if (!p_grandparents.empty()) {
            auto gold_grandparents = gold_trees.at(par)->get_grandparents();
            auto pred_grandparents = predictions.at(par)->get_grandparents();

            actual_gold_grandparents.insert(actual_gold_grandparents.end(), gold_grandparents.begin(), gold_grandparents.end());
            actual_pred_grandparents.insert(actual_pred_grandparents.end(), pred_grandparents.begin(), pred_grandparents.end());
        }

        if (!p_co_parents.empty()) {
            auto gold_co_parents = gold_trees.at(par)->get_coparents();
            auto pred_co_parents = predictions.at(par)->get_coparents();

            actual_gold_co_parents.insert(actual_gold_co_parents.end(), gold_co_parents.begin(), gold_co_parents.end());
            actual_pred_co_parents.insert(actual_pred_co_parents.end(), pred_co_parents.begin(), pred_co_parents.end());
        }
    }

    if (!p_grandparents.empty()) {
        for (auto& grandparent : p_grandparents) {
            grandparent_gold_data.push_back((std::find(actual_gold_grandparents.begin(), actual_gold_grandparents.end(), grandparent.first) != actual_gold_grandparents.end()) ? "1" : "0");
            grandparent_pred_data.push_back((std::find(actual_pred_grandparents.begin(), actual_pred_grandparents.end(), grandparent.first) != actual_pred_grandparents.end()) ? "1" : "0");
        }
    }

    if (!p_co_parents.empty()) {
        for (auto& co_parent : p_co_parents) {
            co_parent_gold_data.push_back((std::find(actual_gold_co_parents.begin(), actual_gold_co_parents.end(), co_parent.first) != actual_gold_co_parents.end()) ? "1" : "0");
            co_parent_pred_data.push_back((std::find(actual_pred_co_parents.begin(), actual_pred_co_parents.end(), co_parent.first) != actual_pred_co_parents.end()) ? "1" : "0");
        }
    }

    // notice that this order is important
    return_vector.push_back(node_gold_data);
    return_vector.push_back(node_pred_data);

    return_vector.push_back(edge_gold_data);
    return_vector.push_back(edge_pred_data);

    return_vector.push_back(stance_gold_data);
    return_vector.push_back(stance_pred_data);

    return_vector.push_back(grandparent_gold_data);
    return_vector.push_back(grandparent_pred_data);

    return_vector.push_back(co_parent_gold_data);
    return_vector.push_back(co_parent_pred_data);

    return return_vector;
}

std::vector<std::string> infer::get_predictions() {
    std::vector<std::string> return_vector;
    std::string head;

    for (auto& par : pars) {
        if (predictions.at(par)->has_node(phantom))
            predictions.at(par)->remove_node(phantom);

        for (auto& node : predictions.at(par)->get_nodes()) {
            head = Label::Node + "(" + arg0 + "," + node.second.id + "," + node.second.arg + ")";
            return_vector.push_back(head);

            if (!p_stances.empty()) {
                head = Label::Stance + "(" + arg0 + "," + node.second.id + "," + node.second.stance + ")";
                return_vector.push_back(head);
            }
        }

        for (auto& edge : predictions.at(par)->get_edges()) {
            if (predictions.at(par)->get_nodes().at(edge.second.to()).arg != Label::MajorClaim) {
                head = Label::Edge + "(" + arg0 + "," + edge.second.from() + "," + edge.second.to() + ")";
                return_vector.push_back(head);
            }
        }

        if (!p_grandparents.empty())
            for (auto& grandparent : predictions.at(par)->get_grandparents()) {
                head = Label::Grandparent + "(" + arg0 + "," + std::get<0>(grandparent) + "," + std::get<1>(grandparent) + "," + std::get<2>(grandparent) + ")";
                return_vector.push_back(head);
            }

        if (!p_co_parents.empty())
            for (auto& co_parent : predictions.at(par)->get_coparents()) {
                head = Label::CoParent + "(" + arg0 + "," + std::get<0>(co_parent) + "," + std::get<1>(co_parent) + "," + std::get<2>(co_parent) + ")";
                return_vector.push_back(head);
            }
    }

    return return_vector;
}

PYBIND11_MODULE(core_argmining, m) {
    py::class_<RandomizedInferencer>(m, "RandomizedInferencer")
        .def(py::init<pynodes, pyedges, pynodes, pyedges, py2ndorder, py2ndorder, std::vector<int>, int, bool, std::string, pygold, pygold, pygold, bool>())
        .def("optimize", &infer::optimize)
        .def("evaluate", &infer::evaluate)
        .def("get_predictions", &infer::get_predictions);
}