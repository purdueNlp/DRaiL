import ad3

from collections import OrderedDict
from .inferencer_base import InferencerBase
from ..model.rule import RuleGrounding
from ..model.label import LabelType

class AD3Inferencer(InferencerBase):

    def __init__(self, ruleGroundingsInput, constraintGroundingsInput, treeConstraintGroundingsInput, treeInfo,
                 arithmetic_rules=[], exact=False, eta=0.1, adapt=True, max_iter=5000):
        super(AD3Inferencer, self).__init__(ruleGroundingsInput, constraintGroundingsInput, treeConstraintGroundingsInput, treeInfo)

        self.rule_variables = OrderedDict()
        self.head_variables = OrderedDict()
        self.aux_variables  = OrderedDict()
        
        self.eta = eta
        self.adapt = adapt
        self.exact = exact
        self.max_iter = max_iter

        self.var_lookup = dict()
        self.var_counter = 0

    def insert_into_lookup(self, binary_var):
        assert binary_var not in self.var_lookup

        self.var_lookup[binary_var] = self.var_counter
        self.var_counter += 1

    def create_model(self):
        self.model = ad3.PFactorGraph()

    def add_variables(self):
        self.headDict = OrderedDict()
        self.multiClassDict = OrderedDict()
        self.multiLabelDict = OrderedDict()
        self.binaryDict = OrderedDict()
        self.numRules = 0

        self.ruleVarCounter = 0

        for ruleGrounding in self.ruleGroundings:
            rvar = self.model.create_binary_variable()
            #print(ruleGrounding, self.ruleGroundings[ruleGrounding])
            rvar.set_log_potential(self.ruleGroundings[ruleGrounding])
            self.insert_into_lookup(rvar)

            self.rule_variables['r_'+str(self.ruleVarCounter)] = rvar
            self.ruleVarCounter += 1

            # BinaryPredicate case
            if (ruleGrounding.head_str not in self.headDict and
                ruleGrounding.neg_head_str not in self.headDict):

                headVar, negVar = self.addHeadVariable(
                    ruleGrounding.head_str,
                    ruleGrounding.neg_head_str,
                    rvar)

                #print("HERE", ruleGrounding.head_str, ruleGrounding.neg_head_str, headVar, negVar)

                if ruleGrounding.is_binary_head:
                    # BinaryPredicate case
                    if ruleGrounding.head['name'] not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']] = {}
                    negated_predicate = ruleGrounding.head.copy()
                    negated_predicate['isneg'] = not ruleGrounding.head['isneg']
                    if headVar not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']][headVar] = ruleGrounding.head
                    if negVar not in self.binaryDict:
                        self.binaryDict[ruleGrounding.head['name']][negVar] = negated_predicate

                elif ruleGrounding.head['target_pos'] is not None and ruleGrounding.head['ttype'] == LabelType.Multiclass:
                    # Multiclass
                    if (ruleGrounding.head_abs_str not in self.multiClassDict):
                        self.multiClassDict[ruleGrounding.head_abs_str] = {}
                    if headVar not in self.multiClassDict[ruleGrounding.head_abs_str]:
                        self.multiClassDict[ruleGrounding.head_abs_str][headVar] = ruleGrounding.head
                elif ruleGrounding.head['target_pos'] is not None and ruleGrounding.head['ttype'] == LabelType.Multilabel:
                    # Multilabel
                    if (ruleGrounding.head_abs_str not in self.multiLabelDict):
                        self.multiLabelDict[ruleGrounding.head_abs_str] = {}
                    if headVar not in self.multiLabelDict[ruleGrounding.head_abs_str]:
                        self.multiLabelDict[ruleGrounding.head_abs_str][headVar] = ruleGrounding.head
                else:
                    print("Head type not supported")
                    exit(-1)

            else:
                self.headDict[ruleGrounding.head_str][2].append(rvar)

    def addRuleVariable(self):
        rvar = self.model.create_binary_variable()
        rvar.set_log_potential(0)
        self.insert_into_lookup(rvar)

        self.rule_variables['r_'+str(self.ruleVarCounter)] = rvar
        self.ruleVarCounter += 1

        return rvar 

    def addHeadVariable(self, head_str, neg_head_str, rvar):
        headCounter = len(self.headDict)

        headVar = self.model.create_binary_variable()
        headVar.set_log_potential(0)
        self.insert_into_lookup(headVar)

        self.head_variables['h_'+str(headCounter)] = headVar
        headCounter += 1

        negVar = self.model.create_binary_variable()
        negVar.set_log_potential(0)
        self.insert_into_lookup(negVar)

        self.head_variables['h_'+str(headCounter)] = negVar
        headCounter += 1

        self.headDict[head_str] = [headVar, negVar, [rvar]]
        self.headDict[neg_head_str] = [negVar, headVar, []]
        return headVar, negVar

    def addHardConstraintHeads(self):
        constraintHeads = set([])
        for constrGrounding in self.constraintGroundings:
            if constrGrounding.head_str not in self.headDict and constrGrounding.neg_head_str not in self.headDict:
                #print("Adding", constrGrounding.head_str)
                rvar = self.addRuleVariable()
                headVar, negVar = self.addHeadVariable(constrGrounding.head_str, constrGrounding.neg_head_str, rvar)
                constraintHeads.add(constrGrounding.head_str)

                '''
                    Adding this to be able to keep track of activations
                    of predicates that are introduced in constraints only
                '''
                negated_predicate = constrGrounding.head.copy()
                negated_predicate['isneg'] = not constrGrounding.head['isneg']

                #print(constrGrounding, constrGrounding.is_binary_head)

                if constrGrounding.is_binary_head:
                    if constrGrounding.head['name'] not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']] = {}
                    if headVar not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']][headVar] = constrGrounding.head
                    if negVar not in self.binaryDict:
                        self.binaryDict[constrGrounding.head['name']][negVar] = negated_predicate

            #print(constrGrounding)
            '''
            if len(constrGrounding.body_unobs_str) == 0 and constrGrounding.head_str not in self.headDict:
                rvar = self.addRuleVariable()
                self.addHeadVariable(constrGrounding.head_str, constrGrounding.neg_head_str, rvar)
                constraintHeads.add(constrGrounding.head_str)
            '''
        #exit()

    def addAuxiliaryTreeVars(self):
        self.auxiliaryVars = {}
        auxCounter = 0

        for pred in self.treeConstraintGroundings:
            self.auxiliaryVars[pred] = {}

            for reln in self.treeConstraintGroundings[pred]:
                i, j = reln['arguments'][-2:]
                var = self.model.create_binary_variable()
                var.set_log_potential(0)
                self.insert_into_lookup(var)

                self.auxiliaryVars[pred]["{0},{1}".format(i,j)] = var
                self.aux_variables["b_{0}".format(auxCounter)] = var

                auxCounter += 1

    def addHardConstraints(self, constrCounter):
        conditionalMulticlass = {}; conditionalMultilabel = {}
        
        for constrGrounding in self.constraintGroundings:
            body_variables = []

            unobserved = constrGrounding.body_unobs_str
            body_variables = [self.headDict[pred][0] for pred in unobserved]
            hvar = self.headDict[constrGrounding.head_str][0]
            n_body_variables = len(body_variables)

            if n_body_variables > 0 and constrGrounding.head['ttype'] not in [1, 2]:
                #print(constrGrounding)
                # sum(body_vars) <= n_body_vars - 1 + hvar
                self.model.create_factor_logic('IMPLY', body_variables + [hvar],
                                                [False] * (n_body_variables + 1)) # hmm, still not sure
                constrCounter += 1

            elif n_body_variables > 0 and constrGrounding.head['ttype'] == 1:
                headVar = self.headDict[constrGrounding.head_str][0]
                curr_hash = " ".join(unobserved) + constrGrounding.head_abs_str
                if curr_hash not in conditionalMulticlass:
                    conditionalMulticlass[curr_hash] = {'keys': {}, 'body_vars': body_variables}
                if headVar not in conditionalMulticlass[curr_hash]['keys']:
                    conditionalMulticlass[curr_hash]['keys'][headVar] = constrGrounding.head
            elif n_body_variables > 0 and constrGrounding.head['ttype'] == 2:
                headVar = self.headDict[constrGrounding.head_str][0]
                curr_hash = " ".join(unobserved) + constrGrounding.head_abs_str
                if curr_hash not in conditionalMultilabel:
                    conditionalMultilabel[curr_hash] = {'keys': {}, 'body_vars': body_variables}
                if headVar not in conditionalMultilabel[curr_hash]['keys']:
                    conditionalMultilabel[curr_hash]['keys'][headVar] = constrGrounding.head

            elif constrGrounding.head['ttype'] == 1:
                # keep track of multiclass elements in head of rule
                # constraint will be added later
                headVar = self.headDict[constrGrounding.head_str][0]
                if constrGrounding.head_abs_str not in self.multiClassDict:
                    self.multiClassDict[constrGrounding.head_abs_str] = {}
                if headVar not in self.multiClassDict[constrGrounding.head_abs_str]:
                    self.multiClassDict[constrGrounding.head_abs_str][headVar] = constrGrounding.head
            elif constrGrounding.head['ttype'] == 2:
                # keep track of multilabel elements in head of rule
                # constraint will be added later
                headVar = self.headDict[constrGrounding.head_str][0]
                if constrGrounding.head_abs_str not in self.multiLabelDict:
                    self.multiLabelDict[constrGrounding.head_abs_str] = {}
                if headVar not in self.multiLabelDict[constrGrounding.head_abs_str]:
                    self.multiLabelDict[constrGrounding.head_abs_str][headVar] = constrGrounding.head
            else:
                # enforcing the head: hvar = 1
                self.model.create_factor_logic('XOR', [hvar], [False]) # fix
                constrCounter += 1

        # Add the conditional MC stuff
        for abstra in conditionalMulticlass:
            head_vs = list(conditionalMulticlass[abstra]['keys'].keys())
            body_vs = conditionalMulticlass[abstra]['body_vars']
            # sum(head_vars) = aux
            # sum(body_vars) <= n_body_vars - 1 + aux
            aux_var_head = self.model.create_binary_variable()
            self.model.create_factor_logic('XOROUT', head_vs + [aux_var_head], [False] * (len(head_vs) + 1))
            self.model.create_factor_logic('IMPLY', body_vs + [aux_var_head], [False] * (len(body_vs) + 1))
            constrCounter += 2
        
        for abstra in conditionalMultilabel:
            head_vs = list(conditionalMultilabel[abstra]['keys'].keys())
            body_vs = conditionalMultilabel[abstra]['body_vars']
            # sum(head_vars) >= aux
            # sum(body_vars) <= n_body_vars - 1 + aux
            aux_var_head = self.model.create_binary_variable()
            self.model.create_factor_logic('OROUT', head_vs + [aux_var_head], [False] * (len(head_vs) + 1))
            self.model.create_factor_logic('IMPLY', body_vs + [aux_var_head], [False] * (len(body_vs) + 1))
            constrCounter += 2
        
        
        return constrCounter

    def addImplyConstraints(self, constrCounter):
        ruleVarCounter = 0
        deactivated_B = 0

        for ruleGrounding in self.ruleGroundings:
            r = self.rule_variables['r_'+str(ruleVarCounter)]

            unobs = ruleGrounding.body_unobs_str
            found = [pred for pred in unobs if pred in self.headDict]

            if len(unobs) != len(found):
                # r = 0
                self.model.create_factor_logic('XOR', [r], [True]) # fix
                constrCounter += 1
                deactivated_B += len(unobs)
            else:
                for pred in found:
                    h = self.headDict[pred][0]

                    # h >= r
                    self.model.create_factor_logic('IMPLY', [r, h], [False, False]) # fix
                    constrCounter += 1

            ruleVarCounter += 1

            # do not need to have imply constr for negation of binary head
            '''
            if (ruleGrounding.is_binary_head and
                    (ruleGrounding.head['target_pos'] is None or
                     ruleGrounding.head['ttype'] == LabelType.Binary)
                ):
                ruleVarCounter += 1
            '''
        return constrCounter

    def addNegationAndRuleHeadConstraints(self, constrCounter):
        for head in self.headDict:
            headVar = self.headDict[head][0]
            if (head.startswith("~")):

                negVar = self.headDict[head][1]
                #print("HEAD", head, headVar, negVar)

                # headVar + negVar = 1
                self.model.create_factor_logic('XOR', [headVar, negVar], [False, False]) # fix
                constrCounter += 1

            # [r1, r2, ...]
            rs = self.headDict[head][2]
            if (len(rs) == 0):
                continue

            for r in rs:
                # h >= r_i
                self.model.create_factor_logic('IMPLY', [r, headVar], [False, False])
                constrCounter += 1

            # h <= sum(r_i)
            self.model.create_factor_logic('OROUT', rs + [headVar], [False] * (len(rs) + 1))
            constrCounter += 1

        return constrCounter

    def addMulticlassMultilabelConstraints(self, constrCounter):
        for abstra in self.multiClassDict:
            vs = list(self.multiClassDict[abstra].keys())

            # sum(vs) = 1
            self.model.create_factor_logic('XOR', vs, [False] * len(vs)) # fix
            constrCounter += 1

        for abstra in self.multiLabelDict:
            vs = list(self.multiLabelDict[abstra].keys())

            # sum(vs) >= 1
            self.model.create_factor_logic('OR', vs, [False] * len(vs)) # fix
            constrCounter += 1

        return constrCounter

    def _node_types(self, template, type_dict, reln_name, reln, hvar):
        observed_data = dict(zip(self.treeInfo[reln_name]['variables'], reln['arguments']))

        if template is not None:
            cand = RuleGrounding.pred_str(
                self._create_observed_predicate(template, observed_data))
            if cand not in type_dict:
                type_dict[cand] = []
            type_dict[cand].append(hvar)

    def _create_observed_predicate(self, predicate_templ, observed_vars):
        args = []

        for j, var in enumerate(predicate_templ.variables):
            if var.isconstant:
                args.append(var.arg)
            else:
                args.append(observed_vars[var.arg])

        ttype=predicate_templ.label_type()
        tpos=predicate_templ.label_pos()
        new_predicate = {'name': predicate_templ.name,
                         'arguments': args, 'ttype': None,
                         'obs': True, 'isneg': predicate_templ.isneg,
                         'target_pos': None}
        return new_predicate

    def addTreeConstraints(self, constrCounter):
        for pred in self.treeConstraintGroundings:
            matrix = {}; matrix_id = {}
            connected_components = []
            roots = {}; notRoots = {}; leaves = {}

            for reln in self.treeConstraintGroundings[pred]:
                i, j = reln['arguments'][-2:]
                reln_str = RuleGrounding.pred_str(reln)
                reln_str_aux = "{0},{1}".format(i,j)

                # collect constraints on outgoing and incoming links
                hvar = self.headDict[reln_str][0]
                if self.treeInfo[pred]['root'] is not None:
                    self._node_types(self.treeInfo[pred]['root'], roots, pred, reln, hvar)
                if self.treeInfo[pred]['notRoot'] is not None:
                    self._node_types(self.treeInfo[pred]['notRoot'], notRoots, pred, reln, hvar)
                if self.treeInfo[pred]['leaf'] is not None:
                    self._node_types(self.treeInfo[pred]['leaf'], leaves, pred, reln, hvar)

                found = False
                for comp in connected_components:
                    if i in comp:
                        found = True
                        comp.add(j)
                        break

                if not found:
                    connected_components.append(set([i, j]))

                if i not in matrix:
                    matrix[i] = []; matrix_id[i] = []

                matrix[i].append(reln_str)
                matrix_id[i].append(j)

                # Source and target can't be the same
                if i == j:
                    hvar = self.headDict[reln_str][0]

                    # hvar = 0
                    self.model.create_factor_logic('XOR', [hvar], [True]) # fix
                    constrCounter += 1

                    # Prevent cycles -- constr 3: auxVar = 0
                    self.model.create_factor_logic('XOR', [self.auxiliaryVars[pred][reln_str_aux]], [True]) # fix
                    constrCounter += 1

                # Prevent cycles -- constr 1: hvar - auxVar <= 0 ... hvar => auxVar .. x_ij => b_ij
                self.model.create_factor_logic('IMPLY', [hvar, self.auxiliaryVars[pred][reln_str_aux]], [False, False])
                #self.model.create_factor_logic('ANDOUT', [hvar, self.auxiliaryVars[pred][reln_str_aux]])
                constrCounter += 1

            # At least one root per connected component
            for comp in connected_components:
                n = len(comp)
                reln_variables = []
                for node in comp:
                    reln_variables += [self.headDict[pr][0] for pr in matrix[node]]

                # sum(reln_vars) <= n - 1
                self.model.create_factor_budget(reln_variables, n - 1) # fix
                constrCounter += 1


            for source in matrix:
                # Only one or zero outgoing links
                target_variables = [self.headDict[pr][0] for pr in matrix[source]]

                # sum(target_vars) <= 1
                self.model.create_factor_logic('ATMOSTONE', target_variables, [False] * len(target_variables)) # fix
                constrCounter += 1

                # Prevent cycles -- constr 2
                for k in matrix_id[source]:
                    for j in matrix_id[source]:
                        ik_index = "{0},{1}".format(source, k)
                        ij_index = "{0},{1}".format(source, j)
                        jk_index = "{0},{1}".format(j, k)

                        b_ik = self.auxiliaryVars[pred][ik_index]
                        b_ij = self.auxiliaryVars[pred][ij_index]
                        b_jk = self.auxiliaryVars[pred][jk_index]

                    # b_ik - b_ij - b_jk >= -1 .... b_ij ^ b_jk => b_ik
                    self.model.create_factor_logic('IMPLY', [b_ij, b_jk, b_ik], [False, False, False]) # fix (if b_ij & b_jk are on, b_ik must be on, otherwise they can have any value)
                    constrCounter += 1

            # Constraints on source/target

            # zero outgoing links for roots
            for node in roots:
                nvar = self.headDict[node][0]

                self.model.create_factor_logic('OROUT', roots[node] + [nvar], [False] * len(roots[node]) + [True])
                constrCounter += 1

            # exactly one outgoing link for not-roots
            for node in notRoots:
                nvar = self.headDict[node][0]

                self.model.create_factor_logic('XOROUT', notRoots[node] + [nvar], [False] * (len(notRoots[node]) + 1))
                constrCounter += 1

            # zero incoming links for leaves
            for node in leaves:
                nvar = self.headDict[node][0]

                # nvar * sum(leaves[node]) <= 0
                for l in leaves[node]:
                    self.model.create_factor_logic('IMPLY', [nvar, l], [False, True])

                constrCounter += 1

            # number of links >= # not_roots
            if len(roots) > 0:
                notroot_vars = [self.headDict[r][0] for r in notRoots]
                reln_vars = [self.headDict[RuleGrounding.pred_str(r)][0] for r in self.treeConstraintGroundings[pred]]

                # sum(notroot_vars) <= len(reln_vars)
                self.model.create_factor_budget(notroot_vars, len(reln_vars)) # fix, but not sure if it's enough

                constrCounter += 1

        return constrCounter

    def addFixGoldConstraints(self, constrCounter, gold_heads, latent_predicates):
        gold_heads_str = set([RuleGrounding.pred_str(h) for h in gold_heads])
        for head in self.headDict:
            head_pred, _ = head.split('(')
            if head_pred.startswith('~'):
                head_pred = head_pred[1:]
            if (head in gold_heads_str) or \
               (head.startswith('~') and (head[1:] not in gold_heads_str) and (head_pred) not in latent_predicates):
                head_var = self.headDict[head][0]
                self.model.create_factor_logic('XOR', [head_var], [False]) #fix
                constrCounter += 1
        return constrCounter

    def add_constraints(self, gold_heads, fix_gold=False, latent_predicates=set()):
        constrCounter = 0

        # add variables needed for constraints
        self.addHardConstraintHeads()
        self.addAuxiliaryTreeVars()

        # hard constraints
        constrCounter = self.addHardConstraints(constrCounter)

        # imply constr
        constrCounter = self.addImplyConstraints(constrCounter)

        # negation constr, rule/head constr
        constrCounter = self.addNegationAndRuleHeadConstraints(constrCounter)

        # Binary/Multi-Class/Multi-Label constr
        constrCounter = self.addMulticlassMultilabelConstraints(constrCounter)

        # Tree constraints
        constrCounter = self.addTreeConstraints(constrCounter)

        # Fix constraints for latent variable learning
        if fix_gold:
            constrCounter = self.addFixGoldConstraints(constrCounter, gold_heads, latent_predicates)

    def encode(self, gold_heads=[], fix_gold=False, latent_predicates=set()):
        self.create_model()
        self.add_variables()
        self.add_constraints(gold_heads, fix_gold, latent_predicates)

    def optimize(self):
        self.model.set_eta_ad3(self.eta)
        self.model.adapt_eta_ad3(self.adapt)
        self.model.set_max_iterations_ad3(self.max_iter)

        if self.exact:
            self.val, self.posteriors, _, self.status = self.model.solve_exact_map_ad3()
        else:
            self.val, self.posteriors, _, self.status = self.model.solve_lp_map_ad3()
        #print("solver_status", self.status)

    def get_binary_metrics(self, dictionary, gold_heads_dict, latent_predicates):
        #print(latent_predicates)
        gold_heads = set()
        for head in gold_heads_dict:
            gold_heads.add(RuleGrounding.pred_str(head))

        #print(gold_heads)
        metrics = {}; predictions = set([])
        results = {}
        for (key, value_ls) in dictionary:
            #print(key)
            for var, head in value_ls.items():

                if head['name'] not in metrics:
                    metrics[head['name']] = {'gold_data': [], 'pred_data': [], 'ids': []}

                head_str = RuleGrounding.pred_str(head)
                pred_value = self.posteriors[self.var_lookup[var]]

                #print(head_str, pred_value)

                if head_str.startswith('~') and head_str[1:] not in results:
                    results[head_str[1:]] = [None, None]
                elif not head_str.startswith('~') and head_str not in results:
                    results[head_str] = [None, None]

                if head_str.startswith('~'):
                    results[head_str[1:]][0] = pred_value
                else:
                    results[head_str][1] = pred_value
                '''
                if head['name'] in latent_predicates:
                    print(head_str, round(pred_value))
                '''

        #print(results)

        for head_str in results:
            head_name = head_str.split('(')[0]

            # Pred label
            max_value = max(results[head_str])
            pred_label = results[head_str].index(max_value)
            #print(head_str, pred_label, max_value)

            if head_name not in latent_predicates:
                # Gold label
                if head_str in gold_heads:
                    metrics[head_name]['gold_data'].append(1)
                else:
                    metrics[head_name]['gold_data'].append(0)
                metrics[head_name]['ids'].append(head_str)

                metrics[head_name]['pred_data'].append(pred_label)

                if pred_label == 1:
                    predictions.add(head_str)
                #else:
                #    predictions.add("~" + head_str)


        #print(metrics[head['name']]['gold_data'].count(1))
        #print(metrics[head['name']]['gold_data'].count(0))
        #exit()

        '''
        for pred in predictions:
            print(pred)
        '''
        return predictions, metrics

    def get_multi_metrics(self, dictionary, gold_heads, binary_predicates):
        metrics = {}
        predictions = set([])

        # load dictionary of gold predictions
        gold_data = {}

        for elem in gold_heads:
            classif_name = elem['name']
            #print("classif_name", classif_name)
            # skip binary predicates
            if classif_name in binary_predicates:
                continue

            if classif_name not in gold_data:
                gold_data[classif_name] = {}

            id_ = ",".join(str(elem['arguments'][i]) for i in range(0, len(elem['arguments'])) if i != elem['target_pos'])
            label = elem['arguments'][elem['target_pos']]
            gold_data[classif_name][id_] = label
        #print(gold_data)

        # load dictionary of pred labels, keep track of max scores
        pred_data = {}
        max_scores = {}
        for (key, value_ls) in dictionary:
            for var, head in value_ls.items():

                if head['name'] not in metrics and head['name'] in gold_data:
                    metrics[head['name']] = {'gold_data': [], 'pred_data': [], 'ids': []}

                if head['name'] not in max_scores:
                    pred_data[head['name']] = {}
                    max_scores[head['name']] = {}

                k_class = head['arguments'][head['target_pos']]
                curr_id = ",".join(str(head['arguments'][i]) for i in range(0, len(head['arguments'])) if i != head['target_pos'])

                pred_value = self.posteriors[self.var_lookup[var]]

                #print(head['name'], k_class, pred_value)
                if curr_id not in max_scores[head['name']] or pred_value > max_scores[head['name']][curr_id][0]:
                    max_scores[head['name']][curr_id] = (pred_value, k_class, head)
                #if round(pred_value) == 1:
                #    pred_data[head['name']][curr_id] = k_class
                #    predictions.add(RuleGrounding.pred_str(head))

        # Recover max prediction, in case solution isn't perfect
        for head_name in max_scores:
            for curr_id in max_scores[head_name]:
                pred_value, k_class, head = max_scores[head_name][curr_id]
                pred_data[head_name][curr_id] = k_class
                predictions.add(RuleGrounding.pred_str(head))

        # create ordered list of predictions to be used in sklearn
        for classif_name in gold_data:

            if classif_name not in pred_data or len(pred_data[classif_name]) != len(gold_data[classif_name]):
                continue

            for id_ in gold_data[classif_name]:
                gold_label = gold_data[classif_name][id_]
                pred_label = pred_data[classif_name][id_]
                metrics[classif_name]['gold_data'].append(gold_label)
                metrics[classif_name]['pred_data'].append(pred_label)
                metrics[classif_name]['ids'].append(id_)

        '''
        for pred in predictions:
            print(pred)
        '''
        return predictions, metrics

    def evaluate(self, gold_heads, binary_predicates=[], get_active_heads=False, latent_predicates=set()):
        # Evaluate multiclass classifiers
        metrics = {}

        heads_multiclass, metrics_multiclass =\
                self.get_multi_metrics(self.multiClassDict.items(),
                gold_heads, binary_predicates)
        heads_binary, metrics_binary = \
                self.get_binary_metrics(self.binaryDict.items(), gold_heads, latent_predicates)

        if get_active_heads:
            active_heads = heads_multiclass | heads_binary
        else:
            active_heads = None

        metrics.update(metrics_multiclass)
        metrics.update(metrics_binary)

        #print(active_heads)
        return metrics, active_heads
