import torch
import numpy as np
from nn_model import NeuralNetworks, ModelType
import torch.nn.functional as F

class FF_2inputs(NeuralNetworks):

    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(FF_2inputs, self).__init__(config, nn_id)
        self.type = ModelType.FF
        self.use_gpu = use_gpu
        self.output_dim = output_dim

    def build_architecture(self, rule_template, fe, shared_params):
        self.minibatch_size = self.config["batch_size"]

        if "shared_layer1" in self.config:
            name = self.config["shared_layer1"]
            self.layer1 = shared_params[name]["layer"]
            n_out1 = shared_params[name]["nout"]
        else:
            self.layer1 = torch.nn.Linear(
                self.config["n_input_1"], self.config["n_hidden_1"])
            n_out1 = self.config["n_hidden_1"]


        if "shared_layer2" in self.config:
            name = self.config["shared_layer2"]
            self.layer2 = shared_params[name]["layer"]
            n_out2 = shared_params[name]["nout"]
        else:
            self.layer2 = torch.nn.Linear(
                self.config["n_input_2"], self.config["n_hidden_2"])
            n_out2 = self.config["n_hidden_2"]

        self.concat2hidden = torch.nn.Linear(
                n_out1 + n_out2,
                self.config["n_hidden_concat"])
        self.hidden2label = torch.nn.Linear(
                    self.config["n_hidden_concat"], self.output_dim)

        if self.use_gpu:
            self.layer1 = self.layer1.cuda()
            self.layer2 = self.layer2.cuda()
            self.concat2hidden = self.concat2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x, debug=False):
        x1 = [elem[0] for elem in x['input']]
        x2 = [elem[1] for elem in x['input']]
        tensor1 = self._get_float_tensor(x1)
        tensor2 = self._get_float_tensor(x2)

        var1 = self._get_grad_variable(tensor1)
        var2 = self._get_grad_variable(tensor2)

        hidden1 = self.layer1(var1)
        hidden2 = self.layer2(var2)

        out = F.relu(torch.cat([hidden1, hidden2], 1))
        out = self.concat2hidden(out)
        out = F.relu(out)
        logits = self.hidden2label(out)
        probas = F.softmax(logits, dim=1)
        

        if debug:
            print tensor1
            print tensor2
            print probas

        return logits, probas

