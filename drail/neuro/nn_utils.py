import os
import sys
import time
import random
import logging

import torch
from torch.autograd import Variable
import numpy as np
from sklearn.metrics import *
from collections import Counter
from transformers import AdamW
from transformers import get_linear_schedule_with_warmup
from transformers import AutoModel
#from allennlp.modules.elmo import Elmo
from tqdm import tqdm, trange

from .nn_model import ModelType

logger = logging.getLogger(__name__)
# random.seed(1234)     # always set seed in the main


def create_layer(definition):
    return torch.nn.Linear(definition['nin'], definition['nout'])

def create_lstm(definition):
    return torch.nn.LSTM(
            input_size=definition['nin'],
            hidden_size=definition['nout'],
            bidirectional=definition['bidirectional'],
            batch_first=True)

def create_bert(bert_model="bert-base-uncased"):
    return AutoModel.from_pretrained(bert_model)

def create_elmo(options_file, weight_file):
    return Elmo(options_file, weight_file, 1, dropout=0)


def compute_weights(Y, output_dim):
    counts = np.bincount(Y)
    num_missing = output_dim - len(counts)
    counts = np.append(counts, np.asarray([0]*num_missing))

    # smoothing
    counts[counts == 0] = 1
    #compute weights
    weights = np.asarray(np.max(counts)*1.0/counts, dtype = "float32")
    return weights
    #eg_weights = [weights[Y[idx]] for idx in range(len(Y))]
    #return np.asarray(eg_weights, dtype='float32')

def measure(y, y_, measurement, average, pos_label):
    # normalize type of y_
    if not isinstance(y_, list):
        y_ = y_.cpu()

    if measurement == "acc":
        if isinstance(y_, list):
            acc = accuracy_score(y, y_)
        else:
            acc = accuracy_score(y, y_.cpu())
        # logger.debug("accuracy_score: {}".format(acc))
        return acc
    elif measurement == "f1":
        if isinstance(y_, list):
            f1 = f1_score(y, y_, average = average, pos_label= pos_label)
        else:
            f1 = f1_score(y, y_.cpu(), average = average, pos_label= pos_label)
        # logger.debug("f1_score: {}".format(f1))
        return f1
    elif measurement == "recall":
        if isinstance(y_, list):
            recall = recall_score(y, y_, average = average, pos_label= pos_label)
        else:
            recall = recall_score(y, y_.cpu(), average = average, pos_label= pos_label)
        # logger.debug("f1_score: {}".format(f1))
        return recall


def index_excerpt(inputs, targets, excerpt, incl_targets=True, ids=None):
    ret = {'vector': [], 'input': [], 'nonuminput': [], 'embedding': {}, 'out': []}
    if len(inputs['vector']) > 0:
        ret['vector'] = [inputs['vector'][i] for i in excerpt]
    if len(inputs['input']) > 0:
        ret['input'] = [inputs['input'][i] for i in excerpt]
    if len(inputs['nonuminput']) > 0:
        ret['nonuminput'] = [inputs['nonuminput'][i] for i in excerpt]
    ret['embedding'] = {}
    for emb in inputs['embedding']:
        ret['embedding'][emb] = [inputs['embedding'][emb][i] for i in excerpt]

    finals = (ret, )
    if incl_targets:
        finals = finals + ([targets[i] for i in excerpt],)
    if ids:
        finals = finals + ([ids[i] for i in excerpt],)
    if len(finals) == 1:
        return finals[0]
    return finals

def sort_sequences(inputs, targets, sort_by, incl_targets=True):

    if sort_by[0] in ['input', 'embedding']:
        seqs = [elem[sort_by[1]] for elem in inputs[sort_by[0]]]
    elif sort_by[0] == 'vector':
        seqs = inputs['vector']
    else:
        logger.error("Invalid sort by input")
        exit()

    seq_lengths = torch.LongTensor(map(len, seqs))
    max_seq_len = seq_lengths.max()
    seq_lengths_sorted, perm_idx = seq_lengths.sort(0, descending=True)

    if not incl_targets:
        ret = index_excerpt(inputs, targets, perm_idx, incl_targets)
        return ret, perm_idx
    else:
        ret, targets_sorted = index_excerpt(inputs, targets, perm_idx, incl_targets)
        return ret, targets_sorted, perm_idx

def unsort_sequences(output, perm_idx, extract_item=False):
    _, original_idx = perm_idx.sort(0)
    if extract_item:
        return [output[i].item() for i in original_idx]
    return [output[i] for i in original_idx]

def balance_batch_classes(batched_Y):
    count_classes = Counter(batched_Y)
    least_elem, least_count = count_classes.most_common()[-1]
    counts = {}; excerpt = []; ret_Y = []
    for i, y in enumerate(batched_Y):
        if y not in counts:
            counts[y] = 0
        counts[y] += 1
        if counts[y] <= least_count:
            excerpt.append(i)
            ret_Y.append(y)
    return excerpt, ret_Y

def iterate_minibatches(inputs, targets, batchsize, incl_targets=True, shuffle=False, ids=None, balance_classes=False):
    indices = np.arange(0, get_batch_length(inputs))
    if shuffle:
       random.shuffle(indices)

    if balance_classes:
        indices, ret_Y = balance_batch_classes(targets)
        print ("Targets", Counter(ret_Y))
    n_size = len(indices)

    for start_idx in range(0, n_size, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield index_excerpt(inputs, targets, excerpt, incl_targets, ids=ids)

def train_local(model, model_lambda, optimizer, loss_fn, X, Y, ids, n_batchsize, class_weights,
          measure_type='acc', avg_type='binary', pos_label=1, sort=False,
          sort_by=None, output='softmax', num_batches=None, max_grad_norm=1.0,
          scheduler=None, clipping=False, evaluator=None, balance_classes=False):

    train_batches = 0; train_loss_sum = 0.0; train_measure_sum = 0.0
    model.train()
    pbar = tqdm(total=get_batch_length(X) / n_batchsize)
    for (batched_X, batched_Y, batched_ids) in \
         iterate_minibatches(X, Y, n_batchsize, shuffle=True, ids=ids, balance_classes=balance_classes):
        #print Counter(batched_Y)

        '''
        if sort and sort_by is not None:
            batched_X, batched_Y, _ = \
                sort_sequences(batched_X, batched_Y, sort_by)
        '''
        model.zero_grad()
        model.minibatch_size = get_batch_length(batched_X)
        out = model(batched_X)
        logits = out[0]; probas = out[1]

        if output == 'softmax':
            _, train_Y_pred = torch.max(probas, 1)
        elif output == 'sigmoid':
            train_Y_pred = torch.round(probas).data.long()

            '''
            print "pred"
            print train_Y_pred
            print "-- gold"
            print batched_Y
            print "-------"
            '''
        #train_loss = model.global_loss(batched_X, train_Y_pred.data.long(), batched_Y)

        # Compute and accumulate loss
        #train_loss = model.global_loss(batched_X, train_Y_pred, batched_Y)
        #print len(batched_X['input']), len(batched_Y)
        #print probas.size()
        #print loss_fn
        train_loss = model.pytorch_loss(probas, batched_Y, loss_fn, output)

        train_loss_sum += train_loss.item()
        if evaluator is None:
            train_measure_sum += measure(batched_Y, train_Y_pred, measure_type, avg_type, pos_label)
        else:
            train_measure_sum += evaluator.measure(train_Y_pred, batched_Y, batched_ids)

        train_batches += 1

        # Zero gradients, perform a backward pass, and update the weights.
        # I-Ta: don't need lambda for local nn
        # train_loss = model_lambda * train_loss
        train_loss.backward()

        if clipping:
            torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)

        if scheduler is not None:
            scheduler.step()

        optimizer.step()

        if num_batches is not None and train_batches == num_batches:
            break

        del train_loss
        del out
        del logits
        del probas

        pbar.update(1)
    pbar.close()
    #print "---"
    return train_batches, train_loss_sum, train_measure_sum

def get_batch_length(X):
    _max = max(len(X['vector']), len(X['input']))
    if 'nonuminput' in X:
        _max = max(_max, len(X['nonuminput']))
    for emb in X['embedding']:
        _max = max(_max, len(X['embedding'][emb]))
    return _max

def predict_local(model, X, sort=False, sort_by=None, output_pred='softmax'):
    with torch.no_grad():
        model.eval()
        y_pred = []
        if 'pred_size' not in model.config:
            batch_size = get_batch_length(X)
        else:
            batch_size = model.config['pred_size']
        pbar = tqdm(get_batch_length(X) / batch_size)
        for (batched_X) in iterate_minibatches(X, None, batch_size, incl_targets=False):

            '''
            if sort and sort_by is not None:
                batched_X, perm_idx = sort_sequences(batched_X, None, sort_by, incl_targets=False)
            '''

            model.minibatch_size = get_batch_length(batched_X)
            out = model(batched_X)
            logits = out[0]; probas = out[1]

            if output_pred == 'softmax':
                _, y_pred_batch = torch.max(probas, 1)
                y_pred_batch = y_pred_batch.data
            elif output_pred == 'sigmoid':
                y_pred_batch = torch.round(probas).data.long()

            # if I sorted, now need to unsort
            '''
            if sort and sort_by is not None:
                #print y_pred_batch
                y_pred_batch = unsort_sequences(y_pred_batch, perm_idx, extract_item=(output_pred=='softmax'))
            else:
                y_pred_batch = list(y_pred_batch.cpu().numpy())
            '''
            y_pred_batch = list(y_pred_batch.cpu().numpy())
            y_pred += y_pred_batch
            pbar.update(1)
        pbar.close()
        return y_pred

def eval_local(model, X, Y, ids, loss_fn, measurement, average, pos_label, sort=False, sort_by=None,
               output_pred='softmax', evaluator=None):
    with torch.no_grad():
        eval_batches = 0; eval_loss_sum = 0.0; eval_measure_sum = 0.0
        model.eval()
        if 'pred_size' not in model.config:
            batch_size = get_batch_length(X)
        else:
            batch_size = model.config['pred_size']
        pbar = tqdm(total=get_batch_length(X) / batch_size)
        for (batched_X, batched_Y, batched_ids) in iterate_minibatches(X, Y, batch_size, shuffle=True, ids=ids):
            '''
            if sort and sort_by is not None:
                batched_X, batched_Y, _ = \
                    sort_sequences(batched_X, batched_Y, sort_by)
            '''
            model.minibatch_size = get_batch_length(batched_X)
            out = model(batched_X)
            logits = out[0]; probas = out[1]

            # Multiclass or binary
            if output_pred == 'softmax':
                _, y_pred_batch = torch.max(probas, 1)
                if evaluator is None:
                    eval_measure_sum += measure(batched_Y, y_pred_batch, measurement, average, pos_label)
                else:
                    eval_measure_sum += evaluator.measure(y_pred_batch, batched_Y, ids=batched_ids)
            elif output_pred == 'sigmoid':
                # Multilabel
                y_pred_batch = torch.round(probas).data.long()
                if evaluator is None:
                    eval_measure_sum += measure(batched_Y, y_pred_batch, measurement, average, pos_label)
                else:
                    eval_measure_sum += evaluator.measure(y_pred_batch, batched_Y, ids=batched_ids)

            #print(Counter(list(batched_Y)))
            #print(probas.shape)
            eval_loss = model.pytorch_loss(probas, batched_Y, loss_fn, output_pred)
            eval_loss_sum += eval_loss.item()

            eval_batches += 1
            pbar.update(1)

            del eval_loss
            del out
            del logits
            del probas

        pbar.close()
        return eval_batches, eval_loss_sum, eval_measure_sum

def predict_local_scores(model, X, probas=False, debug=False):
    with torch.no_grad():
        sort=False; sort_by=None
        if "sort_sequence" in model.config:
            sort=model.config['sort_sequence']
            sort_by=model.config["sort_by"]
        model.eval()
        output = np.asarray([])
        #logits_all = None
        if 'pred_size' not in model.config:
            batch_size = get_batch_length(X)
        else:
            batch_size = model.config['pred_size']
        #print(batch_size)
        for (batched_X) in iterate_minibatches(X, None, batch_size, incl_targets=False):
            '''
            if sort and sort_by is not None:
                batched_X, perm_idx = sort_sequences(batched_X, None, sort_by, incl_targets=False)
            '''

            model.minibatch_size = get_batch_length(batched_X)
            out = model(batched_X)
            logits = out[0]; _probas = out[1]
            if probas:
                output_batch = _probas.data
            else:
                output_batch = logits.data
            output_batch = output_batch.cpu().numpy()

            '''
            # if I sorted, now need to unsort
            if sort and sort_by is not None:
                output_batch = unsort_sequences(output_batch, perm_idx)
            '''
            if output.shape[0] == 0:
                output = output_batch
            elif len(output_batch.shape) > 1:
                output = np.vstack([output, output_batch])
            else:
                output = np.concatenate([output, output_batch])

        #logits = torch.cat(logits_all, 1)
        #print(logits.size())
        #exit()
        ret = np.asarray(output)
        return ret

def predict_local_embeddings(model, X, debug=False):
    with torch.no_grad():
        sort=False; sort_by=None
        if "sort_sequence" in model.config:
            sort=model.config['sort_sequence']
            sort_by=model.config["sort_by"]
        model.eval()

        output = []

        if 'pred_size' not in model.config:
            batch_size = get_batch_length(X)
        else:
            batch_size = model.config['pred_size']
        for (batched_X) in iterate_minibatches(X, None, batch_size, incl_targets=False):
            '''
            if sort and sort_by is not None:
                batched_X, perm_idx = sort_sequences(batched_X, None, sort_by, incl_targets=False)
            '''
            model.minibatch_size = get_batch_length(batched_X)
            embeddings = model.get_embeddings(batched_X)
            if not isinstance(embeddings, tuple):
                logger.error("Expected a type {} from your neural get_embedding method, but got {} instead".format(tuple, type(embeddings)))
                exit(-1)

            n = len(embeddings[0])
            
            current_outputs = [ [None] * len(embeddings) ] * n # num_examples x num_embeddings x embedding_size
            #print(len(current_outputs), len(current_outputs[0]))
            # i : embedding_type
            for i in range(len(embeddings)):
                # j : sample
                for j in range(n):
                    #print(i, j)
                    current_outputs[j][i] = embeddings[i][j].cpu().detach().tolist()
            output += current_outputs
            #exit()
        return output

def backpropagate_global(model, optimizer, X_ls, y_pred_ls, y_pred_index_ls,
                        y_gold_ls, y_gold_index_ls, y_sol_pool_ls, y_sol_pool_index_ls, accum,
                        loss_fn):
    model.train()
    model.zero_grad()

    if loss_fn == "hinge":
        loss = model.hinge_loss(X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, average=False)
    elif loss_fn == "crf":
        loss = model.crf_loss(X_ls, y_sol_pool_ls, y_sol_pool_index_ls, y_pred_ls, y_pred_index_ls)
        loss.backward()
    elif loss_fn == "hinge_smooth":
        loss = model.hinge_smooth(X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, accum)
    else:
        loss = model.sum_local_loss(X_ls, y_gold_ls, y_gold_index_ls, accum)
    optimizer.step()
    return loss

# TO-DO: Deal with eval loss
def eval_loss(model, X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, 
              y_sol_pool_ls, y_sol_pool_index_ls, accum, loss_fn):
    with torch.no_grad():
        model.eval()
        if loss_fn == "hinge":
            loss = model.hinge_loss(X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, average=True)
        elif loss_fn == "crf":
            loss = model.crf_loss(X_ls, y_sol_pool_ls, y_sol_pool_index_ls, y_pred_ls, y_pred_index_ls)
        elif loss_fn == "hinge_smooth":
            loss = model.hinge_smooth(X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, accum)
        else:
            loss = model.sum_local_loss(X_ls, y_gold_ls, y_gold_index_ls, accum)
        return loss

def get_model_params(model):
    n_batchsize = model.config["batch_size"]
    patience = model.config["patience"]
    learning_rate = model.config["learning_rate"]
    n_predsize = None

    # defaults. TO-DO add defaults for the rest of the params
    measurement = "acc"; average = "binary"; weighted_examples=False
    pos_label = 1; sort=False; sort_by=None; output='softmax'
    weight_decay=0.0; adam_epsilon=1e-8; max_grad_norm = 1.0,
    clipping = False; balance_classes = False; 

    if "measurement" in model.config:
        measurement = model.config["measurement"]
    if "average" in model.config:
        average = model.config["average"]
    if "weighted_examples" in model.config:
        weighted_examples = model.config["weighted_examples"]
    print_stat = measurement
    if "print_stat" in model.config:
        print_stat = model.config['print_stat']
    if "pos_label" in model.config:
        pos_label = int(model.config["pos_label"])
    if "sort_sequence" in model.config:
        sort=True; sort_by=model.config["sort_by"]
    if "output" in model.config:
        output=model.config["output"]
    if "weight_decay" in model.config:
        weight_decay = model.config["weight_decay"]
    if "adam_epsilon" in model.config:
        adam_epsilon = model.config["adam_epsilon"]
    if "max_grad_norm" in model.config:
        max_grad_norm = model.config["max_grad_norm"]
    if "clipping" in model.config:
        clipping = model.config["clipping"]
    if "balance_classes" in model.config:
        balance_classes = model.config["balance_classes"]

    return (n_batchsize, patience, n_predsize, measurement, average,
            weighted_examples, pos_label, sort, sort_by, print_stat,
            output, weight_decay, learning_rate, adam_epsilon,
            max_grad_norm, clipping, balance_classes)

def initialize_model(model, savedir):
    #if "continue_from_checkpoint" in model.config and \
    #   model.config["continue_from_checkpoint"]:
    if os.path.isfile(savedir):
        logger.info("Initializing model from {}...".format(savedir))
        model.load_state_dict(torch.load(savedir, map_location=lambda storage, loc: storage))
    else:
        logger.info("Model couldn't be found in {}".format(savedir))
        logger.info("Overwritting directory...")

def train_local_on_epoches(model, model_lambda,
        TrainX, TrainY, train_ids,
        DevX, DevY, dev_ids,
        TestX, TestY, test_ids,
        savedir, optimizer_name, fe, rule_template,
        evaluator=None, tb_writer=None, tb_prefix='', n_epochs=-1):
    logger.info("Neural Networks {} Training Started..".format(model.nn_id))

    n_batchsize, patience, n_predsize, measurement, average, \
    weighted_examples, pos_label, sort, sort_by, \
    print_stat, output, weight_decay, learning_rate, \
    adam_epsilon, max_grad_norm, clipping, balance_classes = \
        get_model_params(model)
    logger.info("using measurement={}, average={}".format(measurement, average))

    # print n_batchsize
    best_val_measure = -float("infinity")
    done_looping = False
    patience_counter = 0

    epoch = 0
    start_time = time.time()

    if weighted_examples and not balance_classes:
        class_weights = torch.from_numpy(compute_weights(TrainY, model.output_dim))
        if model.use_gpu:
            class_weights = class_weights.cuda()
    else:
        class_weights = None

    # THIS IS HARDCODED, NEED TO ALLOW FOR DIFFERENT OPT AND LOSSES
    if output == 'softmax':
        loss_fn = torch.nn.CrossEntropyLoss(weight=class_weights)
        logger.info("class_weights {}".format(class_weights))
    elif output == 'sigmoid':
        loss_fn = torch.nn.BCEWithLogitsLoss()
    #loss_fn = None

    scheduler = None
    if optimizer_name == 'SGD':
        optimizer = torch.optim.SGD(model.parameters(),
                                    lr=learning_rate)
    elif optimizer_name == 'Adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                 lr=learning_rate)
    elif optimizer_name == 'AdamW':
        optimizer = torch.optim.AdamW(model.parameters(), lr=learning_rate)
    # This is transformer-specific
    elif optimizer_name == 'AdamW-transformers':
        # Prepare optimizer and schedule (linear warmup and decay)
        no_decay = ['bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
            {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
                                        ]
        optimizer = AdamW(optimizer_grouped_parameters, lr=learning_rate, eps=adam_epsilon)
		# TO-DO: how to deal with scheduler when the number of epochs is unknown and we rely on patience?
		# Hard-coding 3 epochs and 0 warmup steps
        t_total = len(TrainY) * 3.0; warmup_steps=0
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=warmup_steps, num_training_steps=t_total)

    if patience == 0:
        done_looping = True

    eval_batches, eval_loss_sum, eval_measure_sum = \
        eval_local(model, DevX, DevY, dev_ids, loss_fn,
                   measurement, average, pos_label, sort, sort_by, output_pred=output, evaluator=evaluator)

    val_loss = eval_loss_sum / (1.0 * eval_batches)
    val_measure = eval_measure_sum / (1.0 * eval_batches)
    best_val_measure = val_measure

    if tb_writer:
        tb_writer.add_scalar('{}_val_loss'.format(tb_prefix), val_loss, epoch)
        tb_writer.add_scalar('{}_val_measure'.format(tb_prefix), val_measure, epoch)
    logger.info("STARTING POINT --- epoch {}, val_loss {}, val_measure {}".format(epoch, val_loss, val_measure))
    #exit()
    best_epoch = epoch
    while (not done_looping):
    #for i in range(5):
        epoch += 1

        epoch_start_time = time.time()
        train_batches, train_loss_sum, train_measure_sum = \
            train_local(model, model_lambda, optimizer, loss_fn,
                        TrainX, TrainY, train_ids, n_batchsize, class_weights,
                        measurement, average, pos_label, sort, sort_by, output=output,
                        clipping=clipping, evaluator=evaluator, balance_classes=balance_classes)
        epoch_end_time = time.time()
        #print("epoch {}: {:.3f}s".format(epoch, epoch_end_time - epoch_start_time))

        if 'use_gpu' in model.config and model.config['use_gpu']:
            #print("empty cache")
            torch.cuda.empty_cache()

        eval_batches, eval_loss_sum, eval_measure_sum = \
            eval_local(model, DevX, DevY, dev_ids, loss_fn,
                       measurement, average, pos_label, sort, sort_by, output_pred=output, evaluator=evaluator)

        if 'use_gpu' in model.config and model.config['use_gpu']:
            torch.cuda.empty_cache()

        measure_end_time = time.time()
        train_loss = train_loss_sum / (1.0 * train_batches)
        train_measure = train_measure_sum / (1.0 * train_batches)
        # I-Ta: don't need lambda for local nn
        val_loss = (eval_loss_sum / (1.0 * eval_batches))
        # val_loss = model_lambda * (eval_loss_sum / (1.0 * eval_batches))
        val_measure = eval_measure_sum / (1.0 * eval_batches)

        logger.info("epoch {}, train_loss {}, val_loss {}, train_measure {}, val_measure {}, time {:.3f}s".format(epoch, train_loss, val_loss, train_measure, val_measure, measure_end_time - epoch_start_time))
        if tb_writer:
            tb_writer.add_scalar('{}_train_loss'.format(tb_prefix), train_loss, epoch)
            tb_writer.add_scalar('{}_val_loss'.format(tb_prefix), val_loss, epoch)
            tb_writer.add_scalar('{}_train_measure'.format(tb_prefix), train_measure, epoch)
            tb_writer.add_scalar('{}_val_measure'.format(tb_prefix), val_measure, epoch)

        if n_epochs > 0 and epoch == n_epochs:
            done_looping = True

        if val_measure > best_val_measure:
            #print "epoch", epoch, "train_loss", train_loss, "val_loss", val_loss, "train_measure", train_measure, "val_measure", val_measure, "time {:.3f}s".format(measure_end_time - epoch_start_time)
            patience_counter = 0
            best_val_measure = val_measure
            best_epoch = epoch
            model_state = model.state_dict()
            torch.save(model_state, savedir)
        elif n_epochs < 0:
            # Have to save model in case it does not exist
            if epoch == 1:
                model_state = model.state_dict()
                torch.save(model_state, savedir)

            patience_counter += 1
            if patience_counter >= patience:
                done_looping = True

    model.load_state_dict(torch.load(savedir))
    logger.info("loading best model (epoch {})".format(best_epoch))

    if TestX is not None and TestY is not None and len(TestY) > 0:
        test_Y_pred = predict_local(model, TestX, sort, sort_by, output_pred=output)
        if evaluator is None:
            test_measure = measure(TestY, test_Y_pred, measurement, average, pos_label)
        else:
            test_measure = evaluator.measure(TestY, test_Y_pred, ids=test_ids)
        logger.info("TEST Measure = {}".format(test_measure))
        if tb_writer:
            tb_writer.add_scalar('{}_test_measure'.format(tb_prefix), test_measure, best_epoch)
        logger.info("\tTest scores")
        logger.info(classification_report(TestY, test_Y_pred, digits=4))
        logger.info("MACRO precision_recall_fscore_support = {}".format(precision_recall_fscore_support(TestY, test_Y_pred, average='macro')))
        logger.info("MICRO precision_recall_fscore_support = {}".format(precision_recall_fscore_support(TestY, test_Y_pred, average='micro')))
        logger.info("acc {}".format(accuracy_score(TestY, test_Y_pred)))
        
        # I-Ta: ignore some null majority classes for measurement
        if 'labels_to_ignore' in model.config and len(model.config['labels_to_ignore']) > 0:
            all_label_idxs = set(TestY)
            ignored_label_idxs = set([fe.label2index(rule_template.head.name, l) for l in model.config['labels_to_ignore'] if fe.is_label_valid(rule_template.head.name, l)])
            valid_label_idxs = sorted(list(all_label_idxs - ignored_label_idxs))
            logger.info("=====IGNORE {}=====".format(model.config['labels_to_ignore']))
            logger.debug("valid_label_idxs={}".format(valid_label_idxs))
            logger.info("MACRO precision_recall_fscore_support = {}".format(precision_recall_fscore_support(TestY, test_Y_pred, average='macro', labels=valid_label_idxs)))
            logger.info("MICRO precision_recall_fscore_support = {}".format(precision_recall_fscore_support(TestY, test_Y_pred, average='micro', labels=valid_label_idxs)))
            logger.info("===================")

    logger.info("\nTraining {} Epoches took {:.3f}s".format(epoch, time.time() - start_time))
    return None
