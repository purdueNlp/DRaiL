import torch
import numpy as np
import gc

from . import nn_utils

class NeuralHub(torch.nn.Module):

    def __init__(self, nn_list, nn_open_lhs, nn_lambda_list, learning_rate,
                 use_gpu, l1_lambda, l2_lambda):
        super(NeuralHub, self).__init__()
        self.potentials = torch.nn.ModuleList(nn_list)
        self.nn_open_lhs = nn_open_lhs
        self.lambdas = nn_lambda_list
        self.lr = learning_rate
        self.use_gpu = use_gpu
        self.l1_lambda = l1_lambda
        self.l2_lambda = l2_lambda
        self.local_losses = [None] * len(nn_list)
        self.class_weights = []

    def set_local_losses(self, class_weights):
        for nnidx, cw in enumerate(class_weights):
            self.local_losses[nnidx] = torch.nn.CrossEntropyLoss(weight=cw)
            print(self.local_losses[nnidx], cw)
            self.class_weights.append(cw)

    def _l1_penalty(self):
        if self.use_gpu:
            l1_norm = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        else:
            l1_norm = torch.autograd.Variable(torch.FloatTensor([0]))

        for W in self.parameters():
            l1_norm += torch.abs(W).sum()
        return l1_norm

    def _l2_penalty(self):
        if self.use_gpu:
            l2_norm = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        else:
            l2_norm = torch.autograd.Variable(torch.FloatTensor([0]))
        for W in self.parameters():
            l2_norm += torch.sqrt(torch.pow(W, 2).sum())
        return l2_norm

    # QUESTION: should we return logits or probas?
    def _potential_scores(self, index, X):
        '''
        sort=False; sort_by=None
        if "sort_sequence" in self.potentials[index].config:
            sort=self.potentials[index].config["sort_sequence"]
            sort_by=self.potentials[index].config["sort_by"]

        if sort and sort_by is not None:
            X, perm_idx = nn_utils.sort_sequences(X, None, sort_by, incl_targets=False)
            logits, probas = self.potentials[index](X)
            logits = logits[perm_idx, :]
            probas = probas[perm_idx, :]
        else:
            logits, probas = self.potentials[index](X)
        '''
        out = self.potentials[index](X)
        logits = out[0]; probas = out[1]

        return self.lambdas[index] * logits
        #return probas

    # WARNING: this is ok right now because gold_index and pred_index are the same always
    def hinge_smooth(self, X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls, accum):
        if self.use_gpu and accum:
            score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        elif accum:
            score = torch.autograd.Variable(torch.FloatTensor([0]))
        else:
            score = 0

        for i in range(0, len(self.potentials)):

            if len(X_ls[i]) == 0:
                continue

            config = self.potentials[i].config
            if 'pred_size' not in config:
                batch_size = nn_utils.get_batch_length(X_ls[i])
            else:
                batch_size = config['pred_size']

            n_size = nn_utils.get_batch_length(X_ls[i])

            for j in range(0, n_size, batch_size):
                threshold = min(j+batch_size, n_size)
                X_ls_batch = nn_utils.index_excerpt(X_ls[i], None, range(j, threshold), incl_targets=False)
                # the j is subtracted to get the relative position to the minibatch
                y_gold_index_ls_batch = y_gold_index_ls[i][j:j+batch_size] - j
                y_gold_ls_batch = y_gold_ls[i][j:j+batch_size]
                y_pred_ls_batch = y_pred_ls[i][j:j+batch_size]

                potential_scores = self._potential_scores(i, X_ls_batch)
                scores = potential_scores[y_gold_index_ls_batch, :]
                _, y_pred_local = torch.max(scores, 1)

                relevant = y_pred_ls_batch != y_gold_ls_batch
                #local_agreement = y_pred_ls_batch == y_pred_local.cpu().numpy()
                #relevant = np.logical_or(relevant, local_agreement)

                idx = np.argwhere(relevant).flatten()

                if len(idx) > 0:
                    #print("Loss")
                    scores = scores[idx]
                    y_gold_ls_batch = y_gold_ls_batch[idx]

                    if self.use_gpu:
                        y_gold = torch.cuda.LongTensor(y_gold_ls_batch)
                    else:
                        y_gold = torch.LongTensor(y_gold_ls_batch)
                    y_gold = torch.autograd.Variable(y_gold)

                    #print("calculating losses...")
                    if accum:
                        score += self.local_losses[i](scores, y_gold)
                    else:
                        loss = self.local_losses[i](scores, y_gold)

                        if self.training and loss > 0:
                            loss.backward()
                        score += loss.item()
                else:
                    pass
                    #print("No loss")

        if not accum:
            if self.training:
                score.backward()
            score = score.item()
        return score


    # WARNING: this is ok right now because gold_index and pred_index are the same always
    def sum_local_loss(self, X_ls, y_gold_ls, y_gold_index_ls, accum):
        if self.use_gpu and accum:
            score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        elif accum:
            score = torch.autograd.Variable(torch.FloatTensor([0]))
        else:
            score = 0

        for i in range(0, len(self.potentials)):
            if len(X_ls[i]) == 0:
                continue

            n_instance_size = nn_utils.get_batch_length(X_ls[i])

            config = self.potentials[i].config
            if 'pred_size' not in config:
                batch_size = nn_utils.get_batch_length(X_ls[i])
            else:
                batch_size = config['pred_size']

            n_size = nn_utils.get_batch_length(X_ls[i])

            for j in range(0, n_size, batch_size):

                threshold = min(j+batch_size, n_size)
                X_ls_batch = nn_utils.index_excerpt(X_ls[i], None, range(j, threshold), incl_targets=False)
                # the j is subtracted to get the relative position to the minibatch
                y_gold_index_ls_batch = y_gold_index_ls[i][j:j+batch_size] - j
                y_gold_ls_batch = y_gold_ls[i][j:j+batch_size]

                potential_scores = self._potential_scores(i, X_ls_batch)

                #print(potential_scores.shape)
                #print(y_gold_index_ls_batch)

                scores = potential_scores[y_gold_index_ls_batch, :]

                #print("rix", i, scores.shape, len(y_gold_ls_batch), len(y_gold_index_ls_batch))
                #print(y_gold_index_ls_batch)
                #print(y_gold_ls_batch)

                if self.use_gpu:
                    y_gold = torch.cuda.LongTensor(y_gold_ls_batch)
                else:
                    y_gold = torch.LongTensor(y_gold_ls_batch)
                y_gold = torch.autograd.Variable(y_gold)

                #print("calculating losses...")
                if accum:
                    #print(scores)
                    #print(y_gold)
                    #print(scores.shape, y_gold.shape)
                    loss = self.local_losses[i](scores, y_gold)

                    if self.training:
                        loss.backward()
                    score += self.lambdas[i] * loss.item()
                else:
                    score += self.lambdas[i] * self.self.local_losses[i](scores, y_gold)

        if not accum:
            if self.training:
                score.backward()
            score = score.item()
        return score

    def _score_instance(self, X_ls, y_ls, y_index_ls):
        if self.use_gpu:
            score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        else:
            score = torch.autograd.Variable(torch.FloatTensor([0]))

        #print "y_ls", "y_index_ls", y_ls, y_index_ls
        #print "X_ls", len(X_ls)
        for i in range(0, len(self.potentials)):
            if len(X_ls[i]) == 0:
                continue
            config = self.potentials[i].config
            X = X_ls[i]; y = y_ls[i]; y_index = y_index_ls[i]
            potential_scores = self._potential_scores(i, X)
            #print("====")
            #print "y_index", y_index
            #print "y", y
            score += potential_scores[y_index, y].sum()
        return score

    # Maxim used for log sum exp trick: https://towardsdatascience.com/implementing-a-linear-chain-conditional-random-field-crf-in-pytorch-16b0b9c4b4ea
    def crf_loss(self, X_ls, y_sol_pool_ls, y_sol_pool_index_ls, y_gold_ls, y_gold_index_ls):
        #print("Solution pool", len(y_sol_pool_ls), len(y_sol_pool_index_ls))

        # Gold
        gold_score = self._score_instance(X_ls, y_gold_ls, y_gold_index_ls)

        # Partition function
        if self.use_gpu:
            partition_score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        else:
            partition_score = torch.autograd.Variable(torch.cuda.FloatTensor([0]))

        scores = []
        for y_sol_pool_ls_i, y_sol_pool_index_ls_i in zip(y_sol_pool_ls, y_sol_pool_index_ls):
            #print(y_sol_pool_ls_i, y_sol_pool_index_ls_i)
            score = self._score_instance(X_ls, y_sol_pool_ls_i, y_sol_pool_index_ls_i)
            scores.append(score)
        maxim = max(scores)
        for score in scores:
            partition_score += torch.exp(score - maxim)
        del scores
        partition_score = maxim + torch.log(partition_score)

        # Loss
        loss = partition_score - gold_score
        ret = loss.item()
        if self.training:
            loss.backward()
        return ret


    def _hinge_open_lhs(self, X_ls, y_pred_ls, y_pred_index_ls,
                        y_gold_ls, y_gold_index_ls, i, average, n, score):
        if len(X_ls[i]) == 0:
            return n, score

        if self.use_gpu:
            pred_score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
            gold_score = torch.autograd.Variable(torch.cuda.FloatTensor([0])).cuda()
        else:
            pred_score = torch.autograd.Variable(torch.FloatTensor([0]))
            gold_score = torch.autograd.Variable(torch.FloatTensor([0]))

        config = self.potentials[i].config
        if 'pred_size' not in config:
            batch_size = nn_utils.get_batch_length(X_ls[i])
        else:
            batch_size = config['pred_size']
        n_size = nn_utils.get_batch_length(X_ls[i])

        k_gold = 0; k_pred = 0
        for j in range(0, n_size, batch_size):
            threshold = min(j+batch_size, n_size)

            X = nn_utils.index_excerpt(X_ls[i], None, range(j, threshold), incl_targets=False)
            potential_scores = self._potential_scores(i, X)

            # Gold substructures
            y_gold_index = y_gold_index_ls[i]
            y_gold_index = y_gold_index[np.where(np.logical_and(y_gold_index >= j, y_gold_index < threshold))[0]]
            y_gold_index = y_gold_index - j
            y_gold = y_gold_ls[i][k_gold:k_gold + y_gold_index.shape[0]]
            k_gold += y_gold_index.shape[0]
            #print("y_gold_index", y_gold_index)
            #print("y_gold", y_gold)

            # Predicted substructures
            y_pred_index = y_pred_index_ls[i]
            y_pred_index = y_pred_index[np.where(np.logical_and(y_pred_index >= j, y_pred_index < threshold))[0]]
            y_pred_index = y_pred_index - j
            y_pred = y_pred_ls[i][k_pred:k_pred + y_pred_index.shape[0]]
            k_pred += y_pred_index.shape[0]
            #print("y_pred_index", y_pred_index)
            #print("y_pred", y_pred)
            #print('-----')
            '''
            if self.class_weights[i] is not None:
                pred_score = potential_scores[y_pred_index, y_pred]
                gold_score = potential_scores[y_gold_index, y_gold]

                # weigh the potentials
                weights_pred = [self.class_weights[i][y] for y in y_pred]
                weights_gold = [self.class_weights[i][y] for y in y_gold]
                pred_score = [p * w for (p,w) in zip(ps, weights_pred)]
                gold_score = [p * w for (p,w) in zip(gs, weights_gold)]

                pred_score = sum(ps)
                gold_score = sum(gs)

            else:
            '''

            m = len(y_pred_index)
            pred_score = potential_scores[y_pred_index, y_pred].sum()
            gold_score = potential_scores[y_gold_index, y_gold].sum()
            n += m

        loss = (pred_score - gold_score)
        if self.training:
            loss.backward()
        score += loss.item()

        return n, score

    def _hinge(self, X_ls, y_pred_ls, y_pred_index_ls,
               y_gold_ls, y_gold_index_ls, i, average, n, score):

        if len(X_ls[i]) == 0:
            return n, score

        config = self.potentials[i].config
        if 'pred_size' not in config:
            batch_size = nn_utils.get_batch_length(X_ls[i])
        else:
            batch_size = config['pred_size']

        n_size = nn_utils.get_batch_length(X_ls[i])
        #print(i, y_pred_index_ls[i], y_pred_ls[i])
        #print(i, y_gold_index_ls[i], y_gold_ls[i])

        for j in range(0, n_size, batch_size):
            threshold = min(j+batch_size, n_size)

            X = nn_utils.index_excerpt(X_ls[i], None, range(j, threshold), incl_targets=False)
            # the j is subtracted to get the relative position to the minibatch
            # Gold substructures
            y_gold_index = y_gold_index_ls[i][j:j+batch_size] - j
            y_gold = y_gold_ls[i][j:j+batch_size]
            # Pred substructures
            y_pred_index = y_pred_index_ls[i][j:j+batch_size] - j
            y_pred = y_pred_ls[i][j:j+batch_size]

            potential_scores = self._potential_scores(i, X)

            pred_scores = potential_scores[y_pred_index, y_pred]
            gold_scores = potential_scores[y_gold_index, y_gold]

            m = len(y_pred_index)
            loss = (pred_scores - gold_scores)
            #loss[loss < 0] = 0
            loss = loss.sum()

            if average:
                loss = loss / m
            if self.training:
                loss.backward()
            score += loss.item()
            n += m

        return n, score

    # TO-DO: Test binarizing the factors
    def hinge_loss(self, X_ls, y_pred_ls, y_pred_index_ls,
                     y_gold_ls, y_gold_index_ls, average=False):
        '''
        pred_score = self._score_instance(X_ls, y_pred_ls, y_pred_index_ls)
        gold_score = self._score_instance(X_ls, y_gold_ls, y_gold_index_ls)

        return pred_score - gold_score
        '''
        score = 0; n = 0
        for i in range(0, len(self.potentials)):
            if self.nn_open_lhs[i]:
                n, score = self._hinge_open_lhs(
                        X_ls, y_pred_ls, y_pred_index_ls,
                        y_gold_ls, y_gold_index_ls, i, average, n, score)
            else:
                n, score = self._hinge(
                        X_ls, y_pred_ls, y_pred_index_ls,
                        y_gold_ls, y_gold_index_ls, i, average, n, score)
        return score

