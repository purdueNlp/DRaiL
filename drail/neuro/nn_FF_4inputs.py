import torch
import numpy as np
from nn_model import NeuralNetworks, ModelType
import torch.nn.functional as F

class FF_4inputs(NeuralNetworks):
    def __init__(self, config, nn_id, use_gpu, output_dim):
        super(FF_4inputs, self).__init__(config, nn_id)
        self.type = ModelType.FF
        self.use_gpu = use_gpu
        self.output_dim = output_dim

    def build_architecture(self, rule_template, fe, shared_params):
        self.minibatch_size = self.config["batch_size"]


        if "shared_layer1" in self.config:
            name = self.config["shared_layer1"]
            self.layer1 = shared_params[name]["layer"]
            n_out1 = shared_params[name]["nout"]
        else:
            self.layer1 = torch.nn.Linear(
                self.config["n_input_1"], self.config["n_hidden_1"])
            n_out1 = self.config["n_hidden_1"]


        if "shared_layer2" in self.config:
            name = self.config["shared_layer2"]
            self.layer2 = shared_params[name]["layer"]
            n_out2 = shared_params[name]["nout"]
        else:
            self.layer2 = torch.nn.Linear(
                self.config["n_input_2"], self.config["n_hidden_2"])
            n_out2 = self.config["n_hidden_2"]

        if "shared_layer3" in self.config:
            name = self.config["shared_layer3"]
            self.layer3 = shared_params[name]["layer"]
            n_out3 = shared_params[name]["nout"]
        else:
            self.layer3 = torch.nn.Linear(
                self.config["n_input_3"], self.config["n_hidden_3"])
            n_out3 = self.config["n_hidden_3"]

        if "shared_layer4" in self.config:
            name = self.config["shared_layer4"]
            self.layer4 = shared_params[name]["layer"]
            n_out4 = shared_params[name]["nout"]
        else:
            self.layer4 = torch.nn.Linear(
                self.config["n_input_4"], self.config["n_hidden_4"])
            n_out4 = self.config["n_hidden_4"]

        self.concat2hidden = torch.nn.Linear(
                n_out1 + n_out2 + n_out3 + n_out4,
                self.config["n_hidden_concat"])
        self.hidden2label = torch.nn.Linear(
                    self.config["n_hidden_concat"], self.output_dim)

        if self.use_gpu:
            self.layer1 = self.layer1.cuda()
            self.layer2 = self.layer2.cuda()
            self.layer3 = self.layer3.cuda()
            self.layer4 = self.layer4.cuda()
            self.concat2hidden = self.concat2hidden.cuda()
            self.hidden2label = self.hidden2label.cuda()

    def forward(self, x):
        x1 = [elem[0] for elem in x['input']]
        x2 = [elem[1] for elem in x['input']]
        x3 = [elem[2] for elem in x['input']]
        x4 = [elem[3] for elem in x['input']]
        tensor1 = self._get_float_tensor(x1)
        tensor2 = self._get_float_tensor(x2)
        tensor3 = self._get_float_tensor(x3)
        tensor4 = self._get_float_tensor(x4)

        #print tensor1.size()
        #print tensor2.size()
        #print tensor3.size()
        #print tensor4.size()

        var1 = self._get_grad_variable(tensor1)
        var2 = self._get_grad_variable(tensor2)
        var3 = self._get_grad_variable(tensor3)
        var4 = self._get_grad_variable(tensor4)

        hidden1 = self.layer1(var1)
        hidden2 = self.layer2(var2)
        hidden3 = self.layer3(var3)
        hidden4 = self.layer4(var4)

        out = F.relu(torch.cat([hidden1, hidden2, hidden3, hidden4], 1))
        out = self.concat2hidden(out)
        out = F.relu(out)
        logits = self.hidden2label(out)
        probas = F.softmax(logits, dim=1)
        return logits, probas

