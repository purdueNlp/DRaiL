import pickle
import numpy as np
import os
from itertools import islice
from scipy.spatial.distance import cosine

from drail.features.feature_extractor import FeatureExtractor


class MoralFoundationFE(FeatureExtractor):
    def __init__(self, data_path):
        super(MoralFoundationFE, self).__init__()
        self.data_path = data_path

    def build(self):
        #print(self.data_f)
        # Load preprocessed data
        '''
        Uncomment for on-line prediction
        
        with open(os.path.join(self.data_path, 'drail_data_bert.pickle'), 'rb') as fp:
            [self.tweet_id2bert, self.entity_id2bert, self.ideology_id2one_hot] = pickle.load(fp, encoding="latin1")
            
        with open(os.path.join(self.data_path, 'one_hot_features.pickle'), 'rb') as fp:
            [self.mf2one_hot, self.ideology_id2one_hot, self.group2one_hot,
             self.topic2one_hot, self.frame2one_hot] = pickle.load(fp, encoding="latin1")
             
        self.n_topics = len(self.topic2one_hot)
        self.n_ideologies = len(self.ideology_id2one_hot)
        '''
        
        with open(os.path.join(self.data_path, 'drail_data.pickle'), 'rb') as fp:
            [self.tweet_id2tokens, self.entity_id2tokens,\
             self.ideology_id2one_hot,\
             self.word_dict, self.vocab_size, self.word_emb_size, word2idx]  = pickle.load(fp, encoding="latin1")
        

        self.idx2word = {v: k for k, v in word2idx.items()}

        # Need a dictionary to map back to labels
        self.role2idx = {'care-q1': 0, 'care-q2': 1, 'care-q3': 2,\
                         'loyalty-q3': 3, 'loyalty-q4': 4, 'loyalty-q5': 5,\
                         'authority-q2': 6, 'authority-q3': 7, 'authority-q4': 8, 'authority-q5': 9,\
                         'fairness-q2': 10, 'fairness-q3': 11, 'fairness-q4': 12,\
                         'sanctity-q1': 13, 'sanctity-q2': 14, 'sanctity-q3': 15}

        self.mf2idx = {'care': 0, 'fairness': 1, 'authority': 2, 'loyalty': 3, 'sanctity': 4}
        
        self.yesNo = {'yes': 1, 'no': 0}

    def tweet_bert(self, rule_grd):
        # Returns tweet bert input in first position
        in_instance = rule_grd.get_body_predicates("InInstance")[0]
        tweet_id, _ = in_instance['arguments']
        vector = self.tweet_id2bert[tweet_id]['input_ids']
        mask = self.tweet_id2bert[tweet_id]['attention_mask']
        return (vector, mask)

    def entity_bert_second(self, rule_grd):
        # Returns tweet bert input in second position (remove CLS)
        has_entity = rule_grd.get_body_predicates("HasEntity")[0]
        _, entity_id = has_entity['arguments']
        vector = self.entity_id2bert[entity_id]['input_ids'][1:]
        mask = self.entity_id2bert[entity_id]['attention_mask'][1:]
        return (vector, mask)
    
    def ideology1hot(self, rule_grd):
        has_ideology = rule_grd.get_body_predicates("HasIdeology")[0]
        _, ideology_id = has_ideology['arguments']
        
        if ideology_id in self.ideology_id2one_hot:
            return self.ideology_id2one_hot[ideology_id]
        else:
            return [0.0] * self.n_ideologies

    def topic1hot(self, rule_grd):
        has_topic = rule_grd.get_body_predicates("HasTopic")[0]
        _, topic = has_topic['arguments']
        #print(topic, self.topic2one_hot[topic])
        if topic in self.topic2one_hot:
            return self.topic2one_hot[topic]
        else:
            return [0.0] * self.n_topics

    def extract_multiclass_head(self, rule_grd):
        # Maps the head of a rule to a label
        head = rule_grd.get_head_predicate()
        if head['name'] == 'HasRole':
            label = head['arguments'][-1]
            return self.role2idx[label]
        elif head['name'] == 'HasMf':
            label = head['arguments'][-1]
            return self.mf2idx[label]
        elif head['name'] == 'MentionsConcept':
            label = head['arguments'][-1]
            return self.yesNo[label]
        else:
            print("Head {} not suported.".format(head['name']))
            exit(-1)
            
            from itertools import islice

    def window(self, seq, n=2):
        "Returns a sliding window (of width n) over data from the iterable"
        "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
        it = iter(seq)
        result = tuple(islice(it, n))
        if len(result) == n:
            yield result
        for elem in it:
            result = result[1:] + (elem,)
            yield result
            
    def tweet_concept_cosine_dist(self, rule_grd):
        head = rule_grd.get_head_predicate()
        tweet_id, concept, _ = head['arguments']
        tokens = self.tweet_id2tokens[tweet_id]
        tweet_words = [self.idx2word[t] for t in tokens]
        
        support = self.concept_support['Concept'][concept]
        concept_words = support.lower().split()
        k = len(concept_words)
        
        concept_embed = [self.word_dict[t] for t in concept_words]
        concept_embed = np.mean(concept_embed, axis=0)
        
        min_distance = 1000
        min_span = ""
        for seq in self.window(tweet_words, n=k):
            seq_embed = [self.word_dict[t] for t in seq]
            seq_embed = np.mean(seq_embed, axis=0)
            
            dist = cosine(concept_embed, seq_embed)
            if dist < min_distance:
                min_distance = dist
                min_span = seq
        
        #if min_distance < 0.5:
        #    print(concept_words, tweet_words, min_distance)
        return [min_distance]
    
    def rule_key(self, rule_grd):
        return [rule_grd.body_str]
        
    def bias(self, rule_grd):
        return [1.0]
